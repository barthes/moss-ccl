;;;-*- Mode: Lisp; Package: "MLN" -*-
;;;===============================================================================
;;;20/01/06
;;;                        MLN Names (file moss-mln.lisp)
;;;
;;;===============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
#|
2014
 0625 creation
 0708 rewriting mln-merge with loop construct to get rid of mapcan!
 0727 adding make-mln-from-plist
 0812 adding seset-valuest-values
 0815 correcting bug in set-values
 1119 making the package compatible with the old MLN format
2015
 0210 adding :zh to language tags
 1108 adding get-languages
2016
 0218 :PL removed from allowed languages
 0617 adding normalize-values
2020
 0106 correcting translate
|#

#|
Functions are:
  MLN?
  ADD-VALUE (mln value language-tag &key allow-duplicates)
  CLEAN-VALUES (value-list)
  EXTRACT (mln &key (language *language*) always canonical)
  EXTRACT-ALL-SYNONYMS (mln)
  EXTRACT-MLN-FROM-REF (ref language &key (default :unknown)
  EXTRACT-TO-STRING (mln &key (language *language*) always canonical (separator ", ")
  FILTER-LANGUAGE (mln language-tag)
  GET-CANONICAL-NAME (mln)
  GET-LANGUAGES (mln)
  IDENTICAL? (mln1 mln2)
  INCLUDED (mln1 mln2)
  MAKE-MLN (input &key language (separator #\;))
  MAKE-MLN-FROM-PLIST (expr &optional (separator #\,))
  %MLN? (expr)
  MLN-EQUAL (mln1 mln2 &key language)
  SET-VALUE (mln language-tag syn-string)
  SET-VALUES (mln language-tag val-list)
  SUBTRACT (mln1 mln2)
  TRANSLATE (mln &key (direction :to-list)(separator #\;))
|#
;;;============================ Multilingual names =============================
;;;
;;; The multimingual name type (MLN) was introduced to take care of international 
;;; ontologies.
;;; An entry has the following format:
;;;    ({(<language-tag> <string>+)}+)
;;; <language-tag> ::= :en | :fr | :it | ...
;;; ex:
;;;    ((:en "city" "town") (:fr "ville" "cité" "Municipalité"))
;;;
;;; The following functions are intended to manipulate such names
;;;
;;; :unknown tag (currently illegal)
;;; ============
;;; One new feature is the introduction of the :unknown language tag. When an
;;; unknown string is present in an MLN, two cases may occur: (i) it is the 
;;; only tag, in which case it simply denotes the fact that the string is of
;;; an unknown language; or (ii) it is not the only language tag, in which case
;;; the unknown string may be one of the already present language. Another point
;;; is that the sysnonyms associated with an :unknown string may be from 
;;; different languages.
;;; Thus, when checking for equality or membership, we'll have to check the 
;;; :unknown string

;;; Note: in this version, then :unknown tag is illegal

;;; the set of functions has been modified so as to be compatible with the old
;;; format:
;;;    ({<language-tag> <synonym string>}+)
;;; <language-tag> ::= :en | :fr | :it | ... | :unknown
;;; <synonym-string> ::= "string {; string}*"
;;; ex:
;;;    (:en "city ; town" :fr "ville ; cité : Municipalité")
;;;  
;;; which looked nicer but was more expensive in computational power.
;;; Functions accept both formats but return the new one (a-list format)
;;;=============================================================================

(in-package :mln)

#|
(eval-when (:compile-toplevel :load-toplevel :execute)
  (import '(moss::string+ ; replace (format nil ...)
            moss::*language*))
  (export '(*language-tags*))
  )
|#

;;;=============================================================================
;;;                          Globals (see globals.lisp)
;;;=============================================================================

#|
(defParameter *language-tags* 
  '(:cn :de :en :es :fr :it :jp :pt :br :zh :zh-hant :unknown) 
  "allowed languages")
|#

;;;=============================================================================
;;;                         Service functions
;;;=============================================================================


;;;=============================================================================
;;;                          ALIST functions
;;;=============================================================================
;;; duplicates the functions from MOSS

;;;------------------------------------------------------------------- ALISTP

(defUn alistp (ll)
  (and (listp ll)(every #'listp ll)))

#|
(alistp nil)
T
(alistp '((1 A)(B 2) ("C" 3)))
T
(alistp '((1 A)(B 2) ("C" 3) D))
NIL
(alistp 22)
NIL
|#
;;;---------------------------------------------------------------- ALIST-ADD

(defun alist-add (ll tag val)
  (cond
   ((null ll) `((,tag ,val)))
   ((equal+ (caar ll) tag) 
    (cons (cons (caar ll) (append (cdar ll)(list val))) (cdr ll)))
   (t (cons (car ll)(alist-add (cdr ll) tag val)))))

#|
(alist-add '((1 A)(B 2) ("C" 3))  "c" "cc")
((1 A) (B 2) ("C" 3 "cc"))
(alist-add '((1 A)(B 2) ("C" 3))  'd "dd")
((1 A) (B 2) ("C" 3) (D "dd"))
(alist-add '((1 A)(B 2) ("C" 3))  1 'AA)
((1 A AA) (B 2) ("C" 3))
|#
;;;--------------------------------------------------------- ALIST-ADD-VALUES

(defun alist-add-values (ll tag values)
  (cond
   ((null ll) (list (cons tag values)))
   ((equal+ (caar ll) tag) 
    (cons (cons (caar ll) (append (cdar ll) values)) (cdr ll)))
   (t (cons (car ll)(alist-add-values (cdr ll) tag values)))))

#|
(alist-add-values '((1 A)(B 2) ("C" 3))  "c" '("cc" ccc))
((1 A) (B 2) ("C" 3 "cc" CCC))
(alist-add-values '((1 A)(B 2) ("C" 3))  'd '("dd" dd))
((1 A) (B 2) ("C" 3) (D "dd" DD))
(alist-add-values '((1 A)(B 2) ("C" 3))  1 '())
((1 A AA) (B 2) ("C" 3))
(alist-add-values nil :a '(1 2))
((:A 1 2))
|#
;;;---------------------------------------------------------------- ALIST-REM

(defun alist-rem (ll tag)
  (remove tag ll :key #'car :test #'equal+))

#|
(alist-rem '((1 A)(B 2) ("C" 3))  "c")
((1 A)(B 2))
(alist-rem '((1 A)(B 2) ("C" 3))  1)
((B 2) ("C" 3))
|#
;;;------------------------------------------------------------ ALIST-REM-VAL

(defun alist-rem-val (ll tag val &aux temp)
  (cond
   ((null ll) nil)
   ((and (equal+ (caar ll) tag)(member val (cdar ll) :test #'equal+))
    (setq temp (remove val (cdar ll) :test #'equal+))
    (if temp (cons (cons (caar ll) temp)(cdr ll))
      (cdr ll)))
   (t (cons (car ll) (alist-rem-val (cdr ll) tag val)))))

#|
(alist-rem-val '((1 A)(B 2) ("C" 3))  "c" 3)
((1 A) (B 2))
(alist-rem-val '((1 A)(B 2) ("C" 3))  "c" 4)
((1 A) (B 2) ("C" 3))
(alist-rem-val '((1 A)(B "b" 2 22) ("C" 3))  'b "b")
((1 A) (B 2 22) ("C" 3))
|#
;;;---------------------------------------------------------------- ALIST-SET

(defun alist-set (ll tag val) 
  "replaces value associated to a specific property in an a-list"
  (cond ((null ll) (list (list tag val)))
        ((equal+ tag (caar ll))
         (cons (list tag val)(cdr ll)))
        (t (cons (car ll) (alist-set (cdr ll) tag val)))))

#|
(alist-set '((1 A)(B 2) ("C" 3))  "c" 333)
((1 A) (B 2) ("c" 333))

(alist-set '((1 A)(B 2) ("C" 3))  :d 4)
((1 A) (B 2) ("C" 3) (:D 4))

(alist-set '((1 A)(B 2) ("C" 3))  1 :A)
((1 :A) (B 2) ("C" 3))

(alist-set nil  :d 4)
((:D 4))
|#
;;;--------------------------------------------------------- ALIST-SET-VALUES

(defun alist-set-values (ll tag values) 
  "replaces value associated to a specific property in an a-list"
  (cond ((null ll) (list (cons tag values)))
        ((equal+ tag (caar ll))
         (cons (cons tag values)(cdr ll)))
        (t (cons (car ll) (alist-set-values (cdr ll) tag values)))))

#|
(alist-set-values '((1 A)(B 2) ("C" 3))  "c" '(333 3333))
((1 A) (B 2) ("c" 333 3333))

(alist-set-values '((1 A)(B 2) ("C" 3))  :d '(4))
((1 A) (B 2) ("C" 3) (:D 4))

(alist-set-values '((1 A)(B 2) ("C" 3))  1 '(:A :aa))
((1 :A :AA) (B 2) ("C" 3))

(alist-set-values nil  :d '(4 "dd"))
((:D 4 "dd"))

(alist-set-values'((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("cc" 3) ("dd" 4) ("ee" 5)))
                   'b '(("cc" 3) ("ee" 5)))
((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("cc" 3) ("ee" 5)))
|#
;;;================= End alist functions ====================================

;;;-------------------------------------------------------- MAKE-VALUE-STRING
;;; make-value-string is currently the same as make-entry-point but returns
;;; a string rather than an interned symbol. However, the make-entry-point
;;; could use an intermark strategy for example,
;;;    " Le  vilain petit   canard    "  => VILPC
;;; this is a sort of hash code with possible collisions however.

(defUn make-value-string (value &optional (interchar '#\.))
  "Takes an input string, removing all leading and trailing blanks, replacing ~
   all substrings of blanks and simple quote with a single underscore, and ~
   building a new string capitalizing all letters. The function is used ~
   to normalize values for comparing a value with an external one while ~
   querying the database.  
      French accentuated letter are replaced with unaccentuated capitals."
  (let* ((input-string (if (stringp value) value
                         (format nil "~S" value)))
         (work-list (map 'list #'(lambda(x) x) 
                      ;; remove leading and trailing blanks
                      (string-trim '(#\space) input-string))))
    (string-upcase   ; we normalize entry-point to capital letters
     (map
         'string
       #'(lambda(x) x) 
       ;; this mapcon removes the blocks of useless spaces and uses
       ;; a translation table to replace French letters with the equivalent
       ;; unaccentuated capitals
       (mapcon 
           #'(lambda(x) 
               (cond 
                ;; if we have 2 successive blanks we remove one
                ((and (cdr x)(eq (car x) '#\space)
                      (eq (cadr x) '#\space)) 
                 nil)
                ;; if we have a single blank we replace it with an underscore
                ((eq (car x) '#\space) (copy-list (list interchar)))
                ;; if the char is in the translation table we use the table
                ;((assoc (car x) *translation-table*) ; JPB0901
                ; (copy-list(cdr(assoc (car x) *translation-table*))))
                ;; otherwise we simply list the char
                (t (list(car x)))))
         work-list)))))

#|
? (make-value-string " Le  vilain petit   canard    ")
"LE.VILAIN.PETIT.CANARD"

(make-value-string "un jour d'été d'aujourd'hui à  Ogour")
"UN.JOUR.D'ÉTÉ.D'AUJOURD'HUI.À.OGOUR"

;;;compare with
(moss::%string-norm " Le  vilain petit   canard    ")
"LE-VILAIN-PETIT-CANARD"
(moss::%string-norm "un jour d'été  d'aujourd'hui à  Oïgour")
"UN-JOUR-D'ÉTÉ-D'AUJOURD'HUI-À-OÏGOUR"

;; however
(make-value-string "un jour d'été d'aujourd'hui à  Ogour"  #\-)
"UN-JOUR-D'ÉTÉ-D'AUJOURD'HUI-À-OGOUR"
|#
;;;--------------------------------------------------------------------- SPLIT-VAL

(defUn split-val (text &optional (separator #\,))
  "Takes a string, consider it as a synonym string and extract items separated ~
   by a comma Returns the list of string items.
Arguments:
   text: a string
Return
   a list of strings."
  (let (pos result word)
    (unless (and text (stringp text)) (return-from split-val text))
    (loop
      ;; remove trailing blanks
      (setq text (string-trim '(#\space) text))
      ;; is there any space left?
      (setq pos (position separator text))
      (unless pos
        (push text result)
        (return-from split-val (reverse result)))
      ;; extract first word
      (setq word (string-trim '(#\space) (subseq text 0 pos))
          text (subseq text (1+ pos)))
      (push word result)
      )))

#|
(split-val "Barthès: J.-P., Sugawara: Kenji, Fujita: S.")
("Barthès: J.-P." "Sugawara: Kenji" "Fujita: S.")

(split-val " Barthès: J.-P. " #\space)
("Barthès:" "J.-P.")
|#
;;;=============================================================================
;;;                         MLN Functions
;;;=============================================================================
;;; We implement here a package meant to replace the %mln-xxx functions of the
;;; MOSS package. We go from a string representation 
;;;   (:en "title; header" :fr "titre")
;;; to a list representation
;;;   ((:en "title" "header") (:fr "titre"))
;;; which is less nice but more efficient internally.
;;; The concept of synonyms (a string with separating semi-columns) is replaced 
;;; by a list of different strings, which makes most of the %synonym-xxx functions
;;; useless. However, some of the old functions were dealing with strings repre
;;; senting sets of synonyms and intermreting them as possible MLNs when associated
;;; with the current language (*language*). Tentatively, we replace them by a
;;; string (with no ";" or a list of strings (that could come from executing a
;;; (filter-language function).
;;; Finally to go from the old format to the new format the translate function
;;; does the job.

;;;------------------------------------------------------------------------ MLN?

(defUn mln? (expr &key (allow-old-format t))
  "Checks whether a list is a multilingual name Uses global *language-tags* variable. ~
   nil is not a valid MLN.
Argument:
   expr: something like ((:en \"London\") (:fr \"Londres\"))
                     or ((:fr \"Paris\" \"Lutèce\"))
Result: 
   T if OK, nil otherwise"
  (or
   (and expr
        (listp expr)
        (every #'(lambda(xx)(and (listp xx)
                                 (member (car xx) *language-tags*)
                                 (cdr xx)
                                 (every #'stringp (cdr xx))))
               expr))
   ;; to take into account old format
   (if allow-old-format (%mln? expr))))

#|
(mln? nil)
NIL

(MLN? "ALBERT")
NIL

(mln? '((:en "London")( :fr "Londres")))
T

(mln? '((:en "London")( :fr )))
NIL

(mln? '((:en "London")( :fr nil)))
NIL

(mln? '((:en "London")(:zz "London")))
NIL

(MLN? '(:name :en "London" :fr "Londres"))
T ; old format

(MLN? '(:name :en "London" :zz "Londres"))
NIL ; :zz not a language

(MLN? '(:name :en "London" :fr "Londres") :allow-old-format nil)
NIL

(MLN? '(:name :en "London" :zz "Londres") :allow-old-format t)
NIL ; zz not a language
|#
;;;------------------------------------------------------------------- ADD-VALUE

(defUn add-value (mln value language-tag &key allow-duplicates)
  "Adds a value corresponding to a specific language at the end of the list.
Arguments:
   mln: multilingual name
   value: single value coerced to a string
   language-tag: must be legal, i.e. part of *language-tags*
Return:
   the modified mln."
  (declare (special *language-tags*))
  (unless (member language-tag *language-tags*)
    (error "illegal language tag: ~S~&Legal ones are: ~{~S~^, ~}"
      language-tag *language-tags*))
  
  (let ((values (cdr (assoc language-tag mln)))
        (value (string-trim '(#\space) (format nil "~A" value))))
    (cond
     (allow-duplicates
      (alist-add-values mln language-tag (list value)))
     ;; here we must check that new value is not already present
     ((member value values :test #'string-equal)
      mln)
     (t
      (alist-add-values mln language-tag (list value))))))

#|
(mln::add-value nil "Albert " :fr)
((:FR "Albert"))
(mln::add-value * "Jerôme " :fr)
((:FR "Albert" "Jerôme"))
(mln::add-value * "John " :en)
((:FR "Albert" "Jerôme") (:EN "John"))
(mln::add-value * "Albert " :fr)
((:FR "Albert" "Jerôme") (:EN "John"))
(mln::add-value * "Albert " :fr :allow-duplicates t)
((:FR "Albert" "Jerôme" "Albert") (:EN "John"))
|#
;;;---------------------------------------------------------------- CLEAN-VALUES

(defun clean-values (value-list)
  "remove duplicates from a list preserving order"
  (cond
   ((null value-list) nil)
   (t (cons (car value-list)
            (clean-values
             (remove (car value-list) (cdr value-list) :test #'string-equal))))))

#|
(clean-values '("banane" "banane plantin" "banane plantin" "banane"))
("banane" "banane plantin")
|#
;;;------------------------------------------------------------------- EQUAL+
;;; Extends equality to strings and MLNs (a string is an not an MLN)
;;; Two MLN are equal if
;;; - they are both strings and equal
;;; - if language L is specifies and not :all
;;;   - if one is a string and is present in the synonyms of the other in L
;;;   - if one is a string and is present in the other's unknown language
;;;   - if the two sets of synonyms for L share some
;;;   - if the synonyms for L in one share those of :unknown 
;;;   - if the two sets for unknown share some synonyms
;;; - if language is :all
;;;   - if all the synonyms of any language for both share some
;;; - if language is nil
;;;   - if *language* is unbound or equal to :unknown (NOT TRUE)
;;;     - if both sets of synonyms for a language are intersecting
;;;     - if a set of synonyms for a language is intersecting with the unknown 
;;;       set of the other
;;;   - if *language* is :all (same as when language is :all) not realistic!
;;;   - if *language* is a legal language
;;;     - if both sets of synonyms for *language* are intersecting
;;;     - if a set of synonyms for *language* is intersecting with the unknown 
;;;       set of the other

(defun equal+ (aa bb &key language)
  "test if aa and bb are same, generalize to strings using string-equal and MLNs
Arguments:
   aa: first arg
   bb: second arg
   language: if aa and bb are MLNs restricts the language, unless it is :all"
  (cond
   ((and (stringp aa)(stringp bb))
    (string-equal aa bb))
   ((equal aa bb))
   ((and (or (stringp aa)(mln::mln? aa))(or (stringp bb)(mln::mln? bb)))
    (mln::mln-equal aa bb :language language)) ; jpb 1406
   ))

;;;--------------------------------------------------------------------- EXTRACT
;;; %mln-extract was defined in the MOSS package thus *language* is in the MOSS
;;; package

(defUn extract (mln &key language always canonical raw)
  "Extracts from the MLN the string corresponding to specified language.
   If mln is a string returns the string in a list.
   If language is :all, returns a list of all languages.
   If always is t, then tries to return something: tries English, ~
      then first recorded language.
Arguments:
   mln: a multilingual name (can also be a simple string)
   language-tag (key): a legal language tag (part of *language-tags*)
   always (key): if true, always return something unless syntax or language error
   canonical (key): if true returns the first name in the set of synonyms
   raw (key): gets value directly from MLN structure, no tests are done
Return:
  2 values: a list of synonyms and language
  declare error on bad MLN syntax." 
  ;; if language is not specified, use the local current default language
  (unless language
    (setq language (symbol-value (intern "*LANGUAGE*"))))
  
  ;; transform the MLN into the new syntax
  (if (%mln? mln) (setq mln (make-mln mln)))
  
  ;; if raw option return the list of synonyms right away with language
  (if raw (return-from extract (values (cdr (assoc language mln)) language)))
  
  (let ((tag language)
        result)
    (cond
     ;; if a string returns it
     ((stringp mln)
      (setq result (list mln) tag :unknown))
     ;;========== take care of old syntax
     ((%mln? mln)
      (return-from extract
        (extract (make-mln mln) :language language :always always 
                 :canonical canonical)))
     ;;==========
     ;; bad syntax?
     ((not (mln? mln))
      (error "illegal mln format: ~S" mln))
     ;; if language is :all
     ((eql :all language) 
      (setq result (extract-all-synonyms mln)
          tag :unknown))
     ;; illegal language tag?
     ((not (member language *language-tags*))
      (error "illegal language tag: ~S~&Legal ones are: ~{~S~^, ~}"
        language *language-tags*))
     ;; return nil if language is not there
     ((not always)
      (setq result (cdr (assoc language mln))))
     ;; here we return always something
     ((setq result (cdr (assoc language mln))))
     ((setq result (cdr (assoc :en mln)))
      (setq tag :en))
     ((setq tag (caar mln) result (cdar mln)))
     )
    ;; now if canonical is true and result is not nil, we extract first name
    (when (and result canonical)
      (setq result (list (car result))))
    ;; return value and language tag
    (values result tag)
    ))

#|
(mln::extract '((:en "car" "automobile") (:fr "la voiture est en panne" "OK?"))
              :language :fr)
("la voiture est en panne" "OK?")
:FR

(mln::extract '(:en "car;automobile" :fr "la voiture est en panne; OK?")
              :language :fr)
("la voiture est en panne" "OK?")
:FR

(mln::extract '((:en "car" "automobile") (:fr "la voiture est en panne" "OK?"))
              :language :jp)
NIL
:JP

(mln::extract '(:en "car;automobile" :fr "la voiture est en panne; OK?")
              :language :jp)
NIL
:JP

(mln::extract '((:en "car" "automobile") (:fr "la voiture est en panne" "OK?"))
              :language :en)
("car" "automobile")
:EN

(mln::extract '(:en "car;automobile" :fr "la voiture est en panne; OK?")
              :language :en)
("car" "automobile")
:EN

(mln::extract '((:en "car" "automobile") (:fr "la voiture est en panne" "OK?"))
              :language :en :canonical t)
("car")
:EN

(mln::extract '(:en "car;automobile" :fr "la voiture est en panne; OK?")
              :language :en :canonical t)
("car")
:EN

(mln::extract "la voiture est en panne" :language :en)
("la voiture est en panne")
:UNKNOWN

(mln::extract '(:en "car;automobile" :fr "la voiture est en panne; OK?")
              :language :fr :raw t)
("la voiture est en panne; OK?")
:FR

(mln::extract '((:en "la voiture est en panne" 2)) :language :en)
Error: illegal mln format: ((:EN "la voiture est en panne" 2))
|#
;;;-------------------------------------------------------- EXTRACT-ALL-SYNONYMS

(defUn extract-all-synonyms (mln)
  "Extracts all synonyms as a list of strings regardless of the language.
Arguments:
   mln: multilingual name
Return
   a list of strings."
  (cond
   ;;========== old format
   ((%mln? mln)
    (extract-all-synonyms (make-mln mln)))
   ;;===========
   ((not (mln? mln))
    (error "mln arg: ~S has not the proper format." mln))
   (t
    (delete-duplicates (reduce #'append (mapcar #'cdr mln)) :test #'equal+))))

#|
(mln::extract-all-synonyms
 '((:EN "person" "guy" "bloke") (:FR "personne" "gus" "type" "mec") 
   (:IT "ragazza" "gus")))
("person" "guy" "bloke" "personne" "type" "mec" "ragazza" "gus")

(mln::extract-all-synonyms
 '(:EN "person; guy; bloke" :FR "personne;gus;type; mec" :IT "ragazza; gus"))
("person" "guy" "bloke" "personne" "type" "mec" "ragazza" "gus")
|#
;;;-------------------------------------------------------- EXTRACT-MLN-FROM-REF
;;; the function is used to build ontologies. Now ontologies are defined in files
;;; When loading the file the definitions can be multilingual, in which case  the
;;; :all language marker will allow to build multilingual concepts
;;; If the language marker is specific, then only the corresponding labels will
;;; be selected. If they are missing, then the :default if there will be chosen,
;;; otherwise a random language tag will be picked.
;;; Files defined as multilingual should be processed using the :all language tag.
;;; If the defining file does not contain language tags, then a default language
;;; tag can be supplied, e.g. :en or :fr, corresponding to the language of the 
;;; file.
;;; If the defining file is a mixture of languages (e.g. bibliography) then the
;;; global :unknown tag can be used. It will build MLN prefixed by :unknown
;;; The advantage is that article keywords of unknown language can still be merged
;;; by existing keywords defined in a specific language.

(defUn extract-mln-from-ref (ref language &key (default :unknown) &aux $$$)
  "takes a ref input symbol, string or MLN and builds a specific mln restricted ~
   to the specified language. 
   If language is :all 
      - if ref is a string or symbol, then language defaults to default, 
        e.g. (:unknown <string>).
      - if ref is MLN, then returns the normalized MLN.
   If language is not :all
      - if ref is string or symbol returns (<language> <string>)
      - if ref is an MLN containing language, returns the specific part of MLN.
      - if ref is an MLN not containing language, then returns
         - the unknwon part if :unknown is part of MLN
         - first found language otherwise.
   Removes the leading :name marker if present in MLN.
Arguments:
   ref: a string symbol or mln
   language a language tag or :all
   default (key): default language tag (default: :EN)
Result:
   a valid mln"
  (cond 
   ;; when multilingual-string is a simple string, we assume that it is a
   ;; shorthand notation for (:<current-language> "...")
   ((and (stringp ref) (eql language :all))
    ;; defaults to default (usually :unknown)
    (make-mln ref :language default))
   ;; here language is not :all (no check on validity of tag?)
   ((stringp ref)
    (make-mln ref :language language))
   ;; if a symbol same idea
   ((and (symbolp ref)(eql language :all))
    ;; defaults to default
    (make-mln ref :language default)
    )
   ((symbolp ref)
    (make-mln ref :language language))
   ;;========== old format?
   ((%mln? ref)
    (extract-mln-from-ref (make-mln ref) language :default default))
   ;;========== here ref should be an MLN with the new format
   ;; not mln error
   ((not (mln? ref))
    (error "(mln::extract-mln-from-ref) bad ref format: ~S" ref))
   ;; language is :all, do nothing
   ((eql language :all)
    ref)
   ;; language not :all call filter
   ((null (setq $$$ (filter-language ref language)))
    ;; return default or first found language and send a warning
    (if (setq $$$ (filter-language ref default))
        (make-mln $$$ :language default)
      (progn
        (warn "(mln-extract-mln-from-ref) default language ~S not part of ref: ~
               ~& ~S~&We use first entry: ~S"
          default ref (list (car ref) (cadr ref)))
        (list (car ref)))))
   ;; otherwise return part corresponding to language
   (t (make-mln $$$ :language language))))

#|
(mln::extract-mln-from-ref "titre" :fr)
((:FR "titre"))

(mln::extract-mln-from-ref 'titre :all)
((:UNKNOWN "TITRE"))

(mln::extract-mln-from-ref 'titre :all :default :fr)
((:FR "TITRE"))

(mln::extract-mln-from-ref '((:fr "titre") (:en "title")) :all)
((:FR "titre") (:EN "title"))

(mln::extract-mln-from-ref '(:fr "titre; tiiitre" :en "title") :all)
((:FR "titre" "tiiitre") (:EN "title"))

(mln::extract-mln-from-ref '((:fr "titre") (:en "title")) :fr)
((:FR "titre"))

(mln::extract-mln-from-ref '(:fr "titre; tiiitre" :en "title") :fr)
((:FR "titre" "tiiitre"))

(mln::extract-mln-from-ref '((:fr "titre") (:en "title")) :jp)
Warning: (mln-extract-mln-from-ref) default language :UNKNOWN not part of ref: 
((:FR "titre") (:EN "title"))
We use first entry: ((:FR "titre") (:EN "title"))
((:FR "titre"))

(mln::extract-mln-from-ref '((:fr "titre" "tiiitre") (:en "title")) :jp)
Warning: (mln-extract-mln-from-ref) default language :UNKNOWN not part of ref: 
((:FR "titre; tiiitre") (:EN "title"))
We use first entry: ((:FR "titre; tiiitre") (:EN "title"))
((:FR "titre" "tiiitre"))

(mln::extract-mln-from-ref 
 '((:fr "titre") (:en "title" "header")) :pl :default :en)
((:EN "title" "header"))

(mln::extract-mln-from-ref 
 '(:fr "titre" :en "title; header") :jp :default :en)
((:EN "title" "header"))

(mln::extract-mln-from-ref '((:fr "titre") (:en "title")) :jp :default :it)
Warning: (mln::extract-mln-from-ref) default language :IT not part of ref: 
(:FR "titre" :EN "title")
We use first entry: (:FR "titre")
((:FR "titre"))

(mln::extract-mln-from-ref '(:fr "titre;tiiitre" :en "title") :jp :default :it)
Warning: (mln-extract-mln-from-ref) default language :IT not part of ref: 
((:FR "titre" "tiiitre") (:EN "title"))
We use first entry: ((:FR "titre" "tiiitre") (:EN "title"))
((:FR "titre" "tiiitre"))

(mln::extract-mln-from-ref '((:fr "titre") (:en "title")) :jp)
((:FR "titre"))

(mln::extract-mln-from-ref '(:fr "titre" :en "title") :jp)
Warning: (mln-extract-mln-from-ref) default language :UNKNOWN not part of ref: 
((:FR "titre") (:EN "title"))
We use first entry: ((:FR "titre") (:EN "title"))
((:FR "titre"))

(mln::extract-mln-from-ref "titre" :unknown)
((:UNKNOWN "titre"))
|#
;;;----------------------------------------------------------- EXTRACT-TO-STRING

(defun extract-to-string (mln &key language always canonical (separator ", "))
  "presents the result as a string each synonym separated by a separator.
Arguments:
   mln: mln
   language (key): target language (default *language*)
   always (key): if t returns always something
   canonical (key): if t returns a single representative string
   separator (key): a char to separate synonyms (defual #\,)
Returns:
   a single string (possible empty)."
  (declare (special *language*))
  (let ((language (or language *language*))
        syn-list)
    ;; nil returns empty string (however nil is an illegal mln...
    (unless mln 
      (error "Illegal mln format: NIL"))
      ;(return-from extract-to-string ""))
    ;; canonical option returns a single string?
    (if canonical 
        (return-from extract-to-string
          (car (extract mln :language language :canonical canonical))))
    ;; otherwise get all the synoynms
    (setq syn-list (extract mln :language language :always always))
    ;; insert separators
    (setq syn-list
          (nbutlast 
           (reduce #'append
                   (mapcar #'(lambda (xx) (list xx separator)) 
                     syn-list))))
    ;; return string
    (if syn-list
        (apply #'string+ syn-list)
      "")))

#|
(extract-to-string '((:fr "homme" "gus" "pékin")(:en "man" "bloke")))
"man, bloke"

(extract-to-string '(:fr "homme;gus;pékin" :en "man;bloke"))
"man, bloke"

(extract-to-string '((:fr "homme" "gus" "pékin")(:en "man" "bloke"))
                   :language :fr)
"homme, gus, pékin"

(extract-to-string '(:fr "homme;gus;pékin" :en "man;bloke") :language :fr)
"homme, gus, pékin"

(extract-to-string '((:fr "homme" "gus" "pékin")(:en "man" "bloke"))
                   :canonical t)
"man"

(extract-to-string '(:fr "homme;gus;pékin" :en "man;bloke") :canonical t)
"man"

(extract-to-string '((:fr "homme" "gus" "pékin")(:en "man" "bloke"))
                   :language :fr :canonical t)
"homme"

(extract-to-string '(:fr "homme;gus;pékin" :en "man;bloke") :language :fr 
                   :canonical t)
"homme"

(extract-to-string '((:fr "homme" "gus" "pékin")(:en "man" "bloke"))
                   :language :fr :separator "; ")
"homme; gus; pékin"

(extract-to-string '(:fr "homme;gus;pékin" :en "man;bloke") :language :fr 
                   :language :fr :separator "; ")
"homme; gus; pékin"

(extract-to-string nil :language :fr :separator "; ")
Error: illegal mln format: NIL

(extract-to-string '((:fr "homme" "gus" "pékin")(:en "man" "bloke"))
                   :language :pt :separator "; ")
""

(extract-to-string '(:fr "homme;gus;pékin" :en "man;bloke")
                   :language :pt :separator "; ")
""
|#
;;;------------------------------------------------------------- FILTER-LANGUAGE

(defUn filter-language (mln language-tag)
  "Extracts from the MLN the string corresponding to specified language.
   If mln is a string returns the string in a list
   If language is :all, returns a list containing all the languages.
   If language is :unknown, returns the string associated with the :unknown tag.
   If language is not present, then returns nil.
Arguments:
   mln: a multilingual name (can also be a simple string)
   language-tag: a legal language tag (part of *language-tags*)
Return:
   a string or nil. Throws to :error on bad syntax."
  (cond
   ;; if a string returns it
   ((stringp mln) (list mln))
   ;;========== old syntax?
   ((%mln? mln)
    (filter-language (make-mln mln) language-tag))
   ;;==========
   ;; bad syntax?
   ((not (mln? mln))
    (error "illegal mln format: ~S" mln))
   ;; if language is :all
   ((eql :all language-tag) 
    (extract-all-synonyms mln))
   ;; illegal language tag?
   ((not (member language-tag *language-tags*))
    (error "illegal language tag: ~S~&Legal ones are: ~{~S~^, ~}"
      language-tag *language-tags*))
   ;; return nil if language is not there
   (t (cdr (assoc language-tag mln)))
   ))

#|
(filter-language 
 '((:EN "person" "guy" "bloke") (:FR "personne" "gus" "type" "mec") 
   (:IT "ragazza")) 
 :fr)
("personne" "gus" "type" "mec")

(filter-language 
 '((:EN "person" "guy" "bloke") (:FR "personne" "gus" "type" "mec") 
   (:IT "ragazza")) 
 :all)
("person" "guy" "bloke" "personne" "gus" "type" "mec" "ragazza")

(filter-language 
 '((:EN "person" "guy" "bloke") (:FR "personne" "gus" "type" "mec") 
   (:IT "ragazza")) 
 :pl)
Error: illegal language tag: :PL
Legal ones are: :CN, :DE, :EN, :ES, :FR, :IT, :JP, :PT, :BR, :ZH, :ZH-HANT,
:UNKNOWN

(filter-language 
 '((:EN "person" "guy" "bloke") (:FR "personne" "gus" "type" "mec") 
   (:IT "ragazza")) 
 :jp)
NIL

(filter-language
 '(:EN "person; guy; bloke" :FR "personne; gus; type; mec" :IT "ragazza")
 :fr)
("personne" "gus" "type" "mec")

(filter-language
 '(:EN "person; guy; bloke" :FR "personne; gus; type; mec" :IT "ragazza")
 :pl)
Error: illegal language tag: :PL
Legal ones are: :CN, :DE, :EN, :ES, :FR, :IT, :JP, :PT, :BR, :ZH, :ZH-HANT,
:UNKNOWN
|#
;;;---------------------------------------------------------- GET-CANONICAL-NAME
;;;
;;; returned value should be name corresponding to *language* or first English
;;; name, or random name (first found language)

(defUn get-canonical-name (mln &key (language *language*) &aux names)
  "Extracts from a multilingual name the canonical name that will be used to build ~
   an object ID. By default it is the first name corresponding to the value of *language*, ~
   or else the first name of the English entry, or else the first name of the list. ~
   An error occurs when the argument is not multilingual name.
Arguments:
   mln: a multilingual name
   language (key): specified language (default is mln::*laguage*)
Return:
   2 values: a simple string (presumably for building an ID) and the language tag.
Error:
   error if not a multilingual name."
  (declare (special *language*))
  (cond
   ;;========== for backward compatibility
   ((%mln? mln)
    (get-canonical-name (make-mln mln :language language) :language language))
   ;;==========
   
   ((not (mln? mln))
    (error "~S has not the proper multilingual name format." mln))
   
   ;;== determine the proper canonical name (and corresponding language)
   ;; if language is :all (creation or loading time), then try :English
   ((and (setq names (filter-language mln :en))
         (eql :all language))
    (values (car names) :en))   
   ;; otherwise use first entry
   ((eql :all language)
    (values (cadar mln) (caar mln)))
   ;; if language is specified (not :all) and answer return data
   ((setq names (filter-language mln language))
    (values (car names) language))
   ;; language entry not there, try English
   ((setq names (filter-language mln :en))
    (values (car names) :en))
   ;; otherwise return first entry
   (t
    (values (cadar mln) (caar mln))))
  )

#|
? (mln::get-canonical-name '((:fr "personne" "quidam") (:en "person" "guy"))
                            :language :all)
"person "
:EN

? (mln::get-canonical-name '((:fr "personne" "quidam") (:en "person" "guy"))
                            :language :fr)
"personne"
:FR

? (mln::get-canonical-name '(:fr "personne;quidam" :en "person;guy")
                           :language :all)
"person "
:EN

? (mln::get-canonical-name '((:fr "personne" "quidam") (:it "persona")))
"personne "
:FR

? (mln::get-canonical-name '(:fr "personne;quidam" :it "persona")
                         :language :all)
"personne"
:FR

? (mln::get-canonical-name '((:fr "personne" "quidam") (:en "person" "guy"))
                           :language :pt)
"person"
:EN


? (mln::get-canonical-name '(:fr "personne;quidam" :en "person;guy") :language :pt)
"person"
:EN

? (mln::get-canonical-name '((:fr "personne" "quidam") (:en "person" "guy"))
                           :language :fr)
"personne"
:FR

? (mln::get-canonical-name '(:fr "personne;quidam" :en "person;guy") :language :fr)
"personne"
:FR

(mln::get-canonical-name '((:fr "personne"  "quidam") (:it )))
Error: ((:FR "personne" "quidam") (:IT)) has not the proper multilingual name format.
|#
;;;--------------------------------------------------------------- GET-LANGUAGES

(defun get-languages (mln)
  "retrieves the list of language tags from an mln"
  (if (%mln? mln) (setq mln (make-mln mln)))
  (mapcar #'car mln))

#|
(get-languages '((:fr "personne" "quidam") (:en "person" "guy")))
(:FR :EN)
|#
;;;------------------------------------------------------------------ IDENTICAL?

(defun identical? (mln1 mln2)
  "two mlns are identical if they have the same synonyms for the same language"
  (let (tag val1 val2)
    
    ;;========== compatibility with old format
    (if (%mln? mln1) (setq mln1 (make-mln mln1)))
    (if (%mln? mln2) (setq mln2 (make-mln mln2)))
    ;;==========
    
    (when (and mln1 mln2)
      (loop
        ;(format t "~%; mln1: ~S, mln2: ~S" mln1 mln2)
        ;; if both mlns are null then we are through they are identical
        (unless mln1
          (if mln2 (return-from identical? nil)
            (return-from identical? t)))
        ;; if mln1 is not nil but mln2 is, then failure
        (unless mln2
          (return-from identical? nil))
        
        ;; get first language tag  and value from mln1
        (setq tag (caar mln1) val1 (cdar mln1))
        ;; extract value list from mln2
        (setq val2 (extract mln2 :language tag))
        
        (when (or (set-difference val1 val2 :test #'string-equal)
                  (set-difference val2 val1 :test #'string-equal))
          (return-from identical? nil))
        
        (setq mln1 (cdr mln1))
        (setq mln2 (remove-language mln2 tag))
        ))))

#|
(mln::identical? '((:en "cabbage")) nil)
NIL
(mln::identical? '(:en "cabbage") nil)
NIL

(mln::identical? '((:en "cabbage")) '((:en "carrot")))
NIL
(mln::identical? '(:en "cabbage") '(:en "carrot"))
NIL

(mln::identical? nil '((:en "carrot")))
NIL
(mln::identical? '((:en "carrot")) '((:en "carrot")))
T
(mln::identical? '(:en "carrot") '(:en "carrot"))
T

(mln::identical? '((:en "carrot")) '((:en "carrot;cabbage")))
NIL
(mln::identical? '((:en "cabbage" "carrot")) '((:en "carrot" "cabbage")))
T
(mln::identical? '(:en "cabbage;carrot") '(:en "carrot;cabbage"))
T

(mln::identical? '((:en "carrot") (:fr "navet" "poireau"))
                 '((:fr "poireau" "navet") (:en "carrot")))
T
(mln::identical? '(:en "carrot" :fr "navet;poireau")
                 '(:fr "poireau;navet" :en "carrot"))
T
(mln::identical? '((:en "carrot") (:fr "navet" "poireau"))
                 '(:fr "poireau;navet" :en "carrot"))
T
|# 
;;;------------------------------------------------------------------- INCLUDED?

(defun included? (mln1 mln2)
  "checks if all synonyms of mln1 are included those of mln2 for all languages ~
   of mln1."
  (let (language val1 val2)
    
    ;;========== compatibility with old format
    (if (%mln? mln1) (setq mln1 (make-mln mln1)))
    (if (%mln? mln2) (setq mln2 (make-mln mln2)))
    ;;==========
    
    ;; check mlns (should get rid of the :name notation!
    (when (and (mln? mln1)(mln? mln2))
      (loop
        (unless mln1 (return))
        (setq language (caar mln1) val1 (cdar mln1))
        ;; get the target value
        (setq val2 (extract mln2 :language language))
        ;; if all values of mln1 are not in mln2 return failure
        (if (set-difference val1 val2 :test #'string-equal)
            (return-from included? nil))
        (setq mln1 (cdr mln1))
        ) ; end loop
      t)
    ))

#|
(included? '((:en "Albert")) '((:fr "Jean") (:en "John" "Albert")))
T
(included? '((:en "Albert")) '(:fr "Jean" :en "John; Albert"))
T

(included? '((:en "Albert" "Joe")) '((:fr "Jean") (:en "John" "Albert")))
NIL
(included? '(:en "Albert;Joe") '((:fr "Jean") (:en "John" "Albert")))
NIL

(included? '((:it "Albert")) '((:fr "Jean") (:en "John" "Albert")))
NIL
(included? '(:it "Albert") '(:fr "Jean" :en "John;Albert"))
NIL

(included? '() '((:fr "Jean") (:en "John" "Albert")))
NIL ; nil is not an MLN

(included? '((:en "Albert") (:fr "Jean")) '((:fr "Jean") (:en "John" "Albert")))
T
(included? '(:en "Albert" :fr "Jean") '(:fr "Jean" :en "John;Albert"))
T
(included? '((:en "Albert") (:fr "Jean")) '(:fr "Jean" :en "John;Albert"))
T
|#
;;;-------------------------------------------------------------------- MAKE-MLN

(defun make-mln (input &key language (separator #\;))
  "creates an mln from various inputs.
Argument:
   input: symbol, string, multiple-value string, list of strings, 
          mln with old format, mln
   language (key): one of the legal languages in case input is not mln
   separator (key): ignored
Return:
   mln with list format"
  (declare (special *language* *language-tags*))
  ;; first look at MLN inputs
  (cond
   ;; if already MLN, return value
   ((mln? input :allow-old-format nil) input)   
   ((null input) (error "Can't build an MLN from NIL"))
   ;; if old MLN format, translate it
   ((and (%mln? input) 
         (eql (car input) :name))
    (translate (cdr input) :separator separator)) ; jpb 1406
   ((%mln? input)(translate input :separator separator))

   ;; otherwise we must build the mln from symbol or strings
   (t
    ;; if language is not known then error
    (unless (or language (boundp *language*))
      (error "Unknown language for building MLN: ~S" input))
    (setq language (or language *language*))
    ;; if language is illegal, then error (:all is not a legal language for 
    ;; building mlns
    (unless (member language *language-tags*)
      (error "Illegal language: ~S for ~S.~%Legal ones are: ~S" language
        input *language-tags*))
    (cond
     ;; symbol
     ((symbolp input)
      `((,language ,(symbol-name input))))
     ;; string (simple or multiple valued
     ((stringp input)
      `((,language . ,(split-val input separator))))
     ;; string is a list of strings
     ((and (listp input)(every #'stringp input))
      `((,language ,@input)))
     (t
      (error "Cannot build an MLN form the input: ~S" input))
     ))))

#|
(moss::catch-system-error "ah!..."
       (mln::make-mln nil))
(NIL #<SIMPLE-ERROR @ #x219f5fa2>) 
... system error ah!...: 
#<SIMPLE-ERROR @ #x219f5fa2>
NIL

(mln::make-mln "Albert" :language :fr)
((:FR "Albert"))

(let ((*language* :en))
  (declare (special *language*))
  (mln::make-mln "Albert"))
((:EN "Albert"))

(mln::make-mln "Albert; Ursule; Jérôme" :language :fr)
((:FR "Albert" "Ursule" "Jérôme"))

(mln::make-mln '("Albert" "Ursule" "Arthur") :language :fr)
((:FR "Albert" "Ursule" "Arthur"))

(mln::make-mln '((:en "John")(:fr "Albert" "Ursule" "Arthur")))
((:EN "John") (:FR "Albert" "Ursule" "Arthur"))

(mln::make-mln '(:en "John" :fr "Albert; Ursule; Arthur"))
((:EN "John") (:FR "Albert" "Ursule" "Arthur"))

(mln::make-mln '(:en "John" :fr "Albert, Ursule,Arthur") :separator #\,)
((:EN "John") (:FR "Albert" "Ursule" "Arthur"))

(mln::make-mln '("Albert" "Ursule" 23) :language :fr)
Error: Cannot build an MLN form the input: ("Albert" "Ursule" 23)

(mln::make-mln '((:en "James")(:fr "Jean" "Albert")))
((:EN "James") (:FR "Jean" "Albert"))
|#
;;;--------------------------------------------------------- MAKE-MLN-FROM-PLIST

(defun make-mln-from-plist (expr &optional (separator #\,))
  "builds an MLN from a dismbodied plist, checking language tags and skiping ~
   empty values. If a value is a string (old format) will split the string into ~
   a list of strings using commas as separators.
Argument:
   expr: a disembodied plist
   separator (opt): ignored
Return:
   an MLN, or NIL or an error."
  (let ((item-list expr)
        tag val res result)
    (loop
      (if (null item-list)(return))
      ;; check language tag
      (setq tag (pop item-list))
      (unless (member tag *language-tags*)
        (error "illegal language tag: ~S. Legal ones are: ~S" tag *language-tags*))
      ;; if empty list complain
      (unless item-list 
        (error "bad syntax in ~S" expr))
      ;; process value
      (setq val (pop item-list))
      (cond
       ;; if value is nil, skip it
       ((null val))
       ;; if value is a string, split it
       ((stringp val)
        (setq res (split-val val separator))
        ;; note: empty strings are bona fide values  
        (push (cons tag res) result))
       ((and (listp val)(not (every #'stringp val)))
        (error  "bad syntax in ~S" expr))
       ((listp val)
        (push (cons tag val) result))
       )
      )
    (reverse result)))

#|
(make-mln-from-plist ())
NIL

(make-mln-from-plist '(:fr "Poire" :en "Pear"))
((:FR "Poire") (:EN "Pear"))

(make-mln-from-plist '(:fr "Poire, passe-crassane" :en "Pear"))
((:FR "Poire" "passe-crassane") (:EN "Pear"))

(make-mln-from-plist '(:fr "Poire, passe-crassane" :en nil))
((:FR "Poire" "passe-crassane"))

(make-mln-from-plist '(:fr "Poire, passe-crassane" :en))
Error: bad syntax in (:FR "Poire, passe-crassane" :EN)

(make-mln-from-plist '(:fr ("Poire" "Passe-Crassane") :en ("Pear")))
((:FR "Poire" "Passe-Crassane") (:EN "Pear"))

(make-mln-from-plist '(:fr ("Poire" passe-crassane) :en "Pear"))
Error: bad syntax in (:FR ("Poire" PASSE-CRASSANE) :EN "Pear")

(make-mln-from-plist '(:fr "Poire; passe-crassane" :en "Pear") #\;)
((:FR "Poire" "passe-crassane") (:EN "Pear"))
|#         
;;;----------------------------------------------------------------------- %MLN?
;;; used to check MLN with a string format

(defUn %mln? (expr)
  "Checks whether a list is a multilingual name Uses global *language-tags* variable. ~
   nil is not a valid MLN.
Argument:
   expr: something like (:en \"London\" :fr \"Londres\") or (:name :en \"London\")
Result: 
   T if OK, nil otherwise"
  (and expr
       (listp expr)
       (if (eql (car expr) :name)
           (and (cdr expr) (%mln-1? (cdr expr)))
         (%mln-1? expr))))
#|
(%mln? nil)
NIL

(%mln? '(:name))
NIL

(%MLN? "ALBERT")
NIL

(%mln? '(:en "London" :fr "Londres"))
T

(%mln? '(:name :en "London" :fr "Londres"))
T

(%mln? '(:unknown "albert es-tu là?"))
T
|#

(defUn %mln-1? (expr)
  "Checks whether a list is a multilingual string. Uses global *language-tags* ~
   variable.
Argument:
   expr: something like (:en \"London\" :fr \"Londres\")
Result: 
   T if OK, nil otherwise"
  (declare (special *language-tags*))
  (or (null expr)
      (and (listp expr)
           (member (car expr) *language-tags*)
           (stringp (cadr expr))
           (%mln-1? (cddr expr)))))
#|
(%mln-1? nil)
T

(%mln-1? '(:en "London" :fr "Londres"))
T
|#
;;;------------------------------------------------------------------- MLN-EQUAL
;;; needed when querying with MLN values
;;; Equality is as follows:
;;; - if mln1 and mln2 are both strings, use string-equal
;;; - if one is string the other MLN
;;;   - if language is a legal language, the string should be one of the synonyms
;;;     in the set of synonyms of the mln corresponding to language
;;;   - if the language is :all, the string should be one of the synonyms of
;;;     any of the languages of the mln
;;;   - if language is nil, then we consider that the language is unknown and try
;;;     all languages in turn, but send a warning
;;; - if mln1 and mln2 are both mlns, language context or option is irrelevant
;;;   - the intersection between synonyms must be non empty for some language

;;; we may want to allow list of strings as sets of synonyms

(defUn mln-equal (mln1 mln2 &key language)
  "Checks whether two multilingual-names are equal. They may also be strings. ~
   When the language key argument is not there uses global *language* variable. ~
   When *language* is unbound, tries all possible languages. The presence of ~
   language is only necessary if one of the arguments is a string.
   The function uses %string-norm before comparing strings.
Argument:
   mln1: a multilingual name or a string
   mln2: id.
   language (key): a specific language tag, default is *language* when bound
Result: 
   T if OK, nil otherwise"
  (declare (special *language*))
  
  ;;========== take care of old format
  (if (%mln? mln1) (setq mln1 (make-mln mln1)))
  (if (%mln? mln2) (setq mln2 (make-mln mln2)))
  ;;==========
  
  ;; check first validity of arguments (t required for chinese utf8)
  (unless (and (or (stringp mln1)(mln? mln1))
               (or (stringp mln2)(mln? mln2)) t)
    (error "bad arguments:~& mln1: ~S~& mln2: ~S" mln1 mln2))
  
  ;;=== if both entries are strings, language is irrelevant
  ;; careful strings could contain several synonyms in which case they are equal
  ;; if they share some synonyms (language is implicit)
  (when (and (stringp mln1)(stringp mln2))
    (setq mln1 (string-trim '(#\space) mln1)
        mln2 (string-trim '(#\space) mln2))
    ;(format t "~%;---mln1: ~S" mln1)
    ;(format t "~%;---mln2: ~S" mln2)
    (return-from mln-equal (string-equal mln1 mln2)))
  
  ;; check language variable
  (unless language
    (setq language (and (boundp '*language*) *language*)))
  ;(format t "~%; mln::mln-equal /*language*: ~S language: ~S" *language* language)
  
  (let (val1 val2)
    ;; here one of the args is at least MLN
    (cond
     ;; if mln2 is a string, swap role with mln1
     ((and (stringp mln2) (let (mln)(setq mln mln1 mln1 mln2 mln2 mln)) nil))
     
     ;;=== if one of the mln is a string, then the value of language is important
     ((stringp mln1)
      (setq val1 (string-trim '(#\space) mln1))
      (cond
       ;; if language is :all then checks if the string is one of the synonyms in
       ;; any language, careful mln1 may contain several synonyms
       ((eql language :all)
        (setq val2 (extract-all-synonyms mln2))
        )
       ;; if language is undefined, then do the same but issue a warning
       ((null language)
        (warn "language for comparing ~S and ~S is undefined." mln1 mln2)
        ;; do as in the :all case
        (setq val2 (extract-all-synonyms mln2))
        )
       ;; if language is valid, check the corresponding language
       (t
        (setq val2 (filter-language mln2 language)))
       )
      ;; do the actual test
      (member val1 val2 :test #'string-equal))
     
     ;; if we have 2 mlns, then equality is linked to the sharing of a synonym in
     ;; any of the expressed languages, regardless of the language, try all 
     ;; languages in turn
     (t
      (loop
        (unless mln1 (return))
        ;; check if mln2 contains this language tag (caar mln1)
        (when (setq val2 (filter-language mln2 (caar mln1)))
          (setq val1 (cdar mln1))
          ;(format t "~% val1: ~S val2: ~S" val1 val2)
          (if (intersection val1 val2 :test #'string-equal)
              (return-from mln-equal t)))
        (setq mln1 (cdr mln1))))
     )))

#|
(mln-equal nil nil)
Error: bad arguments:
mln1: NIL
mln2: NIL

(mln-equal nil "albert")
Error: bad arguments:
mln1: NIL
mln2: "albert"

(mln-equal "aa oui " "  AA OUI")
T

;---
(let ((*language* :fr))
  (declare (special *language*))
  (mln-equal " Albert" 
             '((:en "John") (:it "Joseph" "Pierre" "Albert" "gérard cinq"))))
NIL
(let ((*language* :fr))
  (declare (special *language*))
  (mln-equal " Albert" 
             '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq")))
NIL
;---
(let ((*language* :it))
  (declare (special *language*))
  (mln-equal " Albert" '((:en "John")
                         (:it "Joseph" "Pierre" "Albert" "gérard cinq"))))
("Albert" "gérard cinq")

(let ((*language* :it))
  (declare (special *language*))
  (mln-equal " Albert" 
             '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq")))
("Albert" "gérard cinq")

;---
(let ((*language* nil))
  (declare (special *language*))
  (mln-equal " Albert" '((:en "John")
                         (:it "Joseph" "Pierre" "Albert" "gérard cinq"))))
Warning: language for comparing " Albert" and
((:EN "John") (:IT "Joseph" "Pierre" "Albert" "gérard cinq")) is undefined.
("Albert" "gérard cinq")

(let ((*language* nil))
  (mln-equal " Albert" 
             '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq")))
Warning: language for comparing " Albert" and
((:EN "John") (:IT "Joseph" "Pierre" "Albert" "gérard cinq")) is undefined.
("Albert" "gérard cinq")

(mln-equal " Albert" '((:en "John") (:it "Joseph" "Pierre" "Albert" "gérard cinq"))
           :language :fr)
NIL

(mln-equal " Albert" '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq"))
:language :fr)
NIL

(mln-equal " Albert" '((:en "John") (:it "Joseph" "Pierre" "Albert" "gérard cinq"))
           :language :it)
("Albert" "gérard cinq")

(mln-equal " Albert" '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq")
           :language :it)
("Albert" "gérard cinq")

(mln-equal " Albert" '((:en "John") (:it "Joseph" "Pierre" "Albert" "gérard cinq"))
           :language :all)
("Albert" "gérard cinq")

(mln-equal " Albert" '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq")
           :language :all)
("Albert" "gérard cinq")

(mln-equal '((:en "Julius") (:it "Albert" "Gérard  cinq"))
           '((:en "John") (:it "Joseph" "Pierre" "Albert" "gérard  cinq")))
T

(mln-equal '(:en"Julius" :it "Albert; Gérard  cinq")
           '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq"))
T

(let ((*language* nil))
  (mln-equal '((:en "Julius") (:it "Albert" "Gérard  cinq"))
             '((:en "John") (:it "Joseph" "Pierre" "Albert" "gérard cinq"))))
T

(let ((*language* nil))
  (declare (special *language*))
  (mln-equal '(:en"Julius" :it "Albert; Gérard  cinq")
             '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq")))
T 

(mln-equal '((:en "Julius") (:it "Albert" "Gérard  cinq"))
           '((:en "John") (:it "Joseph" "Pierre" "Albert" "gérard cinq"))
           :language :all)
T

(mln-equal '(:en"Julius" :it "Albert; Gérard  cinq")
           '(:en "John" :it "Joseph;Pierre; Albert;gérard cinq")
           :language :all)
T

(mln-equal '("a" "b" "c") '("c" "b"))
Error: bad arguments:
mln1: ("a" "b" "c")
mln2: ("c" "b")
|#
;;;----------------------------------------------------------------- MLN-MEMBER

(defUn mln-member (input-string tag mln)
  "checks whether the input string is one of the synonyms of the mln in the ~
   language specified by tag. If tag is :all or unknown, then we check against ~
   any synonym in any language.
Arguments:
   input-string: input string (to be normed with %norm-string)
   tag; a legal language tag or :all or :unknown.
   mln: a mltilingual name
Return:
   non nil value if true."
  (unless (and (stringp input-string)
               (member tag (cons :all *language-tags*))
               (or (mln? mln)(%mln? mln)))
    (error "Invalid arguments:~& input-string: ~S~& tag: ~S~& mln: ~S"
      input-string tag mln))
  (let ((normed-string (make-value-string input-string)))
    (cond
     ;; if any language, check al synonyms
     ((member tag '(:all :unknown))
      (member input-string
              (mapcar #'make-value-string (extract-all-synonyms mln))
              :test #'string-equal))
     ;; otherwise check corresponding language or :unknown language in second mln
     ((member normed-string 
              (mapcar #'make-value-string (filter-language mln tag))
              :test #'string-equal)))))

#|
(mln-member "albert" :fr '((:en "John") (:fr "Joseph" "Pierre" "Albert" "gérard")))
("ALBERT" "GÉRARD")

(mln-member "albert" :fr '(:en "John" :fr "Joseph; Pierre; Albert; gérard"))
("ALBERT" "GÉRARD")

(mln-member "albert" :it '((:en "John") (:fr "Joseph" "Albert" "gérard")))
NIL
(mln-member "albert" :it '(:en "John" :fr "Joseph; Pierre; Albert; gérard"))
NIL

(mln-member "albert" :all '((:en "John") (:fr "Joseph" "Albert" "gérard")))
("ALBERT" "GÉRARD")
(mln-member "albert" :all '(:en "John" :fr "Joseph; Pierre; Albert; gérard"))
("ALBERT" "GÉRARD")

(mln-member 'albert :all '((:en "John") (:fr "Joseph" "Albert" "gérard")))
Error: Invalid arguments:
input-string: ALBERT
tag: :ALL
mln: ((:EN "John") (:FR "Joseph" "Albert" "gérard"))

(mln-member 'albert :fr '(:en "John" :fr "Joseph; Pierre; Albert; gérard"))
Error: Invalid arguments:
input-string: ALBERT
tag: :FR
mln: (:EN "John" :FR "Joseph; Pierre; Albert; gérard")

(mln-member "albert" :unknown '((:en "John") (:fr "Joseph" "Albert" "gérard")))
("ALBERT" "GÉRARD")
(mln-member "albert" :unknown '(:en "John" :fr "Joseph; Pierre; Albert; gérard"))
("ALBERT" "GÉRARD")
|#
;;;------------------------------------------------------------------- MLN-MERGE

(defUn mln-merge (&rest mln-list)
  "Takes a list of MLN and produces a single MLN as a result, merging the ~
   synonyms (using or norm-string comparison) and keeping the order of the ~
   inputs. all MLN should be in final format.
Arguments:
   mln-list: a list of multilingual names
Return:
   a single multilingual name
Error:
   in case one of the mln has a bad format."
  ;(format t "~2%---------- Entering mln-merge")
  ;; remove first nil entries
  (setq mln-list (remove nil mln-list))
  ;; if nothing left, quit
  (unless mln-list
    (return-from mln-merge nil))
  ;; if only one entry left return it
  (unless (cdr mln-list)
    (return-from mln-merge
      (mln::make-mln (car mln-list))))
  
  ;;========== compatibility with old format
  (setq mln-list (loop for mln in mln-list 
                     if (mln::%mln? mln) collect (mln::make-mln mln)
                     else collect mln))
  ;;==========
  
  (unless (every #'mln? mln-list)
    (error "all arguments to this function should be multilingual names: ~& ~
            ~S" mln-list ))
  ;; extract all language keywords
  (let (languages)
    ;; get the list of languages for each mln
    (setq languages 
          (delete-duplicates
           (loop for item in mln-list
               append (loop for (a) in item collect a))))
    
    ;(format t "~%;---languages: ~S" languages)
    ;; for each language tag fuse corresponding synonym strings
    ;; clean-values removes duplicated strings
    (loop for tag in languages
        collect (cons tag (clean-values
                           (loop for item in mln-list
                               append (cdr (assoc tag item))))))
    ))

#|
;; special cases
(mln-merge nil)
NIL

(mln-merge nil '((:fr "Albert")))
((:FR "Albert"))

(mln-merge nil '(:fr "Albert; Jean"))
((:FR "Albert" "Jean"))

(mln-merge 
 '((:fr "personne" "gus" "type") (:en "person"))
 '((:en "guy") (:it "ragazza")) 
 '((:fr "mec") (:en "bloke")))
((:IT "ragazza") (:FR "personne" "gus" "type" "mec")(:EN "person" "guy" "bloke"))

(mln-merge 
 '(:fr "personne;gus;type" :en "person")
 '((:en "guy") (:it "ragazza")) 
 '(:fr "mec" :en "bloke"))
((:IT "ragazza") (:FR "personne" "gus" "type" "mec") (:EN "person" "guy" "bloke"))

But:
(mln-merge '((:FR "banane plantin" "banane") (:EN "banana"))
           '((:EN "banana") (:FR "banane" "banane plantin")))
((:EN "banana") (:FR "banane plantin" "banane"))

(mln-merge '(:FR "banane plantin;banane" :EN "banana")
           '((:EN "banana") (:FR "banane" "banane plantin")))
((:EN "banana") (:FR "banane plantin" "banane"))

(mln-merge '(:FR "banane plantin;banane" :EN "banana")
           '(:EN "banana" :FR "banane;banane plantin"))
((:EN "banana") (:FR "banane plantin" "banane"))
|#
;;;----------------------------------------------------------------- NORMALIZE-VALUE

(defUn normalize-value (value &optional (interchar #\.))
  "normalize value for comparisons during queries.
   If value is a list, returns the list of normalized values.
   If value is an MLN, then each of the language-strings will be normalized
   Otherwise applies make-value-string.
Arguments:
   value: a number, a symbol, a string, a list of strings, or an MLN
   interchar (opt): a character that will be inserted between two words
Return:
   the initial object with normalized strings"
  (cond 
   ((mln::%mln? value) (mln::normalize-values (mln::make-mln value)
                                              interchar)) ; JPB1606
   ((mln::mln? value) (mln::normalize-values value interchar)) ; jpb 1406, JPB1606
   ((listp value) (mapcar #'(lambda (xx)(make-value-string xx interchar)) value))
   (t (make-value-string value interchar))))

#|
(normalize-value 345)
"345"

(normalize-value 'albert)
"ALBERT"

(normalize-value :none)
":NONE"

(normalize-value '(Albert 223 "la belle   vie  ..."))
("ALBERT" "223" "LA.BELLE.VIE....")

(normalize-value '(Albert 223 "la belle   vie  ...") #\space)
("ALBERT" "223" "LA BELLE VIE ...")

(normalize-value '(Albert 223 "la belle   vie  ...") #\-)
("ALBERT" "223" "LA-BELLE-VIE-...")

(normalize-value "la belle   vie  s'écoule lentement ...")
"LA.BELLE.VIE.S'ÉCOULE.LENTEMENT...."

(normalize-value "le jour  le plus long   ")
"LE.JOUR.LE.PLUS.LONG"

(normalize-value "le jour  le plus long   " #\space)
"LE JOUR LE PLUS LONG"

(normalize-value '(:en "person" :fr "personne"))
((:EN "PERSON") (:FR "PERSONNE"))

(normalize-value '(:en " Joe is   happy!; Oh yes " :fr " Albert est  content "))
((:EN "JOE.IS.HAPPY!" "OH.YES") (:FR "ALBERT.EST.CONTENT"))

(normalize-value '(:en " Joe is   happy!; Oh yes " :fr " Albert est  content ")
                 #\space)
((:EN "JOE IS HAPPY!" "OH YES") (:FR "ALBERT EST CONTENT"))
|#
;;;------------------------------------------------------------ NORMALIZE-VALUES

(defun normalize-values (mln &optional (interchar #\.) &aux result)
  "Brute force normalization of the synonyms in the mln using moss::normalize-value"
  (when (mln? mln)
    (dolist (item mln)
      (push (cons (car item) (normalize-value (cdr item) interchar)) 
            result))
    (reverse result)))
      
#|
(normalize-values '((:en "John" "de Vries")(:fr " ça va ? ")))
((:EN "JOHN" "DE.VRIES") (:FR "ÇA.VA.?"))

(normalize-values '((:en 22 ALBERT)(:fr " ça va ? ")))
NIL
;; this is not an MLN: values must be strings!

(mln? '((:en 2 3)(:fr "albert")))
NIL
|#
;;;---------------------------------------------------------------- PRINT-STRING

(defUn print-string (mln &optional (language-tag :all))
  "Prints a multilingual name nicely.
Arguments:
   mln: multilingual name
   language-tag: legal language-tag (default :all)
Result:
   a string ready to be printed (can be empty)."
  (unless
      (or (eql language-tag :all) (member language-tag *language-tags*))
    (error "illegal language tag: ~S~&Legal ones are: ~{~S~^, ~}"
      language-tag *language-tags*))
  
  ;;========== Compatibility with old format
  (if (%mln? mln) (setq mln (make-mln mln)))
  ;;==========
  
  (unless (mln? mln)
    (error "mln arg: ~S has not the proper format." mln))
  
  (case language-tag
    (:all
     (string-trim '(#\space #\-)
                  (apply #'string+
                         (mapcar #'(lambda(xx)(format nil "~A: ~{~A~^, ~} - " 
                                                (car xx)(cdr xx)))
                           mln))))
    (t
     (format nil "~{~A~^, ~}" (or (filter-language mln language-tag)'(""))))))

#|
(mln::print-string '((:EN "John") (:FR "Albert" "Jerôme" "Albert ")))
"EN: John - FR: Albert, Jerôme, Albert"
(mln::print-string '(:EN "John " :FR "Albert;Jerôme;Albert "))
"EN: John - FR: Albert, Jerôme, Albert"

(print-string '((:EN "John ") (:FR "Albert" "Jerôme" "Albert ")) :fr)
"Albert, Jerôme, Albert "
(mln::print-string '(:EN "John " :FR "Albert;Jerôme;Albert ") :fr)
"Albert, Jerôme, Albert"

(mln::print-string '((:EN "John ") (:FR "Albert" "Jerôme" "Albert ")) :en))
"John "
(mln::print-string '(:EN "John " :FR "Albert;Jerôme;Albert ") :en)
"John"

(mln::print-string '((:EN "John ") (:FR "Albert" "Jerôme" "Albert ")) :it)
""
(mln::print-string '(:EN "John " :FR "Albert;Jerôme;Albert ") :it)
""
|#
;;;------------------------------------------------------------- REMOVE-LANGUAGE

(defUn remove-language (mln language-tag)
  "Removes the entry corresponding to a language from a multilingual name.
Arguments:
   mln: multilanguage name
   language-tag: language to remove (must be legal)
Return:
   modified mln in the new MLN format."
  (unless (member language-tag *language-tags*)
    (error "illegal language tag: ~S~&Legal ones are: ~{~S~^, ~}"
      language-tag *language-tags*))
  
  ;;========== Compatibility with old format
  (if (%mln? mln) (setq mln (make-mln mln)))
  ;;==========
  
  (unless (mln? mln)
    (error "mln arg: ~S has not the proper format." mln))
  
  (alist-rem mln language-tag))

#|
(mln::remove-language 
 '((:EN "person" "guy" "bloke") (:FR "personne" "gus" "type" "mec") 
   (:IT "ragazza")) :sw)
Error: illegal language tag: :SW
Legal ones are: :CN, :EN, :ES, :FR, :IT, :JP, :PT, :PL, :BR, :UNKNOWN

(mln::remove-language 
 '(:EN "person;guy;bloke" :FR "personne;gus;type;mec" :IT "ragazza") :sw)
Error: illegal language tag: :SW
Legal ones are: :CN, :EN, :ES, :FR, :IT, :JP, :PT, :PL, :BR, :UNKNOWN

(mln::remove-language 
 '((:EN "person" "guy" "bloke") (:FR "personne" "gus" "type" "mec") 
   (:IT "ragazza")) :en)
((:FR "personne" "gus" "type" "mec") (:IT "ragazza"))

(mln::remove-language 
 '(:EN "person;guy;bloke" :FR "personne;gus;type;mec" :IT "ragazza") :en)
((:FR "personne" "gus" "type" "mec") (:IT "ragazza"))

(mln::remove-language nil :en)
NIL
|#
;;;---------------------------------------------------------------- REMOVE-VALUE

(defUn remove-value (mln value language-tag)
  "Removes a value in the list of synonyms of a particular language.
Arguments:
   mln: multilingual name
   value: coerced to a string (loose package context)
   language-tag: must be legal
Return:
   the modified mln in the a-list format."
  (unless
      (member language-tag *language-tags*)
    (error "illegal language tag: ~S~&Legal ones are: ~{~S~^, ~}"
      language-tag *language-tags*))
  
  ;;========== Compatibility with old format
  (if (%mln? mln) (setq mln (make-mln mln)))
  ;;==========
  
  (unless (mln? mln)
    (error "mln arg: ~S has not the proper format." mln))
  
  (alist-rem-val mln language-tag value))

#|
(mln::remove-value '((:EN "John") (:FR "Jerôme")) "Jerôme" :fr)
((:EN "John "))

(mln::remove-value '(:EN "John" :FR "Jerôme") "Jerôme" :fr)
((:EN "John "))

(mln::remove-value '((:EN "John") (:FR "Jerôme" "Albert")) "Albert" :fr)
((:EN "John") (:FR "Jerôme"))
(mln::remove-value '(:EN "John" :FR "Jerôme") "Albert" :fr)
((:EN "John") (:FR "Jerôme"))
|#
;;;------------------------------------------------------------------- SET-VALUE

(defUn set-value (mln language-tag syn-string)
  "Sets the language part to the specified value, erasing the old value.
Arguments:
   mln: a multilingual name
   language-tag: language tag
   syn-string: coerced to a string (loose package context)
Return:
   the modified normed MLN."
  (declare (special *language-tags*))
  (unless (member language-tag *language-tags*)
    (error "illegal language tag: ~S~&Legal ones are: ~{~S~^, ~}"
      language-tag *language-tags*))
  
  ;;========== Compatibility with old format
  (if (%mln? mln) (setq mln (make-mln mln)))
  ;;==========
  
  (unless (mln? mln)
    (error "mln arg: ~S has not the proper format." mln))
  (if syn-string 
      (alist-set mln language-tag (format nil "~A" syn-string))
    (alist-rem mln language-tag)))

#|
(mln::set-value '((:EN "person" "guy" "bloke") 
                  (:FR "personne" "gus" "type" "mec")
                  (:IT "ragazza")) :en "lad")
((:EN "lad") (:FR "personne" "gus" "type" "mec") (:IT "ragazza"))

(mln::set-value 
 '(:EN "person;guy;bloke" :FR "personne;gus; type;mec" :IT "ragazza") :en "lad")
((:EN "lad") (:FR "personne" "gus" "type" "mec") (:IT "ragazza"))
;---
(mln::set-value '((:EN "person" "guy" "bloke") 
                  (:FR "personne" "gus" "type" "mec")
                  (:IT "ragazza")) :en nil)
((:FR "personne" "gus" "type" "mec") (:IT "ragazza"))

(mln::set-value 
 '(:EN "person;guy;bloke" :FR "personne;gus; type;mec" :IT "ragazza") :en nil)
((:FR "personne" "gus" "type" "mec") (:IT "ragazza"))
;---
(mln::set-value '((:EN "person" "guy" "bloke") 
                  (:FR "personne" "gus" "type" "mec")
                  (:IT "ragazza")) :sw nil)
Error: illegal language tag: :SW
Legal ones are: :CN, :EN, :ES, :FR, :IT, :JP, :PT, :PL, :BR, :UNKNOWN

(mln::set-value 
 '(:EN "person;guy;bloke" :FR "personne;gus; type;mec" :IT "ragazza") :sw nil)
Error: illegal language tag: :SW
Legal ones are: :CN, :EN, :ES, :FR, :IT, :JP, :PT, :PL, :BR, :UNKNOWN
;---
(mln::set-value 
 '(:EN "person;guy;bloke" :FR "personne;gus; type;mec" :IT "ragazza")
 :unknown "Lola")
((:EN "person" "guy" "bloke") (:FR "personne" "gus" "type" "mec")
 (:IT "ragazza") (:UNKNOWN "Lola"))
|#
;;;------------------------------------------------------------------ SET-VALUES

(defun set-values (mln language-tag val-list)
  "Sets the language part to the specified value, erasing the old value.
Arguments:
   mln: a multilingual name
   language-tag: language tag
   syn-string: coerced to a string (loose package context)
Return:
   the modified MLN in a-list format"
  (declare (special *language-tags*))
  (unless (member language-tag *language-tags*)
    (error "illegal language tag: ~S~&Legal ones are: ~{~S~^, ~}"
      language-tag *language-tags*))
  
  ;;========== Compatibility with old format
  (if (%mln? mln) (setq mln (make-mln mln)))
  ;;==========
  
  (unless (mln? mln)
    (error "mln arg: ~S has not the proper format." mln))
  (unless (and (listp val-list)
               (every #'stringp val-list))
    (error "mln arg: ~S should be a list of strings." val-list))
  (alist-set-values mln language-tag val-list))

#|
(setq mln '((:en "wine" "claret")(:fr "vin")))
(set-values mln :en '("burgundy" "port"))
((:EN "burgundy" "port") (:FR "vin"))

(setq mln '(:en "wine;claret" :fr "vin"))
(set-values mln :en '("burgundy" "port"))
((:EN "burgundy" "port") (:FR "vin"))
|#
;;;-------------------------------------------------------------------- SUBTRACT

(defun subtract (mln1 mln2)
  "returns what's left in mln1 after removing values appearing in mln2. Result ~
   is in a-list format."
  (let (val1-list val2-list result tag)
    
    ;;========== take care of old format
    (if (%mln? mln1) (setq mln1 (make-mln mln1)))
    (if (%mln? mln2) (setq mln2 (make-mln mln2)))
    ;;==========   
    
    (dolist (item mln1)
      (setq tag (car item) val1-list (cdr item))
      (if (setq val2-list (extract mln2 :language tag))
          (push (cons tag (set-difference val1-list val2-list :test #'equal+))
                result)
        (push item result)))
    (setq result (remove-if #'null result :key #'cdr))
    (reverse result)))

#|
(setq mln1 '((:en "wine" "claret") (:fr "vin" "bordeaux")))
(setq mln2 '((:en "wine" "claret" "burgundy") (:fr "vin")))
(setq mln3 '((:fr "vin")))
(subtract mln1 mln2)
((:FR "bordeaux"))

(subtract mln1 mln3)
((:EN "wine" "claret") (:FR "bordeaux"))

(setq mln1 '(:en "wine;claret" :fr "vin;bordeaux"))
(setq mln2 '((:en "wine" "claret" "burgundy") (:fr "vin")))
(setq mln3 '(:fr "vin"))
(subtract mln1 mln2)
((:FR "bordeaux"))

(subtract mln1 mln3)
((:EN "wine" "claret") (:FR "bordeaux"))
|#
;;;------------------------------------------------------------------- TRANSLATE

(defun translate (mln &key (direction :to-list)(separator #\;))
  "translates the mln from a string format to a list format or conversely. It ~
   checks the validity of the structure (language tags, values).
Arguments:
   mln: the mln to translate
   direction (key): either :to-list (default) or :to-string
Return:
   the translated MLN. Error in case of invalid structure."
  (let ((ll mln) result)
    (if (and (listp mln)(eql (car mln) :name))
	    (setq mln (cdr mln)))
    (case direction
      (:to-list
       ;; takes the old format and produces the new new one
       (loop
         (unless ll (return-from translate (reverse result)))
         ;; check tag
         (unless (member (car ll) *language-tags*)
           (error "Illegal language tag: ~S in ~S~%Allowed ones are: ~S"
             (car ll) mln *language-tags*))
         ;; check values
         (unless (stringp (cadr ll))
           (error "Bad mln syntax for values: ~S in ~S" (cadr ll) mln))
         ;; translate
         (push (cons (car ll)(split-val (cadr ll) separator)) result)
         (setq ll (cddr ll))
         ))
      ;; else in the reverse direction
      (:to-string
       (if (%mln? ll) (return-from translate ll))
       (loop
         (unless ll (return-from translate result))
         ;; check struncture
         (unless (and (listp ll)(listp (car ll)))
           (error "Bad structure for mln: ~S" mln))
         ;; check tag
         (unless (member (caar ll) *language-tags*)
           (error "Illegal language tag: ~S in ~S~%Allowed ones are: ~S"
             (caar ll) mln *language-tags*))
         ;; check values
         (unless (every #'stringp (cdar ll))
           (error "Bad mln syntax for values: ~S in ~S" (cdar ll) mln))
         ;; translate
         (setq result 
               (append result 
                       (list (caar ll)
                             (format nil "~{~A~^; ~}" (cdar ll)))))
         (setq ll (cdr ll))
         ))
      (otherwise
       (error "Unknown format for translating mln: ~S" direction)))))

#|
? (mln::translate '(:en "Course followed usually by students") :direction :to-string)
(:EN "Course followed usually by students")

?(mln::translate '(:en "title;header" :fr "titre"))
((:EN "title" "header") (:FR "titre"))

(mln::translate '(:en "title,header" :fr "titre") :separator #\,)
((:EN "title" "header") (:FR "titre"))

(mln::translate '(:en "title;header" :fr))
Error: Bad mln syntax for values: NIL in (:EN "title;header" :FR)

(mln::translate '(:en "title;header" :sw "titre"))
Error: Illegal language tag: :SW in (:EN "title;header" :SW "titre")
Allowed ones are: (:CN :EN :ES :FR :IT :JP :PT :PL :BR :UNKNOWN)

(mln::translate '((:EN "title" "header") (:FR "titre")) :direction :to-string)
(:EN "title; header" :FR "titre")
|#
;;;========================== End MLN functions ================================

:EOF