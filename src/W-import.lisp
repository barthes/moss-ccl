;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==============================================================================
;;;14/03/07
;;;           M O S S - I M P O R T (file moss-W-import-P.lisp)
;;;
;;;==============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| This file contains all the functions to build an intitial menu for loading 
(importing) an ontology or opening an ontology database.
When the window is created, then moss::*moss-output* is set to the window
object so that all texts will be posted into the window message area.
When the window is closed, then it is set back to t, which wil print texts 
into the listener.

History
=======

2019
 0824 Creation from the ACL file
|#

#|
TODO
1. Updating MOSS so that defvirtualattribute, defvirtualrelation and defrule are
recorded on the $SYS instance associated to the $VST attribute, the value being
the type of virtual object and the list of options, e.g.
  ($VART (0 (:vatt "(:name :en \"age\" :fr \"âge\")
    (:class "person")
    (:def 
      (?* \"birth date\" ?y)
      (:fcn \"getYears\" ?y))
    (:type :non-negative-integer)    
    (:unique)
    (:doc :en \"the property age ... birthdate.\"))" )))
the value of $VST (MOSS-VIRTUAL-STORAGE) being the definition of the virtual object.
That way, when reconstructing a file in the MOSS format, we can restore the virtual
entities, so that the MOSS2OWL compiler can then use them to build the corresponding
Jena or SPARQL rules.
|#

(in-package :nodgui-user)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (import 
   '(moss::with-package moss::with-ap moss::*application-package* moss::send 
      moss::broadcast moss::*moss-output* moss::*redis-connected*
      moss:nth-insert moss:nth-move moss:nth-remove moss:nth-replace
      sol::*sol-language* sol::*language-list*
      moss::*language* moss::*language-tags*)))

;;;================================= globals ====================================

#|
(defParameter *moss-import-window* nil) 
(defparameter *default-ontology-name* "Family")
(defparameter *default-package-name* "?")
(defparameter *legal-import-formats* '("MOSS" "RAW DATA(LISP)")) ; OWL in the future?
(defparameter *current-import-format* "MOSS")
(defparameter *legal-export-formats* '("RAW DATA(LISP)" "MOSS" "OWL+JENA/SPARQL"
                                       "HTML"))
(defparameter *ontology-text-file* nil "last file loaded")
(defparameter *current-export-format* "RAW DATA(LISP")
|#

;;;============================== service functions =============================

(defun dp (text)
  (format t "~%~A~%" text)
  (finish-output))

#|
(defun string+ (&rest ll) ; redefine in :nodgui-user
  (if (equal ll '(nil)) "" (format nil "~{~A~}" (remove nil ll))))
|#

;(setq *debug-tk*  t)
;(setq *trace-tk*  nil)

;;;===============================================================================
;;;                              MOSS-IMPORT-WINDOW
;;;===============================================================================

;;;=================================== Classes ===================================
;;; we define the special toplevel class to keep information about the content of 
;;; the window, to allow passing only a reference to this object rather that to 
;;; each concerned widget... Maybe not a good idea!

(defclass import-window (toplevel)
  ((import-format :accessor import-format :initform *current-import-format*)
   (export-format :accessor export-format :initform *current-export-format*)
   (ontology-name :accessor ontology-name :initform *default-ontology-name*)
   (package-name :accessor ontology-package-name :initform *default-package-name*)
   (message-area :accessor message-area :initform nil)
   )
  )

;;;=================================== Methods ===================================

(defmethod moss::display-text ((win import-window) text &key erase &allow-other-keys)
  (let ((area (message-area win)))
    (unless area  (return-from moss::display-text))
    (if erase
        (setf (text area) text)
        (setf (text area) (concatenate 'string (text area) text)))))

;;;==================================== Window ===================================

;;;------------------------------------------------------- MOSS-MAKE-IMPORT-WINDOW
;; The maker-function, which always creates a new window.
#|
(with-nodgui () (make-moss-import-window))
|#

(eval-when (:compile-toplevel :load-toplevel :execute)
  (setq *wish-pathname* 
"/Library/Frameworks/Tk.framework/Versions/8.6/Resources/Wish.app/Contents/MacOS/Wish")
  ;; needed to access nodgui variables
  (named-readtables:in-readtable nodgui.syntax:nodgui-syntax))

(defUn make-moss-import-window ()
  "we assume that wish is set up to communicate with Ltk"
  (declare (special *ipw* moss::*legal-import-formats* moss::*current-import-format*
                    moss::*legal-export-formats* moss::*current-export-format*
                    *moss-import-window*))
  ;; therefore we do not call with-nodgui agaim
  (let*
      ((ipw (make-instance 'import-window :master *tk*))
       (*moss-import-window* ipw)
       (*moss-output* ipw)
       (ipw-f1 (make-instance 'frame :master ipw))
       (ipw-f2 (make-instance 'frame :master ipw))
       (ipw-F3 (make-instance 'frame :master ipw))
       ;; message area is used by all
       (ipw-message-area (make-instance 'label :master ipw-f3 :background "red"
                           :width 80 :text "<message-area>"))

       ;;=== ontology name
       (ipw-onto-name (make-instance 'label :master ipw-F1 :text "Ontology"))
       (ipw-onto-input (make-instance 'entry :master ipw-f1 :width 10 
                         :text *default-ontology-name*))
       (ipw-space (make-instance 'label :width 10 :master ipw-f1))
       (ipw-language-name  (make-instance 'label :master ipw-F1 :text "Language"))
       (ipw-language-input (make-instance 'combobox :master ipw-f1 :width 12
                     :values (mapcar #'car *language-list*)
                     :text (car (rassoc *sol-language* *language-list*))))
       (ipw-more-space (make-instance 'label :width 10 :master ipw-f1))
       (ipw-package-name (make-instance 'label :master ipw-f1 :text "Package"))
       (ipw-package-input (make-instance 'entry :master ipw-f1 
                            :text *default-package-name* :width 10))

       ;;=== labels for buttons
       (ipw-import-label (make-instance 'label :master ipw-f2 :text "Import Format"))
       (ipw-export-label (make-instance 'label :master ipw-f2 :text "Export Format"))
       ;;=== buttons
       ;; DB
       (ipw-open-db (make-instance 'button :master ipw-f2 :text "OPEN/DB"
                      :command (lambda () (iw-open-database-on-click
                                           ipw-onto-input ipw-message-area))))
       (ipw-save (make-instance 'button :master ipw-f2 :text "SAVE ALL/DB"
                   :command (lambda () (iw-save-all-on-click
                                        ipw-message-area ipw-onto-input))))
       (ipw-show-structure (make-instance 'button :master ipw-f2 :text "SHOW ALL DB"
                             :command (lambda () (iw-show-structure
                                                  ipw ipw-message-area))))
       (ipw-erase (make-instance 'button :master ipw-f2 :text "ERASE DB"
                             :command (lambda () (iw-erase ipw-onto-input
                                                           ipw-message-area))))
       ;; Import
       (ipw-import (make-instance 'combobox :master ipw-f2 :width 12
                     :values *legal-import-formats* 
                     :text *current-import-format*))
       (ipw-load (make-instance 'button :master ipw-f2 :text "IMPORT/LOAD"
                    :command (lambda () 
                               (iw-import-on-click ipw-import ipw-onto-input
                                                   ipw-package-input
                                                   ipw-message-area))))
       ;; export
       (ipw-export (make-instance 'combobox :master ipw-f2 :text "RAW DATA(LISP)"
                     :width 12 :values *legal-export-formats*))
       (ipw-export-button 
        (make-instance 'button :master ipw-f2 :text "EXPORT"
          :command (lambda () 
                     (iw-export-on-click ipw-export ipw-onto-input
                                         ipw-message-area))))
       ;; new window
       (ipw-open-new-window (make-instance 'button ::master ipw-f2 :text "NEW WINDOW"
                              :command (lambda () 
                                         (iw-new-window-on-click 
                                          ipw ipw-onto-input ipw-package-input
                                          ipw-message-area))))
       ;; exit
       (ipw-exit-button (make-instance 'button :master ipw-f2 :text "EXIT"
                          :command (lambda () (exit-wish))))
       
       )
    ;; savve message area
    (setf (message-area ipw) ipw-message-area)
    ;; set the window position on the screen
    (set-geometry-xy ipw 400 400)
    (wm-title ipw "MOSS 10.0 alpha")
    ;; adjust ontology and package
    (grid ipw-f1 0 0 :sticky "ew")
    (grid ipw-onto-name 0 0 :pady "10" :padx 10 :sticky "nw")
    (grid ipw-onto-input 0 1)
    (grid ipw-space 0 2)
    (grid ipw-language-name 0 3 :padx 10)
    (grid ipw-language-input 0 4)
    (grid ipw-more-space 0 5)
    (grid ipw-package-input 0 7 :columnspan 2 :sticky "e" :padx 10)
    (grid ipw-package-name 0 6)
   
   ;; adjust labels
    (grid ipw-f2 2 0)
    (grid ipw-import-label 0 2 :padx 10)
    (grid ipw-export-label 0 3 :padx 10)
    (grid ipw-open-db 1 0 :padx 10)
    (grid ipw-show-structure 1 1 :padx 20)
    (grid ipw-erase 2 1)
    (grid ipw-import 1 2 :padx 10)
    (grid ipw-export 1 3 :padx 10) 
    (grid ipw-open-new-window 1 4 :padx 10)
    (grid ipw-save 2 0 :sticky "w" :padx 10 :pady 10)
    (grid ipw-load 2 2 :padx 20 :pady 10)
    (grid ipw-export-button 2 3 :padx 10 :pady 10)
    (grid ipw-exit-button 2 4 :padx 10 :pady 10)
    ;; adjust message area
    (grid ipw-f3 3 0 :sticky "ew")
    (grid ipw-message-area 0 0 :columnspan "6" :padx 10 :pady "0 10")
    
    ;; actions
    #|
(bind ipw-import "<<ComboboxSelected>>"
          (lambda (event)
            (declare (ignore event))
            ;(setf (moss::import-format *IPW*) (text ipw-import))
            (format t "~%newsel:~A~%" (text ipw-import))
            (finish-output)
            ))
|#

    (bind ipw-language-input "<<ComboboxSelected>>"
          (lambda (event)
            (declare (ignore event))
            (setq *language* (cdr (assoc (text ipw-language-input) *language-list*
                                             :test #'string-equal)))))

    ;; when closing this window, reestablish previous one
    (on-close ipw (lambda () 
                   ;; reestablish previous output channel (<lisp listener>)
                   ;; and reset *moss-window* to indicate it is closed
                   (setq *moss-output* t
                         *moss-import-window* nil)
                   ;; close window
                   (destroy ipw)))
    ))
      
#|
(with-nodgui () (make-moss-import-window))

(ccl::process-run-function "test" (with-nodgui () (make-moss-import-window)))
creates an anonymous thread and a housekeeping thread (tk) not useful
|# 
;;;===============================================================================
;;;                      CALLBACKS & SERVICE FUNCTIONS
;;;===============================================================================

;;;------------------------------------------------------------- IW-CLEAN-MESSAGE
;;; not really necessary...

(defun iw-clean-message (msg-widget)
  (setf (text msg-widget) ""))

;;;--------------------------------------------------------------------- IW-ERASE

(defun iw-erase (onto-widget msg-widget)
  "erase the database containing the ontology named in the ontology input slot"
  (declare (special *redis-connected*))
  (let ((area (text onto-widget)))
    ;; clear message area
    (iw-clean-message msg-widget)
    ;; double check
    (unless (ask-OKcancel "Are you sure?")
      (return-from iw-erase))
      
    ;; check for opened database
    (unless *redis-connected*
      (iw-print-message msg-widget "*** Redis server not connected ***")
      (return-from iw-erase))
    ;; check for existing database
    (unless (moss::db-exists? area)
      (iw-print-message msg-widget "*** Unknown database ***")
      (return-from iw-erase))

    (if (moss::db-delete area)
        (iw-print-message msg-widget "*** Done ***")
        (iw-print-message msg-widget "*** Could not do it ***")
        )))
    
;;;----------------------------------------------------------- IW-EXPORT-ON-CLICK
;;; before producing a text file, the application should be saved to the database
;;; Text files are then produced from the database

(defun iw-export-on-click (export-format-widget onto-widget msg-widget)
  "dumps the content of the database according to the specified format. Currently ~
  dumps everything as a list pair (key . value)
  Arguments:
  onto-format: the export format for the target file
  onto-widget: the name of the ontology and the target file
  language-widget: the name of the current language 
  msg-widget: the message area
  Return:
  nothing significant
  Side Effet:
  creates a file in the /MOSS project/Applications/ folder"
  (declare (special moss::*redis-connected* moss::*application-directory*
                    moss::*ontology-text-file*))
  (let ((application-name (string-upcase (text onto-widget)))
        (export-mode (string-trim '(#\space) (text export-format-widget)))
        file-name outfile-name ontology-key infile-name)
    (dp application-name)
    (dp export-mode)
    
    ;; compute the input file (last loaded or from ontology slot)
    (setq infile-name
          (cond
           ;; if we kept a trace of the last loaded file use it
           (*ontology-text-file*)
           ;; otherwise check that ontology slot contains something
           ((not (equal (string-trim '(#\space) application-name) ""))
            ;; use that
             (string+ moss::*application-directory* "applications/"  
                       application-name  ".lisp"))
           ;; otherwise ask user
           ((ccl::choose-file-dialog))
           ;; if empty quit
           (t (return-from iw-export-on-click))))
    (dp (format nil "infile-name set to ~S" infile-name))    
    
    ;; make export-mode a key
    (setq export-mode (intern export-mode :keyword))
    ;(dp (format nil "export mode set to ~S" export-mode))
    
    (case export-mode
      (:|RAW DATA(LISP)|
        ;; exports data when the ontology is in core, when having been loaded from
        ;; a text file or activated from the database
        
        ;;=== raw format is a list of pairs <key . value>
        (iw-print-message msg-widget "saving ontology as a set of pairs of strings")
        ;; set outfile name so that the result is in the same folder as the infile
        (setq outfile-name 
              (merge-pathnames
               (string+ application-name "-DUMP-" (moss::get-current-date :compact t) 
                        ".mos")
               infile-name))
        
        ;; make key for partition
        (setq ontology-key (moss::keywordize application-name))
        ;; if file exists, complain
        (when (probe-file outfile-name)
          (iw-print-message msg-widget 
                            (format nil "dump file ~A already exists" outfile-name))
          (return-from iw-export-on-click))
        
        (unless (find-package ontology-key)
          (iw-print-message msg-widget 
                            (format nil "ontology ~A is not loaded" ontology-key))
          (hemlock-interface:beep)
           (return-from iw-export-on-click))

        ;; otherwise, call the make function in the right package
        (with-package (find-package ontology-key)
          (moss::db-export ontology-key :file-name outfile-name))
        ;; tell user we are done
        (iw-print-message msg-widget (format nil "ontology saved in ~S" outfile-name))
        )
      ;;=== ontology format, reconstruct ontology 
      (:MOSS
       ;; exports data when the ontology is in core, when having been loaded from
       ;; a text file or activated from the database
       (iw-print-message msg-widget "reconstructing the ontology in MOSS textual format")
       ;; set outfile name so that the result is in the same folder as the infile
       (setq outfile-name 
             (merge-pathnames
              (string+ application-name "-ONTOLOGY-" (moss::get-current-date :compact t)
                       ".lisp")
              infile-name))
       ;; if file exists, complain
       (when (probe-file outfile-name)
         (iw-print-message msg-widget
                           (format nil "export file (~A) already exists" outfile-name))
         (return-from iw-export-on-click))

       ;; otherwise, call the make function in the right package
       (with-package (find-package application-name)
         (moss::make-ontology-file :file-name outfile-name :ontology application-name)
         (iw-print-message msg-widget "*** Done ***")
         )
       )
      ;;=== OWM+JENA/SPARQL
      (:|OWL+JENA/SPARQL|
        ;; here we do not want to have the ontology in core but want to build the OWL
        ;; and other files from the reconstructed MOSS file that will be selected
        ;; from the sol window; however, we provide a default init file computed from
        ;; the application name
        (setq infile-name 
             (merge-pathnames
              (string+ application-name "-ONTOLOGY-" (moss::get-current-date :compact t)
                       ".lisp")
              infile-name))
        ;; check that the file exists
        (unless (probe-file infile-name)
           (iw-print-message msg-widget
                             (format nil "file (~A) must exist before translating to OWL" 
                                     infile-name))
          (hemlock-interface:beep)
          (return-from iw-export-on-click))
        (make-sol-window infile-name)
        )
      ;;=== HTML only
      (:HTML
       ;; here we want to produce an HTML file for the posted application ontology
       ;; we prepare the parameters for the SOL compiler and call it
       ;; set input file name
       (setq infile-name 
             (merge-pathnames
              (string+ application-name "-ONTOLOGY-" (moss::get-current-date :compact t) 
                       ".lisp")
              infile-name))
       ;; ask if we want this one or to select another one
       (unless (ask-okcancel (format nil "Input file is ~S?" infile-name))
         (setq infile-name (ccl::choose-file-dialog)))
       ;; if not there quit
       (unless (probe-file infile-name)
         (iw-print-message msg-widget 
                           (format nil "*** Must first build a file named ~S ***"
                                   infile-name))
         (return-from iw-export-on-click))

       (setq outfile-name (merge-pathnames ".html" infile-name))
       ;; we do not want OWM nor rules output
       (setq sol::*owl-output* nil sol::*rule-output* nil sol::*html-output* t)
       ;; open a trace window
       (make-sol-trace-window infile-name)
       ;; build the HTML file
       (sol-html::compile-sol infile-name 
                              :outfile outfile-name
                              :language moss::*language*
                              :verbose t)
       )
      ;;===
      (otherwise
       ;; should never get there since keys are taken from the choice list
       (iw-print-message msg-widget "*** Illegal export format ***")
       )
      )
    ;;=== exit
    ;(iw-print-message msg-widget (format nil "*** Done in ~S ***" file-pathname))
    ;; return file names
    file-name
    ))

;;;----------------------------------------------------------- IW-IMPORT-ON-CLICK

(defun iw-import-on-click (import-format-widget onto-widget package-widget msg-widget)
  "loads the ontology from a text file. Also used to restore the database from ~
   a DUMP file.
Arguments:
   import-format: the import format of the source file
   onto-widget: where the name of the ontology is supposed to be
   package-widget: where the name of the package is spposed to be
   msg-widget: the message area
Return:
   t
Side Effet:
   when restoring the database, the ontology is saved in the Redis server."
  (declare (special *redis-connected* moss::*application-directory*))
  (let ((application-name (string-trim '(#\space)  (string-upcase (text onto-widget))))
        (input-format (string-trim '(#\space) (text import-format-widget)))
        file-name flag)
    ;(dp (format nil "iw-import-on-click import-mode: ~S" input-format))
    ;; processing depends on the import format
    (case (moss::keywordize input-format)
      (:MOSS
       ;;=== here we import from a standard MOSS ontology file. The ontology name
       ;; should be posted in the window 
       ;(dp "Calling iw-load-on-click")
       (iw-load-on-click input-format onto-widget package-widget msg-widget)
       )

      (:|RAW DATA(LISP)|
        ;; here we restore the database from the specific DUMP file
        ;; select the dump-file
        (setq file-name
              (easygui:choose-file-dialog :directory moss::*application-directory*))
        ;; give up if no choice
        (unless file-name (return-from iw-import-on-click))
        ;; otherwise check server, if not there give up
        (unless *redis-connected*
          (iw-print-message msg-widget "*** Redis server not connected ***")
           (return-from iw-import-on-click))
        ;; check for database
        (when (moss::db-exists? application-name)
          ;; ask user if she wants to overwrite the existing database
          (setq flag (ask-OKcancel "Database exists, do you want to overwrite it?"))
          ;; if not, then quit
          (unless flag  
            (iw-print-message msg-widget "*** Nothing imported ***")
            (return-from iw-import-on-click))
          )
        ;; open the database, fill it, return instance number
        (moss::db-open application-name)
        (moss::db-delete application-name)
        (moss::db-restore file-name application-name)
        ))))

;;;------------------------------------------------------------  IW-LOAD-ON-CLICK
#|
 (setq source 
   (string+ moss::*application-directory* "sample-applications/" "FAMILY.lisp"))
|#
(defun iw-load-on-click (input-format onto-widget package-widget msg-widget)
  "launches the application by loading the corresponding file. If the file is not ~
  found in the MOSS/applications folder, asks the user for it. Opens then a MOSS ~
  window to work with the application."
  (declare (ignore input-format) (special moss::*application-directory*))
  (iw-print-message msg-widget "")
  (let ((application-name (string-upcase (text onto-widget)))
        (package-name (string-upcase (text package-widget)))
        (app-dir moss::*application-directory*)
        application-package source)

    ;;=== if there is no name for the file in the application slot, then ask user
    (when (equal "" application-name)
      ;(dp "No ontology name, here we are asking user...")
      (setq source (easygui:choose-file-dialog :directory app-dir))
      (unless source
        (return-from iw-load-on-click))
      ;; here we have the name of a file to load, but no information about the
      ;; name of the ontology or of the package
      (iw-load-on-click-load nil source nil onto-widget package-widget msg-widget)
      (return-from iw-load-on-click t))

    ;;=== here we know the name of the ontology
    (cond
     ;; otherwise, cook up source string, try applications folder
     ((probe-file (setq source (string+ app-dir "applications/"
                                        application-name ".lisp")))
      ;(dp source)
      )
     ;; if file is not found in the applications folder, try sample-applications
     ((probe-file
       (setq source (string+ app-dir "sample-applications/" application-name ".lisp")))
      ;(dp source)
      )
     ;; whenever the file is not there, complain
     (t
      (iw-print-message 
       msg-widget (string+ "*** Ontology file not found for " application-name))
      (return-from iw-load-on-click)))
    
    ;; if package-name is not specified, then use application-name
    (when (or (equal package-name "?") (equal package-name ""))
      (setf (text package-widget) application-name)
      (setq package-name application-name))
    
    ;; check if it already used in this environment; if so, it means that we were doing
    ;; something with the ontology and we do not want to reload the definition file
    (when (find-package package-name)
      (iw-print-message 
       msg-widget 
       (format nil 
               "*** Package ~S already in use, can't load text file. Click NEW WINDOW"
               package-name))
      (return-from iw-load-on-click))
    
    ;; otherwise create package
    (setq application-package (make-package package-name :use '(:moss :cl)))
    
    ;; creating a new process conflicts with posting messages in windows
    (iw-load-on-click-load application-name source application-package 
                           onto-widget package-widget msg-widget)
   #|
    ;; create new process, loading the file catching possible errors
 (ccl::process-run-function application-name
      #'iw-load-on-click-run source application-name application-package msg-widget)
|#
    ;; then, everything OK
    t))

;(trace iw-load-on-click)

;;;-------------------------------------------------------- IW-LOAD-ON-CLICK-LOAD

(defun iw-load-on-click-load (name source package onto-widget package-widget msg-widget)
  "loads the application file guarding against errors.
Arguments:
   name: application name
   source: lisp file
   package: name of application package
Return:
   t if OK, nil if error."
  (declare (special *moss-import-window* moss::*moss-version-number*
                    moss::*application-package* moss::*redis-instance*
                    *ontology-text-file*))
  ;; make sure that no database is opened, otherwise we'll get some name conflicts
  ;; when creating classes
  (setq moss::*redis-instance* nil)
  ;; record source name to be able to put output files into its folder
  (setq *ontology-text-file* source)
  ;; we could disconnect Redis to be sure (*redis-connected* <- nil)
  (let (msg errno test)
    (multiple-value-setq (test errno)
      ;(ignore-errors
       (progn
         ;(dp "...launching m-load")
         ;; first try to catch moss errors
         (setq msg
               (catch 
                :error 
                ;; if we are in the distribution version, then we cannot call the 
                ;; compiler, we can only load source from an exec
                 (moss::m-load source)
                 (dp "text file loaded")
                ))
         ;(dp "...exiting m-load")
         (when (stringp msg)
           (iw-print-message 
            msg-widget
            (format nil "~%;*** Error while processing file:~% ~A" msg))
           (return-from iw-load-on-click-load nil))
         t)
       );)

    ;; look for system error
    (unless test
      (iw-print-message 
       msg-widget        
       (format nil "~%;*** Error while processing file:~%   ~S" errno))
      (return-from iw-load-on-click-load :error))

    ;; when loading a file from choose-file, name is not initialized
    (unless name
      ;; when loading the file, defontology initializes moss::*application-package*
      ;;***** here we must check that we have ontolog-widget, package-widget and *package*
      ;; get the ontology name
      (setq name (package-name moss::*application-package*))
      ;; post it to onto-widget and package-widget
      (setf (text onto-widget) name)
      ;; set package to ontology package
      (setf (text package-widget) name)
      (setq package name)
      )

    ;; if no system error, return success
    (iw-print-message msg-widget (string+ "Ontology loaded."))

    ;; and open a new MOSS window (?)
    ;(dp "Calling make-moss-window")
    (make-moss-window 
     :master *moss-import-window*
     :application-name name
     :application-package package ; pass ontology package (?)
     :title (concatenate 'string "MOSS " moss::*moss-version-number* " - " name)
     )
    t))

;(trace iw-load-on-click-load)
;(trace moss::m-load)
;;;-------------------------------------------------------- IW-NEW-WINDOW-ON-CLICK
;;; When we have closed the processing window and would like to reopen it
;;; the idea is that we closed the window by mistake. If we were working on the
;;; application, then the package exists and we can reuse it, opening a new window
;;; in a new process

(defun iw-new-window-on-click (win onto-widget package-widget msg-widget)
  "if the application name is valid and package exists, opens a new MOSS window.
  Arguments:
  win: the import window
  onto-widget: the slot containing the ontology name
  package-widget: the slot containing the package name
  msg-widget: the area for posting messages"
  (declare (special moss::*application-directory*))
  (iw-print-message msg-widget "")
  (let ((application-name (string-upcase (text onto-widget)))
        (package-name (string-upcase (text package-widget)))
        (app-dir moss::*application-directory*)
        source)
    
    ;; if there is no name for the file in the application slot, then ask user
    (cond
     ((equal "" application-name)
      (dp "No ontology name, here we are asking user...")
      (setq source (ccl:choose-file-dialog))
      (unless source
        (return-from iw-new-window-on-click)
        )
      )
     ;; otherwise, cook up source string and test the applications folder
     ((probe-file (setq source (string+ app-dir "applications/"
                                        application-name ".lisp")))
      (dp source))
     ;; if file is not found in the applications folder, try sample-applications
     (t
      (setq source (string+ app-dir "sample-applications/" application-name ".lisp"))
      (dp source)
      ;; whenever the file is absent, complain
      (unless (probe-file source)
        (iw-print-message 
         msg-widget (string+ "*** Application file not found for " application-name))
        (return-from iw-new-window-on-click))))
    
    ;; if package-name is not specified, then use application-name
    (when (or (equal package-name "?") (equal package-name ""))
      (setf (text package-widget) application-name)
      (setq package-name application-name))
    
    ;; check if it already used in this environment; if so, it means that we were doing
    ;; something with the ontology and we do not want to reload the definition file
    (when (find-package package-name)
      (iw-print-message 
       msg-widget 
       (format nil 
               "*** Package ~S already in use, we try to reconnect"
               package-name))
      ;; problem with starting a new thread for a window, create window in current thread
      ;; and current package (nodgui-user)     
      (make-moss-window 
       :master win
       :application-name (intern application-name :keyword)
       :application-package package-name
       :title (string+ "MOSS " moss::*moss-version-number* " - " application-name)
       )
      )
    ;; OK all set
    (iw-print-message msg-widget "")
    t))

;;;---------------------------------------------------- IW-NEW-WINDOW-ON-CLICK-RUN

(defun iw-new-window-on-click-run (win application-name package-name)
  "new process creates a MOSS window and wait until gate opens.
Arguments:
  win: current window
  application-name: a string, e.g. \"FAMILY\"
  package-name: a string
Return:
  nothing special."
  ;; we assume that context, language and version-graph exist in package
  ;; since we are restarting a window
  (let ((*package* (find-package package-name))
        ;*editing-box*
        ;*editor-window*
        )
    (declare (special *package* *context* *language* *editor-window* *editing-box*))
    ;; create MOSS window (a blocking action: no need to wait)
    (make-moss-window 
     :master win
     :application-name (intern application-name :keyword)
     :application-package *package*
     :title (string+ "MOSS " moss::*moss-version-number* " - " application-name)
     ))
  :done)

;;;----------------------------------------------------- IW-OPEN-DATABASE-ON-CLICK
;;; When an old database is opened, we should first check if the corresponding
;;; package exists, create it eventually and change *package* to the new
;;; value. The name of the package has been normally saved into the file.

(defUn iw-open-database-on-click (onto-widget msg-widget)
  "loads the sequential file that contains all the objects from an application."
  (declare (special moss::*redis-connected* moss::*redis-instance*))
  (let (application-name application-key index)
    ;; clear message area
    (iw-print-message msg-widget "")
    ;; get the application name from the import window
    (setq application-name (string-upcase (text onto-widget)))
    (setq application-key (intern application-name :keyword))
    ;; is Redis connected? no, quit
    (unless moss::*redis-connected*
      (iw-print-message msg-widget "*** Redis server is not connected.")
      (return-from iw-open-database-on-click)) 
    
    ;; does application instance exist? no quit
    (unless (moss::db-exists? application-name)
      (iw-print-message 
       msg-widget 
       (format nil 
               "*** ~A databse does not exist." application-name))
      (moss::db-show-structure)
      (return-from iw-open-database-on-click)) 
    
    ;; open database
    (setq index (moss::db-open application-name))
    ;; set pointer
    (setq moss::*redis-instance* index)
    ;; initialize application
    (moss::db-init-app application-key)
    ;; open window
    (make-moss-window 
     :master nil
     :application-name application-key
     :application-package application-key
     :title (concatenate 'string "MOSS " moss::*moss-version-number* " - " 
                         application-name)
     )))

;;;------------------------------------------------------------- IW-PRINT-MESSAGE

(defun iw-print-message (msg-widget text)
  "displays a message in the bottom message panel"
  (setf (text msg-widget) text))

;;;--------------------------------------------------------- IW-SAVE-ALL-ON-CLICK
;;; the semantics is the following: we just loaded or defined the ontology and
;;; we want to bulk-save it onto disk. The name of the database will probably 
;;; be the same as the name of the text file. In that case we do not modify the
;;; content of the application slot.
;;; We want a clean new file, this it should not exist already. Otherwise, it
;;; it should be removed manually.
;;; Assumptions: moss::*application-package* contains the package of the ontology

(defUn iw-save-all-on-click (msg-widget onto-widget)
  "saves all entities into the database." 
  ;; clear message area
  (iw-print-message msg-widget " ")
  (iw-print-message msg-widget "*** creating the database ***")
  (if (moss::db-save-all (text onto-widget))
      (iw-print-message 
       msg-widget (format nil "*** Data saved. Current package is ~S" 
                          (package-name *package*))))
  t)

;;;----------------------------------------------------------- IW-SHOW-STRIUCTURE

(defun iw-show-structure (ipw msg-widget)
  "displays the databases on the server"
  (declare (special *redis-connected*))
  (unless *redis-connected*
    (iw-print-message msg-widget "*** Redis server not connected ***")
    (return-from iw-show-structure))
  (let ((moss::*moss-output* ipw)
        (moss::*moss-window* ipw))
    (declare (special moss::*moss-output*))
    (moss::db-show-structure)))

:EOF