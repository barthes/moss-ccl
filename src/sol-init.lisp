;;;-*- Mode: Lisp; Package: "COMMON-LISP-USER" -*-
;;;============================================================================
;;;19/11/23		
;;;		S O L - I N I T - (File sol-init.lisp)
;;;	
;;;============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
2019
 1123 copying file from the SOL 5.6 folder
|#

(in-package "COMMON-LISP-USER")

;;;----------------------------- Globals --------------------------------------
;;; globals wer relacated in the globals.lisp file


;;;------------------------------------------------------ compile-and-load-file
;;; useless since CCL compiles files when loading them

(defun compile-and-load-file (file-name &optional (load-pathname *sol-directory*))
  "compiles and load a file given its name.
Arguments:
   file-name: string naming the file
   load-pathname (opt): load-pathname (default is *load-pathname*)
Return:
   t"
  (let ((file-pathname 
         (make-pathname
          :device (pathname-device load-pathname)
          :directory (pathname-directory load-pathname)
          :name file-name
          :type "lisp"))
        (compiled-file-pathname
         (make-pathname
          :device (pathname-device load-pathname)
          :directory (pathname-directory load-pathname)
          :name file-name
          :type #+clisp "fas" #+MCL "cfsl" #+microsoft-32 "fasl")))
    ;; compile file
    (compile-file file-pathname :verbose t)
    (load compiled-file-pathname)))

;;;----------------------------------------------------------- compile-sol-file
;;; unused

(defun compile-sol-file (file-name &optional (load-pathname *sol-directory*))
  "compiles file given its name. Useful for the Eclipse plug in.
Arguments:
   file-name: string naming the file
   load-pathname (opt): load-pathname (default is *load-pathname*)
Return:
   t"
  (let ((file-pathname 
         (make-pathname
          :device (pathname-device load-pathname)
          :directory (pathname-directory load-pathname)
          :name file-name
          :type "lisp")))
    (print file-pathname)
    ;; compile file
    (print (compile-file file-pathname :verbose t))
    ))

#|
(compile-sol-file "sol-init")
|#
;;;-------------------------------------------------------------- get-file-spec

(defun get-file-spec (data)
  "extract file-name and type from a string.
Arguments:
  data: string describing the file
Return:
   a list (file-name file-type) or (file-name)"
  (let ((pos (position '#\. data)))
    (if pos
      (list (subseq data 0 pos)(subseq data (1+ pos)))
      (list data))))

#|
? (get-file-spec "test")
("test")
? (get-file-spec "test.sol")
("test" "sol")
|#
;;;---------------------------------------------------------- process-parameter

(defun process-parameter (line)
  "processes a single line corresponding to a SOL compiler parameter"
  (let ((text (string-trim '(#\space #\linefeed) line))
	parm pos data var)
    ;; whenever the line is empty, return
    (if (equal text "")(return-from process-parameter nil))
    ;; when the line starts with a semi column return
    (if (equal #\; (char text 0)) (return-from process-parameter nil))
    ;; otherwise get first word
    (setq pos (position '#\space text))
    (unless pos (return-from process-parameter nil))
    ;; otherwise process each parameter
    ;; get equal sign
    (setq pos (position '#\= text)
	  parm (string-trim '(#\space)(subseq text 0 pos))
	  data (string-trim '(#\space)(subseq text (1+ pos)))
	  var (cdr (assoc parm *initial-parameters* :test #'equal)))
    ;(format t "~&=====> Parameter: ~S" text)
    (unless var 
      (error "unknown initial parameter ~S in file SOL.PROPERTIES." parm))
    (case var
      (*ontology-title*
       (setq *ontology-title* data)
       ;(format t "~&=====> Ontology title: ~S" *ontology-title*)
       )
      (*language-list*
       (setq *language-list* 
	     (read-from-string 
	      (concatenate 'string "(" data ")")))
       ;; turn a-list into a list of dotted pairs
       (setq *language-list* (mapcar #'(lambda (xx) (cons (car xx)(cadr xx))) 
                                     *language-list*)))
      (*sol-file*
       (setq *sol-file* 
             (let ((file-spec (get-file-spec data)))
               (namestring 
                (make-pathname
                 :device (pathname-device *load-pathname*)
                 :directory (append (butlast 
                                     (pathname-directory *load-pathname*)) 
                                    '("ontologies"))
                 :name (car file-spec)
                 :type (or (cadr file-spec) "sol")
                 )))))
      (*encoding*) ; ignored
      (t
       (error "unknown initial parameter ~S in file SOL.PROPERTIES." parm)))
    ))

;;;------------------------------------------------------------ read-properties

(defun read-properties (filename)
  "read the user specified parameters"
  (let (line)
    (with-open-file 
      (ss filename :direction :input
          ;; tell clisp that file format is UTF-8
          #+clisp :external-format 
          #+clisp (ext:make-encoding :charset charset:utf-8 :line-terminator :dos)
          )
      (loop
        (setq line (read-line ss nil nil))
        (unless line (return))
        (print line)
        (process-parameter line)))
    t))

#|
(read-properties (namestring
                  (make-pathname
                   :device "Odin"
                   :directory '(:absolute "Users" "barthes" "MCL" 
                                "SOL" "ontologies")
                   :name "sol"
                   :type "properties")))
(read-properties "Odin:/Utilisateurs/barthes/MCL/SOL/ontologies/sol.properties")
|#
;;;------------------------------------------------------------------- sol-make

#+(or CLISP MICROSOFT-32)
(defMacro sol-copy-file (filename &key (ext "lisp"))
  `(sys:copy-file
    (make-pathname
     :device (pathname-device *sol-directory*)
     :directory (pathname-directory *sol-directory*) 
     :name ,filename
     :type ,ext)
    (make-pathname
     :device (pathname-device *sol-directory*)
     :directory (list :absolute (format nil "sol-tools v~A" version))
     :name ,filename
     :type ,ext)))

(defun sol-make (&key (version *sol-version*))
    "creates a folder containing a running version of the SOL suite.
Arguments:
   version (key): global number of the SOL suite)
Return:
   :done"
  (let ((to-folder (format nil "sol-tools v~A" version)))
    ;; create a folder named "sol-tools vxx"
    #+(OR Clisp MICROSOFT-32) 
    (make-directory (make-pathname
		   :device (pathname-device *sol-directory*)
		   :directory (list :absolute (format nil "sol-tools v~A" version))))

    #+MCL (create-directory (make-pathname
                             ;:device (pathname-device *sol-directory*)
                             :directory (append (pathname-directory  *sol-directory*)
                                                (list to-folder))))
    ;; compile the init file 
    #+CLISP (compile-sol-file "sol-init")
    ;; copy the needed files
    (sol-copy-file "sol2owl")
    (sol-copy-file "sol2html")
    (sol-copy-file "sol2rules")
    (sol-copy-file "sol-init")
    #+CLISP (sol-copy-file "sol-init" :ext "fas")
    (if *tk* (sol-copy-file "sol-control"))
    #+MCL (sol-copy-file "sol-control-MCL")
    (sol-copy-file "README" :ext "txt")
    (sol-copy-file "History" :ext "txt")
    (sol-copy-file "Encoding" :ext "txt")
    (sol-copy-file "sol" :ext "properties")
    ;; return :done
    :done))

; (sol-make :version 999)
;;;------------------------------ batch mode ----------------------------------

;;; When calling SOL from a Java window, then we use Clisp and a command line
;;; we define a Clisp sol function

#+CLISP
(defun sol ()
  "get the parameters from the command line (ext:*args*) and call the compiler.
   First arg is SOL file, the rest are keywords enclosed in strings."
  ;; example: ("sol-tools/test.list" ":owl" "t" ":html "nil")
  (let ((file-name (car ext:*args*))
        (arg-list (mapcar #'read-from-string (cdr ext:*args*)))
        )
    ;; set up global parameters
    (setq *sol-file* file-name
          *OWL-output* (cadr (member :owl arg-list))
          *html-output* (cadr (member :html arg-list))
          *rule-output* (cadr (member :rules arg-list))
          *language* (or (cadr (member :language arg-list)) :EN)
          *verbose-compile* t
          )
    ;;=== debug: print parameters into a test file
    (with-open-file (ss "sol-tools/control.txt" :if-exists :supersede
                        :if-does-not-exist :create :direction :output)
      (format ss "Command line args: ~&   ~s~&Arg-list: ~s" ext:*args* arg-list)
      (format ss "~%File: ~S~
                  ~%verbose: ~S; OWL output: ~S; TEXT output: ~S; language: ~S; ~
                  RULE output: ~S" 
              file-name *verbose-compile* *OWL-output* *HTML-output* *language*
              *rule-output*)
      )
    ;;=== end debug
    
    (when file-name
      ;; compile-sol returns nil on error
      (in-package :sol-owl)
      (unless (sol-owl::compile-sol file-name 
                                    :rule-output *rule-output*
                                    :verbose *verbose-compile* 
                                    :errors-only (not *OWL-output*))
        ;(format t "*** Error while compiling to OWL ***")
        (return-from sol nil))
      (in-package :cl-user)
      
      ;; when OK, check if html or text output is required
      (when *html-output*
        (in-package :sol-html)
        ;; produce HTML and TEXT files, no trace
        (sol-html::compile-sol file-name
                               :language *language*
                               :verbose nil)
        (in-package :cl-user)
        )
      ;(format t "~%*** End of processing ***")
      )
    t))


;;;----------------------------- initial loads --------------------------------

(eval-when (:load-toplevel :execute)
  (print *load-pathname*)
  ;; the properties file must be in the directory one level up
  (let ((file (namestring 
               (make-pathname
                :device (pathname-device *sol-directory*)
                :directory (pathname-directory *sol-directory*)
                :name "sol"
                :type "properties"))))
    (print file)
    (read-properties file)
    ;; when using the java interface, include following line to remove Tkl window
    ;; (sol-control file)
    #-CLISP
    (compile-and-load-file #-MCL "sol-control" #+MCL "sol-control-MCL")
    (compile-and-load-file "sol2owl")
    (compile-and-load-file "sol2html")
    (compile-and-load-file "sol2rules")
    ;; launch the SOL process
    #+CLISP (sol) 
    #+MCL (eval-enqueue (sol))
    ))

:EOF