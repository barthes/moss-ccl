;;;-*- Mode: Lisp; Package: "NODGUI-USER" -*-
;;;===============================================================================
;;;19/10/08		
;;;		       B R O W S E R (File W-browser.Lisp)
;;;	
;;;===============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| This file contains all the functions to build the MOSS interface window without
using the project mechanism.

History
2019
 0825 File created from the ACL version
|#
#|
TODO
 - tile the various windows: add hpos, vpos to window structure
 - hide wish window
|#
#|
2019
 09/17 Creation by reusing the ACL/MCL file
|#

(in-package :nodgui-user)

#|
(eval-when (:compile-toplevel :load-toplevel :execute)
  (import 
   '(moss:with-package moss:with-ap moss::*application-package* moss::send)))
|#

;;;============================ MOSS function ===================================

(defUn browse (obj-id)
  (moss::with-package (moss::<<symbol-package obj-id)
    (print `("MW-BROWSE ===> *package*" ,*package*))
    (make-browser-window obj-id)))

;;;============================ Service function ================================

(defmethod treeview-select ((tv treeview))
  "Get the selected items (a list of tree-item) of this treeview"
  (format-wish "senddatastrings [~a selection]" (widget-path tv))
  (read-data))

;;;---------------------------------------------------------- FILL-ATTRIBUTES

(defun bw-fill-attributes (current-entity vtree)
  "function that fills the left treeview column with the name of the attibute ~
  and the first column with the associated values using the =format-value method."
  (let* (prop-list value-list)
    ;; first clean the area if anything there
    (treeview-delete-all vtree)
    ;; get the list of attributes from the class (ordered as they are in the class)
    ;; must use apply otherwise 'moss::get-property is not in the right package for
    ;; some reason
    (setq prop-list (apply #'moss::send current-entity 'moss::=get-attributes nil))
    ;; for each property:
    (dolist (prop prop-list)
      ;; for each attribute, get the list of attached values using =format-value
      (setq value-list  
            (mapcar #'(lambda (xx) (moss::send prop 'moss::=format-value xx))
                    (moss::send current-entity 'moss::=get-id prop)))
      (when value-list
        ;; add the prop name to the tree
        (format-wish "~A insert {} end -id ~A -text {~A}" (widget-path vtree)
                     prop (car (moss::send prop 'moss::=instance-name)))
        ;; post the result into the value column of the tree
        (format-wish "~A set ~A value {~A}" (widget-path vtree) prop
                     (format nil "~{~A~^, ~}" value-list)))
      )))

;;;------------------------------------------------------------ FILL-INVERSES

(defun bw-fill-inverses (current-entity vtree)
  "function that fills the left treeview column with the name of the inverse relation ~
  and the first column with the associated values using the =format-value method."
  (let* (prop-list value-list inv)
    ;; first clean the area if anything there
    (treeview-delete-all vtree)
    ;; get the list of attributes from the class (ordered as they are in the class)
    ;; must use apply otherwise 'moss::get-property is not in the right package for
    ;; some reason
    (setq prop-list (moss::send current-entity 'moss::=get-inverse-properties))
    ;; for each property:
    (dolist (prop prop-list)
      (when (moss::%is-relation? prop)
        ;; computer inverse property
        (setq inv (moss::%inverse-property-id prop))
        ;; for each attribute, get the list of attached values using =summary
        (setq value-list 
              (format nil "~{~{~A~}~^, ~}"
                      (mapcar #'(lambda (xx) (moss::send xx 'moss::=summary))
                              (moss::send current-entity 'moss::=get-id inv))))
        (unless (equal value-list "")
          ;; add the prop name to the tree
          (format-wish "~A insert {} end -id ~A -text {~A OF}" (widget-path vtree)
                       prop (car (moss::send prop 'moss::=instance-name)))
          ;; post the result into the value column of the tree        
          (format-wish "~A set ~A value {~A}" (widget-path vtree) prop value-list)
          )))))

;;;----------------------------------------------------------- FILL-RELATIONS

(defun bw-fill-relations (current-entity vtree bw)
  "function that fills the left treeview column with the name of the relation and ~
  the first column with the associated values using the =format-value method."
  (let* (prop-list value-list)
    (with-ap current-entity
      (print `("bw-fill-relations *package*:" ,*package*))
      ;; first clean the area if anything there
      (treeview-delete-all vtree)
      ;; get the list of attributes from the class (ordered as they are in the class)
      ;; must use apply otherwise 'moss::get-property is not in the right package for
      ;; some reason
      (setq prop-list (apply #'moss::send current-entity 'moss::=get-properties nil))
      ;; save the list
      (setf (rel-list bw) prop-list)
      ;(print `("bw-fill-relations prop-list:" ,(rel-list bw)))
      ;; for each property:
      (dolist (prop prop-list)
        (when (moss::%is-relation? prop)
          ;; for each attribute, get the list of attached values using =summary
          (setq value-list 
                (format nil "~{~{~A~}~^, ~}"
                        (mapcar #'(lambda (xx) (moss::send xx 'moss::=summary))
                                (moss::send current-entity 'moss::=get-id prop))))
          ;(print `(value-list ,value-list))
          (unless (equal value-list "")
            ;; add the prop name to the tree
            (format-wish "~A insert {} end -id ~A -text {~A}" (widget-path vtree)
                         prop (car (moss::send prop 'moss::=instance-name)))
            ;(print (car (moss::send prop 'moss::=instance-name)))
            ;; post the result into the value column of the tree        
            (format-wish "~A set ~A value {~A}" (widget-path vtree) prop value-list)
            ))))))

;;;=============================== Globals =======================================

;; browser position on the screen
(defParameter *browser-left* 555)
(defParameter *browser-top* 250)
;(defParameter *browser-height* 530)
(defParameter *browser-width* 520)
;; height is computed by the system
(defparameter *list-area-height* 5 "number of rows in each pane")
;; width of the list areas
(defparameter *list-area-width* 440)
;; ad hoc scaling because can't get the width of the list area from Tk!
(defparameter *detail-area-width* (floor (/ (* *list-area-width* 45) 440)))

;;;============================= Classes =========================================

(defClass browser (toplevel) 
  ((current-entity :accessor current-entity)
   (calling-window :accessor calling-window :initform nil)
   ;; used by update method to refresh the browser window
   (att-pane :accessor att-pane :initform nil)
   (rel-pane :accessor rel-pane :initform nil)
   (inv-pane :accessor inv-pane :initform nil)
   (rel-list :accessor rel-list :initform nil)
   (inv-list :accessor inv-list :initform nil)
   ;; used when retuning from editor to set output channel nil = pop up
   (moss-output :accessor moss-output :initform nil))
  (:documentation 
   "Any object can be viewed. Its attributes, relations and inverse links ~
are shown. 
Objects connected to the displayed object can be shown by ~
selecting the proper row. When a property is multi-valued, then ~
a list appears. One then can select an element to browse it in turn."))

;;;============================= Methods =========================================

(defmethod update ((bw browser) current-entity)
  "called by editor on-close function whe returning from editing to update the ~
   contents of the browser window."
  ;; what we do is to clean the list and rebuild them
  ;; we need to recover the pane references to do that
  (bw-fill-attributes current-entity (att-pane bw))
  (bw-fill-relations current-entity (rel-pane bw) bw)
  (bw-fill-inverses current-entity (inv-pane bw))
  )
  
;;;------------------------------------------------------ MAKE-BROWSER-WINDOW

(defun make-browser-window (current-entity)
  "build the various areas to display object info.
  Arguments:
  current-entity: id ob object to be displayed
  Return:
  does not return (blocking mode)."
  ;(print `(make-browser-window ===> *package* ,*package*))
  (let*
      ((bw (make-instance 'browser :width *browser-width* :title "MOSS Browser"))
       ;;=== top area
       (top (make-instance 'frame :master bw :width *browser-width* :padding "5"))
       ;; edit button
       (edit (make-instance 'button :master top :text "Edit" :width 10
               :command (lambda () (bw-edit-on-click bw current-entity))))
       ;; context area
       (context-name  (make-instance 'label :master top :text "Context"))
       (context (make-instance 'entry :master top :text "0" :width 2))
       ;;=== Object description, e.g. Claire Labrousse
       (title (make-instance 'frame :width *browser-width* :master bw))
       ;;== attributes
       (att (make-instance 'frame :master bw))
       (att-title (make-instance 'label :master att :text "Attributes"))
       (att-list (make-instance 'frame :master att :width *list-area-width*))
       (att-vsb (make-instance 'scrollbar :orientation "vertical" :master att-list))
       (att-tree (make-instance 'treeview :height *list-area-height* :columns "value" 
                   :master att-list))
       ;;===relations
       (rel (make-instance 'frame :master bw))
       (rel-title (make-instance 'label :master rel :text "Relations"))
       (rel-list (make-instance 'frame :master rel :width *list-area-width*))
       (rel-vsb (make-instance 'scrollbar :orientation "vertical" :master rel-list))
       (rel-tree (make-instance 'treeview :height *list-area-height* :columns "value" 
                   :master rel-list))
       ;;===inverse links
       (inv (make-instance 'frame :master bw))
       (inv-title (make-instance 'label :master inv :text "Inverse Link"))
       (inv-list (make-instance 'frame :master inv :width *list-area-width*))
       (inv-vsb (make-instance 'scrollbar :orientation "vertical" :master inv-list))
       (inv-tree (make-instance 'treeview :height *list-area-height* :columns "value" 
                   :master inv-list))
       )

    (setf (calling-window bw) *moss-output*
          *moss-output* bw)
    
    (set-geometry-xy bw *browser-left* *browser-top*) 
    
    (grid top 0 0 :sticky "ew")
    (pack edit :side :left :fill :both)
    (pack context :side :right :fill :both)
    (pack context-name :side :right :fill :both)
    
    ;;=== Posting object name
    ;; create a variant of the label style
    (format-wish 
     "ttk::style configure Center.TLabel -font {Helvetica 16} -justify center -anchor center
    ttk::style layout Center.TLabel {Label.label -sticky ew}")
    ;; title text 
    (grid title 1 0 :sticky "ew")
    
    (format-wish
     "pack [ttk::label ~A.classname -text ~S -width 50 -style Center.TLabel] -pady 5 -fill both -expand 1"
     (widget-path title) 
     (car (moss::send-no-trace current-entity 'moss::=summary)))
    
    ;;=== ATTRIBUTES
    (grid att 3 0 :sticky "ew")
    (grid att-title 0 0 :sticky "w")
    (grid-configure  att-title :padx "10")
    (configure att-vsb :command (format nil "~A yview" (widget-path att-tree)))
    (configure att-tree :yscrollcommand  (format nil "~A set"  (widget-path att-vsb)))
    (grid-configure att-list :padx "40")
    (grid-configure att-list :pady "5")
    (grid att-list 1 0)
    (grid att-tree 0 0)
    (grid att-vsb 0 1 :sticky "ns")
    ;(format-wish "~A insert {} end -text ~A" (widget-path att-tree) "NAME")
    ;(treeview-insert-item att-tree :item "NAME")
    (format-wish "~A heading #0 -text {Attribute Name}" (widget-path att-tree))
    (format-wish "~A heading value -text {~A}"  (widget-path att-tree) "Value")
    (bw-fill-attributes current-entity att-tree)
    ;; show details of the attributes (goal is to edit them)
    
    ;;=== RELATIONS
    (grid rel 5 0 :sticky "ew")
    (grid rel-title 0 0 :sticky "w")
    (grid-configure  rel-title :padx "10")
    (configure rel-vsb :command (format nil "~A yview" (widget-path rel-tree)))
    (configure rel-tree :yscrollcommand  (format nil "~A set"  (widget-path rel-vsb)))
    (grid-configure rel-list :padx "40")
    (grid-configure rel-list :pady "5")
    (grid rel-list 1 0)
    (grid rel-tree 0 0)
    (grid rel-vsb 0 1 :sticky "ns")
    (format-wish "~A heading #0 -text {Relation Name}" (widget-path rel-tree))
    (format-wish "~A heading value -text {~A}"  (widget-path rel-tree) "Value")
    (bw-fill-relations current-entity rel-tree bw)
    ;; show detaisl of the successor list
    (bind rel-tree "<<TreeviewSelect>>" 
          (lambda (xx) 
            (declare (ignore xx))
            (with-ap current-entity
              (let* ((suc-ref (car (treeview-select rel-tree)))
                     (prop-id (read-from-string suc-ref nil nil)))
                (when prop-id
                  (print `("calling details prop-id" ,prop-id))
                  (bw-show-rel-details-on-click bw rel-tree prop-id))))))
    ;(FORMAT-WISH "senddatastring [~A cget -width ]" (widget-path rel)) 
    ;(setq ww (read-data)) ; does not return the width value
    ; (print `(width ,ww))
    ;;=== INVERSE LINKS
    (grid inv 7 0 :sticky "ew")
    (grid inv-title 0 0 :sticky "w")
    (grid-configure  inv-title :padx "10")
    (configure inv-vsb :command (format nil "~A yview" (widget-path inv-tree)))
    (configure inv-tree :yscrollcommand  (format nil "~A set"  (widget-path inv-vsb)))
    (grid-configure inv-list :padx "40")
    (grid-configure inv-list :pady "5 10")
    (grid inv-list 1 0)
    (grid inv-tree 0 0)
    (grid inv-vsb 0 1 :sticky "ns")
    (format-wish "~A heading #0 -text {Inverse Link Name}" (widget-path inv-tree))
    (format-wish "~A heading value -text {~A}"  (widget-path inv-tree) "Value")
    (bw-fill-inverses current-entity inv-tree)
    ;; show detaisl of the successor list
    (bind inv-tree "<<TreeviewSelect>>" 
          (lambda (xx) 
            (declare (ignore xx))
            (with-ap current-entity
              (let* ((pred-ref (car (treeview-select inv-tree)))
                     (prop-id (read-from-string pred-ref nil nil)))
                ;(inv-id (moss::send prop-id 'moss::inverse-id)))
                (when prop-id
                  ;(print `(calling details ,inv-tree))
                  (bw-show-inv-details-on-click bw inv-tree prop-id))))))
    ;; save the displayed entity
    (setf (current-entity bw) current-entity)
    ;; save pane references for updating window when returning from editing
    (setf (att-pane bw) att-tree)
    (setf (rel-pane bw) rel-tree)
    (setf (inv-pane bw) inv-tree)
    ;; when closing this window, reestablish previous one
    (on-close bw (lambda () 
                   ;; reestablish previous output channel
                   ;; and reset *moss-window* to indicate it is closed
                   (setq *moss-output* (calling-window bw))
                   ;; close window
                   (destroy bw)))
    ))

#|
(moss::with-package :family
  ;(setq *browser-left* 555 *browser-top* 250 *browser-width* 520 *context* 0
  ;      *version-graph* '((0)) *list-area-height* 5)
 (catch :error
  (with-nodgui (:debug 3) 
    (moss::with-package :family
     (make-browser-window f::_pxb)))))
|#
;;;============================= Actions ====================================

;;;--------------------------------------------------------- BW-EDIT-ON-CLICK

(defUn bw-edit-on-click (bw current-entity)
  "calls the editor to edit the displayed object"
  ;; we could save position of the window in bw to tile the next one
  ;; should we spawn a new thread ??
  (moss::with-package moss::*application-package*
    (make-editor-window current-entity bw)))

;;;--------------------------------------------- BW-SHOW-INV-DETAILS-ON-CLICK

(defUn bw-show-inv-details-on-click (win tree-list rel-id)
  "Called when a line of the structural property display area has been clicked upon. 
  We display the details of the successors one per line.
Arguments:
  win: the browser window displayin the current-entity
  tree-list: pointer to the pane displaying the list of relations
  current-entity: object being displayed in tree-list window
  rel-id: id of the property we want to detail"
  (moss::with-package moss::*application-package*
    (let* 
        ((current-entity (current-entity win))
         (inv-id (moss::send rel-id 'moss::=inverse-id))
         (inv-list (moss::send current-entity 'moss::=get-id inv-id))
         (details (make-instance 'frame :master tree-list))
         (details-list (make-instance 'listbox :master details 
                         :width *detail-area-width* :height 5))
         (details-button (make-instance 'button :master details :text "exit"
                           :command (lambda () (destroy details))))
         inv-list-details)
      (grid details 0 0 :sticky "ew")
      (grid details-button 0 0 :sticky "w")
      (grid details-list 1 0)
      ;; when selecting a value in the list opens a browser window 
      (bind details-list "<ButtonPress-1>"  
            (lambda (xx) 
              (declare (ignore xx))
              (on-selection details-list current-entity inv-id)))
      ;; get the list of strings summarizing each successor
      ;(setq inv-list-details 
      ;      (mapcar #'car (moss::broadcast inv-list 'moss::=summary)))
      ;; if an entity has not a =summary method, use a compact id
      (setq inv-list-details
            (mapcar #'(lambda (xx) 
                        (let ((res (car (moss::send xx 'moss::=summary))))
                          (if (moss::%%is-id? res)
                              (moss::>>f res)
                              res)))
                    inv-list))
      (listbox-append details-list inv-list-details)
      )))

;;;---------------------------------------------- BW-SHOW-SP-DETAILS-ON-CLICK
;;; this callback has been defined at compiled time, hence it executes in the cl-user
;;; language

(defUn bw-show-rel-details-on-click (win tree-list rel-id)
  "Called when a line of the structural property display area has been clicked upon.
   We display the details of the successors one per line.
Arguments:
   win: the browser window displayin the current-entity
   tree-list: pointer to the pane displaying the list of relations
   rel-id: id of the property we want to detail"
  (moss::with-package moss::*application-package*
    ;(print `("bw-show-rel-details-on-click *package*:" ,*package*))
    (let* 
        ((current-entity (current-entity win))
         ;; get the list of successors
         (suc-list (moss::send current-entity 'moss::=get-id rel-id))
         ;; now bild the details frame
         (details (make-instance 'frame :master tree-list))
         (details-button (make-instance 'button :master details :text "exit"
                           :command (lambda () (destroy details))))
         (details-list (make-instance 'listbox :master details 
                         :width *detail-area-width* :height 5))
         suc-list-details)
      (grid details 0 0 :sticky "ew")
      (grid details-button 0 0 :sticky "w")
      (grid details-list 1 0)
      ;(grid details-vsb 1 1 :sticky "ns")
      ;(configure details-vsb :command  (format nil "~A yview" (widget-path details-list)))
      ;(configure details-list :yscrollcommand  (format nil "~A set"  (widget-path details-vsb)))
      ;; when selecting a value in the list opens a browser window 
      (bind details-list "<ButtonPress-1>"  
            (lambda (xx) 
              (declare (ignore xx))
              (on-selection details-list current-entity rel-id)))
      ;; get the list of strings summarizing each successor
      (setq suc-list-details 
            (mapcar #'car (moss::broadcast suc-list 'moss::=summary)))
       ;(print `("bw-show-rel-details-on-click suc-list-details:" ,suc-list-details))
      ;; display the succcessors in the details frame
      (listbox-append details-list suc-list-details)
      )))

;;; on-selection is a callback defined at compiled time, hence will run in the cl-user
;;; package, which is not important if nothing is selected; must run in the application
;;; package though to post a new browser window for the successor

(defun on-selection (list-details current-entity rel-id)
  (declare (special moss::*application-package*))
  (let ((nn (car (listbox-get-selection-index list-details))))
    (unless nn
      (ask-okcancel "Something wrong with the selection process")
      (return-from on-selection))
    ;; e need to execute the method in the application package, otherwise variables like
    ;; *context* or *version-graph* are not initialized properly
    (moss::with-package moss::*application-package*
      ;; make a new browser window on the selected successor
      (make-browser-window
       (nth nn (moss::send current-entity 'moss::=get-id rel-id))))))

#|
(catch :error
  (with-nodgui () 
     (moss::with-package moss::*application-package*
        (make-browser-window f::_jpb))))
|#
;;;---------------------------------------------- BW-SHOW-TP-DETAILS-ON-CLICK

#|
(defUn bw-show-tp-details-on-click (current-page item)
  "Called when a line of the terminal property display area has been clicked upon. 
   We display the details in the detail window one per line."
  (let* ((current-entity (current-entity current-page))
         tp-id display-list value-list)
    ;; get first selected property
    (setq tp-id 
          (BWF-get-selected-prop-id current-page item current-entity :tp))
    (bwformat "~&;bw-show-tp-details-on-click; tp-id: ~S" tp-id)

    ;; if nothing is selected or we click outside value range, shring show area
    (unless tp-id
      (bwf-disable-view-area (%find-named-item :show-area current-page))
      (return-from bw-show-tp-details-on-click))

    ;; then get the list of values
    (setq value-list (moss::%%get-value current-entity tp-id moss::*context*))
    ;; record property and value-list
    ;(setf (current-property-info current-page) (cons :tp value-list))
    ;; prepare a display of the list of values
    (setq display-list 
          (mapcar #'(lambda (xx)
                      (rformat 60 nil "~A" (moss::send tp-id '=format-value xx)))
                  value-list))
    ;; first deselect sp and il items and remove detail menus, clean edit area
    (bwf-deselect current-page :sp-area :il-area)
    ;; display successors
    (bwf-enable-view-area item :show-area display-list :tp)  
    
    ;; return item identity
    item))
|#

:EOF