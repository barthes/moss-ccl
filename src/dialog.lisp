;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;19/10/16
;;;		D I A L O G (dialog.lisp)
;;;
;;;=============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
This file implements a dialog one can have for querying MOSS. It contains a version 
of ELIZA to be called for doing some small talk, when MOSS does not understand what 
is going on.
In fact the questions one cann address to MOSS depend on the content of the Kbase.
They must lead to formal queries.

Building a formal query from an informal input can be done by trying to define
a path on the graph of model in the Kbase.
Ex: where are the legal texts for specifying how a person can be a candidate to
    the "RMA"?
This should be preprocessed as
 LOCATION LEGAL-DOCUMENT PERSON ELIGIBLE "RMA"
LOCATION is a property
LEGAL-DOCUMENT is an entity
PERSON is an entity
ELIGIBLE is a property
"RMA" is an entry point on an instance of PROGRAM

Questions accepted by MOSS are general questions like:
How do I get information?
What can you do for me?
What day are we today?
What is the syntax of queries?
What is a class?
What is a terminal property?
What is MOSS?
Who created MOSS?
What is the model used by MOSS?
How do I build a query?
What is the current version of MOSS?
How  can I ask questions?
Show me an example of class.
Give me ...

Possible actions are
Display a definition (what is? what are?)
   MOSS, MOSS model, class, property, method,..
Display an example (give, show)
   MOSS object
Display a process (how? show me how? help)
   help to do something (obtain info, formulate queries, 
Give some data (what? give me, who?)
   version of MOSS, author, day of week (date), time
   
Refer to some documentation (what?)

HISTORY
2019
 1016 copied from the ACL file

|#

(in-package :moss)

;;;============================== primitive dialog =============================
;;; debugging functions

;;;(defun dd (&optional previous-flag)
;;;  (let* ((c-context (car (has-q-context *conversation*)))
;;;         (state (car (has-state-object c-context)))
;;;         previous-context)
;;;    (format t "~&*conversation*: ~S" *conversation*)
;;;    (format t "~&   ~S" (eval *conversation*))
;;;    (format t "~&State: ~S" state)
;;;    (format t "~&   ~S" (eval state))
;;;    (format t "~&Context: ~S" c-context)
;;;    (format t "~&   ~S" (eval c-context))
;;;    (when previous-flag
;;;      (setq previous-context (car (%%has-value c-context '$NSTS.OF context)))
;;;      (format t "~&Previous context: ~S" previous-context)
;;;      (format t "~&   ~S" (eval previous-context)))
;;;    
;;;    :done))
;;;
;;;(defun sc (&key q-context (explain t))
;;;  (show-conversation :q-context q-context :explain explain))

;;;(defun print-time (conversation)
;;;  (multiple-value-bind (second minute hour date month year)
;;;                       (decode-universal-time (get-universal-time))
;;;    (send conversation '=display-text
;;;          (format nil "Current time is ~S:~S:~S and today is the ~S/~S/~S~%"
;;;                  hour minute second date month year))))

(defun print-time (conversation &optional (language *language*))
  (multiple-value-bind (second minute hour date month year)
                       (decode-universal-time (get-universal-time))
    (send conversation '=display-text
          (case language
            (:en
             (format nil "Current time is ~S:~S:~S and today is the ~S/~S/~S"
                     hour minute second date month year))
            (:fr
             (format nil 
                     "Il est actuellement ~S:~S:~S et nous sommes aujourd'hui le ~
                      ~S/~S/~S"
                     hour minute second date month year)))
          )))

#|
(defMacro build-simple-pattern-transition (ll target)
  "expands to a list of simple patterns"
  `,(mapcar #'(lambda (xx) `(:patterns ((,xx (?* ?y))
              ((?* ?x) ,xx (?* ?y)))
              :replace ',(intern (make-name 'make- xx)) :target ,target)) 
          ,ll))
|#

;;;================================================================================
;;;
;;;                  CONVERSATIONS AND SUB-CONVERSATIONS
;;;
;;;================================================================================

;;; we declare the top-level conversations
;;; the entry point of the dialog is _MAIN-CONVERSATION

;;; since we are in the MOSS package, we must export the =execute and =resume methods
;;; to be able to use them in the application package

#|
(eval-when (:load-toplevel :compile-toplevel :execute)
  (proclaim '(special 
              _MAIN-CONVERSATION
              _PROCESS-CONVERSATION
              _CREATE-CONVERSATION
              _DELETE-CONVERSATION
              _GET-SINGLE-OBJECT-CONVERSATION
              _HOW-CONVERSATION 
              _KILL-CONVERSATION 
              _MAIN-CONVERSATION
              _PRINT-CONVERSATION
              _SHOW-EXAMPLE-CONVERSATION
              _WHAT-CONVERSATION 
              ))
  (export '(=execute =resume =answer-analysis)))
|#

;;;================================================================================
;;;
;;;                       MAIN CONVERSATION (CONTROL LOOP)
;;;
;;;================================================================================

;;; we declare the top-level conversations
;;; the entry point of the dialog is _MAIN-CONVERSATION

(eval-when (:load-toplevel :compile-toplevel :execute)
  (proclaim '(special 
              _MAIN-CONVERSATION
              _PROCESS-CONVERSATION
              *ELIZA-RULES*
              )))

(defindividual 
  MOSS-DIALOG-HEADER
  (HAS-MOSS-LABEL "Main conversation")
  (HAS-MOSS-EXPLANATION "This is the main conversation loop.")
  (:var _main-conversation))

;;;===== states are declared as global variables
;;; the set of states can be considered as a plan to be executed for conducting the
;;; conversation with the master

;;;--------------------------------------------------------------------------------
(declaim (special _q-entry-state
                  _q-get-input
                  _q-more?
                  _q-process
                  _q-sleep
                  ))
;;;--------------------------------------------------------------------------------

(defstate _q-entry-state
  (:entry-state _main-conversation)
  (:label "Main conversation entry point")
  (:explanation 
   "Initial state when the assistant starts a conversation. Send a welcome message ~
    and wait for data. Also entered on a restart following an abort.")
  (:reset-conversation)
  (:text "Warning. This is not a real dialog in the sense that every question ~
            usually does not use the previous context. Thus, referents like ~
            pronouns or ellipsis (referring to part of the answer) won't work.")
  (:question-no-erase 
   ("- Hello! What can I do for you?"
    "- Hi! What would you like to know?"
    "- Hi! I will try to do my best to answer your question or ~
       execute your orders, but remember that my IQ is limited."))
  (:prompt "-")
  (:transitions 
   (:always :exec (setq moss::*transition-verbose* t) :save :target _q-process))
  )

;;;----------------------------------------------------------------- (MC) GET-INPUT

(defstate _q-get-input
  (:label "Get input")
  (:explanation
   "State in which the user inputs his request.")
  (:reset)
  (:question "- I'm listening....")
  (:transitions
   (:always :save :target _q-process)))

;;;-------------------------------------------------------------------- (MC) MORE?

(defstate _q-more?
  (:label "More?")
  (:explanation "Asking the user it he wants to do more interaction.")
  (:reset-conversation)
  (:question-no-erase
    ("- What else can I do for you?"
     "- Is there anything more I can do for you?"
     "- Would you like to do something else?"
     "- OK.")
   ) 
  (:transitions 
   (:starts-with ("nothing") :target _q-sleep)
   (:no :target _q-sleep)
   (:yes :target _q-get-input)
   (:otherwise :save :target _q-process)))

;;;------------------------------------------------------------------- (MC) PROCESS

(defstate _q-process
  (:label "Process state of main conversation")
  (:explanation
   "Here we call the main process organized as a sub-conversation.")
  (:transitions 
   (:sub-dialog _process-conversation
                :success _q-more?
                :failure _q-more?)))

;;;--------------------------------------------------------------------- (MC) SLEEP

(defstate _q-SLEEP
  (:label "Nothing to do")
  (:explanation
   "User said she wanted nothing more.")
  (:reset)
  (:text "- OK. I wait until you are ready.")
  (:question-no-erase "- Wake me up when you want by typing something...")
  (:transitions
   (:always :save :target _q-process)))

;;;================================================================================
;;;
;;;                      MAIN CONVERSATION (EXECUTION PART)
;;;
;;;================================================================================

;;; this conversation is intended to process the input from te user by first
;;; determining the performative, then the list of possible tasks

;;;-------------------------------------------------------------------------------
(declaim (special _q-eliza
                  _q-eliza-continue
                  _q-failure
                  _q-find-performative
                  _q-find-task
                  _q-select-task
                  _q-task-dialog))
;;;-------------------------------------------------------------------------------

;;;----------------------------------------------------- (MC) PROCESS-CONVERSATION

(defsubdialog 
  _process-conversation 
  (:label "Process conversation")
  (:explanation 
   "Processing steps of the main conversation.")
  )

;;;--------------------------------------------------------------------- (MC) ELIZA

(defstate _q-ELIZA
  (:label "ELIZA")
  (:explanation
   "Whenever MOSS cannot interpret what the user is saying, ELIZA is called to ~
    do some meaningless conversation to keep the user happy.
    We then record what the user says and resume the analysis.")
  (:eliza)
  (:transitions
   (:always :save :target _q-find-performative))
   ;(:always :target _q-eliza-continue))
  )

;;;------------------------------------------------------------ (MC) ELIZA-CONTINUE

(defstate _q-ELIZA-CONTINUE
  (:label " get user input after ELIZA")
  (:explanation
   "After ELIZA has returned an answer, we get user input")
  (:question-no-erase " ")
  (:transitions
   (:always :save :target _q-find-performative))
  )

;;;--------------------------------------------------------- (MC) FIND-PERFORMATIVE

(defstate _q-find-performative
  (:entry-state _process-conversation)
  (:process-state _main-conversation)
  (:label "Find performative")
  (:explanation 
   "Process what the user said trying to determine the type of performative ~
    among :request :assert :command. Put the result if any into the performative ~
    slot of the conversation object.")
  (:transitions 
   ;; time?
   (:patterns (("what" "time" (?* ?x)))
              :exec (moss::print-time moss::conversation) :success)
   ;; now really select performatives
   (:patterns (("what" "is" *)
               ("what" "are" *)
               ("what" *)
               ("when" *)
               ("where" *)
               ("why" *)
               ("who" *)
               ("whom" *)
               ("whose" *)
               ("how" "much" *)
               ("how" *)
               ("is" *)
               ("are" *)
               ("were" *)
               ("was" *)
               ("have" *)
               ("had" *)
               ("do" *)
               ("did" *)
               ("can" *)
               ("could" *)
               ("may" *)
               ("might" *)
               (* "?")
               )
              :set-performative (list :request)
              :target _q-find-task)
   (:patterns ((* "note" ?x)
               (* "remember" ?x))
              :set-performative (list :assert)
              :keep ?x
              :target _q-find-task)
   (:otherwise 
    :set-performative (list :command)
    :save
    :target _q-find-task))
  )

;;;------------------------------------------------------------------- (MC) FAILURE

(defstate _q-failure
  (:label "Failure state of main conversation")
  (:explanation
   "Failure state is entered when we return from a sub-dialog with a :failure tag. ~
    We check for more tasks to perform (listed in the task-list slot of the ~
    conversation object). If there are more, we execute the first one. If there ~
    are no more, we return with a failure tag.")
  (:transitions
   (:test (HAS-MOSS-TASK-LIST conversation) 
          :exec (let ((task-list  (HAS-MOSS-TASK-LIST conversation)))
                 (send conversation '=replace 'HAS-MOSS-TASK (pop task-list))
                 (send conversation '=replace 'HAS-MOSS-TASK-LIST task-list)
                 )
          :target _q-task-dialog)
   (:otherwise :failure))           
  ;(:answer-analysis)
  )

;;;(defownmethod
;;;  =answer-analysis _q-failure (conversation input)
;;;  (declare (ignore input))
;;;  (let* ((task-list (HAS-MOSS-TASK-LIST conversation))
;;;         task)
;;;    (if task-list
;;;      (progn
;;;        ;; record next task
;;;        (setq task (pop task-list))
;;;        ;; remove it from task-list
;;;        (send conversation '=replace 'HAS-MOSS-TASK-LIST task-list)
;;;        ;; add it to conversation
;;;        (send conversation '=replace 'HAS-MOSS-TASK (list task))
;;;        ;; transfer to sub-dialog
;;;        `(:transition ,_q-task-dialog))
;;;      ;; when no more tasks we return :failure and the state variable still contains
;;;      ;; :failure. The crawler will return one more level until hitting a non
;;;      ;; failure return containing a specific state (e.g. the one for the main
;;;      ;; conversation)
;;;      (list :failure)
;;;      )))

;;;----------------------------------------------------------------- (MC) FIND-TASK

(defstate _q-find-task
  (:label "Find task")
  (:explanation 
   "Process the input to determine the task to be undertaken. Combines the words ~
    from the sentence to see if they are entry points for the index property of ~
    any task. Collect all tasks for which there is an index in the sentence.")
  (:answer-analysis)
  )

(defownmethod
    =answer-analysis _q-find-task (conversation input)
  "Using input to find tasks If none: failure, if one: OK, if more: must ask user to ~
   select one. Checks for an entry point for an index of a task.
Arguments:
   conversation: current conversation
   input: list containing the input as a list of words"
  (let* ((performative (read-fact conversation :PERFORMATIVE))
         task-list)
    (moss::vformat "_mc-find-task /performative: ~S, package: ~S, input:~%  ~S" 
                   performative *package* input)
    ;; try to find a task from the input text
    (setq task-list (moss::find-objects 
                     '(moss-task 
                       (has-MOSS-index-pattern 
                        (MOSS-task-index 
                         (has-MOSS-index :is :?))))
                     input :all-objects t)) 
    (moss::vformat "_mc-find-task /possible tasks:~%  ~S" task-list)
    ;; filter tasks that do not have the right performative
;;;    (setq task-list ; JPB 140820 emoved mapcan
;;;          (reduce
;;;           #'append
;;;           (mapcar #'(lambda (xx) 
;;;                       (if (intersection performative (HAS-MOSS-PERFORMATIVE xx))
;;;                           (list xx)))
;;;             task-list)))
    (moss::vformat "_mc-find-task /task list after performative check: ~S" task-list)
    (cond 
     ;; if empty, ask ELIZA
     ((null task-list)
      `(:transition ,_q-ELIZA))
     ;; if one, then OK
     ((null (cdr task-list))
      ;; save results, make the task the conversation task
      (setf (HAS-MOSS-TASK conversation) task-list)
      `(:transition ,_q-task-dialog))
     (t 
      ;; otherwise must select one from the results
      (setf (HAS-MOSS-TASK-LIST conversation) task-list)
      `(:transition ,_q-select-task)))
    ))

;;;--------------------------------------------------------------- (MC) SELECT-TASK

(defstate 
  _q-select-task
  (:label "mc select task")
  (:explanation 
   "we have located more than one task. We rank the tasks by computing ~
    the average weight of the terms in the task index slot. We then ~
    record the list of tasks into the statte-context task-list slot ~
    and activate the highest ranking task.")
  (:answer-analysis) 
  )

(defownmethod =answer-analysis _q-select-task (conversation input)
  "we have located more than one task. We rank the tasks by computing ~
   the MYCIN combination weight of the terms in the task index slot. We then ~
   record the list of tasks into the conversation task-list slot ~
   and activate the highest ranking task."
  ;(declare (ignore input))
  ;; the list of tasks is in the HAS-TASK-LIST slot
  (let* ((task-list (HAS-MOSS-TASK-LIST conversation))
         patterns weights result pair-list selected-task level word-weight-list)
    (moss::vformat "_select-task /input: ~S~&  task-list: ~S" input task-list)
    
    ;; first compute a list of patterns (combinations of words) from the input
    (setq patterns (mapcar #'car (moss::generate-access-patterns input)))
    (moss::vformat "_select-task /input: ~S~&  generated patterns:~&~S" 
                   input patterns)
    ;; then, for each task
    (dolist (task task-list)
      (setq level 0)
      ;; get the weight list
      (setq weights (moss::%get-INDEX-WEIGHTS task))
      (moss::vformat "_select-task /task: ~S~&  weights: ~S" task weights)
      ;; check the patterns according to the weight list
      (setq word-weight-list (moss::%get-relevant-weights weights patterns))  
      (moss::vformat "_select-task /word-weight-list:~&  ~S" word-weight-list)
      ;; combine the weights
      (dolist (item word-weight-list)
        (setq level (+ level (cadr item) (- (* level (cadr item))))))
      (moss::vformat "_select-task /level: ~S" level)
      ;; push the task and weight onto the result list
      (push (list task level) result)
      )
    (moss::vformat "_select-task /result:~&~S" result)
    
    ;; order the list (($E-TASK.25 0.6)...)
    (setq pair-list (sort result #'> :key #'cadr)) 
    (moss::vformat "_select-task /pair-list:~&  ~S" pair-list)
    ;; keep the first task whatever its score
    (setq selected-task (caar pair-list))
    ;; remove the task that have a weight less than task-threshold (default 0.4)
    (setq pair-list
          (remove nil 
                  (mapcar 
                   #'(lambda (xx) 
                       (if (>= (cadr xx) *moss-threshold*) xx))
                   pair-list)))
    ;; if task-list is empty then return the first saved task
    ;; note: this may not be a good policy if the score is too low
    (if (null pair-list)
      (progn
        ;; reset the task-list slot of the conversation object
        (setf (HAS-MOSS-TASK-LIST conversation) nil)
        ;; put the saved task into the task slot
        (setf (HAS-MOSS-TASK conversation) (list selected-task))
        ;; go to task-dialog
        `(:transition ,_q-task-dialog)
        )
      (progn
        ;; remove the weights
        (setq task-list (mapcar #'car pair-list))
        ;; select the first task of the list
        (setq selected-task (pop task-list))
        (moss::vformat "_select-task /selected task: ~S" selected-task)
        ;; save the popped list in the task-list slot of the conversation object
        (setf (HAS-MOSS-TASK-LIST conversation) task-list)
        (setf (HAS-MOSS-TASK conversation) (list selected-task))
        ;; go to task-dialog
        `(:transition ,_q-task-dialog)))
    ))

;;;--------------------------------------------------------------- (MC) TASK-DIALOG

(defstate _q-task-dialog
  (:label "Task dialog")
  (:explanation 
   "We found a task to execute (in the GOAL slot of the conversation). We activate ~
    the dialog associated with this task.")
  ;; we launch the task dialog as a sub-conversation
  ;; the task contains the name of a sub-conversation header, e.g. _get-tel-nb
  (:answer-analysis)
  )

;;; answer-analysis is required because (car (HAS-MOSS-dialog...) must be
;;; evaluated

(defownmethod =answer-analysis _q-task-dialog (conversation input)
  "We prepare the set up to launch the task dialog.
Arguments:
   conversation: current conversation"
  (declare (ignore input))
  (let ((task-id (car (send conversation '=get 'HAS-MOSS-TASK))))
    (moss::vformat "_mc-task-dialog /task-id: ~S dialog: ~S" 
                   task-id (HAS-MOSS-DIALOG task-id))
    ;; we launch the task dialog as a sub-conversation
    ;; the task contains the name of a sub-conversation header, e.g. _get-tel-nb
    `(:sub-dialog ,(car (HAS-MOSS-DIALOG task-id)) :failure ,_q-failure)
    ))


;;;================================================================================
;;;================================================================================
;;;
;;;                             SUB-CONVERSATIONS
;;;
;;;================================================================================
;;;================================================================================

;;; Sub-conversations are defined by a conversation header, that points to the
;;; entry state.
;;; There are two output states:
;;;   SUCCESS: the result is stored into the RESULTS slot of the state-context
;;;   FAILURE: no result could be achieved
;;; In case of bad error there is a throw to an :error label
;;; data are obtained from the HAS-DATA slot of the context object

;;;================================================================================
;;;
;;;              GET SINGLE OBJECT FROM ENTRY POINT CONVERSATION
;;;
;;;================================================================================

;;; this subdialog is intended to get a single object identifier from an entry point.
;;; Takes an entry point from FACTS/RESULT area and accesses the KB. It more than
;;; one object must choose.
;;;--------------------------------------------------------------------------------

(defsubdialog _GET-SINGLE-OBJECT-CONVERSATION
  (:label "Get single object from EP conversation")
  (:explanation 
   "This dialog is meant to return a single object corresponding to a given entry ~
    point. If more than one, the user is asked to make a selection.")
  (:states _gso-entry-state
           _gso-select
           )
  ) 

;;;--------------------------------------------------------------- (GSO) ENTRY-STATE
;;; define an entry instance and record it in a global variable.

(defstate 
  _gso-entry-state
  (:label "start of the GET SINGLE OBJECT dialog")
  (:entry-state _get-single-object-conversation)
  (:explanation 
   "Initial state takes an entry point from input data and accesses ~
    the knowledge base. If one object then success, if more than one, go to select ~
    state, if none, then failure.")
  (:execute
   (let ((input (read-fact conversation :input)) results)
     (vformat "~%entry point in data => ~S" (car input))
     ;; access objects
     (setq results (access (car input)))
     (vformat "~%results of access => ~S" results)
     ;; save results
     (replace-fact conversation :results results)
     ))
  (:transitions 
   (:test (null (read-fact conversation :results)) :failure)
   (:test (cdr (read-fact conversation :results)) :target _gso-select)
   (:otherwise :success)
   )
  )

;;;-------------------------------------------------------------------- (GSO) SELECT

;;; we have several objects to choose from. We show a list using =summary and ask
;;; the user to choose by giving a number. If bad number, return :failure, otherwise
;;; return selected obj-id in FACTS/RESULTS

(defstate 
    _gso-select
    (:label "gso select")
  (:explanation "we have retrieved more than one object we ask user to select one.")
  (:execute-preconditions
   (let ((count 0))
     (dolist (obj-id (read-fact conversation :results))
       (send conversation '=display-text 
             (format nil "   ~S. ~A" (incf count) (send obj-id '=summary))
             :stream (car (HAS-MOSS-INPUT-WINDOW conversation))))))
  (:question-no-erase "- Choose one by typing number and period: e.g. 2.")
  ;; the rest is for the =resume method
  (:execute
   (let* ((obj-id-list (read-fact conversation :results))
          (value (read-from-string obj-id-list nil nil)))
     (if (numberp value)
         (replace-fact conversation :results (nth (1- value) obj-id-list))
       (replace-fact conversation :results nil))))
  (:transitions
   (:test (read-fact conversation :results) :success)
   (:otherwise :failure))
  )

;;;================================================================================
;;;
;;;                               HOW CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to give information about how to perform actions
;;; in the MOSS environment; e.g., how to create or delete objects.
;;; the pattern attached to the conversation is a display function, with a single
;;; slot to be filled with the documentation object answering the question.

(defsubdialog _HOW-CONVERSATION
  (:label "How conversation")
  (:explanation 
   "This dialog is meant to give some information to the user regarding the ~
    various actions that a user can do in the MOSS system. The idea is to get the ~
    identifier of a documentation object corresponding to the request and to ~
    display the associated text.")
  ;(:failure-explanation "could not understand what action was asked.")
  ;(:success-explanation "we have given the requested info.")
  (:states _hc-display-results
           _hc-dont-understand
           _hc-entry-state)
  )

;;;---------------------------------------------------------------- (HC) ENTRY-STATE

(defstate 
  _hc-entry-state
  (:label "start of the HOW dialog")
  (:entry-state _how-conversation)
  (:explanation 
   "Initial state may have data left in the q-context. The data is searched for ~
    the presence of verbs like create, make, edit, modify, etc.
    When present, we look for an entry point in the resst of the sentence.")
  (:transitions
   (:patterns (("how" "are" "you" *))
              :eliza
              :success)
   (:patterns ((* "create" ?y)
               (* "make" ?y)
               (* "build" ?y)
               (* "define" ?y)
               (* "construct" ?y))
              :keep ?y 
              :sub-dialog _how-to-create-conversation 
              :success _hc-display-results)
   (:patterns ((* "find" (?* ?y))
               (* "locate" (?* ?y))
               (* "query" (?* ?y)))
              :replace ("find-object")
              :sub-dialog _get-single-object-conversation
              :success _hc-display-results)
   (:patterns ((* "start" "moss" (?* ?y))
               (* "run" "moss" (?* ?y))
               (* "work" (?* ?y) "MOSS" (?* ?z)))
              :replace ("start-moss")
              :sub-dialog _get-single-object-conversation
              :success _hc-display-results)
   (:patterns ((* "destroy" ?y)
               (* "kill" ?y))
              :keep ?y
              :sub-dialog _kill-conversation
              :success _hc-display-results)
   (:patterns ((* "remove" ?y)
               (* "delete" ?y)
               (* "suppress" ?y))
              :keep ?y
              :sub-dialog _delete-conversation
              :success _hc-display-results)
   (:patterns ((* "edit" (?* ?y))
               (* "modify" (?* ?y)))
              :replace ("modify-object")
              :sub-dialog _get-single-object-conversation
              :success _hc-display-results)
   (:otherwise :target _hc-dont-understand)
   )
  )

;;;------------------------------------------------------------ (HC) DISPLAY-RESULTS

(defstate 
  _hc-display-results
  (:label "HC display results")
  (:explanation "we got a document identifier and display the associated doc.")
  (:execute
   (let ((obj-id (car (read-fact conversation :results))))
     (if obj-id
         (send obj-id '=print-documentation 
               :stream (car (HAS-MOSS-OUTPUT-WINDOW conversation))
               :no-summary t))))
  (:transitions 
   (:always :success))
  )

;;;------------------------------------------------------------ (HC) DONT-UNDERSTAND

(defstate 
  _hc-dont-understand
  (:label "HC arguments not understood")
  (:explanation "we have some data but there is no entry point among them.")
  (:text "- I do not understand what you want to do: how what?")
  (:transitions
   (:always :failure))
  )

;;;================================================================================
;;;
;;;                          HOW TO CREATE CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to find the argument of the "how to create"
;;; question and to return it  in the RESULTS slot.

(defsubdialog _HOW-TO-CREATE-CONVERSATION 
  (:label "[How] to create conversation")
  (:explanation 
   "this conversation is intended to find the argument of the \"how to create\"
    question and to return it  in the RESULTS slot")
  (:states _crc-dont-understand
           _crc-entry-state)
   )

;;;-------------------------------------------------------------- (HCC) ENTRY-STATE

(defstate 
  _crc-entry-state
  (:label "start of the create dialog")
  (:explanation 
   "Initial state may have data left in the q-context. The data is searched for ~
    the presence of words like object, class, attribute, method, etc.")
  (:entry-state _how-to-create-conversation)
  (:transitions
   (:patterns ((* "object" (?* ?y)))
              :replace ("make-object")  
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "class" *)
               (* "concept" *))
              :replace ("make-class")
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "orphan" *))
              :replace ("make-orphan")  
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "own-method" *)
               (* "own" "method" *))
              :replace ("make-own-method")  
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "instance-method" *)
               (* "instance" "method" *))
              :replace ("make-instance-method")
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "universal-method" *)
               (* "universal" "method" *))
              :replace ("make-universal-method") 
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "method" *))
              :replace ("make-method") 
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "attribute" *))
              :replace ("make-attribute") 
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "relation" *))
              :replace ("make-relation")  
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "relationship" *))
              :replace ("make-relationship")  
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "query" *))
              :replace ("make-query") 
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "version" *))
              :replace ("make-version") 
              :sub-dialog _get-single-object-conversation)
   (:otherwise :target _crc-dont-understand))
  )

;;;----------------------------------------------------------- (HCC) DONT-UNDERSTAND

(defstate 
  _crc-dont-understand
  (:label "Create arguments not understood")
  (:explanation "we have some data but there no recognized argument among them.")
  (:text "- I do not understand what you want to create.")
  (:question-no-erase "- What type of object do you want to create?")
  (:transitions
   (:contains (none abort) :failure)
   (:otherwise :save :target _crc-entry-state)))


;;;================================================================================
;;;
;;;                    HOW TO DELETE CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to find the argument of the "how to delete"
;;; question and to return it  in the RESULTS slot.

(defsubdialog 
  _DELETE-CONVERSATION
  (:label "[How] to delete conversation")
  (:explanation 
   "This dialog is meant to give some information to the user regarding the ~
    various actions that a user can do to DELETE an object. The idea is to get the ~
    identifier of a documentation object corresponding to the request and to ~
    display the associated text.")
  ;(:failure-explanation "could not understand what object the user wanted to delete.")
  ;(:success-explanation "success exit with an oid in the results slot")
  (:states _hdc-dont-understand
           _hdc-entry-state)
  )

;;;-------------------------------------------------------------- (HDC) ENTRY-STATE

(defstate 
  _hdc-entry-state
  (:label "start of the delete dialog")
  (:entry-state _DELETE-CONVERSATION)
  (:explanation 
   "Initial state may have data left in the q-context. The data is searched for ~
    the presence of words like object, class, attribute, method, etc.")
  (:transitions
   (:patterns ((* "object" *)
               (* "class" *)
               (* "orphan" *)
               (* "method" *)
               )
              :replace "delete-object"  
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "terminal" "property" *)
               (* "attribute" *)
               (* "value" *))
              :replace ("delete-attribute") 
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "structural" "property" *)
               (* "relation" *)
               (* "relationship" *))
              :replace ("delete-relation")  
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "version" *))
              :replace ("delete-version")
              :sub-dialog _get-single-object-conversation)
   (:patterns ((* "?" *))
              :keep ?y :target _hdc-dont-understand)
   (:otherwise :target _hdc-dont-understand))
  )

;;;----------------------------------------------------------- (HDC) DONT-UNDERSTAND

(defstate 
  _hdc-dont-understand
  (:label "Delete arguments not understood")
  (:explanation "we have some data but there no recognized argument among them.")
  (:answer "- I do not understand what you want to delete")
  (:question-no-erase
   "- What type of object do you want to delete")
  (:transitions
   (:contains ("none" "nothing" "abort" "forget it") :failure)
   (:otherwise :save :target _hdc-entry-state)))

;;;================================================================================
;;;
;;;                  PRINT ALL DOCUMENTATION CONVERSATION
;;;
;;;================================================================================

;;; this conversation is meant to print objects to be extracted from a list of words

(defsubdialog
  _PRINT-ALL-DOCUMENTATION-CONVERSATION
  (:label "Print object documentation")
  (:explanation 
   "This dialog is meant to print the documentation attached to all MOSS objects ~
    that could be referenced by a list of words. It uses the =print-self universal ~
    method.")
  (:states _pad-entry-state)
  )

;;;--------------------------------------------------------------- (PAD) ENTRY-STATE

(defstate 
  _pad-entry-state
  (:label "start of the PRINT dialog")
  (:entry-state _print-all-documentation-conversation)
  (:explanation 
   "Initial state may have data left in the q-context. The data either represent ~
    an entry point or something else that should be used to select objects.")
  (:answer-analysis)
  )

(defownmethod =answer-analysis _pad-entry-state (conversation input)
  "Using list of words to access objects. If one, OK, if more, must ask user to ~
   select one, if none, failure."
  (let (entry-point-list object-list)
    (catch 
     :return
     ;; first get entry-points
     (setq entry-point-list (moss::find-best-entries input))
     ;; if none then failure
     (unless entry-point-list (throw :return (list :failure)))
     
     ;; get objects corresponding to entry points
     ;(moss::trformat "_pad-entry-state /entry-point-list: ~%  ~S" entry-point-list)
     (setq object-list ; JPB140820 removed mapcan
           (delete-duplicates 
            (reduce #'append (mapcar #'access entry-point-list))))
     ;(moss::trformat "_pad-entry-state /object-list: ~%  ~S" object-list)
     
     ;; print documentation (maybe we should get rid of generic properties)
     (dolist (obj-id object-list)
       (send obj-id '=print-documentation 
             :stream (car (HAS-MOSS-OUTPUT-WINDOW conversation))))
     ;; success
     (list :success))))

;;;================================================================================
;;;
;;;                       PRINT HELP CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to help the master by giving information:
;;;   - in general (what tasks are available
;;;      "what can you do for me?" "help." "what can I do?"
;;;   - on a particular topic
;;;      "how do I send a mail?" "help me with the mail?"
;;; specific help corresponding to special help tasks are caught by the task 
;;; task selection mechanism?

(defsubdialog 
  _print-help-conversation 
  (:label "Help conversation")
  (:explanation 
   "Help was asked.")
  (:states _ph-entry-state  ; required by defstate
           _ph-print-global-help)
   )

;;;----------------------------------------------------------- (PH) PH-ENTRY-STATE

(defstate 
  _ph-entry-state
  (:label "Explain dialog entry")
  (:entry-state _print-help-conversation )
  (:explanation "Master is asking for help.")
  (:transitions
   ;; make sure we have a question
   (:patterns ((* "help" *)
               ("sos")
               ("?")
               ((?* ?x) "I" "am" (?* ?y) "lost" (?* ?z))
               ("what" * "you" "propose" *)
               ((?* ?x) "services" (?* ?y) "you" "offer" (?* ?z))
               ("what" "can" * "do" *)
               ("what" "skills" *)
               ("what" * "skills" *))
              :target _ph-print-global-help)
   (:otherwise :failure)
   )
  )

;;;----------------------------------------------------- (PH) PH-PRINT-GLOBAL-HELP

(defstate
    _ph-print-global-help
    (:label "Print general help")
  (:explanation "No specific subject was included.")
  (:execute (let   ; JPB0808
             ((obj-id (car (send '>-help '=get ">MOSS-title"))) jpb1605
              (task-list (access '(moss-task)))
              )
           (when obj-id 
             (send obj-id '=print-documentation 
                   :erase t
                   :stream (car (HAS-MOSS-OUTPUT-WINDOW conversation)))
             (send conversation '=display-text 
                   "More details: I can do the following tasks:" 
                   :final-new-line t :erase nil)
             (dolist (task task-list)
               (send task '=print-documentation :lead "  - " :no-summary t
                     :stream (car (HAS-MOSS-OUTPUT-WINDOW conversation)))
               )
             ;; skip a line
             (send conversation '=display-text " " :erase nil)
             )))
  (:transitions
   (:always :success))
  )

;;;================================================================================
;;;
;;;                       SET FONT SIZE CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to modify the size of the dialog font, usually to
;;; increase it or to return to the default value.

(defsubdialog _set-font-size-conversation
    (:short-name sfs)
  (:label "Font size conversation")
  (:explanation 
   "The data is searched for the presence of words like increase, ~
    bigger, smaller, defaults, reset,...")
  ;(:action _sfs-action)
  (:states _sfs-entry-state  ; required by defstate
           _sfs-dont-understand
           _sfs-try-again
           _sfs-sorry)
  )
  
;;;--------------------------------------------------- (SFS) FONT-SIZE-ENTRY-STATE

(defstate 
  _sfs-entry-state
  (:conversation _set-font-size-conversation)
  (:entry-state _set-font-size-conversation)
  (:label "Font size dialog entry")
  (:short-name sfs)
  (:explanation "we can't modify the font size")
  (:transitions
   (:always :target _sfs-sorry)
   )
  )

;;;--------------------------------------------------------- (SFS) DONT-UNDERSTAND

(defstate 
  _sfs-dont-understand
  (:label "Did not understand. Ask master.")
  (:explanation "Assistant is asking master for font-size clarification.")
  (:question "- Do you want a bigger font size, a smaller one, or use the default?")
  (:transitions
   (:always :save :target _sfs-try-again)))

;;;------------------------------------------------------------------- (sfs) SORRY

(defstate 
  _sfs-sorry
  (:label "Font dialog failure.")
  (:explanation "We cannot make things out from the data.")
  (:reset)
  (:answer "- Sorry I do not know how to do that.")
  (:transitions
   (:always :failure))
  )

;;;--------------------------------------------------------------- (sfs) TRY-AGAIN

;;; The simplest use is to take text in the HAS-DATA slot of the Q-CONTEXT
;;; and send a message to :SA-ADDRESS

(defstate 
  _sfs-try-again
  (:label "Font size try again")
  (:explanation "we asked master and try to pick up the answer.")
  (:immediate-transitions
   (:patterns ((* "bigger" *)
               (* "larger" *))
              ;:exec (mw-use-bigger-font) :success)
              :target _sfs-sorry)
   (:patterns ((* "smaller" *))
              ;:exec (mw-reset-font) :success)
              :target _sfs-sorry)
   (:patterns ((* "default" *)
               (* "defaults" *))
              :target _sfs-sorry)
   ;:exec (mw-reset-font) :success)
   (:otherwise :target _sfs-sorry)
   )
  )

;;;================================================================================
;;;
;;;                         SHOW EXAMPLE CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to give examples of various MOSS constructs.
;;; the pattern attached to the conversation is a display function, with a single
;;; slot to be filled with the documentation object answering the question.

(defsubdialog 
  _SHOW-EXAMPLE-CONVERSATION
  (:short-name sec)
  (:label "Show example conversation")
  (:explanation 
   "This dialog is meant to give some examples of what one can do in the MOSS system.")
  ;(:failure-explanation "could not understand what ation was asked.")
  ;(:success-explanation "we have given the requested info.")
  (:states _sec-display-results
           _sec-dont-understand
           _sec-entry-state
           _sec-sorry
           _sec-try-again)
  )

;;;--------------------------------------------------------------- (sec) ENTRY-STATE

(defstate 
  _sec-entry-state
  (:label "start of the SHOW EXAMPLE dialog")
  (:entry-state _SHOW-EXAMPLE-CONVERSATION)
  (:explanation 
   "Initial state may have data left in the q-context. The data is searched for ~
    the presence of nouns like terminal property, class, method, etc.")
  (:transitions
   (:patterns ((* "class" *)
               (* "model" *))
              :replace ("class-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "terminal" "property" *)
               (* "attribute" *))
              :replace ("attribute-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "structural" "property" *)
               (* "relation" *)
               (* "relationship" *))
              :replace ("relation-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "orphan" *))
              :replace ("orphan-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "own" "method" *))
              :replace ("own-method-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "universal" "method" *)
               (* "instance" "method" *))
              :replace ("universal-method-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "method" *)
               (* "instance" "method" *))
              :replace ("method-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:otherwise :target _sec-dont-understand)
   )
  )

;;;----------------------------------------------------------- (sec) DISPLAY-RESULTS

(defstate 
  _sec-display-results
  (:label "Show example display results")
  (:explanation "we got a document identifier and display the associated doc.")
  (:execute 
   (let ((obj-id (car (read-fact conversation :results))))
     (when obj-id
       (send obj-id '=get-documentation :no-summary t)
       (send conversation '=display-text *answer* :erase t :final-new-line t))
     (unless obj-id
       (send conversation '=display-text "*no example available, sorry*"
             :erase t :final-new-line t))
     ))
  (:transitions (:always :success))
  )

;;;----------------------------------------------------------- (sec) DONT-UNDERSTAND

(defstate 
  _sec-dont-understand
  (:label "sec arguments not understood")
  (:explanation "we have some data but there is no entry point among them.")
  (:answer "I do not understand what you want to do")
  (:question "You want an example of what?")
  (:transitions
   (:contains ("nothing" "abort" "forget it") :failure)
   (:always :save :target _sec-try-again)))

;;;-------------------------------------------------------------------- (sec) SORRY

(defstate _sec-sorry
    (:label "sec arguments not understood")
  (:explanation "we did not understand and quit.")
  (:text "I have no example of that sorry.")
  (:transitions (:always :failure))
  )

;;;---------------------------------------------------------------- (sec) TRY-AGAIN

(defstate 
  _sec-try-again
  (:label "example try again")
  (:explanation "we got some info and try again")
  (:transitions
   (:patterns ((* "class" *)
               (* "model" *))
              :replace ("class-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "terminal" "property" *)
               (* "attribute" *))
              :replace ("attribute-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "structural" "property" *)
               (* "relation" *)
               (* "relationship" *))
              :replace ("relation-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "orphan" *))
              :replace ("orphan-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "own" "method" *))
              :replace ("own-method-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "universal" "method" *)
               (* "instance" "method" *))
              :replace ("universal-method-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:patterns ((* "method" *)
               (* "instance" "method" *))
              :replace ("method-example")
              :sub-dialog _get-single-object-conversation
              :success _sec-display-results)
   (:otherwise :target _sec-sorry)
   )
  )

;;;================================================================================
;;;
;;;                       TRACE CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to modify the size of the dialog font, usually to
;;; increase it or to return to the default value.

(defsubdialog 
  _trace-conversation
  (:label "Trace conversation")
  (:explanation 
   "Command to toggle the dialog trace.")
  (:states _tr-entry-state)
  )

;;;-------------------------------------------------------- (TR) TRACE-ENTRY-STATE

(defstate 
  _tr-entry-state
  (:label "Trace dialog entry")
  (:entry-state _trace-conversation)
  (:explanation "We search the input for the trace command.")
  
  (:transitions
   ;; system commands
   (:patterns ((* "show" "transitions" *)
               (* "trace" "transitions" *)
               (* "transitions" "on" *)
               (* "print" "transitions" *))
              :exec (progn (setq moss::*transition-verbose* t
                               moss::*verbose* t)
                      (send conversation '=display-text
                            "- Transitions are now traced." :erase t))
              :success)
   (:patterns ((* "no" "trace" *)
               (* "kill" "trace" *)
               (* "trace" "off" *)
               (* "stop" "trace" *)
               (* "no" "verbose" *))
              :exec (progn (setq moss::*verbose* nil
                                 moss::*transition-verbose* nil)
                      (send conversation '=display-text 
                            "- Trace is now off." :erase t))
              :success)
   (:patterns ((* "trace" "on" *)
               (* "set" "trace" *)
               (* "start" "trace" *)
               (* "verbose" *))
              :exec (progn (setq moss::*verbose* t)
                      (send conversation '=display-text 
                            "- Now tracing action..." :erase t))
              :success)
   (:patterns ((* "kill" "transitions" *)
               (* "hide" "transitions" *)
               (* "transitions" "off" *)
               (* "do" "not" "show" "transitions" *))
              :exec (progn (setq moss::*transition-verbose* nil)
                      (send conversation '=display-text 
                            "- No longer tracing transitions." :erase t))
              :success)
   (:otherwise :failure)
   )
  )

;;;================================================================================
;;;
;;;                       WHAT CONVERSATION
;;;
;;;================================================================================

;;; this conversation is meant to give some information to the user regarding the
;;; various entities of the MOSS system. The idea is to get the identifier of an
;;; object and to print the corresponding DOCUMENTATION object. 

(defsubdialog
    _WHAT-CONVERSATION
    (:label "What conversation")
  (:explanation 
   "This dialog is meant to give some information to the user regarding the ~
    various entities of the MOSS system. The idea is to get the identifier of an ~
    object and to print the HAS-DOCUMENTATION slot associated with it. If not ~
    present we call the =what? method.")
  ;(:failure-explanation "could not understand what object the user wanted to create.")
  ;(:success-explanation "success exit with an oid in the results slot")
  (:states _wc-access-ep
           _wc-display-more-results
           _wc-display-results
           _wc-dont-understand
           _wc-dont-understand-app
           _wc-entry-state
           _wc-find-entry-state
           _wc-select)
  )

;;;---------------------------------------------------------------- (WC) ENTRY-STATE
;;; ENTRY STATE

;;; the entry state should remove the 'what is' 'what are' or else... and keep the
;;; rest; pattern (?x 'what is' ?y) and  (keep ?y)

(defstate 
    _wc-entry-state
  (:label "start of the WHAT dialog")
  (:entry-state _WHAT-CONVERSATION)
  (:explanation 
   "We try to clean the data to remove useless leading words.")
  (:transitions
   (:patterns ((* "what" "is" ?y)
               (* "what" "are" ?y)
               (* "means" ?y)
               (* "define" ?y)
               (* "explain" ?y)
               )
              :keep ?y :target _wc-find-entry-state)
   (:otherwise :target _wc-find-entry-state))
  )

;;;------------------------------------------------------------------ (WC) ACCESS-EP

(defstate 
  _wc-access-ep
  (:label "Got an entry point")
  (:explanation
   "We have a list entry point and can use it to access the knowledge base by calling ~
    the access function.")
  (:answer-analysis)
  )

(defownmethod =answer-analysis _wc-access-ep (conversation input)
  "Using entry point to access objects. If one, OK, if more, must ask user to ~
   select one."
  (declare (ignore input))
  (let (results new-data)
    ;; get data from previous results
    (vformat "~%...entry points in data => ~S" (read-fact conversation :results))
    ;; access objects
    (dolist (item (read-fact conversation :results))
      (setq new-data (access item :verbose *verbose*))
      (vformat "~%...results of ~S access => ~S" item results)
      ;; save results
      (setq results (append results new-data)))
    
    ;; when we are in the MOSS package we keep only documentation objects and 
    ;; methods (why methods ?)
    ;; I.e., all documentation in the MOSS package must be implemented through 
    ;; documentation objects
    (vformat "_wc-access-ep /results: ~S" results)
    (if (eql *package* (find-package :moss))
        (setq results  ; jpb 140820 removed mapcan
              (reduce
               #'append
               (mapcar #' (lambda(xx) (if (or (%type? xx '$DOCE)
                                              (%type? xx '$FN) 
                                              (%type? xx '$UNI)) 
                                          (list xx))) 
                 results))))
    (vformat "_wc-access-ep /filtered results: ~S" results) 
    ;; save results for transferring to next conversation state
    (replace-fact conversation :results results) 
    ;; if one value, success, otherwise must ask the user to choose
    (cond 
     ((and (eql *package* (find-package :moss))(null results))
      `(:transition ,_wc-dont-understand)) 
     ;; when package different from :moss we assume we are in the application
     ((null results) 
      `(:transition ,_wc-dont-understand-app)) 
     ((null (cdr results)) 
      `(:transition ,_wc-display-results)) 
     ;; when in moss package print everything
     ((eql *package* (find-package :moss)) 
      `(:transition ,_wc-display-results)) 
     ;; even in application package display everything!
     (t 
      `(:transition ,_wc-display-results))) 
    ))

;;;------------------------------------------------------- (WC) DISPLAY-MORE-RESULTS

(defstate 
    _wc-display-more-results
  (:label "WC display more results")
  (:explanation "we had a long list to print and we ask user if he wants to see the ~
                 next 5 entries.")
  (:question "More?")
  (:answer-analysis)
  )

(defownmethod =answer-analysis _wc-display-more-results (conversation input)
  "if no more quit, otherwise print next five, if more than 5 try again."
  (declare (ignore input))
  (let ((obj-id-list (read-fact conversation :RESULTS)))
    (cond
     ;; if nil get out
     ((null obj-id-list)
      '(:failure))
     ;; otherwise print some
     (t
      ;; print first 5 values
      (broadcast (subseq obj-id-list 0 (min 5 (length obj-id-list))) 
                 '=print-documentation 
                 :final-new-line t
               :stream (car (HAS-MOSS-OUTPUT-WINDOW conversation)))
      ;; if more than 5 we go to print more
      (if (> (length obj-id-list) 5)
          (progn
            (replace-fact conversation :RESULTS (subseq obj-id-list 5))
            `(:transition ',_wc-display-more-results))
        ;; otherwise quit
        (progn
          ;; clean results
          (replace-fact conversation :RESULTS nil)
          '(:success)))))))

;;;------------------------------------------------------------ (WC) DISPLAY-RESULTS

(defstate _wc-display-results
  (:label "WC display results")
  (:explanation "we got an object identifier and display the associated doc or ~
                 we send a =what? method to the object.")
  (:answer-analysis)
  )

(defownmethod =answer-analysis _wc-display-results (conversation input)
  "save the obj-id of the result into the action pattern and executes it."
  (declare (special _wc-display-more-results))
  (let* (obj-id-list)
    ;; get results
    (setq input (read-fact conversation :results))
    (catch 
      :return
     ;; MOSS objects have no documentation attached to them. Documentation is in 
     ;; the doc objects. Thus, remove non-$DOCE MOSS objects
     (setq obj-id-list
            (remove nil
                    (mapcar #'(lambda (xx)
                                (cond 
                                 ((or (%type? xx '$DOCE) 
                                      (%type? xx '$FN)
                                      (%type? xx '$UNI))
                                  xx)
                                 ((eql (symbol-package xx) (find-package :moss))
                                  nil)
                                 (t xx)))
                            input)))
      ;; if no more objects then send excuse message
      (unless obj-id-list
        (mformat "Sorry, I have no information about that...")
        (throw :return '(:transition :failure)))
 
      ;; otherwise print first 5 values
      ;; print first object erasing screen
      (send (pop obj-id-list) '=get-documentation)
      (send conversation '=display-text *answer* :erase t :final-new-line t :newline t)
      ;; if no more quit
      (unless obj-id-list
        ;; transition to success
        (throw :return '(:transition :success)))
     
     ;; if more print more
      (dotimes (nn (min 4 (1- (length obj-id-list))))
          (send (nth nn obj-id-list) '=get-documentation)
        (send conversation '=display-text *answer* :final-new-line t :newline t))
     
      ;; if more than 5 we go to print more
      (if (> (length obj-id-list) 5)
        (progn
          (replace-fact conversation :results (subseq obj-id-list 5))
          (throw :return `(:transition ,_wc-display-more-results)))
        
        (progn
          ;(mformat "~%")
          ;; transition to success
          (throw :return '(:transition :success))))
      )))

;;;------------------------------------------------------------ (WC) DONT-UNDERSTAND

(defstate 
  _wc-dont-understand
  (:label "WC arguments not understood")
  (:explanation "we have some data but there is no entry point among them.")
  (:text "- I do not understand what you want to know. I can only give ~
            information about MOSS objects or MOSS procedures (HOW TO...).")
  (:question-no-erase
   "- About what MOSS object do you want information? (if you want ~
        to quit, simply type: none).")
  (:transitions
   (:contains ("nothing" "none" "abort" "forget it") :failure)
   (:always :target _wc-entry-state)))

;;;-------------------------------------------------------- (WC) DONT-UNDERSTAND-APP

(defstate 
  _wc-dont-understand-app
  (:label "WC arguments not understood in the application")
  (:explanation "we have some data but there is no entry point among them.")
  (:text "- I do not understand what you want to know. I cannot find relevant ~
            information in the application.")
  (:question-no-erase
   "- About what concept or object do you want information? (if you want ~
        to quit, simply say: none).")
  (:transitions
   (:contains ("nothing" "none" "abort" "forget it") :failure)
   (:always :target _wc-entry-state)))

;;;----------------------------------------------------------------- (WC) FIND-ENTRY

(defstate 
  _wc-find-entry-state
  (:label "Find entry of the WHAT dialog")
  (:explanation 
   "Initial state may have left data in FACT/INPUT. The data either represent ~
    an entry point or something else that should be used to build a query.")
  (:explanation 
   "examines the FACT/INPUT slot to extract possible entry points.")
  (:transitions
   (:patterns ((* "HELP" *))
              :replace ("HELP") :target _wc-access-ep) 
   (:test (let ((results (find-best-entries (read-fact conversation :input)
                                            :all t)))
            (replace-fact conversation :results results))
          :target _wc-access-ep)
   (:patterns ((* "none"  *)
               (* "forget"  *)
               (* "give" "up"  *))
              :failure)
   (:otherwise :target _wc-dont-understand))
  )
#|
;;;--------------------------------------------------------------------- (WC) SELECT

(defstate 
  _wc-select
  (:label "wc select")
  (:explanation "we have retrieved more than one object we ask user to select one.")
  (:ask-TCW :title "Select one of the objects"
            :choice-list (mapcar #'(lambda (xx) (list (send xx '=summary) xx)) results)
            :why "Select the object that you want documentation about.") 
  ;where do we put the result? in to-do, since it is a user's answer
  (:manual-resume))

(defownmethod =resume _wc-select (conversation &rest more-args)
  "reading answer to selection in the TO-DO slot of the conversation object. ~
   The content should be a number. 0 or anyhing else is equivalent to a cancel.
Arguments:
   conversation: current conversaion
   more-args (rest): ignored
Return:
   nothing."
  (declare (ignore more-args))
  (let* ((state-context (car (HAS-Q-CONTEXT conversation)))
         (candidates (HAS-RESULTS state-context))
         (input-text (caar (HAS-TO-DO conversation)))
         choice)
    ;; should clean the TO-DO
    (setf (HAS-TO-DO conversation) nil)
    ;; read input-text should be a number, if error return :cancel
    (format t "..... input-text: ~S: " input-text)
    (setq choice (read-from-string input-text :eof-error-p :cancel))
    (format t "..... choice key: ~S; candidates: ~S" choice candidates)
    (setq choice
          (if (and (numberp choice) (> choice 0)(<= choice (length candidates)))
            (list (nth (1- choice) candidates))
            candidates))
    (format t "..... choice value: ~S;~&  candidates: ~S " choice candidates)
    ;; if nil or cancel set transition to failure
    ;; could have a case for not-understood and retry
    ;; should also process the :why option
    (cond
     ((or (null choice)(eql choice :cancel))
      `(:transition ,_wc-dont-understand))
     ;; otherwise set result and declare success
     (t
      (setf (HAS-RESULTS state-context) choice)
      `(:transition ,_wc-display-results)))
    ))
|#
;;;================================================================================
;;;
;;;                             ELIZA CONVERSATION
;;;
;;;================================================================================

;;; this conversation is intended to do small talk.


(defParameter *eliza-rules*
  '((("hello")
     "- Hi I am MOSS.")
    (("bonjour")
     "Je suis désolé mon français n'est pas encore utilisable. Please use English."
     )
    (("who" * "Barthès" *)
     "Jean-Paul Barthès is an emeritus professor of artificial intellignece at UTC ~
      (Université de Technologie de Compiègne) in France."
     )
    (("who" * "Barthes" *)
     "Jean-Paul Barthès is an emeritus professor of artificial intelligence at UTC ~
      (Université de Technologie de Compiègne) in France.
      Note however that Barthès is written with an è."
     )
    (("forget" "it")
     "OK. No hard feelings.")
    (("thank" "you" *)
     "you are welcome.")
    (("how" "are" "you" *)
     "Very well, thank you."
     "Is my health really a concern to you?"
     "Could be better..."
     "My keys are aching, might be the weather...")
    ((* "computer" *)
     "Do computer worry you?"
     "What do you think about machines?"
     "Why do you mention computers?"
     "What do you think machines have to do with your problem?")
    ((* "stupid" *)
     "Please, behave!"
     "You think so."
     "I am pained by your remark.")
    ((?x "does" "not" "work" ?y)
     "Of course ?x works!"
     "Why are you saying that ?x does not work?")
    (("help")
     "you want help about what?")
    ((* "sorry" *)
     "Please don't apologize"
     "Apologies are not necessary"
     "What feelings do you have when you apologize?")
    ((* "I" "remember" ?y)
     "Do you often think of ?y"
     "Does thinking of ?y bring anything else to mind?"
     "What else do you remember?"
     "Why do you recall ?y right now?"
     "What in the present situation reminds you of ?y ?"
     "What is the connection between me and ?y ?")
    ((?x "do" "you" "remember" ?y)
     "Did you think I would forget ?y ?"
     "Why do you think I should recall ?y by now ?"
     "What about ?y ?"
     "You mentined ?y ?")
    ((?x "if" ?y)
     "Do you really think its likely that ?y ?"
     "Do you wish that ?y ?"
     "What do you think about ?y ?"
     "Really -- if ?y ?")
    (("what" * "do" "for" "me")
     "So many things..."
     "Currently I do not know."
     "What could please you?")
    
    ((* "I" "dreamt" ?y)
     "Really -- ?y"
     "Have you ever fantasized ?y while you were awake ?"
     "Have you dreamt ?y before ?")
    ((* "dream" "about" ?y)
     "How do you feel about ?y in reality ?")
    ((* "dream" *)
     "What does this dream suggest to you ?"
     "Do you dream often ?"
     "What persons appear in your dreams ?"
     "Don't you believe that dream has to do with your problem ?")
    ((* "my" "mother" ?y)
     "Who else in your family ?y ?"
     "Tell me more about your family")
    ((* "my" "father" *)
     "Your father ?"
     "Does he influence you strongly ?"
     "What else comes to mind when you think of your father ?")
    ((* "I" "want" ?y)
     "What would it mean if you got ?y ?"
     "Why do you want ?y ?"
     "Suppose you got ?y soon ?")
    ((* "I" "am" "glad" ?y)
     "How have I helped you to be ?y ?"
     "What makes you happy just now ?"
     "Can you explain why you are suddenly ?y ?")
    ((* "I" "am" "sad" *)
     "I am sorry to hear you are depressed"
     "I am sure it's not pleasant to be sad")
    ((?x "are" "like" ?y)
     "What resemblance do you see between ?x and ?y ?")
    ((?x "is" "like" ?y)
     "In what way is it that ?x is like ?y ?"
     "What resemblance do you see ?"
     "Could there really be some connection ?"
     )
    ((* "alike" *)
     "In what way ?"
     "What similarities are there ?")
    ((* "same" *)
     "What other connections do you see ?")
    
    ((* "I" "was" ?y)
     "Were you really ?"
     "Perhaps I already knew you were ?y"
     "Why do you tell me you were ?y by now ?")
    ((* "was" "I" ?y)
     "What if you were ?y ?"
     "Do you think you were ?y ?"
     "What would it meant if you were ?y ?")
    ((* "I" "am" ?y)
     "In what way are you ?y ?"
     "Do you want to be ?y ?")
    ((* "am" "I" ?y)
     "Do you believe you are ?y ?"
     "Would you want to be ?y ?"
     "You wish I would tell you you are ?y"
     "What would it mean if you were ?y ?")
    ((* "am" *)
     "Why do you say \"AM?\""
     "I don't understand that")
    ((* "are" "you" ?y)
     "Why are you interested in whether I am ?y or not ?"
     "Would you prefer if I weren't ?y ?"
     "Perhaps I am ?y in your fantasies")
    ((* "you" "are" ?y)
     "What makes you think I am ?y ?")
    ((* "because" *)
     "Is that the real reason ?"
     "What other reasons might there be ?"
     "Does that reason seem to explain anything else ?")
    ((* "were" "you" ?y)
     "Perhaps I was ?y"
     "What do you think ?"
     "What if I had been ?y ?")
    ((* "I" "can't" ?y)
     "Maybe you could ?y now"
     "What if you could ?y ?")
    ((* "I" "feel" ?y)
     "Do you often feel ?y ?")
    ((* "I" "felt" *)
     "What other feelings do you have ?")
    (((?* ?x) "I" (?* ?y) "you" (?* ?z))
     "Perhaps in your fantasy we ?y each other")
    (((?* ?x) "why" "don't" "you" (?* ?y))
     "Should you ?y yourself ?"
     "Do you believe I don't ?y ?"
     "Perhaps I will ?y in good time")
    ((* "yes" *)
     "You seem quite positive."
     "You are sure ?"
     "I understand.")
    ((* "no" *)
     "Why not ?"
     "You are being a bit negative"
     "Are you saying \"NO\" just to be negative?")
    
    ((* "someone" *)
     "Can you be more specific ?")
    ((* "everyone" *)
     "Surely not everyone."
     "Con you think of anyone in particular ?"
     "Who for example ?"
     "You are thinking of a special person ?")
    ((* "always" *)
     "Can you think of a specific example ?"
     "When ?"
     "What incident are you thinking of ?"
     "Really -- always ?")
    ((* "perhaps" *)
     "You do not seem quite certain")
    ((* "are" ?y)
     "Did you think there might not be ?y ?"
     "Possibly they are ?y")
    ((*)
     "I do not understand exactly what you want. You may type: help."
     "I am not sure I understand what you want. You may type: help."
     "Could you rephrase your question please?"
     "Sorry I did not get that. Could you rephrase it? Or type: help."
     "Oops I do not understand. Could you say that differently?"
     )))


;;; We should develop functions that display the state graph (using a grapher?)
;;; print the current state, or the content of the project, etc.


;;;=============================================================================== 
;;;   
;;;                               TASK MODEL
;;;
;;;===============================================================================

;;; Sub-dialogs are triggered by tasks whose purpose is to give some sort of
;;; information to the user. 
;;; the model of MOSS task is defined in the moss-dialog-class file.

;;;=============================================================================== 
;;;   
;;;                               TASK LIBRARY
;;;
;;;=============================================================================== 

;;; it contains the following tasks:
;;;   - explain: gives a definition of a concept
;;;   - how to: shows how to do something (create, modify, delete, print, ...)
;;;   - example: gives an example of something
;;;   - help: gives general help
;;;   - trace: switches traces on and off
;;;   - fonts: changes the font size
;;;   - ...

;;;======================================================================= EXPLAIN 

(deftask "what is xxx?"
    :doc "Task to explain the meaning of a concept in the ontology (question)."
  :dialog _what-conversation
  :indexes ("what is" .4 "what are" .5 "explain" .6 "define" .6 "means" .4 
            "meaning" .4)
  )
  
(deftask "define concept"
    :doc "Task to explain the meaning of a concept in the ontology (command)."
  :dialog _what-conversation
  :performative :command
  :indexes ("explain" .6 "define" .6)
  )

;;;========================================================================== HELP

(deftask "help"
    :doc "Task to print some general help (request)."
  :dialog _print-help-conversation
  :indexes ("can you do" .4 "can I do" .4 "help" .7)
  )

(deftask "help"
    :doc "Task to print some general help (command)."
  :performative :command
  :dialog _print-help-conversation
  :indexes ("can you do" .4 "can I do" .4 "help" .7)
  )

;;;=========================================================================== HOW 

(deftask "how requests"
    :doc "Task to answer how questions."
  :dialog _how-conversation
  :indexes ("can you show" .4 "how" .4)
  )

;;;===================================================================== SET FONTS 

#|
(deftask "set font size"
    :doc "Task to change the size of the dialog font, increasing it, or returning ~
             to the default value."
  :performative :command
  :dialog _set-font-size-conversation
  :indexes ("fonts" .5 "font" .5 "text" .3 "output" .3 "read" .3 "letters" .3)
  )
|#

;;;================================================================== SHOW EXAMPLE 

(deftask "show example"
    :doc "Task to show an example of MOSS objects."
  :performative :command
  :dialog _show-example-conversation
  :indexes ("show me" .4 "show" .3 "example of" .7 "example" .5 "give me" .3)
  )

;;;========================================================================= TRACE

#|
(deftask "set trace"
    :doc "Task to switch on and off the dialog trace."
  :performative :command
  :dialog _trace-conversation
  :indexes ("transition" .5 "transitions" .5 "trace" .5 "verbose" .5)
  )
|#

;;;=============================================================================== 

;(format t "~%;*** MOSS v~A - Moss dialogs loaded ***" *moss-version-number*)

:EOF