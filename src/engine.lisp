;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;19/08/24
;;; 
;;; 		     E N G I N E -  (File moss-engine.lisp)
;;;
;;; This file contains basic version of MOSS to be used with CCL
;;; MOSS is a set of message passing functions implementing multiple 
;;; inheritance
;;;==========================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
2019
 0824 copied from the ACL counterpart
|#

;;;================================= Methods ================================
;;; Functions implementing methods are given unique names, e.g.
;;;  $TIT=S=0=MAKE-ENTRY, $TIT=I=0=MAKE-ENTRY, or *0=MAKE-ENTRY
;;; Thus, when looking for a method associated with an object, it is simple
;;; to check whether the corresponding function name is bound to a lambda 
;;; expr. If so, we found the method.
;;;==========================================================================

(in-package "MOSS")


#|
(eval-when (compile load eval)
  (export '(send send-no-trace ton toff trace-message untrace-message *self*
                 trace-object untrace-object trace-method untrace-method untrace-all
                 broadcast *answer* g==> g-> s->)))
|#

;            s-> s--> s=> s==> g-> g--> g=> g==> h-> h--> h=> h==> p-> p--> p=> p==>)))

#|
(format *standard-output* "~&;*** Loading moss-engine version ~A ..." 
  *moss-version-number*)
|#

;;; All global variables are defined in the moss3macro.lisp file

;;;------------------------------------------------------------------------ SEND

;;; Magic SEND 

(defUn send (object-id method-name &rest method-args &aux answer)
  "The send function complete with tracing and all. May return multiple values."
  (declare (special *trace-flag* *trace-message* *print-level* *print-length*
                    *print-pretty* *trace-level* *self* *answer*))
  ;; distinguish tracing and non tracing cases
  ;(if (and *trace-flag* (lob-typep object-id)
  (if (and *trace-flag* (%pdm? object-id)
           (or *trace-message* (<<get object-id :TRACE) 
               ;; method-name is allowed to be a string!
               (and (symbolp method-name)(get method-name :TRACE))))
      
      (let ((*print-level* 2)(*print-length* 5)(*print-pretty* nil) )
        (incf *trace-level*)
        (tformat "from: ~A to: ~A, request: ~A arg-list: ~S"
                 *self* object-id method-name method-args)
        
        ;; then call the guy that does the job, return values as a list
        (setq answer 
              (multiple-value-list
               (apply #'send-no-trace object-id method-name method-args)))
        
        (tformat "~A for: ~A, to: ~A returns: ~S"
                 *self* method-name object-id answer)
        (decf *trace-level*))
    
    ;; otherwise simply call send-no-trace
    ;(setq answer (apply #'send-no-trace object-id method-name method-args)))
    (setq answer
          (multiple-value-list
           (apply #'send-no-trace object-id method-name method-args))))
  
  ;; finally return values (*answer* is set in send-no-trace)
  (values-list answer))

;;;-------------------------------------------------------------------------- =>

(defUn => (&rest arg-list)
  "shorthand for send. Used to be a macro, but we can't apply macros."
  (apply #'send arg-list))

;;;--------------------------------------------------------------- SEND-NO-TRACE
;;; send-no-trace was defined in moss-boot to keep the compiler quiet
(fmakunbound 'send-no-trace)
#|
(dformat-set :snt 0)
(dformat-reset :snt)
|#
(defUn send-no-trace (object-id method-name &rest args)
  "Now the guy that does all the dirty job. The receiver must not ve NIL ~
   and should be alive. The receiver id is saved in the process self varaible, ~
   the method-name in the method-name slot, previous self in sender slot. ~
   Args are passed to the ~
   method and need not be saved. 
Arguments:
   object-id: id or id pair of object receiving the message
   method-name: name of the method, if a string will be interned into the current
                package
   args: list of arguments to the method
Return:
   nil if error, the result of applying method to arguments otherwise, possibly ~
     as multiple-values."
  (declare (special *self* *answer*))
  (drformat :snt 0 "~2%;---------- Entering send-no-trace")
  
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    (dformat :snt 0 "*context*: ~S" context)
    ;; if object is unbound or nil and database is active, try to load it
    ;; <<* is same as symbol-value for symbols and works for pair ids using *ST*
    ;; a NIL answer means that either the arg is unbound or NIL
    (unless (<<boundp object-id)
      ;; %ldif returns the object ID set or unchanged
      (%ldif object-id))
    
    ;; if still NIL object is illegal
    (unless (<<* object-id)
      (warn "object with id: ~S is illegal or unbound object *** Method: ~S, sender: ~S, ~
               arg-list: ~S, package: ~S, context ~S"
        object-id method-name *self* args (package-name *package*) context)
      (return-from send-no-trace (setq *answer* nil)))
    
    ;;... as well as objects with a tombstone
    (unless (%alive? object-id context)
      ;; we do not signal it unless we trace what is going on, because when using
      ;; versions we test a lot of objects that do not exist in a particular version
      (if *verbose*
          (warn "object with id: ~S is dead or does not exist in context ~S ~
          and package ~S when trying to apply method ~S, with arg-list: ~S"
             (>>f object-id) context (package-name *package*) method-name args))
      (return-from send-no-trace (setq *answer* nil)))
    
    ;; otherwise process message
    (let ((*method-name* (if (stringp method-name) 
                             (intern (string-upcase method-name))
                           method-name))
          (*sender* *self*)
          (*self* object-id)
          answer method-fn)
      (declare (special *method-name* *sender* *self* *answer*))
      
      ;; try to obtain method within the model "plane", otherwise ask meta
      ;; model. Check for fancy or standard lexicographic inheritance;
      (setq method-fn (get-method *self* *method-name* context))
      (dformat :snt 0 "method-fn: ~S" method-fn)
      
      ;; when we have a method we apply it
      ;; before applying it and if object is a class, then we cache it
      (if method-fn 
          (progn
            (if (and *cache-methods* (%is-model? object-id))
                (%putm *self* method-fn *method-name* context))
            ;; we apply the method by using its internal function-name, since
            ;; the method was compiled the first time around
            ;; use multiple-value-list in case the method returns several values
            ;;== we must be careful about args: if it is an object-pair, then it
            ;; looks like a list; in that case make a list of 1 arg
            (if (%%is-id? args) (setq args (list args)))
            (setq answer (multiple-value-list (apply method-fn args))))
        ;; if not we send a message back to the object
        (send-no-trace object-id '=unknown-method method-name))
      
      ;; set global variable just before leaving
      (dformat :snt 0 "*answer*: ~S" (car answer))
      (setq *answer* (car answer))
      ;; turn the list into multiple values
      (values-list answer))))

#|
;; requires kernel
(send-no-trace 'moss::$ENT '=get 'HAS-moss-attribute)
($ENAM $RDX $DOCT $ONEOF $XNB $TMBT)
*answer*
($ENAM $RDX $DOCT $ONEOF $XNB $TMBT)

(with-package :test
  (send '$E-person '=get 'has-moss-attribute))
(TEST::$T-PERSON-NAME TEST::$T-PERSON-FIRST-NAME)
|#
;;;-------------------------------------------------------------------------- ->
;;; Short form for send-no-trace

(defUn -> (&rest arg-list) 
  "Synonymous for send-no-trace" 
  ;(format t "~%;-- -> (sv (intern ...)): ~S" (symbol-value (intern "*CONTEXT*")))
  (apply #'send-no-trace arg-list))

;;;------------------------------------------------------------------ SEND-SUPER

;;; (send-super method arg-list) - Gets method from ancestors of model of
;;; *self*, hence can only be used in methods. One can thus build more
;;; complex methods re-using part of older ones.

;;; 5/89 ***** Watch it! send-super should not ignore $IS-A links at instance
;;; level and give a try to a =inherit-own if a link is defined.
;;; 2) what happens if no method is found (=unknown-method?)

(defUn send-super (method-name &rest arg-list)
  "Gets method from ancestors of model of *self* - Allows thus to build more ~%
   complex methods by reusing old ones.
Arguments:
   method-name: name of the requested method
   arg-list: arguments to the method
Return:
   applies the retrieved method to the object if found."
  (let* ((type-list (%get-value *self* '$TYPE))
         (context (symbol-value (intern "*CONTEXT*")))
         method-id method-function-name precedence-list)
    ;; get lexicographic list of ancestors
    (setq precedence-list (%compute-precedence-list type-list))
    ;; remove direct classes to avoid looping
    (dolist (class-id type-list)
      (setq precedence-list (remove class-id precedence-list)))
    
    ;; look for ancestor classes in turn
    (dolist (class-id precedence-list)
      (cond
       ;; is the method cached?
       ((setq method-function-name (%getm class-id method-name context :instance))
        ;; if so OK
        (return-from send-super
          (setq *answer* (apply method-function-name arg-list))))
       
       ;; try current ancestor class
       ((setq method-id (%has-method class-id method-name :instance context))
        ;; OK we found it locally; now get the associated function name
        (setq method-function-name (car (%get-value method-id '$FNAM context)))
        ;; if not there, error
        (unless method-function-name
          (error "method function name missing for method with id: ~S" method-id))
        ;; return the name of the function implementing the method
        (return-from send-super
          (setq *answer* (apply method-function-name arg-list))))))
    ))

#|
;; requires kernel for testing
|#
;;;------------------------------------------------------------------- BROADCAST

;;; Broadcast macro: sends a message to all receivers specified in 1st arg
;;; Collects the answers as they come back
;;; Should be redefined as a defun...

(defUn broadcast (callees &rest args)
  "Send a message to a list of receivers
Arguments:
   callees: list of object ids
   args (rest): arguments to the method
Return:
   the list of each object answer."
  (mapcar #'(lambda(xx) (apply #'send xx args)) callees))


;;;=============================================================================
;;; Now the functions that are responsible for getting the rigth method
;;; Watch: *object-id must be different from *self*
;;; This is the up-to-date multiple inheritance version
;;; When methods are compiled on p-list it is done as follows;
;;;	(=print-self (:own (nn <int-function name>)*) (:instance  <...>))
;;; where nn is a version number
;;;=============================================================================

;;;---------------------------------------------------------------- CHECK-METHOD
;;; unused, useless since get-method checks for *lexicographic-inheritance*

;;;(defUn check-method (obj-id method-name context)
;;;  "checks if the object has the mentioned method.
;;;Arguments:
;;;   obj-id: identifier of the object
;;;   method-name: name of the method, e.g. =if-needed
;;;   context: specified context
;;;Return:
;;;   the name of the internal corresponding function or nil."
;;;  (if *lexicographic-inheritance*
;;;    (lex-get-method obj-id method-name context)
;;;    (get-method obj-id method-name)))

;;;#|
;;;(moss::check-method _jpb '=get)
;;;MOSS::*0=GET
;;;
;;;(moss::check-method '$T-PERSON-AGE '=if-needed)
;;;$T-PERSON-AGE=S=0=IF-NEEDED
;;;
;;;(moss::check-method '$T-PERSON-BIRTH-YEAR '=if-needed)
;;;NIL
;;;|#
;;;------------------------------------------------------------------ GET-METHOD
;;; get-method should look for a method in the package of the object. Is it 
;;; reasonable to look for a method in the MOSS package?
;;; NO, this prevents to use methods from the MOSS package like =get-instance-list
;;; used by the QUERY system

(defUn get-method (object-id method-name &optional version)
  "Gets the method for the object.
Arguments:
   object-id: id or id pair of an object
   method-name: a symbol or string specifying the method: e.g. =summary
Return:
   the name of the function implementing the method, e.g. $T-TESTA-ATTA=S=0=XI"
  (declare (special *lexicographic-inheritance*))
           ;(inline lex-get-method))
  
  (let ((context (or version (symbol-value (intern "*CONTEXT*")))))
    ;; apparently the next commented lines were there to avoid the case when an
    ;; agent would use a method from the package of another agent. However, this
    ;; prevents the agent from using internal MOSS methods that are not exported
    ;; on objects in a package different from MOSS

    ;; get package of object
;;;    (setq package
;;;          (cond
;;;           ((and object-id (symbolp object-id))(symbol-package object-id))
;;;           ((%%is-id? object-id) (symbol-package (car object-id)))
;;;           ((error "get-method: object-id ~S is not a valid id nor id pair"
;;;              object-id))))
    ;; check package of method-name
;;;    (cond 
;;;     ((and method-name (symbolp method-name))
;;;      (setq method-name (intern (symbol-name method-name) package)))
;;;     ((stringp method-name)
;;;      (setq method-name (intern (string-upcase method-name) package)))
;;;     ((error "get-method: method-name ~S should be a symbol or a string"
;;;        method-name))
;;;     )
    
    (cond
     ;; if we are using lexicographic inheritance, use the proper function
     (*lexicographic-inheritance*
      (lex-get-method object-id method-name context))
     (t
      (error "fancy inheritance is currently disabled...")
      ))))

#|
(get-method '$ENAM '=make-entry 0)
$ENAM=S=0=MAKE-ENTRY

(get-method '$ENAM '=summary 0)
$EPT=I=0=SUMMARY

(get-method '$ENAM '=print-self 0)
*0=PRINT-SELF

_has-name
$T-NAME

_has-person-name
$T-PERSON-NAME

(get-method _has-person-name '=make-entry 0)
DEFAULT-MAKE-ENTRY

(get-method _has-person-name '=summary 0)
$EPT=I=0=SUMMARY
|#
;;;-------------------------------------------------------------- %GET-OWNMETHOD

;;; Methods are recorded locally under a unique identifier on the p-list
;;; of each object when they are brought down the first time around

;;;(defUn %get-own-method (object-id method-name)
;;;  "Tries to obtain a own method in a given context"
;;;  (declare (special *features* *cache-methods*))
;;;  (let ((context (symbol-value (intern "*CONTEXT*")))
;;;        (none-symbol (intern "*NONE*"))
;;;        %method% method-id)
;;;    (cond 
;;;     ;; check if object-id is OK, if null no method
;;;     ((null object-id)(return-from %get-own-method nil))
;;;     ;; check if compiled on property list
;;;     ((setq %method% (%getm object-id method-name context :own))
;;;      (return-from %get-own-method (if (eq %method% none-symbol) nil %method%))
;;;      )
;;;     ;; check if little guy knows what to do
;;;     ((setq %method% (%has-method object-id method-name :own context))	
;;;      ;; OK we found it locally; now we cook up a unique identifier
;;;      (setq method-id (intern (make-name object-id context method-name))) ;???
;;;      ;; and record code
;;;      (set method-id %method%)
;;;      ;; compile method and put it on the p-list
;;;      (if (and (member :microsoft-32 *features*)  ; jpb0410
;;;               (not (member :compiler *features*)))
;;;        (setq method-id %method%)
;;;        (compile method-id %method%))
;;;      (when (and *cache-methods* (%is-model? object-id))
;;;        (%putm object-id method-id method-name context :own))
;;;      (return-from %get-own-method method-id)
;;;      )
;;;     ;; ask ancestor when at a loss
;;;     ;; when no ancestor return nil immediately
;;;     ((null (%parents object-id)) 
;;;      (when (and *cache-methods* (%is-model? object-id))
;;;        (%putm object-id none-symbol method-name context :own))
;;;      (return-from %get-own-method nil)
;;;      )
;;;     ;; Now we have ancestors, and we want to inherit method from them.
;;;     ;; Since we have multiple inheritance, we must have make a decision
;;;     ;; in case of conflict (multiple ancestors). We do that by using
;;;     ;; some local ordering an the way we traverse the inheritance lattice.
;;;     ;; to be more flexible, the ordering is implemented as a special
;;;     ;; =inherit-own method which may be attached to the object locally
;;;     ;; and which specifies how other methods are inherited.
;;;     ;; ask ancestor when at a loss
;;;     ((eq method-name '=inherit-own)	;***was inherit-own?
;;;      (let ((ancestor-list (%parents object-id)))
;;;        (while ancestor-list
;;;          (setq method-id (%get-own-method 
;;;                           (pop ancestor-list) method-name))
;;;          (when method-id 
;;;            (when (and *cache-methods* (%is-model? object-id))
;;;              (%putm object-id method-id method-name context :own))
;;;            (return-from %get-own-method method-id))
;;;          )
;;;        ))
;;;     ;; Otherwise try to inherit from ancestors
;;;     ((setq method-id (-> object-id '=inherit-own method-name))
;;;      ;; when we got one, then we record it
;;;      (when (and *cache-methods* (%is-model? object-id))
;;;        (%putm object-id method-id method-name context :own))
;;;      (return-from %get-own-method method-id)
;;;      )
;;;     ;; Here we did not get anything, so we record it
;;;     ((and *cache-methods* (%is-model? object-id))
;;;      (%putm object-id none-symbol method-name context :own)
;;;      (return-from %get-own-method  nil)
;;;      )
;;;     )))

;;;---------------------------------------------------- %GET-ISA-INSTANCE-METHOD

;;; Here function to get instance method of first object having a class in
;;; the is-a hierarchy of a classless object. This resembles the get-own-method
;;; with the difference that we branch up to class level as soon as we encounter
;;; an ancestor which has a class

;;;(defUn %get-isa-instance-method (object-id method-name)
;;;  "Tries to obtain an instance method for a classless object from an ancestor"
;;;  (declare (special *cache-methods*))
;;;  (let ((context (symbol-value (intern "*CONTEXT*")))
;;;        (none-symbol (intern "*NONE*"))
;;;        %method% method-id)
;;;    (cond 
;;;     ;; check if object-id is OK, if null no method
;;;     ((null object-id)(return-from %get-isa-instance-method nil))
;;;     ;; check if compiled on property list
;;;     ((setq %method% 
;;;            (%getm object-id method-name context :instance))
;;;      (return-from %get-isa-instance-method 
;;;        (if (eq %method% none-symbol) nil %method%))
;;;      )
;;;     ;; if the object is not a classless type then ask its classes in turn
;;;     ((not (eql (car(%get-value object-id '$TYPE)) none-symbol))
;;;      (let ((type-list (%get-value object-id '$TYPE))
;;;            method-id
;;;            )
;;;        (while (or type-list (null method-id))
;;;          (setq method-id 
;;;                (%get-instance-method (car type-list) method-name context))
;;;          (pop type-list)
;;;          )
;;;        method-id))		
;;;     ;; otherwise, ask ancestor
;;;     ;; when no ancestor return nil immediately
;;;     ((null (%parents object-id)) 
;;;      (when (and *cache-methods* (%is-model? object-id))
;;;        (%putm object-id none-symbol method-name context :instance))
;;;      (return-from %get-isa-instance-method nil)
;;;      )
;;;     ;; If early in the game we are looking for the =inherit-own method then
;;;     ;; we want to be able to obtain it from a class but we cannot use it
;;;     ;; to traverse the orphan is-a lattice, so we must resolve to traverse
;;;     ;; the lattice depth first.
;;;     ;; The same is probably true for =inherit-isa-instance, since if we
;;;     ;; cannot find =inherit-isa-instance in the local transitive closure
;;;     ;; using the =inherit-own default method, then we are going to look
;;;     ;; at the class level, and to do so invokes it recursively.
;;;     ((or (eq method-name '=inherit-own)
;;;          (eq method-name '=inherit-isa-instance)
;;;          )
;;;      (let ((ancestor-list (%parents object-id)))
;;;        (while ancestor-list
;;;          (setq method-id 
;;;                (%get-isa-instance-method 
;;;                 (pop ancestor-list) method-name))
;;;          (when method-id 
;;;            (when (and *cache-methods* (%is-model? object-id))
;;;              (%putm object-id method-id method-name context :instance))
;;;            (return-from %get-isa-instance-method method-id)
;;;            )
;;;          )
;;;        ))
;;;     ;; otherwise try to inherit method from ancestors using the own-link??
;;;     ((setq method-id (-> object-id '=inherit-isa-instance method-name))
;;;      ;; when we get one we record it if object is a class
;;;      (when (and *cache-methods* (%is-model? object-id))
;;;        (%putm object-id method-id method-name context :instance))
;;;      (return-from %get-isa-instance-method method-id) 	
;;;      )
;;;     ;; Here we could not find anything, thus we record it for classes
;;;     ((and *cache-methods* (%is-model? object-id))
;;;      (%putm object-id none-symbol method-name context :instance)
;;;      (return-from %get-isa-instance-method nil)
;;;      )
;;;     )))

;;;-------------------------------------------------------- %GET-INSTANCE-METHOD
;;; Now function for getting regular instance methods

;;;(defUn %get-instance-method (object-id method-name context)
;;;  "try to obtain an instance method"
;;;  (let (%method% method-id)
;;;    (cond 
;;;     ;; check if object-id is OK, if null no method
;;;     ((null object-id)(return-from %get-instance-method  nil))
;;;     ;; check if recorded on property list, or unavailable
;;;     ((setq method-id (%getm object-id method-name context :instance))
;;;      (return-from %get-instance-method (if (eq method-id '*none*) nil method-id))
;;;      )
;;;     
;;;     ;; check if little guy knows what to do
;;;     ((setq %method% (%has-method object-id method-name :instance context))	
;;;      ;; OK we found it locally; now we cook up a unique identifier
;;;      (setq method-id (intern (make-name object-id context method-name)))
;;;      ;; and record code
;;;      (set method-id %method%)
;;;      ;; compile method and put it on the p-list
;;;      (if (and (member :microsoft-32 *features*)  ; jpb0410
;;;               (not (member :compiler *features*)))
;;;        (setq method-id %method%)
;;;        (compile method-id %method%))
;;;      (when (and *cache-methods* (%is-model? object-id))
;;;        (%putm object-id method-id method-name context :instance))
;;;      (return-from %get-instance-method  method-id)
;;;      )
;;;     
;;;     ;; ask ancestor when at a loss
;;;     ;; when no ancestor return nil after having recorded the fact
;;;     ((null (%parents object-id)) 
;;;      (when (and *cache-methods* (%is-model? object-id))
;;;        (%putm object-id '*none* method-name context :instance))
;;;      (return-from %get-instance-method  nil)
;;;      )	
;;;     
;;;     ;; Now if we are looking for an =inherit method, and if we are not
;;;     ;; careful we'll end up in a loop, so we organize a depth first local
;;;     ;; strategy
;;;     ((eq method-name '=inherit-instance)	;***was inherit-own?
;;;      (let ((ancestor-list (%parents object-id)))
;;;        (while ancestor-list
;;;          (setq method-id (%get-instance-method 
;;;                           (pop ancestor-list) method-name context))
;;;          (when method-id 
;;;            (when (and *cache-methods* (%is-model? object-id))
;;;              (%putm object-id method-id method-name context :instance))
;;;            (return-from %get-instance-method  method-id))
;;;          )
;;;        ))
;;;     ;; Otherwise try to inherit from ancestors
;;;     ((setq method-id (send-no-trace object-id '=inherit-instance 
;;;                                     method-name))
;;;      ;; when we got one, then we record it
;;;      (when (and *cache-methods* (%is-model? object-id))
;;;        (%putm object-id method-id method-name context :instance))
;;;      (return-from %get-instance-method  method-id)
;;;      )
;;;     ;; Here we did not get anything, so we record it
;;;     ((and *cache-methods* (%is-model? object-id))
;;;      (%putm object-id '*none* method-name context :instance)
;;;      (return-from %get-instance-method nil)
;;;      )
;;;     )))

;;;#|
;;;(%get-instance-method '$ENT '=get-instances 0)
;;;$ENT=I=0=GET-INSTANCES
;;;|#
;;;------------------------------------------------------- %GET-UNIVERSAL-METHOD

;;; We record universal methods as own-methods when found

(defUn %get-universal-method (object-id method-name context)
  "try to obtain a universal method"
  (declare (special *features* *cache-methods*))
  (let (method-function-name method-id)
    (cond
     ;; check if it a universal method first on prop-list
     ((setq method-id (%getm method-name method-name context :universal))
      ;; record it as own method. Hence universal methods
      ;; are default own-methods
      (when *cache-methods* 
        (%putm object-id method-id method-name context :own))
      (return-from %get-universal-method method-id)
      )
     
     ;; then check on object list - we assume universal methods are in core
     ((setq method-id (car (%get-value method-name (%inverse-property-id '$UNAM))))
      ;; OK we found it locally; now get the associated function name
      ;; BUG: won't work if not in context 0
      ;(setq method-function-name (car (%%has-value method-id '$FNAM context)))
      (setq method-function-name (car (%get-value method-id '$FNAM context)))
      ;; if not there, error
      (unless method-function-name
        (error "method function name missing for method with id: ~S" method-id))
      
      ;; otherwise, check if function already built
      (unless (lambdap method-function-name)
        ;; if not, reassemble it from object content
        (set method-function-name (%build-function method-id)))
      ;; compile method unless compiler is no longer there (birrrkkkh!)
      (unless (and (member :microsoft-32 *features*)  ; jpb0410
                   (not (member :compiler *features*)))
        (compile method-function-name (symbol-value method-function-name)))
      ;; and record it
      ;; *** as done there it is wrong since context is not recorded
      (%putm method-name method-function-name method-name context :universal)
      
      ;; record it as own method. Hence universal methods
      ;; are default own-methods
      (when (and *cache-methods* (%is-model? object-id))
        (%putm object-id method-function-name method-name context :own))
      ;; and return it
      (return-from %get-universal-method method-function-name)
      )
     
     ;; if not found we simply return nil, in which case *none* will
     ;; be signalled on the p-list of the calling object coming from
     ;; the fact that no particular own-method was found for that name
     )))

#|
(defuniversalmethod =vvv () (throw :return "=vvv executing" ))
$UNI.2
(($TYPE (0 $UNI)) ($ID (0 $UNI.2)) ($UNAM (0 =VVV)) ($DOCT (0 "=uuu executing"))
 ($FNLS.OF (0 $SYS.1)) ($FNAM (0 *0=VVV)))

(%get-universal-method _jpb '=vvv 0)
*0=VVV

(get-method _jpb '=vvv)
*0=VVV

(send-no-trace _jpb '=vvv)
"=vvv executing"
|#


;;;=============================================================================
;;;
;;;             Functions for LEXICAL INHERITANCE
;;;
;;;=============================================================================

;;; Here we implement a simple lexicographic inheritance scheme which bypasses
;;; the fancy inheritance scheme.

;;;-------------------------------------------------------------- LEX-GET-METHOD
#|
(dformat-set :lgm 0)
(dformat-reset :lgm)
|#

(defUn lex-get-method (object-id method-name context)
  "Gets the method for the object using lexicographic inheritance.
Arguments:
   object-id: object that received the message
   method-name: method id
   context: integer specifying the context
Return:
   NIL if object-id is nil, method is not in core or on disk, "
  (drformat :lgm 0 "~2%;---------- Entering lex-get-method")
  (dformat :lgm 0 "*package*: ~S" *package*)
  (dformat :lgm 0 "context: ~S" context)
  (dformat :lgm 0 "object-id: ~S" object-id)
  
  (cond
   ;; if object is nil, quit
   ((null object-id) nil)
   
   ;; if object not alive in this context, quit
   ((not (%alive? object-id context))
    (warn "Can't get method ~S for object ~S in package ~S context ~S, ~
           object is not alive in this context."
      method-name object-id (package-name *package*) context)
    nil)
   
   ;; look if the method has been cached using method-name as a label
   (((lambda (xx)(if (eq xx '*none*) nil xx))
     (%getm  object-id method-name context)))
   
   ;; do the same with :own methods
   (((lambda (xx)(if (eq xx '*none*) nil xx))
     (%getm object-id method-name context :own)))
   
   ;; before trying to inherit verify if method exists (usual or universal)
   ;; *** method-name could have to be called from disk
   ((prog1 nil
      (dformat :lgm 0 "method-name: ~S" method-name)
      (dformat :lgm 0 "%pdm method-name?: ~S" (%pdm? method-name))))
   
   ((not (%pdm? method-name)) nil)
   
   ;; if method name is not an entry of a method or universal method, return nil
   ((null 
     (or (%get-value method-name (%%make-id :inv :prop-id '$MNAM) context)
         (%get-value method-name (%%make-id :inv :prop-id '$UNAM) context)))
    nil)
   
   ;;===== Look for OWN-METHOD
   ((%%lex-get-own-method object-id method-name context))
   
   
   ;; When object is an orphan, we check if one of the prototypes is an instance
   ;; of a class, in which case we try to inherit an instance method
   ((and (%is-orphan? object-id)
         (%%lex-get-isa-instance-method object-id method-name context)))
   
   ;;===== Look for INSTANCE-METHOD
   ((and 
     (not (%is-orphan? object-id))
     (%%lex-get-instance-method object-id method-name context)))
   
   ;;===== Look for UNIVERSAL-METHOD
   ((%get-universal-method object-id method-name context))
   
   ;; Here we could not find method, hence we record the fact associated to the
   ;; method-name
   ((and *cache-methods* (%is-model? object-id))
    (%putm object-id '*none* method-name context)
    ;; then return nil
    nil)
   ))

#|
;;;===== MOSS package

(lex-get-method '$enam '=make-entry 0)
DEFAULT-MAKE-ENTRY

(catch :error
       (defownmethod =summary $enam () (has-moss-entity-name)))
$FN.12

(lex-get-method '$enam '=summary 0)
$ENAM=S=0=SUMMARY

(lex-get-method '$enam '=print-self 0)
*0=PRINT-SELF

(lex-get-method '$enam '=zzz 0)
NIL

;;;===== TEST package

(catch :error
       (with-package :test
         ;(defconcept "Person" (:att "name" (:entry)))
         (definstmethod =summary test::$e-person ()(test::has-name))))
(TEST::$E-FN . 5)

(<<< $E-FN 5)
(($TYPE (0 TEST::$E-FN)) ($ID (0 (TEST::$E-FN . 5))) ($MNAM (0 TEST:=SUMMARY))
 ($FNLS.OF (0 (TEST::$SYS . 1))) ($IMS.OF (0 TEST::$E-PERSON))
 ($FNAM (0 TEST::$E-PERSON=I=0=SUMMARY)))
(:PDM T)
T

test::$E-PERSON
(($TYPE (0 $ENT)) ($ID (0 TEST::$E-PERSON)) ($ENAM (0 ((:EN "Person"))))
 ($RDX (0 TEST::$E-PERSON)) ($ENLS.OF (0 (TEST::$SYS . 1)))
 ($CTRS (0 TEST::$E-PERSON.CTR)) ($PT (0 TEST::$T-PERSON-NAME))
 ($IMS (0 (TEST::$E-FN . 5))))

(with-package :test
  (defindividual "Person" (:var test::_tpa)("name" "Oscar"))
  (lex-get-method test::_tpa 'test::=summary 0))
TEST::$E-PERSON=I=0=SUMMARY
|#
;;;-------------------------------------------------------- %%LEX-GET-OWN-METHOD

(defUn %%lex-get-own-method (object-id method-name context)
  "Tries to obtain an own method in a given context lexicographically.
   If the object is a class and *cache-methods* is true then the name of ~
   the internal function implementing the method is cached onto the ~
   p-list of the object-id. 
Arguments:
   object-id: the id of current object
   method-name: name of the requested method
Return:
   name of the internal function implementing the method or nil."
  (declare (special *cache-methods*))
  (let (method-function-name method-id precedence-list)
    ;; get precedence list
    (setq precedence-list (%compute-precedence-list object-id))
    
    (dolist (id precedence-list)
      ;; try to get method object-id locally
      (setq method-id (%has-method id method-name :own context))
      (when method-id
        ;; OK we found it locally; now get the associated function name
        ;;********** if we are not in context 0, then %%has-value will fail
        (setq method-function-name (car (%%get-value method-id '$FNAM context)))
        ;; if not there, error
        (unless method-function-name
          (error "method function name missing for method with id: ~S" method-id))
        ;; otherwise, return method name
        (return-from %%lex-get-own-method method-function-name)))
    
    ;; Here we did not get anything, so we record it
    (when (and *cache-methods* (%is-model? object-id))
      (%putm object-id (intern "*NONE*") method-name context :own)
      nil)))

#|
(%%lex-get-own-method '$ENAM '=make-entry 0)
DEFAULT-MAKE-ENTRY

(with-package :test
  (%%lex-get-own-method '$ENAM '=make-entry 4))
DEFAULT-MAKE-ENTRY

(defobject ("name" "Albert")(:var _aa))
*NONE*.1
(defobject ("name" "George")(:is-a _aa)(:var _gg))
*NONE*.2

(defownmethod =zzz _aa () (print 'Hello))
$FN.16

(symbol-plist _aa)
(=ZZZ ((:OWN (0 *NONE*.1=S=0=ZZZ))) :PDM T)

(symbol-plist _gg)
(:PDM T)

(%%lex-get-own-method _gg '=zzz :context 0)
$0-6=S=0=ZZZ
BUG***********
|#
;;;----------------------------------------------- %%LEX-GET-ISA-INSTANCE-METHOD 

(defUn %%lex-get-isa-instance-method (object-id method-name context)
  "Tries to obtain an instance method for a classless object from an ancestor ~
   lexicographically.
Arguments:
   object-id: current object
   method-name: name of the requested method
   context: context of the call
Return:
   name of the internal function implementing the method or nil."
  (declare (special *cache-method*))
  (when object-id 
    (let (method-function-name method-id precedence-list class-list)
      ;; check if compiled on property list (only for classes
      (setq method-function-name (%getm object-id method-name context :instance))
      (if method-function-name 
          (return-from %%lex-get-isa-instance-method  
            (if (eq method-function-name (intern "*NONE*")) 
                nil 
              method-function-name)))
      
      ;; get precedence list (ancestors of object)
      (setq precedence-list (%compute-precedence-list object-id))
      
      (dolist (current-object-id precedence-list)
        ;; get type
        (setq class-list (%get-value current-object-id '$TYPE context))
        ;; if the object is an instance, then ask its classes in turn
        (unless (eql (car class-list) (intern "*NONE*"))
          (setq method-id 
                (%%lex-get-instance-method current-object-id method-name context))
          (when method-id 
            (when (and *cache-methods* (%is-model? object-id))
              (%putm object-id method-id method-name context :instance))
            (return-from %%lex-get-isa-instance-method method-id))))
      
      ;; Here we could not find anything, thus we record it for classes
      (when (and *cache-methods* (%is-model? object-id))
        (%putm object-id (intern "*NONE*") method-name context :instance)
        nil))))

#|
;; requires =get and =get-id
(defconcept "person" (:att "name" (:entry)))
$E-PERSON

(defindividual "person" ("name" "John")(:var _jj))
$E-PERSON.1

$E-PERSON.1
(($TYPE (0 $E-PERSON)) ($ID (0 $E-PERSON.1)) ($T-PERSON-NAME (0 "John")))

;;; orphan with prototype an instance of person
(defobject ("name" "Zoe")(:is-a _jj)(:var _zz))
*NONE*.1

;;; own method for instance of person
(defownmethod =xxx _jj () (list (has-name *self*)))
$FN.95

(send _jj '=xxx)
(("John"))
;;; inherited by orphan
(send _zz '=xxx) (lex-get-method _zz '=xxx)
(("Zoe"))

_jj
$E-PERSON.1

;; instance method for person
(definstmethod =vvv PERSON () "doc" "=vvv executing")
$FN.98

(send _jj '=vvv)
"=vvv executing"

;;; also inherited by orphan...
(%%lex-get-isa-instance-method _zz '=vvv 0)
$E-PERSON=I=0=VVV
(send _zz '=vvv)
"=vvv executing"
|#
;;;--------------------------------------------------- %%LEX-GET-INSTANCE-METHOD

(defUn %%lex-get-instance-method (object-id method-name context)
  "try to obtain an instance method lexicographically. We record ~
   the result on the p-list of the object-id.
   Works with multiple inheritance.
Arguments:
   object-id: identifier of the INSTANCE object
   method-name: name of the method to obtain, e.g. =summary
   context (key): context
Return:
   id of the method or nil if not found."
  (declare (special *cache-methods*))
  (drformat :lgm 0 "~2%;----------- Entering %%lex-get-instance-method")
  (unless object-id (return-from %%lex-get-instance-method nil))
  
  (let (method-function-name method-id precedence-list object-type)
    ;; compute precedence list of classes
    ;(format t "~%;---1 (sv (intern...)): ~S" (symbol-value (intern "*CONTEXT*")))
    (unless (setq object-type (%get-value object-id '$TYPE))
      (warn "while trying to obtain instance-method ~S for object ~S in package ~S ~
             and context ~S, object does not exist in this context."
        method-name object-id (package-name *package*) context)
      (return-from %%lex-get-instance-method nil))
    
    (setq precedence-list (%compute-precedence-list object-type))
    (dformat :lgm 0 "2 *context*: ~S" (symbol-value (intern "*CONTEXT*")))
    (dformat :lgm 0 "precedence-list: ~S" precedence-list)
    
    ;; then walk the precedence list
    (dolist (class-id precedence-list)
      ;; is it cached?  
      (setq method-function-name (%getm class-id method-name context :instance))
      ; Bug:JPB0805 replacing obj-id with class-id
      ;(setq method-function-name (%getm object-id method-name context :instance))
      ;; if so OK
      (if method-function-name 
          (return-from %%lex-get-instance-method method-function-name))
      
      ;; try current class
      (setq method-id (%has-method class-id method-name :instance context))
      (when method-id
        ;; OK we found it locally; now get the associated function name
        ;(setq method-function-name (car (%%has-value method-id '$FNAM context)))
        ;; BUG: must use %get-value when using versions
        (setq method-function-name (car (%get-value method-id '$FNAM context)))
        ;; if not there, error
        (unless method-function-name
          (error "method function name missing for method with id: ~S" method-id))
        ;; otherwise eventually record it in the original object
        (when (and *cache-methods* (%is-model? object-id))
          (%putm object-id method-function-name method-name context :instance))
        ;; and return the name of the function implementing the method
        (return-from %%lex-get-instance-method method-function-name)))
    
    ;; when we could not find it, record the fact 
    (when (and *cache-methods* (%is-model? object-id))
      (%putm object-id (intern "*NONE*") method-name context :instance))
    nil))

#|
(definstmethod =summary "Person" () (has-name))
$FN.11

(setq $E-PERSON.1 '(($TYPE (0 $E-PERSON))($ID (0 $E-PERSON.1))))

(%%lex-get-instance-method '$E-PERSON.1 '=summary 0)
$E-PERSON=I=0=SUMMARY

(setq $E-STUDENT.1 '(($TYPE (0 $E-STUDENT))($ID (0 $E-STUDENT.1))))

(%%lex-get-instance-method '$E-STUDENT.1 '=summary 0)
$E-PERSON=I=0=SUMMARY

;;;? (defindividual "student" ("name" "Robert")(:var _rr))
;;;$E-STUDENT.1
;;;
;;;;;; instance of subclass inherit instance method
;;;? (send _rr '=vvv)
;;;"=vvv executing"
|#
;;;------------------------------------- %%LEX-GET-INSTANCE-METHOD-FROM-CLASS-ID
;;; because is is called from send-super, we do not cache the method

(defUn %%lex-get-instance-method-from-class-id (object-id method-name)
  "try to obtain an instance method lexicographically. We record ~
   the result on the p-list of the object-id. Used by send-super.
   Works with multiple inheritance.
Arguments:
   object-id: identifier of the CLASS object
   method-name: name of the method to obtain, e.g. =summary
Return:
   id of the method or nil if not found."
  ;; recursion test
  (unless object-id (return-from %%lex-get-instance-method-from-class-id nil))
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        method-function-name method-id precedence-list)
    ;; compute precedence list of classes
    (setq precedence-list (%compute-precedence-list object-id))
    ;(format *debug-io* 
    ;        "~%===> %%lex-get-instance-method object-id: ~S method: ~A~% ~
    ;         prececedence-list: ~S" object-id method-name precedence-list)
    ;; then walk the precedence list
    (dolist (class-id precedence-list)
      ;; is it cached?
      (setq method-function-name (%getm object-id method-name context :instance))
      ;; if so OK
      (if method-function-name 
          (return-from %%lex-get-instance-method-from-class-id method-function-name))
      
      ;; try current class
      (setq method-id (%has-method class-id method-name :instance context))
      (when method-id
        ;; OK we found it locally; now get the associated function name
        ;; BUG %%has-value does not work for versions
        ;(setq method-function-name (car (%%has-value method-id '$FNAM context)))
        (setq method-function-name (car (%get-value method-id '$FNAM context)))
        ;; if not there, error
        (unless method-function-name
          (error "method function name missing for method with id: ~S" method-id))
        ;; return the name of the function implementing the method
        (return-from %%lex-get-instance-method-from-class-id method-function-name)))
    nil))

;;;------------------------------------------------------------------ %ANCESTORS
;;; is now in moss-service.lisp
;;;(defUn %ancestors (object-id)
;;;  "Obtains the list of ancestors of an object - transitive closure"
;;;  (%sp-gamma object-id '$IS-A) )
;;;
;;;#|
;;;(with-package :test
;;;  (%ancestors 'test::$E-student))
;;;(TEST::$E-STUDENT TEST::$E-PERSON)
;;;
;;;(with-package :test
;;;  (with-context 4
;;;    (%ancestors 'test::$E-student)))
;;;(TEST::$E-STUDENT TEST::$E-PERSON)
;;;|#
;;;------------------------------------------------------------- %BUILD-FUNCTION

;;; Here we build functions from object descriptions
;;; We build a lambda with main args taken from $ARG property, and with
;;; tail args (&rest) taken from the $REST property
;;; Body is taken from the $CODT property of the $FN.xx object
;;; If the object is nil BOSS tries to load it from disc
;;; the function returns a list starting with lambda.

(defUn %build-function (oid)
  "Assembles the code for a functional object, and returns a lambda list"
  (let*(;(oid-l (mgf-load-if oid)) ; used to load eventually the object
        (args (car (%get-value oid '$ARG)))
        (doc (car (%get-value oid '$DOCT)))
        (code (car (%get-value oid '$CODT)))
        )
    `(lambda ,args ,(or doc "*** no documentation available ***") ,@code)))
#|
$FN.93
(($TYPE (0 $FN)) ($ID (0 $FN.93)) ($MNAM (0 =ZZZ)) ($ARG (0 NIL))
 ($CODT (0 ((PRINT 'HELLO)))) ($FNLS.OF (0 $MOSSSYS)) ($OMS.OF (0 $0-2))
 ($FNAM (0 $0-2=S=0=ZZZ)))
(%build-function '$FN.93)
(LAMBDA NIL "*** no documentation available ***" (PRINT 'HELLO))
|#
;;;------------------------------------------------------------------- %CHILDREN

(defUn %children (object-id)
  "Obtains the list of children of an object - transitive closure"
  (%sp-gamma object-id (%inverse-property-id '$IS-A)))

#|
(%children _person)
($E-PERSON $E-STUDENT)
;; works also in TEST package
|#
;;;----------------------------------------------------------------- %CLEAN-PATH

(defUn %clean-path (path)
  (cond ((null path) nil)
        ((member (car path) (cdr path)) (cdr path))
        (t (cons (car path)(%clean-path (cdr path))))))

#|
? (reverse (%clean-path (reverse '(U V D W E X B Y V D Z A))))
(U V D W E X B Y V Z A)
|#
;;;-------------------------------------------------- %COMPUTE-ALL-PATHS-TO-ROOT

(defUn %compute-all-paths-to-root (path-set result &aux parent-list)
  (cond
   ;; path-set exhausted, return result
   ((null path-set) (reverse (mapcar #'reverse result)))
   ;; when no parents transfer path to result
   ((null (setq parent-list (%parents (caar path-set))))
    (%compute-all-paths-to-root (cdr path-set) (cons (car path-set) result)))
   ;; when parents extend path eventually splitting it
   (t (%compute-all-paths-to-root 
       (append (mapcar #'(lambda (xx) (cons xx (car path-set))) parent-list)
               (cdr path-set))
       result))))


;;;          AA
;;;         /  \
;;;        YY   \
;;;       /     ZZ
;;;      BB     |
;;;     /  \    |
;;;    /    -XX-+---
;;;   /         |   \
;;;  UU         |    EE
;;;  |          |   /
;;;   \         |  WW
;;;    \        | /
;;;     \       DD
;;;      \     /
;;;       \   VV
;;;        \ /
;;;        CC


#|
(defconcept aa)
(defconcept yy (:is-a aa))
(defconcept zz (:is-a aa))
(defconcept bb (:is-a yy))
(defconcept uu (:is-a bb))
(defconcept xx (:is-a bb))
(defconcept ee (:is-a xx))
(defconcept ww (:is-a ee))
(defconcept dd (:is-a zz ww))
(defconcept vv (:is-a dd))
(defconcept cc (:is-a uu vv))

(%compute-all-paths-to-root (list (list _cc)) ())
(($E-CC $E-UU $E-BB $E-YY $E-AA) ($E-CC $E-VV $E-DD $E-AA)
 ($E-CC $E-VV $E-DD $E-EE $E-XX $E-BB $E-YY $E-AA))

(%merge-paths '($E-UU $E-BB $E-YY $E-AA) '($E-VV $E-DD $E-AA))
($E-UU $E-BB $E-YY $E-VV $E-DD $E-AA)
|#
;;;---------------------------------------------------- %COMPUTE-PRECEDENCE-LIST

(defUn %compute-precedence-list (class-id-or-class-list)
  "compute the list of priorities for classes if multiple inheritance is allowed.
Arguments:
   class-id-or-class-list: identifier of starting class or list of them
                           can also be an id pair (for own methods)
Return:
   a list of classes giving the priority for inheritance."
  (let* (all-paths path-list)
    ;; if we start from a single class or an id pair of an instance, then
    (when (or (symbolp class-id-or-class-list)
              (%%is-id? class-id-or-class-list :pair))
      (setq all-paths (%compute-all-paths-to-root 
                       (list (list class-id-or-class-list)) nil))
      ;; remove starting node from the path list and remove empty paths
      (setq path-list (remove nil (mapcar #'cdr all-paths)))
      ;; return precedence list
      (return-from  %compute-precedence-list
        (cons class-id-or-class-list
              (%compute-precedence-list-1 (car path-list) (cdr path-list)))))
    ;; if we start from a list of classes
    (when (listp class-id-or-class-list)
      (setq path-list (%compute-all-paths-to-root
                       (mapcar #'list class-id-or-class-list) nil))
      (return-from  %compute-precedence-list
        (%compute-precedence-list-1 (car path-list) (cdr path-list))))
    (error "wrong type of argument: ~S" class-id-or-class-list)))

#|
(%compute-precedence-list _cc)
($E-CC $E-UU $E-VV $E-DD $E-WW $E-EE $E-XX $E-BB $E-YY $E-VV $E-ZZ $E-AA)
(%compute-precedence-list (list _uu _vv))
($E-UU $E-VV $E-DD $E-WW $E-EE $E-XX $E-BB $E-YY $E-VV $E-ZZ $E-AA)
|#
;;;-------------------------------------------------- %COMPUTE-PRECEDENCE-LIST-1

(defUn %compute-precedence-list-1 (merged-path path-list)
  (cond
   ((null path-list) 
    (reverse (%clean-path (reverse merged-path))))
   
   ((null merged-path)
    (%compute-precedence-list-1 
     (%merge-paths (car path-list)(cadr path-list))
     (cddr path-list)))
   
   (t
    (%compute-precedence-list-1 
     (%merge-paths merged-path (car path-list)) 
     (cdr path-list)))))

;;;----------------------------------------------------------------- %HAS-METHOD
;;; To check locally for possible method

(defUn %has-method (object method-name o-or-i context)
  "Tries to locate method at the object level in a given context
Arguments:
   object: object id
   method-name: name of the requested method
   o-or-i: flag indicating own or instance method (default: instance)
   context: specified version
Returns:
   the method object id or nil."
  (let (method-sp method-list)
    (setq method-sp (if (eq o-or-i :own) '$OMS '$IMS))
    ;; get list of all methods
    (setq method-list (%get-value object method-sp context))
    ;; check if we got rigth one
    (while method-list
           (when (eq method-name 
                     (car (%get-value (car method-list) '$MNAM context)))
             (return-from %has-method (car method-list))
             )
           (pop method-list))
    (return-from %has-method ())))

#|
(%has-method '$enam '=make-entry :own 0)
$FN.3

(with-package :test
  (with-context 4
    (%has-method '$enam '=make-entry :own 4)))
$FN.3

(with-package :test
  (with-context 4
    (%has-method '$enam '=make-entry :instance 4)))
NIL    
|#
;;;---------------------------------------------------------------- %MERGE-PATHS

(defUn %merge-paths (result candidate &optional stem)
  "check if a node of result is found in candidate. If so inserts the part of ~
   candidate preceding the shared node in front of it into result.
Arguments:
   result: a list of objects
   candidate: another list of objects
   stem (opt): stack saving the partial resulting merged list
Result:
   merged paths."
  ;(print (list result candidate stem))
  (cond
   ;; we exhausted the list to test, return
   ((null result) (append (reverse stem) candidate))
   ((member (car result) candidate)
    (append (reverse stem) 
            (subseq candidate 0 (position (car result) candidate)) 
            result))
   (t (%merge-paths (cdr result) candidate (push (car result) stem)))
   ))

#|
(moss::%merge-paths '(a) '(b c))
(A B C)
(moss::%merge-paths '(a b) '(c d))
(A B C D)
(moss::%merge-paths '(a b) '(c d b))
(A C D B)
(moss::%merge-paths '(u b y a) '(v d z a))
(U B Y V D Z A)
(moss::%merge-paths '(U B Y V D Z A) '(v d w e x b y a))
(U V D W E X B Y V D Z A)
|#
;;;-------------------------------------------------------------------- %PARENTS

;;; Must be able to find ancestor
;;; next function is for multiple inheritance
;;; 22/6/87 I am not so sure that including instances in the ancestor process
;;; is such a good idea.

(defUn %parents (object-id)
  "Obtains the list of parents of an object"
  (%get-value object-id '$IS-A))

#|
(%parents _student)
($E-PERSON)
0

(with-package :test
  (with-context 4
    (%parents test::_student)))
(TEST::$E-PERSON)
0

(with-package :test
  (with-context 6
    (%parents 'test::($e-person . 3))))
NIL

(catch :error
       (with-package :test
         (with-context 0
           (%parents 'test::($e-person . 3)))))
($E-PERSON . 3) does not exist or is dead in package "TEST" context 0.
|#
;;;----------------------------------------------------------- TRACE-INHERITANCE
;;; Tracing functions

(defUn trace-inheritance ()
  "Trace inheritance"
  (let ((*trace-bar-frequency* 3) ; print a vertical bar showing links every 3 calls
        (*trace-print-level* 4)
        (*trace-print-length* 5))
    (trace get-method)
    (trace %get-own-method)
    (trace %get-universal-method)
    (trace %get-instance-method)
    (trace %get-isa-instance-method)
    (trace lex-get-method)
    (trace %lex-get-own-method)
    (trace %lex-get-instance-method)
    (trace %lex-get-isa-instance-method)
    "Inheritance trace on, for all methods"
    ))

;;;--------------------------------------------------------- UNTRACE-INHERITANCE

(defUn untrace-inheritance ()
  "Untrace inheritance"
  (progn
    (untrace get-method)
    (untrace %get-own-method)
    (untrace %get-universal-method)
    (untrace %get-instance-method)
    (untrace %get-isa-instance-method)
    (untrace lex-get-method)
    (untrace %lex-get-own-method)
    (untrace %lex-get-instance-method)
    (untrace %lex-get-isa-instance-method)
    "Inheritance trace off"
    ))

;;;---------------------------------------------------------------- TRACE-METHOD

(defUn trace-method (method)
  "Trace all messages sent to a particular method"
  (>>setprop method t :trace)
  ;(setf (get method :trace) t)
  (setq *trace-flag* t)
  )

;;;-------------------------------------------------------------- UNTRACE-METHOD 

(defUn untrace-method (method)
  "Undo the trace-method action"
  (remprop method :TRACE)
  )

;;;----------------------------------------------------------------- UNTRACE-ALL

(defUn untrace-all ()
  "Untrace all traceable messages"
  (setq *trace-flag* nil)
  )

;;;---------------------------------------------------------------- TRACE-OBJECT

(defUn trace-object (object)
  "Trace all messages sent to a particular object"
  (progn
    (>>setprop object t :trace) ; generalized (setf (get 
    (setq *trace-flag* t)
    ))

;;;-------------------------------------------------------------- UNTRACE-OBJECT

(defUn untrace-object (object)
  "Undo the trace-object action"
  (remprop object :TRACE)
  )

;;;--------------------------------------------------------------- TRACE-MESSAGE

(defUn trace-message ()
  "trace all messages"
  (setq *trace-flag* t *trace-message* t) )

;;;------------------------------------------------------------- UNTRACE-MESSAGE

(defUn untrace-message ()
  "Untrace all messages"
  (setq *trace-level* 0)
  (setq *trace-message* nil )
  )

;;;----------------------------------------------------------------- STEP-METHOD

;;; stepping functions (stepping package must be loaded STEP.LSP)

(defUn step-method (method-name)
  "step through a specified method for debugging purposes"
  (setf (get method-name :step) t)
  )

;;;--------------------------------------------------------------- UNSTEP-METHOD

(defUn unstep-method (method-name)
  "remove stepping mode"
  (remprop method-name :step)
  )

;;;------------------------------------------------------------------- TON, TOFF

;;; abbreviations for last two functions
(defUn ton () "Abreviation" (trace-message ))
(defUn toff () "Abreviation" (untrace-message ))

;;; Sundry useful functions

(defUn g-> (prop)
  "Same as g=> but is not traceable"
  (send-no-trace *self* '=get-id prop) )

(defUn g=> (prop)
  "get a value from a property id of *self*"
  (send *self* '=get-id prop) )

(defUn g--> (prop)
  "Same as g==> but is not traceable"
  (send-no-trace *self* '=get prop) )

(defUn g==> (prop)
  "get a value from a property of *self*"
  (send *self* '=get prop) )

(defUn h-> (prop)
  "Looks for a local value no trace"
  (send-no-trace *self* '=has-value-id prop) )
(defUn h--> (prop)
  "Looks for a local value no trace"
  (send-no-trace *self* '=has-value prop) )

(defUn h=> (prop)
  "Looks for a local value - traceable"
  (send *self* '=has-value-id prop) )
(defUn h==> (prop)
  "Looks for a local value - traceable"
  (send *self* '=has-value prop) )

(defUn s-> (prop value)
  "Sets a value"
  (send-no-trace *self* '=set-id prop value) )
(defUn s--> (prop value)
  "Sets a value"
  (send-no-trace *self* '=set prop value) )

(defUn s=> (prop value)
  "Sets a value"
  (send *self* '=set-id prop value) )
(defUn s==> (prop value)
  "Sets a value"
  (send *self* '=set prop value) )

(defUn p-> (prop value)
  "Add a value"
  (send-no-trace *self* '=put-id prop value) )
(defUn p--> (prop value)
  "Add a value"
  (send-no-trace *self* '=put prop value) )

(defUn p=> (prop value)
  "Add a value"
  (send *self* '=put-id prop value) )
(defUn p==> (prop value)
  "Add a value"
  (send *self* '=put prop value) )

;;;=============================================================================
;;; Initialize various flags

;;;(%defsysvar *lisp-flag* (if t then LISP s-expr are considered object) nil )
;;;(%defsysvar *trace-message* (if t all messages are traced) nil )
;;;(%defsysvar *trace-flag* (if t allows selective tracing) nil )
;;;(%defsysvar *trace-level* (Indentation level) 0 )
;;;(%defsysvar *self* (initial Sender is the *user*) '*user* )

;; tell the world that the MOSS engine is loaded
(setq *moss-engine-loaded* t)

;;; build the access functions for the already defined properties
;;; however, such accessors cannot be used before the kernel methods are loaded
;;; since we need =get-id and =set-id functions

;;; one must use the methods rather than the %get-value or other service function
;;; since the methods can be overloaded for some classes.

;********** is this really useful?

(eval-when (compile load eval)
  (let ((prop-list (append (%%has-value *moss-system* '$ETLS *context*)
                           (%%has-value *moss-system* '$ESLS *context*)))
        prop-name)
    ;; for each property, get its name
    (dolist (id prop-list)
      (setq prop-name (%%make-name (car (%%has-value id '$PNAM *context*)) :prop))
      ;; check if already done
      (unless (fboundp prop-name)
        ;; add now a macro to allow to use property name as a function in methods
        (eval `(defUn ,prop-name (&optional obj-id)
                 (if obj-id
                     (send-no-trace obj-id '=get-id ',id)
                   (send-no-trace *self* '=get-id ',id))))
        ;; something more complex to set the value using the newly defined function
        (eval `(defsetf ,prop-name (&optional obj-id) (value) 
                 `(progn (if ,value
                             (send-no-trace ,obj-id '=set-id ',',prop-name ,value)
                           (send-no-trace *self* '=set-id ',',prop-name ,obj-id))
                    (or ,value ,obj-id))))
        ))
    "*done*"))

:EOF