;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;19/12/15
;;;		
;;;		M A C R O S - (File macros.lisp)
;;;	
;;;	The file contains macros and functions used by the code. Some  
;;;	were part of UTC Lisp primitives. First created in April 1992
;;;     The file contains also the definition of global variables
;;;==========================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
2019
 1215 adding defvirtualattribute, defvirtualrelation and defvirtualrule to
      be compatible with augmeted MOSS ontologies to be translated into
      OWL/Jena or OWM/SPARQL rules

|#

#|
(create-user-manual (choose-file-dialog))
(CREATE-USER-MANUAL (cg:ASK-USER-FOR-DIRECTORY :BROWSE-INCLUDE-FILES T))
|#

;;; all the following symbols are defined in the MOSS package

(in-package :moss)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (import
   '(mln::*language-tags*
     ))
  )

;;;========================== debugging macro ===============================

;;;------------------------------------------------------------------- TERROR

(defMacro terror (cstring &rest args)
  "throws an error message back to an :error catch. Prints message if *verbose*"
  `(progn
     (if *verbose* (format *debug-io* 
                           ,(concatenate 'string "~&;***Error " cstring) ,@args))
     (throw :error 
            (format nil ,(concatenate 'string "~&;***Error " cstring) ,@args))))

#|
(catch :error (terror "erreur dans ~S" "Moss"))
";***Error erreur dans \"Moss\""

(let ((*verbose* t))
  (declare (special *verbose*))
  (catch :error (terror "erreur dans ~S" "Moss")))
;***Error erreur dans "Moss"
";***Error erreur dans \"Moss\""
;; prints before throwing
|#
;;;-------------------------------------------------------------------- TWARN

(defMacro twarn (cstring &rest args)
  "inserts a message into the *error-message-list* making sure to return nil"
  (declare (special *error-message-list*))
  `(progn (push (format nil ,cstring ,@args) *error-message-list*) nil))

#|
(setq *error-message-list* nil)
(twarn "erreur dans ~S" "Moss")
NIL
*error-message-list*
("erreur dans \"Moss\"")
|#

;;;----------------------------------------------------------------- TRFORMAT

(defMacro trformat (fstring &rest args)
  "debugging macro used locally explicitely temporarily"
  `(format *debug-io* ,(concatenate 'string "~&;***** " fstring) 
           ,@args))

#+MICROSOFT-32 (defMacro ipcg () `(in-package :cg-user))
#+MICROSOFT-32 (defMacro cg-user::ipm () `(in-package :moss))



;;;======================== code coloring macro =============================
;;; For color code ?? :utf8 is not included by default in *features*

(defMacro +utf () `(pushnew :utf8 *features*))
(defMacro -utf () `(remove :utf8 *features*))


;;;==========================================================================
;;;                     service functions and macros
;;;==========================================================================

;;;--------------------------------------------------------------- ASSREMPROP
;;; we assume here that properties are always symbols

(defMacro assremprop (prop ll)
  `(remove-if #'(lambda(xx)(eq (car xx) ,prop)) ,ll))

;;;-------------------------------------------------------------- CATCH-ERROR

(defMacro catch-error (msg &rest body)
  "intended to catch errors due to poor MOSS syntax. Prints in active window ~
   using mformat unless msg is empty
   Note: body should not return a string (considered as an error message)
Arguments:
   msg: something like \"while executing xxx\"
   body (rest): exprs to execute
Return:
   the value of executing body if no error, nil and prints a message when error"
  (if (equal msg "")
    `(catch :error ,@body nil)
    `(let* ((err (catch :error ,@body nil)))
       ;; if non nil, then string to be printed unless message is empty
       (cond 
        ((stringp err)
         (moss::mformat "~&;*** MOSS error ~A: ~%~A" ,msg err)
         nil)
        ;; otherwise return value returned by catch
        (err)))))

;;;------------------------------------------------------- CATCH-SYSTEM-ERROR

(defMacro catch-system-error (msg &rest body)
  `(let (test errno)
     (multiple-value-setq (test errno) 
       (ignore-errors ,@body))
     ;(print (list test errno))
     ;; if test is nil and errno is not: error
     (if (and (null test) errno)
       (mformat "~%... system error ~A: ~%~S" ,msg errno))
     test))

#|
(catch-system-error "" (+ 2 3 4))
(9 NIL)
9

(catch-system-error "while doing things" (+ 2 3 4)(/ 4 0))
(NIL #<DIVISION-BY-ZERO @ #x21b8ca42>) 
... system error while doing things: 
#<DIVISION-BY-ZERO @ #x21b8ca42>
NIL
|#
;;;?--------------------------------------------------------------- COPY-FACT
;;; used by conversation states to update the FACTS field

(defmacro copy-fact (item1 item2)
  `(move-fact moss::conversation :from ,item1 :to ,item2))

;;;?---------------------------------------------------------------- GET-FACT
;;; used by the conversation states to read the FACTS field

(defmacro get-fact (item)
  `(read-fact moss::conversation ,item))

;;;--------------------------------------------------------------------- GETV
;;; properties are symbols, but getv could be used elsewhere

(defMacro getv (prop ll) 
  "Returns
   1. the value associated to prop
   2. t if the value was specified to be NIL"
  `(values (cdr (assoc ,prop ,ll :test #'equal+)) 
           (and (car (assoc ,prop ,ll :test #'equal+)) t)))

#|
(getv 1 (setq ll '((0 a a)(1 b b) (2))))
(B B)
T

(getv 2 ll)
NIL
T

(getv 3 ll)
NIL
NIL 
|#
;;;--------------------------------------------------------------- IN-VERSION
;;; same as with-context no check for valid context
;;; note that we use the *context* variable of the current package

(defMacro in-version (%_context &rest ll)
  (append `(let ((,(intern "*CONTEXT*") ,%_context))
             (declare (special ,(intern "*CONTEXT*"))))
          ll ))

#|
(setq *version* 0)
(in-version 3
            (print (symbol-value (intern "*CONTEXT*"))))
(LET ((*CONTEXT* 3))
  (DECLARE (SPECIAL *CONTEXT*))
  (PRINT (SYMBOL-VALUE (INTERN "*CONTEXT*"))))
3
3
*version* 
0
|#
;;;-------------------------------------------------------------------- INTER
;;; intersecting any number of lists

(defMacro inter (&rest ll)
  "intersects any number of lists, does not preserve order."
  (if (cdr ll)
    `(intersection ,(car ll)(inter ,@(cdr ll)))
    (car ll)))

#|
(inter nil)
NIL

(inter '(1 2 3))
(1 2 3)

(inter '(1 2 3 4 5) '(2 3 4) '(3 5 4 6))
(4 3)
|#
;;;?----------------------------------------------------------------- %MKNAME
;;; used only in moss-query

(defMacro %mkname (ref type)
  "if ref is a symbol return symbol, otherwise call~
   %make-name-for-<type>.
Arguments:
   ref: a symbol or (multilingual) string
   type: type of name to recover (keyword)
   package (key): default current
Expansion:
   (%make-name-for-<type> ref . more-args)"
  `(if (symbolp ,ref)
     ;; if symbol in current package do nothing
     ;; (*package* is checked at run time)
     (if (eql (symbol-package ,ref) *package*)
       ,ref 
       ;;...otherwise change package
       (intern (symbol-name ,ref)))
     ;; if not a symbol, then create a symbol in the specified package
     (intern (%%make-name-string ,ref ,type))))


#|
(%mkname "NAME" :property)
HAS-NAME
:INTERNAL

(%%make-name "name" :property)
;---
(%%make-name "name" :property)
HAS-NAME
:INTERNAL

;; if symbol keep it as it is
(%mkname 'FIRST-NAME :property)
FIRST-NAME
;---
(%%make-name 'name :property)
HAS-NAME
:INTERNAL

(%mkname 'HAS-NAME :property)
HAS-NAME
;---
(%%make-name 'has-name :property)
HAS-NAME
:INTERNAL

(%mkname '(:en "NAME" :fr "nom") :property)
HAS-NAME
:INTERNAL
;---
(%%make-name '(:en "NAME" :fr "nom") :property)
HAS-NAME
:INTERNAL
;---
(%%make-name '((:en "NAME") (:fr "nom")) :property)
HAS-NAME
:INTERNAL

;;; %mkname does not take :package as keyword

(%mkname "NAME" :property :package :address)
ADDRESS:HAS-NAME
:EXTERNAL

(%mkname '(:en "NAME" :fr "nom") :property :package :address)
ADDRESS:HAS-NAME
:EXTERNAL
|#
;;;------------------------------------------------------------------- MTHROW
;;; differs from terror by not using *verbose*

(defMacro mthrow (control-string &rest args)
  "print a message and throws the message string to :error"
  `(throw :error
     (mformat ,control-string ,@args)))

#|
(catch :error (mthrow "error in ~S" "Moss"))
error in "Moss"
NIL
|#
;;;-------------------------------------------------------------- NASSREMPROP

;;; deletes destructively a pair from an a-list. setf is used to make sure
;;; that the list is effectively modified destructively when removing the
;;; first element. If the list is pointed to from some atom, doing a delete
;;; does not change the list, it simply returns a pointer onto the cdr of
;;; the list.
;;; Does not work for versioned MOSS objects since it deletes all versions!
;;; ... used by %putc, but %putc is currently unused

(defMacro nassremprop (prop ll)
  "ll must be a symbol naming an a-list"
  `(setf ,ll (delete-if #'(lambda(xx)(eq (car xx) ,prop)) ,ll)))

#|
(setq la '((MOSS::$TYPE (0 $E-PERSON)) (MOSS::$ID (0 $E-PERSON.22))
           ($T-PERSON-AGE (0 32))))

(MOSS::nassremprop '$T-PERSON-AGE la)
(($TYPE (0 $E-PERSON)) ($ID (0 $E-PERSON.22)))
|#

;;; print and recover the source code of a function
;;;(defMacro pf (fn) `(function-lambda-expression (function ,fn)))
;;; print plist of an object
;;;(defMacro pl (obj-id) `(symbol-plist (quote ,obj-id)))

;;;-------------------------------------------------------------------- NCONS
;;; ancient primitive not really very useful

(defMacro ncons (arg) `(cons ,arg nil))

#|
(ncons 2)
(2)

(list 2)
(2)
|#
;;;------------------------------------------------------------------ RFORMAT
;;; a macro to limit the length of strings to post using Pascal (birrrkkhh)
;;; used by browser and locator
;;;********** should be removed

(defMacro rformat (limit &rest ll)
  "takes a text and limits its length. Because the silly Pascal strings must be ~
   less than some length.
Arguments:
  limit: max length
  ll: regular arguments to a text."
  `(let ((result (format ,@ll)))
     (remove '#\return
             (if (> (length result) ,limit)
               (concatenate 'string (subseq result 0 (- ,limit 3)) "...")
               result))))

#|
(rformat 40 nil "Ce jour-là nous étions tous partis cueillir des champignons.")
"Ce jour-là nous étions tous partis cu..."
|#
;;;?---------------------------------------------------------------- SET-FACT
;;; used by conversation states to update the FACTS field

(defmacro set-fact (item value)
  `(replace-fact moss::conversation ,item ,value))

;;;---------------------------------------------------------------------- SV
;;; not really used

(defmacro sv (xx) `(symbol-value ,xx))

#|
(setq albert 2345)
(sv 'albert)
(SYMBOL-VALUE 'ALBERT)
2345
|#
;;;?----------------------------------------------------------------- TFORMAT
;;; trace printing function using *trace-output* *trace-level* to indent

(defMacro tformat (format-string &rest arg-list)
  "used to simplify global trace"
  `(format *trace-output* "~&~VT~S ~?" 
           *trace-level* *trace-level* ,format-string (list ,@arg-list)))

;;;------------------------------------------------------------ VERBOSE-THROW
;;; different from terror, throws to any tag

(defMacro verbose-throw (label &rest format-args)
  "when *verbose* is T, prints a warning before throwing the error message ~
   string to the label.
Arguments:
   label: a label to which to throw (usually :error)
   format-args: arguments to build a message string using format nil
Return:
   throws to the label the message string."
  `(let ((error-message (format nil ,@format-args)))
     (if *verbose* (warn error-message))
     (throw ,label error-message)))

;;;------------------------------------------------------------- VERBOSE-WARN

(defMacro verbose-warn (&rest format-args)
  `(when *verbose*
     (warn ,@format-args)))

;;;-------------------------------------------------------------------- WHILE
;;; we define while here as in UTC-Lisp - we must be careful since in UTCLisp 
;;; while returns the value of the last expr in the loop body
;;;********** should use (loop while ... do ...) instead
;;; while is defined in CCL

(defMacro while (condition &rest body)
   `(loop while ,condition do ,@body))

#|
(setq nn 3)
3
(while (> nn 0) (print (decf nn)) (print :a))
(LOOP (IF (NOT (> NN 0)) (RETURN NIL)) (PRINT 1) (PRINT 2))
2 
:A 
1 
:A 
0 
:A 
NIL
|#
;;;?--------------------------------------------------------------- WITH-CONTEXT
;;; (with-context context &rest instructions) - closure in context
;;; because we define the macro in a MOSS package, %%%allowed-context? is the
;;; one defined in MOSS
;;; Should restore initial context in case of a throw from %%allowed-context?
;;; We use %_xxx variables not to be in conflict with ll variables

(defMacro with-context (%_context &rest %_ll)
  `(let ((%_save (symbol-value (intern "*CONTEXT*")))
         %_result)
     ;(format t "~%;+++ with-context /%_context: ~S" ,%_context)
     ;(format t "~%;+++ with-context /save: ~S" %_save)
     (%%allowed-context? ,%_context)
     (set (intern "*CONTEXT*") ,%_context)
     ;; make sure that the context is restored to its previous value
     (unwind-protect
         ;; when ll returns several values, save them into a list
         (setq %_result (multiple-value-list (progn ,@%_ll)))
       (set (intern "*CONTEXT*") %_save))
     ;; but return whatever was computed in the ll exprs using multiple values if
     ;; neede
     (values-list %_result)))

#|
(with-context 3 (print *context*) (1+ *context*))
expands to
(LET ((SAVE 0))
  (SET (INTERN "*CONTEXT*" *PACKAGE*) 3)
  (PRINT *PACKAGE*)
  (%%ALLOWED-CONTEXT? 3)
  (LET ((#:G1000 (PROGN (PRINT *CONTEXT*) (1+ *CONTEXT*))))
    (SET (INTERN "*CONTEXT*") SAVE)
    #:G1000))
and yields
;*** MOSS-error context 3 is illegal in package: #<The MOSS package>.
Error: Attempt to throw to the non-existent tag :ERROR

(with-package :test
  ;; must prefix the variables, otherwise use MOSS...
  (with-context 3 (print test::*context*) (1+ test::*context*)))
expands to
(LET ((*PACKAGE* (FIND-PACKAGE :TEST)))
  (IF *PACKAGE*
      (PROGN (LET ((SAVE 0))
               (SET (INTERN "*CONTEXT*") 3)
               (PRINT (INTERN "*CONTEXT*"))
               (PRINT (SYMBOL-VALUE (INTERN "*CONTEXT*")))
               (%%ALLOWED-CONTEXT? 3)
               (LET ((#:G1000 (PROGN (PRINT TEST::*CONTEXT*) (1+ TEST::*CONTEXT*))))
                 (SET (INTERN "*CONTEXT*") SAVE)
                 #:G1000)))
    (PROGN (IF *VERBOSE*
               (FORMAT *DEBUG-IO* "~&;***Error there is no package called ~S" :TEST)
             NIL)
           (THROW :ERROR (FORMAT NIL "~&;***Error there is no package called ~S" :TEST)))))
and yields
3
3

(with-package :moss
  (with-context 3 (print *context*) (1+ *context*)))
|#
;;;------------------------------------------------------------ WITH-ENVIRONMENT
;;; (with-environment package context language &rest instructions) 
;;;  Set a MOSS environment for processing ontology macros (e.g. deferred)

(defMacro with-environment (%_package %_context %_language &rest ll)
  `(let ((*package* ,%_package))
     (declare (special *package*))
     (set (intern "*LANGUAGE*") ,%_language)
     (set (intern "*CONTEXT*") ,%_context)
     (%%allowed-context? ,%_context)
     ,@ll ))

#|
(defun ff ()(print (list *package* *language* *context*)))
(with-environment (find-package :moss) 3 :fr (ff))
(#<The MOSS package> :FR 3)
|#
;;;--------------------------------------------------------------- WITH-LANGUAGE
;;; first save current global language value, then change language, execute exprs
;;; restore language and return result of executing exprs

(defmacro with-language (%_language &rest ll)
  `(let ((saved-lan (symbol-value (intern "*LANGUAGE*"))) 
         result)
     (set (intern "*LANGUAGE*") ,%_language)
     ;; execute exprs, then restore language, but return result of exprs
     (setq result (multiple-value-list (progn ,@ll)))
     ;; restore language
     (set (intern "*LANGUAGE*") saved-lan)
     ;; return result preserving multiple values
     (values-list result)))

#|
*language*
:EN
(with-language :fr
  (mln::make-mln '("a" "b")))
((:FR "a" "b"))
*language*
:EN

(with-package :address
  (with-language :en
    (send 'address::$E-PERSON '=get-name)))
("person")

(with-package :address
  (with-language :fr
    (send 'address::$E-PERSON '=get-name)))
("personne")

(with-language :DE (print *language*) (values 1 2))
:DE
1
2
|#

;;;--------------------------------------------------------------- WITH-LANGUAGE2
;;; first save current global language value, then change language, execute exprs
;;; restore language and return result of executing exprs

(defmacro with-language2 (%_language &rest ll)
  `(let ((saved-lan (symbol-value (intern "*LANGUAGE*"))) 
         result)
     (set (intern "*LANGUAGE*") ,%_language)
     ;; execute exprs, then restore language, but return result of exprs
     (setq result (progn ,@ll))
     ;; restore language
     (set (intern "*LANGUAGE*") saved-lan)
     ;; return result preserving multiple values
     (values-list result)))

;;;---------------------------------------------------------- WITH-MOSS-LANGUAGE

(defmacro with-moss-language (%_language &rest ll)
  (declare (special *language*))
  `(let ((saved-lan moss::*LANGUAGE*)
         result)
     (setq *LANGUAGE* ,%_language)
     ;; execute exprs, then restore language, but return result of exprs
     (setq result (multiple-value-list (progn ,@ll)))
     ;; restore language
     (setq *LANGUAGE* saved-lan)
     ;; return result preserving multiple values
     (values-list result)))

;;;---------------------------------------------------------------- WITH-PACKAGE

(defMacro with-package (%_package &rest ll)
  ;(declare (special *package*))
  `(let ((*package* (find-package ,%_package)))
     (declare (special *package*)) ; does not seem to change anything
     (if *package*
         (progn ,@ll)
       (terror "there is no package called ~S" ,%_package))))

#|
(catch :error (with-package :address (print *package*)))
";***Error there is no package called :ADDRESS"

(make-package :address-2 :use '(:moss :cl))
#<The ADDRESS-2 package>

(defun ff () (print *package*))
FF

(with-package :address-2 (ff))
(LET ((*PACKAGE* (FIND-PACKAGE :ADDRESS-2)))
  (DECLARE (SPECIAL *PACKAGE*))
  (IF *PACKAGE* 
      (PROGN (FF)) 
    (TERROR "there is no package called ~S" :ADDRESS-2)))
#<Package "ADDRESS-2">
#<Package "ADDRESS-2">
|#
;;;--------------------------------------------------------------------- WITH-AP
;;; with-ap stands for with-application-package and must be used everytime one
;;; executes a callback that was compiled in a different package or uses the
;;; function read-from-string. We assume that we can recover the application
;;; package from an object of the application

(defmacro with-ap (id &rest body)
  `(moss::with-package (moss::<<symbol-package ,id) ,@body))

;;;=============================================================================
;;;                              Defining macros
;;;=============================================================================
;;; defconcept          replaces m-defclass (avoiding CLOS conflict with defclass)
;;; defattribute        replaces m-defattribute or m-deftp
;;; defrelation         replaces m-defrelation or m-defsp
;;; definstmethod       replaces m-defmethod
;;; defownmethod        replaces m-defownmethod
;;; defuniversalmethod  replaces m-defuniversalmethod
;;; defobject           replaces m-defobject
;;; definstance         replaces m-definstance
;;; defindividual       id.

;;; defontology         also applies to SOL ontologies
;;; defchapter          to produce HTML outputs
;;; defsection          id.

;;; We must add specific macros for defining methods the name of which we want to
;;; export:

;;; defmossinstmethod
;;; defmossownmethod
;;; defmossuniversalmethod

;;; deffunction
;;; %defsysvar
;;; defsysvarname

;;; defvirtualconcept

;;; defvirtualattribute  for augmenting MOSS to be translated into OWL/Jena/SPARQL
;;; defvirtualrelation   for augmenting MOSS to be translated into OWL/Jena/SPARQL
;;; defvirtualrule       for augmenting MOSS to be translated into OWL/Jena/SPARQL

;;; Such macros are static macros and are thus executed within a specific package

;***** need to export that stuff

;;;---------------------------------------------------------------- DEFATTRIBUTE

(defMacro defattribute (name &rest option-list)
  `(apply #'%make-tp ',name ',option-list))

;;;------------------------------------------------------------------- %DEFCLASS
;;; for historical reasons

(defMacro %defclass (multilingual-name &rest option-list)
  `(apply #'%make-concept ',multilingual-name ',option-list))

;;;------------------------------------------------------------------ DEFCHAPTER
;;; fake

(defMacro defchapter (&rest option-list)
  (declare (ignore option-list))
  nil)

;;;------------------------------------------------------------------ DEFCONCEPT

(defMacro defconcept (multilingual-name &rest option-list)
  `(apply #'%make-concept ',multilingual-name ',option-list))

;;;----------------------------------------------------------------- %DEFCONCEPT

(defMacro %defconcept (multilingual-name &rest option-list)
  `(apply #'%make-concept ',multilingual-name ',option-list))

;;;----------------------------------------------------------------- DEFFUNCTION

(defMacro deffunction (name var-list &rest body)
  "define a user function: same syntax as defun. Builds a function name and ~
   save its name on *moss-system* $SFL list. Arg-list and body are associated ~
   with a local variable: F_function-name"
  `(let ((ff (intern ,(concatenate 'string "F_" (symbol-name name)))))
     (set ff (list* ',name 'lambda ',var-list ',body))
     (%%add-value (symbol-value (intern "*MOSS-SYSTEM*")) 
                  '$SFL ff (symbol-value (intern "*CONTEXT*")))
     (defUn ,name ,var-list ,@body)
     ))
#|
(deffunction test (a b c d) 
  "test function"
  (declare (ignore A B))
  (list c d))
|#
;;;--------------------------------------------------------------- %DEFGENERICTP

(defMacro %defgenerictp (name &rest option-list)
  "see %make-generic-tp doc"
  `(apply #'%make-generic-tp ',name ',option-list))

;;;--------------------------------------------------------------- DEFINDIVIDUAL

(defMacro defindividual (name &rest option-list)
  `(apply #'%make-individual ',name ',option-list))

;;;----------------------------------------------------------------- DEFINSTANCE

(defMacro definstance (name &rest option-list)
  `(apply #'%make-instance ',name ',option-list))

;;;---------------------------------------------------------------- %DEFINSTANCE
;;; (%definstance class-name &rest option-list) - 
;;; syntax of option list is
;;;	(<tp-name> value[list])
;;;	(<sp-name> <internal-reference>)
;;; The class name is checked for existing classes 
;;; Properties are checked in the same fashion
;;;	Error messages are send if a non existing entity is mentioned
;;; Values for terminal properties are not checked unless for entry-point
;;; generation
;;; Internal references to MOSS entities must correspond to existing objects
;;; and of the proper type.
;;; If not, then an error message is sent.
;;; Example
;;;	(%definstance PERSON
;;;		(HAS-NAME Barthes)
;;;		(HAS-BROTHER p1)
;;;		)
;;; will work if p1 is the internal reference to an already defined person
;;; or specialization of person.
;;; Returns the internal reference to the new object
;;; Once created objects must be modified directly, using access methods
;;; Integrity constraints are checked to see whether the properties are
;;; correctly filled.
;;; The created instance is put onto the system-variable-list.
;;; This function cannot work without the Message Passing kernel

(defMacro %definstance (name &rest option-list)
  `(apply #'%make-instance ',name ',option-list))

;;;--------------------------------------------------------------- DEFINSTMETHOD

(defMacro definstmethod (name selector arg-list &rest body)
  `(funcall #'%make-method ',name ',selector ',arg-list ',body))


;;;----------------------------------------------------------- DEFMOSSINSTMETHOD

(defMacro defmossinstmethod (name selector arg-list &rest body)
  `(funcall #'%make-method ',name ',selector ',arg-list ',body '(:export t)))

;;;------------------------------------------------------------ DEFMOSSOWNMETHOD

(defMacro defmossownmethod (name selector arg-list &rest body)
  `(funcall #'%make-ownmethod ',name ',selector ',arg-list ',body '(:export t)))

;;;------------------------------------------------------ DEFMOSSUNIVERSALMETHOD

(defMacro defmossuniversalmethod (name arg-list &rest body)
  `(funcall #'%make-universal ',name ',arg-list ',body '(:export t)))

;;;------------------------------------------------------------------- DEFOBJECT

(defMacro defobject (&rest option-list)
  `(apply #'%make-object ',option-list))

;;;------------------------------------------------------------------ %DEFOBJECT

(defMacro %defobject (&rest option-list)
  `(apply #'%make-object ',option-list))

;;;----------------------------------------------------------------- DEFONTOLOGY

(defMacro defontology (&rest option-list)
  `(apply #'%make-ontology ',option-list))

;;;---------------------------------------------------------------- DEFOWNMETHOD

(defMacro defownmethod (name selector arg-list &rest body)
  `(funcall #'%make-ownmethod ',name ',selector ',arg-list ',body))

;;;--------------------------------------------------------------- %DEFOWNMETHOD
;;; (%defownmethod name selector arg-list doc body) - 
;;; creates a new method and attaches it to the object defined by the selector
;;; which must exist. If method already exists then error
;;; When selector is a symbol then it is considered an object-id 
;;; Otherwise syntax is:
;;;	(entry tp-name class [:filter function] )
;;; where function is used when more than one object is reached via the
;;; access path entry-tp-name-class
;;; defownmethod is essentially the same function as defmethod. It is easier
;;; to use than defmethod with the :own-method option at the end of the arg
;;; list. Thus it is defined for convenience.

;;; last argument must be a single list otherwise %make-own-method will have to
;;; quote every single instruction of the code

(defMacro %defownmethod (name selector arg-list &rest body)
  `(funcall #'%make-ownmethod ',name ',selector ',arg-list ',body))

;;;------------------------------------------------------------------ %DEFMETHOD

(defMacro %defmethod (name selector arg-list &rest body)
  `(funcall #'%make-method ',name ',selector ',arg-list ',body))

;;;----------------------------------------------------------------- DEFRELATION

(defMacro defrelation (name class suc &rest option-list)
  `(apply #'%make-sp ',name ',class ',suc ',option-list))

;;;--------------------------------------------------------------------- DEFRULE

(defmacro defrule (&rest ll)
  "used in augmented ontologies to be translated into OWL/Jena or OWL/SPARQL ~
   rules. MOSS ignores this macro and uses methods instead."
  `(apply #'%make-virtual-rule ',ll))

;;;------------------------------------------------------------------ DEFSECTION
;;; fake

(defMacro defsection (&rest option-list)
  (declare (ignore option-list))
  nil)

;;;---------------------------------------------------------------------- %DEFSP

(defMacro %defsp (name class suc &rest option-list)
  `(apply #'%make-sp ',name ',class ',suc ',option-list))
 
;;;------------------------------------------------------------------ %DEFSYSVAR
;;; (%defsysvar name doc value) - creates a new system variable
;;; If variable already exists then reinitialize it
;;; The value argument is evaluated.
;;; Example of use
;;;	(%defsysvar *screen* (Initialize system output stream)
;;;		(-> '$TTY '=new))
;;; Should be modified so that system variables are first class objects
;;; like methods. *****
;;; For the time being, we use this simple minded form.

(defMacro %defsysvar (name doc value)
  `(_%defsysvar ',name ',doc ',value))

;;;--------------------------------------------------------------- DEFSYSVARNAME

(defMacro defsysvarname (sysvar)
  "adds the name of a (global) variable to the list of variables recorded at ~
   system level."
  `(%%add-value *MOSS-SYSTEM* '$SVL ',sysvar (symbol-value (intern "*CONTEXT*")))
  )

;;;-------------------------------------------------------------- %DEFSYSVARNAME
;;; The next function can be used to save the name of a variable onto the 
;;; system list

(defMacro %defsysvarname (sysvar-name)
  `(%%add-value '$MOSSSYS '$SVL ',sysvar-name (symbol-value (intern "*CONTEXT*")))
  )

;;;---------------------------------------------------------------------- %DEFTP

(defMacro %deftp (name &rest option-list)
  `(apply #'%make-tp ',name ',option-list))

;;;--------------------------------------------------------------- %DEFUNIVERSAL
;;; (%defuniversal name arg-list body) - creates a new universal 
;;; method and attaches it to its name which must be unique. If name already exists
;;; then error, i.e. universal methods cannot be redefined?

(defMacro %defuniversal (name arg-list &rest body)
  `(funcall #'%make-universal ',name ',arg-list ',body))

;;;---------------------------------------------------------- DEFUNIVERSALMETHOD

(defMacro defuniversalmethod (name arg-list &rest body)
  `(funcall #'%make-universal ',name ',arg-list ',body))

;;;--------------------------------------------------------- DEFVIRTUALATTRIBUTE

(defmacro defvirtualattribute (&rest ll)
  "used in augmented ontologies to be translated into OWL/Jena or OWL/SPARQL ~
   rules. MOSS uses methods instead."
  `(apply #'%make-virtual-attribute ',ll))
                    
;;;----------------------------------------------------------- DEFVIRTUALCONCEPT

(defMacro defvirtualconcept (multilingual-name &rest option-list)
  `(apply #'%make-virtual-concept ',multilingual-name ',option-list))

;;;---------------------------------------------------------- %DEFVIRTUALCONCEPT

(defMacro %defvirtualconcept (multilingual-name &rest option-list)
  `(apply #'%make-virtual-concept ',multilingual-name ',option-list))

;;; macro to print v-object values from test
(defmacro <<< (class nn) 
  `(with-package :test 
     (<< '(,(intern (symbol-name class) :test) . ,nn))))

;;;---------------------------------------------------------- DEFVIRTUALRELATION

(defmacro defvirtualrelation (&rest ll)
  "used in augmented ontologies to be translated into OWL/Jena or OWL/SPARQL ~
   rules. MOSS uses methods instead."
  `(apply #'%make-virtual-relation ',ll))


:EOF