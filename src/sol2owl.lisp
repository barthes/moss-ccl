;;;-*- Mode: Lisp; Package: "SOL-OWL" -*-
;;;=================================================================================
;;;19/11/23
;;;		
;;;		S O L 2 O W L - (File sol2owl.LiSP)
;;;	
;;;=================================================================================

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
Producing an OWL file is done by loading a reconstructed MOSS file twice. The
first pass is used to build an index file and the second fime is used to 
produce the OWL output. The same approach is used to produce the HTML output.
|#
 
(in-package :SOL-OWL)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (import 'nodgui-user::solformat)
  )


;;;--------------------- macros for compiling the SOL file --------------------
;;; Those macros are used when compiling the input sol file and are defined
;;; for producing an OWL output

(defMacro defchapter (&rest mls)
  (declare (ignore mls))
  nil)

(defMacro defconcept (concept-name &rest option-list)
  `(apply #'make-concept ',concept-name ',option-list))

(defMacro defindividual (individual-name &rest option-list)
  `(apply #'make-individual ',individual-name ',option-list))

(defMacro defontology (&rest option-list)
  `(apply #'make-ontology ',option-list))

(defMacro defsection (&rest mls)
  (declare (ignore mls))
  nil)

(defmacro definstmethod (&rest ll)
  (declare (ignore ll))
  nil)

(defmacro defownmethod (&rest ll)
  (declare (ignore ll))
  nil)

(defmacro defuniversalmethodmethod (&rest ll)
  (declare (ignore ll))
  nil)

;;;---------- macros for processing virtual concepts and rules

(defMacro defruleheader (&rest option-list)
  `(apply #'make-rule-header ',option-list))

(defMacro defvirtualconcept (concept-name &rest option-list)
  `(apply #'make-virtual-concept ',concept-name ',option-list))

(defMacro defrule (&rest option-list)
  `(apply #'record-rule ',option-list))

(defMacro defvirtualattribute (property-name &rest option-list)
  `(apply #'make-virtual-attribute ',property-name ',option-list))

(defMacro defvirtualrelation (property-name &rest option-list)
  `(apply #'make-virtual-relation ',property-name ',option-list))

;;;----------------------------------------------------------------------------

(defMacro +result (ll)
  `(setq result (append ,ll result)))
(defMacro result+ (ll)
  `(setq result (append  result ,ll)))

(defMacro terror (cstring &rest args)
  `(throw :error (format nil ,cstring ,@args)))

;;;-------------------------------------------------------------------- cformat
;;; simplify internal code to take care of indentations

(defMacro cformat (control-string &rest args)
  `(push (indent *left-margin* (format nil ,control-string ,@args))
         result))

(defMacro mark-section (text nn)
  `(cformat "<!-- ~A -->~2%" (make-center ,text (max 0 (- ,nn 8)))
            ))

#|
? (mark-section "test" *separation-width*)
Expands as
(setq RESULT
      (cons (INDENT *LEFT-MARGIN*
                    (format NIL
                            "<!-- ~A --!>

"
                            (MAKE-CENTER "test" (max 0 (- *SEPARATION-WIDTH* 8)))))
            RESULT))
("  <!-- +++++++++++++++++++++++++++++++++++++ test +++++++++++++++++++++++++++++++++++++ --!>

|#

(defMacro noformat (&rest ll)
  "sink: does not print anything. used to check UNICODE problems"
  (declare (ignore ll))
  nil)

#|
(defMacro tkformat (cstring &rest args)
  "If Tk window is used, prints to Tk window, otherwise use standard output."
  ;; if using Tk, call the proper printing function
  #+tk`(ltk::append-text cl-user::*trace-window* (format nil ,cstring ,@args))
  ;; otherwise use standard output
  #-tk`(format *log-file* ,cstring ,@args)
  )
|#

(defMacro trformat (fstring &rest args)
  "debugging macro"
  `(solformat ,(concatenate 'string "~&;********** " fstring) ,@args))
 
(defMacro twarn (cstring &rest args)
    "inserts a message into the *error-message-list* making sure to return nil"
  `(progn (push (format nil ,cstring ,@args) *error-message-list*) nil))
 
(defMacro vformat (cstring &rest args)
    "prints something if verbose flag on only."
  `(if *cverbose* (solformat ,cstring ,@args)))
 
 ;(terror "error in ~A and N S" aaa sss)
 
;;; the following macro allows to call a set of exprs and catch any error in them
;;; the result if not in error should not be a string.
;;; The result of the macro is a list containing a string, whether a value or an error
 
(defMacro with-catching-error (error-context &rest body)
    "the result of body should not be a string."
  `(let (message)
     (setq message (catch :error ,@body))
     (if (stringp message)
       (list (format nil "error when ~A: ~A" ,error-context message))
       nil)))
 
;;; if we throw to :error in body, catches the resulting sring and inserts it into
;;; the error list to be printed later. Does not produce any other output
;;; if no error then OWL output is produced if *errors-only* is not set
 
(defMacro with-warning (arg-list &rest body)
  `(let (message)
     (setq message (catch :error ,@body :done))
     (if (stringp message)
       (twarn "~&;***** Error in:~S; ~A" ,(car arg-list) message)
       ;; otherwise print result
       (unless *errors-only* (rp result)))))

;;;================================================================================
;;; Global list to save definitions while processing data
;;;---------------------------- Globals ---------------------------------------

(defParameter *directory* *load-pathname*)

#|
(defParameter *xmlschema* "http://www.w3.org/2001:XMLSchema#")
(defParameter *xmlschema-string* "http://www.w3.org/2001:XMLSchema#string")

(defParameter *ontology-title* "ONTOLOGY" "default ontology title")

(defParameter *log-file* t "contains the path to the error log file")

(defParameter *language-tags* '(:en :fr :it :pl :*))
(defParameter *language-index-property* 
  '((:en . "isEnIndexOf") (:fr . "isFrIndexOf") (:it . "isItIndexOf")
    (:pl . "isPlIndexOf")))

;;; the following types are xsd types built in in OWL
(defParameter *attribute-types*
  '((:string . "string") (:boolean . "boolean") (:decimal . "decimal") 
    (:float . "float") (:double . "double") (:date-time . "dateTime")
    (:time . "time") (:date . "date") (:g-year-month . "gYearMonth")
    (:g-year . "gYear") (:g-month-day . "gMonthDay") (:g-day . "gDay")
    (:g-month . "gMonth") (:hex-Binary . "hexBinary") 
    (:base-64-binary . "base64Binary")
    (:any-uri . "anyURI") (:normalized-string . "normalizedString") 
    (:token . "token")
    (:language . "language") (:nm-token . "NMTOKEN") (:name . "Name")
    (:nc-name . "NCName") (:integer . "integer")  
    (:non-positive-integer . "nonPositiveInteger") 
    (:negative-integer . "negativeInteger") (:long . "long") (:int . "int")
    (:short . "short") (:byte . "byte") 
    (:non-negative-integer . "nonNegativeInteger")
    (:unsigned-long . "unsignedLong") (:unsigned-int . "unsignedInt")
    (:unsigned-short . "unsignedShort") (:unsigned-byte . "unsignedByte")
    (:positive-integer . "positiveInteger")))

(defParameter *attribute-list* ())
(defParameter *index-list* ()) ; JPB051009
(defParameter *relation-list* ())
(defParameter *concept-list* ())
(defParameter *individual-list* ())
(defParameter *super-class-list* ())
(defParameter *virtual-attribute-list* ()) ; JPB071231
(defParameter *virtual-composed-attribute-list* ()) ; JPB080105
(defParameter *virtual-concept-list* ()) ; JPB071231
(defParameter *virtual-property-list* ()) ; JPB080105
(defParameter *virtual-relation-list* ()) ; JPB071231
(defParameter *rule-list* ()) ; JPB080114
(defParameter *error-message-list* ())

(defParameter *index* (make-hash-table :test #'equal) "internal compiler table")

(defParameter *compiler-pass* 1 "compiler has 2 passes")
(defParameter *left-margin* 0)
(defParameter *output* t "default output is listener")
(defParameter *indent-amount* 2)
(defParameter *errors-only* nil)
(defParameter *cverbose* nil)

(defParameter *translation-table* '((#\' #\space)))
(defParameter *current-language* :en "default language is English")

(defParameter *separation-width* 90)

;;; globals for the first syntax check compiler pass
(defParameter *line* nil)
(defParameter *line-number* 0 "counting lines being read")
(defParameter *text*  "")
(defParameter *breaks* '("(defontology" "(defchapter" "(defsection"
                         "(defconcept" "(defindividual" ":EOF"
			 "(defruleheader" "(defvirtualconcept"
			 "(defvirtualrelation" "(defrule"))
(defParameter *syntax-errors* nil)

;;; globals for rules

(defParameter *r-output* t "channel to rule file, default is listener")
(defParameter *rule-error-list* nil "list of error strings")
(defParameter *rule-file* nil)
(defParameter *rule-builtin-operator-list*
  '((:jena (:gt . "greaterThan")(:lt . "lessThan")(:ge . "ge")(:le . "le")
           (:equal . "equal")(:not-equal . "notEqual")
           (:all . "all"))
    (:sparql (:gt . ">")(:lt . "<")(:ge . ">=")(:le . "<=")
             (:equal . "=")(:not-equal . "!="))))
(defParameter *rule-prefix-ref* nil)
(defParameter *user-defined-rule-operators* ())
|#


;;;============================= Service Functions ================================

;;;--------------------------------------------------------------------- alist?

(defun alist? (expr)
  "test whether expr is an a-list.
Arguments:
   expr: any lisp expr
Return:
   T if a-list, nil otherwise."
  (or (null expr)  ; nil is an alist
      (and (listp expr) (consp (car expr)) (alist? (cdr expr)))))

#|
? (alist? nil)
T
? (alist? '(1 2 3))
NIL
? (alist? '((1 . A)(2 B C D)))
T
|#
;;;---------------------------------------------------------------- compile-SOL

(defun compile-sol (infile &key outfile rule-outfile errors-only verbose 
                             rule-output)
  "compile a file containing SOL definition into an OWL file
Arguments:
   infile: string specifying the SOL file
   outfile (key): output OWL file, if nil input name is used, with OWL extension
   errors-only (key): if t does not generate output
   verbose (key): if t prints what it is doing
Return:
   :EOC"
  (declare (special *r-output*))
  ;; we select the file to compile and open temporary files in the same folder
  (let* ((sol-file (or infile (ccl::choose-file-dialog)))
         (tmp-file (or outfile ; ancillary file
                       (make-pathname
                        :device (pathname-device sol-file)
                        :directory (pathname-directory sol-file)
                        :name (pathname-name sol-file)
                        :type "tmp")))
	 (fasl-file (or outfile ; ancillary file
                       (make-pathname
                        :device (pathname-device sol-file)
                        :directory (pathname-directory sol-file)
                        :name (pathname-name sol-file)
                        :type "fasl")))
         (log-file (or outfile ; output of the OWL compiler
                       (make-pathname
                        :device (pathname-device sol-file)
                        :directory (pathname-directory sol-file)
                        :name (pathname-name sol-file)
                        :type "log")))
	 (owl-file (or outfile 
                       (make-pathname
                        :device (pathname-device sol-file)
                        :directory (pathname-directory sol-file)
                        :name (pathname-name sol-file)
                        :type "owl")))
	 (rule-file (or rule-outfile
                        (make-pathname
                         :device (pathname-device sol-file)
                         :directory (pathname-directory sol-file)
                         :name (concatenate 'string (pathname-name sol-file)
					    "-"
					    (symbol-name sol::*rule-format*))
                         :type "rules"))))
    ;; CCL has some problems when files already exist
    (if (probe-file tmp-file) (delete-file tmp-file))
    (if (probe-file fasl-file) (delete-file fasl-file))
    (if (probe-file owl-file) (delete-file owl-file))
    (if (probe-file log-file) (delete-file log-file))
    (if (probe-file rule-file) (delete-file rule-file))
    (print  ; in case of error
     (catch :error
       ;(print `(,sol-file ,owl-file ,rule-file))
        ;; create a working file without the leading UTF-8 mark
        (trim-file sol-file tmp-file)
        ;; open the output log file
        (with-open-file (*log-file* (or log-file t) :direction :output 
                                    :if-exists :supersede)
          ;(print `(+++ log-file/ ,log-file *log-file*/ ,*log-file*))
          ;; then check parents and quote syntax errors, print statistics into
          ;; the trace window
          (solformat "~%Checking Ontology Syntax~&~
                     ========================")
          (solformat (check-syntax tmp-file))
          (solformat "~%")
          ;; if errors, then quit
          (if *syntax-errors* (return-from compile-sol nil))

;(break "compile-sol ..... #0")

#|          ;; compile SOL file to expand macro definitions
          (multiple-value-bind (compiled-file warnings errors) (compile-file tmp-file)
            (declare (ignore warnings))
            (when (or errors (null compiled-file))
	      (solformat "~&Some format errors in the input file... 
Recompiling with trace on")
	      (compile-file sol-file :verbose t)
	      (return-from compile-sol nil))
|#
            (cond
             ;; simply check syntax errors no OWL file no RULE file
             ((and errors-only (not rule-output ))
              (let ((*output* nil))
                (compile-sol-body tmp-file errors-only verbose)))

             ;; no OWL output but output rule file
             ((and errors-only rule-output )
              (let ((*output* nil))
                (with-open-file (*r-output* (or rule-file t) :direction :output 
                                            :if-exists :supersede)
                  (compile-sol-body tmp-file errors-only verbose))))

             ;; output OWL file but not RULE file
             ((and (not errors-only) (not rule-output ))
              (with-open-file (*output* (or owl-file t) :direction :output 
                                        :if-exists :supersede)
                (compile-sol-body tmp-file errors-only verbose)))

             ;; output both OWL and RULE files
             ((and (not errors-only) rule-output )
              (with-open-file (*output* (or owl-file t) :direction :output 
                                        :if-exists :supersede)
                (with-open-file (*r-output* (or rule-file t) :direction :output 
                                            :if-exists :supersede)
                  (compile-sol-body tmp-file errors-only verbose)))))
            )
       ;; clean up only in non error cases
       ; this only happens when the directory is visible
       ;(delete-file fasl-file)
       (delete-file tmp-file)
       :EOC))))


;;; debug function
(defun cl-user::tt ()
  (setq sol-file "/Users/barthes/MOSS projects/applications/FAMILY-ONTOLOGY-191129.lisp")
  ;; for debugging purposes output is listener
  (setq cl-user::*tk* nil)
  (setq sol::*trace-window* nil)
  ;; compiling, no owl file output
  (moss::with-package :ow
    (compile-sol sol-file :verbose nil :errors-only nil))
  )

#|
;; compiling, no OWL output, verbose
(compile-sol "c:/SOL/tg-ontology15pc-utf8.sol" :verbose t :errors-only t)
;;; compiling, OWL output, terse
(compile-sol "c:/SOL/tg-ontology15pc-utf8.sol")

;;; compile errors only, verbose
(compile-sol-body "c:/SOL/tg-ontology15pc-utf8.fasl" t t)
;;; compile errors only, terse
(let ((*output* nil))
  (compile-sol-body "c:/SOL/tg-ontology15pc-utf8.fasl" t nil))
|#
;;;----------------------------------------------------------- compile-sol-body

(defun compile-sol-body (sol-file errors-only verbose)
  "main body of the compiler doing the job."
  (let ((*cverbose* verbose) result)
    
    ;; first reinitialize everything
    (reset)

    (vformat "~%ONTOLOGY~&~
              ========~2&")

    ;;===== first pass
    (setq *errors-only* errors-only)
    (vformat "~&Compiler Pass : 1~&~
              =================~&")
    (load sol-file)
    ;(index)

;(break "compile-sol-body after Pass 1..... #1")

    (unless errors-only 
      (mark-section "Classes" *separation-width*)
      (rp result)
      )
    (setq result nil)

    ;;===== second pass
    (setq *compiler-pass* 2)
    (vformat "~2%Compiler Pass : 2~&~
              =================~%")
    (load sol-file)
    (unless errors-only
      (mark-section "End Classes" *separation-width*)
      (rp result)
      )
    (index) ; debug control

;(break "compile-sol-body after Pass 2 ..... #2")

    ;;===== final processing
    ;;=== process attributes
    ;(print `(+++ att ,*attribute-list*))
    ;; reset left margin
    (setq *left-margin* *indent-amount*)
    (vformat "~2%Processing Attributes~&~
              =====================~&~S~&"
             *attribute-list*)
    (process-attributes)

;(break "compile-sol-body after process-attributes ..... #3")

    ;(print `(+++ REL ,*RELATION-list*))
    ;; reset left margin
    (setq *left-margin* *indent-amount*)
    (vformat "~2%Processing Relations~&~
              ====================~&~S~&"
             *RELATION-list*)
    (process-relations)

;(break "compile-sol-body after process-relations ..... #4")
    
    ;; third pass (for individuals)
    ;; reset left margin
    (setq *left-margin* *indent-amount*)
    (vformat "~2%Processing Individuals~&~
              ======================~&~S~&"
             *individual-list*)
    (process-individuals)

;(break "compile-sol-body after process-individuals ..... #5")
    
    ;; now indexes
    (setq *left-margin* *indent-amount*)
;;;    (vformat "~2%Processing Indexes~&~
;;;                ==================~&~S~2&"
;;;               *index-list*)
;;;(process-indexes)
    
    (unless errors-only 
      (setq result nil)
      (mark-section "End Ontology" *separation-width*)
      (rp result))
    ;; reset left margin
    (setq *left-margin* *indent-amount*)
    
    ;(index) ; debug control
    
    ;; and virtual attributes (2 kinds)
    (vformat "~2%Processing Virtual Attributes~&~
              =============================~&~S~2&"
             (append *virtual-attribute-list*
                     *virtual-composed-attribute-list*))
    (process-virtual-attributes)
    
    ;; and virtual relations
    (vformat "~2%Processing Virtual Relations~&~
              ============================~&~S~2&"
             *virtual-relation-list*)
    (process-virtual-relations)
    
    ;; print now virtual classes
    (vformat "~2%Processing Virtual Concepts~&~
              ===========================~&~S~2&"
             *virtual-concept-list*)
    (process-virtual-concepts)
    
    ;; print now additional rules
    (vformat "~2%Processing Rules~&~
              ================~&~S~2&"
             *rule-list*)
    (process-rules)
    
    ;; close RDF section
    (unless errors-only 
      (make-trailer))
    
    (solformat "~2%Ontology Errors~&~
               ===============")
    (errors)
    
    (solformat "~2%End~&~
               ===")
    :eoc))

;;;----------------------------------------------------------------------- ltab
;;; introduces left tab into printed output
(defun ltab ()
  (decf *left-margin* *indent-amount*))

;;;----------------------------------------------------------------------- rtab
;;; introduces right tab into printed output
(defun rtab ()
  (incf *left-margin* *indent-amount*))

;;;------------------------------------------------------------------------- rp
;;; reverse print the argument list

(defun rp (ll) (progn (mapcar #'(lambda(xx) ;(terpri)(princ xx)) 
                                  (format *output* "~&~A" xx))
                              (reverse ll)) :END))

;;;--------------------------------------------------------------------- errors

(defun errors () 
  "print a list of error messages when there are some (global *error-message-list*)"
  (if *error-message-list*
    (progn (mapc #'(lambda (xx) (solformat "~%~A" xx))
                 (reverse *error-message-list*)) :END)
    (solformat "~%None.")))

;;;---------------------------------------------------------------- make-center

(defun make-center (text nn)
  "builds a string nn char long with centered text between + signs."
  (let* ((ii (max 0 (floor (/ (- nn 4 (length text)) 2))))
         (jj (max 0 (- nn 4 ii (length text)))))
    (format nil "~A ~A ~A"
            (make-string ii :initial-element #\+)
            text
            (make-string jj :initial-element #\+))))

#|
? (make-center "Centered Title" 60)
"----------------------------- Centered Title -----------------------------"
|#

;;;------------------------------------------------------------------------ tt
;;; test the compiler on the sol-test.lisp file

(defun tt (&key (verbose t) (errors-only nil))
  (catch :error (compile-sol (merge-pathnames "sol-test.sol" *directory*) 
                             :outfile (merge-pathnames "sol-test.owl" *directory*)
                             :verbose verbose :errors-only errors-only))
  (format t "~2&Internal Index table~&====================")
  (index))

;;;----------------------------------------------------------------------- ttf
;;; test the compiler on the sol-test-full.lisp file

(defun ttf (&key (verbose t) (errors-only nil))
  (catch :error 
    (compile-sol (merge-pathnames "sol-test-full.sol" *directory*)
		 :outfile (merge-pathnames "sol-test-full.owl" *directory*)
		 :verbose verbose :errors-only errors-only)))

(defun tt15 (&key (verbose t) (errors-only nil))
  (catch :error 
    (compile-sol (merge-pathnames "TG-ontology15-utf8pc.sol" *directory*)
		 :outfile (merge-pathnames "TG-ontology15pc-utf8.owl" *directory*)
		 :verbose verbose :errors-only errors-only)))

;;;================================= functions ====================================

;;;----------------------------------------------------------------- check-type

(defun check-sol-type (value type)
  "check if the value has the type specified by type. If nt, throws an error.
Arguments:
   value: any expression
   type: one of the allowed SOL types, e.g. :integer
Return:
   T if OK, nil otherwise."
  (case type
    (:integer (integerp value))
    (:float (floatp value))
    (:string (stringp value))
    (:date (numberp value))
    (:name (stringp value))
    (:g-year (numberp value))
    (t (terror "unknown attribute type: ~S" type))))

#|
? (check-sol-type 12 :integer)
T
? (check-sol-type "12" :integer)
NIL
|#
;;;--------------------------------------------------------------- check-syntax

(defun check-syntax (sol-file)
  "checks structural syntax of SOL definitions, i.e. parents and quotes.
   In case of error *syntax-errors* is set to T.
Argument:
   sol-file: name of sol-file
Return:
   string stating how many entries were checked."
  (let ((count 0)
	(syntax-error-count 0)
	expr text-string (line-mark 0))
    ;; reset text and syntax error flag and line count
    (setq *text* nil *syntax-errors* nil *line-number* 0)
    ;; open file
    (with-open-file (ss sol-file :direction :input)
      ;; initialize first line
      (setq *line* (read-next-valid-line ss))
      ;(print *line*)
      (setq *text* (list *line*))
      ;; loop on definitions
      (catch :done
        (loop
          ;; read next line
          (setq *line* (read-next-valid-line ss))
          (print `(===> ,*line*))
          ;; read-next-valid-line returns :eof on when end of file is reached
	  (if (eql *line* :eof) (return))
          ;; otherwise check for breaks (lines starting with defxxx)
          (if (member t (mapcar 
		         #'(lambda (xx) (and 
				         (search xx *line* :test #'string-equal)
				         t))
		         *breaks*))
	    ;; check for valid definition
	    (progn
	      ;; we got something
	      ;; mark line position
	      (setq line-mark *line-number*)
	      (incf count)
	      (setq text-string (format nil "~{~A~&~}" (reverse *text*)))
	      ;(format t "~&~{~A~&~}" (reverse *text*))
	      ;; catch the error condition while reading from string
	      ;; this one catches missing closing parents or unbalanced quotes
	      (setq expr (handler-case (read-from-string text-string)
			   (error () :error)))
	      (case expr
	        (:error
		 (solformat
		  "~2%*** ERROR in expression starting at line ~S of the .tmp file"
                  line-mark)
		 ;; we assume that the first line is defxx, and the second
		 ;; one is a mln, and hope that no special char will cause a hangup
		 (solformat 
		  "~%   check parentheses and quotes in:~&~{~A~&~}~&..." 
		  (subseq (reverse *text*) 0 (min 5 (length *text*))))
		 (setq *syntax-errors* t)
		 (incf  syntax-error-count))
	        )
              
	      ;; if last read line was :EOF or we reached end of file
	      ;; then we are finished
	      (if (or (string-equal *line* ":EOF") ; last line contained :EOF
		      (eql *line* :eof)) ; we reached end of file?
                (throw :done nil))
	      ;; reset *text*
	      (setq *text* (list *line*))
	      )
	    ;; otherwise simply push line into text
	    (push *line* *text*))
          )))
    ;; quit, returning a string to be printed
    (format nil "~%We checked ~S entries, and found ~S error(s) ~
                 with parents or quotes." 
	    count syntax-error-count)))

;(check-syntax sol-file)
;;;------------------------------------------------- compute-transitive-closure

(defun compute-transitive-closure (candidates property &optional result)
  "computes the transitive closure of a class using *index*
Arguments:
   class: a class OWL ID; can be a list
   property: the SOL id e.g. :is-a
   result: a list of classes already explored
Return:
   the list of OWL IDs."
  (cond ((null candidates) (reverse result))
        ((not (listp candidates)) 
         (compute-transitive-closure (list candidates) property result))
        ;; when 1st element is not a class, complain
        ((not (index-get (car candidates) :class :name))
         (terror "While computing transitive closure ~S is not a class." 
                 (car candidates)))
        ((member (car candidates) result :test #'equal)
         (compute-transitive-closure (cdr candidates) property result))
        (t
         (compute-transitive-closure
          (append (index-get (car candidates) :class property)
                  (cdr candidates))
          property
          (cons (car candidates) result)))))

#|
? (index)

("Ville" ((:REL (:ID "hasTown")) (:CLASS (:ID "Z-City")))) 
("Z-City" ((:CLASS (:IS-A "Z-Territory") (:NAME :EN "City; town" :FR "ville; cité")))) 
("Name" ((:ATT (:ID "hasName")))) 
("Conurbation" ((:CLASS (:ID "Z-Conurbation" "Z-Conurbation")))) 
("Cité" ((:CLASS (:ID "Z-City")))) 
("Z-Territory" ((:CLASS (:NAME :EN "territory")))) 
("Z-Conurbation" ((:CLASS (:NAME :EN "Conurbation" :FR "conurbation")))) 
("City" ((:CLASS (:ID "Z-City")))) 
("Territory" ((:CLASS (:ID "Z-Territory")))) 
("Town" ((:REL (:ID "hasTown")) (:CLASS (:ID "Z-City")))) 
NIL
? (compute-transitive-closure "Z-City" :is-a)
("Z-City" "Z-Territory")
? (catch :error (compute-transitive-closure "Z-City" :isa))
("Z-City")
? (catch :error (compute-transitive-closure "City" :isa))
"While computing transitive closure \"City\" is not a class."
|#
;;;--------------------------------------- extract-documentation-from-fragments

(defun extract-documentation-from-fragments (fragments)
  "takes a list of fragments (attributes or relations) and extract the ~
      documentation  multilingual strings. Fuses the documentation into a single ~
      multilingual string.
Arguments:
   fragments: a list of fragments
Return:
   a multi-lingual string or nil if there is no documentation."
  (let (doc result language language-doc)
    ;; get the list of strings
    (setq doc (mapcar #'(lambda(xx) (cdr (assoc :doc (cdr xx)))) fragments))
    ;; merge fragments
    (dolist (tag *language-tags*)
      (setq language (remove nil
                             (mapcar #'(lambda(xx) (cadr (member tag xx))) doc)))
      ;(print language)
      (setq language-doc (make-single-string 
                          (delete-duplicates language :test #'string-equal)))
      (unless (string-equal "" language-doc)
        (+result (list tag language-doc))))
    result))

;;;*----------------------------------------------- extract-attribute-fragments

(defun extract-attribute-fragments ()
  "extracts from the *attribute-list* all fragments defining the same attribute.
   To do so, used the names. If two fragments share the same name they apply to ~
   the same attribute. E.g. (:en \"sex\" :fr \"genre\") (:fr \"sexe; genre\")
Arguments:
     none
Return:
    a list of fragments with merged mln names in front.
Side-effet:
    empties *attribute-list*"
  (let (found-some?
        (result (list (pop *attribute-list*)))
        (attributes *attribute-list*)
        names att )
    (if (null (car result)) (return-from extract-attribute-fragments nil))
    ;; when the attribute list is empty return nil (should not have been called ?)
    (unless attributes (return-from extract-attribute-fragments 
                         (cons (cadar result) result)))
    ;; extract first value e.g. ((:en "sex" :fr "sex; genre") (:unique) ...)
    (setq att (cadar result))
    ;; the extracting loop may have several passes
    (loop
      (dolist (item attributes)
        (when (mln::mln-equal att (cadr item))
          (setq names (mln::mln-merge  att (cadr item)))
          (push item result)
          (setq attributes (delete item attributes :test #'equal))
          (setq *attribute-list* (remove item *attribute-list*))
          (setq att names)
          (setq found-some? t)))
      (if found-some?
        (setq found-some? nil)
        (return)
        ))
    ;; exit
    (cons att result)))

#|
? (let ((fragments '(("Z-Person" (:EN "referent" :FR "référent") (:TO "person")
                                 (:DOC :EN "A Person is a REFERENT for another 
                                            person if this person is in ~
                                            charge of keeping track of what happens
                                            to the other person."))
                     ("Z-Mayor" (:en "referent") 
                                (:doc :en "a Mayor is a REFERENT ~
                                           for the prefect of the department."
                                       :fr "le maire est ..."))
                     ("Z-xxx" (:fr "le maire est ..."))
                    )))
  (extract-documentation-from-fragments fragments))
                    
(:FR "le maire est ..." :EN
 "A Person is a REFERENT for another person if this person is in ~
  charge of keeping track of what happens to the other person. ; a Mayor is a REFERENT ~
  for the prefect of the department.")
? 
|#
;;;------------------------------------------------- extract-relation-fragments

(defun extract-relation-fragments ()
  "extracts from the *relation-list* all fragments defining the same relation
   To do so, used the names. If two fragments share the same name they apply to ~
      the same relation 
   E.g. (:en \"brother\" :fr \"frère\") (:fr \"frangin; frère\")
  Arguments:
     none
  Return:
  a list of relations with merged names in front."
  (let (found-some?
        (result (list (pop *relation-list*)))
        (relations *relation-list*)
        names rel)
    (if (null (car result)) (return-from extract-relation-fragments nil))
    ;; extract first value e.g. ((:en "sex" :fr "sex; genre") (:unique) ...)
    (setq rel (cadar result))
    ;; remove :name if in front
    (if (eql :name (car rel)) (setq rel (cdr rel)))
    ;; when the attribute list is empty return nil (should not have been called ?
    (unless relations (return-from extract-relation-fragments 
                        (cons rel result)))
    ;; the extracting loop may have several passes
    (loop
      (dolist (item relations)
        (when (mln::mln-equal rel (cadr item))
          (setq names (mln::mln-merge  rel (cadr item)))
          (push item result)
          (setq relations (delete item relations :test #'equal))
          (setq *relation-list* (remove item *relation-list*))
          (setq rel names)
          (setq found-some? t)))
      (if found-some?
        (setq found-some? nil)
        (return)  ; get out of the loop
        ))
    ;; exit
    (cons rel result)))

#|
? (setq *relation-list* 
         '(("Z-Person" (:en "brother" :fr "frangin; frère") "Person")
           ("Z-Département" (:fr "région") "région")
           ("Z-Tiger" (:fr " frère " :it "fratello")(:min 1) "tiger")
           ("Z-Unicorn" (:pl "?" :en "  brother") "unicorn")))
(("Z-Person" (:EN "brother" :FR "frangin; frère") "Person")
 ("Z-Département" (:FR "région") "région")
 ("Z-Tiger" (:FR " frère " :IT "fratello") (:MIN 1) "tiger")
 ("Z-Unicorn" (:PL "?" :EN "  brother") "unicorn"))
? (catch :error (extract-relation-fragments))
((:IT "fratello" :EN "Brother" :FR "Frangin; Frère" :PL "?")
 ("Z-Unicorn" (:PL "?" :EN "  brother") "unicorn")
 ("Z-Tiger" (:FR " frère " :IT "fratello") (:MIN 1) "tiger")
 ("Z-Person" (:EN "brother" :FR "frangin; frère") "Person"))
? *relation-list*
(("Z-Département" (:FR "région") "région"))
? (catch :error (extract-relation-fragments))
((:FR "région") ("Z-Département" (:FR "région") "région"))
? *relation-list*
NIL
? (catch :error (extract-relation-fragments))
NIL
|#
;;;-------------------------------------------------------------- extract-names

(defun extract-names (name-string)
  "extract names separated by a semi column from name-string.
Argument:
   name-string: a string of names possible separated be semi-columns
Return:
   a list of strings corresponding to each single name
Error:
   if not a string or empty throws a message string to :error."
  (let (text result end word)
    (when (or (not (stringp name-string))
              (equal "" (setq text (string-trim '(#\space) name-string))))
      (terror "Bad name string: ~S" name-string))
    ;; extract names
    (loop
      (setq end (position #\; text))
      (cond 
       ;; still some semi-column?
       ((numberp end)
        ;; yes extract word
        (setq word (string-trim '(#\space) (subseq text 0 end))
              text (subseq text (1+ end)))
        (unless (equal "" word) (push word result)))
       ;; finished.
       (t
        (setq word (string-trim '(#\space) text))
        (unless (equal "" word) (push word result))
        (unless result
          (terror "Empty name string: ~S" name-string))
        (return (reverse result)))))))

#|
? (EXTRACT-NAMES "albert le voisin")
("albert le voisin")
? (EXTRACT-NAMES "albert le voisin ; albertine")
("albert le voisin" "albertine")
? (EXTRACT-NAMES "albert le voisin ; albertine;  Zoe oui   ")
("albert le voisin" "albertine" "Zoe oui")
? (catch :error (MOSS::EXTRACT-NAMES " ;    "))
"Empty name string: \" ;    \""
|#
;;;--------------------------------------------------------------- get-all-name

(defun get-all-names (name-string)
  "extract all names from the name string. Names are separated by semi-columns.
   E.g. \"identity card ; papers\" returns (\"identity card \" \"papers\"
Argument:
   name-string
Return:
   string with the first name." 
  (let ((remaining-string name-string) result pos)
    (loop
      ;; remove trailing spaces
      (setq remaining-string (string-trim '(#\space) remaining-string))
      ;; get position of the next ";"
      (setq pos (position #\; remaining-string))
      (cond
       ;; if non nil, extract first name
       (pos
        (push (subseq remaining-string 0 pos) result)
        (setq remaining-string (subseq remaining-string (1+ pos))))
       ;; otherwise, last name return
       (t
        (push remaining-string result)
        (return))))
    (reverse result)))

#|
? (GET-ALL-NAMES "frangin; frère")
("frangin" "frère")
|#
;;;*------------------------------------------------------ get-attribute-domain

(defun get-attribute-domain (att-fragments)
  "get the list of domains from the fragments.
Arguments:
   att-fragments: list of att specs
Return:
   list of domain OWL IDs."
  (delete-duplicates (mapcar #'car att-fragments) :test #'string-equal))

#|
? (setf (gethash "Person" *index*) "Z-Person")
"Z-Person"
? (setf (gethash "Département" *index*) "Z-Department")
"Z-Département"
? (setf (gethash "Tiger" *index*) "Z-Tiger")
"Z-Tiger"
? (setf (gethash "Unicorn" *index*) "Z-Unicorn")
"Z-Unicorn"
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique))
          ("Département" (:fr "département"))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1))
          ("Unicorn" (:pl "?" :en "  sex"))))
(("Person" (:EN "sex" :FR "sexe; genre") (:UNIQUE))
 ("Département" (:FR "département")) ("Tiger" (:FR " Genre " :IT "sexo") (:MIN 1))
 ("Unicorn" (:PL "?" :EN "  sex")))
? (get-attribute-domain *attribute-list*)
("Person" "Department" "Tiger" "Unicorn")
|#
;;;*------------------------------------------------------ get-attribute-one-of
;;; the attribute :one-of option could be an mln specifying different values
;;; for different languages; in that case all mln should be identical; this 
;;; should be corrected.

(defun get-attribute-one-of (att-fragments)
  "get the one-of option, should be the same. Type must be checked separatly.
Arguments:
   att-fragments: list of att specs
Return:
   nil if no, :one-of option <option> if same for all
Error:
   throws to an :error tag"
  (let ((range (mapcar #'(lambda (xx) (assoc :one-of (cdr xx)))
                       att-fragments)))
    ;; if all the same next line should condense into one element
    (setq range (delete-duplicates range :test #'equal))
    ;; check possible errors
    (cond
     ;; (nil) indicates no one-of option
     ((null (car range)))
     ;; if more than one element, error
     ((cdr range)
      (terror "inconsistent range one-of definitions:~&~S" 
              att-fragments))
     ;; if single option, elements of the list cannot be MLN
     ((multilingual-string? (cadar range))
      (terror "range cannot be a multilingual string in: ~&~S"
              att-fragments))
     ;; range must not be nil
     ((null (cdar range))
      (terror "null range in :one-of option: ~&~S"
              att-fragments)))
    ;; return range
    (car range)))

#|
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique)
           (:one-of (1 2 3)))
          ("Département" (:fr "département")(:type :integer)
           (:one-of (1 2 3)))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1)(:type :string)
           (:one-of (1 2 3)))
          ("Unicorn" (:pl "?" :en "  sex")
           (:one-of (1 2 3)))))
(("Person" (:EN "sex" :FR "sexe; genre") (:UNIQUE) (:ONE-OF (1 2 3)))
 ("Département" (:FR "département") (:TYPE :INTEGER) (:ONE-OF (1 2 3)))
 ("Tiger" (:FR " Genre " :IT "sexo") (:MIN 1) (:TYPE :STRING) (:ONE-OF (1 2 3)))
 ("Unicorn" (:PL "?" :EN "  sex") (:ONE-OF (1 2 3))))
? (catch :error ( get-attribute-one-of *attribute-list*))
(:ONE-OF (1 2 3))
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique)
           (:one-of (:en "sex" :fr "sexe; genre")))
          ("Département" (:fr "département")(:type :integer)
           (:one-of (:en "sex" :fr "sexe; genre")))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1)(:type :string)
           (:one-of (:en "sex" :fr "sexe; genre")))
          ("Unicorn" (:pl "?" :en "  sex")
           (:one-of (:en "sex" :fr "sexe; genre")))))
? (catch :error ( get-attribute-one-of *attribute-list*))
"range cannot be a multilingual string in: 
((\"Person\" (:EN \"sex\" :FR \"sexe; genre\") (:UNIQUE) (:ONE-OF))
 (\"Département\" (:FR \"département\") (:TYPE :INTEGER) (:ONE-OF))
 (\"Tiger\" (:FR \" Genre \" :IT \"sexo\") (:MIN 1) (:TYPE :STRING) (:ONE-OF))
 (\"Unicorn\" (:PL \"?\" :EN \"  sex\") (:ONE-OF)))"
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique)
           (:one-of (:en "sex" :fr "sexe; genre")))
          ("Département" (:fr "département")(:type :integer)
           (:one-of (:en "sex" :fr "sexe; genre")))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1)(:type :string)
           (:one-of (:en "sex" :fr "sexe; genre")))
          ("Unicorn" (:pl "?" :en "  sex")
           (:one-of (:en "sex" :fr "sexe; genre")))))
? (catch :error ( get-attribute-one-of *attribute-list*))
"range cannot be a multilingual string in: 
((\"Person\" (:EN \"sex\" :FR \"sexe; genre\") (:UNIQUE)
  (:ONE-OF (:EN \"sex\" :FR \"sexe; genre\")))
 (\"Département\" (:FR \"département\") (:TYPE :INTEGER)
  (:ONE-OF (:EN \"sex\" :FR \"sexe; genre\")))
 (\"Tiger\" (:FR \" Genre \" :IT \"sexo\") (:MIN 1) (:TYPE :STRING)
  (:ONE-OF (:EN \"sex\" :FR \"sexe; genre\")))
 (\"Unicorn\" (:PL \"?\" :EN \"  sex\") (:ONE-OF (:EN \"sex\" :FR \"sexe; genre\"))))"
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique))
          ("Département" (:fr "département")(:type :integer))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1)(:type :string))
          ("Unicorn" (:pl "?" :en "  sex"))))
? (catch :error ( get-attribute-one-of *attribute-list*))
NIL
|#
;;;*-------------------------------------------------------- get-attribute-type

(defun get-attribute-type (att-fragments)
  "get the various definitions for the attribute range. Should be identical. ~
      If not then we have an error. Literal is given by the :type option. ~
      Default is (:type :string). Option :one-of is incompatble with :type.
Arguments:
   att-fragments: list of att specs
Return:
   nil or range spec if all identical, e.g. :integer"
  (let ((range (mapcar #'(lambda (xx)(or (assoc :type (cdr xx))
                                         '(:type :string) ; default value
                                         ))
                       att-fragments)))
    ;; if all the same next line should condense into one element
    (setq range (delete-duplicates range :test #'equal))
    ;; if more than one element, error
    (when (cdr range)
      (terror "inconsistent range definitions ~S" att-fragments))
    ;; return single attribute
    (cadar range)))

#|
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique))
          ("Département" (:fr "département"))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1)(:type :string))
          ("Unicorn" (:pl "?" :en "  sex"))))
(("Person" (:EN "sex" :FR "sexe; genre") (:UNIQUE))
 ("Département" (:FR "département"))
 ("Tiger" (:FR " Genre " :IT "sexo") (:MIN 1) (:TYPE :STRING))
 ("Unicorn" (:PL "?" :EN "  sex")))
? (get-attribute-type *)
:STRING
? (setq *attribute-list* 
        '(("Person" (:en "sex" :fr "sexe; genre") (:unique))
          ("Département" (:fr "département")(:type :integer))
          ("Tiger" (:fr " Genre " :it "sexo")(:min 1)(:type :string))
          ("Unicorn" (:pl "?" :en "  sex"))))
(("Person" (:EN "sex" :FR "sexe; genre") (:UNIQUE))
 ("Département" (:FR "département") (:TYPE :INTEGER))
 ("Tiger" (:FR " Genre " :IT "sexo") (:MIN 1) (:TYPE :STRING))
 ("Unicorn" (:PL "?" :EN "  sex")))
? (get-attribute-type *)
> Error: Can't throw to tag :ERROR .
> While executing: GET-ATTRIBUTE-RANGE
> Type Command-. to abort.
See the RestartsÉ menu item for further choices.
|#
;;;------------------------------------------------------------- get-first-name

(defun get-first-name (name-string)
  "extracts the first name from the name string. Names are separated by ~
      semi-columns.
   E.g. \"identity card ; papers\" returns \"identity card \"
Argument:
   name-string
Return:
   string with the first name."
  (subseq name-string 0 (position #\; name-string)))

#|
? (get-first-name "frangin; frère")
"frangin"
|#
;;;-------------------------------------------------------------- get-id-string

(defun get-id-string (ll)
  "extracts from a multilingual list the string that will be used to build ~
      an OWL ID.
The algorithm is to take the first English name, of else the first name in the ~
      list.
Arguments:
   ll: a mls, e.g. (:fr \"masculin\" :en \"male   AGAIN\")
Return:
   a candidate string to be further processed."
  (let ((ml-string (multilingual-name? ll)))
    (unless ml-string
      (terror "bad MLS format in ~S" ll))
    (or (get-first-name (cadr (member :en ml-string))) 
        (get-first-name (cadr ml-string)))))

#|
? (get-id-string '(:fr "masculin" :en "male   AGAIN; man"))
"male   AGAIN"
? (get-id-string '(:fr "masculin; mec" :it "male   AGAIN"))
"masculin"
|#
;;;-------------------------------------------------------- get-language-string

(defun get-language-string (multilingual-string language-tag)
  "get the string corresponding to the language tag (return English as default).
Arguments:
   multilingual-string: e.g. (:en \"town\" :fr \"ville\)
   language-tag: e.g. :fr 
Return:
   string or empty-string"
  (or (cadr (member language-tag multilingual-string)) 
      (cadr (member :en multilingual-string))
      (cadr multilingual-string)
      "?"))

#|
? (get-language-string '(:en "town" :fr "ville") :fr)
"ville"
? (get-language-string '(:en "town" :fr "ville") :it)
"town"
|#
;;;------------------------------------------------------------------- get-name

(defun get-name (class-id)
  "recovers one of the class names from the class-id by looking into the index ~
      table.
Argument:
   class-id: e.g. \"Z-FrenchDepartment\"
Return:
   one of the class names, e.g. \"French Department\"."
  (get-id-string (index-get class-id :class :name)))

#|
? (get-name "Z-FrenchDepartment")
"French department"
|#
;;;-------------------------------------------------------- get-relation-domain

(defun get-relation-domain (rel-fragments)
  "gets the list of domains from the fragments.
Arguments:
   att-fragments: list of rel specs
Return:
   list of domain OWL IDs."
  (delete-duplicates (mapcar #'car  rel-fragments) :test #'string-equal))

#|
? (get-relation-domain '(
 ("Z-Unicorn" (:PL "?" :EN "  brother") "unicorn")
 ("Z-Tiger" (:FR " frère " :IT "fratello") (:MIN 1) "tiger")
 ("Z-Person" (:EN "brother" :FR "frangin; frère") "Person")))
("Z-Unicorn" "Z-Tiger" "Z-Person")
|#
;;;-------------------------------------------------------- get-relation-one-of

(defun get-relation-one-of (att-fragments)
  "get the one-of option, should be the same. Type must be checked separatly.
   Since the one-of options are difficult to compare, due tot the possibility ~
      of synonyms in the multilingual names, when they are used at different ~
      places, they should be made a separate class. Thus, if not defined in ~
      exactly the same terms we declare an error.
Arguments:
   att-fragments: list of att specs
Return:
   nil if no, :one-of option <option> if same for all
Error:
   throws to an :error tag"
  (let ((range (mapcar #'(lambda (xx) (get-relation-one-of-local (cddr xx)))
                       att-fragments)))
    ;; if all the same next line should condense into one element
    ;;***** this test is too crude since the spelling could be different and in case
    ;; of relerences to existing instance, the string may be different but refer
    ;; to the same object
    (setq range (delete-duplicates range :test #'equal))
    ;(print range)
    ;; check possible errors
    (cond
     ;; if more than one element, error
     ((cdr range)
      (terror "lacking or possibly different one-of options in:~&~S~
               ~&make a separate class \"xxx Type\" for cases." 
              att-fragments))
     ;; (nil) indicates no one-of option
     ((null (car range)))
     ;; if single option, elements of the list should strings
     ((every #'stringp (cdar range)))
     ;; ... or MLS
     ((some #'null (mapcar #'multilingual-string? (cdar range)))
      (terror "range ~S should be a multilingual string in: ~&~S"
              (cdar range) att-fragments)))
    ;; return range
    (car range)))

#|
? (catch :error
    (get-relation-one-of '(
 ("Z-Unicorn" (:PL "?" :EN "  brother") "day" (:min 1))
 ("Z-Person" (:en "sex") (:one-of (:en "male")(:en "female")))
 ("Z-Tiger" (:FR " genre " :IT "sexo") (:unique) (:one-of (:en "male")(:en "female")))
 )))
"possible different one-of options:
((\"Z-Unicorn\" (:PL \"?\" :EN \"  brother\") \"day\" (:MIN 1))
 (\"Z-Person\" (:EN \"sex\") (:ONE-OF (:EN \"male\") (:EN \"female\")))
 (\"Z-Tiger\" (:FR \" genre \" :IT \"sexo\") (:UNIQUE)
  (:ONE-OF (:EN \"male\") (:EN \"female\"))))
make a separate class \"xxx Type\" for cases."
? (catch :error
    (get-relation-one-of '(
 ("Z-Person" (:en "sex") (:one-of (:en "male")(:en "female")))
 ("Z-Tiger" (:FR " genre " :IT "sexo") (:unique) (:one-of (:en "male")(:en "female")))
 )))
(:ONE-OF (:EN "male") (:EN "female"))
? (catch :error
    (get-relation-one-of '(
 ("Z-Person" (:en "sex") (:one-of (:en "male")(:en "female")))
 ("Z-Tiger" (:FR " genre " :IT "sexo") (:unique) (:one-of (:en "male")(:en "female" :fr "féminin")))
 )))
"lacking or possibly different one-of options in:
((\"Z-Person\" (:EN \"sex\") (:ONE-OF (:EN \"male\") (:EN \"female\")))
 (\"Z-Tiger\" (:FR \" genre \" :IT \"sexo\") (:UNIQUE)
  (:ONE-OF (:EN \"male\") (:EN \"female\" :FR \"féminin\"))))
make a separate class \"xxx Type\" for cases."
|#
;;;-------------------------------------------------- get-relation-one-of-local

(defun get-relation-one-of-local (option-list)
  "extract the :one-of option from the option list. Required since some options
are not lists.
Arguments:
   option-list: e.g. (\"day\" (:min 1)) 
                or ((:unique) (:one-of (:en \"male\")(:en \"female\")))
Result:
   nil or :one-of clause."
  (unless (listp option-list)
    (terror "bad relation format"))
  (dolist (option option-list)
    (if (and (listp option)(eql :one-of (car option)))
      (return-from get-relation-one-of-local option))))

#|
? (get-relation-one-of-local (cddr '("Z-Unicorn" (:PL "?" :EN "  brother") "day" (:min 1))))
NIL
? 
GET-RELATION-ONE-OF-LOCAL
? (get-relation-one-of-local (cddr '("Z-Unicorn" (:PL "?" :EN "  brother") "day" (:min 1))))
NIL
? (get-relation-one-of-local (cddr '("Z-Person" (:en "sex") (:one-of (:en "male")(:en "female")))))
(:ONE-OF (:EN "male") (:EN "female"))
? (get-relation-one-of-local (cddr '("Z-Tiger" (:FR " genre " :IT "sexo") (:unique) (:one-of (:en "male")(:en "female")))))
(:ONE-OF (:EN "male") (:EN "female"))
|#
;;;--------------------------------------------------------- get-relation-range

(defun get-relation-range (rel-fragments)
  "get the list of ranges from the fragments.
Arguments:
   att-fragments: list of rel specs
Return:
   list of domain OWL IDs."
  (let (class-list class-id lres)
    ;; each cdr of fragment should be an a-list, otherwise error
    (dolist (fragment rel-fragments)
      (unless (alist? (cdr fragment))
        (terror "bad syntax in fragment ~S for relation ~S" 
                fragment (car fragment))))
    ;; get the class-list
    (setq class-list (mapcan #'(lambda(xx)(cdr (assoc :to (cdr xx)))) 
                             rel-fragments) )
    ;; if no range, then we might have a one-of option
    (unless class-list (return-from get-relation-range nil))
    ;; check that syntax was OK all must be strings
    (dolist (item class-list)
      ;; we use dolist to locate possible error
      (if (stringp item)
        (push item lres)
        (terror "bad range class format ~S in some relations: ~&~S" 
                item rel-fragments)))
    ;; clean up list normalizing strings
    (setq class-list (delete-duplicates 
                      (mapcar #'make-index-string lres) :test #'string-equal))
    (setq lres nil)
    ;; check now that strings correspond to actual classes
    (dolist (item class-list)
      (if
        (setq class-id (car (index-get item :class :id)))
        (push class-id lres)
        (terror "undefined range class ~S in some relations: ~&~S" 
                item rel-fragments)))
    ;; make sure we do not return the same class id several times
    (delete-duplicates lres :test #'string-equal) ; JPB051029
    ))

#|
? (tt)
...
? (catch :error (get-relation-range '(
 ("Z-Unicorn" (:PL "?" :EN "  brother") (:min 1)(:to "day"))
 ("Z-Tiger" (:FR " frère " :IT "fratello") (:to "time unit") (:MIN 1) )
 ("Z-Person" (:EN "brother" :FR "frangin; frère") (:to "  time Unit")))))
("Z-Day" "Z-TimeUnit")
? (catch :error (get-relation-range '(
 ("Z-Unicorn" (:PL "?" :EN "  brother") (:to "day") (:min 1))
 ("Z-Tiger" (:FR " frère " :IT "fratello") (:to "tiger") (:MIN 1) )
 ("Z-Person" (:EN "brother" :FR "frangin; frère") (:to "  time Unit")))))
"undefined range class \"Tiger\" in some relations: 
((\"Z-Unicorn\" (:PL \"?\" :EN \"  brother\") (:TO \"day\" \"tiger\" \"  time Unit\")
                (:MIN 1))
 (\"Z-Tiger\" (:FR \" frère \" :IT \"fratello\") (:TO \"tiger\" \"  time Unit\") (:MIN 1))
 (\"Z-Person\" (:EN \"brother\" :FR \"frangin; frère\") (:TO \"  time Unit\")))"
? (get-relation-range '(("Z-Address" (:FR "lieu") (:TO "pays"))
    ("Z-PostalAddress" (:EN "location" :FR "lieu") (:TO "country"))))
("Z-Country")
|#
;;;----------------------------------------------------- get-xsd-attribute-type

(defun get-xsd-attribute-type (type-keyword prop-id concept-id)
  "get the string representing the xsd type from the equivalent keyword. ~
      All keywords are in the *attribute-types* global variable.
   E.g. :g-year returns (\"gYear\"
   Throws an error to :error when type is unknown.
Argument:
   type-keyword: a keyword specifying the type
Return:
   xsd type string." 
  (let ((xsd-type-string (cdr (assoc type-keyword *attribute-types*))))
    (unless xsd-type-string
      (terror "illegal attribute type: ~S for ~S of ~S ~
               ~&Legal attribute types are: ~&  ~S"
              type-keyword prop-id concept-id (mapcar #'car *attribute-types*)))
    ;; when legal return type-string
    xsd-type-string))

#|
? (catch :error (get-xsd-attribute-type :g-year "has-year" "Z-Decree"))
"gYear"
? (catch :error (get-xsd-attribute-type :g-years "has-year" "Z-Decree"))
"illegal attribute type: :G-YEARS for \"has-year\" of \"Z-Decree\" 
Legal attribute types are: 
  (:STRING :BOOLEAN :DECIMAL :FLOAT :DOUBLE :DATE-TIME :TIME :DATE
   :G-YEAR-MONTH :G-YEAR :G-MONTH-DAY :G-DAY :G-MONTH :HEX-BINARY
   :BASE-64-BINARY :ANY-URI :NORMALIZED-STRING :TOKEN :LANGUAGE :NM-TOKEN :NAME
   :NC-NAME :INTEGER :NON-POSITIVE-INTEGER :NEGATIVE-INTEGER :LONG :INT :SHORT
   :BYTE :NON-NEGATIVE-INTEGER :UNSIGNED-LONG :UNSIGNED-INT :UNSIGNED-SHORT
   :UNSIGNED-BYTE :POSITIVE-INTEGER)"
|#
;;;--------------------------------------------------------------------- indent

(defun indent (col text)
  "add col spaces in front of the text. In case of error returns text.
Arguments:
   col: integer
   text: text to indent (string)
Return:
   the string with col space in front of it."
  (unless (and (numberp col)(stringp text))
    (return-from indent text))
  (concatenate 'string (make-string col :initial-element '#\space) text))

;;;------------------------------------------------------------ is-instance-of?

(defun is-instance-of? (object class-list)
  "test whether the object is an instance of one of the classes or superclasses ~
      of the class list.
Arguments:
   object: any individual
   class-list: a list of class OWL Ids
Return:
   class of which the individual is a member or nil"
  (let ((class-id  (index-get object :inst :class-id))
        super-classes)
    ;; compute all superclasses of the classes of the object 
    (setq super-classes (compute-transitive-closure class-id :is-a))
    ;; see whether we have some classes in common with class-list
    (intersection super-classes class-list :test #'equal)))

#|
? (index)
                    
("Ville" ((:REL (:ID "hasTown")) (:CLASS (:ID "Z-City")))) 
("Z-City" ((:CLASS (:IS-A "ZTerritory") (:NAME :EN "City; town" :FR "ville; cité")))) 
("Name" ((:ATT (:ID "hasName")))) 
("Conurbation" ((:CLASS (:ID "Z-Conurbation" "Z-Conurbation")))) 
("Cité" ((:CLASS (:ID "Z-City")))) 
("Z-Territory" ((:CLASS (:NAME :EN "territory")))) 
("Z-Conurbation" ((:CLASS (:NAME :EN "Conurbation" :FR "conurbation")))) 
("City" ((:CLASS (:ID "Z-City")))) 
("Territory" ((:CLASS (:ID "Z-Territory")))) 
("Town" ((:REL (:ID "hasTown")) (:CLASS (:ID "Z-City")))) 
("Bordeaux" ((:INST (:CLASS-ID "Z-City")))) 
NIL
? (is-instance-of? "Bordeaux" '("Z-Territory"))
("Z-Territory")
? (is-instance-of? "Bordeaux" '("Z-City"))
("Z-City")
|#
;;;------------------------------------------------------------- make-attribute

(defun make-attribute (id att-name domain-id-list range-type one-of doc)
  "produces a list of strings corresponding to the OWL format for defining a ~
      datatype property locally. Options are translated into the required equivalents.
   Strips the local options and save the result onto the *attribute-list*.
Arguments:
   id: the OWL attribute ID
   att-name: multilingual name
   domain-id-list: list of possible domains
   range-type: type of value (a SOL keyword)
   one-of: enumeration of range values
   doc: documentation multilingual string
Return:
   a list of strings to be printed."
  (let (attribute-type result) 
    ;; get xsd-type throws to error if illegal
    (setq attribute-type (get-xsd-attribute-type range-type id domain-id-list))
    ;; build index entries
    (setq result (make-indices att-name id :att))
    ;; now build attribute
    (cformat "<owl:DatatypeProperty rdf:ID=~S>" id)
    ;; use the string for label
    (rtab)
    (+result (make-labels att-name))
    ;; add eventual documentation
    (if doc (+result (make-doc doc)))
    ;; process the different domains
    ;; if more than 1 use a collection option
    (setq result (append (make-domains domain-id-list) result))
    (if (cdr one-of)
      (progn
        ;; record list of values into *index*
        (%index-set id :att :one-of one-of)
        ;; build the OWL specs
        (+result (process-attributes-one-of (cdr one-of) range-type)))
      ;; set the range (a literal by default a string)
      (cformat "<rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#~A\"/>"
               attribute-type))
    (ltab)
    ;; close the property definition
    (cformat "</owl:DatatypeProperty>")
    (push " " result)
    ;; return result
    result))

#|
? (catch :error 
    (make-attribute "hasCode" '(:en "code")'("FrenchDepartment") :zorglub nil))
                    "illegal attribute type: :ZORGLUB for \"hasCode\" of (\"FrenchDepartment\") 
Legal attribute types are: 
  (:string :BOOLEAN :DECIMAL :FLOAT :DOUBLE :DATE-TIME :TIME :DATE
   :G-YEAR-MONTH :G-YEAR :G-MONTH-DAY :G-DAY :G-MONTH :HEX-BINARY
   :BASE-64-BINARY :ANY-URI :NORMALIZED-STRING :TOKEN :LANGUAGE :NM-TOKEN :NAME
   :NC-NAME :INTEGER :NON-POSITIVE-INTEGER :NEGATIVE-INTEGER :LONG :INT :SHORT
   :BYTE :NON-NEGATIVE-INTEGER :UNSIGNED-LONG :UNSIGNED-INT :UNSIGNED-SHORT
   :UNSIGNED-BYTE :POSITIVE-INTEGER)"
? (catch :error 
(make-attribute "hasCode" '(:en "code")'("FrenchDepartment") :integer nil))
(" " "  </owl:DatatypeProperty>"
 "    <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#integer\"/>"
 "    <rdfs:domain rdf:resource=\"#FrenchDepartment\"/>"
 "    <rdfs:label xml:lang=\"en\">code</rdfs:label>"
 "  <owl:DatatypeProperty rdf:ID=\"hasCode\">")
? (rp *)
<owl:DatatypeProperty rdf:ID="hasCode">
<rdfs:label xml:lang="en">code</rdfs:label>
<rdfs:domain rdf:resource="#FrenchDepartment"/>
<rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#integer"/>
</owl:DatatypeProperty>
                    
:END
|#
;;;---------------------------------------------------------- make-attribute-id

(defun make-attribute-id (name-list)
  "get a multilingual list and build an anttribute id, e.g. \"$hasSex\".
   To do so, uses the first name of the :en option, or if there is no :en option ~
   use the first available name, building a hybrid name!
Arguments:
   name-list: a multi-lingual name list
Return:
   a string that is the attribute ID."
  (unless (mln::mln? name-list)
    (terror "Bad syntax of attribute name ~S." name-list))
  (format nil "~A~:(~A~)"  ; capitalize second word
   "has"
   (car (mln::extract name-list :language *current-language* :always t))))

#|
? (MAKE-ATTRIBUTE-ID '(:IT "sexo" :EN " sex ; genre" :FR "Sexe; Genre"))
"hasSex"
? (catch :error
    (MAKE-ATTRIBUTE-ID '(:IT "sexo" :EZ " sex ; genre" :FR "Sexe; Genre")))
"bad name-list for making attribute-id: (:IT
                                        \"sexo\"
                                        :EZ
                                        \" sex ; genre\"
                                        :FR
                                        \"Sexe; Genre\"
                                        :PL
                                        \"?\")"
? (catch :error
    (MAKE-ATTRIBUTE-ID '(:IT "sexo" :FR "Sexe; Genre")))
"hasSexo"
|#
;;;--------------------------------------------------------------- make-chapter

(defun make-chapter (&rest ll)
  "used to ignore the corresponding macro in the input text"
  (declare (ignore ll))
  nil)

;;;----------------------------------------------------------------- make-class

(defun make-class (class-list)
  "builds the list of classes for the concept.
Arguments:
   class-list: if string, then only one super-class otherwise list
Return:
   list of OWL specs
Error:
   throws to :error tag."
  ;; check syntax
  (cond
   ((listp class-list))
   ((stringp class-list) (setq class-list (list class-list)))
   (t (terror "bad format for :is-a option: ~S" class-list)))
  (let (index id result)
    ;; process all classes
    (dolist (item class-list)
      ;; build index from entry
      (setq index (make-index-string item))
      ;; check for existence using the *index* table
      (setq id (car (index-get index :class :id)))
      ;; if nil error
      (unless id
        (terror "unknown class: ~S in ~S" item class-list))
      ;; otherwise
      (cformat  "<rdfs:domain rdf:resource=\"#~A\"/>" id))
    ;; return spec list
    result))

#|
? (index)
(...
("Person" ((:IDX (:EN "Z-Person")) (:CLASS (:ID "Z-Person"))))
...
("Z-Person" ((:CLASS (:NAME :EN "Person" :FR "personne" :IT "Persona"))))
...)

? (make-class "Person")
("<rdfs:domain rdf:resource=\"#Z-Person\"/>")
|#
;;;--------------------------------------------------------------- make-concept

(defun make-concept (concept-name &rest option-list 
                                    &aux message result index-specs)
  "produces a list of strings corresponding to the OWL format for defining the ~
      concept.
   This function is called twice: once during the first pass, another time ~
      during the second pass.
Arguments:
   concept-name: a multi-lingual :name list
   option-list: a possible sequence of SOL options
Return:
   a list of strings corresponding to the OWL format."
  (vformat "~%===== ~S" (make-concept-id concept-name))
  (cond 
   ((eql *compiler-pass* 1)
    ;; first compiler pass, we collect classes and build index entries
    (let (id)
      (setq 
       message
       (catch :error
         ;; rough check of the name input format, returning a normalized format
         ;; e.g. ({:name} :en "person" :fr "personne") 
         ;; returns  ((:en "Person")(:fr "Personne"))
         (unless (setq concept-name (multilingual-name? concept-name))
           (terror "Bad concept name format: ~S" concept-name))

         ;; we use the :en label whenever possible, otherwise the first of the list
         ;;     here the id should be "Z-Person"
         ;; if an error, throws to :error
         (setq id (make-concept-id concept-name))
         ;; check for existing id
         (if (index-get id)
           (terror "Internal ID ~S already exists. Can't use it for concept." id))
         ;; save it into the concept list
         (push id *concept-list*)
         ;; save into index  "Z-Class" ((:CLASS (:NAME <concept-name>)))
         (%index-set id :class :name concept-name)
         ;; return the list of index entries
         (setq result (make-indices concept-name id :class))
         ;; debug print
         ;(mapcar #'(lambda (xx) (terpri) (princ xx))  result)
         ;(print `(+++ att list ,*attribute-list*))
         ))
      ;; is message is a string then we have an error otherwise todo bem
      (if (stringp message)
        (twarn "~&;***** Error in:~S; ~A" concept-name message)
        ;; otherwise print result
        (unless *errors-only* (rp result))))
    )
   ;; second compiler pass
   ((eql *compiler-pass* 2)
    (let (id)
      (setq 
       message
       (catch :error
         ;; build id again, no check, done during the first pass
         (if (eql :name (car concept-name)) (pop concept-name))
         (setq id (make-concept-id concept-name))
         ;; verify that id is part of the *concept-list* otherwise error
         (unless (member id *concept-list* :test #'equal)
           (terror "concept id ~S disappeared from the concept list." id))
         ;; build the id line
         (cformat "<owl:Class rdf:ID=~S>" id)
         (rtab)
         ;; now process labels
         (+result (make-labels concept-name))
         
         ;; process now options
         (dolist (option option-list)
           (cond
            ;; first take care of subclass
            ((eql (car option) :is-a)
             (+result (make-subclass (cdr option) id)) ; JPB 051029
             )
            ;; then of documentation
            ((eql (car option) :doc)
             (+result (make-doc (cdr option)))
             )
            ;; then one-of
            ((eql (car option) :one-of)
             (multiple-value-bind (class-result index-result)
                                  (make-concept-one-of (cdr option) id)
               (+result class-result)
               (setq index-specs index-result)))
            ;; then attributes
            ((eql (car option) :att)
             ;; add info
             (push (cons id (cdr option)) *attribute-list*))
            ;; then relations
            ((eql (car option) :rel)
             (push (cons id (cdr option)) *relation-list*)
             ;; include here inverse relations (not enough)
             ;(push (make-inverse-relation id option) *relation-list*)
             )
            ;; otherwise error
            (t (terror "unknown option ~S while defining ~S" option concept-name))
            ))
         (decf *left-margin* *indent-amount*)
         ;; closes the class definition
         (cformat "</owl:Class>")
         (push " " result)
         (+result index-specs)
         ;; debug print
         ;(mapcar #'(lambda (xx) (terpri) (princ xx)) (reverse result))
         ;; return result
         result))
      ;; is message is a string then we have an error otherwise todo bem
      (if (stringp message)
        (twarn "~&;***** Error in:~S; ~A" concept-name message)
        ;; otherwise print result
        (unless *errors-only* (rp result)))))))

#|
? (index-clear)
T
? (let ((*compiler-pass* 1))
    (make-concept '(:name :en "time unit")))

<$Index rdf:ID="TimeUnit">
<isEnIndexOf rdf:resource="ZTimeUnit"/>
</$Index>

:END
? (let ((*compiler-pass* 1)
        (*error-message-list* nil))
    (make-concept '(:name :en "time unit")))
NIL

? (let ((*compiler-pass* 2))
    (make-concept '(:name :en "time unit")))

<owl:Class rdf:ID="ZTimeUnit">
<rdfs:label xml:lan="en">time unit</rdfs:label>
</owl:Class>

:END

? (index-clear)
T
? (progn 
    (print (setq *compiler-pass* 1))
    (defconcept (:en "gender type")
      (:one-of (:fr "masculin" :en "male")
               (:en "female" :fr "féminin")))
    (print (setq *compiler-pass* 2))
    (defconcept (:en "gender type")
      (:one-of (:fr "masculin" :en "male")
               (:en "female" :fr "féminin"))))

1 
2 
<owl:Class rdf:ID="Z-GenderType">
<rdfs:label xml:lang="en">gender type</rdfs:label>
<owl:equivalentClass>
<owl:Class>
<owl:oneOf rdf:parseType="Collection">
<Z-GenderType rdf:ID="z-male">
<rdfs:label xml:lang="fr">masculin</rdfs:label>
<rdfs:label xml:lang="en">male</rdfs:label>
</Z-GenderType>
<Z-GenderType rdf:ID="z-female">
<rdfs:label xml:lang="en">female</rdfs:label>
<rdfs:label xml:lang="fr">féminin</rdfs:label>
</Z-GenderType>
</owl:oneOf>
</owl:Class>
</owl:equivalentClass>
</owl:Class>

:END
? (index)

("Female" ((:IDX (:EN "z-female")) (:INST (:ID "z-female")))) 
("Féminin" ((:IDX (:FR "z-female")) (:INST (:ID "z-female")))) 
("GenderType"
 ((:IDX (:EN "Z-GenderType")) (:CLASS (:ID "Z-GenderType")))) 
("Male" ((:IDX (:EN "z-male")) (:INST (:ID "z-male")))) 
("Masculin" ((:IDX (:FR "z-male")) (:INST (:ID "z-male")))) 
("Z-GenderType" ((:CLASS (:NAME :EN "gender type")))) 
("z-female" ((:INST (:CLASS-ID "Z-GenderType")))) 
("z-male" ((:INST (:CLASS-ID "Z-GenderType")))) 
:END
|#
;;;*----------------------------------------------------------- make-concept-id

(defun make-concept-id (name-option)
  "Parses the mln input.
Tries to obtain the English tag, if not take the first tag of the list
Takes all names and capitalizes the first letter of each word.
Arguments:
   name-option: an mln
Returns:
   a string
Error:
   throws to :error if bad syntax" 
  (unless (mln::mln? name-option)
    (terror "Bad syntax of name-option ~S." name-option))
  (make-concept-string 
   (car (mln::extract name-option :language *current-language* :always t))))


#|
? (make-concept-id '(:name :en "person" :fr "personne"))
"Z-Person"

? (make-concept-id '(:name :en "person; man" :fr "personne: gus"))
"Z-Person"

? (make-concept-id '(:name :fr "personne"))
"Z-Personne"

? (make-concept-id '(:fr "personne"))
"Z-Personne"

? (make-concept-id '(:en "person"))
"Z-Person"

? (catch :error (make-concept-id '(:en :fr "person")))
"bad MLS format in (:EN :FR \"person\")"

? (make-concept-id '(:en "person   of DUTY "))
"Z-PersonOfDuty"
|#
;;;-------------------------------------------------------- make-concept-one-of

(defun make-concept-one-of (individual-id-names class-id &optional rel-id)
  "build a restriction on the class telling that all elements are to be ~
      individuals of that class. Called by make-concept and make-relation.
Arguments:
   individual-id-names: a list of alternative multilingual names, e.g.
           ((:it \"uomo\" :en \"male for once  ; gentleman\" :fr \"mec\")
            (:en \"female\" :fr \"féminin\"))
         or a list of instance references  (\"france\" \"italy\")
   class-id: OWL ID of the newly created class, e.g. \"Z-GenderType\"
   rel-id (opt): OWL ID of the relation, e.g. \"hasGender\"
Return:
   2 values a list of OWL specs for the class and a list of index specs"
  (let (result individual-list ind-id index-result)
    ;; first check for validity of input argument
    (cond
     ;; validity of strings was checked in process-relations
     ((every #'stringp individual-id-names))
     ((some #'null (mapcar #'multilingual-string? individual-id-names))
      (terror "Bad format in :one-of option: ~&~S" 
              individual-id-names))
     )
    ;; if OK builds the inside of the enumerated class
    (cformat "<owl:equivalentClass>")
    (rtab)
    (cformat "<owl:Class>")
    (rtab)
    (cformat "<owl:oneOf rdf:parseType=\"Collection\">")
    (rtab)
    ;; for each option build the proper label lines
    (dolist (option individual-id-names)
      ;; if the option is a string, then it denoted a reference onto an existing
      ;; object. This was checked in the process-relations function
      (cond
       ((stringp option)
        ;; get id of instance from index
        (setq ind-id (car (index-get (make-index-string option) :inst :id)))
        (unless ind-id 
          (terror "bad instance reference ~S in one-of option ~S"
                  option individual-id-names))
        ;; get class of instance
        (setq class-id (or (car (index-get ind-id :inst :class-id)) "owl:Thing"))
        ;; produce OWL line
        (cformat "<~A rdf:ID=~S />" class-id ind-id)
        ;; also populate :one-of option in relation representation ???
        ;; e.g. (index-add "hasGender" :rel :one-of "z-female")
        (when rel-id (index-add rel-id :rel :one-of ind-id))
        )
       
       (t
        ;; an option is something like (:en "male" :fr "male; masculin")
        ;... do something
        (setq ind-id (make-concept-id option))
        (cformat "<~A rdf:ID=~S>" class-id ind-id)
        
        ;; add new individual to index 
        ;; e.g. (index-add "z-female" :inst :class-id "Z-GenderType")
        (index-add ind-id :inst :class-id class-id)
        ;; also populate :one-of option in relation representation
        ;; e.g. (index-add "hasGender" :rel :one-of "z-female")
        (when rel-id (index-add rel-id :rel :one-of ind-id))
        
        (rtab)
        (+result (make-labels option))
        ;; record for building index later
        (push (list ind-id option) individual-list)
        (ltab)
        (cformat "</~A>" class-id) ; JPB 051011
        )))
    (ltab)
    (cformat "</owl:oneOf>")
    (ltab)
    (cformat "</owl:Class>")
    (ltab)
    (cformat "</owl:equivalentClass>")
    ;; build all individual entry points from mln data
    (dolist (option individual-list)
      (setq index-result 
            (append (make-indices (cadr option) (car option) :inst) index-result)))
    ;; should first decide about the individual id 
    (values result index-result)))

#|
? (catch :error (make-concept-one-of '((:it "uomo" :en "male for once  ; gentleman" 
                                            :fr "mec")(:en "female" :fr "féminin")) "Z-Person"))
("  </owl:equivalentClass>" "    </owl:Class>" "      </owl:oneOf>"
 "        </Z-Person>"
 "          <rdfs:label xml:lang=\"fr\">féminin</rdfs:label>"
 "          <rdfs:label xml:lang=\"en\">female</rdfs:label>"
 "        <Z-Person rdf:ID=\"z-female\">" "        </Z-Person>"
 "          <rdfs:label xml:lang=\"fr\">mec</rdfs:label>"
 "          <rdfs:label xml:lang=\"en\">male for once  ; gentleman</rdfs:label>"
 ...)
NIL
? (rp *)

<owl:equivalentClass>
<owl:Class>
<owl:oneOf rdf:parseType="Collection">
<Z-Person rdf:ID="z-maleForOnce">
<rdfs:label xml:lang="it">uomo</rdfs:label>
<rdfs:label xml:lang="en">male for once  ; gentleman</rdfs:label>
<rdfs:label xml:lang="fr">mec</rdfs:label>
</Z-Person>
<Z-Person rdf:ID="z-female">
<rdfs:label xml:lang="en">female</rdfs:label>
<rdfs:label xml:lang="fr">féminin</rdfs:label>
</Z-Person>
</owl:oneOf>
</owl:Class>
</owl:equivalentClass>
:END
|#
;;;?-------------------------------------- make-concept-one-of-from-individuals

(defun make-concept-one-of-from-individuals
       (individual-id-names class-id &optional rel-id)
  "build a restriction on the class telling that all elements are to be ~
      the specified individuals. Individuals are declared to be of type ~
      owl:Thing.
Arguments:
   individual-id-names: a list of references, e.g.
           (\"France\" \"Italy\")
   class-id: OWL ID of the newly created class, e.g. \"Z-ResidenceType\"
   rel-id (opt): OWL ID of the relation, e.g. \"hasResidence\"
Return:
   2 values a list of OWL specs for the class and a list of index specs"
  (let (result individual-list ind-id index-result)
    ;; first check for validity of input argument
    (if (not (every #'stringp individual-id-names))
      (terror "Bad format in :one-of option: ~&~S" individual-id-names))

    ;; otherwise builds the class
    (cformat "<owl:equivalentClass>")
    (rtab)
    (cformat "<owl:Class>")
    (rtab)
    (cformat "<owl:oneOf rdf:parseType=\"Collection\">")
    (rtab)
    ;; for each option build the proper label lines
    (dolist (ref individual-id-names)
      ;; an option is a reference to an individual, e.g. "z-234"
      (setq ind-id (make-individual-id ref))
      (cformat "<~A rdf:ID=~S>" class-id ind-id)
      
      ;; add new individual to index 
      ;; e.g. (index-add "z-france" :inst :class-id "Z-ResidenceType")
      (index-add ind-id :inst :class-id class-id)
      ;; also populate :one-of option in relation representation
      ;; e.g. (index-add "hasResidence" :rel :one-of "z-france")
      (when rel-id (index-add rel-id :rel :one-of ind-id))
      
      (rtab)
      ;(+result (make-labels option))
      (+result (make-labels ref)) ; JPB060809 (?)
      ;; record for building index later (("z-france" "france")...)
      ;(push (list ind-id option) individual-list) 
      (push (list ind-id ref) individual-list) ; JPB060809
      (ltab)
      (cformat "</~A>" class-id) ; JPB 051011
      )
    (ltab)
    (cformat "</owl:oneOf>")
    (ltab)
    (cformat "</owl:Class>")
    (ltab)
    (cformat "</owl:equivalentClass>")
    ;; build all individual entry points
    (dolist (option individual-list)
      (setq index-result 
            (append (make-indices (cadr option) (car option) :inst) index-result)))
    ;; should first decide about the individual id 
    (values result index-result)))

#|
? (catch :error (make-concept-one-of-from-individuals
                 '("france" "italy") "Z-ResidenceType" ))
|#
;;;*------------------------------------------------------- make-concept-string

(defun make-concept-string (name-string)
  "makes a concept string from a user provided string.
Argument:
   name-string: string 
Return:
   concept-id string."
  (apply #'concatenate 'string "Z-" 
         (make-text-list (make-value-string name-string))))

#|
? (MAKE-CONCEPT-STRING "Time   Unit  ")
"Z-TimeUnit"
|#
;;;--------------------------------------------------------- make-standard-name

(defun make-standard-name (name-string)
  "makes a concept name from a user provided string.
Argument:
   name-string: string 
Return:
   concept-id string."
  (apply #'concatenate 'string  
         (make-text-list (make-value-string name-string))))

#|
? (MAKE-standard-name "Time Unit")
"TimeUnit"
|#
;;;------------------------------------------------------------------- make-doc

(defun make-doc (doc-spec &aux result doc-list)
  "we build doc options from the list of doc strings.
Arguments:
   concept-name: a list like (:en \"...\" :fr \"...\")
Return:
   a list of OWL comment specifications."
  (unless (listp doc-spec)
    (terror "bad doc-list list: ~S" doc-spec))
  ;;
  (setq doc-list doc-spec)
  (loop
    ;; when empty return
    (unless doc-list 
      (return-from make-doc result))
    ;; the first tag is the language tag. Should be known
    (unless (member (car doc-list) *language-tags*)
      (terror "bad language tag ~S in ~S" (car doc-list) doc-spec))
    ;; build OWL spec. The ? directive removes the ~ from the strings
    (cformat "<rdfs:comment xml:lang=~S>~?</rdfs:comment>" 
             (string-downcase (symbol-name (car doc-list)))
             (string-trim '(#\space) (cadr doc-list)) ())
    ;; adjust list
    (setq doc-list (cddr doc-list)))
  )

#|
? (make-doc '(:en "English doc" :fr "French doc" :it "Italian doc"))
("<rdfs:comment xml:lang=\"it\">Italian doc</rdfs:comment>"
 "<rdfs:comment xml:lang=\"fr\">French doc</rdfs:comment>"
 "<rdfs:comment xml:lang=\"en\">English doc</rdfs:comment>")
|#
;;;--------------------------------------------------------------- make-domains

(defun make-domains (domain-id-list &aux result)
  "build a list of OWL specs for domais.
Arguments;
   domain-id-list: a list of OWL domain IDs
Return:
   a list of OWL strings."
  (cond
   ;; when more than one domain we must use the union option
   ((cdr domain-id-list)
    (cformat "<rdfs:domain>")
    (rtab)
    (cformat "<owl:Class>")
    (rtab)
    (cformat "<owl:unionOf rdf:parseType=\"Collection\">")
    (rtab)
    (dolist (id domain-id-list)
      (cformat "<owl:Class rdf:about=\"#~A\"/>" id))
    (ltab)
    (cformat "</owl:unionOf>")
    (ltab)
    (cformat "</owl:Class>")
    (ltab)
    (cformat "</rdfs:domain>")
    result)
   ;; otherwise a single domain
   (t (cformat "<rdfs:domain rdf:resource=\"#~A\"/>" (car domain-id-list)))))

#|
SOL(48): (make-domains '("Z-Person" "Z-Child" "Z-Wife"))
("  </rdfs:domain>" "    </owl:Class>" "      </owl:unionOf>"
 "        <owl:Class rdf:about=\"#Z-Wife\"/>"
 "        <owl:Class rdf:about=\"#Z-Child\"/>"
 "        <owl:Class rdf:about=\"#Z-Person\"/>"
 "      <owl:unionOf rdf:parseType=\"Collection\">" "    <owl:Class>"
 "  <rdfs:domain>")
SOL(49): (rp *)
<rdfs:domain>
<owl:Class>
<owl:unionOf rdf:parseType="Collection">
<owl:Class rdf:about="#Z-Person"/>
<owl:Class rdf:about="#Z-Child"/>
<owl:Class rdf:about="#Z-Wife"/>
</owl:unionOf>
</owl:Class>
</rdfs:domain>
:END
? (make-domains '("Z-Person" ))
("<rdfs:domain rdf:resource=\"#Z-Person\"/>")
? (rp *)

<rdfs:domain rdf:resource="#Z-Person"/>
:END
|#
;;;---------------------------------------------------------------- make-header

(defun make-header ()
  "fake function to printout a header (copied from Protégé output)"
  (format *output* 
          "<?xml version=\"1.0\" ~A?>
<!DOCTYPE owl [
     <!ENTITY owl  \"http://www.w3.org/2002/07/owl#\" >
     <!ENTITY xsd  \"http://www.w3.org/2001/XMLSchema#\" >
     <!ENTITY terregov \"http://www.terregov.eupn.net/ontology/2005/10/terregov.owl/terregov#\">
   ]>

<rdf:RDF
  xmlns:owl = \"http://www.w3.org/2002/07/owl#\"
  xmlns:rdf = \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
  xmlns:rdfs= \"http://www.w3.org/2000/01/rdf-schema#\"
  xmlns:xsd = \"http://www.w3.org/2001/XMLSchema#\"
  xmlns=\"http://www.owl-ontologies.com/TerreGov.owl#\"
  xml:base=\"http://www.owl-ontologies.com/TerreGov.owl\">"
          #+microsoft
          (if (eql cg-user::*encoding* :utf8) "encoding=\"UTF-8\"" 
              "encoding=\"LATIN-1\"")
          #-microsoft
          " ")
  (format *output* "
  <owl:FunctionalProperty rdf:ID=\"indexing\">
    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#AnnotationProperty\"/>
    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#DatatypeProperty\"/>  
    <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#boolean\"/>
    <rdfs:domain rdf:resource=\"http://www.w3.org/2002/07/owl#Restriction\"/>
    <rdfs:comment xml:lang=\"en\">Default: rdfs:label allows the indexing of any ontology element (Concept, Property, Individual).
Indexing is a boolean annotation property having for domain Datatype Properties. Such a property having
true as the value of indexing, allows each instance of its domain to be indexed by the value of the property.</rdfs:comment>
  </owl:FunctionalProperty>

")
  (rtab))

;;;---------------------------------------------------------- make-index-string

(defun make-index-string (name-string)
  "makes a concept string from a user provided string, e.g. \"time unit\".
Argument:
   name-string: string 
Return:
   concept-id string."
  (apply #'concatenate 'string  
         (make-text-list (make-value-string name-string))))

#|
? (make-index-STRING "Time Unit")
"TimeUnit"
|#
;;;*-------------------------------------------------------------- make-indices

(defun make-indices (name-option value-string data-type)
  "takes a name option and builds all possible indices from it.
   E.g. ((:en \"man of war\" \"ship\") (:fr \"navire\")
   will produce the following entries: ManOfWar, Ship, Navire
   Uses the *language-tags* global variable.
Arguments:
   name-option: a multi-lingual list
   value-string: string respresenting the concept id or property id, e.g. \"Z-Country\"
   data-type: :class :att :rel or :inst
Return:
   a list of OWL index objects
Error:
   throws to an :error tag."
  (let (result index-list language-list index-string)
    ;; check name-option format removing the possible :name tag
    (unless (mln::mln? name-option)
      (terror "Bad mln format in ~S" name-option))

    ;; get the used languages
    (setq language-list (mln::get-languages name-option))
    ;; then loop on the various languages
    (dolist (tag language-list)
      ;; extract the list of names
      (setq index-list (mln::extract name-option :language tag))
      ;; for each name build an index object
      (dolist (index index-list)
        ;; cook up index string
        (setq index-string (make-index-string index))
        ;; insert entry into *index* hash table
        (index-add index-string data-type :id value-string)
        (index-add index-string :idx tag value-string)
        (pushnew index-string *index-list* :test #'string-equal)
        )
      )
    ;; return (normally nil)
    result
    ))

#|
? (index-clear)
T
? (make-indices '(:en "first name; given name" :fr "prénom") "hasFirstName" :att)
NIL
? (index)
("FirstName"
 ((:IDX (:EN "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
("GivenName"
 ((:IDX (:EN "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
("Prénom" ((:IDX (:FR "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
:END

? (make-indices '((:EN "Adult") (:FR "Adulte")) "Z-Adult" :vclass)
NIL
? (index)
("Adult" ((:IDX (:EN "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Adulte" ((:IDX (:FR "Z-Adult")) (:VCLASS (:ID "Z-Adult"))))
|#
;;;*----------------------------------------------------------- make-individual
;;; The input being a reconstructed MOSS ontology, each individual is uniquely
;;; identified by its :var option

(defun make-individual (concept-entry &rest option-list)
  "produces a list of strings corresponding to the OWL format for defining the ~
   individual.
   This function is called twice: once during the second pass, another time during ~
   the third pass.
Arguments:
   concept-entry: a string that should be the name of a concept, e.g. \"Person\"
   option-list: a sequence of MOSS options including :name and :var
Return:
   a list of strings corresponding to the OWL format."
  ;; only works during the second compiler pass (needs concept names)
  (when 
    (eql *compiler-pass* 2)
    ;; first compiler pass, we collect classes and build index entries
    (let ((mln-print-name (cdr (assoc :name option-list)))
          ;; make individual id from the:var option, e.g. "z-234"
          (id (make-individual-id (cadr (assoc :var option-list))))
          class-id message result)
      ;; for tracing, we use the printing name rather than the id
      (vformat "~%===== ~S (~S)" id (format nil "~S" mln-print-name))
      (setq 
       message
       (catch :error         
         ;; check first existence of corresponding concept/class
         (unless 
             ;; use e.g. "StreetCar" to get the class id from index
             (setq class-id 
                   (car (index-get (make-index-string concept-entry) :class :id)))
           (terror "Non existing-class ~S" concept-entry))

         ;; check the format of the printing information
         (unless (mln::mln? mln-print-name)
           (terror "Format error in individual name: ~S." mln-print-name))

         (setq option-list (remove :name option-list :key #'car))

         (if (assoc id *individual-list* :test #'string-equal)
           (terror "Another individual with same id ~S already exists." id))

         (setq option-list (remove :var option-list :key #'car))

         ;; otherwise make a new entry in the individual list
         ;; e.g. ("bordeaux" "Z-City" (:fr "bordeaux") (<option>*))        
         (push (list* id class-id mln-print-name option-list)
               *individual-list*)
         ;; save entries 
         (+result (make-indices mln-print-name id :inst))
         (index-add id :inst :class-id class-id)
         result))
      ;; is message is a string then we have an error otherwise todo bem
      ;(print message)
      (when (stringp message)
        (twarn "~&;***** While creating individual ~S: ~&~A" 
               mln-print-name message)
        (return-from make-individual nil)
        )
      (unless *errors-only* (rp result))
      result)))

#|
? (let ((*compiler-pass* 2))
    (make-individual "city" '(:name :fr "Florence" :it "Firenze") '(:var _I-22)))
NIL
? (let ((*compiler-pass* 2))
    (make-individual "city" '(:fr "Florence" :it "Firenze")))
NIL
? (errors)

;***** While creating individual (:FR \"Florence\" :IT \"Firenze\"): 
Another individual with same key "z-florence" already exists.
;***** While creating individual (:NAME :FR "pyrénées atlantiques"): 
non existing-class "département"
:END
|#
;;;--------------------------------------------------------- make-individual-id

(defun make-individual-id (key &aux tag)
  "Parses the input, e.g. _I-345 to produce \"z-345\""
  (moss::string+ "z" (subseq (symbol-name key) 2)))

#|
? (make-individual-id '_I-123)
"z-123"
|#
;;;----------------------------------------------------- make-individual-string

(defun make-individual-string (name-string)
  "makes an individual string from a user provided string.
Argument:
   name-string: string 
Return:
   concept-id string."
  ;; make-value-string capitaalizes a list of words inside a string
  ;; make-text-list splits a string into a list of word strings
  (let* ((word-list (make-text-list (make-value-string name-string)))
         (first-word (string-downcase (car word-list))))
    ;; when string represents a variable, remove "_I-"
    ;(if (eql #\_ (char first-word 0)) (setq first-word (subseq first-word 3)))
    (apply #'concatenate 'string "z-" first-word (cdr word-list))))

#|
? (make-individual-string  "male")
"z-male"
? (make-individual-string  "MALE")
"z-male"
? (make-individual-string  "MALE  de jour   ")
"z-maleDeJour"
? (make-individual-string  '_I-123)
"z-_i-123"
|#
;;;*-------------------------------------------------------- make-inverse-names

(defun make-inverse-names (name-option)
  "takes a multilingual name and builds another one, corresponding to the ~
      inverse property names, with the right suffix in each language.
Argument:
   name-option: a multilingual name qualifying the relation
Return:
   a multi-lingual name with inverted names."
  (let (language-list name-list result)
    (unless (mln::mln? name-option) 
      (terror "bad multilingual name ~S" name-option))
    (setq language-list (mln::get-languages name-option))
    (dolist (tag language-list)
      (setq name-list (mln::filter-language name-option tag))
      (push tag result)
      (push
          (subseq (apply #'concatenate 'string 
                         (mapcar
			  #'(lambda (xx) (concatenate 'string "; " xx " of")) 
			  name-list))
                  1)
          result))
    (reverse result)))

#|
? (MAKE-INVERSE-NAMES '(:NAME :EN "French department; district" :FR "département"))
(:EN " French department of; district of" :FR " département de")
|#
;;;*-------------------------------------------------- make-inverse-relation-id

(defun make-inverse-relation-id (mln-name &aux name-string)
  "get a multilingual name and build an inverse relation id, e.g. \"isBrotherOf\".
   To do so, uses the first name of the :en option, or if there is no :en option ~
      use the first available name, building a hybrid name!
Arguments:
   name-list: a multi-lingual name list
Return:
   a string that is the attribute ID."
  (unless (mln::mln? mln-name)
    (terror "bad name-list for making relation-id"))

  ;; if name list contains :en then
  (setq name-string 
        (car (mln::extract mln-name :language *current-language* :always t)))
  (concatenate 'string "is" (make-index-string name-string) "Of"))


#|
? (make-inverse-relation-id '(:fr "propriété inverse" :en "inverse property; inv prop"))
"isInversePropertyOf"
|#
;;;---------------------------------------------------------------- make-labels

(defun make-labels (concept-name &aux result language-list)
  "we build label options from the list of concept names.
Arguments:
   concept-name: a list like (:en \"person\" :fr \"personne\")
Return:
   a list of OWL label specifications."
  (unless (mln::mln? concept-name)
    (terror "bad concept-name mln: ~S" concept-name))
  ;; get the languages
  (setq language-list (mln::get-languages concept-name))
  (dolist (tag language-list)
    ;; the first tag is the language tag. Should be known
    (unless (member tag *language-tags*)
      (terror "bad language tag ~S in ~S" tag concept-name))
    ;; build OWL spec
    (cformat  "<rdfs:label xml:lang=~S>~A</rdfs:label>" 
              (string-downcase tag)
              (format nil "~{~A~^; ~}" 
                      (mln::filter-language concept-name tag)))
   )
  result)

#|
? (make-labels '(:en "person ; human" :fr "personne"))
("<rdfs:label xml:lan=\"fr\">\"personne\"</rdfs:label>"
 "<rdfs:label xml:lan=\"en\">\"person ; human\"</rdfs:label>")
|#
;;;-------------------------------------------------------------- make-ontology

(defun make-ontology (&rest options &aux result) 
  "builds the ontology header. Options are:
   :name, :version, :xmlns, :base, :imports
Arguments:
   options: list of options use Protégé header 
Return:
   noting interesting."
  (when (eql *compiler-pass* 1)  ; do that only once
    ;; first print that stuff 
    (cformat
     "<?xml version=\"1.0\" ~A?>"
     "encoding=\"UTF-8\"")
    
    ;; extract from the defontology options the name space to be used in the 
    ;; rule file
    (when sol::*rule-output*
      (set-rule-name-space options)
      ;; get user-defined operators
      (apply #'make-user-defined-operator-list options)
      (apply #'make-rule-header options))
    
    ;; now take care of the DOCTYPE section if any
    (dolist (option options)
      (case (pop option)
        (:doctype
         ;; for each option build entry
         (cformat "~%<!DOCTYPE owl [")
         (loop
           (unless option (return nil))
           (cformat "~&  <!ENTITY ~A~20T~S>" 
                    (pop option) (pop option)))
         (cformat "~&]>"))))
    ;; now introduce real body
    (cformat "~2&<rdf:RDF")
    ;;;     <!ENTITY owl  \"http://www.w3.org/2002/07/owl#\" >
    ;;;     <!ENTITY xsd  \"http://www.w3.org/2001/XMLSchema#\" >
    ;;;           <!ENTITY terregov \"http://www.terregov.eupn.net/ontology/2005/10/terregov.owl\" >
    ;;;   ]>
    
    (let (tag)
      ;; then process each option
      (dolist (option options)
        (case (setq tag (pop option))
          (:xmlns
           ;; for each name space construct the right code
           (loop
             (unless option (return nil))
             (if (string-equal (car option) "self")
               (progn
                 (pop option)
                 (cformat "~&  xmlns=~S" (pop option)))
               (cformat "~&  xmlns:~A=~S" (pop option)(pop option)))))
          (:base
           (cformat "~&  xml:base=~S" (pop option)))))
      ;; close case before addressing imports
      (cformat " >~2%")
      (dolist (option options)
        (case (setq tag (pop option))
          (:imports
           ;; build import list
           (cformat "~&  <owl:Ontology rdf:about=\"\">")
           (cformat "~%<!--")
           (loop
             (unless option (return nil))
             (cformat "~&    <owl:imports rdf:resource=~S/>" (pop option)))
           (cformat "~%-->")
           (cformat "~&  </owl:Ontology>~2%"))
          ((:xmlns :base :doctype)
           ;; already processed
           )
          ;; simply options used elsewhere
          ((:name :version :operator-location :user-operators :title :package 
                  :language)) 
          (otherwise
           (error "bad options in defontology: ~S" tag))))
      ;; introduce index classes
      ;;+++++++++++++++++++++++++++ Declaration for Indexing ++++++++++++++++++++++++
      (rtab)
      (mark-section "Declaration for Indexing" *separation-width*)
      (cformat 
       "<owl:FunctionalProperty rdf:ID=\"indexing\">
       <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#AnnotationProperty\"/>
       <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#DatatypeProperty\"/>  
       <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#boolean\"/>
       <rdfs:domain rdf:resource=\"http://www.w3.org/2002/07/owl#Restriction\"/>
       <rdfs:comment xml:lang=\"en\">Default: rdfs:label allows the indexing of any ontolog element (Concept, Property, Individual).
Indexing is a boolean annotation property having for domain Datatype Properties. Such a property having
true as the value of indexing, allows each instance of its domain to be indexed by the value of the property.</rdfs:comment>
</owl:FunctionalProperty>

")
      (mark-section "End Declaration for Indexing" *separation-width*)
      ;;+++++++++++++++++++++++ End Declaration for Indexing ++++++++++++++++++++++++
      ;; Declarations for virtual categories (we introduce them even if there are no 
      ;; rules
      ;;++++++++++++++++++++ Declaration for Virtual Categories +++++++++++++++++++++
      (mark-section "Declaration for Virtual Categories" *separation-width*)
      (ltab)
      (cformat
       "
  <rdfs:Class rdf:ID=\"VirtualClass\">
     <rdfs:comment xml:lang=\"en\">Class of all the virtual classes</rdfs:comment>
     <rdfs:label xml:lang=\"fr\">Classe Virtuelle</rdfs:label>
     <rdfs:subClassOf rdf:resource=\"http://www.w3.org/2002/07/owl#Class\"/>
     <rdfs:label xml:lang=\"en\">Virtual Class</rdfs:label>
  </rdfs:Class>

  <rdfs:Class rdf:ID=\"VirtualObjectProperty\">
     <rdfs:label xml:lang=\"fr\">Propriété Objet Virtuelle</rdfs:label>
     <rdfs:comment xml:lang=\"en\">The class of the virtual object properties</rdfs:comment>
     <rdfs:subClassOf rdf:resource=\"http://www.w3.org/2002/07/owl#ObjectProperty\"/>
     <rdfs:label xml:lang=\"en\">Virtual Object Property</rdfs:label>
  </rdfs:Class>

  <rdfs:Class rdf:ID=\"VirtualDatatypeProperty\">
     <rdfs:subClassOf rdf:resource=\"http://www.w3.org/2002/07/owl#DatatypeProperty\"/>
     <rdfs:comment xml:lang=\"en\">The class of virtual datatype properties</rdfs:comment>
     <rdfs:label xml:lang=\"fr\">Propriété datatype virtuelle</rdfs:label>
     <rdfs:label xml:lang=\"en\">Datatype Virtual Property</rdfs:label>
  </rdfs:Class>

")
      ;;++++++++++++++++++ End Declaration for Virtual Categories +++++++++++++++++++
      (rtab)
      (mark-section "End Declaration for Virtual Categories" *separation-width*)
      (mark-section "Ontology" *separation-width*)
      ))
  (rp result))

#|
? (make-ontology
  '(:version 1.0)
  '(:doctype "owl" "http://www.w3.org/2002/07/owl#"
            "test" "http://www.terregov.eupn.net/ontology/2005/10/test.owl/test#"
            "xsd"       "http://www.w3.org/2001/XMLSchema#" )
  '(:xmlns "rdf" "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
          "rdfs" "http://www.w3.org/2000/01/rdf-schema#"
          "owl" "http://www.w3.org/2002/07/owl#"
          "xsd" "&xsd;"
          "self" "&test;"
          "test" "&test;")
  '(:base "http://www.terregov.eupn.net/ontology/2005/10/test.owl/test")
  '(:imports "http://www.w3.org/2000/01/rdf-schema"
            "http://www.w3.org/1999/02/22-rdf-syntax-ns"
            "http://www.w3.org/2002/07/owl"))
|#
;;;------------------------------------------------- make-owl-virtual-attribute

(defun make-owl-virtual-attribute (id att-name domain-id-list value-type doc)
  "produces a list of strings corresponding to the OWL format for defining a ~
      datatype property locally. Options are translated into the required ~
      equivalents. When id is already the id of something else, then error.
Arguments:
   id: the OWL attribute internal name, e.g. \"hasAge\"
   att-name: multilingual name
   domain-id-list: list of possible domains
   value-type: an xsd type for the value
   doc: eventual documentation (multilingual string)
Return:
   a list of strings to be printed."
  (when (or (index-get id :att)(index-get id :rel)(index-get id :vrel)
            (index-get id :class)(index-get id :vclass))
    (terror "internal name ~S of virtual attribute already used." id))
  (let (result)
    ;; build index entries
    (setq result (make-indices att-name id :vatt))
    ;; now build virtual relation
    (cformat "<VirtualDatatypeProperty rdf:ID=~S>" id)
    ;; use the string for label
    (rtab)
    (+result (make-labels att-name))
    ;; process the different domains
    ;; if more than 1 use a collection option
    (+result (make-domains domain-id-list))
    ;; process type
    (+result (make-type value-type))
    ;; add eventual documentation
    (if doc (+result (make-doc doc)))
    ;; close the property definition
    (ltab)
    (cformat "</VirtualDatatypeProperty>")
    (push " " result)
    ;; return result
    result))

#|
? (make-owl-virtual-attribute "hasAge" '(:en "age" :fr "aage") '("Z-Person")
                              :non-negative-integer '(:en "...test doc"))
(" " "  </VirtualDatatypeProperty>"
 "  <rdfs:comment xml:lang=\"en\">...test doc</rdfs:comment>"
 "  <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\"/>"
 "  <rdfs:domain rdf:resource=\"#Z-Person\"/>"
 "  <rdfs:label xml:lang=\"fr\">aage</rdfs:label>"
 "  <rdfs:label xml:lang=\"en\">age</rdfs:label>"
 "<VirtualDatatypeProperty rdf:ID=\"hasAge\">")

? (rp *)
<VirtualDatatypeProperty rdf:ID="hasAge">
  <rdfs:label xml:lang="en">age</rdfs:label>
  <rdfs:label xml:lang="fr">aage</rdfs:label>
  <rdfs:domain rdf:resource="#Z-Person"/>
  <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#nonNegativeInteger"/>
  <rdfs:comment xml:lang="en">...test doc</rdfs:comment>
</VirtualDatatypeProperty>
|#
;;;-------------------------------------------------- make-owl-virtual-relation

(defun make-owl-virtual-relation (id rel-name domain-id-list range-id-list doc
                                          inv-id)
  "produces a list of strings corresponding to the OWL format for defining an ~
      object property locally. Options are translated into the required ~
      equivalents. When id is already the id of an attribute, then error.
Arguments:
   id: the OWL relation ID
   rel-name: multilingual name
   domain-id-list: list of possible domains
   range-id-list: list of possible ranges
   doc: eventual documentation (multilingual string)
Return:
   a list of strings to be printed."
  (when (or (index-get id :att)(index-get id :vatt)(index-get id :rel)
            (index-get id :class)(index-get id :vclass))
    (terror "internal name ~S of virtual relation already used." id))
  (let (result)
    ;; build index entries
    (setq result (make-indices rel-name id :rel))
    ;; now build virtual relation
    (cformat "<VirtualObjectProperty rdf:ID=~S>" id)
    ;; use the string for label
    (rtab)
    (+result (make-labels rel-name))
    ;; process the different domains
    ;; if more than 1 use a collection option
    (+result (make-domains domain-id-list))
    ;; range is range of last class of compose
    (+result (make-ranges range-id-list))
    ;; add eventual documentation
    (if doc (+result (make-doc doc)))
    ;; add pointer to inverse property
    (cformat "<owl:inverseOf>")
    (rtab)
    (cformat "<VirtualObjectProperty rdf:about=\"#~A\"/>" inv-id)  
    (ltab)
    (cformat "</owl:inverseOf>")
    (ltab)
    ;; close the property definition
    (cformat "</VirtualObjectProperty>")
    (push " " result)
    ;; return result
    result))

#|
? (make-owl-virtual-relation "hasUncle" '(:en "Uncle" :fr "Oncle") '("Z-Person")
            '("Z-Person") '(:en "test doc...") "isUncleOf")
(" " "</VirtualObjectProperty>" "  </owl:inverseOf>"
 "    <VirtualObjectProperty rdf:about=\"#isUncleOf\"/>" "  <owl:inverseOf>"
 "  <rdfs:comment xml:lang=\"en\">test doc...</rdfs:comment>"
 "  <rdfs:range rdf:resource=\"#Z-Person\"/>"
 "  <rdfs:domain rdf:resource=\"#Z-Person\"/>"
 "  <rdfs:label xml:lang=\"fr\">Oncle</rdfs:label>"
 "  <rdfs:label xml:lang=\"en\">Uncle</rdfs:label>"
 "<VirtualObjectProperty rdf:ID=\"hasUncle\">")

? (rp *)
<VirtualObjectProperty rdf:ID="hasUncle">
  <rdfs:label xml:lang="en">Uncle</rdfs:label>
  <rdfs:label xml:lang="fr">Oncle</rdfs:label>
  <rdfs:domain rdf:resource="#Z-Person"/>
  <rdfs:range rdf:resource="#Z-Person"/>
  <rdfs:comment xml:lang="en">test doc...</rdfs:comment>
  <owl:inverseOf>
    <VirtualObjectProperty rdf:about="#isUncleOf"/>
  </owl:inverseOf>
</VirtualObjectProperty>
|#
;;;---------------------------------------------------------------- make-ranges

(defun make-ranges (range-id-list &aux result)
  "build a list of OWL specs for ranges (similar to domains).
Arguments;
   range-id-list: a list of OWL range IDs
Return:
   a list of OWL strings."
  (cond
   ;; when more than one domain we must use the union option
   ((cdr range-id-list)
    (cformat "<rdfs:range>")
    (rtab)
    (cformat "<owl:Class>")
    (rtab)
    (cformat "<owl:unionOf rdf:parseType=\"Collection\">")
    (rtab)
    (dolist (id range-id-list)
      (cformat "<owl:Class rdf:about=\"#~A\"/>" id))
    (ltab)
    (cformat "</owl:unionOf>")
    (ltab)
    (cformat "</owl:Class>")
    (ltab)
    (cformat "</rdfs:range>")
    result)
   ;; otherwise a single range
   (t (cformat "<rdfs:range rdf:resource=\"#~A\"/>" (car range-id-list)))))

#|
SOL(56): (make-ranges '("Z-Person" "Z-Child" "Z-Wife"))
("  </rdfs:range>" "    </owl:Class>" "      </owl:unionOf>"
 "        <owl:Class rdf:about=\"#Z-Wife\"/>"
 "        <owl:Class rdf:about=\"#Z-Child\"/>"
 "        <owl:Class rdf:about=\"#Z-Person\"/>"
 "      <owl:unionOf rdf:parseType=\"Collection\">" "    <owl:Class>"
 "  <rdfs:range>")
SOL(57): (rp *)
<rdfs:range>
<owl:Class>
<owl:unionOf rdf:parseType="Collection">
<owl:Class rdf:about="#Z-Person"/>
<owl:Class rdf:about="#Z-Child"/>
<owl:Class rdf:about="#Z-Wife"/>
</owl:unionOf>
</owl:Class>
</rdfs:range>
:END
? (make-ranges '("Z-Person"))
("<rdfs:range rdf:resource=\"#Z-Person\"/>")
|#
;;;-------------------------------------------------------------- make-relation

(defun make-relation (id rel-name domain-id-list range-id-list one-of doc)
  "produces a list of strings corresponding to the OWL format for defining an ~
      object property locally. Options are translated into the required ~
      equivalents. When id is already the id of an attribute, then error.
Arguments:
   id: the OWL relation ID
   rel-name: multilingual name
   domain-id-list: list of possible domains
   range-id-list: list of possible ranges
   one-of: enumeration of range individuals
   doc: eventual documentation (multilingual string)
Return:
   a list of strings to be printed."
  (when (index-get id :att)
    (terror "same name ~S~%   for attribute of domains: ~S ~
             and relation of domains: ~S. ~%We keep the attribute."
            id (index-get id :att :domain)
            (index-get id :rel :domain)))
  (let (result one-of-result new-class-id inv)
    ;; build index entries
    (setq result (make-indices rel-name id :rel))
    ;; now build attribute
    (cformat "<owl:ObjectProperty rdf:ID=~S>" id)
    ;; use the string for label
    (rtab)
    (+result (make-labels rel-name))
    ;; process the different domains
    ;; if more than 1 use a collection option
    (+result (make-domains domain-id-list))
    ;; add eventual documentation
    (if doc (+result (make-doc doc)))
    ;(print `(=====>ONE-OF ,one-of))
    (if one-of
      ;; 2 cases:
      ;;   - the objects of the oneof clause are referenced existing individuals
      ;;     e.g. ("france " "italy" "poland")
      ;;   - the objects are new to be created ((:en "male")(:en "female"))
      ;; build a special class "$XxxType" to attach individual options
      ;; (defconcept (:en "<id> type") (:one-of ...))
      ;; use the new class id for the range 
      (progn
        (setq new-class-id (concatenate ' string (make-concept-id rel-name) "Type"))
        ;; record class in the index
        (%index-set new-class-id :class :name 
                    (list :en (concatenate 'string (get-id-string rel-name) " type")))
        (setq one-of-result (process-relations-one-of one-of new-class-id rel-name id))
        (+result (make-ranges (list new-class-id)))
        ;; record new class as the range of the relation, erasing whatever was there
        (%index-set id :rel :range (list (list new-class-id)))
        )
      ;; set the range (a literal by default a string)
      (+result (make-ranges range-id-list))
      )
    ;; add the inverse property
    ;;;    (if (setq inv (car (index-get id :rel :inv)))
    ;;;      (cformat "<owl:InverseOf rdf:resource=\"#~A\"/>" inv))
    ;;; JPB0510
    (when (setq inv (car (index-get id :rel :inv)))
      (cformat "<owl:inverseOf>")
      (rtab)
      (cformat "<owl:ObjectProperty rdf:about=\"#~A\"/>" inv)  
      (ltab)
      (cformat "</owl:inverseOf>"))
    (ltab)
    ;; close the property definition
    (cformat "</owl:ObjectProperty>")
    (push " " result)
    (+result one-of-result)
    ;; return result
    result))

#| (index)
SOL(60): (make-relation "hasBrother" '(:en "brother" :fr "frangin; frère") 
'("Z-Person" "Z-Tiger") '("Z-Mammal") nil)
(" " "  </owl:ObjectProperty>"
 "    <rdfs:range rdf:resource=\"#Z-Mammal\"/>" "    </rdfs:domain>"
 "      </owl:Class>" "        </owl:unionOf>"
 "          <owl:Class rdf:about=\"#Z-Tiger\"/>"
 "          <owl:Class rdf:about=\"#Z-Person\"/>"
 "        <owl:unionOf rdf:parseType=\"Collection\">"
 "      <owl:Class>" ...)
SOL(61): (rp *)
  <owl:ObjectProperty rdf:ID="hasBrother">
    <rdfs:label xml:lang="en">brother</rdfs:label>
    <rdfs:label xml:lang="fr">frangin; frère</rdfs:label>
    <rdfs:domain>
      <owl:Class>
        <owl:unionOf rdf:parseType="Collection">
          <owl:Class rdf:about="#Z-Person"/>
          <owl:Class rdf:about="#Z-Tiger"/>
        </owl:unionOf>
      </owl:Class>
    </rdfs:domain>
    <rdfs:range rdf:resource="#Z-Mammal"/>
  </owl:ObjectProperty>
 
:END
SOL(62): (make-relation "hasGender" '(:en "gender; sex") '("Z-Person" "Z-Tiger") nil
                 '(:one-of (:en "male") (:en "female")))
(" " "</owl:Class>" "  </owl:equivalentClass>" "    </owl:Class>"
 "      </owl:oneOf>" "        </Z-GenderType>"
 "          <rdfs:label xml:lang=\"en\">female</rdfs:label>"
 "        <Z-GenderType rdf:ID=\"z-female\">"
 "        </Z-GenderType>"
 "          <rdfs:label xml:lang=\"en\">male</rdfs:label>" ...)
SOL(63): (rp *)
  <owl:ObjectProperty rdf:ID="hasGender">
    <rdfs:label xml:lang="en">gender; sex</rdfs:label>
    <rdfs:domain>
      <owl:Class>
        <owl:unionOf rdf:parseType="Collection">
          <owl:Class rdf:about="#Z-Person"/>
          <owl:Class rdf:about="#Z-Tiger"/>
        </owl:unionOf>
      </owl:Class>
    </rdfs:domain>
    <rdfs:range rdf:resource="#Z-GenderType"/>
  </owl:ObjectProperty>
 
<owl:Class rdf:ID="Z-GenderType">
  <owl:equivalentClass>
    <owl:Class>
      <owl:oneOf rdf:parseType="Collection">
        <Z-GenderType rdf:ID="z-male">
          <rdfs:label xml:lang="en">male</rdfs:label>
        </Z-GenderType>
        <Z-GenderType rdf:ID="z-female">
          <rdfs:label xml:lang="en">female</rdfs:label>
        </Z-GenderType>
      </owl:oneOf>
    </owl:Class>
  </owl:equivalentClass>
</owl:Class>
 
:END
|#
;;;*---------------------------------------------------------- make-relation-id
;;; same function as make-attribute-id (could be different)

(defun make-relation-id (name-list)
  "get a multilingual list and build a relation id, e.g. \"$hasBrother\".
   To do so, uses the first name of the :en option, or if there is no :en option ~
      use the first available name, building a hybrid name!
Arguments:
   name-list: a multi-lingual name list
Return:
   a string that is the attribute ID."
  (unless (mln::mln? name-list)
    (terror "Bad syntax of relation name ~S." name-list))
  (format nil "has~A"  ; capitalize second word
    (make-index-string
     (car (mln::extract name-list :language *current-language* :always t)))))

#|
? (MAKE-relation-ID '(:IT "nome" :EN " first name" :FR "prénom"))
"hasFirstName"
? (MAKE-relation-ID '(:IT "fratello" :FR "frangin; frère"))
"hasFratello"
|#
;;;--------------------------------------------------------------- make-section

(defun make-section (&rest ll)
  "used to ignore the corresponding macro in the input text"
  (declare (ignore ll))
  nil)

;;;--------------------------------------------------------- make-single-string

(defun make-single-string (ll)
  (cond ((null ll) "") 
        ((null (cdr ll)) (car ll))
        (t (concatenate 'string (car ll) " ; " (make-single-string (cdr ll))))))

#|
? (make-single-string '("a" "b" "c"))
"a ; b ; c"
? (make-single-string '("a" "b" ))
"a ; b"
? (make-single-string '("a" ))
"a"
? (make-single-string '())
""
|#
;;;-------------------------------------------------------------- make-subclass

(defun make-subclass (super-class-list class-id)
  "builds the list of superclasses for the concept.
Arguments:
   super-class-list: if string, then only one super-class, otherwise list
Return:
   list of OWL specs
Error:
   throws to :error tag."
  ;; check syntax
  (cond
   ((listp super-class-list))
   ((stringp super-class-list) (setq super-class-list (list super-class-list)))
   (t (terror "bad format for :is-a option: ~S" super-class-list)))

  (let (index super-id result)
    ;; process all superclasses
    (dolist (item super-class-list)
      ;; build index from entry, e.g. "Person": entry point for class Person
      (setq index (make-index-string item))
      ;; check for existence using the *index* table (should be "Z-Person"?)
      (setq super-id (car (index-get index :class :id)))
      ;; if nil error
      (unless super-id
        (terror "unknown super-class: ~S in ~S" item super-class-list))
      ;; otherwise
      (cformat  "<rdfs:subClassOf rdf:resource=\"#~A\"/>" super-id)
      ;; add to model of class
      (index-add class-id :class :is-a super-id)
      ;; save concept into the superclass list (what for?)
      (push super-id *super-class-list*))
    ;; return spec list
    result))

#|
? (index-clear)
T
? (index-add "Z-City" :class :name '(:en "city"))
(:EN "city")
? (index-add "City" :class :id "Z-City")
"Z-City"
? (index)
("City" ((:CLASS (:ID "Z-City")))) 
("Z-City" ((:CLASS (:NAME (:EN "city"))))) 
:END

? (make-subclass '("city ") "Z-District")
("<rdfs:subClassOf rdf:resource=\"Z-City\"/>")
? (index)
("City" ( (:CLASS (:ID "Z-City"))))
("Z-City" ((:CLASS (:NAME (:EN "city"))))) 
("Z-District" ((:CLASS (:IS-A "Z-City")))) 
:END
NIL
|#
;;;*------------------------------------------------------------ make-text-list

(defun make-text-list (text)
  "Norm a text string by separating each word making it lower case with a leading~
      uppercased letter.
Arguments:
   text: text string
Return:
   a list of normed words."
  (let (pos result word)
    (unless text (return-from make-text-list nil))
    (loop
      ;; remove trailing blanks
      (setq text (string-trim '(#\space) text))
      ;; is there any space left?
      (setq pos (position #\space text))
      (unless pos
        (push (string-capitalize text) result)
        (return-from make-text-list (reverse result)))
      ;; extract first word
      (setq word (subseq text 0 pos)
            text (subseq text (1+ pos)))
      (push (string-capitalize word) result)
      )))

#|
? (make-text-list "the   DAY when I    fell INTO the PIT   ")
("The" "Day" "When" "I" "Fell" "Into" "The" "Pit")
|#
;;;--------------------------------------------------------------- make-trailer

(defun make-trailer ()
  "to close the ontology"
  (ltab)
  (format *output* "~&</rdf:RDF>"))

;;;------------------------------------------------------------------ make-type

(defun make-type (type)
  "builds the xsd-type for a datatype property (attribute). 
   Default is string.
Arguments:
   type: a keyword, e.g. :integer, :float, :date, ... or nil
Return:
   list of OWL specs
Error:
   throws to :error tag."
  (let (xsd-type result)
    (unless type
      (setq xsd-type (or xsd-type (cdr (assoc :string *attribute-types*)))))
    (when type
      
      (setq xsd-type (cdr (assoc type *attribute-types*)))
      ;; error if not in the list
      (unless xsd-type
        (terror "bad attribute type: ~S" type)))
    ;; build OWL spec
    (cformat  "<rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#~A\"/>" 
              xsd-type)
    ;; return spec list
    result))

#|
? (make-type :float)
("    <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#float\"/>")

? (make-type :non-negative-integer)
("<rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#nonNegativeInteger\"/>")

? (make-type nil)
("    <rdfs:range rdf:resource=\"http://www.w3.org/2001/XMLSchema#string\"/>")

? (catch :error (make-type :babar))
"bad attribute type: :BABAR"
|#
;;;*--------------------------------------------------------- make-value-string

(defun make-value-string (value &optional (interchar '#\space))
  "Takes an input string, removing all leading and trailing blanks, replacing ~
   all substrings of blanks and simple quote with a single underscore, and ~
   building a new string capitalizing all letters. The function is used ~
   to normalize values for comparing a value with an external one while ~
   querying the database."
  (let* ((input-string (if (stringp value) value
                           (format nil "~S" value)))
         (work-list (map 'list #'(lambda(x) x) 
                         ;; remove leading and trailing blanks
                         (string-trim '(#\space) input-string))))
    (string-capitalize   ; we normalize entry-point to lower case
     (map
      'string
      #'(lambda(x) x) 
      ;; this mapcon removes the blocks of useless spaces and uses
      ;; a translation table to replace French letters with the equivalent
      ;; unaccentuated capitals
      (mapcon 
       #'(lambda(x) 
           (cond 
            ;; if we have 2 successive blanks we remove one
            ((and (cdr x)(eq (car x) '#\space)
                  (eq (cadr x) '#\space)) 
             nil)
            ;; if we have a single blank we replace it with an intermediate char
            ((eq (car x) '#\space) (copy-list (list interchar)))
            ;; if the char is in the translation table we use the table
            ((assoc (car x) *translation-table*)
             (copy-list (cdr (assoc (car x) *translation-table*))))
            ;; otherwise we simply list the char
            (t (list (car x)))))
       work-list)))))

#|
? (make-value-string "à ce moment-là  j'avais   très faim.    ")
"À Ce Moment-Là J Avais Très Faim."
|#
;;;----------------------------------------------------- make-virtual-attribute
;;; JPB080106
;;; during compiler PASS 1 an entry is created in the INDEX table to track the
;;; existence of the virtual attribute The function simply creates the corresponding
;;; rule
;;;
;;; Syntax:
;;;   (defvirtualattribute <mln>
;;;     (:class <concept-ref>)
;;;     (:def <concept-definition>)
;;;     (:type <xsd-type>)
;;;     (:doc <documentation>))

(defun make-virtual-attribute (attribute-name &rest option-list 
                                                  &aux message)
  "copies the definition of the virtual attribute into the index table.
   It will be processed later.
Arguments:
   attribute-name: a multi-lingual :name list
   option-list: a possible sequence of SOL options
Return:
   a list of strings corresponding to the OWL format."
  (when 
    (eql *compiler-pass* 1)
    ;; first compiler pass, we build index entries
    (let (id)
      (setq 
       message
       (catch :error
         ;;=== check simple syntax errors. if an error, throw to :error
         ;; rough check of the name input format, removing :name tag if present
         ;; e.g. ({:name} :en "age" :fr "‰ge")
         (unless (mln::mln? attribute-name)
           (terror "Bad attribute name format: ~S." attribute-name))

         (unless (or (assoc :compose option-list) (assoc :def option-list))
           (terror "Missing :def or ;compose option."))

         ;; we use the :en label whenever possible, otherwise the first of the list
         ;;     here the id should be something like: "hasAge"
         (setq id (make-attribute-id attribute-name))

        ;; check for existing id
         (if (or (index-get id :att)(index-get id :vatt)(index-get id :rel)
                 (index-get id :vrel))
           (terror "Internal ID ~S already exists." id))
         ;;=== end error check
         
         ;; save it into the attribute list
         (when (assoc :def option-list)
           (pushnew id *virtual-attribute-list* :test #'string-equal))
         (when (assoc :compose option-list)
           (pushnew id *virtual-composed-attribute-list* :test #'string-equal))         
         ;; set the index table entry to be used by process-virtual-attributes
         (%index-set id :vatt :def (cons (list :name attribute-name) option-list))
         ;; add index entries, used by other entities
         (make-indices attribute-name id :vatt)
         ))
      ;; is message is a string then we have an error otherwise todo bem
      (if (stringp message)
        (twarn "~&;***** Error in virtual attribute: ~S;~&;  ~A" 
               attribute-name message))
      :done
      )))

#|
? (index-clear)
T
? (let ((*compiler-pass* 1))
    (defvirtualattribute (:name :en "age" :fr "âge")
      (:class "person")
      (:def
         (?class "birth date" ?y)
         ("getYears" ?y ?*)
         (:type :integer)
         (:min 1)
         (:doc :en "the attribute age is computed from the birthdate of a ~
               person by applying the function getYears to the birthdate."))))
:DONE


? (index)
("Age" ((:IDX (:EN "hasAge")) (:VATT (:ID "hasAge")))) 
("hasAge"
 ((:VATT
   (:DEF (:NAME (:EN "age" :FR "‰ge")) (:CLASS "person")
     (:DEF (?CLASS "birth date" ?Y) ("getYears" ?Y ?*)) (:TYPE :INTEGER) (:MIN 1)
     (:DOC :EN "the attribute age is computed from the birthdate of a ~
                person by applying the function getYears to the birthdate."))))) 
("åge" ((:IDX (:FR "hasAge")) (:VATT (:ID "hasAge")))) 
:END

? (let ((*compiler-pass* 1))
    (make-virtual-attribute '(:name :en "French national" :fr "personne francaise")
                            '(:class "person")
                            '(:def
                               (?class "country" ?y)
                               (?result "name" :equal "france")
                               )
                            '(:type :boolean)
                            '(:doc :en "A French national is a person whose country is France.")))

? (index)
("FrenchNational"
 ((:IDX (:EN "hasFrenchNational")) (:VATT (:ID "hasFrenchNational")))) 
("PersonneFrancaise"
 ((:IDX (:FR "hasFrenchNational")) (:VATT (:ID "hasFrenchNational")))) 
("hasFrenchNational"
 ((:VATT
   (:DEF (:NAME (:EN "French national" :FR "personne francaise")) (:CLASS "person")
     (:DEF (?CLASS "country" ?Y) (?RESULT "name" :EQUAL "france")) (:TYPE :BOOLEAN)
     (:DOC :EN "A French national is a person whose country is France."))))) 
:END
|#
;;;------------------------------------------------------- make-virtual-concept
;;; JPB080106
;;; during compiler PASS 1 an entry is created in the INDEX table to track the
;;; existence of the virtual class. The function simply creates the corresponding
;;; rule
;;;
;;; Syntax:
;;;   (defvirtualconcept <mln>
;;;     (:is-a <concept-ref>)
;;;     (:def <concept-definition>)
;;;     (:doc <documentation>))

(defun make-virtual-concept (concept-name &rest option-list &aux message id)
  "saves definition into the index table to be processed later..
Arguments:
   concept-name: a multi-lingual :name list
   option-list: a possible sequence of SOL options
Return:
   a list of strings corresponding to the OWL format."
  ;; we cannot process data before the end of processing properties and attributes
  ;; since we need to know the type of some propeerties
  ;; thus we simply record the data in the index table
  (when 
    (eql *compiler-pass* 1)
    (setq 
     message
     (catch :error
       (let (concept-id )
         ;;=== check for simple syntax errors
         (unless (setq concept-name (multilingual-name? concept-name))
           (terror "Bad concept name format: ~S" concept-name))
         
         (setq concept-id (make-concept-id concept-name))
         ;; check for existing id
         (if (or (index-get concept-id :class)(index-get concept-id :vclass))
           (terror "Internal ID ~S already exists." concept-id))
         ;;=== end error check
         
         ;; make a list of index entries
         (make-indices concept-name concept-id :vclass)
         (pushnew concept-id *virtual-concept-list* :test #'string-equal)
         ;; save concept definition as well (actually all options)
         (%index-set concept-id :vclass 
                     :def (cons (list :name concept-name) option-list))
         :done)))
    ;; is message is a string then we have an error otherwise todo bem
    (if (stringp message)
      (twarn "~&;***** Error in virtual concept: ~S;~&;  ~A" 
             concept-name message))
    :done))

#|
? (index-clear)
T

? (let ((*compiler-pass* 1))
    (make-virtual-concept '(:name :en "Adult" :fr "Adulte")
                          '(:is-a  "person ")
                          '(:def 
                             (?* "age" > 18))
                          '(:doc :en "An ADULT is a person over 18.")))
:DONE

? (index)
("Adult" ((:IDX (:EN "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Adulte" ((:IDX (:FR "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Z-Adult"
 ((:VCLASS
   (:DEF (:NAME (:EN "Adult" :FR "Adulte")) (:IS-A "person ") (:DEF (?* "age" > 18))
     (:DOC :EN "An ADULT is a person over 18."))))) 
:END

? (let ((*compiler-pass* 1))
    (make-virtual-concept '(:name :en :fr "Adulte") ; bad mln format
                          '(:is-a  "person ")
                          '(:def 
                             (?* "age" > 18))
                          '(:doc :en "An ADULT is a person over 18.")))
:DONE

? (index)  ; no entry in index table
:END
|#
;;;------------------------------------------------------ make-virtual-relation
;;; JPB071231
;;; during compiler PASS 1 an entry is created in the INDEX table to track the
;;; existence of the virtual relation
;;;
;;; Syntax:
;;;   (defvirtualrelation <mln>
;;;     (:class <concept-ref>)
;;;     (:compose <relation-path>)
;;;     (:doc <documentation>))

(defun make-virtual-relation (relation-name &rest option-list 
                                                &aux message)
  "produces a list of strings corresponding to the OWL format for defining the ~
      virtual relation. Sets an entry for later processing the OWL definition. ~
      Builds the rule according to the current rule formalism.
   This function is called twice: once during the first pass, another time ~
      during the second pass.
Syntax is:
     (defvirtualrelation (:name :en \"uncle\" :fr \"oncle\")
        {(:class \"person\")}
        {(:to \"person\")}
        (:compose  \"father\" \"brother\")
        {(:max 12)}
        (:doc :en \"the property uncle is composed from father and brother.\"))
Arguments:
   relation-name: a multi-lingual :name list
   option-list: a possible sequence of SOL options
Return:
   a list of strings corresponding to the OWL format."
  ;; the idea is to create an entry in the inces table that will be processed later
  (when 
    (eql *compiler-pass* 1)
    ;; first compiler pass, we collect classes and build index entries
    (let (id)
      (setq 
       message
       (catch :error
         ;;=== check simple syntax errors. if an error, throw to :error
         ;; rough check of the name input format, removing :name tag if present
         ;; e.g. ({:name} :en "person" :fr "personne")
         (unless (mln::mln? relation-name)
           (terror "Bad relation name format ~S" relation-name))
         ;; check simple things
         (unless (assoc :compose option-list)
           (terror "Missing :compose option."))
         ;; we use the :en label whenever possible, otherwise the first of the list
         ;;     here the id should be "hasUncle"
         (setq id (make-relation-id relation-name))
         ;; check for existing id
         (if (or (index-get id :rel)(index-get id :att)(index-get id :vatt)
                 (index-get id :vrel))
           (terror "Internal ID ~S already exists." id))
         ;;=== end error check
         
         ;; save it into the concept list
         (pushnew id *virtual-relation-list* :test #'string-equal)
         ;; set index entry
         (%index-set id :vrel :def (cons (list :name relation-name) option-list))
         ;; add index entries
         (make-indices relation-name id :vrel)
         ))
      ;; is message is a string then we have an error otherwise todo bem
      (if (stringp message)
        (twarn "~&;***** Error in virtual realtion: ~S~&;  ~A" relation-name message))
      :done
      )))

#|
? (index-clear)
T
? (let ((*compiler-pass* 1))
    (defvirtualrelation (:name :en "uncle" :fr "oncle")
       (:class "person")
       (:compose "mother" "brother")
       (:to "person")
       (:max 12)
       (:doc :en "an uncle is a person who is the brother of the mother.")))
:DONE

? (index)
("Oncle" ((:IDX (:FR "hasUncle")) (:VREL (:ID "hasUncle")))) 
("Uncle" ((:IDX (:EN "hasUncle")) (:VREL (:ID "hasUncle")))) 
("hasUncle"
 ((:VREL
   (:DEF (:NAME (:EN "uncle" :FR "oncle")) (:CLASS "person")
     (:COMPOSE "mother" "brother") (:TO "person") (:MAX 12)
     (:DOC :EN "an uncle is a person who is the brother of the mother."))))) 
:END
|#
;;;---------------------------------------------------------------- merge-names

(defun merge-names (name-list)
  "reverse from extract-names. Takes a list of strings and builds a single sring~
      by concatenating the strings separated be \"; \". E.g.
   (\"sexe\" \"genre\") -> \"sexe; genre\"
Arguments;
   a list of strings
Return:
   a composite string, nil if incorrect entry"
  (if (every #'stringp name-list) 
    (apply #'concatenate 'string 
           (cdr (mapcan #'(lambda (xx) (list "; " xx)) name-list)))))

#|
? (merge-names '("papers " " letters " " la nuit   des temps "))
"papers ;  letters ;  la nuit   des temps "
|#
;;;--------------------------------------------------------- multilingual-name?

(defun multilingual-name? (expr)
  "checks whether a list starting with :name followed by a multilingual string ~
      or a multilingual string.
Argument:
   expr: something like ({:name} <multilingual-string>)
Result: 
   nil or normalized MLN (new a-list format)"
  (and (mln::mln? expr)(mln::make-mln expr)))

#|
? (multilingual-name? '(:name))
NIL
? (multilingual-name? '(:en "Albert"))
((:EN "Albert"))
? (multilingual-name? '(:name :en "Al; Albert" :fr "Albert"))
((:EN "Al" "Albert") (:FR "Albert"))
|#
;;;------------------------------------------------------- multilingual-string?

(defun multilingual-string? (expr)
  "checks whether the expression is a multilanguage string (MLS). It must be a list ~
      with alternated language tags and strings. nil is not a valid MLS.
Arguments:
   expr: expression to be tested
Return:
   expr or nil"
  (when expr (multilingual-string-1? expr)))

(defun mls? (expr)
  "same as multilingual-string?"
  (when expr (multilingual-string-1? expr)))

(defun multilingual-string-1? (expr)
  "auxiliary recursive function checking MLS"
  (or (null expr)
      (and (listp expr)
           (member (car expr) *language-tags*)
           (stringp (cadr expr))
           (multilingual-string-1? (cddr expr)))))

#|
? (mls? '(:en "sex" :fr "sexe; genre"))
T
? (mls? '(:en "sex" :fr "sexe; genre" :pl))
NIL
? (mls? nil)
NIL
|#
;;;------------------------------------------------------ ONE-OF-ALL-INSTANCES?

(defun one-of-all-instances? (object-list)
  "tests whether all objects are references to existing instances. Uses index ~
      entries to do that.
Argments:
   object-list: a list of mln or strings
Return:
   nil if not the case, list of instance-references if so."
  (let ((id-list 
         (mapcar #'(lambda (xx) 
                     (car (index-get (make-index-string xx) :inst :id)))
                 object-list)))
    ;; if we get (nil nil nil) we replace by nil
    (if (and id-list (every #'null id-list)) (setq id-list nil))
    (if (some #'null id-list)
      (terror 
       "Some objects of the :one-of option are not existing instances: ~% ~S"
       object-list))
    id-list)
  )

#|
? (one-of-all-instances? '(" france " "Italy"))
("z-france" "z-italy")
? (catch :error (one-of-all-instances? '(" france " "Italy" "zimbabwe")))
"Some objects of the :one-of option are not existing instances: 
 (\" france \" \"Italy\" \"zimbabwe\")"
? (one-of-all-instances? '())
NIL
? (catch :error (one-of-all-instances? '((:en "France")(:en "italy"))))
NIL
|#
;;;--------------------------------------------------------- process-attributes

(defun process-attributes ()
  "takes *attribute-list* and build the attribute structures. A given attribute ~
      is composed of fragments when used in different classes. Such fragments must ~
      be checked for consistency.
   - all names are merged and one is chosen as the OWL ID
   - domains if different are replaced by the LUB with a warning
   - ranges must be identical
   - :one-of must be the same
   In the last two cases, subproperties must be defined in case of conflict.
   Example of format
      (:att (:en \"name\" :fr \"nom\")(:type :name)(:unique))
      (:att (:en \"documentation\" :fr \"documentation\"))
      (:att (:en \"age range\" :fr \"tranche d'âge\")
             (:type :integer-range) 
             (:one-of (0 4) (5 12) (13 19)))
   Formalisme:
      (:att <MLS> {<value-type>} {<card-constraint>} {<value-one-of>})
Arguments:
   none
Return:
   list of OWL spec to be printed
Error:
   throws to :error tag."
  (let (result att-fragments id att-names domain-id-list range-type one-of message
               doc)
    (mark-section "Datatype Properties" *separation-width*)
    (loop
      (setq 
       message
       (catch :error
         (unless *attribute-list*
           (cformat " ")
           (mark-section "End Datatype Properties" *separation-width*)
           (rp result)
           (return-from process-attributes result))

         ;; get fragments corresponding to NEXT attribute
         ;; returns an a-list (<mln-name> ("$Person" . <att def>)(...))
         (setq att-fragments (extract-attribute-fragments))
         ;; gather the merged mln name of the attribute
         (setq att-names (pop att-fragments))
         ;; get a name out of that
         (setq id (make-attribute-id att-names)) ; e.g. "hasName"
         ;; get domains JPB0510 (moved from below) 
         ;;   e.g. ("Z-Organization" "Z-Person")
         (setq domain-id-list (get-attribute-domain att-fragments))
         ;; the name should be unique for the attribute
         (when (index-get id :att)
           (terror "We have an attribute with the same name:~S ~
                    ~%   for domain: ~S and domain: ~S"
                   id (index-get id :att :domain) domain-id-list))
         (vformat "~%===== att: ~S" id)
         ;; record attribute into index
         (index-add id :att :name att-names)
         ;; if several ranges and not all same, then error (default type is :string)
         (setq range-type (get-attribute-type att-fragments))
         
         ;; record domain and range type
         (index-add id :att :domain domain-id-list)
         (index-add id :att :range range-type)
         
         (setq one-of (get-attribute-one-of att-fragments))
         ;; get documentation if any
         (setq doc (extract-documentation-from-fragments att-fragments))
         ;; build now the OWL code for the global datatype property
         (+result (make-attribute id att-names domain-id-list range-type one-of doc))
         
         ;; Then process each fragment in turn, applying restrictions to each class
         (dolist (att att-fragments)
           ;; if local options, build OWL about clause
           (+result (process-attributes-local-options att id))
           )))
      
      ;; print each attribute in turn
      (if (stringp message)
        (twarn "~&;***** Error in:~S; ~A" att-names message)
        ;; otherwise print result
        (unless *errors-only* (rp result)))
      ;; reset result for next candidate in loop
      (setq result nil)
      )))

;;;-------------------------------------------- process-atributes-local-options

(defun process-attributes-local-options (att id)
  "for each attribute process local restrictions, namely cardinality ~
      conditions.
Arguments:
   att: attribute fragment, e.g. 
        (\"Tiger\" (:FR \" Genre \" :IT \"sexo\") (:MIN 1) (:TYPE :STRING))
   id: attribute OWL ID
Return:
   a list of OWL specs."
  ;; get attribute SOL spec, removing the :att tag
  (let ((att-spec (cdr att))
        (class-id (car att))
        result spec spec-max index-spec)
    ;; check for cardinality conditions
    (if
      ;; uniquenes
      (assoc :unique att-spec)
      (setq spec
            "<owl:cardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:cardinality>"
            )
      ;; else chack min AND max options
      (progn
        ;; min
        (if (assoc :min att-spec)
          (setq spec
                (format
                 nil
                 "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:minCardinality>"
                 (cadr (assoc :min att-spec)))))
        ;; max
        (if (assoc :max att-spec)
          (setq spec-max
                (format
                 nil
                 "<owl:maxCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:maxCardinality>"
                 (cadr (assoc :max att-spec)))))))
    ;; check indexing
    (if (assoc :index att-spec)
      (setq index-spec 
            (format
             nil
             "<indexing rdf:datatype=\"http://www.w3.org/2001/XMLSchema#boolean\">true</indexing>")))
    ;; if we got something then
    (when (or spec spec-max index-spec)
      ;; set up the stage
      (push " " result)
      (cformat "<owl:Class rdf:about=\"#~A\">" class-id)
      (rtab)
      (cformat "<rdfs:subClassOf>")
      (rtab)
      (cformat "<owl:Restriction>")
      (rtab)
      (cformat "<owl:onProperty rdf:resource=\"#~A\"/>" id)
      (if spec (cformat spec))
      (if spec-max (cformat spec-max))
      (if index-spec (cformat index-spec))
      ;; if no restriction but index, introduce a phony restriction
      (unless (or spec spec-max)
	(cformat "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">0</owl:minCardinality>"))
      (ltab)
      (cformat "</owl:Restriction>")
      (ltab)
      (cformat "</rdfs:subClassOf>")
      (ltab)
      (cformat "</owl:Class>"))
    result))

#|
? (process-attributes-local-options 
   '("Person" (:FR " Age " :IT "age") (:MIN 1) (:TYPE :non-negative-integer))
   "hasAge")

("</owl:Class>" "  </rdfs:subClassOf>" "    </owl:Restriction>"
 "      <owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:minCardinality>"
 "      <owl:onProperty rdf:resource=\"#hasAge\"/>" "    <owl:Restriction>"
 "  <rdfs:subClassOf>" "<owl:Class rdf:about=\"#Person\">" " ")

? (rp *)

<owl:Class rdf:about="#Person">
<rdfs:subClassOf>
<owl:Restriction>
<owl:onProperty rdf:resource="#hasAge"/>
<owl:minCardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:minCardinality>
</owl:Restriction>
</rdfs:subClassOf>
</owl:Class>
:END

? (process-attributes-local-options 
   '("Person" (:FR " Age " :IT "age") (:unique) (:TYPE :non-negative-integer))
   "hasAge")
...
<owl:Class rdf:about="#Person">
<rdfs:subClassOf>
<owl:Restriction>
<owl:onProperty rdf:resource="#hasAge"/>
<owl:cardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:cardinality>
</owl:Restriction>
</rdfs:subClassOf>
</owl:Class>
:END

|#
;;;--------------------------------------------------- process-atributes-one-of

(defun process-attributes-one-of (one-of-list xsd-range-type)
  "we build a list of OWL specs for making a list
Arguments:
   one-of-list: list of values, e.g. (1 2 3)
   range-type: xsd-type of a value, e.g. \"gYear\"
Return:
   a list of OWL specs."
  ;; if no value return nil
  (when one-of-list
    (let (result)
      (cformat "<rdfs:range>")
      (rtab)
      (cformat "<owl:DataRange>")
      (rtab)
      (cformat "<owl:oneOf>")
      (rtab)
      (+result 
       (reverse (process-attributes-one-of-list  one-of-list xsd-range-type)))
      (ltab)
      (cformat "</owl:oneOf>")
      (ltab)
      (cformat "</owl:DataRange>")
      (ltab)
      (cformat "</rdfs:range>")
      result)))

#|
SOL(64): (process-attributes-one-of '(1 2 3) "integer")
("  </rdfs:range>" "    </owl:DataRange>" "      </owl:oneOf>" "        </rdf:List>"
 "          </rdf:rest>" "            </rdf:List>" "              </rdf:rest>"
 "                </rdf:List>"
 "                  <rdf:rest rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#nil\"/>"
 "                  <rdf:first rdf:datatype=\"&xsd;integer\"/>3</rdf:first>"
 "                <rdf:List>" "              <rdf:rest>"
 "              <rdf:first rdf:datatype=\"&xsd;integer\"/>2</rdf:first>"
 "            <rdf:List>" "          <rdf:rest>"
 "          <rdf:first rdf:datatype=\"&xsd;integer\"/>1</rdf:first>"
 "        <rdf:List>" "      <owl:oneOf>" "    <owl:DataRange>" "  <rdfs:range>")
SOL(65): (rp *)
<rdfs:range>
<owl:DataRange>
<owl:oneOf>
<rdf:List>
<rdf:first rdf:datatype="&xsd;integer"/>1</rdf:first>
<rdf:rest>
<rdf:List>
<rdf:first rdf:datatype="&xsd;integer"/>2</rdf:first>
<rdf:rest>
<rdf:List>
<rdf:first rdf:datatype="&xsd;integer"/>3</rdf:first>
<rdf:rest rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#nil"/>
</rdf:List>
</rdf:rest>
</rdf:List>
</rdf:rest>
</rdf:List>
</owl:oneOf>
</owl:DataRange>
</rdfs:range>
:END
|#
;;;--------------------------------------------- process-attributes-one-of-list
;;; could be improved to avoid resolving the range type for each value.

(defun process-attributes-one-of-list (one-of-list xsd-range-type)
  "builds a list of codes recursively starting at the <rest> level. When we ~
      reach the end of the list we relace value with the nil special marker
Argument:
   one-of-list: list of values to add
   value-type: type of the value to add (xsd type string)
Return:
   a list of OWL codes to be printed."
  ;; construct list
  `(
    ;; start with List class
    ,(indent *left-margin* "<rdf:List>")
    ,(indent (incf  *left-margin* *indent-amount*)
             (format nil "<rdf:first rdf:datatype=\"&xsd;~A\"/>~S</rdf:first>" 
                     xsd-range-type
                     (car one-of-list)))
    ;; start the rest unless we have no more value to include
    ,@(process-attributes-one-of-list-rest (cdr one-of-list) xsd-range-type)
    ;; return on the rest
    ,(indent (decf *left-margin* *indent-amount*) "</rdf:List>")
    ;; 
    ))

(defun process-attributes-one-of-list-rest (one-of-list xsd-range-type)
  "builds the rest of the OWL list. Calls process-attributes-one-of-list recursively.
Argument:
   one-of-list: list of values to add
   value-type: type of the value to add
Return:
   a list of OWL codes to be printed."
  (if one-of-list
    `(,(indent *left-margin* "<rdf:rest>")
      ,@(prog2  ; used for indenting
         (rtab)
         (process-attributes-one-of-list one-of-list xsd-range-type)
         (ltab))
      ,(indent *left-margin* "</rdf:rest>"))
    ;; otherwise terminal state
    (list
     (indent 
      *left-margin* 
      "<rdf:rest rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#nil\"/>")
     )))

#|
? (process-attributes-one-of-list '(1 2 3) "integer")
("<rdf:List>" "  <rdf:first rdf:datatype=\"&xsd;integer\"/>1</rdf:first>"
 "  <rdf:rest>" "    <rdf:List>"
 "      <rdf:first rdf:datatype=\"&xsd;integer\"/>2</rdf:first>" "      <rdf:rest>"
 "        <rdf:List>"
 "          <rdf:first rdf:datatype=\"&xsd;integer\"/>3</rdf:first>"
 "          <rdf:rest rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#nil\"/>"
 "        </rdf:List>" "      </rdf:rest>" "    </rdf:List>" "  </rdf:rest>"
 "</rdf:List>")
? (rp (reverse *))
<rdf:List>
<rdf:first rdf:datatype="&xsd;integer"/>1</rdf:first>
<rdf:rest>
<rdf:List>
<rdf:first rdf:datatype="&xsd;integer"/>2</rdf:first>
<rdf:rest>
<rdf:List>
<rdf:first rdf:datatype="&xsd;integer"/>3</rdf:first>
<rdf:rest rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#nil"/>
</rdf:List>
</rdf:rest>
</rdf:List>
</rdf:rest>
</rdf:List>
:END
|#
;;;------------------------------------------------------------ process-indexes
;;; JPB051009

(defun process-indexes ()
  "takes *index-list* and processes each entryin turn.
An entry of the individual list is for example:
   (\"z-pyrénéesAtlantiques\" \"Z-Département\" (:FR \"pyrénées atlantiques\") (\"code\" \"64\")
  (\"région\" \"aquitaine\"))
Arguments:
   none
Return:
   list of OWL specs."
  (let (result entry-list prop-string)
    ;; sort index list
    (setq *index-list* (sort *index-list* #'string-lessp))
    ;; for each entry of the index list build an index 
    (dolist (index *index-list* )
      ;; get an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
      (setq entry-list (index-get index :idx))
      ;; print when verbose
      (vformat "~&===== idx: ~S" index)
      ;; if nil error
      (if (null entry-list)
        (twarn "Bad index entry: ~S" index)
        ;; otherwise set up index entry
        (progn
          (cformat "<Index rdf:ID=~S>" index)
          (rtab)
          ;; and process each entry in turn
          (dolist (pair entry-list)
            ;; for each pair 
            ;; get the language property
            (setq prop-string (cdr (assoc (car pair) *language-index-property*)))
            ;; for each name build an index object
            (dolist (val (cdr pair))
              (cformat "<~A rdf:resource=\"#~A\"/>" prop-string val)))
          ;; close index entry
          (ltab)
          (cformat "</Index>")
          ;; add blank line
          (cformat " "))))
    (unless *errors-only* (rp result))
    ;; return
    result
    ))

#|
? (index)

("FirstName" ((:IDX (:EN "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
("GivenName" ((:IDX (:EN "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
("Prénom" ((:IDX (:FR "hasFirstName")) (:ATT (:ID "hasFirstName")))) 
:END
? *index-list*
("FirstName" "GivenName" "Prénom")
? (process-indexes)
("   " "  </Index>" "    <isFrIndexOf rdf:resource=\"hasFirstName\"/>"
 "  <Index rdf:ID=\"Prénom\">" "   " "  </Index>"
 "    <isEnIndexOf rdf:resource=\"hasFirstName\"/>" "  <Index rdf:ID=\"GivenName\">"
 "   " "  </Index>" "    <isEnIndexOf rdf:resource=\"hasFirstName\"/>"
 "  <Index rdf:ID=\"FirstName\">")
? (rp *)
<Index rdf:ID="FirstName">
<isEnIndexOf rdf:resource="#hasFirstName"/>
</Index>

<Index rdf:ID="GivenName">
<isEnIndexOf rdf:resource="#hasFirstName"/>
</Index>

<Index rdf:ID="Prénom">
<isFrIndexOf rdf:resource="#hasFirstName"/>
</Index>

:END
|#
;;;-------------------------------------------------------- process-individuals

(defun process-individuals ()
  "takes *individual-list* and processes each entry checking validity of options.
An entry of the individual list is for example:
   (\"z-4\" \"Z-Person\" (:FR \"Jean-Paul A Barthès\" :EN \"Jean-Paul A Barthès\")
     (\"NAME\" \"Barthès\") (\"FIRST-NAME\" \"Jean-Paul\" \"A\") (\"BIRTH YEAR\" 1945)
     (\"SEX\" \"m\") (\"BROTHER\" _I-10) (\"SISTER\" _I-11) (\"WIFE\" _I-3) 
     (\"MOTHER\" _I-12)(\"FATHER\" _I-13) (\"SON\" _I-14) (\"DAUGHTER\" _I-8))
Arguments:
   none
Return:
   list of OWL specs."
  (let (result ind-fragment ind-names id class-id option-list message suc-id
               rel-id inv-id)
    (mark-section "Individuals" *separation-width*)
    ;; Establish the inverse links and record them in index
    (dolist (individual *individual-list*)
      ;; for each description do some checking
      ;; format: (<class-id> <ind-id> <ind-names> {<option>}* )
      (setq id (car individual)
            option-list (cdddr individual))
      ;; check properties
      (dolist (option option-list)
        ;; consider only relations; this returns e.g. ("hasBrother")
        (when (setq rel-id (car (index-get (make-index-string (car option)) :rel :id)))
          ;; for each successor, check existence
          ;; get inverse relation id, e.g. "isBrotherOf"
          (setq inv-id (car (index-get rel-id :rel :inv)))
          (dolist (value (cdr option))
            ;; get successor id from *index*
            ;(setq suc-id (car (index-get (make-index-string value) :inst :id)))
            ;; make the id from the MOSS reconstructed id, e.g. _I-8 => "z-8"
            (setq suc-id (make-individual-id value))
            ;; record the inverse link (it is the inverse of a single object)
            (index-add suc-id :inst :inv (cons inv-id id))
            ))))
    ;; then build the objects
    (loop
      (setq 
       message
       (catch :error
         (unless *individual-list*
           (mark-section "End Individuals" *separation-width*)
           (rp result)
           (return-from process-individuals result))

         ;; get next individual description
         (setq ind-fragment (pop *individual-list*))
         ;; format: (<class-id> <ind-id> <ind-names> {<option>}* )
         (setq id (car ind-fragment)
               class-id (cadr ind-fragment)
               ind-names (caddr ind-fragment)
               option-list (cdddr ind-fragment))
         ;; print when verbose
         (vformat "~%===== ind: ~S" id)
         ;; build index entries (done in make-individual)
         ;(+result (make-indices ind-names id :inst))
         ;; start building the output
         (cformat "<~A rdf:ID=~S>" class-id id)
         (rtab)
         ;; should make some labels
         (+result (make-labels ind-names))
         ;; process each option
         (dolist (option option-list)
           (cond
            ;; if option is :doc do as usual
            ((eql (car option) :doc)
             (+result (make-doc (cdr option))))
            ;; if option is an attribute process-individuals-attribute
            ((index-get (make-index-string (car option)) :att)
             (+result (process-individuals-att id class-id option)))
            ;; if relation same approach
            ((index-get (make-index-string (car option)) :rel)
             (+result (process-individuals-rel id class-id option)))
            ;; otherwise ignore option
            (t
             (twarn ";***** Warning: unknown option: ~S" option)
             )))
         ;; add all inverse relations
         (dolist (pair (index-get id :inst :inv))
           (cformat "<~A rdf:resource=\"#~A\"/>" (car pair)(cdr pair))
           )
         ;; end of object
         (ltab)
         (cformat "</~A>" class-id)
         (push " " result)
         ;; then check for possible local property restrictions
         result))
      
      (if (stringp message)
        (twarn ";***** When defining individual ~S: from ~S~&  ~A" 
               id (car ind-fragment) result)
        ;; otherwise print result
        (unless *errors-only* (rp result)))
      
      ;; result
      (setq result nil)
      )))

#|
(let ((*individual-list* 
       '(("z-jpb" "Z-Person" (:FR "jpb") ("gender" "masculin")))))
  (process-individuals))

<Z-Person rdf:ID="z-jpb">
<rdfs:label xml:lan="fr">jpb</rdfs:label>
<hasGender rdf:datatype="http://www.w3.org/2001/XMLSchema#STRING">masculin</hasGender>
</Z-Person>
|# 
;;;---------------------------------------------------- process-individuals-att

(defun process-individuals-att (id class-id option)
  "processes an attribute option of an individual. Takes each option and tries to make the best out of it.
Arguments:
   id: OWL ID of the individual
   class-id: OWL ID of the class of the individual
   option: attribute option e.g. (\"first name\" \"Albert\" \"jules\")
Return:
   a list of OWL spec
Error:
   throws to an :error tag"
  (let (result xsd-att-type att-one-of att-id local-restrictions)
    ;; get attribute id
    (setq att-id (car (index-get (make-index-string (car option)) :att :id)))
    (unless att-id
      (terror "Unknow atribute; ID not in *index* table at entry ~S" (car option)))
    ;; check if attribute OWL ID is in the index
    (unless (index-get att-id)
      (terror "Attribute ~S has no entry in the *index* table." att-id))
    
    ;; check now for local options, i.e. cardinality
    (setq local-restrictions
          (index-get att-id :att class-id))
    (dolist (restriction local-restrictions)
      (cond
       ;; min
       ((eql (car restriction) :min)
        (if (< (length (cdr option)) (cadr restriction))
          (terror "not enough values for ~S in ~S" (car option) id)))
       ;; max
       ((eql (car restriction) :max)
        (if (> (length (cdr option)) (cadr restriction))
          (terror "too many values (~S) for ~S in ~S" 
                  (length (cdr option)) (car option) id)))
       ;; unique
       ((eql (car restriction) :unique)
        (if (not (eql (length (cdr option)) 1))
          (terror "~S in ~S should have a unique value." (car option) id)))
       ))
    
    ;; get attribute type (default is string
    (setq xsd-att-type      ;JPB 051028
          (get-xsd-attribute-type 
           (or (car (index-get att-id :att :type)) :string)
           att-id id))
    ;; test if option is associated with :one-of
    (setq att-one-of (index-get att-id :att :one-of))
    ;; when option is there check values
    (cond
     ((and att-one-of
           (every #'(lambda(xx) (member (if (stringp xx) (make-value-string xx) xx)
                                        att-one-of :test #'equal))
                  (cdr option)))
      ;; build each value
      (dolist (value (cdr option))
        (cformat "<~A rdf:datatype=\"http://www.w3.org/2001/XMLSchema#~A\">~A</~A>"
                 att-id xsd-att-type value att-id)))
     ;; otherwise if not one of the values complain
     (att-one-of
      (terror "Some value in ~S is not one of ~&~S" option att-one-of))
     ;; when values are not prespecified do brute-force spec
     (t
      (dolist (value (cdr option))
        ;; when value is an mln, get the first synonym of the current language
        (if (mln::mln? value)
            (setq value 
                  (car (mln::extract value :language *current-language* :always t))))
        (cformat "<~A rdf:datatype=\"http://www.w3.org/2001/XMLSchema#~A\">~A</~A>"
                 att-id xsd-att-type value att-id))))
    ;; return spec list
    result))

#|
? (index)
("Clothes" ((:CLASS (:NAME (:EN "clothes"))))) 
("Size" ((:ATT (:ID "hasSize")))) 
("hasSize"
 ((:ATT ("Clothes" (:MAX 1) (:MIN 1)) (:ONE-OF "Small" "Medium" "Large")))) 
NIL

? (catch :error (process-individuals-att "Chemises" "Z-Clothes" '("Size" " medium" "large ")))
"too many values (2) for \"Size\" in \"Chemises\""

? (catch :error (process-individuals-att "Chemises" "Clothes" '("Size" " medium" )))
("<hasSize rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">\" medium\"</hasSize>")

? (catch :error (process-individuals-att "z-pyrénéesAtlantiques" "Département" '("code" "64")))
("  <hasCode rdf:datatype=\"http://www.w3.org/2001/XMLSchema#STRING\">64</hasCode>")

? (catch :error (process-individuals-att "z-0" "Z-City" '("name" (:fr "Bordeaux"))))
("<hasName rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">Bordeaux</hasName>")
|#
;;;---------------------------------------------------- process-individuals-rel

(defun process-individuals-rel (id class-id option)
  "processes a relation option. Takes each option and tries to make the best out of it.
Arguments:
   id: OWL ID of the individual
   class-id: OWL ID of the class of the individual
   option: rel option e.g. (\"département\" _I-33 _I-44)
Return:
   a list of OWL spec
Error:
   throws to an :error tag"
  (let (result rel-range rel-domain rel-one-of rel-id local-restrictions suc-id
               ind-val-id)
    ;; get relation id
    (setq rel-id (car (index-get (make-index-string (car option)) :rel :id)))
    (unless rel-id
      (terror "Unknow relation; ID not in *index* table at entry ~S" (car option)))
    ;; check if relation OWL ID is in the index
    (unless (index-get rel-id)
      (terror "Relation ~S has no entry in the compiler *index* table." rel-id))
    
    ;; check now for local options, i.e. cardinality
    (setq local-restrictions
          (index-get rel-id :rel class-id))
    (dolist (restriction local-restrictions)
      (cond
       ;; min
       ((eql (car restriction) :min)
        (if (< (length (cdr option)) (cadr restriction))
          (terror "not enough values for ~S in ~S" (car option) id)))
       ;; max
       ((eql (car restriction) :max)
        (if (> (length (cdr option)) (cadr restriction))
          (terror "too many values (~S) for ~S in ~S" 
                  (length (cdr option)) (car option) id)))
       ;; unique
       ((eql (car restriction) :unique)
        (if (not (eql (length (cdr option)) 1))
          (terror "~S in ~S should have a unique value." (car option) id)))
       ;; one-of is a local restriction ?
       ))
    ;; we can also check that the property is in the domain of the individual class
    ;; ********** could be more complex with subproperties and subclasses
    (setq rel-domain (car (index-get rel-id :rel :domain)))
    (unless (is-instance-of? id rel-domain)
      (twarn "***** Error: property ~S is not one of the classes ~S~&~
              when creating individual ~S"
             rel-id rel-domain id))
    
    ;; get relation range classes
    (setq rel-range (car (index-get rel-id :rel :range)))
    
    ;; test if option is associated with :one-of, in which case we get the list of
    ;; individual ids
    (setq rel-one-of (index-get rel-id :rel :one-of))   
    ;; when option is there, check if value designates one of the option individuals
    (when rel-one-of
      ;; if more than one value complain (remember we are in a ONE-of option)
      (when (cddr option)
        (terror "too many values: ~S for property ~S in individual ~S"
                (cdr option) (car option) id))
      ;; first get option individual id
      (setq ind-val-id 
            (car (index-get (make-index-string (cadr option)) :inst :id)))
      ;; if instance-id, e.g. "$female" is part of the :one-of list, we win
      (if (member ind-val-id rel-one-of :test #'equal)
        ;; in which case we build the value
        (cformat "<~A rdf:resource=\"#~A\"/>" rel-id ind-val-id)
        ;; otherwise problem
        (terror "The value ~S is not one of ~&~S" ind-val-id rel-one-of)))
    
    ;; when values are not prespecified do brute-force spec
    ;; we check however that each oject to be linked is of proper class
    
    (unless rel-one-of
      (dolist (value (cdr option))  ; value is something like _I-44
        ;; get successor id from *index*
        (setq suc-id (make-individual-id value))
;(break "process-individual id: ~A rel-id: ~A suc-id: ~A" id rel-id suc-id) 
        ;; if not there complain
        (cond
         ;; check if of the right type
         ;; ********* could belong to a subclass...
         ((not (is-instance-of? suc-id rel-range))
          (twarn "***** Error: individual ~S is not of type ~S~&~
                  when populating ~S relation for ~S"
                 suc-id rel-range rel-id id))
         ;; otherwise OK
         (t 
          (cformat "<~A rdf:resource=\"#~A\"/>" rel-id suc-id)
          ))))
    
    ;; return spec list
    result))

#|
? (process-individuals-rel "pyrénéesAtlantiques" "Département" '("région" "aquitaine"))
("<hasRegion rdf:resource=\"#z-aquitaine\"/>")
? (catch :error (process-individuals-rel "pyrénéesAtlantiques" "Département" 
                                         '("ville" "aquitaine")))
NIL
? (errors)

***** Error: individual "aquitaine" is not of type ("Z-City")
when populating "hasTown" relation for "pyrénéesAtlantiques"
:END
|#
;;;---------------------------------------------------------- process-relations
;;; adding the inverse relations. Assumes that an owl:inverse exists (OWL extension)

(defun process-relations ()
  "takes *relation-list* and build the relation structures. A given relation ~
      is composed of fragments when used in different classes. Such fragments must ~
      be checked for consistency.
   - all names are merged and one is chosen as the OWL ID
   - domains are collected
   - ranges are collected
   - if both domain and range are multiple an error is declared
  - inverse properties are created automatically
   - :one-of must indicates instances
   In the last two cases, subproperties must be defined in case of conflict.
   Examples of format
      (:rel (:en \"brother\" :fr \"frère\")(:type \"person\"))
      (:rel (:en \"employer\" :fr \"employeur\") (:type \"organisation\"))
      (:rel (:en \"employer\" :fr \"employeur\") (:type \"person\"))
      (:rel (:en \"sex; gender\" :fr \"sexe\") (:unique)
              (:one-of (:en \"male\"  :fr \"masculin\") (:en \"female\" :fr \"féminin\"))))
   Formalisme:
      (:rel <MLN> {<suc-type>} {<card-constraint>} {<suc-one-of>})
Arguments:
   none
Return:
   list of OWL spec to be printed
Error:
   throws to :error tag."
  (let (result rel-fragments id rel-names domain-id-list range-id-list one-of
               message inv-names inv-id doc one-of-id)
    (mark-section "Object Properties" *separation-width*)
    (loop
      (setq 
       message
       (catch :error
         (unless *relation-list*
           (mark-section "End Object Properties" *separation-width*)
           (rp result)
           (return-from process-relations nil))
         ;; get fragments corresponding to next relation
         ;; returns an a-list (<name-list> ("$Person" . <att def>)(...))
         (setq rel-fragments (extract-relation-fragments))
         ;; gather all names
         (setq rel-names (pop rel-fragments))
         ;; get an id out of that
         (setq id (make-relation-id rel-names))
         ;; print relation id
         (vformat "~%===== rel: ~S" id)
         ;; cook up inverse names
         (setq inv-names (make-inverse-names rel-names))
         ;; make an id
         (setq inv-id (make-inverse-relation-id rel-names))
         ;; print it
         (vformat "~%===== inv: ~S" inv-id)
         ;; fix domains
         (setq domain-id-list (get-relation-domain rel-fragments))
         ;; same for ranges
         (setq range-id-list (get-relation-range rel-fragments))
         ;; if both domain and range are multiple then error
         (if (and (cdr domain-id-list) (cdr range-id-list))
           (terror "multiple domains and ranges for relation ~S:~&~S"
                   rel-names rel-fragments))
         ;; check for one-of option (this is a range option (global)
         ;; should be all the same: nil none, (nil) failure, (option) OK
         (setq one-of (get-relation-one-of rel-fragments))
         ;(print `(=====>ONE-OF ,one-of))
         (when one-of
           ;; build a special class "Z-XxxType" to attach individual options
           ;; e.g. as an enumerated class
           ;; (defconcept (:en "<id> type") (:one-of ...))
           ;; use the new class id for the range 
           (setq one-of-id 
                 (concatenate 'string (make-concept-id rel-names) "Type"))
           ;; add class to range (that should be empty)
           (push one-of-id range-id-list))
         
         ;; record relation into index, domain, range and inverse
         (index-add id :rel :name rel-names)
         (index-add id :rel :domain domain-id-list)
         (index-add id :rel :range range-id-list)
         (index-add id :rel :inv inv-id)
         ;; do the same for inverse property
         (index-add inv-id :rel :name inv-names)
         (index-add inv-id :rel :domain range-id-list)
         (index-add inv-id :rel :range (or domain-id-list '("owl:Thing"))) ;JPB0510
         (index-add inv-id :rel :inv id)
         
         ;; get documentation if any
         (setq doc (extract-documentation-from-fragments rel-fragments))
         
         ;; build now the OWL code for the global object property
         (push " " result)
         (+result (make-relation id rel-names domain-id-list range-id-list one-of doc))
         ;; and for the inverse relation
         (push " " result)
         (+result (make-relation inv-id inv-names range-id-list domain-id-list nil nil))         
         
         ;; Then process each fragment in turn, applying local restrictions
         (dolist (rel rel-fragments)
           ;; if local options, build OWL about clause
           (+result (process-relations-local-options rel id))
           )
         result))
      ;; print each relation in turn
      (if (stringp message)
        (twarn "~&;***** Error in:~S; ~A" rel-names message)
        ;; otherwise print result
        (unless *errors-only* (rp result)))
      ;; reset result
      (setq result nil)
      ) ; end of loop on fragments
    ))

#|
? (setq *relation-list* 
        '(("Conurbation" (:NAME :EN "town" :FR "ville") (:TO "city"))))
(("Conurbation" (:NAME :EN "town" :FR "ville") (:TO "city")))
? (catch :error (process-relations))
(" " "</owl:ObjectProperty>" "  <rdfs:range rdf:resource=\"#Z-City\"/>"
 "  <rdfs:domain rdf:resource=\"#Z-Conurbation\"/>"
 "  <rdfs:label xml:lan=\"fr\">ville</rdfs:label>"
 "  <rdfs:label xml:lan=\"en\">town</rdfs:label>"
 "<owl:ObjectProperty rdf:ID=\"hasTown\">" " " "</Index>"
 "  <isFrIndexOf rdf:resource=\"hasTown\"/>" "<Index rdf:ID=\"Ville\">" " "
 "</Index>" "  <isEnIndexOf rdf:resource=\"hasTown\"/>" "<Index rdf:ID=\"Town\">"
 " ")
? (rp *)


<Index rdf:ID="Town">
<isEnIndexOf rdf:resource="hasTown"/>
</Index>

<Index rdf:ID="Ville">
<isFrIndexOf rdf:resource="hasTown"/>
</Index>

<owl:ObjectProperty rdf:ID="hasTown">
<rdfs:label xml:lan="en">town</rdfs:label>
<rdfs:label xml:lan="fr">ville</rdfs:label>
<rdfs:domain rdf:resource="#Z-Conurbation"/>
<rdfs:range rdf:resource="#Z-City"/>
<owl:InverseOf rdf:resource="#isTownOf"/>
</owl:ObjectProperty>


<Index rdf:ID="TownOf">
<isEnIndexOf rdf:resource="isTownOf"/>
</Index>

<Index rdf:ID="VilleDe">
<isFrIndexOf rdf:resource="isTownOf"/>
</Index>

<owl:ObjectProperty rdf:ID="isTownOf">
<rdfs:label xml:lan="en">town of</rdfs:label>
<rdfs:label xml:lan="fr">ville de</rdfs:label>
<rdfs:domain rdf:resource="#Z-City"/>
<rdfs:range rdf:resource="#Z-Conurbation"/>
<owl:InverseOf rdf:resource="#hasTown"/>
</owl:ObjectProperty>
:END
|#
;;;--------------------------------------------- process-relations-local-options

(defun process-relations-local-options (rel id)
  "for each relation process local restrictions, namely cardinality ~
      conditions.
Arguments:
   rel: relation fragment, e.g. 
        (\"Tiger\" (:FR \" frère \" :IT \"fratello\") \"person\" (:MIN 1) )
   id: relation OWL ID, e.g. \"hasGender\"
Return:
   a list of OWL specs."
  ;; get relations SOL spec, removing the :rel tag
  (let ((rel-spec (cdr rel))
        (class-id (car rel))
        result spec spec-max)
    ;; check for cardinality conditions
    (if
      ;; uniquenes
      (assoc :unique rel-spec)
      (setq spec
            "<owl:cardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:cardinality>"
            )
      ;; else chack min AND max options
      (progn
        ;; min
        (if (assoc :min rel-spec)
          (setq spec
                (format
                 nil
                 "<owl:minCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:minCardinality>"
                 (cadr (assoc :min rel-spec)))))
        ;; max
        (if (assoc :max rel-spec)
          (setq spec-max
                (format
                 nil
                 "<owl:maxCardinality rdf:datatype=\"&xsd;nonNegativeInteger\">~S</owl:maxCardinality>"
                 (cadr (assoc :max rel-spec)))))))
    ;; if we got something then
    (when (or spec spec-max)
      ;; set up the stage
      ;(push " " result)
      (cformat "<owl:Class rdf:about=\"#~A\">" class-id)
      (rtab)
      (cformat "<rdfs:subClassOf>")
      (rtab)
      (cformat "<owl:Restriction>")
      (rtab)
      (cformat "<owl:onProperty rdf:resource=\"#~A\"/>" id)
      (if spec (cformat spec))
      (if spec-max (cformat spec-max))
      (ltab)
      (cformat "</owl:Restriction>")
      (ltab)
      (cformat "</rdfs:subClassOf>")
      (ltab)
      (cformat "</owl:Class>"))
    result))

#|
? (process-relations-local-options 
   '("Person" (:en "Gender; sex" :fr "genre") (:one-of (:fr "masculin" :en "male")(:en "female" :fr "féminin")) (:unique))
   "hasGender")
("  </owl:Class>" "    </rdfs:subClassOf>" "      </owl:Restriction>"
 "        <owl:cardinality rdf:datatype=\"&xsd;nonNegativeInteger\">1</owl:cardinality>"
 "        <owl:onProperty rdf:resource=\"hasGender\"/>" "      <owl:Restriction>"
 "    <rdfs:subClassOf>" "  <owl:Class rdf:about=\"#Z-Person\">" " ")
? (rp *)

<owl:Class rdf:about="#Z-Person">
<rdfs:subClassOf>
<owl:Restriction>
<owl:onProperty rdf:resource="hasGender"/>
<owl:cardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:cardinality>
</owl:Restriction>
</rdfs:subClassOf>
</owl:Class>
:END
|#
;;;--------------------------------------------------- process-relations-one-of

(defun process-relations-one-of (one-of class-id rel-name rel-id)
  "here we have a relation with a one-of option. We build a special class with ~
      name $XxxType and link to that-class.
Arguments:
   one-of: the one-of option, e.g. (:one-of (:fr \"masculin\" :en \"male\")
                     (:en \"female\" :fr \"féminin\"))
   class-id: the id of the newly created class
   rel-name: the multilingual definition of the relation
   rel-id: the OWL ID of the relation, e.g. \"$hasGender\"
Result:
   a list of OWL specs concerning the new class."
  (let ((*left-margin* 0) result index-specs)
    (+result (make-indices 
              (list :en (concatenate 'string (get-id-string rel-name) " Type"))
              class-id 
              :class))
    (cformat "<owl:Class rdf:ID=~S>" class-id)
    (rtab)
    (multiple-value-bind (class-result index-result)
                         (make-concept-one-of (cdr one-of) class-id rel-id)
      (+result class-result)
      (setq index-specs index-result))
    (ltab)
    (cformat "</owl:Class>")
    (push " " result)
    ;; now indices
    (+result index-specs)))

#|
SOL(69): (process-relations-one-of '(:one-of (:fr "masculin" :en "male")
                                     (:en "female" :fr "féminin")) "Z-GenderType"
                                   '(:fr "genre" :en "gender; sex") "hasGender")
(" " "</owl:Class>" "  </owl:equivalentClass>" "    </owl:Class>"
 "      </owl:oneOf>" "        </Z-GenderType>"
 "          <rdfs:label xml:lang=\"fr\">féminin</rdfs:label>"
 "          <rdfs:label xml:lang=\"en\">female</rdfs:label>"
 "        <Z-GenderType rdf:ID=\"z-female\">"
 "        </Z-GenderType>" ...)
SOL(70): (rp *)
<owl:Class rdf:ID="Z-GenderType">
<owl:equivalentClass>
<owl:Class>
<owl:oneOf rdf:parseType="Collection">
<Z-GenderType rdf:ID="z-male">
<rdfs:label xml:lang="fr">masculin</rdfs:label>
<rdfs:label xml:lang="en">male</rdfs:label>
</Z-GenderType>
<Z-GenderType rdf:ID="z-female">
<rdfs:label xml:lang="en">female</rdfs:label>
<rdfs:label xml:lang="fr">féminin</rdfs:label>
</Z-GenderType>
</owl:oneOf>
</owl:Class>
</owl:equivalentClass>
</owl:Class>
:END
|#
;;;------------------------------------------------- process-virtual-attributes
;;; JPB080111

(defun process-virtual-attributes ()
  "takes *virtual-attribute-list* and processes each entry in turn.
An entry of the individual list is for example:
;   (\"hasAge\"
;     ((:VATT 
;        (:TYPE (:INTEGER))
;        (:DOC
;          (:EN \"the attribute age is computed from the birthdate of a ~
      ;                     person by applying the function getYears to the birthdate.\"))
;        (:CLASS (\"person\")) 
;        (:NAME :EN \"age\" :FR \"aage\"))))
Arguments:
   none
Return:
   list of OWL specs."
  (let (result attribute-name domain domain-id-list type doc definition)
    ;; sort index list and print header
    (setq *virtual-attribute-list* (sort *virtual-attribute-list* #'string-lessp))
    (mark-section "Virtual Datatype Properties" *separation-width*)
    (unless *errors-only* (rp result))
    (rtab)
    
    ;; for each entry of the index list build an index 
    (dolist (vatt *virtual-attribute-list* )
      (with-warning (attribute-name)
        ;; get the a-list defining the attribute, e.g. 
        ;;   ((:NAME (:EN "hasFirstName" :FR "...")) (:DEF ...) ...)
        (setq definition (index-get vatt :vatt :def))
        ;(print `(++++++++++ definition of vatt ,definition))
        (setq attribute-name (cadr (assoc :name definition))
              domain (cdr (assoc :class definition))
              doc (cdr (assoc :doc definition))
              type (cadr (assoc :type definition))
              )
        ;; check errors
        (cond
         ((null attribute-name)
          (terror "Bad virtual attribute entry: ~S" vatt))
         ((null domain)
          (terror "Bad or missing virtual :class option."))
         )
        
        ;; build the list of domain ids
        (setq domain-id-list (mapcar #'(lambda (xx) 
                                         (make-concept-id 
                                          (list sol::*sol-language* xx)))
                                     domain))
        ;; set up index entry
        ;; print when verbose
        (vformat "~&===== att: ~S" vatt)
        ;; cook up the OWL description
        (+result
         (make-owl-virtual-attribute vatt attribute-name domain-id-list type doc))
        
        ;; process local cardinality options (restrictions)
        (+result 
         (process-attributes-local-options  
          (list* (car domain-id-list) attribute-name definition)
          vatt))
        
        ;;=== produce attribute rule
        ;; call the function building the rule (in the sol2rule file)
        (apply #'make-virtual-attribute-rule attribute-name definition)
        ;; add blank line
        (cformat " ")
        )
      ;; reset result for each attribute
      (setq result nil))
    
    ;; before closing the section process virtual composed attributes
    (setq result nil)
    (process-virtual-composed-attributes)    
    
    (ltab)
    ;; add blank line
    (mark-section "End Virtual Datatype Properties" *separation-width*)
    (unless *errors-only* (rp result))
    
    ;; return
    :done
    ))

#| (index) (index-get "hasAge" :vatt :def)
? (reset)
T
? (index-clear)
T
? (let ((*compiler-pass* 1))
    (make-concept '(:name :en "person" :fr "personne"))
    (make-virtual-attribute 
     '(:name :en "age" :fr "‰ge")
     '(:class "person")
     '(:def
        (?class "birth date" ?y)
        ("getYears" ?y ?*)
        )
     '(:type :integer)
     '(:min 1)
     '(:doc :en "the attribute age is computed from the birthdate of a ~
                 person by applying the function getYears to the birthdate.")))
:DONE

? (index)
("Age" ((:IDX (:EN "hasAge")) (:VATT (:ID "hasAge")))) 
("Person" ((:IDX (:EN "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Personne" ((:IDX (:FR "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Z-Person" ((:CLASS (:NAME :EN "person" :FR "personne")))) 
("hasAge"
 ((:VATT
   (:DEF (:NAME (:EN "age" :FR "‰ge")) (:CLASS "person")
    (:DEF (?CLASS "birth date" ?Y) ("getYears" ?Y ?*)) (:TYPE :INTEGER) (:MIN 1)
    (:DOC :EN "the attribute age is computed from the birthdate of a ~
                 person by applying the function getYears to the birthdate."))))) 
("åge" ((:IDX (:FR "hasAge")) (:VATT (:ID "hasAge")))) 
:END

? (process-virtual-attributes)

<!-- +++++++++++++++++++++++++ Virtual Datatype Properties ++++++++++++++++++++++++++ -->


### the attribute age is computed from the birthdate of a person by applying the function getYears to the birthdate.

[Role_Age:
   (?SELF test4:hasAge ?RESULT)
<-
   (?SELF rdf:type test4:Z-Person)
   (?CLASS birth date ?Y)
   (getYears ?Y ?SELF)
 ]
  <VirtualDatatypeProperty rdf:ID="hasAge">
    <rdfs:label xml:lang="en">age</rdfs:label>
    <rdfs:label xml:lang="fr">‰ge</rdfs:label>
    <rdfs:domain rdf:resource="#Z-Person"/>
    <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#integer"/>
    <rdfs:comment xml:lang="en">the attribute age is computed from the birthdate of a person by applying the function getYears to the birthdate.</rdfs:comment>
  </VirtualDatatypeProperty>
 
  <owl:Class rdf:about="#Z-Person">
    <rdfs:subClassOf>
      <owl:Restriction>
        <owl:onProperty rdf:resource="#hasAge"/>
        <owl:minCardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:minCardinality>
      </owl:Restriction>
    </rdfs:subClassOf>
  </owl:Class>
   
<!-- +++++++++++++++++++++++ End Virtual Datatype Properties ++++++++++++++++++++++++ -->

:DONE

? (let ((*compiler-pass* 1))
    (make-virtual-attribute '(:name :en "French national" :fr "personne francaise")
                            '(:class "person")
                            '(:def
                               (?class "country" ?y)
                               (?result "name" :equal "france")
                               )
                            '(:type :boolean)
                            '(:doc :en "A French national is a person whose country is France.")))

? (index)
("FrenchNational"
 ((:IDX (:EN "hasFrenchNational")) (:VATT (:ID "hasFrenchNational")))) 
("PersonneFrancaise"
 ((:IDX (:FR "hasFrenchNational")) (:VATT (:ID "hasFrenchNational")))) 
("hasFrenchNational"
 ((:VATT
   (:DEF (:NAME (:EN "French national" :FR "personne francaise")) (:CLASS "person")
     (:DEF (?CLASS "country" ?Y) (?RESULT "name" :EQUAL "france")) (:TYPE :BOOLEAN)
     (:DOC :EN "A French national is a person whose country is France."))))) 
:END

? (process-virtual-attributes)

### A French national is a person whose country is France.

[Role_FrenchNational:
   (?SELF FrenchNational ?RESULT)
<-
   (?SELF rdf:type test4:Z-Person)
   (?CLASS test4:Z-Country ?Y)
   (?RESULT test4:hasName ?V1323)
   equal(?V1323, "france")
 ]
  <VirtualDatatypeProperty rdf:ID="hasFrenchNational">
    <rdfs:label xml:lang="en">French national</rdfs:label>
    <rdfs:label xml:lang="fr">personne francaise</rdfs:label>
    <rdfs:domain rdf:resource="#Z-Person"/>
    <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#boolean"/>
    <rdfs:comment xml:lang="en">A French national is a person whose country is France.</rdfs:comment>
  </VirtualDatatypeProperty>
   
|#
;;;---------------------------------------- process-virtual-composed-attributes

(defun process-virtual-composed-attributes ()
  "a virtual relation is one obtained by composition. Syntax is:
;   (defvirtualrelation <mln>
;     (:class <concept-ref>)
;     (:compose <relation-path>)
;     {(:to <range>)}
;     {(:min nn) and/or (:max nn) | (:unique)}
;    (:doc <documentation>))
The function processes each entry of the *virtual-relation-list*
Argument:
   none
Return:
   unimportant."
  (let (result attribute-name domain domain-id-list doc definition type)
    ;; sort index list
    (setq *virtual-composed-attribute-list* 
          (sort *virtual-composed-attribute-list* #'string-lessp))
    ;; for each entry of the index list build an index 
    (dolist (vatt *virtual-composed-attribute-list*)
      
      (with-warning (attribute-name)
        ;; definition is an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
        (setq definition (index-get vatt :vatt :def))
        ;(print `(++++++++++ definition ,definition))
        (setq attribute-name (cadr (assoc :name definition)))
        (unless attribute-name
          (terror "Bad virtual attribute entry: ~S" vatt))
        
        (setq  domain (cdr (assoc :class definition))
               type (cadr (assoc :type definition))
               doc (cdr (assoc :doc definition))
               )        
        (cond
         ((null domain)
          (terror "Missing :class option (domain)."))
         ((null type)
          (setq type :string)))
        
        ;; build the list of domain ids and range ids
        (setq domain-id-list (mapcar #'(lambda (xx) 
                                         (make-concept-id 
                                          (list sol::*sol-language* xx)))
                                     domain))
        ;; print attribute
        (vformat "~&===== att: ~S" vatt)
        ;; produce OWL output
        (cformat "<VirtualDatatypeProperty rdf:ID=~S>" vatt)
        (rtab)
        ;; process labels
        (+result (make-labels attribute-name))
        ;; domain is class option or domain of first class of compose
        (+result (make-domains domain-id-list))
        ;; process type and doc
        (+result (make-type type))           
        (+result (make-doc doc))           
        (ltab)
        ;; add blank line
        (cformat " ")
        
        (cformat "</VirtualDatatypeProperty>")
        
        ;;=== process local options if any (e.g. cardinality)
        (+result 
         (process-attributes-local-options  
          `(,(car domain-id-list) ,attribute-name ,@definition)
          vatt))           
        
        ;;=== make now rule (get the options back from the index :def entry)
        (apply #'make-virtual-relation-rule attribute-name definition)
        )
      ;; reset result for each entry
      (setq result nil))
    ;; return
    :done))

#|
? (let ((*compiler-pass* 1))
     (make-virtual-attribute
      '(:name :en "uncle-age" :fr "aage de l'oncle")
      '(:class "person")
      '(:compose "mother" "brother" "age")
      '(:type :non-negative-integer)
      '(:unique)
      '(:doc :en "test for virtual composed attribute.")))
:DONE

? (index)
(...
("Uncle-Age" ((:IDX (:EN "hasUncle-Age")) (:VATT (:ID "hasUncle-Age")))) 
("hasUncle-Age"
 ((:VATT
   (:DEF (:NAME (:EN "uncle-age" :FR "aage de l'oncle")) (:CLASS "person")
    (:COMPOSE "mother" "brother" "age") (:TYPE :NON-NEGATIVE-INTEGER) (:UNIQUE)
    (:DOC :EN "test for virtual composed attribute.")))))
 ("ågeDeLOncle" ((:IDX (:FR "hasUncle-Age")) (:VATT (:ID "hasUncle-Age")))
 ...)

? (process-virtual-composed-attributes)

### test for virtual composed attribute.

[Role_Uncle-Age:
   (?V1119 test4:hasUncle-Age ?V1122)
<-
   (?V1119 test4:hasMother ?V1120)
   (?V1120 test4:hasBrother ?V1121)
   (?V1121 test4:hasAge ?V1122)
 ]
  <VirtualDatatypeProperty rdf:ID="hasUncle-Age">
    <rdfs:label xml:lang="en">uncle-age</rdfs:label>
    <rdfs:label xml:lang="fr">aage de l'oncle</rdfs:label>
    <rdfs:domain rdf:resource="#Z-Person"/>
    <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#nonNegativeInteger"/>
    <rdfs:comment xml:lang="en">test for virtual composed attribute.</rdfs:comment>
   
  </VirtualDatatypeProperty>
  <owl:Class rdf:about="#Z-Person">
    <rdfs:subClassOf>
      <owl:Restriction>
        <owl:onProperty rdf:resource="#hasUncle-Age"/>
        <owl:cardinality rdf:datatype="&xsd;nonNegativeInteger">1</owl:cardinality>
      </owl:Restriction>
    </rdfs:subClassOf>
  </owl:Class>
|#
;;;--------------------------------------------------- process-virtual-concepts
;;; JPB071279
;;; because virtual concepts can make references to virtual properties, they must
;;; be processed after the properties have been processed

(defun process-virtual-concepts ()
  "takes *virtual-concept-list* and processes each entry in turn.
An entry of the individual list is for example:
;(\"Z-Adult\"
; ((:VCLASS (:DOC (:EN \"An ADULT is a person over 18.\")) (:IS-A (\"person \"))
;           (:NAME :EN \"Adult\" :FR \"Adulte\"))))
Arguments:
   none
Return:
   list of OWL specs."
  (let (result superclass concept-name doc definition)
    ;; sort index list
    (setq *virtual-concept-list* (sort *virtual-concept-list* #'string-lessp))
    (mark-section "Virtual Classes" *separation-width*)
    ;; for each entry of the index list build an index 
    (dolist (vclass *virtual-concept-list* )
      (with-warning (concept-name)
        ;; get an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
        (setq definition (index-get vclass :vclass :def)
              concept-name (cadr (assoc :name definition))
              superclass (cadr (assoc :is-a definition))
              doc (cdr (assoc :doc definition))
              )
        ;; print when verbose
        (vformat "~&===== idx: ~S" vclass)
        ;; if nil error
        (if (null concept-name)
          (twarn "Bad virtual class entry: ~S" vclass)
          ;; otherwise set up index entry
          (progn
            (cformat "<VirtualClass rdf:ID=~S>" vclass)
            (rtab)
            ;; now process labels
            (+result (make-labels concept-name))
            ;; if null superclass then issue a warning
            (if superclass
              (+result (make-subclass superclass vclass))
              (twarn "Missing superclass in virtual class entry: ~S" vclass))
            (+result (make-doc doc))
            ;; and process each entry in turn
            (ltab)
            (cformat "</VirtualClass>")
            ;; add blank line
            (cformat " ")
            ;; build rule 
            (apply #'make-virtual-concept-rule concept-name definition)
            )))
      ;; reset print list
      (setq result nil)
      )
    (mark-section "End Virtual Classes" *separation-width*)
    (unless *errors-only* (rp result))
    ;; return
    :done
    ))

#|
? (index-clear)
T
? (let ((*compiler-pass* 1))
     (make-concept '(:name :en "person" :fr "personne"))
     (make-virtual-concept '(:name :en "Adult" :fr "Adulte")
        '(:is-a  "person ")
        '(:def 
             (?* "age" > 18))
        '(:doc :en "An ADULT is a person over 18.")))
:DONE

? (let ((*compiler-pass* 2))
    (make-concept '(:name :en "person" :fr "personne"))
    )

<owl:Class rdf:ID="Z-Person">
  <rdfs:label xml:lang="en">person</rdfs:label>
  <rdfs:label xml:lang="fr">personne</rdfs:label>
</owl:Class>

? *virtual-concept-list*
("Z-Adult")

? (index)
("Adult" ((:IDX (:EN "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Adulte" ((:IDX (:FR "Z-Adult")) (:VCLASS (:ID "Z-Adult")))) 
("Person" ((:IDX (:EN "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Personne" ((:IDX (:FR "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Z-Adult"
 ((:VCLASS
   (:DEF (:NAME (:EN "Adult" :FR "Adulte")) (:IS-A "person ") (:DEF (?* "age" > 18))
    (:DOC :EN "An ADULT is a person over 18."))))) 
("Z-Person" ((:CLASS (:NAME :EN "person" :FR "personne")))) 
:END

? (let ((*compiler-pass* 2))
        (process-virtual-concepts))
<!-- +++++++++++++++++++++++++++++++ Virtual Classes ++++++++++++++++++++++++++++++++ --!>

<owl:VirtualClass rdf:ID="Z-Adult">
  <rdfs:label xml:lang="en">Adult</rdfs:label>
  <rdfs:label xml:lang="fr">Adulte</rdfs:label>
  <rdfs:subClassOf rdf:resource="#Z-Person"/>
  <rdfs:comment xml:lang="en">An ADULT is a person over 18.</rdfs:comment>
</owl:VirtualClass>
  in (?SELF "age" > 18)

### An ADULT is a person of 18 years of age or more.

[Role_Adult:
   (?SELF rdf:type test4:Z-Adult)
<-
   (?SELF rdf:type test4:Z-Person)
   (?SELF test4:hasAge ?V1577)
   ge(?V1577, 18)
 ]
<!-- +++++++++++++++++++++++++++++ End Virtual Classes ++++++++++++++++++++++++++++++ --!>

:DONE
|#
;;;-------------------------------------------------- process-virtual-relations
;;; JPB080110
;;; process-virtual-relations actually create the OWL code corresponding to the
;;; virtual relation and inverse cirtual relation.
;;; a virtual relation cannot have the same internal name as a relation, attribute,
;;; or virtual attribute (or concept or virtual concept as a matter of fact).
;;; checks are made for domain and range, and local options 

(defun process-virtual-relations ()
  "a virtual relation is one obtained by composition. Syntax is:
;   (defvirtualrelation <mln>
;     (:class <concept-ref>)
;      (:compose <relation-path>)
;      {(:to <range>)}
;      {(:min nn) and/or (:max nn) | (:unique)}
;      (:doc <documentation>)
;      )
The function processes each entry of the *virtual-relation-list*
   Argument:
none
   Return:
unimportant."
  (let (result relation-name range domain doc definition
               inv-id inv-name range-id-list domain-id-list)
    ;; sort index list
    (setq *virtual-relation-list* (sort *virtual-relation-list* #'string-lessp))
    (mark-section "Virtual Object Properties" *separation-width*)
    ;; for each entry of the index list build an index 
    (dolist (vrel *virtual-relation-list* )
      (with-warning (relation-name)
        ;; definition is an a-list like ((:EN "hasFirstName")(:FR "..." "..."))
        (setq definition (index-get vrel :vrel :def))
        (setq
         relation-name (cadr (assoc :name definition))
         domain (cdr (assoc :class definition))
         range (cdr (assoc :to definition))
         doc (cdr (assoc :doc definition))
         )
        ;; build the list of domain ids and range ids
        (setq domain-id-list (mapcar #'(lambda (xx) 
                                         (make-concept-id 
                                          (list sol::*sol-language* xx)))
                                     domain)
              range-id-list (mapcar #'(lambda (xx) 
                                        (make-concept-id 
                                         (list sol::*sol-language* xx)))
                                    range))
        ;; print when verbose
        (vformat "~&===== rel: ~S" vrel)
        
        (cond
         ;; check now domain and range
         ((null domain)
          (twarn "Missing :class option (domain) in virtual relation: ~S" vrel))
         ((null range)
          (twarn "Missing :to option (range) in virtual relation: ~S" vrel))
         ;; otherwise set up index entry
         (t
          (setq inv-id (make-inverse-relation-id relation-name))
          (+result (make-owl-virtual-relation vrel relation-name domain-id-list 
                                              range-id-list doc inv-id))
          
          ;; process cardinality option if there (OWL output only)
          (+result 
           (process-relations-local-options  
            `(,(car domain-id-list) ,relation-name ,@definition)
            vrel))
          
          ;; build inverse prop
          ;;********** we should enter the inverse references into the index table
          (vformat "~&===== inv: ~S" inv-id)
          ;; and for the inverse relation
          (push " " result)
          ;; cook up inverse names
          (setq inv-name (make-inverse-names relation-name))
          (+result (make-owl-virtual-relation inv-id inv-name range-id-list 
                                              domain-id-list nil vrel))         
          ))
        
        ;; make now rule (get the options back from the index :def entry)
        (apply #'make-virtual-relation-rule relation-name 
               (index-get (make-relation-id relation-name) :vrel :def))
        )
      ;; reset result for next relation
      (setq result nil)
      )
    
    (mark-section "End Virtual Object Properties" *separation-width*)
    (unless *errors-only* (rp result))
    
    ;; return
    :done))

#| (index)

(apply #'make-virtual-relation-rule '(:EN "uncle" :FR "oncle") 
             (index-get (make-relation-id '(:EN "uncle" :FR "oncle")) :vrel :def))
? (reset)
T
? (index-clear)
T
? (let ((*compiler-pass* 1))
       (defconcept 
         (:name :en "Person" :fr "personne" :it "Persona")
         (:att (:en "name" :fr "nom") (:type :name)(:min 1))
         (:rel (:en "mother" :fr "mÃ¨re") (:to "person") (:unique))  
         (:rel (:en "father" :fr "pÃ¨re") (:to "person") (:unique))
         (:rel (:en "brother" :fr "frÃ¨re") (:to "person"))  (:rel (:en "country" :fr "pays")(:to "country"))
         (:doc :en "A Person is the generic class for human being. A person may be viewed as ~
             a citizen, employee, referent, etc."))
      (make-virtual-relation 
       '(:name :en "uncle" :fr "oncle")
       '(:class "person")
       '(:compose "mother" "brother")
       '(:to "person")
       '(:max 12)
       '(:doc :en "an uncle is a person who is the brother of the mother.")))
:DONE

? (let ((*compiler-pass* 2))
       (defconcept 
         (:name :en "Person" :fr "personne" :it "Persona")
         (:att (:en "name" :fr "nom") (:type :name)(:min 1))
         (:rel (:en "mother" :fr "mÃ¨re") (:to "person") (:unique))  
         (:rel (:en "father" :fr "pÃ¨re") (:to "person") (:unique))
         (:rel (:en "brother" :fr "frÃ¨re") (:to "person"))  (:rel (:en "country" :fr "pays")(:to "country"))
         (:doc :en "A Person is the generic class for human being. A person may be viewed as ~
             a citizen, employee, referent, etc."))
      (make-virtual-relation 
       '(:name :en "uncle" :fr "oncle")
       '(:class "person")
       '(:compose "mother" "brother")
       '(:to "person")
       '(:max 12)
       '(:doc :en "an uncle is a person who is the brother of the mother.")))
[...OWL output for class...]

? (index)
("Oncle" ((:IDX (:FR "hasUncle")) (:VREL (:ID "hasUncle")))) 
("Person" ((:IDX (:EN "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Persona" ((:IDX (:IT "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Personne" ((:IDX (:FR "Z-Person")) (:CLASS (:ID "Z-Person")))) 
("Uncle" ((:IDX (:EN "hasUncle")) (:VREL (:ID "hasUncle")))) 
("Z-Person" ((:CLASS (:NAME :EN "Person" :FR "personne" :IT "Persona")))) 
("hasUncle"
 ((:VREL
   (:DEF (:NAME (:EN "uncle" :FR "oncle")) (:CLASS "person")
    (:COMPOSE "mother" "brother") (:TO "person") (:MAX 12)
    (:DOC :EN "an uncle is a person who is the brother of the mother."))))) 
:END

? (progn (process-attributes)
         (process-relations))
[...OWL output for properties...]
  
? (index)
[... Index table contaning entries for properties...]
  
? (process-virtual-relations)

### an uncle is a person who is the brother of the mother.

[Role_Uncle:
   (?V1507 test4:hasUncle ?V1509)
<-
   (?V1507 test4:hasMother ?V1508)
   (?V1508 test4:hasBrother ?V1509)
 ]
<!-- ++++++++++++++++++++++++++ Virtual Object Properties +++++++++++++++++++++++++++ -->

<VirtualObjectProperty rdf:ID="hasUncle">
  <rdfs:label xml:lang="en">uncle</rdfs:label>
  <rdfs:label xml:lang="fr">oncle</rdfs:label>
  <rdfs:domain rdf:resource="#Z-Person"/>
  <rdfs:range rdf:resource="#Z-Person"/>
  <rdfs:comment xml:lang="en">an uncle is a person who is the brother of the mother.</rdfs:comment>
  <owl:inverseOf>
    <VirtualObjectProperty rdf:about="#isUncleOf"/>
  </owl:inverseOf>
</VirtualObjectProperty>
 
<owl:Class rdf:about="#Z-Person">
  <rdfs:subClassOf>
    <owl:Restriction>
      <owl:onProperty rdf:resource="#hasUncle"/>
      <owl:maxCardinality rdf:datatype="&xsd;nonNegativeInteger">12</owl:maxCardinality>
    </owl:Restriction>
  </rdfs:subClassOf>
</owl:Class>
 
<VirtualObjectProperty rdf:ID="isUncleOf">
  <rdfs:label xml:lang="en">uncle of</rdfs:label>
  <rdfs:label xml:lang="fr">oncle de</rdfs:label>
  <rdfs:domain rdf:resource="#Z-Person"/>
  <rdfs:range rdf:resource="#Z-Person"/>
  <owl:inverseOf>
    <VirtualObjectProperty rdf:about="#hasUncle"/>
  </owl:inverseOf>
</VirtualObjectProperty>
 
<!-- ++++++++++++++++++++++++ End Virtual Object Properties +++++++++++++++++++++++++ -->

:DONE

? (index-clear  "hasUncle" :vrel :def)
T
? (let ((*compiler-pass* 1))
      (make-virtual-relation 
       '(:name :en "uncle" :fr "oncle")
       '(:class "person")
       '(:compose (:or ("mother" "brother")("father" "brother")))
       '(:to "person")
       '(:max 12)
       '(:doc :en "an uncle is a person who is the brother of the mother or of the ~
                   father.")))
:DONE

? (process-virtual-relations)

### an uncle is a person who is the brother of the mother or of the father.

[Role_Uncle:
   (?V1039 test4:hasUncle ?V1041)
<-
   (?V1039 test4:hasMother ?V1040)
   (?V1040 test4:hasBrother ?V1041)
 ]

### an uncle is a person who is the brother of the mother or of the father.

[Role_Uncle:
   (?V1042 test4:hasUncle ?V1044)
<-
   (?V1042 test4:hasFather ?V1043)
   (?V1043 test4:hasBrother ?V1044)
 ]
[...OWL output...]
|#
;;;------------------------------------------------------- read-next-valid-line

(defun read-next-valid-line (ss)
  "skip block commented lines.
Arguments:
   ss: stream
   initial-line: first line (that was already read)
return:
   string or :eof"
  (let ((comment-flag 0) line)
    (loop
      (setq line (read-line ss nil :eof))
      (incf *line-number*)
      ;(format t "~&~s" line)
      ;; did we reach end of file? If so return
      (if (eql line :eof)(return-from read-next-valid-line :eof))
      ;; first trim line removing spaces and tabs
      (setq line (string-trim '(#\space #\tab) line))
      ;(format t "~&=> ~s" line)
      ;; otherwise check for comment sign
      (cond
       ;; when empty line, skip it
       ((equal "" line))
       ;; skip simple comment lines
       ((eql (char line 0) #\;))
       ;; if not inside a commented block and line is valid, return it
       ((and (< comment-flag 1)
             (not (search "#|" line :test #'string-equal)))
        (return-from read-next-valid-line line))
       ;; if line is beginning comment
       ;; signal comment and read next line
       ((search "#|" line :test #'string-equal)
        (incf comment-flag)
        ;(format t "~&------- ~S" comment-flag)
        )
       ;; if inside commented-block and line is end of comment, decrease counter
       ((and (> comment-flag 0)
             (search "|#" line :test #'string-equal))
        (decf comment-flag)
        ;(format t "~&------- ~S" comment-flag)
        )
       ;; otherwise, skip line
       ))))

;;;---------------------------------------------------------------------- reset

(defun reset ()
  "reset all structures preparing for compiling SOL into OWL.
    Arguments:
    none
    Return:
    t"
  (setq *compiler-pass* 1
        *concept-list* nil
        *attribute-list* ()
        *index-list* () ; JPB051009
        *relation-list* ()
        *virtual-attribute-list* ()
        *virtual-composed-attribute-list* ()
        *virtual-property-list* ()
        *virtual-relation-list* ()
        *individual-list* ()
        *rule-list* ()
        *super-class-list* ()
        *error-message-list* ()
        *index* (clrhash *index*)
        *left-margin* 0)
  t)

;;;------------------------------------------------------------------ same-name

#|
(defun same-name (first-name-spec second-name-spec)
  "checks whether two multi-lingual name specs share the same name. E.g.
    (:en \"sex\" :fr \"genre\") (:fr \"sexe; Genre\") share  \"genre\"
   comparisons are made on normalized strings.
Arguments:
    first-name-spec: 
    second-name-spec:
Return:
    nil or the list of merged specs"
  (let (result first-name-list second-name-list shared-names tag shared-name? names)
    (loop
      ;; when fist name spec is empty, get out of the loop
      (unless first-name-spec (return))
      ;; norm name-strings
      (setq first-name-spec (multilingual-name? first-name-spec)
            second-name-spec (multilingual-name? second-name-spec))
      ;; get language tag
      (setq tag (pop first-name-spec))
      ;; if second-name-spec has something in this language, then compare values
      (if (setq names (cadr (member tag second-name-spec)))
        (progn
          
          ;; by intersecting normed srings
          (setq first-name-list (mapcar #'make-index-string 
                                        (extract-names (car first-name-spec)))
                second-name-list (mapcar #'make-index-string 
                                         (extract-names names)))
          (setq shared-names 
                (intersection first-name-list second-name-list :test #'string-equal))
          (if shared-names (setq shared-name? t))
          ;; merge the names anyway
          (setq result
                (append 
                 (list tag 
                       (merge-names
                        (union first-name-list second-name-list 
                               :test #'string-equal)))
                 result)))
        ;; otherwise, record first name
        (setq result (append (list tag (car  first-name-spec)) result)))
      (pop first-name-spec) ; remove string part
      )
    ;; check intersection result, if so retruned the merged string
    (when shared-name? 
      ;; must add all language spec from the second-name-spec that were not recorded
      (loop
        (unless second-name-spec (return))
        ;; check if already recorded
        (unless (member (car second-name-spec) result)
          (setq result (append result (list (car second-name-spec)
                                            (cadr second-name-spec)))))
        (pop second-name-spec)
        (pop second-name-spec)
        )
      result)))
|#

#|
? (mln::mln-equal '(:en "night") '(:en "night"))
(:EN "Night")
? (mln::mln-equal '(:en "night") '(:en " NIGHT   "))
(:EN "Night")
? (mln::mln-equal '(:en "night") '(:en " NIGHT   "))
(:EN "Night")
? (mln::mln-equal '(:en "night") '(:en "nite  ; NIGHT   "))
(:EN "Nite; Night")
? (mln::mln-equal '(:en "night" :fr "nuit ") '(:en "nite  ; NIGHT   " :it "notte"))
(:FR "nuit " :EN "Nite; Night")
? (mln::mln-equal '(:en "night" :fr "nuit ") '(:en "nite  ; NIGHT   " :it "notte"))
(:FR "nuit " :EN "Nite; Night" :IT "notte")
? (mln::mln-equal '(:en "night" :fr "nuit ") '(:en "nite   " :it "notte"))
NIL
|#
;;;-------------------------------------------------------- set-rule-name-space

(defun set-rule-name-space (options)
  "extract the name space to be used in the rule file from the ontology options."
  (declare (special *rule-prefix* *rule-prefix-ref*)) 
  (let (self ref)
    (setq self (cadr (member "self" (assoc :xmlns options) :test #'string-equal)))
    (setq self (string-trim '(#\space) self))
    (cond
     ((null self)
      (twarn "can't find self in defontology xmlns option, needed for rule file"))
     ;; if first char is & we have a reference
     ((char-equal #\& (char self 0))
      ;; get rid of leading & and trailing ;
      (setq self (subseq self 1 (1- (length self))))
      ;; get the reference in the DOCTYPE option
      (setq ref (cadr (member self (assoc :doctype options) :test #'string-equal)))
      (if (null ref)
        (twarn "can't find self reference in defontology doctype option ~
                (needed for rule file).")
        (setq *rule-prefix* self 
              *rule-prefix-ref* ref)))
     ;; if not reference qe assume that it contaisn the ref
     (t 
      (setq *rule-prefix* self
            *rule-prefix-ref*
            (cadr (member self (assoc :xmlns options) :test #'string-equal)))))))

#|
? (set-rule-name-space
   '((:version 1.0)
     (:doctype "owl" "http://www.w3.org/2002/07/owl#"
               "test4" "http://www.terregov.eupn.net/ontology/2005/10/test.owl/test4#"
               "xsd"       "http://www.w3.org/2001/XMLSchema#" )
     (:xmlns "rdf" "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
             "rdfs" "http://www.w3.org/2000/01/rdf-schema#"
             "owl" "http://www.w3.org/2002/07/owl#"
             "xsd" "&xsd;"
             "self" "&test4;"
             "test" "&test4;")
     (:base "http://www.terregov.eupn.net/ontology/2005/10/test.owl/test4")
     (:imports "http://www.w3.org/2000/01/rdf-schema"
               "http://www.w3.org/1999/02/22-rdf-syntax-ns"
               "http://www.w3.org/2002/07/owl")))
"http://www.terregov.eupn.net/ontology/2005/10/test.owl/test4#"
|#
;;;------------------------------------------------------------------ trim-file

(defun trim-file (input-file output-file)
  "Cleans a file of the leading UTF-8 mark (3 bytes?) by copying the file into
    new one.
Arguments:
   input-file: file to clean
   output-file: cleaned file
Return:
   t"
  (let (line)
    (with-open-file (in input-file :direction :input)
      (with-open-file (out output-file :direction :output :if-exists :supersede
			   :if-does-not-exist :create)        
	;; copy the rest of the file
	(loop
	  (setq line (read-line in nil :eof))
	  ;; if :eof then end of file
	  (when (eql line :eof) 
	    (close out)
	    (return-from trim-file t))
          ;; if the line starts with defpackage or in-package, skip the line
          (unless (or (search "(defpackage" line :test #'string-equal)
                      (search "(in-package" line :test #'string-equal))
            ;; otherwise copy into new file
            (format out "~&~A" line))
#|
          #+MCL
          ;; remove PC formatted line-feed
          (format out "~&~A" 
                  (if (char-equal (char line 0) #\Linefeed)
                    (subseq line 1)
                    line))
|#
          )
	nil))))

#|
(ext:letf 
  ((custom:*default-file-encoding* 
  (ext:make-encoding :charset charset:utf-8 :line-terminator :dos)))
  (trim-file "c:/sol/ontologies/test0.sol" "c:/sol/ontologies/test0.tmp"))
|#
;;;=================================== INDEX ======================================
#|
The *index* table is a hash-table with complex entries depending on the kind of
information kept at compilation time.
The index will contain entries like
- index onto classes
"Ville" ((:CLASS (:ID "Z-City")))
"Prénom" ((:ATT (:ID "Z-hasFirstName")))
- desciption of classes
"Z-City" ((:PROP (:ATT "Z-hasName" "Z-hasPeople")(:REL "Z-hasRegion")))
- description of properties
"hasName" ((:ATT (:TYPE :STRING)(:DOMAIN ...)("Z-City" (:max 2)))
           "hasRegion" ((:REL (:RANGE "Z-Region")("Z-Region" (:unique)))))
-description of individuals
"albert" ((:IND (:id "albert")(:class "Z-Person")))

A key is a string usually produces by make-index-string from any string.
? (make-index-string "le jour  le PLUS long   ")
"LeJourLePlusLong"
Associated with the key is an a-list whose properties are tags 
(:ATT :REL :CLASS or :INST) 
describing properties attached to the entry when it designates an 
attribute, a relation, a concept or an individual.

Each value attached to a tag is itself an a-list whose properties are different
for different tags. For example 

In the case of :ATT properties can be
:id giving the id of the attribute, e.g. "hasFirstName"
:class-id giving the domains of the attribute, e.g. ("Z-Person" "Z-Animal")
:type giving the SOL type of the corresponding values, e.g. :integer
:one-of giving the list of allowed values, e.g. (:one-of 1 2 3)
:card giving the cardinality restrictions, e.g. (:card 1 1)

In the case of :REL
:id giving the id of the relation, e.g. "hasBrother"
:class-id giving the domain of the relation e.g. ("Z-Person" "Z-Animal")
:to giving the range, e.g. ("Z-Person" "Z-Animal")
<note that is is an error to have multiple domains AND ranges>
Constraints local to a particular class
<class-id> e.g. "Z-Person" whose value is an a-list
:card e.g. (:card 1 2)
:one-of e.g. <is :one-of local?>

In the case of :CLASS
:id giving the id of the class, e.g. "Z-City"
:is-a giving the entry of the superclass

In the case of :INST
:id giving the id of the individual

The table entries will be used in particular for checking the existence of various
objects or for checking constraints for properties of individuals.

Several functions are used to handle the *index* hash table:

INDEX-ADD to add something
INDEX-CLEAR to remove something
INDEX-GET to get some value
INDEX to see what is in the table

Typically something is added as follows:
(index-add "Person" :class :id "Z-Person") ; record the id for the class

|#
;;;---------------------------------------------------------------------- index

(defun index ()
  "dumps the content of the *index* hash-table in alphabetic order (debug function)."
  (let (index-list)
    (maphash #'(lambda (xx yy) (if xx (push (list xx yy) index-list)))
             *index*)
    (mapcar #'pprint
            (setq index-list (sort index-list #'string< :key #'car)))
    :end))

;;;------------------------------------------------------------------ index-add

(defun index-add (key tag prop value)
  "add an entry into the *index* table. Replaces previous entry at the prop level.
Arguments:
   key: the entry string
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value associated with the entry
Return:
   value"
  (let ((entry (gethash key *index*))
        tag-value prop-value old-pair)
    ;; if no entry, then create one
    (unless entry 
      (setf (gethash key *index*) `((,tag . ((,prop ,value)))))
      (return-from index-add value))
    ;; check if value has tag
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    ;; if not set up one
    (unless tag-value
      (setf (gethash key *index*) (cons `(,tag . ((,prop ,value))) entry))
      (return-from index-add value))
    ;; check for prop
    (setq prop-value (cdr (assoc prop tag-value :test #'equal)))
    ;; if no, then set up one
    (unless prop-value
      (setq tag-value (cons `(,prop ,value) tag-value))
      ;; replace old tag-value
      (setq entry (cons (cons tag tag-value)
                        (remove (assoc tag entry :test #'equal)
                                entry :test #'equal)))
      (setf (gethash key *index*) entry)
      (return-from index-add value))
    ;; if prop present get previous value, add new one and replace
    (setq old-pair (assoc prop tag-value :test  #'equal)
          ;          value (append (cdr old-pair) (list value)))
          value (if (member value (cdr old-pair) :test #'equal)
                  (cdr old-pair)
                  (append (cdr old-pair) (list value))))
    ;; replace old value with new value
    (setq tag-value (cons `(,prop . ,value)
                          (remove old-pair tag-value :test #'equal)))
    ;; replace old tag-value
    (setq entry (cons (cons tag tag-value) (remove (assoc tag entry :test #'equal)
                                                   entry :test #'equal)))
    ;; replace entry
    (setf (gethash key *index*) entry)
    (return-from index-add value)))

#|
(index-clear)
T
? (index)
:END
? (index-add "Ville" :CLASS :id "Z-City")
"Z-City"
? (index)

("Ville" ((:CLASS (:ID "Z-City"))))
:END
? (index-add "Ville" :class :one-of "Caen")
"Caen"
? (index)

("Ville" ((:CLASS (:ONE-OF "Caen") (:ID "Z-City")))) 
:END
? (index-add "Ville" :class :one-of "Nice")
("Caen" "Nice")
? (index-add "Ville" :class :one-of "Toulouse")
("Caen" "Nice" "Toulouse")
? (index)

("Ville" ((:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END
? (index-add "Ville" :inst :att "hasName")
"hasName"
? (index)

("Ville"
 ((:INST (:ATT "hasName"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END
? (index-add "Ville" :inst :att "hasColor")
("hasName" "hasColor")
? (index)

("Ville"
 ((:INST (:ATT "hasName" "hasColor"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END
? (index-add "Ville" :inst :rel "hasNeighbor")
"hasNeighbor"
? (index)

("Ville"
 ((:INST (:REL "hasNeighbor") (:ATT "hasName" "hasColor"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END
|#
;;;*----------------------------------------------------------- index-add-merge

(defun index-add-merge (key tag prop inv-id pred-id)
  "like index-add but pred-id must be merge with the previous predecessors"
  (let (item-list flag item result)
    ;; get the list of inverses (("isSisterOf "z-18" "z-19")("isCousinOf" "z-16"))
    (setq item-list (index-get key tag prop))
    (loop
      ;; at the end of the list we did not find inv-id add new item
      (unless item-list
        (if flag
            (return (reverse result))
            (return (reverse (push (list inv-id pred-id) result)))))
      
      (setq item (pop item-list))
      ;; if same inverse property add predecessor
      (if (string-equal inv-id (car item))
          (progn
            (push (cons (car item)
                        (append (cdr item) (list pred-id)))
                result)
            (setq flag t))
          ;; otherwise skip this item
          (push item result)))
    ;; replace the entry
    (%index-set key tag prop (reverse result))))

#|
? (index-clear)
? (index-add-merge "z-8" :INST :inv "isCousinOf" "z-20")
(("isCousinOf" "z-20"))
? (index-add-merge "z-8" :INST :inv "isCousinOf" "z-19")
(("isCousinOf" "z-20" "z-19"))
? (index-add-merge "z-8" :INST :inv "isDaughterOf" "z-22")
(("isCousinOf" "z-20" "z-19") ("isDaughterOf" "z-22"))
? (index-add-merge "z-8" :INST :inv "isCousinOf" "z-16")
(("isCousinOf" "z-20" "z-19" "z-16") ("isDaughterOf" "z-22"))

? (index)
("z-8" ((:INST (:INV ("isCousinOf" "z-20" "z-19" "z-16") ("isDaughterOf" "z-22")))))
|#     
;;;---------------------------------------------------------------- index-clear

(defun index-clear (&optional key tag prop)
  "removes one of the levels of the index depending on the arguments. If none, ~
      then clears the whole table
Arguments:
   key (opt): key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
Return:
   t"
  (let ((entry (gethash key *index*))
        tag-value)
    ;; if no args, clear table
    (unless key
      (clrhash *index*)
      (return-from index-clear t))
    ;; if key but no tag, then remove entry
    (unless tag 
      (remhash key *index*)
      (return-from index-clear t))
    ;; if no prop, we want to remove tag option
    (setq entry (gethash key *index*))
    (unless prop
      (setf (gethash key *index*) 
            (remove (assoc tag entry :test #'equal) entry :test #'equal))
      (return-from index-clear t))
    ;; check for value, if no value we want to remove prop option
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    
    (setq tag-value (remove (assoc prop tag-value :test #'equal)
                            tag-value :test #'equal))
    ;; replace old tag-value
    (setq entry 
          (if tag-value
            (cons (cons tag tag-value) (remove (assoc tag entry :test #'equal)
                                               entry :test #'equal))
            (remove (assoc tag entry :test #'equal)
                    entry :test #'equal)))
    (setf (gethash key *index*) entry)
    (return-from index-clear t)))

#|
? (index-clear)
T
? (index)
NIL
? (%index-set "hamlet" nil nil '((:class (:id "Z-Village")(:one-of "Margny"))))
((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))
? (index)
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (index-clear "Ville")
T
? (index)
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (index-clear "hamlet" :class :one-of)
T
? (index)
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (%index-set "Ville" :class :id '("Z-City"))
("ZCity")
? (%index-set "Ville" :class :id '("Z-City"))
("ZCity")
? (index)
("Ville" ((:CLASS (:ID "Z-City") (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (index-clear "Ville" :class )
T
? (index)
("Ville" NIL) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
|#
;;;--------------------------------------------------------------- index-remove

(defun index-remove (key tag prop value)
  "remove a value associated with prop. If not there, do nothing.
Arguments:
   key (opt): key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value to remove
Return:
   t"
  (let ((old-value (index-get key tag prop))
        new-value)
    (setq new-value (remove value old-value :test #'equal))
    (if new-value
      (%index-set key tag prop new-value)
      (index-clear key tag prop))))

#|
? (index-clear)
T
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
:END
? (index-add "Ville" :class :one-of "Paris")
("Caen" "Nice" "Paris")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice" "Paris")))) 
:END
? (index-add "Ville" :class :id "Z-City")
"Z-City"
? (index)
("Ville" ((:CLASS (:ID "Z-City") (:ONE-OF "Caen" "Nice" "Paris")))) 
:END
? 
INDEX-REMOVE
? (index-remove "Ville" :class :one-of "Nice")
("Caen" "Paris")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Paris") (:ID "Z-City")))) 
:END
? (index-remove "Ville" :class :id "Z-City")
T
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Paris")))) 
NIL
|#
;;;----------------------------------------------------------------- %index-set

(defun %index-set (key tag prop value)
  "set a value associated with key tag or prop. Must have the proper format.
Arguments:
   key: key (error if not present)
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value to set (must have the proper format)
Return:
   value"
  (let (entry tag-value prop-value)
    (unless key (error "no key arg"))
    ;; if no tag replaces whole entry
    (unless tag
      (setf (gethash key *index*) value)
      (return-from %index-set value))
    ;; get old entry
    (setq entry (gethash key *index*))
    ;; if prop is nil, then replaces the value associated with tag
    (unless prop
      (setf (gethash key *index*) 
            (cons (cons tag value)
                  (remove (assoc tag entry :test #'equal) entry :test #'equal)))
      (return-from %index-set value))
    ;; get old tag-value
    (setq tag-value (assoc tag entry :test #'equal))
    ;; get prop-value part
    (setq prop-value (assoc prop (cdr tag-value) :test #'equal))
    ;; if prop there replace old-value with new value
    ;; e.g. entry ((:CLASS (:id "ZCity")(:one-of ...)) (:INST ...))
    ;;      tag-value (:CLASS (:id "ZCity")(:one-of ...))
    ;;      prop-value (:id "ZCity")
    ;; (%index-set "Ville" :CLASS :id '("ZTown"))
    (setq tag-value 
          (list* tag 
                 (cons prop value) 
                 (remove prop-value (cdr tag-value) :test #'equal)))
    
    (setf (gethash key *index*) 
          (cons tag-value
                (remove (assoc tag entry :test #'equal) entry :test #'equal)))
    value))

#|
? (index-clear)
T
? (%index-set "Ville" nil nil '((:CLASS (id "Z-City"))))
((:CLASS (ID "Z-City")))
? (index)
("Ville" ((:CLASS (ID "Z-City")))) 
NIL
? (%index-set "Ville" nil nil '((:CLASS (id "Z-Town"))))
((:CLASS (ID "Z-Town")))
? (index)
("Ville" ((:CLASS (ID "Z-Town")))) 
NIL
? (%index-set "Ville" :class nil '((:id "Z-Town")))
((:ID "Z-Town"))
? (index)
("Ville" ((:CLASS (:ID "Z-Town")))) 
NIL
? (%index-set "Ville" :class nil '((:id "Z-Town")(:one-of "Bordeaux")))
((:ID "Z-Town") (:ONE-OF "Bordeaux"))
? (index)
("Ville" ((:CLASS (:ID "Z-Town") (:ONE-OF "Bordeaux")))) 
NIL
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice") (:ID "Z-Town")))) 
NIL
|#
;;;------------------------------------------------------------------ index-get

(defun index-get (key &optional tag prop)
  "get one of the levels of the index depending on the arguments. If none, ~
      then clears the whole table
Arguments:
   key: key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
Return:
   corresponding value"
  (let ((entry (gethash key *index*)) tag-value)
    (unless tag (return-from index-get entry))
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    (unless prop (return-from index-get tag-value))
    (cdr (assoc prop tag-value :test #'equal))))

#|
? (index)

("Ville" ((:CLASS (:ID "Z-City")))) 
("Hamlet" ((:CLASS (:ID "Z-Hamlet") (:ONE-OF "margny")))) 
NIL
? (index-get "Hamlet" :class :id)
("Z-Hamlet")

|#

;(format t "~&;*** Sol2owl loaded ***")


:EOF
