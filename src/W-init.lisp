;;;==========================================================================
;;;19/08/21
;;;		
;;;		     M O S S - W - I N I T - (File W-init.LISP)
;;;	 
;;;==========================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;;==========================================================================
;;; This file contains the code for showing initial windows when loading MOSS
;;; alone independently from OMAS.

#|
2019
 0821 creation
|#

(in-package :nodgui-user)

;;;---------------------------------------------------- MAKE-MOSS-INIT-WINDOW

(defun make-moss-init-window ()
  "Welcome window for starting MOSS in stand alone mode"
  (declare (special *tk*))
   (with-nodgui ()

     ;; get rid of the wish window by hiding it
     (format-wish "wm iconify .")

     ;; create a frame to contain widgets
     (let* 
         ((tw1 (make-instance 'toplevel))
          (f1 (make-instance 'frame :master tw1))
          (l1 (make-instance 'label :master f1
                :text "MOSS v10.1 alpha"))
          (l2 (make-instance 'label :master f1
                :text "Copyright Barthès@Heudiasyc-CNRS-UTC"))
          (b1 (make-instance 'button :master f1 :text "START" 
                :command #'(lambda ()(moss-start-on-click tw1))))
          )

       (set-geometry-xy tw1 400 400)
       (wm-title tw1 "MOSS")

       ;; use the grid to post text and button
       (grid f1 0 0)
       (grid l1 0 0 :padx 100 :pady "40 0")
       (grid l2 1 0 :padx 100 :pady "5 0")
       (grid b1 2 0 :pady "20 40")
       )))

; (make-moss-init-window)



;;; Check to see if buttons are operational. Answer: yes
(defun make-omas-init-window ()
  "Welcome window for starting OMAS"
  (declare (special *tk*))
  (with-nodgui ()
    
    ;; get rid of the wish window by hiding it
    (format-wish "wm iconify .")
    
    ;; create a frame to contain widgets
    (let* 
        ((tw1 (make-instance 'toplevel))
         (f1 (make-instance 'frame :master tw1))
         ;;=== name of the SITE, e.g. "UTC"
         (lrt (make-instance 'label :master f1 :text "LOCAL REFERENCE"))
         (lr (make-instance 'entry :master f1 :text "<e.g. UTC>" :width 20))
         ;;=== name of the application or the coterie
         (apt (make-instance 'label :master f1 :text "APPLI/COTERIE"))
         (ap (make-instance 'entry :master f1 :text " " :width 20))
         ;;=== folder (supposedly the folder in which we find the application files)
         (fdt (make-instance 'label :master f1 :text "FOLDER"))
         (fd (make-instance 'entry :master f1 :text "*** option not available ***"
               :width 20))
         ;;=== IP address
         (ipat (make-instance 'label :master f1 :text "IP ADDRESS"))
         (ipa (make-instance 'entry :master f1 :text "<omas broadcast address> "
               :width 20))
         ;;=== port
         (prt (make-instance 'label :master f1 :text "FOLDER"))
         (pr (make-instance 'entry :master f1 :text "50000"
               :width 20))
         
         #|
         (l2 (make-instance 'label :master f1
         :text "Copyright Barthès@Heudiasyc-CNRS-UTC"))
         |#
         (b1 (make-instance 'button :master f1 :text "HIDE" 
               :command #'(lambda ()(hide-on-click tw1))))
         (b2 (make-instance 'button :master f1 :text "LOAD" 
               :command #'(lambda ()(moss-start-on-click tw1))))
         )
      
      (set-geometry-xy tw1 400 400)
      (wm-title tw1 "OMAS-CCL v14.0")
      
      ;; use the grid to post text and button
      (grid f1 0 0)
      (grid lrt 0 0 :padx "10 10" :pady "10 0")
      (grid lr 0 1 :padx "0 10" :pady "10 0")

      (grid apt 1 0 :padx "10 10")
      (grid ap 1 1 :padx "0 10")

      (grid fdt 2 0 :padx "10 10")
      (grid fd 2 1 :padx "0 10")

      (grid ipat 3 0 :padx "10 10")
      (grid ipa 3 1 :padx "0 10")

      (grid prt 4 0 :padx "10 10")
      (grid pr 4 1 :padx "0 10")

      (grid b1 5 0 :pady "5 10")
      (grid b2 5 1 :pady "5 10") 
      )))


;;;------------------------------------------------------- MOSS-START-ON-CLICK
;(format-wish "destroy .") don't use: breaks wish channel

(defun moss-start-on-click (win)
  "calls MOSS and close the window"
  ;(print "To be done...")
  (destroy win)
  ;(print "calling make-import-window")
  (make-moss-import-window)
  ;; closes the current window
  ;; BUG?: cannot create other windows when *tk* is iconified!
  ;(iconify win) ; could do that...
  t)

:EOF