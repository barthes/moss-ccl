;;;=================================================================================
;;;19/08/06
;;;		
;;;		G L O B A L S - (File globals.lisp)
;;;	
;;;=================================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
;;;=================================================================================

;;; File defining MOSS globals and some debug/service functions

#|
History
2019
 0806 Creation
|#

(in-package :moss)

;;; tell MOSS that we have a compiler (not always the case, e.g. with ACL)
(pushnew :compiler *features*)

;;; record loading path at loading time
(defParameter *moss-directory-pathname*
  (make-pathname :name nil :type nil :defaults *load-pathname*))

;;;==============================================================================
;;;                             Version Number
;;;==============================================================================
;;; MOSS version is maintained independently of OMAS versions. It is supposed to
;;; evolve less frequently than the OMAS functions

(defparameter *moss-version-number* "10.1")

;;;==============================================================================
;;;                    Applications / Sample Applications
;;;==============================================================================
;;; applications should be stored into the "/MOSS projects/applications" directory
;;; at the root of the user home directory (e.g. /Users/barthes/), or into the
;;; "/MOSS projects/sample applications/" directory (for distribution as demos)

(defparameter *application-directory* "~/MOSS projects/")

(defParameter *application-name* "MOSS-TEST" "default file name")

;;; ------------------------------------------------------------------------------
;;; Kludge: define EDITOR class to keep compiler happy when calling save-old-value
;;; or save-new-id

(defClass editor (dialog)
  ())

;;;========================== Global Variables ==============================
;;; first declare global variables that are used by MOSS
;;; global variables should be part of the MOSS system object

;;; When using a multi-process environment with different MOSS representations in
;;; the different processes, one has to specify which global variables are specific
;;; to a process and which are simply MOSS global variables that will be shared by 
;;; all processes.

;;;====================== Process Global Variables ==========================
;;; the following global variables must be redefined in each process, which
;;; allows to confine them to a process

;;; ------------------------------------------------------------------------------
;;; Globals for storing instance variables, entry points, application special symbols
;;; ------------------------------------------------------------------------------  

(defparameter *st-size* 2000 "initial size of local symbol table")
;; actually not necessary in the MOSS package since instances are symbols
(defparameter *ST* (make-hash-table :size *st-size* :test 'equal)
  "empty if no application")

;;; used by MOSS

(defParameter *context* 0 "number of the current ontology version")
(defParameter *language* :EN "default language is English")
(defParameter *version-graph* '((0)) "configuration lattice")

;;; used to set the current package when using windows. Set up by defontology.
(defParameter *application-package* nil "ontology package")

;;; ------------------------------------------------------------------------------
;;; Globals for debugging
;;; ------------------------------------------------------------------------------ 

(defparameter *debug-tags* nil "list of keywords controlling trace")
(defparameter *debug-tag-list* nil "list of pairs (tag reason)")
(defparameter *debug-level* 0 "deprecated")


;;; not really usefull since files are loaded by asdf
;;; used by mref to locate a symbol in the files (sort of grep)

(defParameter *moss-file-names*
  (eval-when (:load-toplevel :execute)
    `("globals"
      "mln"           ; MLN package independent (uses *language-tags*)
      "texts-UTF8"
      "macros"
      "service"       ; requires engine for add-values
      "utils"
      "boot"
      "engine"
      "def"           ; requires engine
      "persistency"
      "kernel"
      "query"
      "dialog-classes"
      "dialog-engine"
      #-OMAS "dialog"          ; MOSS help dialog (loaded after patches)
      "W-init"
      "W-import"
      "W-window"
      "W-browser"
      "W-editor"
      "W-overview"
      "paths"
      "time"
      ;"web"
      ;"patches"
      "export"
      "online-doc"      ; aditional info for help dialog (id.)
      ;; SOL files
      "W-sol-control"
      "sol2html"
      "sol2owl"
      "sol2rules"
      ))
  "MOSS files to be loaded")

;(format t "~%*moss-directory-pathname*: ~S" *moss-directory-pathname*)

;;; ------------------------------------------------------------------------------
;;; build the list of file access paths from the file names and directory path

#| ;; CCL loads a file from its cache, which is not the directory of the sources
(defParameter *moss-files*
  (mapcar #'(lambda (xx)
              (make-pathname
               :defaults *moss-directory-pathname*
               :name xx
               :type "lisp"))
          *moss-file-names*)
  "MOSS file pathnames")
|#

;; temporary brute force assignment
(defParameter *moss-files*
  (mapcar #'(lambda (xx)
              (concatenate 'string 
                           "/Users/barthes/quicklisp/local-projects/MOSS-20180817/" 
                           xx ".lisp")
               )
          *moss-file-names*)
  "MOSS file pathnames")

;;; ------------------------------------------------------------------------------
;;; Globals for moss engine
;;; ------------------------------------------------------------------------------ 

(defParameter *sender* nil "sender of MOSS message")
(defVar *answer* "answer returned by a MOSS method")
(defParameter *self* nil "id of object that received the MOSS message")
(defParameter *method-name* "name of the method being executed")

;;; used while booting

(defParameter *boot-mode* t "signals that we are booting MOSS")

;;; used for modifying objects

(defvar *editing-box* () "pointer to editing box outside EDITORS")

;;;===================== Shared MOSS Global Variables =======================

(defParameter *export-entry-symbols* t "MOSS system exports entry symbols")

(defParameter *debug* nil "debug flag triggering the vformat macro")
(defParameter *delimiters* '(#\space #\, #\' #\. #\; #\: #\! #\?) 
  "characters delimiting a word")
(defParameter *traced-agent* nil "omas agent being traced")
;; what does this thing does in the MOSS environment?
(defVar *system-entities* () "record the list of MOSS entities (classes)")
(defParameter *warning-for-own-methods* nil "flag for warning messages")
(defVar *verbose* nil "trace error messages in warnings")

;;; ------------------------------------------------------------------------------
;;; Globals for defining concepts, or properties
;;; ------------------------------------------------------------------------------ 

(defparameter *attribute-options*
  '(:class-ref :concept :context :default :doc :entry :min :max :type :not-type :value 
               :unique :one-of :forall :exists :not-in :same :different 
               :between :outside :id) ; adding :id JPB1607
  "legal options for defattribute")

(defparameter *concept-options*
  '(:is-a :att :rel :doc :do-not-save :if-exists :id :rdx)
  "legal options for defconcept")

(defparameter *relation-options*
  '(:from :to :min :max :unique :one-of :exists :forall :default
         :id :inv :var :class-id :class :suc-id :id) ; adding :id JPB1607
  "legal options for defrelation")

;;; restriction operators are used to restrict the value of a property. They
;;; work as if-added demons.

(defVar *allowed-restriction-operators*
  '(:type       ; values should be of that type
    :not-type   ; no value should be of that type
    :unique     ; only this value is allowed
    :one-of     ; the value should be a member of this list
    :exists     ; among the possible values one must be of that type
    :forall     ; all values should be of that type
    :not        ; the value should not be one of this list
    :value      ; only this value is allowed
    :between    ; the numerical value should be in that range
    :outside    ; the numerical value should be outside that range
    :filter     ; user defined filter (single arg function)
    :same       ; all values should be the same
    :different  ; all values should be different
    
    :min        ; min number of values
    :max        ; max number of values
    :unique     ; only one value
    )
  "allowed operators used to restrict the value of a property")

(defparameter *attribute-restrictions*
  '($VALR $TPRT $NTPR $ONEOF $VRT $OPR $OPRALL $MAXT $MINT)
  "list of internal restrictions ids for attributes")

;; max and min should be at the end of the list in case values are repaired
(defparameter *relation-restrictions*
  '($SUC $NSUC $ONEOF $MAXT $MINT)
  "list of internal restriction ids for relations")

;;; ------------------------------------------------------------------------------
;;; Globals for loading ontology text files
;;; ------------------------------------------------------------------------------
;;; used when loading a text file containing an ontology with forward references

(defParameter *allow-forward-references* nil "for relations to an undefined object")
(defParameter *allow-forward-instance-references* nil "for relatiosns to an undefined instance")

(defParameter *deferred-actions* nil "list of actions deferred until the end of the file")
(defParameter *deferred-instance-creations*  () "list of instance creation actions")
(defParameter *deferred-defaults* () "list of default creation actions")
(defParameter *deferred-mode* :class "used in %make-instance")
(defParameter *deferred-instances* () "used in %make-instance")
(defParameter *deferred-instance-links* () "used for instances")
(defParameter *deferred-instance-methods* () "used for instances own methods")
(defParameter *deferred-links* () "to record links to be done after load")
(defParameter *deferred-methods* () "to record methods to be created after classes")

;;; ------------------------------------------------------------------------------
;;; Globals for the message engine
;;; ------------------------------------------------------------------------------

(defVar *left-margin* 0)
(defVar *moss-trace-level* 0 "Column number for starting the trace print out.")
(defVar *pretty-trace* nil)
(defVar *string-hyphen* "-")
(defVar *moss-system*)
(defVar *ontology* nil "synonym of *moss-system* (more user friendly)")
(defvar *ontology-text-file* nil "records the initial ontology file")

(defVar *moss-engine-loaded* nil "indicates that the send mechanism is loaded")
(defVar *cache-methods* nil "Flag that allows to cache methods onto p-list of ~
                             classes and objects. Turned off for debugging purposes")
(defVar *lexicographic-inheritance* t "Disable fancy inheritance scheme in favor ~
                                       of a standard lexicographic scheme")
(defVar *user* 'Anonymous "The identity of the user of the system.")
(defVar *trace-flag* () "Flag for toggling trace of messages on and off.")
(defvar *trace-level* 0 "value of indentation for tracing")
(defVar *trace-message* () "Flag for tracing all messages to all objects.")

;;; ------------------------------------------------------------------------------
;;; Globals for I/O channels
;;; ------------------------------------------------------------------------------ 

;;; moss-output indicates the window that will display the output. We assume that
;;; each window has a display-text method and that there is a single pane in
;;; each window for displaying the output. Otherwise, the display-text method
;;; would have to make the choice as where to display the text

(defParameter *moss-output* t "output channel: t or MOSS window")
(defParameter *moss-input* t "input channel: t or MOSS window")

;;; used when building agent MOSS environment
(defParameter *objects-to-be-saved* nil)

;;; ------------------------------------------------------------------------------
;;; Globals for the query file
;;; ------------------------------------------------------------------------------

(defParameter *abort-qery* nil "It true access is aborted")

(defVar *query-trace-flag* nil "Flag used to trace details of the query process~
                                for debugging purposes")

(defVar *explain-flag* t "flag used to trace the behavior of the query system ~
                          - used for explaining purposes.")
#|
(defVar *grundy-level* 0 "Distance from the goal class within a query branch. ~
                                                    Used to indent explanations.")
|#
(defParameter *input* t "default moss channel")
(defParameter *output* t "default moss channel")
(defVar *query-allow-sub-classes* t "Flag allowing to include sub-classes")

(if  *query-allow-sub-classes*
  (format *standard-output* 
          "~%;*** Instances of sub-classes are also included, this can be changed
~&;     by setting the global variable *query-allow-sub-classes* to nil ***")
  (format *standard-output* 
          "~%;*** Instances of sub-classes are not included, this can be changed
~&;     by setting the global variable *query-allow-sub-classes* to t ***"))

(defParameter *filters* () "alist to cache the filters avoiding recomputation")

(defParameter *operators*
  '(:BETWEEN :OUTSIDE :IS :IS-NOT :EQUAL :NOT-EQUAL 
             :IN :ALL-IN :EST :N-EST-PAS :PP :PG :PPE :PGE
             :CARD< :CARD<= :CARD= :CARD>= :CARD>))

(defparameter *cardinality-operators*
  '(:CARD< :CARD<= :CARD= :CARD>= :CARD>))

(defParameter *numeric-operators*
  '(;; operators requiring numerical values
    :LESS-THAN :LT :LESS-THAN-OR-EQUAL :LTE
    :GREATER-THAN :GT :GREATER-THAN-OR-EQUAL :GTE
    :EQUAL :NOT-EQUAL < <= > >= = <>
    ;; range operators, unary: take a list representing a range
    :BETWEEN :OUTSIDE 
    ;; structural operators related to cardinality
    :CARD< :CARD<= :CARD= :CARD<> :CARD>= :CARD>))

(defParameter *legal-attribute-operators*
  ;; operators are in English and are keywords
  (append *numeric-operators*
          '(:IS :IS-NOT :IN :ALL-IN :NOT-IN)))

;;;(defParameter *legal-attribute-operators*
;;;  ;; operators are in English and are keywords
;;;  '(:LESS-THAN :LT :LESS-THAN-OR-EQUAL :LTE
;;;    :GREATER-THAN :GT :GREATER-THAN-OR-EQUAL :GTE
;;;    :EQUAL :NOT-EQUAL :IS :IS-NOT
;;;    ;; range operators
;;;    :BETWEEN :OUTSIDE 
;;;    :IN :NOT-IN :ALL-IN :NOT-ALL-IN
;;;    ;; structural operators related to cardinality
;;;    :CARD< :CARD<= :CARD= :CARD<> :CARD>= :CARD>))

(defParameter *legal-cardinality-operators*
  '(< <= = <> > >= :between :outside))

(defParameter *assignment-operators* '(:is = :equal))

(defVar *query-handler* () "Current instance of the query handler")
(defVar *qh* () "Abbreviated name of the current instance of the query handler")

(defVar *query-include-kernel-objects* nil 
  "Flag allowing to include kernel objects as an answer to the query, e.g. ~
   meta-classes.")

;;; ------------------------------------------------------------------------------
;;; Globals for dialogs
;;; ------------------------------------------------------------------------------

;;; globals for dialogs/conversations

(defparameter *legal-tag-names* '("English" "Français") "used in moss-import-window")
(defparameter *language-names-tags* 
  '(("English" . :EN) ("Français" . :FR) ("Italiano" . :IT)
    ("Español" . :SP) ("Deutsch" . :DE) ("Português" . :PT)
    ("日本語" . :JP) ("中文" . :ZH) ("unknown" . :unknown)))                            
(defparameter *current-language* "English")
;(defparameter *language-tags*  defined in :mln and exported to MOSS

(defParameter *conversation* nil "current-conversation; not used for OMAS")
(defParameter *moss-conversation* nil "current-conversation; not used for OMAS")
(defParameter *dialog-verbose* nil "trace of dialog flag")
(defParameter *transition-verbose* t "trace dialog transitions flag")
(defparameter *MOSS-threshold* .4 "threshold for selecting a task")

(defVar _q-abort () "abort state in main dialog")

;; we use a string for marker to avoid package problems with symbol-names
;; we could have used a keyword
(defParameter *pattern-marker* "?*" "segment pattern marker")
(defParameter *performatives* '(:request :command :assert :answer))

(defParameter *abort-commands* '(":quit" "abort" ":exit" ":reset" ":cancel")
  "when typed by the user triggers a transition to the _q-abort state.")
(defParameter *yes-answers* '("yes" "Y" "oui" "Ja" "Jawohl"))
(defParameter *no-answers* '("no" "N" "nein" "non" "never" "nimmer" "jamais"))
(defParameter *why-patterns* 
  '(("why" *) 
    )
  "patterns to recognize a why question")
(defParameter *request-patterns*
  '(("what" "is" *)
    ("what" "are" *)
    ("what" *)
    ("when" *)
    ("where" *)
    ("why" *)
    ("who" *)
    ("whom" *)
    ("whose" *)
    ("how" "much" *)
    ("how" *)
    ("is" *)
    ("are" *)
    ("were" *)
    ("was" *)
    ("have" *)
    ("had" *)
    ("do" *)
    ("did" *)
    ("can" *)
    ("could" *)
    ("may" *)
    ("might" *)
    (* "?")
    )
  "patterns allowing to recognize a request")

;;; process-patterns translates the simplified format
(defParameter *assert-patterns*
          '((* "note" *)
            (* "remember" *)
            )
  "patterns allowing to recognize an assertion")

(defParameter *command-patterns*
  '((* "do" (?* ?y))
    (* "make" (?* ?y))
    (* "file" (?* ?y))
    (* "tell" "me" (?* ?y))
    (* "show" (?* ?y))
    (* "display" (?* ?y))
    (* "call" (?* ?y))
    (* "find" (?* ?y))
    (* "set" (?* ?y))
    (* "reset" (?* ?y))
    (* "post" (?* ?y))
    (* "send" (?* ?y))
    (* "mail" (?* ?y))
    (* "use" (?* ?y))
    (* "forget" (?* ?y))
    )
  "patterns allowing to recognize a command")

;;; ------------------------------------------------------------------------------
;;; Globals for persistency
;;; ------------------------------------------------------------------------------

;;; persistency depends on the availability of a Redis server
(defparameter *redis-host* "192.168.1.11" "default/must agree with menu!!!")
(defparameter *redis-port* 6379)

(defparameter *redis* nil "we  have a Redis server running")
(defparameter *redis-connected* nil "t when Redis server is connected")
(defparameter *redis-instance* nil "an integer specifying the current Redis instance")

;;; used when using several ontologies in different packages
(defParameter *database-stub*
  '(("$SYS" . $SYS)("$FN" . $FN)("$UNI" . $UNI)("$CTR" . $CTR)
    ("*ANY*" . *ANY*)("*NONE*" . *NONE*)) "Proxy classes in the database stub")

;;; used for persistency

(defVar *base* () "Name of the currently opened base")
(defVar *disk* () "a-list simulating a disk file")
(defVar *lob* () 
  "LOB object for handling update programs. Initialized in the LOB module.")
(defVar *mgf-error* nil)
(defVar *moss-stand-alone* t "indicates whether MOSS is standalone or not")

;;; ------------------------------------------------------------------------------
;;; Globals for export
;;; ------------------------------------------------------------------------------

(defparameter *attribute-list* ()) ; when defined outside classes
(defparameter *class-list* ())
(defparameter *method-list* ())
(defparameter *relation-list* ()) ; when defined outside classes
(defparameter *universal-method-list* ())
(defparameter *variable-list* ())
(defparameter *function-list* ()) ; $SFL
(defparameter *instance-list* ())
(defparameter *virtual-object-list* ())

(defparameter *title-width* 80)

;;;==============================================================================
;;;                            SOL Globals
;;;==============================================================================

(in-package :sol)

(defParameter *sol-version* "5.6" "global version of SOL suite")

(defParameter *sol-header* (concatenate 'string "SOL Compiler v" *sol-version*))

(defParameter *ontology-title* "SOL-ONTOLOGY" "default title")
(defParameter *language-list* '(("English" . :en)("Français" . :fr)) "default languages")
(defParameter *sol-file* "ontologies/test.sol" "default sol file")
(defVar *ontology-initial-directory* nil "default ontology directory")

(defParameter *log-file* t "contains the path to the error log file")

(defParameter *initial-parameters*
  '(("ontology.title" . *ontology-title*)
    ("ontology.languages" . *language-list*)
    ("ontology.input-file" . *sol-file*)
    ;("ontology.encoding" . *encoding*) ; must be UTF-8
    ))

(defParameter *filename-display* nil "area to display the filename")

;;; *tk* is used in the compiler files

(defParameter *tk* nil "nil means we do not use Tk interface")

(defParameter *compiling* t "default is to compile")

(defParameter *owl-output* t "default is to produce OWL code")
(defParameter *html-output* nil "default does not produce HTML nor TEXT code")
(defParameter *rule-output* nil "default is no rule output")

(defParameter *sol-language* :en "default language is English")
(defParameter *encoding* :utf8 "default encoding is UTF-8")

(defParameter *rule-format* :jena "default rule format is JENA")
(defParameter *rule-format-list* 
  '(("Jena" . :jena)("Moss" . :moss)("SPARQL" . :sparql)))

(defParameter *verbose-compile* nil "default is no trace")

(defparameter *trace-window* nil "pointer to the SOL trace window")
(defparameter *trace-pane* nil "printing pane in SOL trace-window")
(defParameter *trace-header* "SOL Compiler Trace
Copyright Barthes@UTC, 2006" 
  "the header to appear in the trace window")

#+MCL
(progn ; macros allowing to switch packages easily
  (defun ipo () (in-package :sol-owl))
  (defun iph () (in-package :sol-html))
  )

;;;==============================================================================
;;;                           SOL-HTML Globals
;;;==============================================================================

(in-package :sol-html)

(defParameter *copyright*
  "SOL to HTML compiler - v 1.2 @Barthès@UTC,2005")

(defParameter *output* t)
(defParameter *graph* nil "text file for the grapher")
(defParameter *isa-graph* nil "is-a text file for the grapher")
(defParameter *compiler-pass* 1 "compiler has 2 passes")

(defParameter *language-tags* '(:en :fr :it :pl :*))
(defParameter *language-index-property* 
  '((:en . "isEnIndexOf") (:fr . "isFrIndexOf") (:it . "isItIndexOf")
    (:pl . "isPlIndexOf")))
(defVar *ontology-title* "default local to this file")
(defParameter *current-language* :en)
(defParameter *class-title* 
  '(:en "Classes" :fr "Classes"))
(defParameter *attribute-title*
  '(:en "Attributes" :fr "Attributs"))
(defParameter *relation-title*
  '(:en "Relations" :fr "Relations"))
(defParameter *individual-title*
  '(:en "Individuals" :fr "Instances"))
(defParameter *class-header*
  '(:en "Class:" :fr "Classe :"))

;;; the following types are xsd types built in in OWL
(defParameter *attribute-types*
  '((:string . "string") (:boolean . "boolean") (:decimal . "decimal") 
    (:float . "float") (:double . "double") (:date-time . "dateTime")
    (:time . "time") (:date . "date") (:g-year-month . "gYearMonth")
    (:g-year . "gYear") (:g-month-day . "gMonthDay") (:g-day . "gDay")
    (:g-month . "gMonth") (:hex-Binary . "hexBinary") 
    (:base-64-binary . "base64Binary")
    (:any-uri . "anyURI") (:normalized-string . "normalizedString") 
    (:token . "token")
    (:language . "language") (:nm-token . "NMTOKEN") (:name . "Name")
    (:nc-name . "NCName") (:integer . "integer")  
    (:non-positive-integer . "nonPositiveInteger") 
    (:negative-integer . "negativeInteger") (:long . "long") (:int . "int")
    (:short . "short") (:byte . "byte") 
    (:non-negative-integer . "nonNegativeInteger")
    (:unsigned-long . "unsignedLong") (:unsigned-int . "unsignedInt")
    (:unsigned-short . "unsignedShort") (:unsigned-byte . "unsignedByte")
    (:positive-integer . "positiveInteger")))

(defParameter *index* (make-hash-table :test #'equal) "internal compiler table")
(defParameter *index-list* ())
(defParameter *attribute-list* ())
(defParameter *concept-list* ())
(defParameter *relation-list* ())
(defParameter *individual-list* ())

(defParameter *indent-amount* 2)
(defParameter *left-margin* 0)

(defParameter *chapter-counter* 0)
(defParameter *section-counter* 0)

(defParameter *cverbose* t)

(defParameter *translation-table* '((#\' #\space)))

;;; globals for the first syntax check compiler pass
(defParameter *line* nil)
(defParameter *text*  "")
(defParameter *breaks* '("(defontology" "(defchapter" "(defsection"
                         "(defconcept" "(defindividual"))
(defParameter *syntax-errors* nil)

;;;==============================================================================
;;;                           SOL-OWL Globals
;;;==============================================================================

(in-package :sol-owl)

;;; from SOL2OWL.lisp

(defParameter *xmlschema* "http://www.w3.org/2001:XMLSchema#")
(defParameter *xmlschema-string* "http://www.w3.org/2001:XMLSchema#string")

(defParameter *ontology-title* "ONTOLOGY" "default ontology title")

(defParameter *log-file* t "contains the path to the error log file")

(defParameter *language-tags* '(:en :fr :it :pl :*))
(defParameter *language-index-property* 
  '((:en . "isEnIndexOf") (:fr . "isFrIndexOf") (:it . "isItIndexOf")
    (:pl . "isPlIndexOf")))

;;; the following types are xsd types built in in OWL
(defParameter *attribute-types*
  '((:string . "string") (:boolean . "boolean") (:decimal . "decimal") 
    (:float . "float") (:double . "double") (:date-time . "dateTime")
    (:time . "time") (:date . "date") (:g-year-month . "gYearMonth")
    (:g-year . "gYear") (:g-month-day . "gMonthDay") (:g-day . "gDay")
    (:g-month . "gMonth") (:hex-Binary . "hexBinary") 
    (:base-64-binary . "base64Binary")
    (:any-uri . "anyURI") (:normalized-string . "normalizedString") 
    (:token . "token")
    (:language . "language") (:nm-token . "NMTOKEN") (:name . "Name")
    (:nc-name . "NCName") (:integer . "integer")  
    (:non-positive-integer . "nonPositiveInteger") 
    (:negative-integer . "negativeInteger") (:long . "long") (:int . "int")
    (:short . "short") (:byte . "byte") 
    (:non-negative-integer . "nonNegativeInteger")
    (:unsigned-long . "unsignedLong") (:unsigned-int . "unsignedInt")
    (:unsigned-short . "unsignedShort") (:unsigned-byte . "unsignedByte")
    (:positive-integer . "positiveInteger")))

(defParameter *attribute-list* ())
(defParameter *index-list* ()) ; JPB051009
(defParameter *relation-list* ())
(defParameter *concept-list* ())
(defParameter *individual-list* ())
(defParameter *super-class-list* ())
(defParameter *virtual-attribute-list* ()) ; JPB071231
(defParameter *virtual-composed-attribute-list* ()) ; JPB080105
(defParameter *virtual-concept-list* ()) ; JPB071231
(defParameter *virtual-property-list* ()) ; JPB080105
(defParameter *virtual-relation-list* ()) ; JPB071231
(defParameter *rule-list* ()) ; JPB080114
(defParameter *error-message-list* ())

(defParameter *index* (make-hash-table :test #'equal) "internal compiler table")

(defParameter *compiler-pass* 1 "compiler has 2 passes")
(defParameter *left-margin* 0)
(defParameter *output* t "default output is listener")
(defParameter *indent-amount* 2)
(defParameter *errors-only* nil)
(defParameter *cverbose* nil)

(defParameter *translation-table* '((#\' #\space)))
(defParameter *current-language* :en "default language is English")

(defParameter *separation-width* 90)

;;; globals for the first syntax check compiler pass
(defParameter *line* nil)
(defParameter *line-number* 0 "counting lines being read")
(defParameter *text*  "")
(defParameter *breaks* '("(defontology" "(defchapter" "(defsection"
                         "(defconcept" "(defindividual" ":EOF"
			 "(defruleheader" "(defvirtualconcept"
			 "(defvirtualrelation" "(defrule"))
(defParameter *syntax-errors* nil)

;;; globals for rules

(defParameter *r-output* t "channel to rule file, default is listener")
(defParameter *rule-error-list* nil "list of error strings")
(defParameter *rule-file* nil)
(defParameter *rule-builtin-operator-list*
  '((:jena (:gt . "greaterThan")(:lt . "lessThan")(:ge . "ge")(:le . "le")
           (:equal . "equal")(:not-equal . "notEqual")
           (:all . "all"))
    (:sparql (:gt . ">")(:lt . "<")(:ge . ">=")(:le . "<=")
             (:equal . "=")(:not-equal . "!="))))
(defParameter *rule-prefix-ref* nil)
(defParameter *user-defined-rule-operators* ())

;;; from SOL2RULES.lisp

(defParameter *rule-prefix* "" "no prefix by default")
(defParameter *sparql-rule-header* nil "to cache rule header")
(defParameter *sparql-function-prefix* nil "name space of sparql functions")
(defParameter *owl-meta-concepts*
  '("owl:Class" "owl:DatatypeProperty" "owl:ObjectProperty" "owl:VirtualClass"
    "owl:VirtualDatatypeProperty" "owl:VirtualObjectProperty"))


;;;==============================================================================
;;;                            NODGUI interface
;;;==============================================================================

(in-package :nodgui-user)

;; tell CCL which version of Tk to use and where it is

(eval-when (:compile-toplevel :load-toplevel :execute)
  (setq *wish-pathname* 
"/Library/Frameworks/Tk.framework/Versions/8.6/Resources/Wish.app/Contents/MacOS/Wish"))

(defParameter *moss-window* nil "MOSS interface window")

(defParameter *moss-import-window* nil) 
(defparameter *default-ontology-name* "Family")
(defparameter *default-package-name* "?")
(defparameter *legal-import-formats* '("MOSS" "RAW DATA(LISP)")) ; OWL in the future?
(defparameter *current-import-format* "MOSS")
(defparameter *legal-export-formats* '("RAW DATA(LISP)" "MOSS" "OWL+JENA/SPARQL"
                                       "HTML"))
(defparameter *ontology-text-file* nil "last file loaded")
(defparameter *current-export-format* "RAW DATA(LISP")

;;;==============================================================================
;;;                            MLN multilingual
;;;==============================================================================

(in-package :mln)

(defParameter mln::*language-tags* 
  '(:cn :de :en :es :fr :it :jp :pt :br :zh :zh-hant :unknown) 
  "allowed languages")



:EOF
