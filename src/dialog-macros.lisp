;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;21/01/06
;;;		D I A L O G - M A C R O S (File dialog-macros.lisp)		
;;;
;;;=============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
This file contains a set of global variables and macros to let users design and
build dialogs more easily.
Because the current version of Tcl/Tk is blocking the dialog process has been 
rewritten as of October 2019 to work as follows:
 - the user types something into an input window
 - a function takes the resulting string, creates a conversation object if
   needed, segments the input unless it is to be taken verbatim, and saves it
   in the :raw-input and :input slots of the FACTS database
 - the crawler is then called on the node recorded in the conversation object
 - the =resume method makes an analysis of the input executes any specified 
   action, computes a transition, saves it in the conversation object and 
   returns control to the user if some data are needed from the user
The file contains some additional functions to simplify the analysis of the input
sentence and facilitate the pattern matching.

HISTORY
2019
 1016 receated using the experience from the ACL approach
2021
 0106 adding conversation-id when sending a message from dialog to indicate which
      conversation is concerned when there are multiple conversations
|#

(in-package :moss)


;;;=============================================================================
;;;                     Global variables (see globals.lisp)
;;;=============================================================================

#|
(defVar _q-abort () "abort state in main dialog")

;; we use a string for marker to avoid package problems with symbol-names
;; we could have used a keyword
(defParameter *pattern-marker* "?*" "segment pattern marker")
(defParameter *performatives* '(:request :command :assert :answer))

(defParameter *abort-commands* '(":quit" "abort" ":exit" ":reset" ":cancel")
  "when typed by the user triggers a transition to the _q-abort state.")
(defParameter *yes-answers* '("yes" "Y" "oui" "Ja" "Jawohl"))
(defParameter *no-answers* '("no" "N" "nein" "non" "never" "nimmer" "jamais"))
(defParameter *why-patterns* 
  '(("why" *) 
    )
  "patterns to recognize a why question")
(defParameter *request-patterns*
  '(("what" "is" *)
    ("what" "are" *)
    ("what" *)
    ("when" *)
    ("where" *)
    ("why" *)
    ("who" *)
    ("whom" *)
    ("whose" *)
    ("how" "much" *)
    ("how" *)
    ("is" *)
    ("are" *)
    ("were" *)
    ("was" *)
    ("have" *)
    ("had" *)
    ("do" *)
    ("did" *)
    ("can" *)
    ("could" *)
    ("may" *)
    ("might" *)
    (* "?")
    )
  "patterns allowing to recognize a request")

;;; process-patterns translates the simplified format
(defParameter *assert-patterns*
          '((* "note" *)
            (* "remember" *)
            )
  "patterns allowing to recognize an assertion")

(defParameter *command-patterns*
  '((* "do" (?* ?y))
    (* "make" (?* ?y))
    (* "file" (?* ?y))
    (* "tell" "me" (?* ?y))
    (* "show" (?* ?y))
    (* "display" (?* ?y))
    (* "call" (?* ?y))
    (* "find" (?* ?y))
    (* "set" (?* ?y))
    (* "reset" (?* ?y))
    (* "post" (?* ?y))
    (* "send" (?* ?y))
    (* "mail" (?* ?y))
    (* "use" (?* ?y))
    (* "forget" (?* ?y))
    )
  "patterns allowing to recognize a command")
|#

(defUn noop (&rest ll) ll) ; to keep the compiler quiet when bindings is used

(defUn in-let-list? (xx let-list)
  (cond
   ((null let-list) nil)
   ((and (listp (car let-list))(eql xx (caar let-list))))
   ((listp (car let-list)) (in-let-list? xx (cdr let-list)))
   ((eql xx (car let-list)))
   (t (in-let-list? xx (cdr let-list)))))


;;;=============================================================================
;;;                                 Macros
;;;=============================================================================

;;; Macros are:
;;;  - defsimple-subdialog
;;;  - defstate
;;;  - defsubdialog
;;;  - deftask

;;;-------------------------------------------------------------------- DEFSTATE
;;; The defstate macro is fairly complex and defines the dialog language.
;;; The use of the different options is non trivial.
;;; When a state is entered, the =execute method is applied to the new state.
;;; Then the =resume method is called on the state.
;;; If a question to the user was asked (:question, :ask, options),
;;; then processing the answer waits until something appears in the
;;; to-do slot of the conversation object.
;;;
;;; the =execute method
;;; ===================
;;; The method normally processes the content of the :INPUT area of the FACTS slot
;;; of the conversation. It may have a single option :question. Without options 
;;; the method does nothing and the =resume method is called immediately.
;;; The FACTS/INPUT area should contain a list of words or the symbol :FAILURE.
;;; The return value is :WAIT or :RESUME.
;;;
;;;   :answer-type  
;;;   ------------
;;; Can be used to specify the type of the expected answer message. If the answer
;;; is not of that type, it shows a rupture in the dialog.
;;;
;;;   :clear-all-facts
;;;   ----------------
;;; Clears the content of the FACT base (removes everything)
;;;
;;;   :clear-fact (item)
;;;   ------------------
;;; Sets the value associated with item to NIL. Usually item is a keyword.
;;;
;;;   :clear-facts (item-list)
;;;   ------------------------
;;; Same as clear-fact but takes a list of items
;;;
;;;   :delay-answer (&optional direction)
;;;   -------------------------
;;; Applies to web interfaces. Sets a flag in the FACT area of the conversation 
;;; objects so that the answer of requests to external agents are not posted
;;; immediately but saved until OMAS asks the next question. Direction is :inc,
;;; :dec, or :clear (default is :inc).
;;; When setting the ref count must be done before calling :send-message.
;;;
;;;   :eliza  
;;;   ------
;;; Forced call to ELIZA. We expect an answer from the master. 
;;;
;;;   :execute-preconditions  
;;;   ----------------------
;;; Escape mechanism to execute Lisp code directly in the =execute method, e.g. 
;;; to set flags. 
;;;
;;;   :question | :question-no-erase
;;;   ------------------------------
;;; The associated value is either a string or a list of strings. When 
;;; we have a list of strings, one is chosen randomly for printing. uses =display-text
;;; Once the question is asked, the user is supposed to answer. The answer is
;;; inserted into the INPUT area of the FACTS slot in the conversation object 
;;; that is the link between the external system and MOSS.
;;; The question is processed by the =resume method.
;;;
;;;   :reset-conversation
;;;   -------------------
;;; Restarts the main conversation, cleaning all values of the conversaiton oject.
;;;
;;;   :send-message args
;;;   ------------------
;;; Send a message to other agents. Args are those for an OMAS message. An answer
;;; is expected. A timeout may be specified.
;;;
;;;   :send-message-no-wait args
;;;   --------------------------
;;; Send a message to other PAs. Since the message is similar to an e-mail, we do
;;; not wait for the answer in the dialog (not in the process).
;;;
;;;   :text {:no-erase}
;;;   -----------------
;;; Print text e.g. prior to asking a question. Uses =display-text
;;;
;;;   :text-exec expr
;;;   ---------------
;;; If expr is a list starting with format, execute it before printing it. Uses
;;; =display-text
;;;
;;;   :wait
;;;   -----
;;; Force the dialog crawler to wait for an input even if no question was put to
;;; the master by letting the =execute method return a :wait tag
;;;
;;; The =resume method
;;; ==================
;;; Called after the =resume method. The user data are in the :INPUT area of the 
;;; current conversation, or in different field of the FACTS slot of the 
;;; conversation object.
;;; Options are the following.
;;;
;;; Whenever the input contains ":quit :abort :exit :reset :cancel" a transition
;;; to the global _Q-abort state occurs.
;;;
;;;   :answer-analysis
;;;   ----------------
;;; Indicates that a special method named =answer-analysis will be applied to
;;; the data (INPUT). The method must return a transition state or
;;; the :abort keyword, in which case the conversation is locally restarted.
;;; All other options are ignored.
;;;
;;;   :execute function args
;;;   --------
;;; Executes the corresponding FUNCTION and args
;;;
;;;   :transitions
;;;   ------------
;;; transitions are clauses (transition rules) with an IF part, an INTERMEDIATE part
;;; and a TRANSITION part, each with options.
;;; IF part options of a transition clause:
;;;
;;; :always
;;;    always execute this clause, any clause following is ignored
;;; :contains <list of words>
;;;    test if the input contains one of the words
;;; :empty 
;;;    test if the input is nil (not very useful?)
;;; :no 
;;;    test if the input is a negation (does not work for Asian languages)
;;; :on-failure
;;;    tests if the returned value from the master or from the agents was a failure
;;;    by checking if FACTS/INPUT is (:failure) 
;;; :otherwise
;;;    always fire (should be the last option)
;;; :patterns ({<pattern> {<action>}+}+)
;;;    test each pattern against the content of INPUT, if it matches, executes 
;;;    actions
;;; :rules ({{<pattern>}{<answer>}*}* {<action>}+}+)
;;;    tries to apply a set of rules. If one applies, then computes an answer ELIZA
;;;    style, should use :display-result to display it
;;; :starts-with 
;;;    test if the answer starts with a word (use rather patterns)
;;; :test expr
;;;    fires if the test applies (expr evaluates to non nil)
;;; :yes 
;;;    test if the answer is yes (does not work with Asian languages)
;;;
;;; THEN options for intermediate actions (before a transition):
;;; :display-answer
;;;    send the content of the FACTS/ANSWER area to the output channel for
;;;    printing; calls display-answer that uses =display-text that uses the 
;;;    display-text method of the output channel (window)
;;; :exec {sexpr}
;;;    executes the sexpr in the context of =resume
;;; :display-result
;;;    displays the sentence resulting from applying the set of rules to the input;
;;;    uses =display-text directly
;;; :format-answer format-function
;;;    formats the content of the FACTS/ANSWER area applying the format-function to
;;;    produce a string, uses then the =display-text method. It is the responsibility
;;;    of the format-function to produce HTML strings when the answer must be shippec
;;;    to the web server. format-answer takes 2 arguments: the first one is the answer
;;;    the second one if t indicates that the result needs to be an HTML string
;;; :format-answer-full format-function
;;;    like :format-answer but accepts conversation as a third argument, allowing to
;;;    let format-function to have access to the FACTS area of conversation
;;; :keep
;;;    used with patterns, saves the value of the pattern variable into FACTS/INPUT 
;;; :print-answer print-function
;;;    prints the content of the FACTS/ANSWER area applying the print funtion
;;; :replace {value}
;;;    replaces the content of INPUT with the specified value (word list)
;;; :set-performative {:request|:command|:assert}
;;;    set the FACTS/PERFORMATIVE area (canbe used to check if we are waiting for an
;;;    answer rather than a request
;;;
;;; THEN options for transitions:
;;; :failure
;;;    exit from state with :failure marker, anything following that is ignored 
;;; :reset
;;;    restarts the local (sub-)conversation, incompatible with anything else
;;; :sub-dialog dialog-header
;;;    specifies a sub-dialog
;;;    :success state : transfer state in case of success
;;;    :failure state : transfer state in case of failure
;;; :success
;;;    exit from state with success marker, anything following that is ignored
;;; :target state
;;;    specifies the transition state
;;; Default is to restart the conversation at the beginning.

#|
;;;--------------------------------------------------------- DEFSIMPLE-SUBDIALOG
;;; used by OMAS agents

;;; This macro is provided for simplifying the writing of very simple dialogs 
;;; when the conversation graph contains a single node and we want to send a 
;;; message to another agent to get some data or information that we are going
;;; to print. 
;;; The macro produces the declarations of the global variables, the subdialog 
;;; headers, the state, and the =execute and =resume methods.
;;; E.g.
;;;  (defsimple-subdialog "get-financing" "_gfi"
;;;     :explanation "Master is trying to obtain info about a financing program."
;;;     :from PA_HDSRI :to :FINANCING :action :get-financing
;;;     :language :fr
;;;     :pattern ("financement" ("pays")("titre")("date limite") ("URL"))		 
;;;     :sorry "- Désolé, je ne trouve pas le programme de financement demandé."
;;;     :print-fcn #'print-financing
;;;     )
;;; This macro call builds a graph for sending a message to the FINANCING agent
;;; with action :get-financing, pattern for the answer, a message to print in
;;; case of failure and a function to print the results in case of success. The
;;; data arguments are read from the conversation :input slot.
;;; Default printing function is display-answer

(defMacro defsimple-subdialog (label prefix &key explanation from to action
                                     language pattern format-fcn print-fcn sorry)
  (declare (ignore print-fcn))
  (let ((conversation-tag 
         (intern  ; could use make-symbol ?
          (string-upcase (concatenate 'string "_" label "-conversation"))))
        (entry-state-tag
         (intern 
          (string-upcase (concatenate 'string prefix "-entry-state"))))
        (sorry-tag
         (intern 
          (string-upcase (concatenate 'string prefix "-sorry"))))
        )
    `(progn
       (eval-when (:compile-toplevel :load-toplevel :execute)
         (proclaim '(special ,conversation-tag ,entry-state-tag ,sorry-tag)))
       
       (defsubdialog 
           ,conversation-tag
           (:label ,(format nil "~A dialog" label))
         (:explanation ,explanation))
       
       (defstate
           ,entry-state-tag
           (:label ,(format nil "~A dialog - entry state" label))
         (:entry-state ,conversation-tag)
         (:explanation 
          ,(format nil "Assistant is sending a free-style message to the ~A agent."
             to))
         (:send-message :to ,to :self ,from :action ,action
                        :args `(((:data . ,(get-fact :input))
                                 (:language . ,,language)
                                 ,@,(if pattern `'((:pattern . ,pattern)))
                                 )))
         (:transitions
          (:on-failure :target ,sorry-tag)
          (:otherwise
           ,@(if format-fcn `(:format-answer ,format-fcn) '(:display-answer))
           :success)))
       
       (defstate
           ,sorry-tag
           (:label ,(format nil "~A failure." label))
         (:explanation "We can't get the info.")
         (:reset)
         (:text ,sorry)
         (:transitions (:failure)))
       )))
|#

#|
CG-USER(1): (moss::defsimple-subdialog "get-financing" "_gfi"
     :explanation "Master is trying to obtain info about a financing program."
     :from PA_HDSRI :to :FINANCING :action :get-financing
     :language :fr
     :pattern ("financement" ("pays")("titre")("date limite") ("URL"))		 
     :sorry "- Désolé, je ne trouve pas le programme de financement demandé."
     :format-fcn #'format-financing
     )

(PROGN (EVAL-WHEN (:COMPILE-TOPLEVEL :LOAD-TOPLEVEL :EXECUTE)
         (PROCLAIM '(SPECIAL _GET-FINANCING-CONVERSATION _GFI-ENTRY-STATE _GFI-SORRY)))
  (DEFSUBDIALOG _GET-FINANCING-CONVERSATION (:LABEL "get-financing dialog")
    (:EXPLANATION "Master is trying to obtain info about a financing program."))
  (DEFSTATE _GFI-ENTRY-STATE (:LABEL "get-financing dialog - entry state")
    (:ENTRY-STATE _GET-FINANCING-CONVERSATION)
    (:EXPLANATION
     "Assistant is sending a free-style message to the FINANCING agent.")
    (:SEND-MESSAGE :TO :FINANCING :SELF PA_HDSRI :ACTION :GET-FINANCING :ARGS
                   (EXCL::BQ-LIST (EXCL::BQ-LIST* (EXCL::BQ-CONS `:DATA
                                                                 (READ-FACT
                                                                  MOSS::CONVERSATION
                                                                  :INPUT))
                                                  (EXCL::BQ-CONS `:LANGUAGE :FR)
                                                  '((:PATTERN "financement" ("pays") ("titre")
                                                              ("date limite") ("URL"))))))
    (:TRANSITIONS (:ON-FAILURE :TARGET _GFI-SORRY)
                  (:OTHERWISE :PRINT-ANSWER #'PRINT-FINANCING :SUCCESS)))
  (DEFSTATE _GFI-SORRY (:LABEL "get-financing failure.")
    (:EXPLANATION "We can't get the info.") (:RESET)
    (:TEXT "- Désolé, je ne trouve pas le programme de financement demandé.")
    (:TRANSITIONS (:FAILURE))))
|#
;;;---------------------------------------------------------------- DEFSUBDIALOG

(defMacro defsubdialog (dialog-name &rest option-list)
  "defines a piece of dialog that will be used in larger dialogs. A sub-dialog  ~
   can be called as a subroutine in a programming environment. It is in charge ~
   of finding a specific information. 
   The sub-dialog  gets data in the HAS-DATA slot if the context object and ~
   returns answer in the :RESULTS area of the FACTS slot Its has a ~
   header, an entry state, a success exit state and failure exit state.
Arguments:
   dialog-name: a symbol that will be used to make various names
   option-list: a list of options
   ;action: action to be executed
   label: title of the subdialog
   sates: list of state variables of the dialog
   explanation: text
Return:
   the identifier of the dialog header."
  (let* (#|
(action (cadr (assoc :action option-list)))
|#
         (label (cadr (assoc :label option-list)))
         (explanation (cadr (assoc :explanation option-list)))
         (states (cdr (assoc :states option-list)))
         )
    `(progn
       (declaim (special ,dialog-name ,@states))
       (definstance 
           MOSS-DIALOG-HEADER
           (HAS-MOSS-LABEL ,label)
         (HAS-MOSS-EXPLANATION ,explanation)
         #|
,@(if action `((HAS-MOSS-ACTION ,action)))
|# ; points towards the subclass
         (:var ,dialog-name)
         )
       ,dialog-name)))

#|
(defsubdialog _explain-conversation 
              (:label "Explain conversation")
  (:states  _exp-entry-state)
  (:explanation "The data is searched for a concept to be explained.")
  )
Expansion

(PROGN (DECLAIM (SPECIAL _EXPLAIN-CONVERSATION _EXP-ENTRY-STATE))
  (DEFINSTANCE MOSS-DIALOG-HEADER (HAS-MOSS-LABEL "Explain conversation")
    (HAS-MOSS-EXPLANATION "The data is searched for a concept to be explained.")
    (:VAR _EXPLAIN-CONVERSATION))
  _EXPLAIN-CONVERSATION)
|#
;;;-------------------------------------------------------------------- DEFSTATE
;;; the result of defstate can be checked by setting moss::*debug* to t. In that
;;; case executing defstate only prints the resulting =ECECUTE and =RESUME 
;;; methods and does not build any object

(defMacro defstate (state-name &rest arg-list)
  "macro to produce the code for buiding a conversation state.
Arguments:
   state-name: name of a global variable pointing to the state object
   arg-list: rest of the arguments for defining the state (the syntax is complex)."
  `(apply #'make-state ',state-name ',arg-list))

#|
? (setq moss::*debug* t)
T
Should be executed from the :ALBERT package

? (defstate 
  _pt-brush
  (:label "bookkeeping")
  (:explanation "do some light bookkeeping.")
  (:exec (setf (has-details conversation) nil))
  (:immediate-transitions
   (:always :success))
  (:no-resume)
  )

;***** Debug: STATE: _PT-BRUSH

;*** Debug: state: _PT-BRUSH =EXECUTE 
   (MOSS::%MAKE-OWNMETHOD
     =EXECUTE
     _PT-BRUSH
     (MOSS::CONVERSATION &REST MOSS::MORE-ARGS)
     "*see explanation attribute of the _PT-BRUSH state*"
     (DECLARE (IGNORE MOSS::MORE-ARGS))
     (let* ((moss::data (MOSS::GET-DATA MOSS::CONVERSATION))
            (moss::context (car (HAS-Q-CONTEXT MOSS::CONVERSATION)))
            (moss::results (SEND MOSS::CONTEXT '=GET 'HAS-RESULTS))
            moss::return-value
            moss::bindings)
       (MOSS::NOOP MOSS::BINDINGS MOSS::DATA MOSS::RESULTS)
       (setf (HAS-PERFORMATIVE MOSS::CONTEXT) NIL)
       (if (intersection MOSS::*ABORT-COMMANDS* MOSS::DATA :TEST #'STRING-EQUAL)
           (throw :DIALOG-ERROR NIL))
       (catch :RETURN
         (setf (HAS-DETAILS CONVERSATION) NIL)
         (cond (T (throw :RETURN (setq MOSS::RETURN-VALUE (list :SUCCESS)))))
         (setq MOSS::RETURN-VALUE '(:RESUME)))
       MOSS::RETURN-VALUE))
ALBERT:_PT-BRUSH
|#
;;;------------------------------------------------------------------------ DEFTASK
;;; previous version of the deftask macro that allowed defining tasks in the agent
;;; package, avoiding possible confusion when several dialogs are involved. In other
;;; words, task objects belong to the PA ontology. This has consequences on the 
;;; functions dealing with tasks, since they were defined for the MOSS Help dialog
;;; and must be adapted to each agent
;;; - another question: do we use MOSS properties or redefine properties for each
;;; agent? 
;;; for the same reason it seems better to redefine the properties for each agent
;;; In short, the whole dialog mechanism should be rewritten for OMAS, rather than
;;; borrowing pieces from MOSS, leading to isolate the MOSS dialog from the OMAS
;;; dialogs

(defMacro deftask (name &key target (performative '(:request :command)) dialog
                        indexes doc where-to-ask action)
  (let (res)
    ;; check first current package
    (if (eql *package* (find-package :moss))
        ;;== if :MOSS, then we are defining the MOSS dialog
        `(defindividual "MOSS-TASK"
             (:doc ,@(if doc (list doc) '("*no documentation*")))
           ,@ (if target `(("moss target" ,target)))
           ("moss task name" ,name)
           ,@(if (listp performative) `(("moss performative" . ,performative))
               `(("moss performative" ,performative)))
           ;("moss performative" ,performative)
           ("moss dialog" ,@(if dialog (list dialog)
                              (error "missing dialog for ~S" name)))
           ("moss index pattern"
            ,@(loop
                (unless indexes (return (reverse res)))
                (unless (cdr indexes)
                  (error "arg to :indexes should be an even list in ~S" name))
                (push `(:new "MOSS task index" ("MOSS index" ,(pop indexes))
                             ("MOSS weight" ,(pop indexes)))
                      res)))
           )
      ;;== otherwise we are in an the application package
      `(defindividual "TASK"
           (:doc ,@(if doc (list doc) '("*no documentation*")))
         ,@(if target `(("target" ,target)))
         ("task name" ,name)
         ;; if performative is a list, adds it as a list
         ,@(if (listp performative) `(("performative" . ,performative))
             `(("performative" ,performative)))
         ("dialog" ,@(if dialog (list dialog) (error "missing dialog for ~S" name)))
         ("index pattern"
          ,@(loop
              (unless indexes (return (reverse res)))
              (unless (cdr indexes)
                (error "arg to :indexes should be an even list in ~S" name))
              (push `(:new "task index" ("index" ,(pop indexes))
                           ("weight" ,(pop indexes)))
       
             res)))
         ,@ (if where-to-ask `(("where to ask" ,where-to-ask)))
         #|
,@ (if action `(("message action" ,action)))
|#
         ))))

#|
(defMacro deftask (name &key (performative '(:request :command)) dialog
                        indexes doc)
  (let (res)
    `(defindividual "MOSS-TASK"
                    (:doc ,@(if doc (list doc) '("*no documentation*")))
       ("moss task name" ,name)
       ,@(if (listp performative) `(("moss performative" . ,performative))
             `(("moss performative" ,performative)))
       ("moss dialog" ,@(if dialog (list dialog)
                            (error "missing dialog for ~S" name)))
       ("moss index pattern"
        ,@(loop
            (unless indexes (return (reverse res)))
            (unless (cdr indexes)
              (error "arg to :indexes should be an even list in ~S" name))
            (push `(:new "MOSS task index" ("MOSS index" ,(pop indexes))
                         ("MOSS weight" ,(pop indexes)))
                  res)))
       )))
|#

#| 
(deftask "What is XXX?"
  :doc "Task to explain the meaning of a concept in the ontology (question)."
  ;:performative :request
  :dialog _what-conversation
  :target _my-target
  :indexes
   ("what is" .4
    "what are" .5
    )
  )
expands to:
(DEFINDIVIDUAL "MOSS-TASK" 
    (:DOC "Task to explain the meaning of a concept in the ontology (question).")
  ("moss target" _MY-TARGET) ("moss task name" "What is XXX?")
  ("performative" :REQUEST :COMMAND) ("moss dialog" _WHAT-CONVERSATION)
  ("moss index pattern"
   (:NEW "MOSS task index" ("MOSS index" "what is") ("MOSS weight" 0.4))
   (:NEW "MOSS task index" ("MOSS index" "what are") ("MOSS weight" 0.5))))
|#


;;;=============================================================================
;;;                             =EXECUTE METHOD
;;;=============================================================================

;;;---------------------------------------------- MAKE-STATE-EXECUTE-METHOD-BODY

(defun make-state-execute-method-body (arg-list &aux body)
  (macrolet ((+append (arg) `(setq body (append body ,arg))))   
    (dolist (item arg-list body)
      (+append 
       (case (car item)
         (:execute-preconditions (make-state-execute-preconditions (cdr item)))
         (:clear-all-facts (make-state-clear-all-facts))
         (:clear-fact (apply #'make-state-clear-fact (cdr item)))
         (:clear-facts (apply #'make-state-clear-facts (cdr item)))
		 (:delay-answer (apply #'make-state-delay-answer (cdr item)))
         (:eliza (make-state-eliza t))
         (:reset-conversation (make-state-reset-conversation t))
         (:text (apply #'make-state-text (cdr item)))
         (:text-exec (apply #'make-state-text-exec (cdr item)))
         (:question (make-state-question (cdr item)))
         (:question-no-erase (make-state-question-no-erase (cdr item)))
         #+OMAS (:send-message (make-state-send-message (cdr item)))
         #+OMAS (:send-message-no-wait (make-state-send-message (cdr item)))
         (:answer-type (make-state-set-answer-type (cdr item)))
         )))))
#|
(make-state-execute-method-body
 '((:question "Is ELIZA here?")))
((SEND CONVERSATION '=DISPLAY-TEXT "Is ELIZA here?" :ERASE T))

(make-state-execute-method-body
 '((:execute-preconditions
    (setq flag t)
    (replace moss::conversation :item t))
   (:question "Is ELIZA here?")
   ))
((SETQ MOSS::FLAG T) (REPLACE MOSS::CONVERSATION :ITEM T)
 (SEND MOSS::CONVERSATION '=DISPLAY-TEXT '"Is ELIZA here?" :ERASE T))

(make-state-execute-method-body
  '((:send-message :to :ANATOLE-NEWS :self PA_ANATOLE :action :add-category
                 :args 
                 `(((:data . ,(mln::make-mln `((:fr ,(get-fact :label))
                                               (:en ,(get-fact :english-label)))))
                    (:language . :FR))))))
|#
;;;--------------------------------------------------- MAKE-STATE-EXECUTE-METHOD

(defUn make-state-execute-method (state-name arg-list)
  "builds the =execute own method for a STATE. only prints the result ~
   if *debug* is true.
Arguments:
   state-name: e.g. _pt-clean
   arg-list: the arguments to the defstate macro
Return:
   the name of the method"
  (declare (special *debug*))
  ;; when *debug* is true simply prints the method
  ((lambda (body)
     (if *debug*
         (progn
           (format *debug-io* "~2&;*** Debug: state: ~S =EXECUTE" state-name)
           (pprint
            (list* '%make-ownmethod (intern "=EXECUTE")
                   state-name
                   '(conversation &rest more-args)
                   (if (%occurs-in? 'conversation body)
                       (cons (car body)
                             (cons
                              '(declare (ignore more-args))
                              (cdr body)))
                       (cons (car body)
                             (cons 
                              '(declare (ignorable conversation more-args)) 
                              (cdr body)))))))
       (%make-ownmethod 
        (intern "=EXECUTE") state-name '(conversation &rest more-args)
        (if (%occurs-in? 'conversation body)
            (cons (car body)
                  (cons
                   '(declare (ignore more-args))
                   (cdr body)))
          (cons (car body)
                (cons 
                 '(declare (ignorable conversation more-args)) 
                 (cdr body)))))))
   ;; body
   (let ((options (make-state-execute-method-body arg-list)))
     `(,(format nil "*see explanation attribute of the ~s state*" state-name)
         ;(declare (ignore more-args))
         ;; the only options are :question and :question-no-erase
         ,@options
         ;; the return value is (:wait) or (:resume)
         ,(cond
           ;; if no-wait is forced, then obey
           ((assoc :no-wait arg-list) ''(:resume))
           ;; if options contain a question or send-message, then wait
           ((make-state-question-there? arg-list) ''(:wait))
           ;; in all other cases don't wait
           (t ''(:resume)))
         ;; the return value is a (:wait) or (:resume)
         ;,(if (make-state-question-there? arg-list) ''(:wait) ''(:resume))
         ))))



#|
(setq moss::*debug* t)
(make-state-execute-method '_test-state '((:question "What is it?")))
;*** Debug: state: _TEST-STATE =EXECUTE 
(%MAKE-OWNMETHOD =EXECUTE _TEST-STATE (CONVERSATION &REST MORE-ARGS) 
                 "*see explanation attribute of the _TEST-STATE state*"
                 (DECLARE (IGNORE MORE-ARGS)) 
                 (SEND CONVERSATION '=DISPLAY-TEXT '("What is it?") :ERASE T) 
                 '(:WAIT))
(make-state-execute-method '_test-state '((:reset-conversation)
                                          (:question "What is it?")))
|#
;;;=============================================================================
;;;                             =RESUME METHOD
;;;=============================================================================

;;;---------------------------------------------------- MAKE-STATE-RESUME-METHOD

(defUn make-state-resume-method 
    (state-name arg-list &aux (*local-resume-vars* '(return-value)))
  "builds the =RESUME own method for a given state. If *debug* is true, then ~
      simply prints it.
Arguments:
   state-name: e.g. _pt-clean
   arg-list: the list of arguments of the defstate macro
Returns:
   the name of the method, NIL when printing."
  (declare (special *debug* *local-resume-vars*))
  ;; when *debug* is true simply prints the method
  ((lambda (body)
     (if *debug*
         (progn
           (format *debug-io* "~2&;*** Debug: state: ~S =RESUME" state-name)
           (pprint
            (list* '%make-ownmethod (intern "=RESUME") state-name
                   '(conversation)
                   (if (%occurs-in? 'conversation body)
                       body
                       (cons (car body)
                             (cons 
                              '(declare (ignorable conversation)) 
                              (cdr body)))))))
       (%make-ownmethod 
        (intern "=RESUME") state-name '(conversation)
        (if (or (%occurs-in? 'conversation body)
                (%occurs-in? 'get-fact body)
                (%occurs-in? 'copy-fact body)
                (%occurs-in? 'set-fact body))
            body
          `(,(car body) (declare (ignore conversation)) . ,(cdr body))
          ))))
   ;; body of the method 
   (list
    (format nil "*see explanation attribute of the ~s state*" state-name)
    ;; data to analyze is in the :INPUT area of FACTS
    (let* ((resume-body (make-state-resume-method-body state-name arg-list)))
      `(let* ,*local-resume-vars* 
         (declare (ignorable bindings))
         (catch :return
                ,@resume-body)
         return-value)))))

#|
(make-state-resume-method '_test '((:transitions(:patterns (("OK")) :target _OK))))
;*** Debug: state: _TEST =RESUME 
(%MAKE-OWNMETHOD
 =RESUME
 _TEST
 (CONVERSATION)
 "*see explanation attribute of the _TEST state*"
 (LET* (BINDINGS RETURN-VALUE)
   (CATCH :RETURN
          (COND ((SETQ BINDINGS (PROCESS-PATTERNS INPUT ''(("OK"))))
                 (SETQ RETURN-VALUE (LIST :TRANSITION _OK)))
                (T
                 (THROW :DIALOG-ERROR
                   (FORMAT NIL "bad transition syntax in state ~S" _TEST)))))
   RETURN-VALUE))
NIL
|#
;;;----------------------------------------------- MAKE-STATE-RESUME-METHOD-BODY

(defUn make-state-resume-method-body (state-name arg-list)
  "build the body of the =resume method."  
  (cond 
   ;; if we need a complex detailed analysis we call =answer-analysis
   ((assoc :answer-analysis arg-list )
    (make-state-answer-analysis 
     (or (cdr (assoc :answer-analysis arg-list)) :input) state-name))
   ;; otherwise we do a simple analysis
   (t
    `(;; eventually execute a simple expression (escape mechanism)
      ,@(getv :execute arg-list)
         ;; now deal with the transitions
         ,(let ((transitions (getv :transitions arg-list)))
            `(cond
              ;; when input contains a reset indicator, we quit
              ;; this is only necessary when we have an input from master
              ,@(if (make-state-question-there? arg-list)
                    '(((intersection *abort-commands* (read-fact conversation :input)
                                     :test #'string-equal)
                       ;; ... and restart dialog
                       (throw :dialog-error nil))))
              ;; add a line for each option of the  transitions
              ,@(mapcar #'make-state-resume-transitions transitions)
              ;; close all transitions with a safeguard clause
              ;; i.e., if we did not find a transition, we restart the dialog
              ,@(unless (or (assoc :otherwise transitions)
                            (assoc :always transitions)
                            (assoc :eliza arg-list))
                  `((t (throw :dialog-error 
                         (format nil "bad transition syntax in state ~S"
                           ,state-name)))))
              ))))))

#|
(make-state-resume-method-body '_test 
                               '((:transitions
                                  (:patterns 
                                   ((?* ?x) "Albert")
                                   :set-performative (list :request)
                                   :target _q-find-task))))
((COND ((SETQ BINDINGS (PROCESS-PATTERNS INPUT '((?* ?X) "Albert")))
        (REPLACE-FACT CONVERSATION :PERFORMATIVE (LIST :REQUEST))
        (SETQ RETURN-VALUE (LIST :TRANSITION _Q-FIND-TASK)))
       (T
        (THROW :DIALOG-ERROR
          (FORMAT NIL "bad transition syntax in state ~S" _TEST)))))

(make-state-resume-method-body '_test '((:eliza)))
((LET ((ELIZA-RULES (INTERN "*ELIZA-RULES*" *PACKAGE*)))
   (UNLESS (BOUNDP ELIZA-RULES) (SETQ ELIZA-RULES '*ELIZA-RULES*))
   (SEND CONVERSATION
         '=DISPLAY-TEXT
         (MOSS-ELIZA DATA (SYMBOL-VALUE ELIZA-RULES))
         :ERASE
         T)))
|#
;;;----------------------------------------------- MAKE-STATE-RESUME-TRANSITIONS

(defUn make-state-resume-transitions (arg-list)
  "input format: (:contains (...) :action {:clean, :add} :target new-state
                  :reset t)
Argument:
   arg-list: an alternate list containing keyword options"
  (declare (special *local-resume-vars*))
  (let ((target (cadr (member :target arg-list))))
    (remove 
     nil 
     (append 
      ;;--- start of the clause (conditional part)
      (if (member :contains arg-list)
          `((intersection ',(cadr (member :contains arg-list)) 
                          (read-fact conversation :input)
                          :test #'string-equal)))
      (if (member :empty arg-list)
          `((null (read-fact conversation :input))))
      (if (member :no arg-list)
          `((intersection *no-answers* (read-fact conversation :input)
                          :test #'string-equal)))
      (if (member :on-failure arg-list)
          `((is-answer-failure? (car (read-fact conversation :input)))))
      (if (member :starts-with arg-list)
          `((member (car (read-fact conversation :input)) 
                    ',(cadr (member :starts-with arg-list))
                    :test #'string-equal)))
      (when (member :patterns arg-list)
        ;; declare local variable
        (pushnew 'bindings *local-resume-vars*)
        ;; we got a set of patterns ELIZA style. Apply process-pattern to each one
        `((setq bindings 
                (process-patterns (read-fact conversation :input)
                                  ',(cadr (member :patterns arg-list))))
          ;(noop bindings) ; to keep the compiler quiet when bindings is not used
          ))
      (when (member :rules arg-list)
        ;; declare a local variable
        (pushnew 'result *local-resume-vars*)        
        ;; fires if applying the rules yields a non nil answer
        `((setq result (moss-eliza (read-fact conversation :input) 
                                   ',(getf arg-list :rules))))
        ;; display the answer
        )
      (if (member :test arg-list)
          `(,(cadr (member :test arg-list))))
      (if (member :yes arg-list)
          `((intersection *yes-answers* (read-fact conversation :input)
                          :test #'string-equal)))
      (if (or (member :otherwise arg-list)
              (member :always arg-list))
          '(t))
      
      ;;--- then deal with the rest of condition clause
      
      ;;--- intermediate actions
      ;; only to be used in a patterns clause
      (if (member :keep arg-list)
          ;`((replace-fact conversation :input
          `((replace-fact conversation ',(getf arg-list :keep)
                          (cdr (assoc ',(getf arg-list :keep) bindings)))))
      ;; replace INPUT with whatever is associated to :replace
      (if (member :replace arg-list)
          `((replace-fact conversation :input
                          ',(cadr (member :replace arg-list)))))
      (if (member :set-performative arg-list)
          `((replace-fact conversation :PERFORMATIVE 
                          ,(cadr (member :set-performative arg-list)))))
      ;; usually for bookkeeping (must occur after :keep before printing)
      (if (member :exec arg-list)
          `(,(cadr (member :exec arg-list))))
      ;; default function to print using the conversation output channel
	  ;; should appear after :exec in order to let :exec set up :answer for example
      (if (member :display-answer arg-list)
          `((if (web-active? conversation)
                (web-add-text conversation (get-fact :answer))
              (send conversation '=display-text 
                    (format nil "~S" (get-fact :answer))))))
      ; we could do without the display-answer function, e.g.
      ;`((send conversation '=display-text 
      ;        (format nil "~S" (get-fact :answer))))
      ;`((display-answer conversation))) ; JPB1011		  
		
      ;; should replace all printing options. Applies the formatting function that
      ;; produces a string, then uses =display-text to output data, taking care of
      ;; web servers if the WEB tag (marker) is set
      (if (member :format-answer arg-list)
          `((let ((text (funcall ,(cadr (member :format-answer arg-list))
                                 (read-fact conversation :answer)
                                 (web-active? conversation))))
              (vformat "~%; ++++format-answer option/ :web ~S~% :answer ~S~% text: ~S"
                (read-fact conversation :web)
                (read-fact conversation :answer)
                       text)
              ;; may not be useful, since =display-text checks the same situation
              (if (web-active? conversation)
                  (web-add-text conversation text)
                (send conversation '=display-text text)))))
      ;; like :format-answer but give access to the converation object so that we
      ;; can pass more arguments to the printing function
      (if (member :format-answer-full arg-list)
          `((let ((text (funcall ,(cadr (member :format-answer-full arg-list))
                                 (read-fact conversation :answer)
                                 (web-active? conversation)
                                 conversation))) ; added arg JPB 1602
              (vformat "~%; ++++format-answer-full args: :web ~S~% :answer ~S~
                        ~%conversation: ~S ~% text: ~S"
                       (read-fact conversation :web)
                       (read-fact conversation :answer)
                       conversation
                       text)
              ;; may not be useful, since =display-text checks the same situation
              (if (web-active? conversation)
                  (web-add-text conversation text)
                (send conversation '=display-text text)))))
      ;`((send conversation '=display-text
      ;     (funcall ,(cadr (member :format-answer arg-list))
      ;	            (read-fact conversation :answer)
      ;			   (read-fact conversation :web)))))  ; JPB1301
      ;; to be used after sending a message to other agents
      ;; kept for backward compatibility, does not apply to web exchanges
      (if (member :print-answer arg-list)
          `((funcall ,(cadr (member :print-answer arg-list))
                     (read-fact conversation :answer)))) ; JPB1010
      ;; to be used in conjunction with a :rules clause
      ;; don't see the purpose of this function
      (if (member :display-result arg-list)
          `((if (web-active? conversation)
                (web-add-text conversation result)
              (send conversation '=display-text result
                    ,@(if (member :erase arg-list) '(:erase t))))))
      ;`((send conversation '=display-text result
      ;       ,@(if (member :erase arg-list) '(:erase t)))))
      
      ;;--- transitions
      (if (and (member :failure arg-list) (not (member :sub-dialog arg-list)))
          `((throw :return (setq return-value (list :failure)))))
      (if (member :reset arg-list)
          ;; reset conversation
          `((restart-conversation conversation)
            ;; we must return the entry state of the dialog
            (setq return-value
                  (list :transition 
                        (car (HAS-MOSS-ENTRY-STATE 
                              (car (HAS-MOSS-DIALOG-HEADER conversation)))))))
        )
      (if (member :sub-dialog arg-list)
          `((throw :return 
              (setq return-value
                    '(:sub-dialog ,@(cdr (member :sub-dialog arg-list)))))))
      (if (and (member :success arg-list) (not (member :sub-dialog arg-list)))
          `((throw :return (setq return-value (list :success)))))
      (if (member :target arg-list)
          `((setq return-value (list :transition ,target))))
      ))))

#|
(make-state-resume-transitions '(:contains ("get" "obtain" "give" ":get") 
                                           :clean-data (input data)
                                           :target _Q-get-info))
((INTERSECTION '("get" "obtain" "give" ":get") (READ-FACT CONVERSATION :INPUT) 
               :TEST #'STRING-EQUAL)
 (SETQ RETURN-VALUE (LIST :TRANSITION _Q-GET-INFO)))

(make-state-resume-transitions '(:test (has-to-do conversation)
                                       :self =resume))
((HAS-TO-DO CONVERSATION) (SEND *SELF* '=RESUME CONVERSATION))
(make-state-resume-transitions '(:PATTERNS  (("get" "obtain" "give" ":get"))
                                           :set-performative (list :request)
                                           :target _Q-get-info))
|#
;;;=============================================================================
;;;                          BUILDING THE STATE
;;;=============================================================================

;;;------------------------------------------------------------------ MAKE-STATE

(defUn make-state (state-name &rest arg-list)
  "builds three things:
   - an instance of MOSS-STATE
   - an =execute method for this state
   - a =resume method for this state
   When *debug* is t then only prints info, does not build the corresponding objects.
Arguments:
   state-name: e.g. _pt-clean
   arg-list (rest): options to the defstate macro
Returns:
   the name of the state."
  (eval-when (:compile-toplevel :load-toplevel :execute)
    (proclaim `(special ,state-name))
    (export (list state-name)))
  
  ;; when debugging print header
  (if *debug* (format *debug-io* "~&;***** Debug: STATE: ~S" state-name))
  
  ;; made a MOSS instance of a MOSS-STATE, initialize it
  (unless *debug*
    (set state-name
         (%make-instance  
          'MOSS-state 
          `(has-moss-label ,(cadr (assoc :label arg-list)))
          ;; if no explanation, then repeat the label to have something to print
          `(has-moss-explanation ,(or (cadr (assoc :explanation arg-list))
                                      (cadr (assoc :label arg-list)))) ; JPB1607
          ))
    
    ;(break "MOSS::make-state: before checking entry-state")  
    
    ;; when an entry state, link it to the conversation header
    (when (assoc :entry-state arg-list)
      ;(trformat "make-state /conversation header: ~S ; entry state of: ~S"
      ;          (eval (cadr (assoc :entry-state arg-list))) (list (eval state-name)))
      (send (eval (cadr (assoc :entry-state arg-list))) 
            '=replace 'HAS-MOSS-ENTRY-STATE
            (list (eval state-name))))
    
    ;(break "MOSS::make-state: after checking entry-state")     
    
    ;; ???
    (when (assoc :process-state arg-list)
      ;(trformat "make-state /conversation header: ~S ; process state of: ~S"
      ;          (eval (cadr (assoc :process-state arg-list))) (list (eval state-name)))
      (send (eval (cadr (assoc :process-state arg-list))) '=replace 'HAS-MOSS-PROCESS-STATE
            (list (eval state-name))))
    )
  
  ;; synthetizing the =execute method (should always exist)
  (unless (assoc :manual-execute arg-list)
    (make-state-execute-method state-name arg-list))
  
  (make-state-resume-method state-name arg-list)
  ;(trformat "make-state /state: ~S value: ~S" state-name (eval state-name))
  state-name)
  
  #|
(in-package :moss)
(in-package :stevens)
(in-package :albert)
*package*
|#
;;;=============================================================================
;;;            FUNCTIONS FOR CONSTRUCTING PIECES OF STATE METHODS
;;;=============================================================================

;;;----------------------------------------------------------- MAKE-STATE-ACTION

#|
(defun make-state-action (action)
  "action contains the name of a method and arguments for *self*"
  (when action
    `((send *self* ,@action))))
|#

;;;----------------------------------------------------------- MAKE-STATE-ANSWER

(defUn make-state-answer (answer &optional no-erase)
  "if answer print it"
  (when answer
    `((send conversation '=display-text  ',answer 
            ,@(unless no-erase '(:erase t))))))
#|
(make-state-answer "youppee..." :no-erase)
((SEND CONVERSATION '=DISPLAY-TEXT "youppee..."))
(make-state-answer '("encore" "youppee..."))
((SEND CONVERSATION '=DISPLAY-TEXT '("encore" "youppee...") :ERASE T))
(moss::make-state '_test-state 
                  '(:label "Test state")
                  '(:explanation "no explanation")
                  '(:reset-conversation)
                  '(:answer "youppee..."))
|#
;;;-------------------------------------------------- MAKE-STATE-ANSWER-ANALYSIS

(defUn make-state-answer-analysis (tag state-name)
  "if we need a complex detailed analysis we call =answer-analysis"
  `((send *self* (intern "=ANSWER-ANALYSIS" *package*) ; defined in agent's package
          conversation (read-fact conversation ,tag))
    ;; the returned answer should be :abort or a transition state
    (if (eql *answer* :abort)
        (throw :dialog-error 
          (format nil "abort from special analysis in state ~S" ,state-name))
      (throw :return (setq return-value *answer*)))))

;;;-------------------------------------------------------------- MAKE-STATE-ASK

(defUn make-state-ask (ask prompt)
  "ask user"
  (declare (ignore prompt))
  (when ask
    ;; do not forget that welcome can contain arguments
    `((send conversation '=display-text (format nil ,@ask))
      ;; make answer panel active
      '(:resume)
      )))

;;;--------------------------------------------------------- MAKE-STATE-ASK-TCW

#|
(defun make-state-ask-tcw (ask-tcw)
  "ask user for choice"
  (when ask-tcw
    ;; asking text-choice-why question, put answer into com bin
    `((ask-tcw conversation ,@ask-tcw)
      (setq return-value '(:resume)))))
|#

;;;--------------------------------------------------------- MAKE-STATE-ASK-TSW

#|
(defun make-state-ask-tsw (ask-tsw)
  "ask user for multiple choice"
  (when ask-tsw
    ;; asking text-select-why question, put answer into com bin
    `((setf (has-to-do conversation) 
        (list (ask-tsw conversation ,@ask-tsw)))
      (setq return-value '(:resume)))))
|#
;;;-------------------------------------------------- MAKE-STATE-CLEAR-ALL-FACTS

(defun make-state-clear-all-facts ()
  "clears the FACT base in the conversation object"
  `((%%set-value conversation '$FCT nil (symbol-value (intern "*CONTEXT*")))))

;;;------------------------------------------------------- MAKE-STATE-CLEAR-FACT

(defun make-state-clear-fact (item)
  "resets a specific item in the conversation FACT base"
  (when item
    `((replace-fact conversation ',item nil))))

;;;------------------------------------------------------ MAKE-STATE-CLEAR-FACTS

(defun make-state-clear-facts (&rest item-list)
  "resets several items in the conversation fact base"
  (when item-list
    `((mapc #'(lambda (xx) (replace-fact conversation xx nil)) ',item-list))))
	
;;;----------------------------------------------------- MAKE-STATE-DELAY-ANSWER

(defun make-state-delay-answer (&optional (item :inc))
   "handles the delay-ref-count parameter in conversation/FACTS"
 (case item
   (:inc '((web-set-delay conversation)))
   (:dec '((web-dec-delay conversation)))
   (:clear '((web-clear-delay conversation)))))

;;;------------------------------------------------------------ MAKE-STATE-ELIZA

(defUn make-state-eliza (ok)
  "switches to an ELIZA dialog, using the rules found in the execution package."
  (when OK
    `(      
      ;(format *debug-io* "~&+++ make-state-eliza; package: ~S" *package*)
      ;; check if the application has a set of ELIZA rules
      (let ((eliza-rules (intern "*ELIZA-RULES*" *package*)))
        ;; if there are no local rules, use the MOSS set
        (unless (boundp eliza-rules)
          (setq eliza-rules 'moss::*eliza-rules*))
        ;(trformat "ELIZA /data: ~S" data)
        (send conversation '=display-text 
              (moss-eliza (read-fact conversation :input)
                          (symbol-value eliza-rules)) :erase t)
        ;(format *debug-io* "~&+++ make-state-eliza; package: ~S" *package*)
        ))))

#|
(make-state-eliza t)
((LET ((MOSS::ELIZA-RULES (INTERN "*ELIZA-RULES*" *PACKAGE*)))
   (UNLESS (BOUNDP MOSS::ELIZA-RULES) (SETQ MOSS::ELIZA-RULES 'MOSS::*ELIZA-RULES*))
   (SEND MOSS::CONVERSATION
         '=DISPLAY-TEXT
         (MOSS::MOSS-ELIZA (READ-FACT MOSS::CONVERSATION :INPUT)
                           (SYMBOL-VALUE MOSS::ELIZA-RULES))
         :ERASE
         T)))
|#
;;;---------------------------------------------------- MAKE-STATE-EXECUTE-ARGS?

(defUn make-state-execute-args? (arg-list)
  "execute a single expression in the =execute method and proceed."
  ;; return the list of arguments 
  (or (assoc :text arg-list)
      (assoc :question arg-list)
      (assoc :question-no-erase arg-list)
      (assoc :reset-conversation arg-list)))

;;;-------------------------------------------- MAKE-STATE-EXECUTE-PRECONDITIONS

(defUn make-state-execute-preconditions (arg-list)
  "execute Lisp expression in the =execute method (escape mechanism)."
  arg-list)

;;;--------------------------------------------------------- MAKE-STATE-QUESTION
;;; adapted to web JPB1301
;;;********** seems to be wrong when arg is a single value...

(defUn make-state-question (question)
  "if question and web save it otherwise ask it, erasing-screen
Arguments:
   question : a string or list of strings"
  (when question
    `((if (web-active? conversation)
	      (web-add-text conversation ',@question)
		(send conversation '=display-text ',@question :erase t)))))
		
#|
(defUn make-state-question (question)
  "if question ask it, erasing-screen
Arguments:
   question : a string or list of strings
   no-erase (opt): if t does not erase assistant pane"
  (when question
    `((send conversation '=display-text ',@question :erase t))
    ))
|#
#|
(make-state-question '("Hello?" "How are you?"))
((SEND CONVERSATION '=DISPLAY-TEXT '("Hello?" "how are you?")))
(make-state-question "Hello?")
((SEND CONVERSATION '=DISPLAY-TEXT '"Hello?" :ERASE T))
|#
;;;------------------------------------------------ MAKE-STATE-QUESTION-NO-ERASE

(defUn make-state-question-no-erase (question)
  "if question and web save it, otherwise ask it but do not erase screen.
Arguments:
   question : a string or list of strings"
  (when question
    `((if (web-active? conversation)
	     (web-add-text conversation ',@question)
	   (send conversation '=display-text ',@question)))))

#|
(make-state-question-no-erase "Autre chose?")
((IF (GET-FACT :WEB)
     (SET-FACT :QUESTION (QUOTE . "Autre chose?"))
   (SEND CONVERSATION '=DISPLAY-TEXT (QUOTE . "Autre chose?"))))
|#
;;;-------------------------------------------------- MAKE-STATE-QUESTION-THERE?

(defUn make-state-question-there? (arg-list)
  "test if the arg-list contains a question.
Arguments:
   arg-list: options of the defstate macro."
  (or
   (assoc :question arg-list)
   (assoc :question-no-erase arg-list)
   (assoc :text-exec arg-list)  ; JPB 1409
   (assoc :eliza arg-list)
   (assoc :send-message arg-list)
   ;; if we ask the state to wait for an external input
   (assoc :wait arg-list)
   ))

;;;----------------------------------------------- MAKE-STATE-RESET-CONVERSATION

(defUn make-state-reset-conversation (ok)
  "reset does several things:
     - clean the FACTS
     - puts a new goal (action pattern) into the GOAL slot of conversation"
  (when OK
    `((let (#|
(action (car (HAS-MOSS-ACTION
                          (car (HAS-MOSS-DIALOG-HEADER conversation)))))
|#)
        ;; reset facts
        (setf (HAS-MOSS-FACTS conversation) nil)
        ;; remove task and task list
        (send conversation '=delete-all-successors 'HAS-MOSS-TASK)
        (send conversation '=delete-all-successors 'HAS-MOSS-TASK-LIST)
        ;; set a new goal specified by the type of action in the dialog header
        #|
(if action
            (send conversation '=replace 'HAS-MOSS-GOAL
                  (list (send action '=create-pattern)))
          ;; otherwise should delete everything
          (progn
            (send conversation '=get 'HAS-MOSS-GOAL)
            ;; seems to indicate that we can have several goals at the same time...
            (send conversation '=delete-sp 'HAS-MOSS-GOAL *answer*))
          )
|#))))

;;;----------------------------------------------------- MAKE-STATE-SEND-MESSAGE
;;; When sending a message, the dialog can be run from the master window or from
;;; a browser page. In the first case the answer of the message should be a string
;;; or a list of strings, in the second case the whole process was started by a
;;; get-answer function and the result should be a list with the ref to the object
;;; that was modified (to be able to refresh the web page) and a list of messages.
;;; The precise situation is indicated by the $WEBT property of the conversation
;;; object, and is tested by the web-active? service function. We thus add to the
;;; message arguments a new parameter :pa-dialog that is true if we have a dialog
;;; from the master window and false if the dialog is run from the web.


(defUn make-state-send-message (send-message)
  "creates an internal message for sending a request to other agents (staff?) ~
   the content of the internal message is part of a message free-style with ~
   the content of INPUT area of the fact base."
  (when send-message
    `((let* ((agent-id (car (HAS-MOSS-AGENT conversation)))
             (from (or ,(cadr (member :from send-message))
                       (omas::key agent-id)))
             (to ,(or (cadr (member :to send-message)) :ALL))
             (action ,(or (cadr (member :action send-message)) :explain))
             (type ,(or (cadr (member :type send-message)) :request))
             (data ,(cadr (member :args send-message)))
             (pa-dialog (not (web-active? conversation)))
             ;(args   ; adding a tag to track sending context
             ;      (list (cons (cons :pa-dialog pa-dialog) (car data)))) ; JPB0902, JPB1409
             (agent-key (omas::key agent-id))
             (timeout ,(cadr (member :timeout send-message)))
             (strategy ,(cadr (member :strategy send-message)))
             (protocol ,(cadr (member :protocol send-message)))
             (repeat-count ,(or (cadr (member :repeat-count send-message)) 0))
             message content)
        
        ;; make a list to be given as a task description list in the internal 
        ;; :send skill
        (setq content
              (append (list :from from :to to :action action ;:args args ; JPB2101
                            :type type)
                      (if protocol (list :protocol protocol))
                      (if strategy (list :strategy strategy))
                      (if timeout (list :timeout timeout))
                      (if repeat-count (list :repeat-count repeat-count))
                      ))
        ;; we must create a task-id in case we have a request, so that the system
        ;; knows where to send the answer and to which conversation
        (setq message
              (omas::message-make 
               :date (get-universal-time) 
               :type :internal 
               :from agent-key :to agent-key
               :action :send-message
               :task-id (omas::create-task-id) 
               :args (list content conversation)))
        (moss::vformat "message: ~S" message)
        ;; save it into the pending-task-id slot of the master bins
        ;; this should not be necessary (only for trace)
        ;(omas::assistant-display-pending-requests agent)
        ;; put message directly into agenda; if it is put into the mailbox, since the
        ;; action is :send-message it is transferred to the agenda readily; thus,
        ;; this saves a delay
        (omas::agent-add agent-id message :agenda)
        ;; the answer to an internal message is posted into the
        ;; to-do slot of the agent as a string. This wakes up the
        ;; =resume method that finds the answer in the corresponding
        ;; slot of the conversation object
        ))))


#|
(MAKE-STATE-SEND-MESSAGE
         '(:TO :ANATOLE-NEWS :SELF PA_ANATOLE :ACTION :ADD-CATEGORY :ARGS
               `(((:DATA . "test") (:LANGUAGE . :FR)))))
((LET* ((AGENT-ID (CAR (HAS-MOSS-AGENT CONVERSATION)))
        (FROM (OR NIL (OMAS::KEY AGENT-ID)))
        (TO :ANATOLE-NEWS)
        (ACTION :ADD-CATEGORY)
        (TYPE :REQUEST)
        (DATA `(((:DATA . "test") (:LANGUAGE . :FR))))
        (PA-DIALOG (NOT (WEB-ACTIVE? CONVERSATION)))
        (ARGS (LIST (CONS (CONS :PA-DIALOG PA-DIALOG)) (CAR DATA))))
   (AGENT-KEY (OMAS::KEY AGENT-ID))
   (TIMEOUT NIL)
   (STRATEGY NIL)
   (PROTOCOL NIL)
   (REPEAT-COUNT 0)
   MESSAGE
   CONTENT)
 (SETQ CONTENT
       (APPEND (LIST :FROM FROM :TO TO :ACTION ACTION :ARGS ARGS :TYPE TYPE)
               (IF PROTOCOL (LIST :PROTOCOL PROTOCOL)) (IF STRATEGY (LIST :STRATEGY STRATEGY))
               (IF TIMEOUT (LIST :TIMEOUT TIMEOUT)) (IF REPEAT-COUNT (LIST :REPEAT-COUNT REPEAT-COUNT))))
 (SETQ MESSAGE
       (OMAS::MESSAGE-MAKE :DATE (GET-UNIVERSAL-TIME) :TYPE :INTERNAL :FROM AGENT-KEY :TO AGENT-KEY
                           :ACTION :SEND-MESSAGE :TASK-ID (OMAS::CREATE-TASK-ID) :ARGS CONTENT))
 (VFORMAT "message: ~S" MESSAGE) (OMAS::AGENT-ADD AGENT-ID MESSAGE :AGENDA))
|#
;;;-------------------------------------------------- MAKE-STATE-SET-ANSWER-TYPE

(defun make-state-set-answer-type (answer-type)
  "puts the specified performatives into the fact-base"
  (when answer-type 
    ;(trformat "make-state-set-performative /answer-type: ~S" answer-type)
    `((replace-fact conversation :answer-type ',answer-type)))
  )

;;;------------------------------------------------- MAKE-STATE-SET-PERFORMATIVE

(defUn make-state-set-performative (answer-type)
  "puts the specified performatives into the state context object"
  (when answer-type 
    ;(trformat "make-state-set-performative /answer-type: ~S" answer-type)
    `((replace-fact conversation :performative) ',answer-type))
  )

;;;------------------------------------------- MAKE-STATE-PROCESS-SUBDIALOG-ARGS

(defUn make-state-process-subdialog-args (arg-list)
  (cond
   ((null arg-list) nil)
   ((not (member (car arg-list) '(:failure :success))) nil)
   ((eql (car arg-list) :failure)
    (append (list :failure (eval (cadr arg-list)))
            (make-state-process-subdialog-args (cddr arg-list))))
   ((eql (car arg-list) :success)
    (append (list :success (eval (cadr arg-list)))
            (make-state-process-subdialog-args (cddr arg-list))))))
#|
? (make-state-process-subdialog-args '(:failure _q-more?))
(:FAILURE $QSTE.3)
? (make-state-process-subdialog-args '(:failure _q-more? :success _q-process))
(:FAILURE $QSTE.3 :SUCCESS $QSTE.4)
|#
;;;------------------------------------------------------------- MAKE-STATE-TEXT
;;; adapting to web exchange JPB1301

(defUn make-state-text (text &optional no-erase)
  "if text and web save it otherwise print it"
  (when text
    `((if (web-active? conversation)
	     (web-add-text conversation ',text) 
	   (send conversation '=display-text  ',text  ; JPB1104 removing quote
            ,@(if no-erase '(:erase nil) '(:erase t)))))))

#|
(make-state-text "tout va bien")
((IF (GET-FACT :WEB)
     (SET-FACT :TEXT '"tout va bien")
   (SEND CONVERSATION '=DISPLAY-TEXT '"tout va bien" :ERASE T)))
|#
#|
(defUn make-state-text (text &optional no-erase)
  "if text print it"
  (when text
    `((send conversation '=display-text  ',text  ; JPB1104 removing quote
            ,@(if no-erase '(:erase nil) '(:erase t))))))
|#
;;;-------------------------------------------------------- MAKE-STATE-TEXT-EXEC
;;; adapting to web exchange JPB1301

(defUn make-state-text-exec (text &optional no-erase)
  "if text print it"
  (when text
    `((if (web-active? conversation)
	     (web-add-text conversation ,text)
	   (send conversation '=display-text  ,text 
            ,@(if no-erase '(:erase nil) '(:erase t)))))))

#|
(make-state-text-exec '(format nil "~S" "Bonjour"))
((IF (GET-FACT :WEB)
     (SET-FACT :TEXT (FORMAT NIL "~S" "Bonjour"))
   (SEND CONVERSATION '=DISPLAY-TEXT (FORMAT NIL "~S" "Bonjoue") :ERASE T)))
|#
#|
(defUn make-state-text-exec (text &optional no-erase)
  "if text print it"
  (when text
    `((send conversation '=display-text  ,text 
            ,@(if no-erase '(:erase nil) '(:erase t))))))
|#


;;;================================================================================
;;;                       
;;;               Peter Norvig ELIZA Pattern matcher
;;;
;;;================================================================================

;;;================================================================== DEFELIZARULES
;;; the following macro allows using a simplified format for the rules and produces
;;; the standard Norvig format. An ELIZA rule has a single pattern and several
;;; possible conclusions.

(defmacro defelizarules (&rest rules)
  "takes a set of simplified rules and expands it to standard format"
  `(mapcar #'(lambda (xx) (cons (make-pattern (car xx)) (cdr xx))) ',rules))

#|
(defelizarules
    (((* "were" "you" ?y)
      "Perhaps I was ?y"
      "What do you think ?"
      "What if I had been ?y ?")
     ((* "I" "can't" ?y)
      "Maybe you could ?y now"
      "What if you could ?y ?")
     ((* "I" "feel" ?y)
      "Do you often feel ?y ?")))
((((?* :*) "were" "you" (?* ?Y)) "Perhaps I was ?y" "What do you think ?" "What if I had been ?y ?")
 (((?* :*) "I" "can't" (?* ?Y)) "Maybe you could ?y now" "What if you could ?y ?")
 (((?* :*) "I" "feel" (?* ?Y)) "Do you often feel ?y ?"))
|#
;;;=========================================================== DEFCOMPLEXELIZARULES
;;; same but allow expressions to be evaluated, e.g.
;;; (make-complex-pattern 
;;;     '(<> (* "were" < ?y) (symbol-name (omas::name albert::PA_ALBERT))))

(defmacro defcomplexelizarules (&rest rules)
  "takes a set of simplified rules and expands it to standard format"
  `(mapcar #'(lambda (xx) (cons (make-complex-pattern (car xx))
                                (mapcar #'make-complex-action (cdr xx))))
     ',rules))

;;;================================================================================

;;; Set up special space
; (unless (find-package :matcher)(create-package :matcher))
; (in-package :matcher)

;;;
(defConstant fail nil "Indicates pat-match failure")
(defConstant no-bindings '((t . t)) "Indicates pat-match success, with no variables.")

;;;-------------------------------------------------------------------- GET-BINDING

(defUn get-binding (var bindings)
  "Find a (variable . value) pair in a binding list."
  (assoc var bindings))

;;;-------------------------------------------------------------------- BINDING-VAL

(defUn binding-val (binding)
  "Get the value part of a single binding."
  (cdr binding))

;;;------------------------------------------------------------------------- LOOKUP
;;; adding inline directive

(defUn lookup (var bindings)
  "Get the value part (for var) from a binding list."
  (declare (inline get-binding binding-val)) ; jpb1105
  (binding-val (get-binding var bindings)))

;;;---------------------------------------------------------------- EXTEND-BINDINGS

(defUn extend-bindings (var val bindings)
  "Add a (var . value) pair to a binding list."
  (cons (cons var val)
        ;; Once we add a "real" binding,
        ;; we can get rid of the dummy no-bindings
        (if (and (eq bindings no-bindings))
            nil
          bindings)))

;;;------------------------------------------------------------ MAKE-COMPLEX-ACTION
;;; this function allows composing strings statically when defining ELIZA rules
;;; the answer is either a string or a list like
;;; (<> "Bonjour, je suis ~A!" (omas::key (omas::agent-being-loaded *omas*))

(defun make-complex-action (answer)
  "allows format expressions as possible ELIZA answers, which is quite limited."
  (cond
   ;; noop if a string
   ((stringp answer) answer)
   ((and (listp answer)(eql (car answer) '<>))
    (apply #'format nil (cadr answer)(mapcar #'eval (cddr answer))))
   ;; anything else is an error
   (t "?")))
  
;;;----------------------------------------------------------- MAKE-COMPLEX-PATTERN
;;; this function allows inserting expressions to evaluate into ELIZA rules. 
;;; Evaluating each argument must return a list of strings.

(defun make-complex-pattern (pattern)
  (if (eql (car pattern) '<>)
      (let ((pat (cadr pattern))(arg-list (cddr pattern)))
        (reduce 
         #'append
         (mapcar #'(lambda (xx)
                     (cond ((skip-symbol-p xx) '((?* :*)))
                           ((variable-p xx) `((?* ,xx)))
                           ((stringp xx) (list xx))
                           ((listp xx) (list xx))
                           ((eql xx '<) (eval (pop arg-list)))
                           (t (error "bad pattern format: ~S" pattern))))
           pat)))
    (make-pattern pattern)))

#|
(make-complex-pattern '(<> (* "were" < ?y) '( "me" "albert")))
((?* :*) "were" "albert" (?* ?Y))
(make-complex-pattern '(* "were" "you" ?y))
((?* :*) "were" "you" (?* ?Y))
(make-complex-pattern '(<> (* "were" < ?y) 
                           (list (symbol-name (omas::name albert::PA_ALBERT)))))
((?* :*) "were" "PA_ALBERT" (?* ?Y))
|#
;;;------------------------------------------------------------------- MAKE-PATTERN
;;; function to allow simpler pattersn
;;;  (* "je" "vous" "ai" "compris" ?x)
;;; is transformed into
;;;  ((?* :*) "je" "vous" "ai" "compris" (?* ?x))
;;; the reason for not simplifying further the input to
;;;  (* "je vous ai compris" ?x)
;;; rather than translating it
;;; is related to Japanese and Chinese segmentation. Thus, we already segment the 
;;; pattern
;;; Former patterns are left unchanged by make-pattern

(defUn make-pattern (pattern)
    (mapcar #'(lambda (xx) 
                (cond ((skip-symbol-p xx) '(?* :*))
                      ((variable-p xx) `(?* ,xx))
                      ((stringp xx) xx)
                      ((listp xx) xx)
                      (t (error "bad pattern format: ~S" pattern))))
      pattern))

#|
? (make-pattern '(* "j" "ai" "compris" "que" "vous" ?x))
((?* :*) "j" "ai" "compris" "que" "vous" (?* ?X))

? (make-pattern '((?* ?Y) "j" "ai" "compris" "que" "vous" (?* ?X)))
((?* ?Y) "j" "ai" "compris" "que" "vous" (?* ?X))

? (make-pattern '("alors" *))
("alors" (?* :*))

? (make-pattern '(RELATIONSHIP *))
Error: bad pattern format: (RELATIONSHIP *)
;; we must have strings not symbols except for * and variables ?x
|#

;;;----------------------------------------------------------------- MATCH-VARIABLE
;;; adding :*

(defUn match-variable (var input bindings)
  "Does VAR match input? Uses or updates and returns bindings."
  (let ((binding (get-binding var bindings)))    
    (cond ((eql var :*) bindings)
          ((null binding) (extend-bindings var input bindings))
          ((same-p input (binding-val binding)) bindings)
          (t fail))))

;;;---------------------------------------------------------------------- PAT-MATCH

(defUn pat-match (pattern input &optional (bindings no-bindings))
  "Match pattern against input in the context of bindings."
  (cond
   ((eq bindings fail) fail)
   ((variable-p pattern)
    (match-variable pattern input bindings))
   ;((prog1 nil (print `(+++ pattern ,pattern input ,input))))
   ((or (and (stringp pattern)(stringp input)(string-equal pattern input))
        (eql pattern input)) bindings)
   ((segment-pattern-p pattern)
    (segment-match pattern input bindings))
   ((and (consp pattern)(consp input))
    (pat-match (rest pattern) (rest input)
               (pat-match (first pattern) (first input) bindings)))
   (t fail)))

#|
;;; tests
(pat-match '((?* ?x) "I" "want" (?* ?y)) '("Now" "i" "want" "sugar"))
(pat-match '((?* ?x) "I" "want" (?* ?y)) '("I" "want" "sugar"))
(pat-match '((?* ?x) "I" "want" (?* ?y)) '("I" "want"))
(pat-match '((?* ?x) "I" "want" (?* ?y)) '("want"))
(pat-match '((?* ?x) "I" (?* ?y)) '("I"))
(pat-match '((?* ?x) "I" (?* ?y)) '("i"))
(pat-match '((?* ?x) "I" (?* ?y)) '("today" "i" "TODAY"))
(pat-match '((?* ?x) "I" (?* ?x)) '("today" "i" "TODAY"))
(pat-match '((?* ?x) "I" (?* ?x)) '("today" "i" "not" "TODAY"))
|#
;;;------------------------------------------------------------------------- SAME-P

(defUn same-p (x y)
  "allows to compare strings when needed."
  (or (and (stringp x)(stringp y)(string-equal x y))
      (and (listp x)(listp y) (same-p (car x)(car y)) (same-p (cdr x)(cdr y)))
      (equal x y)))

#|
Bug:
(pat-match '((?* ?x) "I" (?* ?x)) '("today" "morning" "i" "TODAY" "afternoon"))
((?X "today" "morning"))
((?X "today" "morning"))
|#
;;;-------------------------------------------------------------- SEGMENT-PATTERN-P

(defUn segment-pattern-p (pattern)
  "Is this a segment matching pattern: ((?* var) . pat)"
  (declare (special *pattern-marker*))
  ;; it is necessary to compare marker independently from the packages where 
  ;; they have been defined 
  ;; since the marker is a symbol we should compare symbol names
  (and (consp pattern)
       (starts-with (first pattern) *pattern-marker*)))

;;;------------------------------------------------------------------ SEGMENT-MATCH
;;; This function does not work for a pattern like ((moss::?* ?x)(moss::?* :*))
;;; because it assumes that the value following the variable is a string
;;; This construct is however illegal since the first part is a segment variable


(defUn segment-match (pattern input bindings &optional (start 0))
  "Match the segment pattern ((?* var) . pat) against input."
  (let ((var (second (first pattern)))
        (pat (rest pattern)))
    (if (null pat)
        ;; if pat is exhausted, match variable against input
        (match-variable var input bindings)
      
      ;; we assume that pat starts with a constant
      ;; in other words, a pattern cannot have 2 consecutive vars
      
      ;; if pat is not nil try to locate the first element of pat in the input
      (let ((pos (position (first pat) input :start start :test #'string-equal)))
        (if (null pos)
            ;; if it does not appear, then we fail to match pat to input
            fail
          ;; if it appears, then we try to match pat with the input, starting at
          ;; the anchor position
          ;; set the bindings so that the first variable matches the beginning
          ;; of the input
          (let ((b2 (pat-match
                     pat (subseq input pos)
                     (match-variable var (subseq input 0 pos) bindings))))
            ;; if this match failed, try another moving 1 position down the input
            (if (eq b2 fail)
                (segment-match pattern input bindings (1+ pos))
              ;; otherwise return the bindings
              b2)))))))

;;;------------------------------------------------------------------ SKIP-SYMBOL-P
;;; when we do not need to store the sentence fragment

(defUn skip-symbol-p (x)
  "is x the skip symbol * ?"
  (and (symbolp x)(equal (char (symbol-name x) 0) #\*)))

;;;-------------------------------------------------------------------- STARTS-WITH

(defUn starts-with (subpattern mark)
  "Do we have a cons starting with a ?* atom?"
  (and (consp subpattern)
       (symbolp (car subpattern))
       (equal (symbol-name (car subpattern)) mark)))
#|
?(starts-with '(?* ?x) "?*")
T
?(starts-with '((?* ?x)) "?*")
nil
|#
;;;--------------------------------------------------------------------- VARIABLE-P

(defUn variable-p (x)
  "Is x a variable (a symbol beginning with '?')?"
  (and (symbolp x)(equal (char (symbol-name x) 0) #\?)))

;;;================================================================================
;;;                      ELIZA according to Peter NORVIG
;;;================================================================================

(defUn rule-pattern (rule) (car rule))
(defUn rule-responses (rule)(cdr rule))

(defUn eliza ()
  (loop
    ;(print 'eliza>)
    (format t "~A"
      (use-eliza-rules 
       (%get-words-from-text (read-from-listener) '(#\space #\, #\' #\. #\?))
       ))))

(defUn moss-eliza (input eliza-rules)
  "a version of ELIZA taylored for moss.
Arguments: 
  input: a list of atoms as a sentence
  eliza-rules: the set of rules defined for ELIZA smalltalk
Return:
   a string to be printed."
  (some #'(lambda (rule)
            (let ((result (pat-match (make-pattern (rule-pattern rule)) input)))
              (if result
                  (upgrade-answer (switch-viewpoint result)
                                  (random-elt (rule-responses rule))))))
        eliza-rules))

(defUn use-eliza-rules (input)
  (declare (special *eliza-rules*))
  (some #'(lambda (rule)
            (let ((result (pat-match (make-pattern (rule-pattern rule)) input)))
              (if result
                  (upgrade-answer (switch-viewpoint result)
                                  (random-elt (rule-responses rule))))))
        *eliza-rules*))

(defUn switch-viewpoint (words)
  (case *language* 
    (:fr
     (sublis '(("Je" . "vous")("vous" . "je")("me" . "vous")("suis" . "Ítes")
               ("mon" . "votre")("ma" . "votre") ("votre" . "mon")
               ("moi" . "vous")) 
             words
             :test #'(lambda (xx yy) 
                       (or (and (stringp xx)(stringp yy)(string-equal xx yy))
                           (equal xx yy)))))
    (:en
     (sublis '(("I" . "you")("you" . "I")("me" . "you")("am" . "are")("my" . "your")
               ("your" . "my")("our" . "your")) 
             words
             :test #'(lambda (xx yy) 
                       (or (and (stringp xx)(stringp yy)(string-equal xx yy))
                           (equal xx yy)))))
    (otherwise
     words)))

(defUn replace-substrings (str old-substr new-substr)
  (let ((index (string-find str old-substr)))
    (if (> index 0)
        (format nil "~a~a~a" 
          (subseq str 0 (- index 1) )
          new-substr 
          (replace-substrings (subseq str (+ (- index 1) (length old-substr)))
                              old-substr new-substr)
          )
      str)))

(defUn string-find (string phrase)
  (if (search phrase string)
      (+ (search phrase string) 1)
    0))

(defUn upgrade-answer (alist answer)
  "takes something like ((?Y #\"sugar#\") (?X #\"Now#\" #\"you#\") (?Z)) and ~
   replaces occurences of the variables in the answer string.
Arguments:
   alist: variables and values
   answer: answer pattern
Return:
   upgraded answer string."
  (let (new-value (result answer))
    (dolist (pair alist)
      (unless (equal pair '(T . T))
        ;; for each pair replace value
        (setq new-value (if (cdr pair)(format nil "~{~A~^ ~}" (cdr pair)) ""))
        (setq result 
              (replace-substrings 
               result  (string-downcase (format nil "~A" (car pair))) new-value))))
    result))

(defUn flatten (the-list)
  (mappend #'mklist the-list))

(defUn mklist (x)
  (if (listp x) x (list x)))

(defUn mappend (fn the-list)
  (apply #'append (mapcar fn the-list)))

(defUn random-elt (choices)
  (elt choices (random (length choices))))

;;;================================================================================
;;;                        Additional Service Functions
;;;================================================================================

;;;-------------------------------------------------------------- CLEAN-PATTERNS
;;; used by find-objects

(defUn clean-patterns (patterns used-patterns)
  "removes patterns in patterns that belong to used-patterns.
Arguments:
   patterns: list of patterns to clean
   used-patterns: patterns that have been tested already
Return:
   a list of patterns to be tested."
  (let (result)
    (dolist (item patterns)
      (unless (assoc (car item) used-patterns :test #'string=)
        (push item result)))
    (reverse result)))

;;;------------------------------------------------------ DETERMINE-PERFORMATIVE
;;; used once in the old version of crawler when checking for an interruption

(defUn determine-performative (conversation &key data)
  "tries to determine the performative corresponding to the data input of the ~
   current conversation.
Arguments:
   conversation: current conversation
   data (key): a list of string words, if present conversation is ignored
Return:
   one of the keywords :request :assert :command"
  (setq data (or data (read-fact conversation :input)))
  (cond
   ((process-patterns data *assert-patterns*) :assert)
   ((process-patterns data *request-patterns*) :request)
   ((process-patterns data *command-patterns*) :command)
   (t :answer)))

#|
? (moss::determine-performative 
   '$CVSE.1
   :data '("how" "do" "you" "do" "today" "?"))
:REQUEST
? (moss::determine-performative 
   '$CVSE
   :data '("please" "note" "this" "fact"))
:ASSERT
? (moss::determine-performative 
   '$CVSE
   :data '("Dominique" "s" "address"))
:ANSWER
? (moss::determine-performative 
   '$CVSE
   :data '("send" "a " "mail" "to" "John" "now"))
:COMMAND
|#
;;;--------------------------------------------------------- FILL-ANSWER-PATTERN
;;; unused but could be handy in dialogs

(defUn fill-answer-pattern (pattern object-id-list)
  "considers each object of the object list and returns a list of filled paterns.
Arguments:
   pattern: an answer pattern
   object-id-list: a list of object identifiers
Return:
   a list of filled patterns."
  (let (result)
    ;; build a list for each object
    (dolist (obj-id object-id-list)
      (push (fill-answer-pattern-for-one-level pattern obj-id) result))
    (reverse result)))

;;;------------------------------------------- FILL-ANSWER-PATTERN-FOR-ONE-LEVEL

(defUn fill-answer-pattern-for-one-level (pattern obj-id)
  "fills a pattern with the info for one object. Calls fill-pattern recursively ~
   if needed.
Arguments:
   pattern: answer pattern
   obj-id: id of a single object
Return:
   a list representing the pattern filled with the info from the object."
  (when (and (%pdm? obj-id) 
             (%is-a?
              (car (%type-of obj-id))
              (%%get-id (car pattern) :class)))
    (let ((saved-pattern pattern)
          prop-id successors value lres)
      ;; first element must be a class name
      ;; instead of (car pattern) should record class-name of obj-id
      (push (car (send (car (%type-of obj-id)) '=get-name)) lres)
      (pop pattern)
      ;; if null pattern, call =summary
      (unless pattern
        (push (send obj-id '=summary) lres)
        (return-from fill-answer-pattern-for-one-level (reverse lres)))
      ;; then take care of properties, e.g. (("name")("first name") 
      ;;                                     ("telephone" 
      ;;                                         ("cell phone")))
      (loop
        (dformat :dialog 0
                  "~%; fill-answer-pattern-for-one-level/ obj-id: ~S pattern:~% ~S"
          obj-id pattern)
        (unless pattern (return nil))
        ;; check if attribute
        (setq prop-id (%get-property-id-from-ref obj-id (caar pattern)))
        (cond
         ((%is-attribute? prop-id)
          (setq value (send obj-id '=get-id prop-id))
          ;; fill in value only if non nil
          (when value
            ;(push (append (pop pattern) value) lres)
            (push (append (car pattern) value) lres) ;jpb0910
            )
          (pop pattern)) ; jpb0910
         ;; check now relation cadr should be a list, e.g. ("telephone" 
         ;;                                                 ("cell phone"))
         ((and (%is-relation? prop-id) (cadar pattern)(listp (cadar pattern)))
          (setq successors (send obj-id '=get-id prop-id))
          (when successors
            (setq value (remove nil (fill-answer-pattern (cadar pattern) successors)))
            (when value
              (vformat "fill-answer-pattern-for-one-level/value: ~S; pattern: ~S" 
                       value pattern)
              (push (cons (caar pattern) value) lres)
              ))
          
          (pop pattern)) ; JPB1010
         
         ;; when pattern has an isolated relation, e.g. ("telephone")
         ;; then if there are values associated to this property, we summarize them
         ((and (%is-relation? prop-id) (null (cdar pattern)))
          (setq successors (send obj-id '=get-id prop-id))
          (setq value (remove nil (broadcast successors '=summary)))
          (when value
            (push (cons (caar pattern) (mapcar #'car value)) lres))
          (pop pattern))
         
         ;; otherwise bad format
         (t (warn "bad pattern format ~S" saved-pattern)
            (return-from fill-answer-pattern-for-one-level nil))))
      (reverse lres))))

#|
? (in-package :sa-address )
#<Package "SA-ADDRESS">
? (moss::fill-answer-pattern '("person") '($E-PERSON.1))
(("person" "Barthes: Jean-Paul"))

? (moss::fill-answer-pattern '("person" ("name")) '(sa-address::$E-PERSON.1))
(("person" ("name" "Barthes")))

? (moss::fill-answer-pattern '("person" ("name") ("first-name")) '(sa-address::$E-PERSON.1))
(("person" ("name" "Barthes") ("first-name" "Jean-Paul")))

? (moss::fill-answer-pattern-for-one-level '("person" ("phone" ("telephone"))) '$E-PERSON.1)
("person"
 ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
  ("cell phone" "(0)6 80 45 32 67")))

? (moss::fill-answer-pattern '("person" ("phone" ("telephone"))) '($E-PERSON.1))
(("person"
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67"))))

? (moss::fill-answer-pattern '("person" ("name") ("first name")("phone" ("telephone"))) '($E-PERSON.1))
(("person" ("name" "Barthes") ("first name" "Jean-Paul")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67"))))

? (moss::fill-answer-pattern '("person" ("phone" ("telephone"))("name") ("first name")) '($E-PERSON.1))
(("person"
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67"))
  ("name" "Barthes") ("first name" "Jean-Paul")))
|#

;;;---------------------------------------------------------------- FIND-OBJECTS

(defUn find-objects (query word-list &key all-objects (max-pattern-size 3)
                           used-patterns)
  "function to access objects from a list of words and a MOSS query.
Arguments:
   query: moss query pattern in which the symbol ? will be replaced by a string
   word-list: the list of words (strings) making the sentence
   all-objects (key): if true tries to extract all possible objects otherwise only
                      the first one
   max-pattern size (key): maximum number of words in a pattern
   used-patterns (key): patterns that have already been tried
Return:
   a list of objects identifiers."
  (let ((first-find (not all-objects))
        patterns tested-patterns result entry input pat)
    ;; first compute the pattern list, returns (("xxx" start end)*)
    (setq patterns (generate-access-patterns word-list :max max-pattern-size))
    ;(trformat "find-objects /patterns 1: ~S" patterns)
    (if used-patterns (setq patterns (clean-patterns patterns used-patterns)))
    ;(trformat "find-objects /patterns 2: ~S" patterns)
    ;; if no pattern left, then quit
    (unless patterns (return-from find-objects nil))
    ;(moss::trformat "find-objects /patterns: ~S" patterns)
    (is-answer-failure? nil :failure)
    ;; access each entry
    (setq pat 
          (catch :next
                 (dolist (pattern patterns)
                   ;(trformat "find-objects /pattern: ~S~%; query: ~S" pattern 
                   ;          (subst (car pattern) :? query))
                   ;; save pattern
                   (push pattern tested-patterns)
                   (setq entry (moss::access (subst (car pattern) :? query)))
                   ;; if result and we need only first entry then quit
                   (if (and first-find entry) (return-from find-objects entry))
                   ;; if we have an entry, then we must remove the entry from the initial 
                   ;; compute a new list of patterns, remove the ones that have been 
                   ;; checked and try again
                   (when entry 
                     ;(trformat "find-objects /entry: ~S" entry) 
                     (setq result entry) 
                     (throw :next pattern)))))
    ;(trformat "find-objects /pat: ~S; result: ~S; tested-patterns: ~&~S" 
    ;          pat result tested-patterns)
    ;; if pat is nil then we are done
    (unless pat (return-from find-objects result))
    ;; otherwise remove pattern from the word-list
    (setq input (list (subseq word-list 0 (cadr pat)) ; before pattern
                      (subseq word-list (caddr pat)))) ; after pattern
    ;; input is a set of fragments of the word-list (sentence)
    (setq input (remove nil input))
    ;(trformat "find-objects /input fragments left: ~S" input)
    ;; if no imput left then quit
    (unless input (return-from find-objects result))
    (setq result  ; JPB 140820 removed mapcan
          (append result
                  (reduce
                   #'append
                   (mapcar #'(lambda (xx) 
                               (find-objects query xx 
                                             :all-objects all-objects
                                             :max-pattern-size max-pattern-size
                                             :used-patterns tested-patterns))
                     input))))
    ;(trformat "find-objects /result: ~S" result)
    (delete-duplicates result)))
  
#|
(in-package :albert)
#<The ALBERT package>
(moss::find-objects '(task (has-index-pattern
                            (task-index (has-index :is :?))))
                    '("est" "qui") :all-objects t)
($E-TASK.11)

$E-TASK.11
((MOSS::$TYPE (0 $E-TASK)) (MOSS::$ID (0 $E-TASK.11))
 (MOSS::$DOCT
  (0 (:EN "Task to obtain information about a person." 
          :FR "Tache permettant d'obtenir des infos sur une personne.")))
 ($T-TASK-TASK-NAME (0 "who is")) ($T-TASK-PERFORMATIVE (0 :REQUEST)) 
 ($T-TASK-DIALOG (0 _GET-PERSON-INFO-CONVERSATION))
 ($T-TASK-INDEX (0 "qui" "est" "c'est qui")) 
 ($T-TASK-INDEX-WEIGHTS (0 ("qui" 0.5) ("est" 0.2) ("c'est qui" 0.7))))
|#
;;;-------------------------------------------------------- FIND-TASKS-FROM-TEXT

(defun find-tasks-from-text (agent args &key (performative :all))
  "this function is used for OMAS agenst. It takes a data structure and computes ~
   a list of possile tasks in the current package
Arguments:
   agent: current agent
   args: a-list like ((:raw-data . <text>)(:language . :FR)
                      (:empty-words . <empty-word-lis>))
   performative (key): :request, :command, :assert, :all (default :all)
Return:
   a list of tasks possibly empty"
  (let ((input (cdr (assoc :raw-input args)))
        ;(language (or (cdr (assoc :language args)) *language*))
        ;(empty-words (cdr (assoc :empty-words args)))
        task-list patterns level weights word-weight-list first-task result 
        pair-list)
    ;; preprocess email string -> list of words
    ;; must remove final question mark or period
    (setq input (moss::segment-master-input agent input))
    ;; we assume that resulting text contains a request or command
    ;; try to find a task from the input text
    (setq task-list (moss::find-objects 
                     '("task" ("index-pattern" ("task-index" ("index" :is :?))))
                     input :all-objects t))
    ;; filter tasks that do not have the right performative
    (unless (eql performative :all) 
      (setq task-list   ; JPB 140820 removed mapcan
            (reduce 
             #'append
             (mapcar #'(lambda (xx) 
                         (if (member performative 
                                     (send xx '=get "performative"))
                             (list xx)))
               task-list))))
    (moss::vformat "_mc-find-task /task list after performative check: ~S" task-list)
    
    ;; if no task left, quit
    (unless task-list (return-from find-tasks-from-text nil))
    
    ;;=== select task to activate
    ;; first compute a list of patterns (combinations of words) from the input
    (setq patterns (mapcar #'car (generate-access-patterns input)))
    (vformat "find-tasks-from-text /input: ~S~&  generated patterns:~& ~S" 
             input patterns)
    
    ;;=== then, for each task compute index
    (dolist (task task-list)
      (setq level 0)
      ;; get the weight list
      (setq weights (%get-INDEX-WEIGHTS task))
      (vformat "find-tasks-from-text /task: ~S~&  weights: ~S" task weights)
      ;; check the patterns according to the weight list
      (setq word-weight-list (moss::%get-relevant-weights weights patterns))
      (vformat "find-tasks-from-text /word-weight-list:~&  ~S" word-weight-list)
      ;; combine the weights
      (dolist (item word-weight-list)
        (setq level (+ level (cadr item) (- (* level (cadr item))))))
      (vformat "find-tasks-from-text /level: ~S" level)
      ;; push the task and weight onto the result list
      (push (list task level) result)
      )
    (vformat "find-tasks-from-text /result:~&~S" result)
    
    ;; if no task applies return nil
    (unless result
      (return-from find-tasks-from-text))
    
    ;; otherwise, order the list
    (setq pair-list (sort result #'> :key #'cadr))
    (vformat "process-single-email /pair-list:~&  ~S" pair-list)
    
    ;; keep the first task even if its score is low
    (setq first-task (caar pair-list))
    ;; remove the tasks that have a weight less than task-threshold (default 0.4)
    (setq pair-list
          (remove nil 
                  (mapcar 
                      #'(lambda (xx) 
                          (if (>= (cadr xx) (omas::task-threshold omas::*omas*)) xx))
                    pair-list)))
    ;; compute list of tasks
    (setq task-list (or (mapcar #'car pair-list) (list first-task)))
    ;; return the list of tasks
    task-list))

#|
HDSRI(44): (MOSS::FIND-TASKS-FROM-TEXT PA_HDSRI '((:raw-data . "contacts de barthès.")))
($E-TASK.7 $E-TASK.8 $E-TASK.5 $E-TASK.3)

HDSRI(45): (MOSS::FIND-TASKS-FROM-TEXT PA_HDSRI '((:raw-data . "contacts de barthès."))
                                       :performative :request)
($E-TASK.7 $E-TASK.8)

HDSRI(46): (MOSS::FIND-TASKS-FROM-TEXT PA_HDSRI '((:raw-data . "contacts de barthès."))
                                       :performative :command)
($E-TASK.5 $E-TASK.3)

HDSRI(47): (MOSS::FIND-TASKS-FROM-TEXT PA_HDSRI '((:raw-data . "contacts de barthès."))
                                       :performative :assert)
NIL
|#

;;;---------------------------------------------------- GENERATE-ACCESS-PATTERNS

(defUn generate-access-patterns (word-list &key (max 3))
  "function to generate a list of patterns from a sentence expressed as a list of ~
      words.
Arguments:
   word-list: list of words (strings)
   max (key): max length if word combination (default 3)
Return:
   a list of entries like (\"soupe du jour\" 2 5) where 2 is the position of the ~
      substring in the sentence and 5 its end position."
  (let ((word-list-length (length word-list))
        nmax kk expr result)
    ;; otherwise try several words at a time starting with triples
    (setq nmax (min max (1- (1+ word-list-length))))
    (dotimes (nn nmax)
      ;; nn takes values 0 1 2 if list has more than 3 (max) elements
      ;; 0 1 if list has 3 elements, 0 if list has 2 elements
      ;; Now try to access using combination of words starting with triples
      ;; and down
      (setq kk (- nmax nn))
      ;; kk ranges from 3 down to 1
      (dotimes (jj (1+ (- word-list-length kk)))
        ;; jj sweeps the word-list
        (setq expr (apply #'moss::%make-phrase (subseq word-list jj (+ jj kk))))
        ;; extract combination of words
        (push (list expr jj (+ jj kk)) result)
        ;; whenever we find something, then quit
        ))
    (reverse result)))

#|
(moss::generate-access-patterns '("le" "jour" "le" "plus" "long") :max 3)
(("le jour le" 0 3) ("jour le plus" 1 4) ("le plus long" 2 5) ("le jour" 0 2) 
 ("jour le" 1 3) ("le plus" 2 4) ("plus long" 3 5) ("le" 0 1) ("jour" 1 2) 
 ("le" 2 3) ...)
|#
;;;---------------------------------------------------------- IS-ANSWER-FAILURE?
;;; not sure it is really useful

(defUn is-answer-failure? (input &optional answer)
  "argument means failure if input is :failure, nil, \"failure\", :error, ~
   \"*failure*\".
Arguments:
   input: argument to test (usually input slot of FACTS)
   answer (opt): secondary argument (usually answer slot of FACTS). If input ~
                 contains answer-there, then answer is checked for error mark
Return:
   t or nil"
  (cond
   ((eql input :failure) t)
   ((eql input :error) t)
   ((null input) t)
   ((and (eql input :answer-there)
         (or (eql answer :failure)
             (and (stringp answer)
                  (or (string-equal answer "failure")
                      (string-equal answer "*failure*"))))))
   ;; now string case
   ((and (stringp input)(string-equal input "failure")))
   ((and (stringp input)(string-equal input "*failure*")))
   ((and (stringp input)
         (string-equal input "answer-there")
         (eql answer :error)))
   ((and (stringp input)
         (string-equal input "answer-there")
         (stringp answer)
         (or (string-equal answer "failure")
             (string-equal answer "*failure*"))))
   ;; next line puts the system into a loop when the answer slot has not been
   ;; cleaned from a previous failure. JPB 1006
   ;((equal answer :failure)) 
   ))

#|
(is-answer-failure? "failure" nil)
7
(is-answer-failure? nil nil)
T
(is-answer-failure? :failure nil)
T
(is-answer-failure? "answer-there" "failure")
7
(is-answer-failure? "answer-there" "*failure*")
9
(is-answer-failure? nil :failure)
T
(is-answer-failure? "youppee..." nil)
NIL
(is-answer-failure? "answer-there" :error)
T
(is-answer-failure? "Albert" :failure)
NIL
|#
;;;------------------------------------------------------------ MAKE-ANSWER-LIST

(defUn make-answer-list (expr &optional (offset 0) no-head)
  "default print function for returned answer patterns.
Argument:
   expr: pattern filled with data
   offset (opt): initial offset for printing the data
   no-head (opt): if t do not print the name of the property
Return;
   nil."
  (let ((indent (make-string  offset :initial-element #\space))
        lres)
    ;; check case with only class reference and summary value
    ;; or property and list of values (recursive call)
    (cond
     ;; class name and summary or attribute and values
     ((stringp (cadr expr))
      (push 
       (if no-head
           (format nil "~A~*~{~A~^, ~}" indent (pop expr) expr)
         (format nil "~A~A: ~{~A~^, ~}" indent (pop expr) expr))
       lres))
     (t
      (setq lres (list (format nil "~A~A:" indent (pop expr))))
      (incf offset 3)
      ;; loop on properties (("first-name" "Jean-Paul")
      ;;                     ("phone" ("home phone" "(0)3 44 23 31 37")))
      (loop
        (unless expr (return nil))
        ;; an attribute is followed by a simple list of strings
        (push (make-answer-list (car expr) offset no-head) lres)
        (setq expr (cdr expr))
        ;;
        )))
    (reverse lres)))

#|
? (make-answer-list '("person" "Barthes: Jean-Paul"))
("person: Barthes: Jean-Paul")

? (make-answer-list 
   '("person" ("name" "Barthes") ("first-name" "Jean-Paul") 
     ("phone" ("home phone" "(0)3 44 23 31 37") 
      ("office phone" "(0)3 44 23 44 66")
      ("cell phone" "(0)6 80 45 32 67"))))
("person:" ("   name: Barthes") ("   first-name: Jean-Paul")
 ("   phone:" ("      home phone: $E-HOME-PHONE.1")
  ("      office phone: $E-OFFICE-PHONE.1") ("      cell phone: $E-CELL-PHONE.1")))

? (make-answer-list 
   '("person" ("name" "Barthes" "Biesel") ("first-name" "Jean-Paul") 
     ("phone" ("home phone" "(0)3 44 23 31 37") 
      ("office phone" "(0)3 44 23 44 66")
      ("cell phone" "(0)6 80 45 32 67"))))
("person:" ("   name: Barthes, Biesel") ("   first-name: Jean-Paul")
 ("   phone:" ("      home phone: (0)3 44 23 31 37")
  ("      office phone: (0)3 44 23 44 66") ("      cell phone: (0)6 80 45 32 67")))

? (make-answer-list 
   '("person" ("name" "Barthes" "Biesel") ("first-name" "Jean-Paul") 
     ("phone" ("home phone" "(0)3 44 23 31 37") 
      ("office phone" "(0)3 44 23 44 66")
      ("cell phone" "(0)6 80 45 32 67")))
   0 t)
|#
;;;------------------------------------------------------------- MAKE-PRINT-LIST

(defUn make-print-list (expr &optional (offset 0) no-head)
  "takes a list of objects to print in MOSS format and produces a list to be ~
      printed.
Argument:
   expr: list of expr to print
   offset (opt): initial offset for printing the data
   no-head (opt): if t do not print the name of the property
Return;
   nil."
  (let ((result (mapcar #'(lambda (xx) (make-answer-list xx offset no-head)) expr)))
    (format nil "~{~A~%~}" (mapcar #'print-answer-pattern result))))


#|
? (make-print-list
    '(("person" ("name" "Barthes") ("first-name" "Jean-Paul")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67")))
 ("person" ("name" "Barthes Biesel" "Barthes" "Biesel") ("first-name" "Dominique")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 43 93")
   ("cell phone" "(0)6 81 24 46 66")))
 ("person" ("name" "Barthes") ("first-name" "Camille")
  ("phone" ("home phone" "+86 571 8701 0782") ("cell phone" "+86 134 5678 3173")))))
"
person:
   name: Barthes
   first-name: Jean-Paul
   phone:
      home phone: (0)3 44 23 31 37
      office phone: (0)3 44 23 44 66
      cell phone: (0)6 80 45 32 67

person:
   name: Barthes Biesel, Barthes, Biesel
   first-name: Dominique
   phone:
      home phone: (0)3 44 23 31 37
      office phone: (0)3 44 23 43 93
      cell phone: (0)6 81 24 46 66

person:
   name: Barthes
   first-name: Camille
   phone:
      home phone: +86 571 8701 0782
      cell phone: +86 134 5678 3173
"

? (make-print-list
    '(("person" ("name" "Barthes") ("first-name" "Jean-Paul")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 44 66")
   ("cell phone" "(0)6 80 45 32 67")))
 ("person" ("name" "Barthes Biesel" "Barthes" "Biesel") ("first-name" "Dominique")
  ("phone" ("home phone" "(0)3 44 23 31 37") ("office phone" "(0)3 44 23 43 93")
   ("cell phone" "(0)6 81 24 46 66")))
 ("person" ("name" "Barthes") ("first-name" "Camille")
  ("phone" ("home phone" "+86 571 8701 0782") ("cell phone" "+86 134 5678 3173"))))
 0 t)
"
person:
   Barthes
   Jean-Paul
   phone:
      (0)3 44 23 31 37
      (0)3 44 23 44 66
      (0)6 80 45 32 67

person:
   Barthes Biesel, Barthes, Biesel
   Dominique
   phone:
      (0)3 44 23 31 37
      (0)3 44 23 43 93
      (0)6 81 24 46 66

person:
   Barthes
   Camille
   phone:
      +86 571 8701 0782
      +86 134 5678 3173
"
|#

;;;--------------------------------------------------------------- PROCESS-INPUT

(defun process-input (conversation text)
  "receives a text string and must fill the :raw-input slot and the :input slot
  of the FACTS area."
  (let ()
    ;; if conversation not there, complain
    (unless conversation (error "no conversation in process-input"))
    ;; save text into raw-input
    (replace-fact conversation :raw-input text)
    ;; if verbatim, input is a cop of raw input
    (cond
     ((read-fact conversation :verbatim)
      ;; calling function must take care of :verbatim
      (replace-fact conversation :input text))
    ;; otherwise, segment the input
     (t (replace-fact conversation :input (segment-master-input text))))
    ;; return
    ))

;;;------------------------------------------------------------ PROCESS-PATTERNS
;;; dealing with simplified patterns 1105

(defUn process-patterns (input pattern-list)
  "takes a list representing test and a set of ELIZA-type rules and returns the ~
      bindings associated with the first rule that applies.
Arguments:
   input: a list of word strings representing text
   pattern-list: a list of patterns (ELIZA style)
Return:
   the list of bindings corresponding to the first pattern that applies ~
      or nil if it fails."
  ;(trformat "process-patterns input: ~S ~&  patterns: ~S" input pattern-list)
  (some #'(lambda (pattern) (pat-match pattern input)) 
        (mapcar #'make-pattern pattern-list)))

;;;-------------------------------------------------------- PROCESS-WHY-QUESTION
;;; nice idea, needs to be rewritten

#|
(defUn process-why-question (conversation)
  "prints info about why assistant asked the previous question.
Arguments:
   conversation: current conversation
Return:
   not significant."
  (let* ((task (car (send conversation '=get 'HAS-MOSS-TASK)))
         (sub-dialog-header (car (HAS-MOSS-SUB-DIALOG-HEADER conversation)))
         explanation)
    ;; we should print the current goal and the content of the why slot attached
    ;; to the current state
    (when task
      (moss::mformat "~%We are executing: ~A" (car (HAS-MOSS-DOCUMENTATION task))))
    ;; check if there is an explanation in the current state
    (setq explanation (HAS-MOSS-EXPLANATION (car (HAS-MOSS-STATE conversation))))
    ;; if so, print it
    (when explanation
      (moss::mformat "~%~A~%" (car explanation)))
    ;; if sub-dialog print more info
    (when sub-dialog-header
      (moss::mformat "~%Current objective: ~A"
                     (car (HAS-MOSS-EXPLANATION sub-dialog-header))))
    :done))
|#

#|
? (PROCESS-WHY-QUESTION (car (last (access '("moss-conversation")))))

Asking the user it he wants to do more interaction.

Current objective: This is the main conversation loop.
:DONE
|#
;;;---------------------------------------------------------- READ-FROM-LISTENER

(defUn read-from-listener (&optional read-parents)
  "read lines of text until a period or a question mark is found. Uses readline, ~
  concatenating the lines and then reading from the resulting string.
Arguments:
  read-parents (opt): if t checks if parents are balanced, if nil replaces parents 
  with space
Return:
  a list or nil if read-from-string fails."
  (let (line line-list text)
    (loop
      (setq line (read-line))
      (nsubstitute #\space #\, line)
      (nsubstitute #\space #\; line)
      ;(nsubstitute #\space #\: line)
      (unless read-parents
        (nsubstitute #\space #\( line)
        (nsubstitute #\space #\) line))
      (nsubstitute #\space #\, line)
      (nsubstitute #\space #\# line)
      (nsubstitute #\space #\' line)
      (nsubstitute #\space #\" line)
      ;(print line)
      ;; if dot or question mark or exclamation mark, end of text
      (when (position  #\. line)
        (nsubstitute #\space #\. line)
        (push line line-list)
        (return nil))
      (when (position  #\? line)
        (nsubstitute #\space #\? line)
        (push line line-list)
        (return nil))
      (when (position  #\! line)
        (nsubstitute #\space #\! line)
        (push line line-list)
        (return nil))
      (push line line-list)
      ;; add space between 2 lines
      (push " " line-list))
    ;; make a single list
    (setq text (apply #'concatenate 
                      (if read-parents
                          `(string ,@(reverse line-list))
                          `(string ,@(reverse line-list)))))
    (if (and read-parents (not (eql (count #\( text) (count #\) text))))
        (progn
          (verbose-warn "unbalanced parenthesis in expression~& ~A" text)
          nil)
        text)))

;;;-------------------------------------------------------- RESTART-CONVERSATION

(defUn restart-conversation (conversation-id)
  "restarts a conversation. Uses the conversation object to restart at the ~
   beginning. Reinitializes with initial state. 
Arguments:
   conversation: conversation to be restarted
Return:
   the id of the conversation object."
  (let* ((initial-state 
          (car (HAS-MOSS-ENTRY-STATE 
                (car (HAS-MOSS-DIALOG-HEADER conversation-id))))))
    (print `("restart-conversation initial-state" ,initial-state))
    ;; keep input and output channels, DIALOG-HEADER 
    ;; reset frame-list, facts
    (send 'moss::$FLT '=delete-all conversation-id)
    (send 'moss::$FCT '=delete-all conversation-id)
    ;; copy initial state into STATE (current state)
    (send conversation-id '=replace 'HAS-MOSS-STATE (list initial-state))
    
    (if *transition-verbose*
        (mformat "~&===== Restarting conversation with state: ~S" 
          (car (has-MOSS-label initial-state))))
    ;;*** debug
    ;(print-conversation conversation-id)
    ;;*** end debug
    ;; go execute the new state with the associated context
    ;(send initial-state '=execute conversation-id)
;(break)
    ;; return the id of the conversation object
    initial-state))

;;;-------------------------------------------------------- SEGMENT-MASTER-INPUT

(defun segment-master-input (text)
  "only valid for usual western languages, i.e. mostly English"
  ;; keep "?" 
  (%get-words-from-text text *delimiters* '(#\?)))

;;;------------------------------------------------------------------- TRIM-DATA
;;; unused, but could be useful

(defUn trim-data (word-list data)
  "remove all words of the word-list from the data list. Keep the order.
Arguments:
   word-list: list of words to remove from data
   data: list of words to be cleaned.
Return:
   list without the trimmed words."
  (cond
   ((null word-list) data)
   ((null data) nil)
   ((and (stringp (car data))(member (car data) word-list :test #'string-equal))
    (trim-data word-list (cdr data)))
   ((member (car data) word-list)
    (trim-data word-list (cdr data)))
   (t (cons (car data) (trim-data word-list (cdr data))))))
  


:EOF
