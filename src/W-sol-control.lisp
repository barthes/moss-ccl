;;;-*- Mode: Lisp; Package: "NODGUI-USER" -*-
;;;==============================================================================
;;;19/11/24		
;;;		W - S O L - C O N T R O L  (File w-sol-control.lisp)
;;;	
;;;==============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
#|
2019
 1124 copy from SOL 5.6 folder
|#

(in-package :nodgui-user)

(eval-when (:load-toplevel :compile-toplevel :execute)
  (use-package :sol)
  (import '(sol::*sol-file* sol::*sol-version* sol::*language-list*
            sol::*sol-language* sol::*owl-output* sol::*html-output* sol::*rule-output* 
            sol::*rule-format* sol::*rule-format-list* sol::*verbose-compile*
            sol::*trace-window* sol::*trace-pane*)))

;;;------------------------------ Macros ----------------------------------------
;;; in this file we need not check for presence of trace window

#|
(defMacro tkformat (cstring &rest args)
  `(append-text *trace-pane* (format nil ,cstring ,@args)))
|#

;;;===============================================================================
;;;                                 SOL-WINDOW
;;;===============================================================================

;;;================================== Class ======================================

(defclass sol-window (toplevel)
  ((language :accessor language :initform :en)
   (rule-format :accessor rule-format :initform :jena)
   (file-name :accessor file-name :initform nil)
   (package-name :accessor ontology-package-name :initform nil)
   )
  )

;;;==================================== Window ===================================

;;;--------------------------------------------------------------- MAKE-SOL-WINDOW
#|
(with-nodgui () (make-sol-window))
|#

(defun make-sol-window (infile-name)
  "Define the SOL control window"
  (declare (special *sol-file* *language-list* *sol-version*))
    (let* ((sw (make-instance 'sol-window :master *tk*))
           (frame (make-instance 'frame :master sw))  
           (file-label 
            (make-instance 'label
              :master frame
              :text "SOL file"
              :font "ansi 10 bold"
              :justify :left
              ))
           (file-pane
            (make-instance 'text
              :master frame
              :width 20
              :height 1
              ))
           (fb-choose-file
            (make-instance 'button
              :master frame
              :text "choose-file"
              :command (lambda ()(sol-choose-file-on-click))
              ))
           ;; trace mode
           (trace-label 
            (make-instance 'label
              :master frame
              :text "SOL trace mode"
              :font "ansi 10 bold"
              :justify :left
              ))
           (cb-verbose 
            (make-instance 'check-button
              :master frame
              :text "verbose"
              :command (lambda (value)
                         (sol-verbose-on-change value)
                         (format t "Verbose status: ~S" 
                                 *verbose-compile*))
              ))
           ;; compiler output
           (owl-label 
            (make-instance 'label
              :master frame
              :text "SOL compiler: OWL output"
              :font "ansi 10 bold"
              :justify :left
              ))
           (cb-no-owl-output 
            (make-instance 'check-button
              :master frame
              :text "no OWL output (error check only)"
              :command (lambda (value)
                         (sol-OWL-output-on-change value))
              ))

           ;; HTML and text outouts          
           (html-label 
            (make-instance 'label
              :master frame
              :text "SOL compiler: HTML and TEXT outputs"
              :font "ansi 10 bold"
              :justify :left
              ))
           (cb-html-output 
            (make-instance 'check-button
              :master frame
              :text "HTML and TEXT outputs"
              :command (lambda (value)
                         (sol-HTML-output-on-change value))
              ))
           (mb-language 
            (make-instance 'spinbox
              :master frame
              :text "English"
              ))
           (text-4 
            (make-instance 'label
              :master frame
              :text "SOL compiler: rule extraction"
              :font "ansi 10 bold"
              :justify :left
              ))
           (cb-rule-output 
            (make-instance 'check-button
              :master frame
              :text "RULE output"
              :command (lambda (value)
                         (sol-RULE-output-on-change value))
              ))
           (mb-rules 
            (make-instance 'spinbox
              :master frame
              :text "Jena"
              ))
           (compile-button
            (make-instance 'button
              :master frame
              :text "Compile"
              :command (lambda (&rest ll)
                         (declare (ignore ll))
                         (sol-compile-on-click file-pane mb-language mb-rules))
              ))
           )
      (wm-title sw (concatenate 'string "SOL Compiler v " *sol-version*))

      ;; geometry
      (pack frame)
      (configure frame :borderwidth 10)
      (pack file-label :anchor :w :pady 5)
      (pack file-pane :side :top :anchor :w :padx 10)
      (configure file-pane :width 40)
      (pack fb-choose-file :side :top :anchor :e :pady 5)
      (pack trace-label :anchor :w)
      (pack cb-verbose :anchor :w :padx 10)
      (pack owl-label :side :top :anchor :w :pady 5)
      (pack cb-no-owl-output :anchor :w :padx 10)
      (pack html-label :side :top :anchor :w :pady 5)
      (pack cb-html-output :anchor :w :padx 10)
      (pack mb-language :anchor :w :padx 10)
      (pack text-4 :side :top :anchor :w :pady 5)
      (pack cb-rule-output :anchor :w :padx 10)
      (pack compile-button :side :right :anchor :n)
      (pack mb-rules :anchor :w :padx 10)
      (pack compile-button :side :right)

      ;; initializations
      (setf (text file-pane) (setq *sol-file* infile-name))
      (setf (value cb-verbose) *verbose-compile*)
      (setf (value cb-no-owl-output) *owl-output*)
      (setf (value cb-html-output) *html-output*)
      (setf (value cb-rule-output) *rule-output*)
      (configure mb-language :values (format nil "~{~A~^ ~}" 
                                             (mapcar #'car *language-list*)) 
                 :wrap t)
      (configure mb-rules :values (format nil "~{~A~^ ~}" 
                                          (mapcar #'car *rule-format-list*)) 
                 :wrap t)

      (setq *filename-display* file-pane)
      ))

#|
(with-nodgui () (make-sol-window))
|#
;;;----------------------------------------------------- SOL-CHOOSE-FILE-ON-CLICK

(defun sol-choose-file-on-click ()
  (declare (special *sol-file* *ontology-initial-directory* *filename-display*))
  (let ((filename (ccl::choose-file-dialog)))
    ;; if nil we keep previous filename, otherwise change to the specified one
    (when filename
      (setf (text *filename-display*) filename)
      (setq *sol-file* filename)
      )))

;;;------------------------------------------------ SOL-CHOOSE-LANGUAGE-ON-CHANGE

(defun sol-choose-language-on-change (new-value)
  (declare (special *sol-language* *language-list*))
  (setq *sol-language* (cdr (assoc new-value *language-list* :test #'string-equal))))

;;;--------------------------------------------- SOL-CHOOSE-RULE-FORMAT-ON-CHANGE

(defun sol-choose-rule-format-on-change (new-value)
  (declare (special *rule-format* *rule-format-list*))
  (setq *rule-format* 
	(cdr (assoc new-value *rule-format-list* :test #'string-equal))))

;;;--------------------------------------------------------- SOL-COMPILE-ON-CLICK

(defun sol-compile-on-click (file-name-widget language-widget rule-widget)
  "Called when the user clicks the Compile button.
   Asks for a file name to compile, creates a trace window to trace the ~
      compiling process.
Arguments:
   none
Return:
   t"
  (let ((file-name (string-trim '(#\space) (text file-name-widget)))
	pos errors)
    (declare (special *sol-file* *trace-window* *trace-header* *rule-format*
                      *verbose-compile* *OWL-output* *HTML-output* *sol-language*))
    (setq *sol-language* 
          (cdr (assoc  (text language-widget) *language-list* :test #'string-equal)))
    (setq *rule-format* 
          (cdr (assoc  (text rule-widget) *rule-format-list* :test #'string-equal)))
(print `("language: " ,*sol-language*))
(print `("rule format: " ,*rule-format*))
    ;; we must remove the EOL
    (if (setq pos (position '#\newline file-name))
      (setq file-name (subseq file-name 0 pos)))
(print `("file name: " ,file-name))
    ;; if choice is cancelled, file name is empty
    (when (or (equal file-name "") (not (probe-file file-name)))
      (ask-OKcancel "Can't find this file...")
      (return-from sol-compile-on-click nil))

    (setq *sol-file* file-name)
(print `("*verbose-compile*: "  ,*verbose-compile*))
    (if *verbose-compile* (make-sol-trace-window file-name))
    
    (solformat *trace-header*)
    ;; print parameters
    (solformat "~%File: ~S~
                 ~%verbose: ~S; OWL output: ~S; TEXT output: ~S; language: ~S" 
                 file-name *verbose-compile* *OWL-output* *HTML-output* *sol-language*)
(print `("*trace-window*: "  ,sol::*trace-window*))

      
      (when file-name
	;; compile-sol returns nil on error
        (moss::with-package :sol-owl  ; we need that to select the right version of
          ; make-xx in the file being compiled
          (unless (sol-owl::compile-sol file-name 
                                        :verbose *verbose-compile* 
                                        :errors-only (not *OWL-output*)
                                        :rule-output *rule-output*)
            (solformat "~%*** Error while compiling to OWL - no output files ***")
            (setq errors t)
            ;(return-from sol-compile-on-click t)
            ))

	;; when OK, check if html or text output is required
	(unless errors
	  (when *html-output*
	    (moss::with-package :sol-html
              ;; produce HTML and TEXT files, no trace
              (sol-html::compile-sol file-name
                                     :language *sol-language*
                                     :verbose nil))
	    )
	  (solformat "~%*** End of processing ***")
	  )))
    ;; destroy the logical reference to the window (will print into the listener)
  (setq sol::*trace-window* nil)
    )

;;;---------------------------------------------------- SOL-HTML-OUTPUT-ON-CHANGE

(defun sol-html-output-on-change (new-value)
  (declare (special *html-output*))
  (setq *html-output* (eql new-value 1)))

;;;---------------------------------------------------- SOL-OWL-OUTPUT-ON-CHANGE

(defun sol-owl-output-on-change (new-value)
  (declare (special *owl-output*))
  (setq *owl-output* (eql new-value 0)))

;;;---------------------------------------------------- SOL-RULE-OUTPUT-ON-CHANGE

(defun sol-rule-output-on-change (new-value)
  (declare (special *rule-output*))
  (setq *rule-output* (eql new-value 1)))

;;;-------------------------------------------------------- SOL-VERBOSE-ON-CHANGE

(defun sol-verbose-on-change (new-value)
  ;; interface returns 1 if box is checked, 0 otherwise
  (declare (special *verbose-compile*))
  ;(print `("verbose on change " ,(eql new-value 1)))
  (setq *verbose-compile* (eql new-value 1)))

;;;===============================================================================
;;;                            SOL TRACE WINDOW
;;;===============================================================================
;;; we make a window to trace compiling of the MOSS alias SOL files

(defclass trace-window (toplevel)
  ((text-pane :accessor text-pane :initform nil)))

(defun make-sol-trace-window (file-name)
  "window to trace SOL compiler execution
  Arguments:
file-name: the name of the ontology file
  outfile-name: the name of the resulting OWL file
  Return:
  t"
  (declare (special sol::*trace-window* sol::*trace-pane*))
  ;; therefore we do not call with-nodgui agaim
  (let*
      ((stw (make-instance 'trace-window))
       (st (make-instance 'text :master stw :width 600 :height 800))
       )
    (set-geometry stw  600 800 50 50)
    (wm-title stw sol::*sol-header*)
    (pack st :fill :both)

    ;; can't call solformat here since *trace-window* is not initialized yet
    (append-text st (format nil "~%Input File: ~S~
  ~%Output File: ~S~
  ~%verbose: ~S; TEXT output: ~S; language: ~S" 
               file-name
               sol::*verbose-compile* sol::*OWL-output* 
               sol::*HTML-output* sol::*sol-language*))
    (on-close stw (lambda () 
                    ;; and reset *trace-window* to indicate it is closed
                    (setq sol::*trace-window* nil)
                    ;; close window
                    (destroy stw)))
    (setf (text-pane stw) st)
    (setq sol::*trace-window* stw)
    ))

#|
? (with-nodgui () (setq sol::*trace-window* (make-sol-trace-window "infile"))
         (print sol::*trace-window*)
         (solformat "~2% Il pleut ?...")
         (solformat "~2% Il pleut ?..."))
|#
;;;-------------------------------------------------------------------- SOLFORMAT

 (defun solformat (cstring &rest args)
  "prints to trace window."
  (declare (special sol::*trace-pane* sol::*trace-window*))
  (if sol::*trace-window*
      ;; if using Tk, call the proper printing function
      (append-text (text-pane sol::*trace-window*) (apply #'format nil cstring args))
      ;; otherwise print into listener
      (apply #'format t cstring args))
  )



;:EOF