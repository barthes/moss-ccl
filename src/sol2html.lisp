;;;-*- Mode: Lisp; Package: "SOL-HTML" -*-
;;;============================================================================
;;;19/11/23	
;;;		S O L 2 H T M L - (File sol2html-vxx.Lisp)
;;;         version 6 alpha
;;;	
;;;============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;;============================================================================
#|
This file contains a parser for the simplified OWL language that translates SOL
expressions into an HTML file for visualization.

The HTML pages should contain the vocabulary of the user. Thus the names and 
expressions are those of the definitions, normalized by a string normalizing 
function.
We borrow the OWL IDs to uniquely define anchor points in the HTML page. However, 
we produce a list of all indexes
   concepts 
   attribute (list of domain concepts)
   relations (list of domain concepts)
   individuals

The compiler uses two passes.
Pass 1
======
The index table is built to contain:
   for each concept: all synonymous (indexes) referring to the concept ID
   for each attributes: all synonymous referring to the attribute id, and the
      references to the corresponding concepts
   for each relation: all synonymous referring to the relation ID, and the
      references to the corresponding domain concepts
   for each individual: all synonymous referring to the individual

History
2019
 1123 copy of the file from the SOL 5.6 folder
|#

(in-package :SOL-HTML)  ; inherits exported names from :SOL

(defParameter *directory* *load-pathname*)

;;;============================================================================
;;; Definition macros
;;;============================================================================
;;; we ignore the following definitions:
;;;  defattribute: used when defining attributes outside concepts, e.g. for orphans
;;;  defrelation: id.
;;;  defobject: for defining orphans
;;;  definstmethod: do not appear in an HTML page
;;;  defownmethod: id.
;;;  defuniversalmethod: id.
;;;  defruleheader: used when translating to JENA or SPARQL
;;;  defrule: id.
;;;  defvirtualconcept: id.
;;;  defvirtualattribute: id.
;;;  defvirtualrelation: id.
;;;
;;;  defchapter, defsection are used for large ontologies
 
(defMacro defchapter (&rest mls)
  `(apply #'make-chapter ',mls nil))

(defMacro defconcept (&rest options)
  `(apply #'make-concept ',options ))

(defMacro defindividual (&rest options)
  `(apply #'make-individual ',options))

(defMacro defattribute (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defrelation (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defobject (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro definstmethod (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defownmethod (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defuniversalmethod (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defontology (&rest option-list)
  `(apply #'make-ontology ',option-list))

(defMacro defsection (&rest mls)
  `(apply #'make-section ',mls))

;;;---------- macros for taking care of virtual concepts and rules

(defMacro defruleheader (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defvirtualconcept (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defrule (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defvirtualattribute (&rest option-list)
  (declare (ignore option-list))
  nil)

(defMacro defvirtualrelation (&rest option-list)
  (declare (ignore option-list))
  nil)

;;;============================================================================

(defMacro terror (cstring &rest args)
  `(throw :error (format nil ,cstring ,@args)))

(defMacro twarn (cstring &rest args)
  "inserts a message into the *error-message-list* making sure to return nil"
  `(progn (push (format nil ,cstring ,@args) *error-message-list*) nil))

(defMacro gformat (control-string &rest args)
  `(format *graph* ,control-string ,@args))

(defMacro hformat (control-string &rest args)
  `(format *output* ,control-string ,@args))

(defMacro sformat (control-string &rest args)
  `(format *isa-graph* ,control-string ,@args))

(defun trformat (cstring &rest args)
  "If Trace window is used, prints to window, otherwise use standard output."
  (declare (special nodgui-user::*trace-window* *log-file*))
  (if nodgui-user::*trace-window*
      (nodgui-user::solformat cstring args)
    (apply #'format *log-file* cstring args)))
	
(defMacro vformat (cstring &rest args)
  "prints something if verbose flag on only."
  `(if *cverbose* (trformat ,cstring ,@args)))

(defMacro dformat (fstring &rest args)
  "debugging macro"
  `(trformat ,(concatenate 'string "~&;********** " fstring) ,@args))

;;;================================= Globals ==================================
;;; see globals.lisp

#|
(defParameter *copyright*
  "SOL to HTML compiler - v 1.2 @Barthès@UTC,2005")

(defParameter *output* t)
(defParameter *graph* nil "text file for the grapher")
(defParameter *isa-graph* nil "is-a text file for the grapher")
(defParameter *compiler-pass* 1 "compiler has 2 passes")

(defParameter *language-tags* '(:en :fr :it :pl :*))
(defParameter *language-index-property* 
  '((:en . "isEnIndexOf") (:fr . "isFrIndexOf") (:it . "isItIndexOf")
    (:pl . "isPlIndexOf")))
(defVar *ontology-title* "default local to this file")
(defParameter *current-language* :en)
(defParameter *class-title* 
  '(:en "Classes" :fr "Classes"))
(defParameter *attribute-title*
  '(:en "Attributes" :fr "Attributs"))
(defParameter *relation-title*
  '(:en "Relations" :fr "Relations"))
(defParameter *individual-title*
  '(:en "Individuals" :fr "Instances"))
(defParameter *class-header*
  '(:en "Class:" :fr "Classe :"))

;;; the following types are xsd types built in in OWL
(defParameter *attribute-types*
  '((:string . "string") (:boolean . "boolean") (:decimal . "decimal") 
    (:float . "float") (:double . "double") (:date-time . "dateTime")
    (:time . "time") (:date . "date") (:g-year-month . "gYearMonth")
    (:g-year . "gYear") (:g-month-day . "gMonthDay") (:g-day . "gDay")
    (:g-month . "gMonth") (:hex-Binary . "hexBinary") 
    (:base-64-binary . "base64Binary")
    (:any-uri . "anyURI") (:normalized-string . "normalizedString") 
    (:token . "token")
    (:language . "language") (:nm-token . "NMTOKEN") (:name . "Name")
    (:nc-name . "NCName") (:integer . "integer")  
    (:non-positive-integer . "nonPositiveInteger") 
    (:negative-integer . "negativeInteger") (:long . "long") (:int . "int")
    (:short . "short") (:byte . "byte") 
    (:non-negative-integer . "nonNegativeInteger")
    (:unsigned-long . "unsignedLong") (:unsigned-int . "unsignedInt")
    (:unsigned-short . "unsignedShort") (:unsigned-byte . "unsignedByte")
    (:positive-integer . "positiveInteger")))

(defParameter *index* (make-hash-table :test #'equal) "internal compiler table")
(defParameter *index-list* ())
(defParameter *attribute-list* ())
(defParameter *concept-list* ())
(defParameter *relation-list* ())
(defParameter *individual-list* ())

(defParameter *indent-amount* 2)
(defParameter *left-margin* 0)

(defParameter *chapter-counter* 0)
(defParameter *section-counter* 0)

(defParameter *cverbose* t)

(defParameter *translation-table* '((#\' #\space)))

;;; globals for the first syntax check compiler pass
(defParameter *line* nil)
(defParameter *text*  "")
(defParameter *breaks* '("(defontology" "(defchapter" "(defsection"
                         "(defconcept" "(defindividual"))
(defParameter *syntax-errors* nil)
|#

;;;================================== Functions ===============================

;;;--------------------------------------------------------------- check-syntax

(defun check-syntax (sol-file &aux expr (count 0))
  "checks structural syntax of SOL definitions, i.e. parents and quotes.
   In case of error *syntax-errors* is set to T.
Argument:
   sol-file: name of sol-file
Return:
   string stating how many entries were checked."
  ;; reset text
  (setq *text* nil *syntax-errors* nil)
  ;; open file
  (with-open-file (ss sol-file :direction :input)
    ;; initialize first line
    (setq *line* (read-next-valid-line ss))
    ;(print *line*)
    (setq *text* (list *line*))
    ;; loop on definitions
    (loop
      ;; read next line
      (setq *line* (read-next-valid-line ss))
      ;(print *line*)
      (if (eql *line* :eof) (return))
      ;; otherwise check for breaks
      (if (member t (mapcar 
                     #'(lambda (xx) (and 
                                     (search xx *line* :test #'string-equal)
                                     t))
                     *breaks*))
        ;; check for valid definition
        (progn
          ;; we got something
          (incf count)
          ;(format t "~&~{~A~&~}" (reverse *text*))
          ;; catch the error condition while reading from string
          (setq expr (handler-case 
                       (read-from-string 
                        (format nil "~{~A~&~}" (reverse *text*)))
                       (error () :error)))
          (case expr
            (:error
             (vformat "~%*** ERROR: check parentheses and quotes in:~&~{~A~&~}" 
                      (reverse *text*))
             (setq *syntax-errors* t))
            )
          ;; reset *text*
          (setq *text* (list *line*))
          )
        ;; otherwise simply push line into text
        (push *line* *text*))
      ))
  ;; quit
  (format nil "We checked ~S entries." count))

;(check-syntax "c:/SOL/test.sol")
#|
;;;----------------------------------------------------------------------- ltab
;;; introduces left tab into printed output
(defun ltab ()
  (decf *left-margin* *indent-amount*))

;;;----------------------------------------------------------------------- rtab
;;; introduces right tab into printed output
(defun rtab ()
  (incf *left-margin* *indent-amount*))
|#
;;;-------------------------------------------------------- compile-SOL-to-HTML

(defun compile-sol (infile &key outfile (language :en) verbose standalone)
  "compile a file containing SOL definition into an HTML file
Arguments:
   infile: string specifying the SOL file
   outfile (key): output file
   language (key): ontology language (default English :en)
   verbose (key): if t prints a trace of what is going on
   standalone (key): if t we did not call the OWL compiler beforehand
Return:
   :EOC"
  (declare (special *verbose* *log-file*))
  (let* ((sol-file (or infile (ccl::choose-file-dialog)))
         (tmp-file (make-file-pathname sol-file "tmp"))
         (fasl-file (make-file-pathname sol-file  "dx64fsl"))
         (lib-file (make-file-pathname sol-file "lib"))
         (log-file (make-file-pathname sol-file "log"))
         (html-file (or outfile (make-file-pathname sol-file "html" :language t)))
         (graph-file (make-file-pathname sol-file "txt" :language t))
         (isa-graph-file (make-file-pathname sol-file "isa" :language t))
         *current-language* 
         )
    (print (list sol-file tmp-file fasl-file lib-file html-file graph-file isa-graph-file))
    (print  ; for printing eventual error message
     (catch :error
       ;; initialize ontology title to the value from the sol.properties file
       ;(setq *ontology-title* sol-owl::*ontology-title*)
       (format t "~&=====> *ontology-title* ~s" *ontology-title*)
       (progn
         ;; create a working file without the leading UTF-8 mark by copying sol-file
         (trim-file sol-file tmp-file)

;(break "compile-sol")

         ;; open the output log file
         (with-open-file (*log-file* (or log-file t) :direction :output 
                                    :if-exists :supersede)
         
        ;; when the function is called without calling first the OWL compiler, we
        ;; must check syntax
        (when standalone
          ;; print copyright and file name to the Tk window
          (trformat "~&~S~&~S~&~&verbose: ~S; language: ~S~2%" 
                    *copyright* sol-file verbose language)
          ;; then check parents and quote syntax errors, print statistics
          (trformat (check-syntax tmp-file))
          ;; if errors, then quit
          (if *syntax-errors* (return-from compile-sol nil))
          )
        
        ;; otherwise, syntax (parents matches) has been checked while compiling for OWL
        ;; compile SOL file to expand HTML macro definitions
        (multiple-value-bind (compiled-file warnings errors) (compile-file tmp-file)
          (declare (ignore warnings))
          (when (or errors (null compiled-file))
            (trformat "~&Some format errors in the SOL file... ")
            (compile-file tmp-file :verbose t :print t)
            (return-from compile-sol nil))
          
          ;; no compilation errors, we proceed using the compiled file
          ;; reset chapter and section numbers, etc...
          (reset)
          (setq *current-language* language
                *cverbose* verbose)
          ;(tkaformat " current-language: ~S" *current-language*)
          (with-open-file (*output* (or html-file t) :direction :output 
                                    :if-exists :supersede)
            (with-open-file (*graph* graph-file :direction :output 
                                     :if-exists :supersede)
              (with-open-file (*isa-graph* isa-graph-file :direction :output 
                                           :if-exists :supersede)
                
                (vformat "~2&Compiler Pass : 1~&~
                          =================~2%")
                (load compiled-file)
                ;; at the end of first pass process index table
                (make-html-index-header)
                ;; second pass
                (setq *compiler-pass* 2)
                (vformat "~2&Compiler Pass : 2~&~
                          =================~2%")
                (load compiled-file)
                ;; closing mention
                (hformat "<table
 style=\"text-align: center; width: 100%; height: 32px;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td
 style=\"text-align: center; background-color: rgb(76, 89, 255); vertical-align: center;\">
      <h2>
      <font color=\"yellow\">End ~A ONTOLOGY </font>
      </h2>
      </td>
    </tr>
  </tbody>
</table>~&" *ontology-title*)
                (hformat "~&</body>~&</html>")
                )))

          (trformat "~%End of HTML compilation")
          ) ; end multiple value bind
        ) ; end letf
       
       ;; clean up only in the no-error case
       (delete-file fasl-file)
       (delete-file tmp-file)
       ;(delete-file lib-file)
       )))
    :eoc))

#|
;;; reset *tk*
(setq cl-user::*tk* nil)
;;; build HTML and text output: English, 
(sol-html::compile-sol "/Users/barthes/MOSS projects/sol-tests/test0.sol" :verbose t)
(sol-html::compile-sol "/Users/barthes/MOSS projects/sol-tests/test1.sol" :verbose t)
(sol-html::compile-sol "/Users/barthes/MOSS projects/sol-tests/test2.sol" :verbose t)
(sol-html::compile-sol "/Users/barthes/MOSS projects/sol-tests/test3.sol" :verbose t)
(sol-html::compile-sol "/Users/barthes/MOSS projects/sol-tests/test4.sol" :verbose t)
;;; virtual information is ignored in the htmml file
(sol-html::compile-sol "/Users/barthes/MOSS projects/sol-tests/tg-virtual.sol" :verbose t)
|#
;;;================================= functions ====================================

;;;--------------------------------------------------------- get-attribute-type

(defun get-attribute-type (options)
  "looks for a :type option in the list of options of an attribute. It there, ~
      returns the XSD type, otherwise returns nil.
Arguments:
   options: e.g. ((:name ...)(:type :integer)(:unique)...)
Return:
   XSD type string."
  (or (cdr (assoc (cadr (assoc :type options)) *attribute-types*)) "string"))

#|
? (get-attribute-type '((:name :en "street")(:type :integer)(:unique)))
"integer"
|#
;;;------------------------------------------------------------ get-cardinality

(defun get-cardinality (options)
  "looks for a cardinality option: min, max or unique. returns a list of one ~
      value if max=min or two values otherwise, or nil if option not there.
Arguments:
   options:  e.g. ((:name ...)(:type :integer)(:unique)...)
Return:
   string as a list of min max, e.g. \"(1 1) \"or \"(2 3)\" or empty string"
  (let ((min (cadr (assoc :min options)))
        (max (cadr (assoc :max options))))
    (cond
     ((assoc :unique options) (format nil " ~S"'(1 1)))
     ((and min max ) (format nil " ~S" (list min max)))
     (min (format nil " ~S" (list min '-)))
     (max (format nil " ~S" (list 0 max)))
     (t ""))))

#|
? (get-cardinality '((:name :en "street")(:type :integer)(:unique)))
"(1 1)"
? (get-cardinality '((:name :en "street")(:type :integer)(:min 2)))
"(2 -)"
? (get-cardinality '((:name :en "street")(:type :integer)(:max 2)))
"(0 2)"
? (get-cardinality '((:name :en "street")(:type :integer)(:min 1)(:max 3)))
"(1 3)"
|#
;;;------------------------------------------------------------- get-first-name

(defun get-first-name (name-string)
  "extracts the first name from the name string. Names are separated by semi-columns.
   E.g. \"identity card ; papers\" returns \"identity card \"
Argument:
   name-string
Return:
   string with the first name."
  (subseq name-string 0 (position #\; name-string)))

#|
? (get-first-name "frangin; frère")
"frangin"
|#
;;;-------------------------------------------------------------- get-id-string

(defun get-id-string (mln)
  "extracts from a multilingual list the string that will be used to build an OWL ID.
The algorithm is to take the first English name, of else the first name in the list.
Arguments:
   ll: a mls, e.g. (:fr \"masculin\" :en \"male   AGAIN\")
Return:
   a candidate string to be further processed."
  (mln::get-canonical-name mln))

#|
? (get-id-string '(:fr "masculin" :en "male   AGAIN"))
"male   AGAIN"
:EN
? (get-id-string '(:fr "masculin" :it "male   AGAIN"))
"masculin"
:FR
|#
;;;--------------------------------------------------- get-id-string-from-entry

(defun get-id-string-from-entry (text)
  "takes a string input that is a synonymous to the class of the object and ~
      recover the string of the corresponding id.
   E.g., \" adresse Email \" -> \"Z-Courriel\"
Arguments:
   text: the entry string
   type: :class :att :rel :ins
Return:
   nil or the corresponding id string."
  (let ((ref-string (get-reference-string text)))
    (car (index-get ref-string :class :id))))

#|
? (get-id-string-from-entry "adresse email ")
"Z-Courriel"
? (get-id-string-from-entry "courriel ")
"Z-Courriel"
? (get-id-string-from-entry "ship")
NIL
? (get-id-string-from-entry "adresse email ; courriel")
NIL
|#
;;;------------------------------------------------------------------ get-color
#+MCL
(defun get-color ()
  (multiple-value-bind (cr cb cg) (color-values (user-pick-color))
    (format t "~&Red: ~S ; Blue: ~S ; Green: ~S"
            (floor (/ cr 256)) (floor (/ cb 256)) (floor (/ cg 256)))))

;;;-------------------------------------------------------- get-language-string

(defun get-language-string (multilingual-string language-tag)
  "get the string corresponding to the language tag (return English as default).
Arguments:
   multilingual-string: e.g. (:en \"town\" :fr \"ville\)
   language-tag: e.g. :fr 
Return:
   string or empty-string"
  (car (mln::extract multilingual-string :language language-tag :always t)))

#|
? (get-language-string '(:en "town" :fr "ville") :fr)
"ville"
? (get-language-string '(:en "town" :fr "ville") :it)
"town"
? (car (mln::extract '(:en "town" :fr "ville;cité") :language :fr))
"ville"
? (car (mln::extract '(:en "town" :fr "ville") :language :de :always t))
"town"
? (car (mln::extract '(:zh "城市" :fr "ville") :language :de :always t))
"城市"
(car (mln::extract nil :language :de :always t))
Error: illegal mln format: NIL
|#
;;;----------------------------------------------------------------- get-one-of

(defun get-one-of (options)
  "check for the one-of option. if there list the implied values. The one-of ~
      option is global and should be collected over all the attribute definitions...
Arguments:
   options: e.g. ((:name ...)(:type :integer)(:unique)(:one-of 1 2 3))
Return:
   the list of values or empty string"
  (let ((values (cdr (assoc :one-of options))))
    (if values (format nil "~{~S~^, ~}" values) "")))

#|
? (get-one-of '((:name :en "street")(:type :integer)(:unique)(:one-of 1 2 3)))
"1, 2, 3"
|#
;;;----------------------------------------------------------- get-print-string

(defun get-print-string (text)
  "takes a string as input and returns the string to be printed.
Argument:
   text: a string
Return:
   a header string."
  (let ((id (make-index-string (get-reference-string text))))
    (car (index-get id :hdr :prt))))

#|
? (get-print-string " year number ")
"Numéro de l'année"
? (get-print-string " region ")
"Région"
|#
;;;-------------------------------------------------- get-prop-reference-string

(defun get-prop-reference-string (name-string)
  "get the property referenced by the string in the right language. Returns ~
   the referenced string or nil if it does not exist in *current-language*.
Arguments:
   name-string: e.g. \"pays\"
Return:
   \"Country\"."
  (or (cadar (index-get (make-index-string name-string) :ref))
      ;; check if index entry
      (if (or (index-get (make-index-string name-string) :att :class-id) 
              (index-get (make-index-string name-string) :rel :class-id))
        (make-value-string name-string))))

#| 
? (get-prop-reference-string "code")
"Code"
? (get-prop-reference-string "country")
"Pays"
|#
;;;------------------------------------------------------- get-reference-string

(defun get-reference-string (name-string)
  "checks whether a string has a reference string as a synonym, and if so, ~
   replace it with the synonymous string.
   Normalizes it if in *current-language*, return nil otherwise.
Arguments:
   name-string: e.g. \"adresse   Postale\"
Return:
   normalized string or synonym, e.g. \"Postal Address\"."
  (or (cadar (index-get (make-index-string name-string) :ref))
      ;; check if index entry 
      (if (index-get (make-index-string name-string)) 
        (make-value-string name-string))))

#|
? ( GET-REFERENCE-STRING "adresse   Postale")
"Postal Address"
? ( GET-REFERENCE-STRING "Postal   address ")
"Postal Address"
? ( GET-REFERENCE-STRING "Ship ")
NIL
|#
;;;-------------------------------------------------------- get-relation-one-of

(defun get-relation-one-of (options)
  "check for the one-of option. if there, list the implied values. The one-of ~
      option is global and should be the same over all the attribute definitions...
Arguments:
   options: e.g. (:one-of (:en \"male\" :fr \"masculin\")
                          (:en \"female\" :fr \"féminin\"))
Return:
   the list of values or empty string"
  (let ((values (cdr (assoc :one-of (cdr options)))) string-list)
    ;; values is a list of (virtual) indviduals referenced by multilingual strings
    ;; considered if in *current-language* only.
    ;; it may be a list of references to existing individuals
    (cond
     ((every #'stringp values)
      ;; get print-names from index
      (setq string-list (mapcar #'(lambda (xx) (car (index-get xx :hdr :prt)))
                                values)))
     ;; otherwise try mln
     ((setq string-list
            (remove nil 
                    (mapcar #'(lambda(xx) (has-language-string? xx *current-language*))
                            values))))
     )
    (if string-list (format nil "~{~A~^, ~}" string-list) "")))

#|
? (get-relation-one-of '((:en "gender" :fr "sexe") (:one-of (:en "male" :fr "masculin")
                                           (:en "female" :fr "fÃ©minin"))))
"male, female"
|#
;;;------------------------------------------------------------- get-successors

(defun get-successors (suc-list)
  "gets the successors for an individual.
Argument:
   suc-list: a list of strings specifying the successors
Return:
   an HTML string for printing the successors."
  (format nil "~{~{<a href=\"#~A\">~A</a>~}~^, ~}" 
          ;; get nice printing names from internal index table
          (mapcar #'(lambda(xx) (list
                                 (make-individual-string (get-reference-string xx))
                                 (get-print-string xx)))
                  suc-list)))
#|
? (get-successors '(" dordogne " "lot et garonne"))
"<a href=\"#z-dordogne\">Dordogne</a>, <a href=\"#z-lotEtGaronne\">Lot et garonne</a>"
|#
;;;--------------------------------------------------------- get-successors-new

(defun get-successors-new (suc-list)
  "gets the successors for an individual.
Argument:
   suc-list: a list of strings specifying the successors
Return:
   an HTML string for printing the successors."
  (format nil "~{~{<a href=\"#~A\">~{~A~}</a>~}~^, ~}"
          (mapcar #'(lambda (xx) (list
                                  ;; get the reference to ?
                                  (make-individual-string xx)
                                  ;; insert the name to print
                                  (index-get (make-individual-string xx) :hdr :prt)))
                  suc-list)))

#|
? (get-successors-new '(_I-4 _I-7))
"<a href=\"#z-4\">John A. Barthès</a>, <a href=\"#z-7\">CUT</a>"
|#
;;;--------------------------------------------------------------------- get-to

(defun get-to (options)
  "check for the to option. if there list the implied values. The one-of ~
      option is global and should be collected over all the attribute definitions...
Arguments:
   options: e.g. ((:name ...)(:type :integer)(:unique)(:to \"person\" \"student\"))
Return:
   the list of values or empty string"
  (let ((values (cdr (assoc :to options)))
        ref-list)
    ;(dformat "to option: ~S" values)
    (setq ref-list (make-ref-concept-list values))
    ;(dformat "to ref-list: ~S" ref-list)
    ;(if values (format nil "~{<a href=\"#~A\">~:*~A</a>~^, ~}" values) "")
    (if ref-list
      (format nil "~{~{<a href=\"#~A\">~A</a>~}~^, ~}" 
              ;; get nice printing names from internal index table
              (mapcar #'(lambda (xx) 
                          (cons (car xx)
                                (index-get (make-index-string (cadr xx)) :hdr :prt)))
                      ref-list))
      "")))

#|
? (get-to '((:en "service provider organization") 
        (:to "Council" "municipality" "organization" "PréFecture" 
           "Sous-PréFecture")))
"<a href=\"#Z-Conseil\">Conseil</a>, <a href=\"#Z-Municipalité\">Municipalité</a>, 
<a href=\"#Z-Organisme\">Organisme</a>, <a href=\"#Z-PréFecture\">PréFecture</a>, 
<a href=\"#Z-Sous-PréFecture\">Sous-PréFecture</a>"
|#
;;;------------------------------------------------------- has-language-string?

(defun has-language-string? (multilingual-string language-tag)
  "get the string corresponding to the language tag.
Arguments:
   multilingual-string: e.g. (:en \"town\" :fr \"ville\)
   language-tag: e.g. :fr 
Return:
   string or nil"
  (cadr (member language-tag multilingual-string)))

#|
? (has-language-string? '(:en "town" :fr "ville") :fr)
"ville"
? (has-language-string? '(:en "town" :fr "ville") :it)
NIL
|#
;;;------------------------------------------------------------ index-attribute

(defun index-attribute (option concept-id)
  "create entries in the INDEX table for each national name of the attribute. If ~
      national name for the attribute does not exist skip attribute.
Argument:
   option: e.g. (:att (:en \"name\" :fr \"nom\") (:type :name)(:unique))
Return:
   T"
  (let (name-list)
    ;; get list of synonyms
    (setq name-list (mln::filter-language (cadr option) *current-language*))
    ;; return if list is empty
    (unless name-list (return-from index-attribute nil))
    ;; otherwise index attribute ("name" :att "Z-Person")
    (dolist (entry name-list)
      ;; add entry for the attribute
      (push (make-string-header entry) *attribute-list*)
      ) 
    ;; reuse
    (make-national-indices-and-synonyms (cadr option) concept-id :att :class-id)
    t))

#|
? (index-clear)
T
? (index-attribute '(:att (:en "name" :fr "nom") (:type :name)(:unique)) "Z-Territory")
T
? (index)

("Name" ((:ATT (:CLASS-ID "Z-Territory")))) 
:END

? (let ((*current-language* :fr))
    (index-clear)
    (index-attribute '(:att (:en "name" :fr "nom ; désignation du territoire")
                       (:type :name)(:unique)) "Z-Territory")
    (index))

("DésignationDuTerritoire" ((:REF (:FR "Nom")) (:ATT (:CLASS-ID "Z-Territory")))) 
("Name" ((:REF (:EN "Nom")))) 
("Nom" ((:ATT (:CLASS-ID "Z-Territory")))) 
:END
|#
;;;--------------------------------------------------------------- index-one-of

(defun index-one-of (option concept-id)
  "create entries in the INDEX table for each national name of the relation. If ~
  national name for the relation does not exist skip relation.
  Argument:
  option: e.g. (:one-of
  (:en \"consequence\" :fr \"conséquence\")
  (:en \"postcondition\" :fr \"postcondition\")
  (:en \"output\" :fr \"sortie\"))
  Return:
  T"
  (let (name-list)
    ;; otherwise index attribute ("name" :att "Z-Person")
    (dolist (entry (cdr option))
      (setq name-list (mln::filter-language entry *current-language*))
      ;; add entry for the individual
      (dolist (name name-list)
        (push (make-string-header name) *individual-list*)
        (index-add (make-index-string name) :one-of :class-id concept-id))
      (make-national-indices-and-synonyms entry concept-id :one-of :class-id))
    t))

#|
? (let ((*current-language* :fr))
    (index-clear)
    (index-one-of '(:one-of
                    (:en "consequence ; conclusion" :fr "conséquence")
                    (:en "postcondition" :fr "postcondition")
                    (:en "output; exit" :fr "sortie")) 
                  "Z-OutcomeType")
    (index))

("Conclusion" ((:REF (:EN "Conséquence")))) 
("Consequence" ((:REF (:EN "Conséquence")))) 
("Conséquence" ((:ONE-OF (:CLASS-ID "Z-OutcomeType")))) 
("Exit" ((:REF (:EN "Sortie")))) 
("Output" ((:REF (:EN "Sortie")))) 
("Postcondition" ((:ONE-OF (:CLASS-ID "Z-OutcomeType")))) 
("Sortie" ((:ONE-OF (:CLASS-ID "Z-OutcomeType")))) 
:END
|#
;;;------------------------------------------------------------- index-relation

(defun index-relation (option concept-id)
  "create entries in the INDEX table for each national name of the relation. If ~
      national name for the relation does not exist skip relation.
Argument:
   option: e.g. (:rel (:en \"country\" :fr \"pays\") (:to \"country\")
        (:doc :en \"qualifies the country\" :fr \"a pour pays\"))
Return:
   T"
  (let (name-list)
    ;; get the list of relation names
    (setq name-list (mln::filter-language (cadr option) *current-language*))
    ;; if nil, then relation does not eist in this language, don't index it
    (unless name-list (return-from index-relation nil))

    ;; otherwise index attribute ("name" :att "Z-Person")
    (dolist (entry name-list)
      ;; add entry for the relation
      (push (make-string-header entry) *relation-list*)
      ) 
    ;; reuse
    (make-national-indices-and-synonyms (cadr option) concept-id :rel :class-id)
    t))

#|
? (progn
    (index-clear)
    (index-relation '(:rel (:en "country" :fr "pays") (:to "country")
        (:doc :en "qualifies the country" :fr "a pour pays")) "Z-Territory")
     (index)
     )
("Country" ((:REL (:CLASS-ID "Z-Territory")) (:HDR (:PRT "Country")))) 
("Pays" ((:REF (:FR "Country")) (:HDR (:PRT "Pays"))))  
:END

? (let ((*current-language* :fr))
    (index-clear)
    (index-relation '(:rel (:en "country" :fr "pays ; Élément du territoire")
                       (:doc :en "qualifies the country" :fr "a pour pays"))
                       "Z-Territory")
    (index))

("Country" ((:REF (:EN "Pays")) (:HDR (:PRT "Country")))) 
("Pays" ((:REL (:CLASS-ID "Z-Territory")) (:HDR (:PRT "Pays")))) 
("ÉlémentDuTerritoire" ((:REF (:FR "Pays")) (:REL (:CLASS-ID "Z-Territory"))
  (:HDR (:PRT "Élément du territoire"))))
:END
|#
;;;------------------------------------------------------------- make-attribute

(defun make-attribute (options)
  "prints an attribute
Arguments:
   options: attribute-options, i.e., name, type, cardinality, one-of
Return:
   nil"
  (hformat "~&<table
 style=\"text-align: left; width: 100%;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
  <tbody>
    <tr>
      <td style=\" text-align: left; width: 25%;\">
      <b>~{~A~^ / ~}~A~A: </b>
      </td>
      <td style=\"text-align: left; width: 15%;\">
      ~A
      </td>
      <td style=\" text-align: left; width: 60%;\">      
      ~A
      </td>
    </tr>
  </tbody>
</table>
"
           (mapcar #'make-string-header
                   (mln::filter-language (car options) *current-language*))
           (get-cardinality options)
           (if (eql *current-language* :fr) " " "")
           ;; print type of value, e.g. (:type :positive-integer)
           (get-attribute-type options)
           ;; print the list of :one-of option
           (get-one-of options))
  nil)

#|
? (let ((*current-language* :fr))
    (index-clear)
    (make-attribute '((:en "age ; oldness" :fr "âge")
                      (:max 1)
                      (:one-of 10 20 30)
                      (:doc :en "qualifies the country" :fr "a pour pays")
                     ))
    (index))

<table
 style="text-align: left; width: 100%;"
 border="0" cellpadding="2" cellspacing="0">
  <tbody>
    <tr>
      <td style=" text-align: left; width: 25%;">
      <b>Âge (0 1) : </b>
      </td>
      <td style="text-align: left; width: 15%;">
      string
      </td>
      <td style=" text-align: left; width: 60%;">      
      10, 20, 30
      </td>
    </tr>
  </tbody>
</table>
:END

|#
;;;------------------------------------------------------- make-attribute-value
;;; a value can be a string, a number, an mln, a list of string or a list of numbers

(defun make-attribute-value (value-list)
  "takes a list of options, checks if mln, and rreturns a string with values 
  separated by commas"
  (let (string-list)
    (setq string-list
          (if (mln::mln? (car value-list))
            (mln::filter-language (car value-list) *current-language*)
            value-list))
    (format nil "~{~A~^, ~}" string-list)))

#|
(make-attribute-value '((:en "Londres ; the City" :fr "Londres")))
"Londres, the City"
|#
;;;--------------------------------------------------------------- make-chapter

(defun make-chapter (options)
  " prints a chapter title.
Arguments:
   options:
     multilingual-string giving the title of the chapter
     (:number nn) reset the chapter number to nn
     (:no-number) does not print a number
Return:
   nil"
  ;(print mls)
  (when (eql *compiler-pass* 2)
    ;; when we do not want a number we center the title
    (if (assoc :no-number options)
      (hformat "<table
 style=\"text-align: center; width: 100%; height: 32px;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td style=\"background-color: rgb(76, 89, 255); vertical-align: center;\">
      <h3>
        <font color=\"yellow\"> ~A</font>
      </h3>
      </td>
    </tr>
  </tbody>
</table>" 
               (get-language-string (car options) *current-language*))  
      ;; otherwise print a number and left justify
      (hformat "<table
 style=\"text-align: left; width: 100%; height: 32px;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td style=\"background-color: rgb(76, 89, 255); vertical-align: center;\">
      <h3>
        <font color=\"yellow\"> ~A. ~A</font>
      </h3>
      </td>
    </tr>
  </tbody>
</table>"
               ;; number could be reset
               (if  (assoc :number options)
                 (setq *chapter-counter* (cadr (assoc :number options)))
                 (incf *chapter-counter*))               
               (get-language-string (car options) *current-language*)))
    ;; reset section number 
    (setq *section-counter* 0)
    nil))

;;;--------------------------------------------------------------- make-concept

(defun make-concept (concept-name &rest options)
  "produces a list of strings corresponding to the OWL format for defining the concept.
This function is called twice: once during the first pass, another time during the second pass.
Arguments:
   concept-name: a multi-lingual :name list
   options: a possible sequence of SOL options
Return:
   NIL."
  (let (name-list id )
    ;; first check that the concept has a definition in the current language
    ;; if concept has no value for current language, quit
    ;; this is because in multi-lingual ontologies some entries are specific to 
    ;; a language and may not mean anything in another one, thus we ignore the
    ;; corresponding concept
    (unless (member *current-language* (mln::get-languages concept-name)) 
      (return-from make-concept nil))

    ;; get the list of synonyms for the current language e.g. ("person" "guy")
    (setq name-list (mln::extract concept-name :language *current-language*))

    ;; otherwise, depends on compiler pass
    (cond 
     ((eql *compiler-pass* 1)
      ;; first compiler pass, we collect classes and build index entries
      ;; get the concept name in the current language
      ;; all errors should have been detected using the SOL2OWL compiler
      (setq id (make-concept-string (car name-list)))
      (vformat "~%===== ~S" id)
      ;; save all concept names (including synonyms) onto the concept list
      (setq *concept-list* 
            (append *concept-list*
                    (mapcar #'make-string-header name-list)))
      ;; return the list of index entries
      (make-national-indices-and-synonyms concept-name id :class)
      ;; index properties and relations, with special case of concept one-of
      (dolist (option options)
        (case (car option)
          (:att
           (index-attribute option id))
          (:rel
           (index-relation option id))
          (:one-of
           (index-one-of option id))))
      nil)
     
     ;;===== second compiler pass
     ((eql *compiler-pass* 2)
      ;; compute string id of concept
      (setq id (make-concept-string (car name-list)))
      (vformat "~%===== ~S" id)
      ;; include anchor
      (hformat "~%<a name=\"~A\" />" id)
      ;; name of concept (all of the national names)
      (hformat "~2%<table
 style=\"text-align: center; width: 100%; height: 32px;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td 
 style=\"text-align: left; background-color: rgb(255, 255, 102); 
 vertical-align: center; width: 20px\">
      <a href=\"#top_of_ontology\">TOP</a>
      </td>
      <td
 style=\"text-align: center; background-color: rgb(255, 255, 102); 
 vertical-align: center;\">
      <h2>
      <center>~{~A ~^/ ~}</center>
      </h2>
      </td>
    </tr>
  </tbody>
</table>"
               (mapcar #'make-value-string-lite name-list))
      ;; documentation
      (make-documentation (assoc :doc options))
      ;; is-a, if there
      (make-is-a (cdr (assoc :is-a options)))
      ;; one-of option
      (dolist (option options)
        (if (eql (car option) :one-of)
          (make-one-of (cdr option))))
      ;; attributes
      (dolist (option options)
        (if (eql (car option) :att)
          (make-attribute (cdr option))))
      ;; relations
      (dolist (option options)
        (if (eql (car option) :rel)
          (make-relation (cdr option))))
      ;; skip a few lines
      (hformat "~&<br><br>")
      
      ;;=== Prepare grapher file
      ;; first id of the concept
      (gformat "~&~A " id)
      (sformat "~&~A " id)
      ;; add is-a concept or "Thing"
      (gformat "~{~A ~}" 
               (or (mapcar #'(lambda(xx) (make-concept-string xx))
                           (remove nil (mapcar #'get-reference-string 
                                               (cdr (assoc :is-a options)))))
                   '("Thing")))
      (sformat "~{~A ~}" 
               (or (mapcar #'(lambda(xx) (make-concept-string xx))
                           (remove nil (mapcar #'get-reference-string 
                                               (cdr (assoc :is-a options)))))
                   '("Thing")))
      ;; add all the neighbors
      (dolist (option options)
        (if (eql (car option) :rel)
          (gformat "~{~A ~}" 
                   (or (mapcar #'car (make-ref-concept-list 
                                      (cdr (assoc :to (cdr option)))))
                       '("")))))
      ;;=== End grapher file
      
      ;; return nil
      nil))))

#|
? (progn
    (reset)
    (let ((*current-language* :fr))
      (dolist (*compiler-pass* '(1 2))
        (defconcept 
          ((:en "EMail Address") (:fr "courriel" "adresse email") (:it "Indirizzo EMail"))
          (:is-a "Evidence Place Holder" "identifier")
          (:att (:en "email address" :fr "courriel"))
          (:doc :en "an E-Mail-Address is something like forname.name@domain.country"
                :fr "Identifiant personnel d'un internaute grÃÂ¢ce auquel il peut communiquer par courrier Ã©lectronique avec d'autres internautes."))
        )
      (index)
      *concept-list*))

===== "Z-Courriel"
===== "Z-Courriel"
<a name="Z-Courriel" />

<table
 style="text-align: center; width: 100%; height: 32px;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td 
 style="text-align: left; background-color: rgb(255, 255, 102); 
 vertical-align: center; width: 20px">
      <a href="#top_of_ontology">TOP</a>
      </td>
      <td
 style="text-align: center; background-color: rgb(255, 255, 102); 
 vertical-align: center;">
      <h2>
      <center>Courriel / Adresse email </center>
      </h2>
      </td>
    </tr>
  </tbody>
</table>
<table
 style="text-align: left; width: 100%;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="background-color: rgb(255, 255, 202)">
      Identifiant personnel d'un internaute grâce auquel il peut communiquer par courrier Ã©lectronique avec d'autres internautes.
      </td>
    </tr>
  </tbody>
</table>
<br>
<table
 style="text-align: left; width: 100%;"
 border="0" cellpadding="2" cellspacing="0">
  <tbody>
    <tr>
      <td style=" text-align: left; width: 25%;">
      <b>Courriel : </b>
      </td>
      <td style="text-align: left; width: 15%;">
      string
      </td>
      <td style=" text-align: left; width: 60%;">      
      
      </td>
    </tr>
  </tbody>
</table>
<br><br>
("AdresseEmail" ((:REF (:FR "Courriel")) (:CLASS (:ID "Z-Courriel")))) 
("Courriel" ((:ATT (:CLASS-ID "Z-Courriel")) (:CLASS (:ID "Z-Courriel")))) 
("EmailAddress" ((:REF (:EN "Courriel")))) 
("IndirizzoEmail" ((:REF (:IT "Courriel")))) 
("Courriel" "AdresseEmail")
|#
;;;-------------------------------------------------------- make-concept-string

(defun make-concept-string (name-string)
  "makes a concept string from a user provided string.
Argument:
   name-string: string 
Return:
   concept-id string."
  (apply #'concatenate 'string "Z-" 
         (make-text-list (make-value-string name-string))))

#|
? (MAKE-CONCEPT-STRING "Time    unit")
"Z-TimeUnit"
|#
;;;--------------------------------------------------------- make-documentation

(defun make-documentation (doc)
  "prints a concept.
Arguments:
   doc: documentation mls or nil
Return:
   nil"
  (let ((national-text (has-language-string? doc *current-language*)))
    (when national-text
      (hformat "~&<table
 style=\"text-align: left; width: 100%;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td style=\"background-color: rgb(255, 255, 202)\">
     ~A
      </td>
    </tr>
  </tbody>
</table>
<br>"
               (remove "~" (format nil " ~A" national-text)
                       :test #'string-equal))) ; remove "~"
    nil))

;;;--------------------------------------------------------- make-file-pathname

(defun make-file-pathname (base-file extension &key language)
  "makes a new file pathname for the output files.
Arguments:
   base-file: input file pathname
   extension: output extension
   language (key): language flag if true adds a language spec
Return:
   a file pahtname."
  (make-pathname
   :device (pathname-device base-file)
   :directory (pathname-directory base-file)
   :name (if language
           (concatenate 'string (pathname-name base-file) "-"
                        (format nil "~A" sol::*sol-language*))
           (pathname-name base-file))
   :type extension))

#|
? (make-file-pathname (make-pathname ; ancillary file
                    ;:device (pathname-device sol-file)
                    :directory "albert"
                    :name "myfile"
                    :type "lib")
                    "html" :language t)
#P"albert:myfile-EN.html"
? (make-file-pathname (make-pathname ; ancillary file
                    ;:device (pathname-device sol-file)
                    :directory "ontologies"
                    :name "test0"
                    :type "sol")
                    "lib" )
#P"ontologies:test0.lib"
|#
;;;----------------------------------------------------- make-html-index-header

(defun make-html-index-header ()
  "build a list of anchor references for concepts, attributes, relations and ~
      individuals. Use alphabetical order.
Arguments:
   none
Return:
   t"
  ;; build a sequence of references for classes
  (hformat "<h3>~A</h3>"
           (get-language-string *class-title* *current-language*))
  (setq *concept-list* (sort (delete-duplicates *concept-list* :test #'string-equal) 
                             #'string-lessp))
  ;(dformat "Concept-list: ~&" *concept-list*)
  (hformat "~&~{<a href=\"#~A\">~A</a>~^, ~}"
           (mapcan #'(lambda(xx)
                       (list (car (index-get (make-index-string xx) :class :id)) xx))
                   *concept-list*))
  ;; same for attributes
  (hformat "<h3>~A</h3>"
           (get-language-string *attribute-title* *current-language*))
  (setq *attribute-list* (sort (delete-duplicates *attribute-list* :test #'string-equal)
                               #'string-lessp))
  (hformat "~&~{~A (~{<a href=\"#~A\">~A</a>~^, ~})~^, ~}"
           (mapcan #'(lambda(xx)
                       (list xx 
                             ;; remove first 2 chars ("Z-")
                             (mapcan #'(lambda(yy) (list yy (subseq yy 2))) 
                                     (index-get (make-index-string xx) :att :class-id))))
                   *attribute-list*))
  ;; same for relations
  (hformat "<h3>~A</h3>"
           (get-language-string *relation-title* *current-language*))
  (hformat "~&~{~A (~{<a href=\"#~A\">~A</a>~^, ~})~^, ~}"
           (mapcan #'(lambda(xx)
                       (list xx 
                             ;; remove first 2 chars
                             (mapcan #'(lambda(yy) (list yy (subseq yy 2))) 
                                     (index-get (make-index-string xx) :rel :class-id))))
                   (sort (delete-duplicates *relation-list* :test #'string-equal)
                         #'string-lessp)))
  ;; individuals
  (hformat "<h3>~A</h3>"
           (get-language-string *individual-title* *current-language*))
  (setq *individual-list* 
        (sort (delete-duplicates *individual-list* :test #'string-equal)
              #'string-lessp))
  (when *individual-list*
    (hformat "~&~{~{<a href=\"#~A\">~A</a>~}~^, ~}"
             (mapcar 
              #'(lambda(xx)
                  (cons  xx ;(or xx
                            ;(car (index-get (make-index-string xx) :one-of :class-id)))
                        (index-get xx :HDR :PRT)))
              *individual-list*)))
  ;; end of index
  (hformat "<br><br>")
  )
#| 
<a href="Z-Territory">Territoire</a>, ...
PostalCode (<a href="#Z-PostalAddress">PostalAddress</a>), ...
|#
;;;----------------------------------------------------------- make-id-from-mln

(defun make-id-from-mln (mln)
  "produces a key, e.g. \"z-JeanPaulBarthès\" from an mln"
  (moss::string+ 
   "z-" (car (mln::extract mln :language *current-language* :always t))))

#|
(make-id-from-mln '(:en "Bob;Bill" :fr "William"))
"z-Bob"
|#
;;;---------------------------------------------------------- make-index-string

(defun make-index-string (name-string)
  "makes a concept string from a user provided string, e.g. \"time unit\".
Argument:
   name-string: string 
Return:
   concept-id string."
  (apply #'concatenate 'string  
         (make-text-list (make-value-string name-string))))

#|
? (make-index-STRING "Time Unit")
"TimeUnit"
|#
;;;--------------------------------------------------------------- make-indices

(defun make-indices (name-option value-string data-type)
  "takes a name option and builds all possible indices from it.
   E.g. ({:name} :en \"man of war ; ship\" :fr \"navire\")
   will produce the following entries: ManOfWar, Ship, Navire
   Uses the *language-tags* global variable.
Arguments:
   name-option: a multi-lingual list
   value-string: string respresenting the concept id or property id, e.g. \"z-london\"
   data-type: :class :att :rel or :inst
Return:
   a list of OWL index objects
Error:
   throws to an :error tag."
  (let (result index-list option-list index-string)
    ;; check name-option format removing the possible :name tag
    (unless (setq option-list (multilingual-name? name-option))
      (terror "Unknown language tag ~S in ~S" (car option-list) name-option))

    ;; then loop on the various languages
    (dolist (tag (mln::get-languages option-list))
      ;; extract the list of names
      (setq index-list (mln::filter-language option-list tag))
      ;; for each name build an index object
      (dolist (index index-list)
        ;; cook up index string
        (setq index-string (make-index-string index))
        ;; save string for prettier printing
        (unless (index-get index-string :hdr :prt)
          (index-add index-string :hdr :prt (make-string-header index)))
        ;; insert entry into *index* hash table
        (index-add index-string data-type :id value-string)
        (index-add index-string :idx tag value-string)
        (pushnew index-string *index-list* :test #'string-equal)
        )
      )
    ;; return
    result
    ))

#|
? (progn 
   (index-clear)
   (make-indices '(:en "first name; given name") "hasFirstName" :att)
   (index))

("FirstName"
 ((:IDX (:EN "hasFirstName")) (:ATT (:ID "hasFirstName"))
  (:HDR (:PRT "First name")))) 
("GivenName"
 ((:IDX (:EN "hasFirstName")) (:ATT (:ID "hasFirstName"))
  (:HDR (:PRT "Given name")))) 
:END
|#
;;;------------------------------------------------------------ make-individual
;;; we have a problem with individuals
;;; In the SOL environment, individuals has a :name property that contained an
;;; mln value to name the object.
;;; In MOSS this is not the case. AN individual is defined as e.g.
;;; (DEFINDIVIDUAL "PERSON" ("NAME" "Labrousse")
;;;    ("FIRST-NAME" "Michel")
;;;    ("AGE" 52)
;;;    ("SEX" "m")
;;;    ("WIFE" _I-3)
;;;    ("DAUGHTER" _I-10 _I-11 _I-12)
;;;    (:VAR _I-14)
;;;    (:NAME :fr "Michel Labrousse" :en "Michel Labroussse")
;;;    )
;;; Nothing actually characterize the individual, except its internal id assigned
;;; when it is created. Entry points point to the object but are not unique. Thus, 
;;; it is difficult to rebuild a unique name from the instantiated object.
;;; one possibility is to ask the person building the KB to assign a unique key
;;; (an mln) to very object in the KB. Another one would be to use the name of the
;;; class and the sequence number of the MOSS internal id.However, in reconstructed 
;;; MOSS files, the :var option is unique and can define the object. We use this 
;;; property here.

(defun make-individual (class-ref &rest options)
  "prints an individual.
Arguments:
   class-ref: e.g. \"Person\"
   options (rest): e.g. ({(<att ref> ...)}*
                         {(<rel ref> ...)}*
                         {(:doc ...)}
                         (:name ...) ; mln result of =summary method (old format)
                         (:var _I-xxx)
                         )
                   other options are ignored
Return:
   nil"
  (let ((obj-ref (cdr (assoc :name options))) ; an MLN, e.g. ((:en "London")...)
        name-list id prop)
    ;; first get the list of print names for the current language  ("Bob" "Bill")
    (setq name-list (mln::filter-language obj-ref *current-language*))
    ;; if empty, quit
    (unless name-list (return-from make-individual nil))

    (vformat "~%===== ~S" (make-id-from-mln obj-ref)) ; e.g. "z-london"
    ;; remove :name option from options
    (setq options (remove :name options :key #'car))
    ;; get the individual id from the :var option -> e.g. "z-33"
    (setq id (make-individual-id (cadr (assoc :var options))))
    ;; remove :var option from options
    (setq options (remove :var options :key #'car))
    
    (cond 
     ;;===== FIRST COMPILER PASS
     ((eql *compiler-pass* 1)
      ;; during pass 1, we collect individuals and build index entries
      ;; create an index entry, e.g. ("z-3" ((:HDR (:PRT "London"))))
      (index-add id :hdr :prt (format nil "~{~A~^, ~}" name-list))
      ;; save reference onto the ad hoc list, e.g. "z-3"
      (pushnew id *individual-list*)
      )
     
     ;;===== SECOND COMPILER PASS
     ((eql *compiler-pass* 2)
      ;; build the HTML output
      ;(setq name (get-first-name (car name-lisst)))
      ;; include anchor
      (hformat "<a name=\"~A\" />" id)
      ;;=== set up grapher entry
      (gformat "~&~A " (make-id-from-mln obj-ref))
      ;; name of concept
      (hformat "~2%<table
 style=\"text-align: center; width: 100%; height: 32px;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td 
 style=\"text-align: left; background-color: rgb(219, 253, 149); 
 vertical-align: center; width: 20px\">
      <a href=\"#top_of_ontology\">TOP</a>
      </td>
      <td
 style=\"text-align: center; background-color: rgb(219, 253, 149); 
 vertical-align: center;\">
      <h2>
      <center>~{~A ~^/ ~}</center>
      </h2>
      </td>
    </tr>
  </tbody>
</table>"
               (mapcar #'make-string-header name-list))
      
      ;; documentation removing the class spec
      ;(make-documentation (assoc :doc (cdr options)))
      
      ;; class
      (hformat "~%<table
 style=\"text-align: left; width: 100%; color: rgb(118, 183, 78); \" 
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td 
 style=\"text-align: left; width: 20%\">
      <b>~A</b>
      </td>
      <td style=\"width: 80%;\">
      <a href=\"#~A\">~A</a>
      </td>
    </tr>
  </tbody>
</table>"
               (get-language-string *class-header* *current-language*) ; header
               (car (index-get
                     (make-index-string (get-reference-string class-ref))
                     :class :id))
               ;; get the print name of the class
               (get-print-string class-ref)
               )
      ;;=== grapher entry
      (gformat "~A " (make-concept-string (get-reference-string class-ref)))
      ;; documentation
      (make-documentation (assoc :doc options))
      ;; properties
      (dolist (option options)
        (unless (eql (car option) :doc)
          ;;***** test prop for att or rel
          (setq prop (make-index-string (get-reference-string (car option))))
          ;; if prop is nil we skip the rest (may be a bad idea)
          (when prop
            (cond

             ;;===== ATTRIBUTE
             ((index-get prop :att)
              (hformat "~%<table
 style=\" width: 100%; \"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td 
 style=\"text-align: left; width: 20%\">
      <b>~A~A:</b>
      </td>
      <td
 style=\"width: 80%;\">
      ~{~A~^, ~}
      </td>
    </tr>
  </tbody>
</table>"
                       (get-print-string (car option))
                       (if (eql *current-language* :fr) " " "") ;French spacing
                       ;(mapcar #'make-value-string (cdr option))
                       (if (mln::mln? (cadr option))
                           (mln::filter-language (cadr option) *current-language*)
                           (cdr option))
                       ))

             ;;===== RELATION (otherwise assume it is a relation)
             ((index-get prop :rel)

;(break "make-individual 2; prop: ~S, option: ~S" prop option)

              (hformat "~%<table
 style=\"text-align: left; width: 100%; color: rgb(0, 0, 221); \" 
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td 
 style=\"text-align: left; width: 20%\">
      <b>~A~A:</b>
      </td>
      <td
 style=\"width: 80%;\">
      ~A
      </td>
    </tr>
  </tbody>
</table>"
                       (get-print-string (car option)) ; property  name
                       (if (eql *current-language* :fr) " " "") ;French spacing
                       ;; build a line for each successor
                       (get-successors-new (cdr option)))
              ;;=== grapher entry
              (gformat "~{~A ~}"
                       (or (mapcar #'(lambda(xx) 
                                       (make-individual-string 
                                        (get-reference-string xx)))
                                   (cdr option))
                           '("")))
              )))))
      ;; skip a few lines
      (hformat "~&<br><br>")
      ;; return nil
      nil))))

#|
? (defun tt ()
    (reset)
    (let ((*current-language* :fr))
      (dolist (*compiler-pass* '(1 2))
        (defconcept (:name :en "Country" :fr "Pays")
          (:att (:name :en "name" :fr "nom")(:type :mln))
          )
        (defconcept (:name :en "City ; Town" :fr "Ville ; Cité")
          (:att (:name :en "name" :fr "nom")(:type :mln))
          (:rel (:name :en "country" :fr "pays")(:to "Country"))
          )
        (defindividual "Pays"
          (:name (:en "United Kingdom" :fr "Royaume Uni"))
          ("name" (:fr "Royaume Uni" :en "United Kongdom"))
          (:var _I-22)
          )
        (defindividual "City"
          (:name (:en "London" :fr "Londres"))
          ("name" (:en "London" :fr "Londres"))
	  ("pays" "Royaume Uni")
          (:var _I-33)
          )
      (print "=== Index")
      (index)
      (print "=== Concept list")
      (print *concept-list*)
      (print "=== Individual list")
      *individual-list*)))
(tt)

===== "Z-Pays"
===== "Z-Ville"
===== "z-bordeaux"
===== "Z-Pays"
<a name="Z-Pays" />
...
"=== Index"
("Bordeaux" ((:IDX (:FR "z-bordeaux") (:EN "z-bordeaux")) (:IND (:ID "z-bordeaux")) (:HDR (:PRT "Bordeaux")))) 
("City" ((:REF (:EN "Ville")) (:HDR (:PRT "City")))) 
("Cité" ((:REF (:FR "Ville")) (:CLASS (:ID "Z-Ville")) (:HDR (:PRT "Cité")))) 
("Country" ((:REF (:EN "Pays")) (:HDR (:PRT "Country")))) 
("Name" ((:REF (:EN "Nom")) (:HDR (:PRT "Name")))) 
("Nom" ((:ATT (:CLASS-ID "Z-Pays" "Z-Ville")) (:HDR (:PRT "Nom")))) 
("Pays" ((:REL (:CLASS-ID "Z-Ville")) (:CLASS (:ID "Z-Pays")) (:HDR (:PRT "Pays")))) 
("Town" ((:REF (:EN "Ville")) (:HDR (:PRT "Town")))) 
("Ville" ((:CLASS (:ID "Z-Ville")) (:HDR (:PRT "Ville")))) 
"=== Concept list" 
("Pays" "Ville" "Cité") 
"=== Individual list" 
("Bordeaux")
|#
;;;--------------------------------------------------------- make-individual-id

(defun make-individual-id (key &aux tag)
  "Parses the input, e.g. _I-345 to produce \"z-345\""
  (moss::string+ "z" (subseq (symbol-name key) 2)))

#|
? (make-individual-id '_I-444)
"z-444"
|#
;;;----------------------------------------------------- make-individual-string

(defun make-individual-string (name-string)
  "makes an individual string from a user provided string.
Argument:
   name-string: string 
Return:
   concept-id string."
  (let* ((word-list (make-text-list (make-value-string name-string)))
         (first-word (string-downcase (car word-list))))
    ;; when string represents a variable, remove "_I-"
    (if (eql #\_ (char first-word 0)) (setq first-word (subseq first-word 3)))
    (apply #'concatenate 'string "z-" first-word (cdr word-list))))

#|
? (make-individual-string  "male")
"z-male"
? (make-individual-string  "MALE")
"z-male"
? (make-individual-string  "MALE  de jour   ")
"z-maleDeJour"
? (make-individual-string  "_I-33")
"z-33"
|#
;;;------------------------------------------------------------------ make-is-a

(defun make-is-a (string-list)
  "produces the ancestor reference of a concept.
Arguments:
   doc: list of strings, e.g. (\"Personne\"  \"Student\")
Return:
   nil"
  (let (name-list)
    ;; first get the right language references
    ;; e.g. (("Z-AdressePostale" "Adresse Postale") ("Z-ZoneRurale" "Zone Rurale"))
    (setq name-list (make-ref-concept-list string-list))
    ;(print name-list)
    (when name-list
      (hformat "~&<table
 style=\"text-align: left; width: 100%;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td style=\" text-align: left; width: 25%;\"> 
        <b>~A:</b> 
      </td>
      <td style=\" text-align: left; width: 75%;\">      
      ~{~{<a href=\"#~A\">~A</a>~}~^, ~}
      </td>
    </tr>
  </tbody>
</table>"
               (format nil (get-language-string  
                            '(:en "Specific of"
                              :fr "Sous-concept de ")
                            *current-language*))
               ;; get nice printing names from internal index table
               (mapcar #'(lambda (xx) 
                           (cons (car xx)
                                 (index-get (make-index-string (cadr xx)) :hdr :prt)))
                       name-list)))
    nil))

#|
? (make-is-a '("postal address" "ZONE rurale"))
<table
 style="text-align: left; width: 100%;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style=" text-align: left; width: 20%;"> 
        <b>Specific of:</b> 
      </td>
      <td style=" text-align: left; width: 80%;">      
      <a href="#Z-PostalAddress">Postal Address</a>, <a href="#Z-RuralArea">Rural Area</a>
      </td>
    </tr>
  </tbody>
</table>
NIL
|#
;;;----------------------------------------- make-national-indices-and-synonyms

(defun make-national-indices-and-synonyms 
       (name-option value-string data-type &optional (value-type :id))
  "takes a name option and builds all possible indices from it.
   E.g. ({:name} :en \"man of war ; ship\" :fr \"navire\")
   will produce the following entries: ManOfWar, Ship for English
   and navire as a synonym (reference)
   will produce navire as an entry for :fr and the others as
   synonyms (references)
   Uses the *language-tags* global variable.
   Does not do anything if language of tag is not present.
Arguments:
   name-option: a multi-lingual list
   value-string: string respresenting the concept id or property id, 
      e.g. \"Z-Country\"
   data-type: :class :att :rel or :inst
Return:
   t
Error:
   throws to an :error tag."
  (let (reference-string index-list index-string name-list)
    ;; check name-option format removing the possible :name tag
    ;; this is supposed to have been done by sol2owl
    (unless (mln::mln? name-option)
      (terror "Bad MLN format ~S" name-option))
    ;; get the current language, e.g. ("person" "guy")
    (setq name-list (mln::extract name-option :language *current-language*))
    ;; if nothing quit
    (unless name-list 
      (return-from make-national-indices-and-synonyms nil))

    ;; set up reference string, normalizing it (make-value-string)
    (setq reference-string (make-value-string (car name-list)))
    ;; then loop on all languages
    (dolist (tag (mln::get-languages name-option))
      ;; extract the list of values for the language
      (setq index-list (mln::extract name-option :language tag))
      ;; for each name build an index object
      (dolist (index index-list)
        ;; cook up index string
        (setq index-string (make-index-string index))
        ;; keep a nice printing pattern
        (unless (index-get index-string :hdr :prt)
          (index-add index-string :hdr :prt (make-string-header index)))
        ;; insert entry into *index* hash table
        (cond  ; JPB051119
         ;; for a reference string insert regular entry
         ((equal reference-string index-string)
          (index-add index-string data-type value-type value-string))
         ;; if proper language a regular entry and a reference
         ((eql tag *current-language*)
          (index-add index-string data-type value-type value-string)
          (index-add index-string :ref tag reference-string))
         ;; for other languages, insert reference only
         (t
          (index-add index-string :ref tag reference-string)))
        )
      )
    ;; return
    t))

#|
? (let ((*current-language* :fr))
     (index-clear)
     (make-national-indices-and-synonyms '(:name :en "City; town" :fr "ville; cité") 
                                         "Z-Ville" :class)
     (index))

("City" ((:REF (:EN "Ville")) (:HDR (:PRT "City")))) 
("Cité" ((:REF (:FR "Ville")) (:CLASS (:ID "Z-Ville")) (:HDR (:PRT "Cité")))) 
("Town" ((:REF (:EN "Ville")) (:HDR (:PRT "Town")))) 
("Ville" ((:CLASS (:ID "Z-Ville")) (:HDR (:PRT "Ville")))) 
:END
|#
;;;---------------------------------------------------------------- make-one-of

(defun make-one-of (option)
  "prints a list of options.
Arguments:
   option: list of mls, e.g.
     ( (:en \"married\" :fr \"marié; mariée\" :it \"sposato\")
       (:en \"batchelor; spinster\" :fr \"célibataire\" :it \"Nubile; celibe\"))
Return:
   nil."
  (hformat 
   "~&<table
 style=\"text-align: left; width: 100%;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td style=\" text-align: left; width: 20%;\"> 
        <b>~A:</b> 
      </td>
      <td style=\" text-align: left; width: 80%;\">      
      ~A
      </td>
    </tr>
  </tbody>
</table>"
   (format nil (get-language-string  
                '(:en "One of"
                  :fr "Un parmi ")
                *current-language*))
   (format nil "~{~{~A~^/~}~^, ~}"
           (remove nil
                   (mapcar #'(lambda (xx) (mln::extract xx :language *current-language*))
                           option)))
   )
  nil)

#|
;; if the language is English, then the values should be married, batchelor, spinster,
;; divorced, widowed
(setq option  '(
   (:en "married" :fr "marié; mariée" :it "sposato")
   (:en "batchelor; spinster" :fr "célibataire" :it "nubile; celibe")
   (:fr "pacsé; pacsée" )
   ;(:doc :en "A pacs is a contract between two person in France."))
   (:en "divorced" :fr "divorcé; divorcée")
   (:en "widowed" :fr "veuf; veuve")))

? (make-one-of option)
<table
 style="text-align: left; width: 100%;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style=" text-align: left; width: 20%;"> 
        <b>One of:</b> 
      </td>
      <td style=" text-align: left; width: 80%;">      
      married, batchelor/spinster, divorced, widowed
      </td>
    </tr>
  </tbody>
</table>
NIL

? (let ((*current-language* :fr))
   (make-one-of option))
<table
 style="text-align: left; width: 100%;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style=" text-align: left; width: 20%;"> 
        <b>Un parmi:</b> 
      </td>
      <td style=" text-align: left; width: 80%;">      
      marié/mariée, célibataire, pacsé/pacsée, divorcé/divorcée, veuf/veuve
      </td>
    </tr>
  </tbody>
</table>
NIL

(let ((*current-language* :it))
   (make-one-of option))
<table
 style="text-align: left; width: 100%;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style=" text-align: left; width: 20%;"> 
        <b>One of:</b> 
      </td>
      <td style=" text-align: left; width: 80%;">      
      sposato, nubile/celibe
      </td>
    </tr>
  </tbody>
</table>
NIL
|#
;;;-------------------------------------------------------------- make-ontology

(defun make-ontology (&rest options)
    "set up the file header and file identification
Arguments:
   option-list (rest): list of options
        (:name <ontology name>) or (:title <ontologie name>)
        (:version <version number>)
        (:doctype <name space>*)
        (:xmlns <name space>*)
        (:base <name space>)
        (:imports <URI>*)
Return:
   nil"  
  (let ((version (cadr (assoc :version options)))
	(title (or (cadr (assoc :name options)) (cadr (assoc :title options)))))
    (when (eql *compiler-pass* 1)
      (hformat "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">
<html>
<head>
  <meta content=\"text/html; charset=UTF-8\" http-equiv=\"content-type\">
  <title>~A Ontology</title>
</head>
<body>
<a name=\"top_of_ontology\"/>
" title)
      ;(rtab)
      ;; should make some sort of title, with version and language
      (hformat "<table
 style=\"text-align: center; width: 100%; height: 32px;\"
 border=\"0\" cellpadding=\"2\" cellspacing=\"2\">
  <tbody>
    <tr>
      <td
 style=\"text-align: center; background-color: rgb(76, 89, 255); vertical-align: center;\">
      <h2>
      <font color=\"yellow\">~A - Version ~A</font>
      </h2>
      </td>
    </tr>
  </tbody>
</table>~&"
	       title
	       (or version "(unspecified)"))
      nil)))

#|

|#
;;;------------------------------------------------------ make-ref-concept-list

(defun make-ref-concept-list (string-list)
  "takes a list of strings refering to concepts in any language, and produces ~
      a list of pairs concept-id concept-reference. If the concept is not available ~
      in *current language* skips the corresponding pair.
Arguments:
   string-list: list of strings referring to concepts
Return:
   list of pairs or nil."
  (let (concept-string-list name-list)
    ;; first get the right language references
    (setq concept-string-list 
          (remove nil (mapcar #'get-reference-string string-list)))
    ;; then compute concept-id, which is done by adding "Z-" to reference string
    (when concept-string-list
      (setq name-list 
            (mapcar #'(lambda(xx) (list (make-concept-string xx) 
                                        (make-string-header xx))) 
                    concept-string-list)))
    ;; return list of pairs
    name-list))

#|
? (make-ref-concept-list '("postal address" "zone rurale" "ship"))
(("Z-PostalAddress" "Postal Address") ("Z-RuralArea" "Rural Area"))
|#
;;;-------------------------------------------------------------- make-relation

(defun make-relation (options)
  "prints a relation.
Arguments:
   options: relation-options, i.e., name, to, type, cardinality, one-of
            '((:en \"city;town\" :fr \"ville; cité\") (:to \"Cité\"))
Return:
   nil"
  (hformat "~&<table
 style=\"text-align: left; width: 100%; color: rgb(0, 0, 221); \" 
 border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
  <tbody>
    <tr>
      <td style=\" text-align: left; width: 25%;\">
      <b> ~{~A~^ / ~}~A~A:</b>
      </td>
      <td style=\"text-align: left; width: 25%;\">
      ~A
      </td>
      <td style=\" text-align: left; width: 50%;\">      
      ~A
      </td>
    </tr>
  </tbody>
</table>"
           (mapcar #'make-string-header
                   (mln::filter-language (car options) *current-language*))
           (get-cardinality options)
           (if (eql *current-language* :fr) " "  "") ; French needs a space
           (get-to options)
           (get-relation-one-of options))
  nil)

#|
? (let ((*current-language* :fr))
    (make-relation '((:en "city;town" :fr "ville; cité") (:to "Cité"))))
<table
 style="text-align: left; width: 100%; color: rgb(0, 0, 221); " 
 border="0" cellpadding="2" cellspacing="0">
  <tbody>
    <tr>
      <td style=" text-align: left; width: 25%;">
      <b> ville / cité :</b>
      </td>
      <td style="text-align: left; width: 25%;">
      <a href="#Z-Ville">Ville</a>
      </td>
      <td style=" text-align: left; width: 50%;">      
      
      </td>
    </tr>
  </tbody>
</table>
NIL
|#
;;;--------------------------------------------------------------- make-section

(defun make-section (mls)
  " prints a section title.
Arguments:
   mls: multilingual-string giving the title of the chapter
Return:
   nil"
  (when (eql *compiler-pass* 2)
    (hformat "<table
 style=\" width=100%; \"
 border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
  <tbody>
    <tr>
      <td
 style=\"text-align: right; background-color: rgb(76, 89, 255); width=10%;
 \">
      <h3>
      <font color=\"yellow\"> ~S.~S </font>
      </h3> 
      </td>
      <td
 style=\"text-align: left; background-color: rgb(76, 89, 255); width=90%;
 vertical-align=center;\">
      <h3>
      <font color=\"yellow\"> ~A </font>
      </h3>
      </td>
    </tr>
  </tbody>
</table>" 
             *chapter-counter* 
             (incf *section-counter*)
             (get-language-string mls *current-language*)))
  nil)

;;;--------------------------------------------------------- make-string-header

(defun make-string-header (text)
  "Used to print string headers and html ref texts. Same as make-value-string-lite."
  (make-value-string-lite text))

#|
? (make-string-header " Régime de   la sécurité sociale  ")
"Régime de la sécurité sociale"
|#
;;;------------------------------------------------------------- make-text-list

(defun make-text-list (text)
  "Norm a text string by separating each word making it lower case with a leading~
      uppercased letter.
Arguments:
   text: text string
Return:
   a list of normed words."
  (let (pos result word)
    (unless text (return-from make-text-list nil))
    (loop
      ;; remove trailing blanks
      (setq text (string-trim '(#\space) text))
      ;; is there any space left?
      (setq pos (position #\space text))
      (unless pos
        (push (string-capitalize text) result)
        (return-from make-text-list (reverse result)))
      ;; extract first word
      (setq word (subseq text 0 pos)
            text (subseq text (1+ pos)))
      (push (string-capitalize word) result)
      )))

#|
? (make-text-list "the   DAY when I    fell INTO the PIT   ")
("The" "Day" "When" "I" "Fell" "Into" "The" "Pit")
? (make-text-list "le Jour où je suis    tombé dans le puits   ")
("Le" "Jour" "Où" "Je" "Suis" "Tombé" "Dans" "Le" "Puits")
|#
;;;---------------------------------------------------------- make-value-string

(defun make-value-string (value &optional (interchar '#\space))
  "Takes an input string, removing all leading and trailing blanks, replacing ~
   all substrings of blanks and simple quote with interchar, and ~
   building a new string capitalizing all letters. The function is used ~
   to normalize values for comparing a value with an external one while ~
   querying the database. Keeps non ascii characters."
  (let* ((input-string (if (stringp value) value
                           (format nil "~S" value)))
         (work-list (map 'list #'(lambda(x) x) 
                         ;; remove leading and trailing blanks
                         (string-trim '(#\space) input-string))))
    (string-capitalize   ; we normalize entry-point to lower case
     (map
      'string
      #'(lambda(x) x) 
      ;; this mapcon removes the blocks of useless spaces and uses
      ;; a translation table to replace French letters with the equivalent
      ;; unaccentuated capitals
      (mapcon 
       #'(lambda(x) 
           (cond 
            ;; if we have 2 successive blanks we remove one
            ((and (cdr x)(eq (car x) '#\space)
                  (eq (cadr x) '#\space)) 
             nil)
            ;; if we have a single blank we replace it with an intermediate char
            ((eq (car x) '#\space) (copy-list (list interchar)))
            ;; if the char is in the translation table we use the table
            ((assoc (car x) *translation-table*)
             (copy-list (cdr (assoc (car x) *translation-table*))))
            ;; otherwise we simply list the char
            (t (list (car x)))))
       work-list)))))

#|
? (make-value-string "aujourd'hui     j'ai très faim.    ")
"Aujourd Hui J Ai Très Faim."
|#
;;;----------------------------------------------------- make-value-string-lite

(defun make-value-string-lite (value &optional (interchar '#\space))
  "Takes an input string, removing all leading and trailing blanks, and ~
   multiple spaces. Capitalizes the first letter. The function is used ~
   to print headers.  
   French accentuated letter are replaced with unaccentuated capitals."
  (let* ((input-string (if (stringp value) value
                           (format nil "~S" value)))
         (work-list (map 'list #'(lambda(x) x) 
                         ;; remove leading and trailing blanks
                         (string-trim '(#\space) input-string))))
    (string-upcase   ; we normalize entry-point to lower case
     (map
      'string
      #'(lambda(x) x) 
      ;; this mapcon removes the blocks of useless spaces and uses
      ;; a translation table to replace French letters with the equivalent
      ;; unaccentuated capitals
      (mapcon 
       #'(lambda(x) 
           (cond 
            ;; if we have 2 successive blanks we remove one
            ((and (cdr x)(eq (car x) '#\space)
                  (eq (cadr x) '#\space)) 
             nil)
            ;; if we have a single blank we replace it with an intermediate char
            ((eq (car x) '#\space) (copy-list (list interchar)))
            ;; if the char is in the translation table we use the table
            ;((assoc (car x) *translation-table*)
            ; (copy-list (cdr (assoc (car x) *translation-table*))))
            ;; otherwise we simply list the char
            (t (list (car x)))))
       work-list))
     :start 0 :end 1)))

#|
? (MAKE-VALUE-STRING-LITE "  le   jour d' aujourd'hui   ")
"Le jour d' aujourd'hui"
(MAKE-VALUE-STRING-LITE "  le   jour d'aujourd'hui   ")
"Le jour d'aujourd'hui"
|#
;;;--------------------------------------------------------- multilingual-name?

(defun multilingual-name? (expr)
  "checks whether a list starting with :name followed by a multilingual string ~
      or a multilingual string.
Argument:
   expr: something like ({:name} <multilingual-string>)
Result: 
   nil or normalized MLN (new a-list format)"
  (and (mln::mln? expr)(mln::make-mln expr)))

#|
(defun multilingual-name? (expr)
  "checks whether a list starting with :name followed by a multilingual string ~
      or a multilingual string.
Argument:
   expr: something like ({:name} <multilingual-string>)
Result: 
   nil or expr without :name if it was there"
  (and (listp expr)
       (or (and (mls? expr) expr)
           (and (eql (car expr) :name) (mls? (cdr expr)) (cdr expr)))))
|#
#|
? (multilingual-name? '(:name))
NIL
? (multilingual-name? '(:name :en "Abert" :fr "Jean"))
((:EN "Abert") (:FR "Jean"))
? (multilingual-name? '(:name :en "Albert"))
((:EN "Albert"))
;; compare with:
? (mln::mln? '(:name :en "Albert"))
T
|#
;;;----------------------------------------- PROCESS-INDIVIDUAL-ATTRIBUTE-VALUE

(defun process-individual-attribute-value (data)
  "data is usually a list of strings. It can be a list of multilingual strings. ~
      In the latter case, extracts the right substrings.
      ((:fr \"paris; lutèce\" :en \"paris\")) -> (\"paris/lutèce\")
Arguments:
   data: list of strings or of multi-lingual strings
Return:
   a list of strings."
  ;; when simple list of strings norm it
  (when (every #'stringp data)
    (return-from process-individual-attribute-value 
      (mapcar #'make-string-header data)))
  ;; otherwise
  (when (every #'mln::mln? data)
    (mapcar #'(lambda (xx)
                (format nil "~{~A~^/~}"
                        (mapcar #'make-string-header
                                (mln::filter-language xx *current-language*))))
            data)))

#|
? (let ((*current-language* :fr))
    (PROCESS-INDIVIDUAL-ATTRIBUTE-VALUE '(( :fr "paris; lutèce" :en "paris"))))
("Paris/Lutèce")

? (let ((*current-language* :fr))
    (PROCESS-INDIVIDUAL-ATTRIBUTE-VALUE '("paris" "Ville   Lumière")))
("Paris" "Ville Lumière")

(let ((*current-language* :fr))
  (dolist (*compiler-pass* '(1 2))
    (defindividual "ville"
      (:name :fr "Ville-1" :en "City-1")
      ("nom" "Paris"))))
===== "z-ville-1"
===== "z-ville-1"<a name="z-ville-1" />

<table
 style="text-align: center; width: 100%; height: 32px;"
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td 
 style="text-align: left; background-color: rgb(219, 253, 149); 
 vertical-align: center; width: 20px">
      <a href="#top_of_ontology">TOP</a>
      </td>
      <td
 style="text-align: center; background-color: rgb(219, 253, 149); 
 vertical-align: center;">
      <h2>
      <center>Ville-1 </center>
      </h2>
      </td>
    </tr>
  </tbody>
</table>
<table
 style="text-align: left; width: 100%; color: rgb(118, 183, 78); " 
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td 
 style="text-align: left; width: 20%">
      <b>Classe :</b>
      </td>
      <td style="width: 80%;">
      <a href="#Z-Ville">Ville</a>
      </td>
    </tr>
  </tbody>
</table>
<table
 style=" width: 100%; "
 border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td 
 style="text-align: left; width: 20%">
      <b>Nom :</b>
      </td>
      <td
 style="width: 80%;">
      Paris
      </td>
    </tr>
  </tbody>
</table>
<br><br>
NIL
|#
;;;------------------------------------------------------- read-next-valid-line

(defun read-next-valid-line (ss)
  "skip block commented lines.
Arguments:
   ss: stream
   initial-line: first line (that was already read)
return:
   string or :eof"
  (let ((comment-flag 0) line)
    (loop
      (setq line (read-line ss nil :eof))
      ;(format t "~&~s" line)
      ;; did we reach end of file? If so return
      (if (eql line :eof)(return-from read-next-valid-line :eof))
      ;; otherwise check for comment sign
      (cond
       ;; when empty line, skip it
       ((equal "" (string-trim '(#\space) line)))
       ;; skip simple comment lines
       ((eql (char line 0) #\;))
       ;; if not inside a commented block and line is valid, return it
       ((and (< comment-flag 1)
             (not (search "#|" line :test #'string-equal)))
        (return-from read-next-valid-line line))
       ;; if line is beginning comment
       ;; signal comment and read next line
       ((search "#|" line :test #'string-equal)
        (incf comment-flag)
        ;(format t "~&------- ~S" comment-flag)
        )
       ;; if inside commented-block and line is end of comment, decrease counter
       ((and (> comment-flag 0)
             (search "|#" line :test #'string-equal))
        (decf comment-flag)
        ;(format t "~&------- ~S" comment-flag)
        )
       ;; otherwise, skip line
       ))))

;;;---------------------------------------------------------------------- reset

(defun  reset ()
  (setq *chapter-counter* 0
        *section-counter* 0
        *left-margin* 0
        *index* (clrhash *index*)
        *index-list* ()
        *concept-list* ()
        *attribute-list* ()
        *relation-list* ()
        *individual-list* ()
        *compiler-pass* 1
        *current-language* :en))

;;;------------------------------------------------------------------ trim-file
;;; this function is needed to remove (in-package <ontology>) from the top of
;;; the text file and replace it with (in-package :sol-html) so that the defxxx
;;; macro are those of the html package, not those of MOSS
;;; we also remove comment and empty lines and the final :EOF

(defun trim-file (input-file output-file)
  "Cleans a file of the leading UTF-8 mark (3 bytes?) by copying the file into
    new one.
Arguments:
   input-file: file to clean
   output-file: cleaned file
Return:
   t"
  (let (line)
    (with-open-file (in input-file :direction :input)
      (with-open-file (out output-file :direction :output :if-exists :supersede
			   :if-does-not-exist :create)
	#|
        (setq line (read-line in))
	;; remove leading char that if it is a UTF-8 mark (#xFEFF)
	#+CLISP
        (if (char-equal (char line 0) #+CLISP #\ZERO_WIDTH_NO-BREAK_SPACE)
          (format out "~&~A" (subseq line 1))
          (format out "~&~A" line))
        #+MCL
        ;; MCL does not process UTF-8
        ;; get rid of the first 3 chars if they look suspicious: ï»¿ (UTF-8 tag)
        (dotimes (nn 3)
          (if (member (peek-char t in) '(#\ï #\» #\¿) :test #'char-equal)
            (read-char in)))
        |#
        ;;insert an in-package line
        (format out "(in-package :sol-html)~&")
	;; copy the rest of the file
	(loop
	  (setq line (string-trim '(#\space) (read-line in nil :eof)))
	  ;; if :eof then end of file
	  (when (string-equal line :eof) 
	    (close out)
	    (return-from trim-file t))
          (cond
           ;; skip empty lines
           ((equal line ""))
           ;; skip comment lines
           ((eql (char line 0) #\;))
           ;; skip line starting with (in-package
           ((search "in-package" line :test #'string-equal))
           ;; copy other lines
           (t
            (format out "~%~A" line)))
          )
        nil))))

(trace trim-file)
#|
(ext:letf 
     ((custom:*default-file-encoding* 
               (ext:make-encoding :charset charset:utf-8 :line-terminator :dos)))
     (trim-file "c:/sol/ontologies/test0.sol" "c:/sol/ontologies/test0.tmp"))
|#
;;;================================ INDEX =====================================

;;;---------------------------------------------------------------------- index

(defun index ()
  "dumps the content of the *index* hash-table in alphabetic order (debug function)."
  (let (index-list)
    (maphash #'(lambda (xx yy) (if xx (push (list xx yy) index-list)))
             *index*)
    (mapcar #'print
            (setq index-list (sort index-list #'string< :key #'car)))
    :end))

;;;------------------------------------------------------------------ index-add

(defun index-add (key tag prop value)
  "add an entry into the *index* table. Replaces previous entry at the prop level.
Arguments:
   key: the entry string
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value associated with the entry
Return:
   value"
  (let ((entry (gethash key *index*))
        tag-value prop-value old-pair)
    ;; if no entry, then create one
    (unless entry 
      (setf (gethash key *index*) `((,tag . ((,prop ,value)))))
      (return-from index-add value))
    ;; check if value has tag
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    ;; if not set up one
    (unless tag-value
      (setf (gethash key *index*) (cons `(,tag . ((,prop ,value))) entry))
      (return-from index-add value))
    ;; check for prop
    (setq prop-value (cdr (assoc prop tag-value :test #'equal)))
    ;; if no, then set up one
    (unless prop-value
      (setq tag-value (cons `(,prop ,value) tag-value))
      ;; replace old tag-value
      (setq entry (cons (cons tag tag-value)
                        (remove (assoc tag entry :test #'equal)
                                entry :test #'equal)))
      (setf (gethash key *index*) entry)
      (return-from index-add value))
    ;; if prop present get previous value, add new one and replace
    (setq old-pair (assoc prop tag-value :test  #'equal)
          ;value (append (cdr old-pair) (list value))) ; JPB 051119
          value (if (not (member value (cdr old-pair) :test #'equal))
                  (append (cdr old-pair) (list value))
                  (cdr old-pair)))
    ;; replace old value with new value
    (setq tag-value (cons `(,prop . ,value)
                          (remove old-pair tag-value :test #'equal)))
    ;; replace old tag-value
    (setq entry (cons (cons tag tag-value) (remove (assoc tag entry :test #'equal)
                                                   entry :test #'equal)))
    ;; replace entry
    (setf (gethash key *index*) entry)
    (return-from index-add value)))
#|
(index-clear)
T
? (index)
:END
? (index-add "Ville" :CLASS :id "Z-City")
"Z-City"
? (index)
("Ville" ((:CLASS (:ID "Z-City"))))
:END

? (index-add "Ville" :class :one-of "Caen")
"Caen"
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen") (:ID "Z-City")))) 
:END

? (index-add "Ville" :class :one-of "Nice")
("Caen" "Nice")
? (index-add "Ville" :class :one-of "Toulouse")
("Caen" "Nice" "Toulouse")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END

? (index-add "Ville" :inst :att "hasName")
"hasName"
? (index)
("Ville"
 ((:INST (:ATT "hasName"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END

? (index-add "Ville" :inst :att "hasColor")
("hasName" "hasColor")
? (index)
("Ville"
 ((:INST (:ATT "hasName" "hasColor"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END

? (index-add "Ville" :inst :rel "hasNeighbor")
"hasNeighbor"
? (index)
("Ville"
 ((:INST (:REL "hasNeighbor") (:ATT "hasName" "hasColor"))
  (:CLASS (:ONE-OF "Caen" "Nice" "Toulouse") (:ID "Z-City")))) 
:END
|#
;;;---------------------------------------------------------------- index-clear

(defun index-clear (&optional key tag prop)
  "removes one of the levels of the index depending on the arguments. If none, ~
      then clears the whole table
Arguments:
   key (opt): key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
Return:
   t"
  (let ((entry (gethash key *index*))
        tag-value)
    ;; if no args, clear table
    (unless key
      (clrhash *index*)
      (return-from index-clear t))
    ;; if key but no tag, then remove entry
    (unless tag 
      (remhash key *index*)
      (return-from index-clear t))
    ;; if no prop, we want to remove tag option
    (setq entry (gethash key *index*))
    (unless prop
      (setf (gethash key *index*) 
            (remove (assoc tag entry :test #'equal) entry :test #'equal))
      (return-from index-clear t))
    ;; check for value, if no value we want to remove prop option
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    
    (setq tag-value (remove (assoc prop tag-value :test #'equal)
                            tag-value :test #'equal))
    ;; replace old tag-value
    (setq entry 
          (if tag-value
            (cons (cons tag tag-value) (remove (assoc tag entry :test #'equal)
                                               entry :test #'equal))
            (remove (assoc tag entry :test #'equal)
                    entry :test #'equal)))
    (setf (gethash key *index*) entry)
    (return-from index-clear t)))

#|
? (index-clear)
T
? (index)
NIL
? (%index-set "hamlet" nil nil '((:class (:id "Z-Village")(:one-of "Margny"))))
((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))
? (index)
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (index-clear "Ville")
T
? (index)
("hamlet" ((:CLASS (:ID "Z-Village") (:ONE-OF "Margny")))) 
:END
? (index-clear "hamlet" :class :one-of)
T
? (index)
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (%index-set "Ville" :class :id '("Z-City"))
("ZCity")
? (%index-set "Ville" :class :id '("Z-City"))
("ZCity")
? (index)
("Ville" ((:CLASS (:ID "Z-City") (:ONE-OF "Caen" "Nice")))) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
? (index-clear "Ville" :class )
T
? (index)
("Ville" NIL) 
("hamlet" ((:CLASS (:ID "Z-Village")))) 
:END
|#
;;;--------------------------------------------------------------- index-remove

(defun index-remove (key tag prop value)
  "remove a value associated with prop. If not there, do nothing.
Arguments:
   key (opt): key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value to remove
Return:
   t"
  (let ((old-value (index-get key tag prop))
        new-value)
    (setq new-value (remove value old-value :test #'equal))
    (if new-value
      (%index-set key tag prop new-value)
      (index-clear key tag prop))))
#|
? (index-clear)
T
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice")))) 
:END
? (index-add "Ville" :class :one-of "Paris")
("Caen" "Nice" "Paris")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice" "Paris")))) 
:END
? (index-add "Ville" :class :id "Z-City")
"ZCity"
? (index)
("Ville" ((:CLASS (:ID "Z-City") (:ONE-OF "Caen" "Nice" "Paris")))) 
:END
? 
INDEX-REMOVE
? (index-remove "Ville" :class :one-of "Nice")
("Caen" "Paris")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Paris") (:ID "Z-City")))) 
:END
? (index-remove "Ville" :class :id "Z-City")
T
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Paris")))) 
NIL
|#
;;;----------------------------------------------------------------- %index-set

(defun %index-set (key tag prop value)
  "set a value associated with key tag or prop. Must have the proper format.
Arguments:
   key: key (error if not present)
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
   value: value to set (must have the proper format)
Return:
   value"
  (let (entry tag-value prop-value)
    (unless key (error "no key arg"))
    ;; if no tag replaces whole entry
    (unless tag
      (setf (gethash key *index*) value)
      (return-from %index-set value))
    ;; get old entry
    (setq entry (gethash key *index*))
    ;; if prop is nil, then replaces the value associated with tag
    (unless prop
      (setf (gethash key *index*) 
            (cons (cons tag value)
                  (remove (assoc tag entry :test #'equal) entry :test #'equal)))
      (return-from %index-set value))
    ;; get old tag-value
    (setq tag-value (assoc tag entry :test #'equal))
    ;; get prop-value part
    (setq prop-value (assoc prop (cdr tag-value) :test #'equal))
    ;; if prop there replace old-value with new value
    ;; e.g. entry ((:CLASS (:id "ZCity")(:one-of ...)) (:INST ...))
    ;;      tag-value (:CLASS (:id "ZCity")(:one-of ...))
    ;;      prop-value (:id "ZCity")
    ;; (%index-set "Ville" :CLASS :id '("ZTown"))
    (setq tag-value 
          (list* tag 
                 (cons prop value) 
                 (remove prop-value (cdr tag-value) :test #'equal)))
    
    (setf (gethash key *index*) 
          (cons tag-value
                (remove (assoc tag entry :test #'equal) entry :test #'equal)))
    value))

#|
? (index-clear)
T
? (%index-set "Ville" nil nil '((:CLASS (id "Z-City"))))
((:CLASS (ID "Z-City")))
? (index)
("Ville" ((:CLASS (ID "Z-City")))) 
NIL
? (%index-set "Ville" nil nil '((:CLASS (id "Z-Town"))))
((:CLASS (ID "Z-Town")))
? (index)
("Ville" ((:CLASS (ID "Z-Town")))) 
NIL
? (%index-set "Ville" :class nil '((:id "Z-Town")))
((:ID "Z-Town"))
? (index)
("Ville" ((:CLASS (:ID "Z-Town")))) 
NIL
? (%index-set "Ville" :class nil '((:id "Z-Town")(:one-of "Bordeaux")))
((:ID "Z-Town") (:ONE-OF "Bordeaux"))
? (index)
("Ville" ((:CLASS (:ID "Z-Town") (:ONE-OF "Bordeaux")))) 
NIL
? (%index-set "Ville" :class :one-of '("Caen" "Nice"))
("Caen" "Nice")
? (index)
("Ville" ((:CLASS (:ONE-OF "Caen" "Nice") (:ID "Z-Town")))) 
NIL
|#
;;;------------------------------------------------------------------ index-get

(defun index-get (key &optional tag prop)
  "get one of the levels of the index depending on the arguments. If none, ~
      then clears the whole table
Arguments:
   key: key
   tag: one of :CLASS :ATT :REL :INST
   prop: depends on the type of entry (may be :id, :one-of...)
Return:
   corresponding value"
  (let ((entry (gethash key *index*)) tag-value)
    (unless tag (return-from index-get entry))
    (setq tag-value (cdr (assoc tag entry :test #'equal)))
    (unless prop (return-from index-get tag-value))
    (cdr (assoc prop tag-value :test #'equal))))

#|
? (index)

("Ville" ((:CLASS (:ID "Z-City")))) 
("Hamlet" ((:CLASS (:ID "Z-Hamlet") (:ONE-OF "margny")))) 
NIL
? (index-get "Hamlet" :class :id)
("Z-Hamlet")

|#
:EOC


