;;;==========================================================================
;;;20/01/04
;;;		
;;;		     M O S S - P A C K A G E S - (File packages.LISP)
;;;	 
;;;==========================================================================

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
This file contains the definitions of packages used by the MOSS application

History
2019
 0824 creation
2020
 0104 adding exports to package definitions
|#
 
(defpackage :moss (:use :ccl :cl)
  (:nicknames :m)
  (:export 
   ;;=== from globals
   *ANSWER*
   *APPLICATION-PACKAGE*
   *LANGUAGE*
   *MOSS-OUTPUT*
   *MOSS-WINDOW*
   *SELF*
   *TRANSITION-VERBOSE* 
   *VERBOSE* 
   ;;=== from macros
   =MAKE-ENTRY 
   CATCH-ERROR 
   DEFATTRIBUTE 
   DEFCHAPTER 
   DEFCONCEPT 
   DEFFUNCTION
   DEFINDIVIDUAL 
   DEFINSTANCE 
   DEFINSTMETHOD 
   DEFOBJECT 
   DEFONTOLOGY 
   DEFOWNMETHOD
   DEFRELATION 
   DEFRULE 
   DEFSECTION 
   DEFSIMPLE-SUBDIALOG 
   DEFSTATE 
   DEFSUBDIALOG
   DEFTASK               ; from dialog macros
   DEFUNIVERSALMETHOD 
   DEFVIRTUALATTRIBUTE 
   DEFVIRTUALCONCEPT 
   DEFVIRTUALRELATION
   ;DMFORMAT 
   WITH-AP 
   WITH-CONTEXT 
   WITH-DATABASE 
   WITH-ENVIRONMENT 
   WITH-LANGUAGE
   WITH-PACKAGE
   ;;== from dialog-macros
   DEFCOMPLEXELIZARULES
   DEFELIZARULES
   ;;=== from def
   MAKE-ATTRIBUTE
   MAKE-CONCEPT
   MAKE-INDIVIDUAL
   MAKE-INST-METHOD
   MAKE-OWN-METHOD
   MAKE-ORPHAN
   MAKE-OWN-METHOD
   MAKE-RELATION
   MAKE-UNIVERSAL-METHOD
   M-LOAD
   ;;=== from service
   <<
   <<BOUNDP
   <<GET
   <<SYMBOL-PLIST
   <<VOMIT
   >>
   >>MAKUNBOUND
   >>REMPROP
   >>SETPROP
   ACCESS-FROM-WORDS
   ADD-VALUES
   CLEAR-ALL-FACTS
   DELETE-VALUES
   FILL-PATTERN
   FILTER
   GET-CURRENT-YEAR
   ;GET-OBJECTS-FROM-ATTRIBUTE-AND-VALUE
   ;MOVE-FACT
   MREF
   PACKAGE-KEY
   PEP
   POP-FACT
   PRINT-SYS
   READ-FACT
   REPLACE-FACT
   SET-LANGUAGE
   STRING+
   SYMBOL-KEY
   WEB-ACTIVE?
   WEB-ADD-TEXT
   WEB-CLEAR-GATE
   WEB-CLEAR-TAG
   WEB-CLEAR-TEXT
   WEB-GET-GATE
   WEB-GET-TAG
   WEB-GET-TEXT
   WEB-SET-GATE
   WEB-SET-MARK
   WEB-SET-TEXT
   ;;=== from utils
   ALIST-ADD 
   ALIST-ADD-VALUES 
   ALIST-REM
   ALIST-REM-VAL 
   ALIST-SET 
   ALIST-SET-VALUES 
   ALISTP COPY-FACT 
   D+ 
   D- 
   EQUAL+ 
   FIRSTN
   GET-FACT 
   MFORMAT 
   NTH-INSERT 
   NTH-MOVE 
   NTH-REMOVE 
   NTH-REPLACE 
   PP 
   SET-FACT 
   V+ 
   V-
   ;VERBOSE-FORMAT
   STRING+
   ;;=== from engine
   SEND 
   SEND-NO-TRACE 
   TON 
   TOFF 
   TRACE-MESSAGE 
   UNTRACE-MESSAGE 
   TRACE-OBJECT 
   UNTRACE-OBJECT 
   TRACE-METHOD 
   UNTRACE-METHOD 
   UNTRACE-ALL
   BROADCAST 
   G==> 
   G-> 
   S->
   ;;=== from query
   ACCESS
   <> 
   FILTER-OBJECTS
   ;;=== from paths
   LOCATE-OBJECTS
   ;;=== persistency 
   DB-CLOSE 
   DB-CREATE 
   DB-ERASE 
   DB-EXTEND 
   DB-LOAD 
   DB-OPEN 
   DB-STORE 
   DB-VOMIT
   ))

(defpackage :mln 
  (:use :ccl :cl) ; for multilingual names
  (:export :*language-tags*)
  (:import-from :moss "STRING+" "*LANGUAGE*"))

;; because MOSS is part of the OMAS system, there are some references to OMAS in the code
;; (should be fixed)

(defpackage :omas (:use :moss :ccl :cl)(:nicknames :o))

;; nodgui is loaded by the ccl-init file

(defpackage :nodgui (:use :cl))

(defpackage :nodgui-user (:use :nodgui :cl)(:nicknames :n)
   (:import-from :moss "STRING+" 
                 "*MOSS-WINDOW*" 
                 "WITH-PACKAGE" 
                 "WITH-AP" 
                 "SEND"
                 "*APPLICATION-PACKAGE*"
                 :broadcast 
                 "*MOSS-OUTPUT*"
                 :nth-insert 
                 :nth-move 
                 :nth-remove 
                 :nth-replace))

;; pakages for producing OWL or HTML files

(defpackage :sol (:use :ccl :cl)
  (:EXPORT 
   :*SOL-VERSION* 
   :*SOL-HEADER*
   :*ONTOLOGY-TITLE* 
   :*LANGUAGE-LIST* 
   :*SOL-FILE*
   :*ONTOLOGY-INITIAL-DIRECTORY* 
   :*INITIAL-PARAMETERS*
   :*FILENAME-DISPLAY* 
   :*COMPILING* 
   :*OWL-OUTPUT*
   :*RULE-OUTPUT* 
   :*HTML-OUTPUT*
   :*RULE-FORMAT* 
   :*RULE-FORMAT-LIST* 
   :*VERBOSE-COMPILE*
   :*TRACE-WINDOW* 
   :*TRACE-PANE* 
   :*TRACE-HEADER* 
   :*LOG-FILE*
   ))

(defpackage :sol-owl (:use :sol :ccl :cl)(:nicknames :so))

(defpackage :sol-html (:use :sol :ccl :cl)(:nicknames :sh))

:EOF