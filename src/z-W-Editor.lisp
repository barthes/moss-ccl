;;;-*- Mode: Lisp; Package: "NODGUI-USER" -*-
;;;=================================================================================
;;;19/09/24
;;;		
;;;		E D I T O R - (File W-EDITOR.LiSP)
;;;	
;;;=================================================================================

#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
History
2019
 0924 Creating this editor quite different from the ACL one
|#

(in-package :nodgui-user)


(defClass editor-window (toplevel)
  ((columns :accessor columns :initform nil)
   ;(database :accessor database)
   (rows :accessor rows :initform nil)
   (selection :accessor selection :initform nil)
   ))

;;;---------------------------------------------------------- MAKE-EDITOR-WINDOW

(defun make-editor-window (object-id)
  "create a window to edit the specified object by interacting directly in a tabled 
  presentation of the object"
  (with-nodgui ()
    (let*
        ((ew (make-instance 'editor-window))
         (ew-header (make-instance 'frame :master ew))
         (ew-title (make-instance 'label 
               :text (car (moss::send object-id 'moss::=summary)) :master ew-header))
         (ew-data (make-instance 'frame :master ew))
         (ew-tv (make-instance 'treeview :height 10 :master ew-data
                  :columns '(cl c0 c1 c2 cr)))
         )
      (set-geometry-xy ew 250 60)
      (grid ew-header 0 0 :sticky "ew")
      (grid ew-title 0 0 :sticky "ew")

      (grid ew-data 1 0)
      (treeview-column ew-tv 0 :width 20)
      (treeview-column ew-tv 4 :width 20)
      (grid ew-tv 0 0) ; propoerty name

      (lb-initialize ew object-id)
      (lb-fill-table ew ew-tv)
      )
    ))

#|
(moss::with-package :family
  (catch :error
    (with-nodgui (:debug 3) 
      (moss::with-package :family
        (setq *context* 0 *version-graph* '((0)))
        (make-editor-window f::_dbb)))))
|#

#|
(defun make-editor-window (object-id)
  "create a window to edit the specified object by interacting directly in a tabled 
  presentation of the object"
  (with-nodgui ()
    (let*
        ((ew (make-instance 'editor-window))
         (ew-header (make-instance 'frame :master ew))
         (ew-title (make-instance 'label 
               :text (car (moss::send object-id 'moss::=summary)) :master ew-header))
         (ew-data (make-instance 'label :master ew-header :text "Object Name"))
         (cp (make-instance 'listbox :width 15 :height 10 :master ew-data))
         (cl (make-instance 'listbox :width 2 :master ew-data))
         (c0 (make-instance 'listbox :width 20 :master ew-data))
         (c1 (make-instance 'listbox :width 20 :master ew-data))
         (c2 (make-instance 'listbox :width 20 :master ew-data))
         (cr (make-instance 'listbox :width 2 :master ew-data))
         )
      (set-geometry-xy ew 250 60)
      (grid ew-header 0 0)
      (grid ew-title 0 0 :sticky "ew")
      (grid ew-data 1 0)
      (grid cp 0 0) ; propoerty name
      (grid cL 0 1) ; # of values hidden to the left
      (grid c0 0 2)
      (grid c1 0 3)
      (grid c2 0 4)
      (grid cR 0 5) ; # of values hidden to the right

      (lb-initialize ew object-id)
      (lb-fill-table ew cp cl c0 c1 c2 cr)
      )
    ))
|#



;;;-------------------------------------------------------------------- POST-ROW
;;; Posting a row consists in displaying the name of the attribute or of the relation
;;; in the first column, then 3 values at most in columns c1 to C3. If there are 
;;; more values say 7, then column CR (for right) will post the number of values that
;;; could not have been displayed. Column CL (for Left) will show 0. If one moves the
;;; "cursor" to the right, then the rightmost value will disappear and the first 
;;; hidden value at the right will appear in column C3. Column CL will then display
;;; 1 to indicate that 1 value is hidden at the right.


;;; DELETE
;;; Deleting a value is done be selecting it and clicking the DELETE button. The 
;;; values will be eventuelly rearranged to get rid of the gap if there is one.

;;; INSERT
;;; Inserting a value is done, either by adding it directly into an empty slot, or
;;; by selecting a value and clicking the INSERT button. The value will be inserted 
;;; in front of the selected value.
;;; Inserting an attribute can be done directly. Linking a new object by a relation
;;; needs to query the system, and select the object among the ones that are an answer
;;; to the query.

;;; Internal representation
;;; Each line is represented by a list, e.g.
;;;   (1 "FIRST NAME" 0 ("Jean" "Joseph" "Gearges" "Jules" "Christian"))
;;; 1: 2e ligne 
;;; "FIRST-NAME": attribute name
;;; 0: index meaning that the first 3 values are displayed in columns C0, C1, C2
;;; ("Jean" ...) values

;;; Note: 
;;; INSERT: when inserting a new value into one of the columns, if the line number is 
;;; higher than the last column that is displayed, the value will be appended to the list
;;; In the same way one cannot insert into the middle of a list.
;;; Thus a column has to be represented and should be modified in its representation
;;; before being displayed again.
;;;  (c0 "v1" "v2")
;;; (listbox-insert c0 5 "v5") should yield
;;;  (c0 "v1" "v2" "" "" "v5") to be displayed (listbox-clear c0) + (listbox-insert ...)


;;;------------------------------------------------------------------- LB-INSERT

(defun lb-insert (lb nn val ll)
  "inserting into a filled position replaces the value"
  (cond 
   ((<= nn 0) (cons val (cdr ll))) ; reached the position
   ((null ll) (cons "" (lb-insert lb (1- nn) val (cdr ll)))) ;  passed the position
   (t (cons (car ll) (lb-insert lb (1- nn) val (cdr ll))))))

#|
(setq ll '("" "" "v2"))

(lb-insert :lb 0 "v0" nil)
("v0")

(lb-insert :lb 2 "v2" nil)
("" "" "v2")

(lb-insert :lb 0 "v0" ll)
("v0" "" "v2")

(lb-insert :lb 1 "v1" ll)
("" "v1" "v2")

(lb-insert :lb 2 "zz" ll)
("" "" "zz")

(lb-insert :lb 6 "zz" ll)
("" "" "v2" "" "" "" "zz")
|#
    
;;; INITIALIZATION

(defun lb-initialize (ew obj-id)
  (let (prop-id-list prop-name-list val-list row-list col-list)
    ;; get the list of properties
    (setq prop-id-list (moss::send obj-id 'moss::=get-properties))
    ;; get the names
    (setq prop-name-list (moss::broadcast prop-id-list 'moss::=instance-name))
    ;; for each property, get the list of values
    (setq val-list (mapcar #'(lambda (xx) (lb-norm-values obj-id xx))
                           prop-id-list))
    ;; normalize the list of value by adding spaces
    (setq val-list (mapcar #'(lambda (xx) (append xx '(""))) val-list))
    ;; set the list of rows
    (setq row-list (lb-build-row prop-name-list val-list))
    (setf (rows ew) row-list)
    ;; no values are hidden to the right
    ;(setq col-list (lb-build-columns row-list))
    ;; set the list of columns
    ;(setf (columns ew) col-list)
    (inspect ew)
    ))


(defun lb-norm-col (ll nmax)
  "add spaces to a list of values if necessary up to nmax+1
Arguments:
   ll: list of values
   nmax: max number of effective values always greater or equal to the length of ll
Return:
   a list of length nmax+1 ending with a null string"
  (if (>= (length ll) nmax)
      (append ll '(""))
      ;; add one "" more than the length of the list of values to allow insertions
      (append ll (make-list (- (1+ nmax) (length ll)) :initial-element "")))
  )

#|
(lb-norm-col nil 3)
("" "" "" "")

(lb-norm-col '(a b c) 3)
(A B C "")

(lb-norm-col '(a ) 3)
(A "" "" "")

;; this is never the case!
(lb-norm-col '(a b c d e f) 3)
(A B C D E F "")
|#

(defun lb-fill-table (ew ew-tv)
  "uses the list of row descriptions to fill the columns"
  (dolist (item (rows ew))
    (treeview-insert-item ew-tv :text (car item) :column-values (cdr item))))

#|
(defun lb-fill-table (lb cp cl c0 c1 c2 cr)
  "reads the list of column contents from the window object and fills the table"
  (let ((column-lists (columns lb))
        (column-id-list (list cp cl c0 c1 c2 cr)))
    (dolist (item column-lists)
      (print `(lb-fill-table column ,(car column-id-list)))
      (print `(lb-fill-table item ,item))
      (listbox-insert (pop column-id-list) 0 item))))
|#

#|
(defun lb-build-columns (row-list)
  "build a representation of a row
  Arguments:
  row-list: is the list of row contents
  val-list: list of list of values
  Return a list of values representing a row of the edit table"
  (let (cp cl c0 c1 c2 cr)
    (dolist (item (reverse row-list))
      (print `(lb-build-columns item ,item))
      (push (nth 0 item) cp) 
      (push (nth 1 item) cl)
      (push (nth 2 item) c0)
      (push (nth 3 item) c1)
      (push (nth 4 item) c2)
      (push (nth 5 item) cr)
      )
    (list cp cl c0 c1 c2 cr)
    ))
|#

#|
(lb-build-columns '(("NAME" 0 "Barthès" "" "" 0) ("FIRST-NAME" 0 "Jean-Paul" "A" "" 0) 
   ("NICK-NAME" 0 NIL "" "" 0) ("AGE" 0 74 "" "" 0) ("BIRTH YEAR" 0 1945 "" "" 0) 
   ("SEX" 0 "m" "" "" 0)))
|#

(defun lb-build-row (prop-list val-list)
  "build a representation of a row
  Arguments:
  prop-list is property-names
  val-list: list of list of values
  Return a list of values representing a row of the edit table"
  (let (carcol result)
    (dolist (prop prop-list)
      (setq carcol (pop val-list))
      (push `(,(car prop) 0 ,(pop carcol) 
              ,(if carcol (pop carcol) "")
              ,(if carcol (pop carcol) "")
              ,(if carcol (1- (length carcol)) 0)
              ,@carcol
              )
            result))
    (pprint (reverse result))
    (reverse result)
    ))

#|
(lb-build-row 
   '(("NAME") ("FIRST-NAME") ("NICK-NAME") ("AGE") ("BIRTH YEAR") ("SEX"))
   '(("Barthès") ("Jean-Paul" "A") NIL (74) (1945) ("m")))   
|#

(defun lb-norm-values (obj-id prop-id)
  "get the values associated with the property, if a relation, then uses =summary"
  (let ((val-list (moss::send obj-id 'moss::=get-id prop-id)))
    (print `(lb-norm-values val-list ,val-list))
    (if (moss::%is-relation? prop-id)
        (mapcar #'(lambda (xx) (car (moss::send xx 'moss::=summary)))
                               val-list)
        val-list)))
      
#|
(moss::with-package :family
  (nodgui-user::lb-norm-values f::_jpb 'f::$s-person-brother))
("Pierre-Xavier Barthès")

(moss::with-package :family
  (nodgui-user::lb-norm-values f::_jpb 'f::$t-person-name))
("Barthès")
|#