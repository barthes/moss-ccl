;;;-*- Mode: Lisp; Package: "NODGUI-USER" -*-
;;;==============================================================================
;;;19/10/22
;;;            O V E R V I E W (file W-overview.lisp)
;;;
;;;==============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
#| This file contains functions to create an OVERVIEW window allowing to 
visuallize the content of an ontology and associated knowledge base
It was created by extracting the code from the moss-W-window.lisp file

History
2019
 1022 adapted from the MOSS/ACL file
2021
 0430 correcting *context* when posting it
|#

#|
TODO
 - make columns stretchable
 - add an on-close function?
 - solve the problem of waiting until a window is closed...
 - BUG: crash when posting ids in instance list and calling edit
|#

(in-package :nodgui-user)


;;;================================= globals ====================================

;;;============================= service functions ==============================

;;;--------------------------------------------------------------- DISPLAY-OBJECT

(defun display-object (ow ow-text object-id &key append-flag)
  "display an object in the ow-text area using the recorded (and posted) format
Arguments:
   ow: overview window
   ow-text: display pane
   object-id: id or pair of object to display
   append-flag (key): if t append text to what is already displayed
Return:
   nil"
  (print `("OVERVIEW display-object *moss-output*:" ,moss::*moss-output*
           "*moss-window*:" ,moss::*moss-window*))
  ;; if flag is t, then do not clear the text pane
  (unless append-flag (clear-text ow-text))
  ;; display using the external or internal format
  (if (display-instance-with-internal-format ow)
      (append-text ow-text 
                   ;; pprint into a string
                   (format nil "~:W" (moss::<< object-id)))
      (moss::send object-id 'moss::=print-self))
  ;; printing functions usually return nil
  nil)

;;;---------------------------------------------------------------- POPULATE-TREE

(defun populate-tree (pane tree-list &optional (parent +treeview-root+) last-node)
  "fills the pane with the items from the tree
  Argumments:
  pane: treeview
  tree-list: a tree to post formatted as a list
  parent (opt): the parent of the tree items (default tree root)
  last-node (opt): last node that was posted"
  ;(print tree-list)
  (when tree-list    
    (cond
     ;; when first item is a list, this is a new branch of the tree whose parent
     ;; is the last node that was posted
     ((listp (car tree-list))
      ;; insert branch
      (format-wish "~A insert ~A end -id {~A} -text {~A}" (widget-path pane)
                   last-node (caar tree-list) (caar tree-list))
      (nodgui::tcldebug "")
      ;; post the following children
      (populate-tree pane (cdar tree-list) last-node (caar tree-list))
      ;; then continue with the rest of the branch
      (populate-tree pane (cdr tree-list) parent last-node)
      )
     (t 
      ;; if the tree is a simple list, then we are in a branch
      ;; insert node
      (nodgui::tcldebug (format nil "entry: ~A" (car tree-list)))
      (format-wish "~A insert ~A end -id {~A} -text {~A}" (widget-path pane)
                   parent (car tree-list) (car tree-list))
      ;; post the rest of the branch
      (populate-tree pane (cdr tree-list) parent (car tree-list))))
    ))

#|
(populate-tree nil tree)
|#

;;;===============================================================================
;;;
;;;                              OVERVIEW WINDOW 
;;;
;;;===============================================================================
;;; This is the window in which objects are displayed

;;;------------------------------------------------------- OW-MAKE-OUTLINE-WINDOW
;;; make special OVERVIEW and SIL to keep index of objects
;;; one problem is to display ontologies when concepts are described in the 
;;; package of a particular agent. 

;;; When working with OMAS, the window is created from the agent window, itself
;;; created in the cg-user package.
;;; Whenever we click on a specific button, we must install the package, context, 
;;; and version graph corresponding to the particular ontology.
;;; All the necessary contextual information is stored inside the assistant.
;;; It can be extracted reinstalled temporarily each time we click onto something

;;;============================= Classes =========================================

(defClass overview-window (toplevel)
  ((agent :accessor agent)
   (application-package :accessor application-package :initform nil)
   (database :accessor database)
   (display-instance-with-internal-format 
    :accessor display-instance-with-internal-format :initform nil)
   (instance-list :accessor instance-list :initform nil)
   ;; needed to update window
   (instance-pane :accessor instance-pane :initform nil)
   ;; record text pane so that we can restore the output channel when returning 
   ;; from the editor
   (moss-output :accessor moss-output :initform nil)
   ;; keep track of MOSS window (MOSS only)
   (moss-window :accessor moss-window :initform nil)
   ;; keep track of owner (OMAS only), duplicates owner in that case
   (owner :accessor owner :initform nil)
   (showing-concept :accessor showing-concept :initform nil)
   (shown-object-id :accessor shown-object-id :initform nil)
   ;; needed to update window
   (text-pane :accessor text-pane :initform nil)
   (tree-pane :accessor tree-pane :initform nil)
   ))

(defClass text-area (text)
  ((class-index :accessor class-index :initform nil)))

#|
(defClass instance-sil (cg:single-item-list)
  ((instance-index :accessor instance-index :initform nil)))
|#
;;;============================= Methods =========================================

;;;------------------------------------------------------ DISPLAY-TEXT (TEXT-AREA)

(defmethod moss::display-text ((out text-area) text &key erase &allow-other-keys)
  "when moss::*moss-output* is attached to the text pane, then the method will show
   the text in the pane"
  (declare (ignore erase))
  (append-text out text))

;;;------------------------------------------------------ UPDATE (OVERVIEW-WINDOW)
;;; - when returning from editing an instance should refresh the detail pane
;;; - when returning from adding a new object, should refresh the list of instances
;;; and display the details of the new object in the details pane

(defmethod update ((ow overview-window) current-entity)
  "refresh the overview window when returning from the editor"
  ;; update the instance list by accessing the data from the selected class
  ;; rebuilds the instance-list slot of the winwdow
  (ow-outline-on-change ow (instance-pane ow) (tree-pane ow))
  ;; upgrade details area
  (display-object ow (text-pane ow) current-entity)
  )

;;;===============================================================================
;;;                             OUTLINE-WINDOW
;;;===============================================================================

(defparameter *win* nil)

(defun make-overview-window (tree parent-window package &rest ll)
  (with-package package
    ;(print `("make-overview-window package " ,package))
    (apply #'ow-make-outline-window parent-window tree ll)))

(defun ow-make-outline-window (owner tree &key (title "ONTOLOGY"))
  "make a window with an outline pane"
  (declare (special moss::*moss-output* *context*))
  (let*
      ((context  (symbol-value (intern "*CONTEXT*")))
       (ow (make-instance 'overview-window :master owner :title title))
       ;;=== left viewtree to contain classes
       (ow-tree (make-instance 'treeview :master ow))
       ;;=== center column to contain instances names
       (ow-instances (make-instance 'listbox :master ow :width 30))
       ;;=== right part to detail instances
       (ow-data (make-instance 'frame :master ow :width 300))
       ;; instance detail pane
       (ow-text (make-instance 'text-area :master ow-data :width 60))

       ;; top right part
       (ow-top (make-instance 'frame :master ow-data :width 300))
       (ow-format-label (make-instance 'label :text "Internal format" :master ow-top))
       (ow-format (make-instance 'check-button :master ow-top))
       (ow-toggle-label (make-instance 'label :text "Show concept" :master ow-top))
       (ow-toggle (make-instance 'check-button :master ow-top))
       (ow-mk-instance (make-instance 'button :master ow-top :text "MK INSTANCE"
                         :command (lambda () (ow-mkinst-button-on-click ow))))
       (ow-edit (make-instance 'button :master ow-top :text "EDIT"
                  :command (lambda () (ow-edit-button-on-click ow))))
       (ow-context-name (make-instance 'label :master ow-top :text "CONTEXT"))
       (ow-delete (make-instance 'button :master ow-top :text "DEL INSTANCE"
                    :command (lambda () (ow-delinst-button-on-click 
                                              ow ow-tree ow-instances ow-text))))
       (ow-context (make-instance 'entry :master ow-top :width 2 :text context))
       )

    (setf (application-package ow) *package*)

    ;; window location on the screen
    (set-geometry-xy ow 300 200)

    (grid ow-tree 0 0 :sticky "ns")
    (populate-tree ow-tree tree)
    (bind ow-tree "<<TreeviewSelect>>"  
          (lambda (xx) (declare (ignore xx))
            (ow-outline-on-change ow ow-instances ow-tree)))
    ;; instance list
    (grid ow-instances 0 1 :sticky "ns")
    (bind ow-instances "<<ListboxSelect>>"  
          (lambda (xx) (declare (ignore xx))
            (ow-instance-list-on-change 
             ow ow-text (listbox-get-selection-index ow-instances))))
    (grid ow-data 0 2)
    (grid ow-top 0 0)
    (grid ow-format-label 0 0 :sticky "w")
    (grid ow-format 0 1)
    (setf (command ow-format) #'(lambda (state)
                                  (ow-format-on-change ow ow-format ow-text state)))
    (grid ow-toggle-label 1 0 :sticky "w")
    (grid ow-toggle 1 1)
    (setf (command ow-toggle) #'(lambda (state)
                           (ow-toggle-on-change ow ow-toggle ow-text state)))
    (grid ow-mk-instance 0 2)
    (grid ow-edit 0 3)
    (grid ow-context-name 0 5)
    (grid ow-context 0 6 :sticky "e")
    (grid ow-delete 1 2)
    ;; procedure to ask Tk to filter non digit characters
    (format-wish "proc ValidInt {val}  {
                    return [ expr  {[ string is integer $val ]
                          || [ string match {[-+]} $val ]} ]
                             }")
    ;;=== context (should be an integer) ValidInt defined for Moss Window
    (format-wish "~A configure -validatecommand {ValidInt %P}" (widget-path ow-context))
    ;; call right function on return
    (bind ow-context "<Return>" 
          (lambda (event) (declare (ignore event))
            (with-package package
              (ow-context-on-change ow-context context))))
    (grid ow-text 1 0)
    (setq moss::*moss-output* ow-text)
    ;; record output channel to restore it when returning from the editor
    (setf (moss-output ow) ow-text)
    ;; record panes for updating the window
    (setf (instance-pane ow) ow-instances)
    (setf (text-pane ow) ow-text)
    (setf (tree-pane ow) ow-tree)
    ))
      
#|
(moss::with-package :family
  (catch :error
    (setq tree  (moss::%make-entity-tree-names 
                 (moss::%make-entity-tree)))
    (with-nodgui (:debug 3) 
      (moss::with-package :family
        (setq f::*context* 0 f::*version-graph* '((0)))
        (ow-make-outline-window *tk* tree)))))
|#
;;;--------------------------------------------------------- OW-CONTEXT-ON-CHANGE

(defUn ow-context-on-change (ow-context context)
  "sets up displaying context
  Arguments:
  win: moss window
  on-context: entry pane containing the new context value"
  (declare (special *context*))
  (let* ((new-value (text ow-context))
         (context-value (read-from-string new-value)))
    (cond
     ;; checking if the context is a number
     ((not (numberp context-value))
      ;; not a number
      (ask-okcancel 
       (format nil "*** ~S is not a number ***" new-value))
      ;; reset the value to *context* in the entry pane
      (setf (text ow-context) context)
      )
     ;; checking if the context is valid
     ((catch :error (moss::%%allowed-context? context-value))
      (set (intern "*CONTEXT*") context-value))
     ;; not valid
     (t
      (format-wish "bell")
      (ask-okcancel 
       (format nil "*** ~S is an illegal context ***" new-value))
      ;; should reset the value to *context* in the entry pane
      (setf (text ow-context) context)
      ))))

;;;--------------------------------------------------- OW-DELINST-BUTTON-ON-CLICK

(defun ow-delinst-button-on-click (ow ow-tree ow-instances ow-text)
  "deletes the selected instance, checking that it is not a concept"
  (let ((index (car (listbox-get-selection-index ow-instances)))
        obj-id)
    (print `("ow-delinst-button-on-click index:" ,index))
    (setq obj-id (cadr (nth index (instance-list ow))))
    (print `("ow-delinst-button-on-click obj-id:" ,obj-id)) 
    (cond
     ;; if nothing selected complain
     ((null obj-id)
      (ask-OKcancel "nothing is selected.")
      )
     ;; if a concept, complain
     ((showing-concept ow)
      (ask-OKcancel "Can't delete a concept.")
      )
     ;; ask for confirmation
     ((null (ask-OKcancel "Are you sure you want to delete this individual?")))
     (t
      (print `("ow-delinst-button-on-click we now delete obj-id:" ,obj-id))
      (with-ap obj-id
        ;; create an editing box
        (moss::start-editing)
        ;; delete the instance from the environment
        (moss::send obj-id 'moss::=delete)
        ;; commit the change
        (moss::commit-editing)
        ;; update the instance list by accessing the data from the selected class
        ;; rebuilds the instance-list slot of the winwdow
        (ow-outline-on-change ow ow-instances ow-tree)
        ;; clean the text area
        (clear-text ow-text)
        t)))
    ))

#|
(moss::with-package :family
  (catch :error
    (setq tree  (moss::%make-entity-tree-names 
                 (moss::%make-entity-tree)))
    (with-nodgui (:debug 3) 
      (moss::with-package :family
        (setq f::*context* 0 f::*version-graph* '((0)))
        (ow-make-outline-window *tk* tree)))))
|#
;;;------------------------------------------------------ OW-EDIT-BUTTON-ON-CLICK
;;; We removed the possibility of editing a class JPB1403

(defUn ow-edit-button-on-click (ow &aux obj-id)
    "calls the EDIT window on the selected object on click"
  ;; not allowed to edit concepts
  (when (showing-concept ow)
    (ask-OKcancel "Not allowed to edit a concept. Please select an individual.")
    (return-from ow-edit-button-on-click))

  ;; selected object?
  (setq obj-id (shown-object-id ow))
  ;; no, complain
  (unless obj-id
    (ask-OKcancel "Please select an individual.")
    (return-from ow-edit-button-on-click))
  ;; call editor
  (make-editor-window obj-id ow))

;;;---------------------------------------------------- OW-IF-CHECK-BOX-ON-CHANGE

(defun ow-format-on-change (ow ow-format ow-text val)
  "set global variable *display-instance-with-internal-format* to print instance"
  (with-package (application-package ow)
    ;; if nothing is being displayed, complain
    (unless (or (shown-object-id ow)(showing-concept ow))
      (ask-okcancel "you must display an object before.")
      (setf (value ow-format) nil)
      (return-from ow-format-on-change))
    ;; record value 1 is true (want to display internal), 0 is nil for Tk
    (setf (display-instance-with-internal-format ow) (if (eql val 1) t))
    (if (showing-concept ow)
        (display-object ow ow-text (showing-concept ow))
        (display-object ow ow-text (shown-object-id ow)))
    )
  t)

;;;--------------------------------------------------- OW-INSTANCE-LIST-ON-CHANGE
;;; can be used instead of on-click

(defUn ow-instance-list-on-change (ow ow-text index-list)
  "display the selected object"
  (with-package (application-package ow)
    (when index-list
      (let* ((obj-id (cadr (nth (car index-list) (instance-list ow)))))
        ;; record object being displayed
        (setf (shown-object-id ow) obj-id)
        (display-object ow ow-text obj-id)
        ))))

;;;---------------------------------------------------- OW-MKINST-BUTTON-ON-CLICK
;;; create an empty instance of the concept, and call the editor to fill it

(defUn ow-mkinst-button-on-click (ow)
  "calls the EDIT window to make an instance of the selected class on click"
  (with-package (application-package ow)
    (let ((class-id-string (showing-concept ow))
          class-id instance-id)
      ;;=== find class
      (cond
       ;; are we displaying a class?
       ((and class-id-string
             (setq class-id (read-from-string class-id-string nil nil))))
       ;; no an instance?
       ((setq instance-id (shown-object-id ow))
        ;; if instance, get clas
        (setq class-id (car (moss::%type-of instance-id))))
       ;; if neither of those, complain
       (t (ask-okcancel "you must select an individual or display a concept")
          (return-from ow-mkinst-button-on-click)))
      ;; call the editor, asking it to create the new object
      (make-editor-window class-id ow :create)
      )))

;;;--------------------------------------------------------- OW-OUTLINE-ON-CHANGE

(defUn ow-outline-on-change (ow ow-instances ow-tree)
  "displays the list of instances in the instance list pane, saving info on the 
  instance-list slot of the editor window.
  Arguments:
  ow: overview window
  ow-instances: list of instances
  ow-tree: list of classes and sub-classes"

  (with-package (application-package ow)
    (let ((ref-list (treeview-select ow-tree))
          instance-list instance-id-list ll)

(print `("moss::ow-outline-on-change/ ref-list: " ,ref-list))
(print `("moss::ow-outline-on-change/ *package*: " ,*package*))

      ;; get the list of object ids
      (setq instance-id-list  (moss::access (list (car ref-list))))
      ;; make a list of object info to save e.g. ("Labrousse: Claire" ($E-PERSON . 14))
      (setq instance-list
            (mapcar #'(lambda (xx) 
                        (list
                         (format nil "~A (~A)" 
                                 (car (moss::send xx "=official-summary")) (cdr xx))
                         xx))
                    instance-id-list))
      (print instance-list)
      ;; sort the list by comparing uppercased strings (car info)
      (setq ll (sort instance-list 
                     #'(lambda (x y) (string< (string-upcase x)(string-upcase y)))
                     :key #'car))
      ;; save list in the window object (entries are infos)
      (setf (instance-list ow) ll)
      ;; clean list area
      (listbox-delete ow-instances)
      ;; post the new list taking only the strings
      (listbox-append ow-instances (mapcar #'car ll))
      )))

#|
    ;; if the detail window is showing an agent ontology we must set up the proper
    ;; environment
    #+OMAS
    (when (setq agent (agent dialog))
      ;(print `(ow-outline-on-change +++agent ,agent))
      (setq *package* (omas::ontology-package agent)
          *context* (omas::moss-context agent)
          *version-graph* (omas::moss-version-graph agent)
          *language* (omas::language agent))
      )

    ;; reinstall old environment if needed
    (when agent
      (setq *context* old-context
          *language* old-language
          *package* old-package
          *version-graph* old-version-graph))
|#
;;;---------------------------------------------------------- OW-TOGGLE-ON-CHANGE

(defun ow-toggle-on-change (ow ow-toggle ow-text state)
  "switch from instance to class (state = 1) or back (state = 0)"
  (with-package (application-package ow)
    (let ((obj-id (shown-object-id ow)))
      (cond
       ((null obj-id)
        (ask-okcancel "You must select an object before.")
        ;; reset checkbutton value
        (setf (value ow-toggle) nil)
        (return-from ow-toggle-on-change))
       ;; do we want to display the class?
       ((eql state 1) ; true when state is 1 (Tk)
        ;; get class-id
        (setq obj-id (car (moss::%type-of obj-id)))
        ;; record that we are displaying a class
        (setf (showing-concept ow) obj-id)
        (print `(ow-toggle-on-change obj-id ,obj-id))
        )
       ;; no, we are displaying an instance
       (t 
        (setf (showing-concept ow) nil)
        ))
      (print `(ow-toggle-on-change obj-id ,obj-id))      
      ;; display result
      (display-object ow ow-text obj-id)
      ))
  t)


:EOF