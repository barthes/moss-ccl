;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;21/06/28
;;;		
;;;		M O S S - W E B - (File web.lisp)
;;;		Copyright Fuckner@UTC 2013
;;;	
;;;==========================================================================

#|
This file contains functions for drawing charts in a Web page. 
|#

#|
2013
 1121 Creation
 1128 Adapting to the MOSS environment + alphabetic order + cosmetic changes
      + export of main functions
2014
 0307 -> UTF8 encoding
2021 
 0531 editing to add to MOSS-CCL
|#

;;;==========================================================================
;;; BUILD-MOSS-OBJECT-KEY (object keys)                            [FUNCTION]
;;; COMBINATIONS (&rest lists)                                     [FUNCTION]
;;; EXPAND-MOSS-LIST (symb attrs &optional label)                  [FUNCTION]
;;; CREATE-CHART-MESSAGE                                           [FUNCTION]
;;;    (chart-type categories group-function attribute content 
;;;                      &optional series-selector chart-title)
;;; CREATE-ENVELOPE (sender messages)                              [FUNCTION]
;;; CREATE-GRAPH-MESSAGE (content)                                 [FUNCTION]
;;; DISPLAY-MESSAGE (message conversation)                         [FUNCTION]
;;; EVAL-ATTR-VALUE (class-ref attr-name attr-value language)      [FUNCTION]
;;; EXPAND-MOSS-LIST (symb attrs &optional label)                  [FUNCTION]
;;; GENERATE-SUMMARY-LINE                                          [FUNCTION]
;;;   (group-function group-value count-value id attribute 
;;;    current-key)
;;; GET-SELECTED-ATTRIBUTES (word-list categories)                 [FUNCTION]
;;; GROUP-MOSS-LIST (moss-list keys attribute group-function)      [FUNCTION]
;;; IS-ATTR-REP (ref-symbol)                                       [FUNCTION]
;;; IS-LIST-OBJ-REP (ref-symbol)                                   [FUNCTION]
;;; IS-OBJ-REP (ref-symbol)                                        [FUNCTION]
;;; IS-SIMPLE-ATTR-REP (ref-symbol)                                [FUNCTION]
;;; MARSHALL-MOSS-OBJECT                                           [FUNCTION]
;;;   (elem &optional inv-rel-to-expand expanded-list)
;;; MOSS-LIST-TO-JSON (result &optional language pred-ref )        [FUNCTION]
;;; PARSE-STRING-TO-FLOAT (line)                                   [FUNCTION]
;;; SORT-MOSS-LIST (moss-list keys)                                [FUNCTION]
;;; UNMARSHALL-MOSS-OBJECT (elem)                                  [FUNCTION]
;;;==========================================================================

(in-package :moss)

;;;--------------------------------------------------------------------------
;;; Special functions related to the graph visualization. They
;;;--------------------------------------------------------------------------

(eval-when (:compile-toplevel :load-toplevel :execute)
  (export '(display-message
            create-graph-message
            create-envelope
            ;marshall-moss-object
            moss-list-to-json
            get-selected-attributes
            create-chart-message
            group-moss-list
            get-selected-attributes
            ))
  )

;;;---------------------------------------------------- BUILD-MOSS-OBJECT-KEY

(defun build-moss-object-key (object keys)
  "Extract properties corresponding to a group of keys. If keys are strings, ~
   they must have the same case as the properties (otherwise should use equal+).
Arguments:
  object: the source object, e.g. (class (a 1)(b 2)(c 3)(d 4))
  keys: the candidate keys, e.g. (a c)
Return
  e.g. (class (a 1)(c 3))"
  (let (obj-key)
    (dolist (attrib (cdr object))
      ;; if the property is one of the keys, extract the sublist
      (if (some #'(lambda (x) (if (equal x (car attrib)) x)) keys)
          (push attrib obj-key)))
    ;; add class
    (push (car object) obj-key)
    ;; return the result
    obj-key))

#|
(build-moss-object-key '("project" ("start" "2011") ("montant" "2000")
                         ("ann? "2011")) ("start" "ann?))
                       '("start" "montant"))
("project" ("montant" "2000") ("start" "2011"))
|#
;;;-------------------------------------------------------------- COMBINATION

(defun combinations (&rest lists)
  "Cartesian product of lists 
Arguments:
  lists: a set of lists used for the product operation"
  (if (car lists)
      (mapcan (lambda (inner-val)
                (mapcar (lambda (outer-val)
                          (cons outer-val
                                inner-val))
                        (car lists)))
              (apply #'combinations (cdr lists)))
    (list nil)))

(defun expand-moss-list (symb attrs &optional label)
  "Creates a de-normalized version of a tree-like representation
  Arguments:
  symb: the moss object
  attrs: list that will be used as a filter
  label: to add in front of the selected attributes"
  ;;(format t "~%@expand-moss-list called symb: ~S attrs: ~S label: ~S " symb attrs label)
  (let (res exp-attr)
    (cond
      ;; if the symbol is a list of moss objects
      ((is-list-obj-rep symb)
        ;; iterates over the list of symbols, calling the expand function recursively
        (dolist (s symb)
          (setf exp-attr (expand-moss-list s attrs label))         
          (if exp-attr 
            (dolist (elem exp-attr)
              (push elem res)
            )
          )
        )
      )
      ;; if the symbol is a moss object
      ((is-obj-rep symb)
        (let (tuple attr (has-rel nil))
          (dolist (y (cdr symb))
            (cond
              ;; is a simple property
              ((is-simple-attr-rep y)
                (if (some #'(lambda (x) (if (equal x (car y)) x)) attrs)
                  (progn
                    (setf attr nil)
                    (dolist (elem (cdr y))
                      ;;(format t " ~%@expand-moss-list y = ~S elem = ~S" (car y) elem)
                      (push (list (car y) elem) attr)
                    )
                    (push attr tuple)
                  )
                )
              )
              ;; is a complex property
              (t 
                (let (rel)
                  (setq rel (expand-moss-list (cdr y) attrs nil))
                  ;;(format t " ~%@expand-moss-list rel = ~S" rel)
                  (if rel
                    (progn 
                      (push rel tuple)
                      (setf has-rel t)
                    )
                  )
                )
              )
            )
          )
          ;;(format t " ~%@expand-moss-list tuple at the end = ~S, symb = ~S" tuple symb)
          (if tuple
            (progn
              (setf res (reverse (apply #'combinations tuple)))
              ;;(format t " ~%@expand-moss-list res = ~S tuple = ~S" res tuple)
              (if label
                (let (newtuple)
                  (dolist (elem res)
                    (push label elem)
                    (push elem newtuple)
                  )
                  (setf res newtuple)
                )
              )
              (if has-rel
                (progn
                  ;;(format t "~%@expand-moss-list has-rel is true: ~S" res)
                  (let (newtuple newres)
                    (dolist (line res)
                      (setf newtuple nil)
                      (dolist (elem line)
                        (format t "~%@expand-moss-list elem: ~S" elem)
                        (if (and (listp elem) (listp (car elem)))
                          (mapcar #'(lambda(xx) (push xx newtuple)) elem)
                          (push elem newtuple)
                        )
                      )
                      (push (reverse newtuple) newres)
                    )
                    (setf res (reverse newres))
                  )
                )
              )
            )
            (setf res nil)
          )
        )
      )
      (t 
        (error "invalid parameter: ~S" symb)
      )
    )
    (return-from expand-moss-list res)
  )
)


#|
;; unitary test
(progn
;; multiple attr values
(if (not (equal 
  (expand-moss-list 
  '(("a" ("b" "1" "2") ("c" "3" "4" "5") 
         ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1")) 
              ("d" ("e" "5.1" "5.2") ("f" "6.1"))) 
         ("g" ("g" ("h" " 10" "11"))))
    ("a" ("b" "a1" "a2") ("c" "a3" "a4" "a5") 
         ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1")) 
              ("d" ("e" "a5.1" "a5.2") ("f" "a6.1"))) 
         ("g" ("g" ("h" " a10" "a11"))))) '("b" "c") "line") 
  '(("line" ("c" "a3") ("b" "a1")) 
    ("line" ("c" "a4") ("b" "a1")) 
    ("line" ("c" "a5") ("b" "a1")) 
    ("line" ("c" "a3") ("b" "a2")) 
    ("line" ("c" "a4") ("b" "a2")) 
    ("line" ("c" "a5") ("b" "a2")) 
    ("line" ("c" "3") ("b" "1")) 
    ("line" ("c" "4") ("b" "1")) 
    ("line" ("c" "5") ("b" "1")) 
    ("line" ("c" "3") ("b" "2")) 
    ("line" ("c" "4") ("b" "2")) 
    ("line" ("c" "5") ("b" "2"))))) 
    (error "unitary test error") 
    (print "ok"))
;; empty value    
(if (not (equal 
  (expand-moss-list 
  '(("a" ("b") ("c" "3" "4" "5") 
         ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1")) 
              ("d" ("e" "5.1" "5.2") ("f" "6.1"))) 
         ("g" ("g" ("h" " 10" "11"))))
    ("a" ("b" "a1" "a2") ("c" "a3" "a4" "a5") 
         ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1")) 
              ("d" ("e" "a5.1" "a5.2") ("f" "a6.1"))) 
         ("g" ("g" ("h" " a10" "a11"))))) '("b" "c") "line")
  '(("line" ("c" "a3") ("b" "a1")) 
    ("line" ("c" "a4") ("b" "a1")) 
    ("line" ("c" "a5") ("b" "a1"))
    ("line" ("c" "a3") ("b" "a2")) 
    ("line" ("c" "a4") ("b" "a2")) 
    ("line" ("c" "a5") ("b" "a2"))
    ("line" ("c" "3")) 
    ("line" ("c" "4")) 
    ("line" ("c" "5")))))       
    (error "unitary test error") 
    (print "ok"))
;; single values
(if (not (equal
(expand-moss-list 
  '(("a" ("b" "1") ("c" "3") 
         ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1")) 
              ("d" ("e" "5.1" "5.2") ("f" "6.1"))) 
         ("g" ("g" ("h" " 10" "11"))))
    ("a" ("b" "a1") ("c" "a3") 
         ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1")) 
              ("d" ("e" "a5.1" "a5.2") ("f" "a6.1"))) 
         ("g" ("g" ("h" " a10" "a11"))))) '("b" "c") "line")
  '(("line" ("c" "a3") ("b" "a1")) ("line" ("c" "3") ("b" "1")))))
    (error "unitary test error") 
    (print "ok"))
;; using relation    
(if (not (equal
  (expand-moss-list 
    '(("a" ("b" "1" "2") ("c" "3" "4" "5") 
          ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1")) 
               ("d" ("e" "5.1" "5.2") ("f" "6.1"))) 
          ("g" ("g" ("h" " 10" "11"))))
      ("a" ("b" "a1" "a2") ("c" "a3" "a4" "a5") 
           ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1")) 
                ("d" ("e" "a5.1" "a5.2") ("f" "a6.1"))) 
           ("g" ("g" ("h" " a10" "a11"))))) '("b" "f") "line")
    '(("line" ("f" "a2.1") ("b" "a1")) 
      ("line" ("f" "a6.1") ("b" "a1")) 
      ("line" ("f" "a2.1") ("b" "a2"))
      ("line" ("f" "a6.1") ("b" "a2")) 
      ("line" ("f" "2.1") ("b" "1")) 
      ("line" ("f" "6.1") ("b" "1"))
      ("line" ("f" "2.1") ("b" "2")) 
      ("line" ("f" "6.1") ("b" "2")))))
    (error "unitary test error") 
    (print "ok"))  
;; multiple relation    
(if (not (equal
  (expand-moss-list 
    '(("a" ("b" "1" "2") ("c" "3" "4" "5")
           ("d" ("d" ("e" "1.1" "1.2") ("f" "2.1"))
                ("d" ("e" "5.1" "5.2") ("f" "6.1")))
           ("g" ("g" ("h" " 10" "11"))))
      ("a" ("b" "a1" "a2") ("c" "a3" "a4" "a5")
           ("d" ("d" ("e" "a1.1" "a1.2") ("f" "a2.1"))
                ("d" ("e" "a5.1" "a5.2") ("f" "a6.1")))
           ("g" ("g" ("h" " a10" "a11"))))) '("b" "e") "line")
  '(("line" ("e" "a1.1") ("b" "a1")) 
   ("line" ("e" "a1.2") ("b" "a1")) 
   ("line" ("e" "a5.1") ("b" "a1"))
   ("line" ("e" "a5.2") ("b" "a1")) 
   ("line" ("e" "a1.1") ("b" "a2")) 
   ("line" ("e" "a1.2") ("b" "a2"))
   ("line" ("e" "a5.1") ("b" "a2")) 
   ("line" ("e" "a5.2") ("b" "a2")) 
   ("line" ("e" "1.1") ("b" "1"))
   ("line" ("e" "1.2") ("b" "1")) 
   ("line" ("e" "5.1") ("b" "1")) 
   ("line" ("e" "5.2") ("b" "1"))
   ("line" ("e" "1.1") ("b" "2")) 
   ("line" ("e" "1.2") ("b" "2")) 
   ("line" ("e" "5.1") ("b" "2"))
   ("line" ("e" "5.2") ("b" "2")))))
    (error "unitary test error") 
    (print "ok"))
)    
|#

;;;----------------------------------------------------  CREATE-CHART-MESSAGE
#|
(defun create-chart-message 
    (chart-type categories group-function attribute content)
  "Creates a chart message that will be usually sent to a web browser.
Arguments:
  chart-type: pie, line, column for example
  categories: list of candidate attributes
  group-function: sum, avg for example
  attribute: target attribute
  content: the underlying data
"
  (list "pa-chart-message" 
        (list "chart-type" chart-type)
        (cons "categories" categories)
        (list "group-function" group-function)	
        (list "attribute" attribute)	
        (cons "content" content)))
|#

(defun create-chart-message 
  (chart-type categories group-function attribute content 
              &optional series-selector chart-title)
  "Creates a chart message that will be usually sent to a web browser
Arguments:
  chart-type: pie, line, column for example
  categories: list of candidate attributes
  group-function: sum, avg for example
  attribute: target attribute
  content: the underlying data
  series selector"
  (let (message) 
    (push (cons "content" content) message)
    (if (not (eq chart-title nil))
        (push (list "chart-title" chart-title) message))
    (if (not (eq series-selector nil))
        (push (list "series-selector" series-selector) message))
    (push (list "attribute" attribute) message)
    (push (list "group-function" group-function) message)
    (push (list "chart-type" chart-type) message)
    (push (cons "categories" categories) message)
    (push "pa-chart-message" message)
    (return-from create-chart-message message)
  )
)

#|
(create-chart-message "pie" '("year" "month") "sum" "net-value"
 '(("sales" ("year" "2012") ("month" "December") ("net-value" "100520.32"))
   ("sales" ("year" "2013") ("month" "January") ("net-value" "252000.00"))))
("pa-chart-message" ("chart-type" "pie") ("categories" "year" "month") 
 ("group-function" "sum") ("attribute" "net-value")
 ("content" ("sales" ("year" "2012") ("month" "December") ("net-value" "100520.32"))
  ("sales" ("year" "2013") ("month" "January") ("net-value" "252000.00"))))
|#	
;;;---------------------------------------------------------- CREATE-ENVELOPE
	
(defun create-envelope (sender messages)
  "Creates an envelope to send to the end-user.
Arguments:
  sender: the sender agent
  messages: the list of message objects
"
  (list "pa-message-envelope"
        (list "sender" sender)
        (list "timestamp" (write-to-string (get-universal-time)))	
        (list "payload" messages)))

#|
(create-envelope
 "ALBERT"
 (create-chart-message 
  "pie" '("year" "month") "sum" "net-value"
  '(("sales" ("year" "2012") ("month" "December") ("net-value" "100520.32"))
    ("sales" ("year" "2013") ("month" "January") ("net-value" "252000.00")))))
("pa-message-envelope" ("sender" "ALBERT") ("timestamp" "3594713083")
 ("payload"
  ("pa-chart-message" ("chart-type" "pie") ("categories" "year" "month") ("group-function" "sum")
   ("attribute" "net-value")
   ("content" ("sales" ("year" "2012") ("month" "December") ("net-value" "100520.32"))
    ("sales" ("year" "2013") ("month" "January") ("net-value" "252000.00"))))))

(create-envelope 
 "ALBERT"
 (create-graph-message 
  '(("projet" ("id" "PROJECT/$E-PROJECT.202") ("pays" "Brésil")
     ("titre" "MOBYDICK") ("sigle" "MDK") ("débutt" "2012") ("fin" "2015")
     ("participants" "Thouvenin, I.") ("domaine" "GI"))
    ("projet" ("id" "PROJECT/$E-PROJECT.218") ("pays" "Belgique")
     ("titre" "NIKITA") ("sigle" "NKT") ("début" "2010") ("fin" "2013")
     ("participants" "Lourdeaux, D.") ("domaine" "BS"))
    ("projet" ("id" "PROJECT/$E-PROJECT.22") ("pays" "France")
     ("titre" "ARAKIS") ("sigle" "ARA") ("début" "2010") ("fin" "2013")
     ("participants" "Lourdeaux, D." "Lenne, D.") ("domaine" "BIO")))))
("pa-message-envelope" ("sender" "ALBERT") ("timestamp" "3663309693")
 ("payload"
  ("pa-graph-message"
   ("content"
    ("projet" ("id" "PROJECT/$E-PROJECT.202") ("pays" "Brésil") ("titre" "MOBYDICK") ("sigle" "MDK")
     ("débutt" "2012") ("fin" "2015") ("participants" "Thouvenin, I.") ("domaine" "GI"))
    ("projet" ("id" "PROJECT/$E-PROJECT.218") ("pays" "Belgique") ("titre" "NIKITA") ("sigle" "NKT")
     ("début" "2010") ("fin" "2013") ("participants" "Lourdeaux, D.") ("domaine" "BS"))
    ("projet" ("id" "PROJECT/$E-PROJECT.22") ("pays" "France") ("titre" "ARAKIS") ("sigle" "ARA")
     ("début" "2010") ("fin" "2013") ("participants" "Lourdeaux, D." "Lenne, D.") ("domaine" "BIO"))))))
|#
;;;----------------------------------------------------  CREATE-GRAPH-MESSAGE
	
;;;(defun create-graph-message (content)
;;;  "Creates a graph message, usually for browser rendering.
;;;Arguments:
;;;  content: The graph using the moss object representation"
;;;  (list "pa-graph-message" 
;;;        (cons "content" content)))

;;; redefining function for the use of orientation option in HDSRI-R-DIALOG

(defun create-graph-message (content &optional orientation)
  "Creates a graph message, usually for browser rendering
Arguments:
  content: The graph using the moss object representation"
  (cond
   (orientation
    (list "pa-graph-message" 
          (list "orientation" orientation)
          (cons "content" content)))
   (t 
    (list "pa-graph-message" 
          (cons "content" content)))))

#|
(create-graph-message 
   '(("projet" ("id" "PROJECT/$E-PROJECT.202") ("pays" "Br?l")
     ("titre" "MOBYDICK") ("sigle" "MDK") ("d?t" "2012") ("fin" "2015")
     ("participants" "Thouvenin, I.") ("domaine" "GI"))
    ("projet" ("id" "PROJECT/$E-PROJECT.218") ("pays" "Belgique")
     ("titre" "NIKITA") ("sigle" "NKT") ("d?t" "2010") ("fin" "2013")
     ("participants" "Lourdeaux, D.") ("domaine" "BS"))
    ("projet" ("id" "PROJECT/$E-PROJECT.22") ("pays" "France")
     ("titre" "ARAKIS") ("sigle" "ARA") ("d?t" "2010") ("fin" "2013")
     ("participants" "Lourdeaux, D." "Lenne, D.") ("domaine" "BIO"))))
("pa-graph-message"
 ("content"
  ("projet" ("id" "PROJECT/$E-PROJECT.202") ("pays" "Br?l") ("titre" "MOBYDICK")
   ("sigle" "MDK")
   ("d?t" "2012") ("fin" "2015") ("participants" "Thouvenin, I.") ("domaine" "GI"))
  ("projet" ("id" "PROJECT/$E-PROJECT.218") ("pays" "Belgique") ("titre" "NIKITA") ("sigle" "NKT")
   ("d?t" "2010") ("fin" "2013") ("participants" "Lourdeaux, D.") ("domaine" "BS"))
  ("projet" ("id" "PROJECT/$E-PROJECT.22") ("pays" "France") ("titre" "ARAKIS") ("sigle" "ARA")
   ("d?t" "2010") ("fin" "2013") ("participants" "Lourdeaux, D." "Lenne, D.") ("domaine" "BIO"))))
|#	
;;;---------------------------------------------------------- DISPLAY-MESSAGE

(defun display-message (message conversation)
  "takes a message and displays its content into the assistant pane.
Arguments:
  message: A string containing the message
  conversation: the moss conversation object"
  (send conversation '=display-text
        (format nil "~%~A" message)))

;;;---------------------------------------------------------- EVAL-ATTR-VALUE

;; --------------------------------------------------------------------------
;; Evaluate attribute during transformation
;; --------------------------------------------------------------------------

(defun eval-attr-value (class-ref attr-name attr-value language)
  "this function deals with a variety of attribute types (mln,ids,references)
Arguments:
   class-ref: the class reference
   attr-name: the attribute name
   attr-value: the attribute
Return:
   changed attribute"
  (cond
    ;;=== if the content is multilingual
    ((mln::mln? attr-value)
      ;; extracts the corresponding value according to the language 
      (return-from eval-attr-value 
        (concatenate 'string "\"" (mln::print-string attr-value language) "\""))
    )
    ;;=== if the attribute is a unique id
    ((equal attr-name "marshaller::uid")
      ;; if the content is a string then concatenate the symbol & at the first position
      (when (stringp attr-value)
        (return-from eval-attr-value
          (concatenate 'string "\"&" attr-value "\"")))
      ;; is a symbol then create the string representation
      (when (symbolp attr-value)
        (return-from eval-attr-value
          (concatenate 'string "\"" (symbol-name attr-value) "\""))
      )
    )
    ;;=== a symbol value as a content is presumably a relationship (we don't need to check)
    ((symbolp attr-value)
      (return-from eval-attr-value
        (concatenate 'string "\"" (symbol-name attr-value) "\""))
    )
    ;;=== if pair-id must return summary string
    ((%%is-id? attr-value)
     (return-from eval-attr-value
       (string+ "\"" (car (send attr-value '=summary)) "\""))
     )
    ;;=== a string value with related class (must investigate if it is a relation)
    ((and class-ref (stringp attr-value))
      (if (%%get-id attr-name :relation :class-ref class-ref)
         ;; add an address symbol in front of the string if is a reference    
        (return-from eval-attr-value (concatenate 'string "\"&" attr-value "\""))
        (return-from eval-attr-value (concatenate 'string "\"" attr-value "\""))
      )
    )
    ;;=== a normal string value (without class ref)
    ((stringp attr-value)
      (return-from eval-attr-value (concatenate 'string "\"" attr-value "\""))
    )
    ;;=== a complex attribute (list)
    ((listp attr-value)
      (return-from eval-attr-value (moss-list-to-json attr-value language))
    )    
  )
)

#|
(eval-attr-value '("marshaller::uid" $E-CREDIT-CARD.3))

|#
;;;--------------------------------------------------------- EXPAND-MOSS-LISP

(defun expand-moss-list (symb attrs &optional label)
  "Creates a de-normalized version of a tree-like representation.
Arguments:
  symb: the moss object
  attrs: list that will be used as a filter
  label: to add in front of the selected attributes"
  (let (res exp-attr response)
    (cond
     ;; if the symbol is a list of moss objects
     ((is-list-obj-rep symb)
      ;; iterates over the list of symbols in order to expand each moss object
      (dolist (s symb)
        (setf exp-attr (expand-moss-list s attrs label))         
        (if exp-attr 
            (dolist (elem exp-attr)
              (push elem res)))))
     ;; if the symbol is a moss object
     ((is-obj-rep symb)
      (let (line relations proc-relations)
        ;; push the object type into the line (push (car symb) line)
        
        ;; iterate over the object attributes
        (dolist (y (cdr symb))
          (cond
           ((is-simple-attr-rep y)
            (if (some #'(lambda (x) (if (equal x (car y)) x)) attrs)
                (push y line)))
           ;; complex
           (t 
            (let (rel)
              (setq rel (expand-moss-list (cdr y) attrs))
              (if rel
                  (push rel relations))))))
        (dolist (r relations)
          (dolist (ri r)
            (dolist (attr line)
              (push attr ri))
            (push (cons label ri) proc-relations)))
        (if label
            (push label line))
        (if (> (length proc-relations) 0)
            (setf res proc-relations)
          (if line (setf res (list line))))))
     
     (t (error "invalid parameter: ~S" symb)))		 
    res))

#|
(expand-moss-list 
 '(("project" ("title" "@QUA") ("start" "2011") ("financement" "20000")
    ("financement annuel"
     ("financement annuel" ("année" "2011") ("montant" "2000")) 
     ("financement annuel" ("année" "2012") ("montant" "6000"))
     ("financement annuel" ("année" "2013") ("montant" "1000"))
     ("financement annuel" ("année" "2014") ("montant" "12000")))
    ("?ipe" ("team" ("nom" "ICI") ("perc" "50")) 
     ("team" ("nom" "DI") ("perc" "40"))
     ("team" ("nom" "RO") ("perc" "10"))))
   ("project" ("title" "SCOOP") ("start" "2007") ("financement" "3000")
    ("financement annuel" 
     ("financement annuel" ("année" "2008") ("montant" "1000")) 
     ("financement annuel" ("année" "2009") ("montant" "2000")))
    ("?ipe" ("team" ("nom" "ICI") ("perc" "50")) 
     ("team" ("nom" "DI") ("perc" "40"))
     ("team" ("nom" "RO") ("perc" "5"))
     ("team" ("nom" "ASER") ("perc" "5")))))
 '("start" "année" "montant") "project"))

(("project" ("montant" "1000") ("année" "2008") ("start" "2007"))
 ("project" ("montant" "2000") ("année" "2009") ("start" "2007"))
 ("project" ("montant" "2000") ("année" "2011") ("start" "2011"))
 ("project" ("montant" "6000") ("année" "2012") ("start" "2011"))
 ("project" ("montant" "1000") ("année" "2013") ("start" "2011"))
 ("project" ("montant" "12000") ("année" "2014") ("start" "2011")))
|#
;;;---------------------------------------------------- GENERATE-SUMMARY-LINE
;;; ???

(defun generate-summary-line 
    (group-function group-value count-value id attribute current-key)
  "generates a summary line with categories and grouped data.
Arguments:
   group-function: sum, avg for example
   group-value: 
   count-value: 
   id: object id
   attribute: target attribute
   current-key: category
"						
  (if (equal group-function "avg")
      (setf group-value (float (/ group-value count-value))))            
  (if (equal group-function "count")
      (setf group-value count-value))	
  (append current-key 
          (list (list "id" (write-to-string id))) 
          (list (list (concatenate 'string group-function "(" attribute ")") 
                      (write-to-string group-value)))))

;;;-------------------------------------------------- GET-SELECTED-ATTRIBUTES

(defun get-selected-attributes (word-list categories)
  "Selects attributes based on the raw input.
Arguments:
  word-list: A list of words typed by user
  categories: The list of attributes
Returns:
  The selected list of attributes"
  (let (patterns selected)
    (setq patterns (generate-access-patterns word-list :max 4))
    (dolist (pattern patterns)
      (setq pattern (car pattern))
      (dolist (category categories)
        (if (equalp category pattern)
            (push category selected)
          )
        )
      )
    (reverse selected)
    )
  )
 
#| ???
(get-selected-attributes '("la" "date" "de" "début" "et" "la" "durée")
                         '("date de début" "durée" "id"))
("date de début" "durée")
|#
;;;---------------------------------------------------------- GROUP-MOSS-LIST

(defun group-moss-list (moss-list keys attribute group-function) 
  "executes grouping functions
Arguments:
  moss-list: source-list
  keys: candidate attributes
  attribute: target attribute
  group-function: sum, avg for example
"
  (if (eq 0 (length moss-list))
      (return-from group-moss-list nil))
  
  (let (expanded-list sorted-list current-key grouped-list (group-value 0) 
                      (count-value 0) (id 0))
    ;; first expand the list
    (setf expanded-list (expand-moss-list moss-list 
                                          (cons attribute keys)
                                          (caar moss-list)))
    
    ;; sort the list
    (setq sorted-list (reverse (sort-moss-list expanded-list keys)))
    
    ;; generate the current key
    (setf current-key (build-moss-object-key (car sorted-list) keys))
    
    ;(format t "~%; group-moss-list / current-key = ~S keys = ~S" current-key keys)
    (let (attr-value)
      (dolist (object sorted-list)
        (if (not (equal (build-moss-object-key object keys) current-key))
            (progn   
              ;(format t "~%; - break: current-key = ~S object-key = ~S keys = ~S ~
              ;          group-value = ~S count-value = ~S" 
              ;  
              ;  current-key (build-moss-object-key object keys) 
              ;  keys group-value count-value)
              ;; generate summary line from the previous key            
              (setf id (+ id 1))
              (push (generate-summary-line
                     group-function group-value 
                     count-value id attribute current-key) 
                    grouped-list)
              ;; store new key
              (setf current-key (build-moss-object-key object keys))
              ;(format t "~%; group-moss-list / new current-key ~S" current-key)
              (setf group-value 0)
              (setf count-value 0)
              )
          )
        ;; find attribute
        (dolist (attr (cdr object))
          (if (equal attribute (car attr))
              (setq attr-value (cadr attr))))
        ;; if attribute was found			  
        (if attr-value
            (progn
              (setq attr-value (car (parse-string-to-float attr-value)))
              ;; execute grouping function
              (cond
               ((equal group-function "sum")
                (setq group-value (+ group-value attr-value)) 
                )
               ((equal group-function "count")
                (setq count-value (+ count-value 1))
                )
               ((equal group-function "avg")
                (setq group-value (+ group-value attr-value)) 
                (setq count-value (+ count-value 1))
                )
               )
              )
          )
        ))
    (push (generate-summary-line group-function group-value 
                                 count-value (+ id 1) attribute 
                                 current-key)
          grouped-list)))

#| ???
(group-moss-list 
 '(("project" ("start" "2007") ("montant" "2000") ("année" "2009"))
   ("project" ("start" "2007") ("montant" "1000") ("année" "2008"))
   ("project" ("start" "2011") ("montant" "12000") ("année" "2014"))
   ("project" ("start" "2011") ("montant" "1000") ("année" "2013"))
   ("project" ("start" "2011") ("montant" "6000") ("année" "2012"))
   ("project" ("start" "2011") ("montant" "2000") ("année" "2012")))
 '("start" "année") "montant" "sum")

(("project" ("start" "2007") ("année" "2008") ("id" "5") ("sum(montant)" "1000"))
 ("project" ("start" "2007") ("année" "2009") ("id" "4") ("sum(montant)" "2000"))
 ("project" ("start" "2011") ("année" "2012") ("id" "3") ("sum(montant)" "8000"))
 ("project" ("start" "2011") ("année" "2013") ("id" "2") ("sum(montant)" "1000"))
 ("project" ("start" "2011") ("année" "2014") ("id" "1") ("sum(montant)" "12000")))
|#
;;;-------------------------------------------------------------- IS-ATTR-REP

;; --------------------------------------------------------------------------
;; Check of symbol is a moss attribute (alist format)
;; --------------------------------------------------------------------------

(defun is-attr-rep (ref-symbol &aux res)
  "check if the symbol has a moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  (drformat :json 1 "~%;===== Entering is-attr-rep =====")
  (dformat :json 1 "IAR: ref-symbol: ~S" ref-symbol)
  (if (not (listp ref-symbol))
      (return-from is-attr-rep nil))
  ;; to be an attribute the first element must be a string
  (if (not (stringp (car ref-symbol)))
      (return-from is-attr-rep nil))
  ;; to be an attribute the remainder must be a:
  ;; string, symbol, mln string, an object
  (setq res
        (every #'(lambda(x) 
                   (or (stringp x)    ; standard value
                       (symbolp x)    ; old moss object id symbol
                       (%%is-id? x)   ; new moss object id pair
                       (mln::mln? x)  ; mln
                       (is-obj-rep x) ; current style of object
                       ))
               (cdr ref-symbol)))
  (dformat :json 1 "is-attr-rep returns ~S --" res)
  res)
#|
Careful: with the new notation, obj-ids are no longer attributes
(setq ref-symbol '("marshaller::moss-id" onto::$E-CSCWD-CONCEPT.59 onto::$E-CSCWD-CONCEPT.55))
(is-attr-rep ref-symbol)
T

(m::%%is-id? '(onto::$E-CSCWD-CONCEPT . 59)
T
(setq ref-symbol '("marshaller::moss-id" (onto::$E-CSCWD-CONCEPT . 59)))
(is-attr-rep ref-symbol)
NIL
|#
;;;---------------------------------------------------------- IS-LIST-OBJ-REP

;; --------------------------------------------------------------------------
;; Check of symbol is a list of moss objects (alist format)
;; --------------------------------------------------------------------------
(defun is-list-obj-rep (ref-symbol)
  "check if the symbol is a list of moss objects
Arguments:
   ref-symbol: the moss list
Return:
   a boolean result"
  ;; must be a list
  (if (not (listp ref-symbol))
      (return-from is-list-obj-rep nil))
  ;; evaluate each list member to check its contents	
  (mapc #'(lambda(x) 
            (if (not (is-obj-rep x))
                (return-from is-list-obj-rep nil))) 
    (cdr ref-symbol))
  t)


;;;--------------------------------------------------------------- IS-OBJ-REP

;; --------------------------------------------------------------------------
;; Check of symbol is a moss object (alist format)
;; --------------------------------------------------------------------------

(defun is-obj-rep (ref-symbol)
  "check if the symbol has the moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  ;; must be a list
  (drformat :json 1 "~%;===== Entering is-obj-rep =====")
  (when (not (listp ref-symbol))
      (dformat :json 1 "IOR: fails: arg is not a list")
      (return-from is-obj-rep nil))
  ;; to be an object the first element must be a string
  (when (not (stringp (car ref-symbol)))
    (dformat :json 1 "IOR: fails: leading element not a string")
    (return-from is-obj-rep nil))

  ;; car is a string, the rest must be attributes
  (dformat :json 1 "IOR: ~S is presumably a concept name" (car ref-symbol))
  ;; to be an object the remainder must be attributes
  (mapc #'(lambda(x) 
            (dformat :json 1 "IOR: object pair: ~S" x)
            (unless (is-attr-rep x)
              (dformat :json 1 "IOR: ~S is not an attribute representation" x)
              (return-from is-obj-rep nil))) 
    (cdr ref-symbol))
  t)


;;;------------------------------------------------------- IS-SIMPLE-ATTR-REP
;;; ???

(defun is-simple-attr-rep (ref-symbol)
  "check if the argument obeys the moss object format
Arguments:
   ref-symbol: the moss potential object
Return:
   a boolean result"
  (if (not (listp ref-symbol))
      (return-from is-simple-attr-rep nil))
  ;; to be an attribute the first element must be a string
  (if (not (stringp (car ref-symbol)))
      (return-from is-simple-attr-rep nil))
  ;; to be an attribute the remainder must be a:
  ;; string, mln string
  (mapc #'(lambda(x) 
            (if (and (not (stringp x) ) 
                     (not (mln::mln? x))) ; jpb 1406
                (return-from is-simple-attr-rep nil))) 
    (cdr ref-symbol))
  t)

;;;----------------------------------------------------- MARSHALL-MOSS-OBJECT

;; ==========================================================================
;; Function: marshall-moss-object
;; Creates an alist representation of an object or a list of objects;; 
;; ? (marshall-moss-object (car (access ("cscwd concept" ("label" :is "Domotique")))

;; ==========================================================================

(defun marshall-moss-object (elem &optional inv-rel-to-expand expanded-list)
  (when (not expanded-list)
    (push nil expanded-list))
  (let (result-rep obj-rep to-expand uid summ)
    (cond 
      ;;=== if elem is a moss object
      ((%pdm? elem)
        ;; if elem is already in the expanded list, ignore the expansion
        (when (find elem expanded-list :test #'equal+)
          (return-from marshall-moss-object))
        ;; add type to object
        (push (car (%get-object-class-name elem)) obj-rep)
        ;; look for a unique entry point
        (setq uid (%get-entry-point-if-unique elem))
        ;; if there is no uid, use moss id (note it will not be used by the unmarshaller, 
        ;; only used as a guide!)
        (if uid
            (setq uid (symbol-name uid))
            (setq uid (car (%get-value elem '$id)))
            )
        (push (list "marshaller::uid" uid) obj-rep)
        (push (list "marshaller::moss-id" (car (%get-value elem '$id))) obj-rep)
        ;; get summary
        (setq summ (car (send elem '=summary)))
        (if (stringp summ)
          (push (list "marshaller::summary" summ) obj-rep)
          ;;***** next line won't work with pair ids!
          (push (list "marshaller::summary" (symbol-name summ)) obj-rep)
        )        
        ;; iterate over properties
        (let (prop-name)
          (dolist (property (%get-properties elem))
            ;(format t "~%;MMO: property: ~S" property)
            ;; get property name
            (setq prop-name (car (%%get-value property '$pnam *context*)))
            ;(format t "~%;MMO: prop-name: ~S" prop-name)
            ;; extract canonical name
            (let (partial-rep)
              (push (mln::get-canonical-name prop-name) partial-rep)
              ;(format t "~%;MMO: partial-rep: ~S" partial-rep)
              (cond
                ((%is-attribute? property)
                    ;; iterate over values
                    ;;(dolist (item (%get-value elem property))
                    (dolist (item (send elem '=get (mln::get-canonical-name prop-name)))
                      (push item partial-rep)))
                ((%is-relation? property)
                    ;; iterate over references
                    ;;(dolist (item (%get-value elem property))
                    (dolist (item (send elem '=get (mln::get-canonical-name prop-name)))
                      ;; look for a unique entry point of object
                      (setq uid (%get-entry-point-if-unique item))
                      ;; if there is no uid, use moss id (not it will not be used by the unmarshaller, only used as a guide!)
                      (if (not uid)
                        (setq uid (car (%get-value item '$id)))
                        (setq uid (symbol-name uid))
                      )
                      (push uid partial-rep)
                      ;; if reference is not in the expanded list, so it has to be expanded!
                      (when (not (find item expanded-list))
                        (push item to-expand))))
                (t
                  (error "not an attribute nor a relation")))
              ;; add attribute only if it has values
              (when (> (length partial-rep) 1)
                (push (reverse partial-rep) obj-rep))))) 
        ;; iterate over inverse properties
        (when inv-rel-to-expand
          ;(format t "~%@marshall-moss-object - entered inverse for [~S] - inv-rel-to-expand: [~S]" elem inv-rel-to-expand)                  
          (dolist (inv-rel 
                    (%%get-all-class-inverse-relations 
                      (%%get-id 
                        (car (%get-object-class-name elem)) :class)))
            (let (src-prop src-prop-name src-canonical-name partial-rep)                          
              ;(format t "~%@marshall-moss-object - each inv-rel for [~S] - inv-rel: [~S]" elem inv-rel)            
              ;; get source property
              (setq src-prop (car (%%get-value inv-rel '$inv.of *context*)))
              ;; if property is marked to expand
              (when (find src-prop inv-rel-to-expand)
                ;(format t "~%@marshall-moss-object - each inv-rel for [~S] - src-prop [~S] is desired" elem src-prop)            
                ;; get src property name
                (setq src-prop-name (car (%%get-value src-prop '$pnam *context*)))              
                ;; get canonical name
                (setq src-canonical-name (mln::get-canonical-name src-prop-name))
                ;; add prefix
                (setq src-canonical-name (string+ "inverse::" src-canonical-name))
                ;; iterate over references
                (push src-canonical-name partial-rep)
                (dolist (item (%get-value elem inv-rel))
                  ;; look for a unique entry point of object
                  (setq uid (%get-entry-point-if-unique item))
                  ;; if there is no uid, use moss id (not it will not be used by the unmarshaller, only used as a guide!)
                  (if (not uid)
                    (setq uid (car (%get-value item '$id)))
                    (setq uid (string+ "&" (symbol-name uid)))
                  )                  
                  (push uid partial-rep)
                  ;; if reference is not in the expanded list, so it has to be expanded!
                  (when (not (find item expanded-list))
                    (push item to-expand)
                  )
                )
                ;; add partial representation to object representation
                (when (> (length partial-rep) 1)
                  (push (reverse partial-rep) obj-rep)
                )                
              )
            )
          )
        )
        ;; add the object to the result representation
        (push (reverse obj-rep) result-rep)        
        ;; add object to the expanded-list
        (setf (cdr (last expanded-list)) (cons elem nil))        
        ;; process to-expand list
        (dolist (te to-expand)
          (let ((result (marshall-moss-object te inv-rel-to-expand expanded-list)))
            (when result
              (setq result-rep (append result-rep result))))))

      ;;=== if elem is a list (presumably of moss objects!)
      ((listp elem)
        (dolist (e elem)
          (let ((response (marshall-moss-object e inv-rel-to-expand expanded-list)))
            (when response
              (setq result-rep (append result-rep response))))
        )
        ;; identify direct and indirect objects
        (setq result-rep (mapcar #'(lambda(x)  
            (let (moss-id)
              (setq moss-id (cadr (assoc "marshaller::moss-id" (cdr x) :test #'equal)))
              (if (find moss-id elem)
                (append x (list (list "marshaller::condition" :direct)))
                (append x (list (list "marshaller::condition" :indirect))))               
            )
          ) result-rep))
       )
      ;;=== not an object nor a list
      (t
        (error "element is not an object nor a list")))    
    (return-from marshall-moss-object (reverse result-rep))))
    
#|
(defconcept "credit-card"
	(:att "number")
	(:att "name"))
	
(defconcept "typec"
	(:att "acronym")
	(:att "name")
	(:rel "cards" (:to "credit-card")))

(defrelation "typec" (:from "credit-card") (:to "typec"))

(defindividual "credit-card" ("number" "123") ("name" "Marcio Fuckner")(:var _CMF))
(defindividual "credit-card" ("number" "456") ("name" "Ana Maria")(:var _CAM))
(defindividual "credit-card" ("number" "789") ("name" "Giovani Bernardes")(:var _CGB))

(defindividual "typec" ("acronym" "VISA") ("name" "VISA CORP") ("cards" _CMF _CGB)(:var _TV))
(defindividual "typec" ("acronym" "AMEX") ("name" "American Express") ("cards" _CAM)(:var _TAE))

(send _CMF '=add-values 'has-type `(,_TV))
(send _CAM '=add-values 'has-type `(,_TAE))
(send _CGB '=add-values 'has-type `(,_TV))

? (pprint (m::marshall-moss-object `(,_CMF ,_CAM ,_CGB)))
 
;; in package not MOSS (object ids are pairs)
(("credit-card" ("marshaller::uid" ($E-CREDIT-CARD . 3))
  ("marshaller::moss-id" ($E-CREDIT-CARD . 3))
  ("marshaller::summary" "E-CREDIT-CARD..3") ("number" "789")
  ("name" "Giovani Bernardes") ("marshaller::condition" :DIRECT))
 ("credit-card" ("marshaller::uid" ($E-CREDIT-CARD . 2))
  ("marshaller::moss-id" ($E-CREDIT-CARD . 2))
  ("marshaller::summary" "E-CREDIT-CARD..2") ("number" "456") ("name" "Ana Maria")
  ("marshaller::condition" :DIRECT))
 ("credit-card" ("marshaller::uid" ($E-CREDIT-CARD . 1))
  ("marshaller::moss-id" ($E-CREDIT-CARD . 1))
  ("marshaller::summary" "E-CREDIT-CARD..1") ("number" "123")
  ("name" "Marcio Fuckner") ("marshaller::condition" :DIRECT)));; in package MOSS

;; in package MOSS (object ids are symbols)
(("credit-card" ("marshaller::uid" $E-CREDIT-CARD.3)
  ("marshaller::moss-id" $E-CREDIT-CARD.3) ("marshaller::summary" "E-CREDIT-CARD.3")
  ("number" "789") ("name" "Giovani Bernardes") ("marshaller::condition" :DIRECT))
 ("credit-card" ("marshaller::uid" $E-CREDIT-CARD.2)
  ("marshaller::moss-id" $E-CREDIT-CARD.2) ("marshaller::summary" "E-CREDIT-CARD.2")
  ("number" "456") ("name" "Ana Maria") ("marshaller::condition" :DIRECT))
 ("credit-card" ("marshaller::uid" $E-CREDIT-CARD.1)
  ("marshaller::moss-id" $E-CREDIT-CARD.1) ("marshaller::summary" "E-CREDIT-CARD.1")
  ("number" "123") ("name" "Marcio Fuckner") ("marshaller::condition" :DIRECT)))
|#
;;;-------------------------------------------------------- MOSS-LISP-TO-JSON
#|
(defun moss-list-to-json (result)
  "Creates a JSON representation of a MOSS list of individuals
Arguments:
   result: the resulting moss list
Return:
   the list in a json format"
  (let (json)
    (cond
     ;; is an attribute representation
     ((is-attr-rep result)
      (setq json (concatenate 'string "\"" (car result) "\"" " : "))
      (setq json (concatenate 'string json " [ "))
      (let ((index 0))
        (mapc #'(lambda(x) 
                  (setq json (concatenate 'string json (eval-attr-value x) ))
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (concatenate 'string json " , "))))
          (cdr result)))
      (setq json (concatenate 'string json " ] ")))
     ;; is an object representation
     ((is-obj-rep result)
      (setq json (concatenate 'string json " { "))
      (setq json (concatenate 'string json "\"type\" : \"" 
                   (car result) "\","))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (concatenate 'string 
                               json (moss-list-to-json x))) 
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (concatenate 'string json " , ")))) 
          (cdr result)))
      (setq json (concatenate 'string json " } ")))
     ;; is a list of objects
     ((is-list-obj-rep result)
      (setq json (concatenate 'string json " [ "))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (concatenate 'string 
                               json (moss-list-to-json x))) 
                  (incf index)
                  (if (< index (list-length result))
                      (setq json (concatenate 'string json " , ")))) result))
      (setq json (concatenate 'string json " ] "))))	
    json))
|#
;; --------------------------------------------------------------------------
;; Transform a moss alist representation into a json format
;; --------------------------------------------------------------------------

(defun moss-list-to-json (result &optional language pred-ref )
  "Creates a JSON representation of a MOSS list of individuals
Arguments:
   result: the resulting moss list
   pred-ref (optional): the reference of the predecessor. 
Return:
   the list in a json format"
  (drformat :json 0 "~2%;========== Entering moss-list-to-json ==========")
  (dformat :json 1 "MLTJ: result: ~S" result)
  (setf language (or language *language*))
  (dformat :json 1 "MLTJ: language: ~S" language)
  (let (json card prop-ref list-flag)
    ;; we start by testing if we have an object before testing for attributes
    ;; because testing for attributes recursively calls testing for objects
    (cond
     ;;=== is an object representation
     ((is-obj-rep result)
      (setq json (string+ json " { "))
      (setq json (string+ json "\"marshaller::type\" : \"" (car result) "\","))
      (dformat :json 1 "json: ~S" json)
      (let ((index 0))
        ;; recursively calls function on the object properties that should be sublists
        ;; (attribute {value/object desc}*)
        (mapc #'(lambda(x)
                  (setq json (string+ 
                               json (moss-list-to-json x language (car result) ))) 
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (string+ json " , ")))
                  (dformat :json 1 "json: ~S" json)) 
          (cdr result)))
      (setq json (string+ json " } ")))

     ;;=== is an attribute representation
     ((is-attr-rep result)            
      ;; check attribute cardinality
      (setq list-flag t)
      (setq prop-ref nil)
      (setq card nil)
      (when pred-ref
        (setq prop-ref (%%get-id-for-property (car result) :property :class-ref pred-ref))
        ;; to avoid error when prop is not an actual application property
        (if prop-ref
            (setq card (car (%get-value prop-ref '$maxt))))
        ;; anyway, because most properties have no posted max on the number of values
        ;; the json translation will nearly always be a list even of 1 value.
        ;; one could test the number of values directly on (cdr result) ?
      )
      (dformat :json 1 "pred-ref: ~S, card: ~S" pred-ref card)
      ;; avoid list creation if we have <= one element with cardinality equal to 1
      (when (and (<= (length (cdr result)) 1) (equal card 1))
          (setq list-flag nil))
      (dformat :json 1 "list-flag: ~S" list-flag)
      
      (setq json (string+ "\"" (car result) "\"" " : "))
      (when list-flag
        (setq json (string+ json " [ "))
      )
      (dformat :json 1 "json 1: ~S" json)
      (let ((index 0))
        (mapc #'(lambda(x) 
                  (setq json (string+ json 
                                      (eval-attr-value pred-ref (car result) x language)))
                  (incf index)
                  (if (< index (list-length (cdr result)))
                      (setq json (string+ json " , "))))
          (cdr result)))
      (when list-flag
        (setq json (string+ json " ] ")))
      (dformat :json 1 "json 2: ~S" json)
     
     ;; if empty and without list-flag
     (when (and (equal (length result) 1) (not list-flag))
       (setq json (string+ json "null"))))
      
     ;;=== is a list of objects
     ((is-list-obj-rep result)
      (dformat :json 1 "MLTJ: we got a list presumably of objects")
      (setq json (string+ json " [ "))
      (let ((index 0))
        (mapc #'(lambda(x)
                  (setq json (string+ json (moss-list-to-json x language))) 
                  (incf index)
                  (if (< index (list-length result))
                      (setq json (string+ json " , ")))) result))
      (setq json (string+ json " ] "))))	
    json))

#|
? (moss-list-to-json 

|#

;;;---------------------------------------------------- PARSE-STRING-TO-FLOAT

(defun parse-string-to-float (line)
  "Parse a string to float
Arguments:
  line: a line with potential numbers"
  (with-input-from-string (s line)
    (loop
      :for num := (read s nil nil)
      :while num
      :collect num)))

#|
(parse-string-to-float "30 123")
(30 123)

(parse-string-to-float "30.123")
(30.123)
|#
;;;----------------------------------------------------------- SORT-MOSS-LIST

(defun sort-moss-list (moss-list keys)
  "Executes a non-destructive sort in a list
Arguments:
  moss-list: the source list, e.g. ((\"project\" (\"start\" \"2007\") (...))...)
  keys: the candidate attributes, e.g. (\"start\" \"année\")
Return:
   the list ordered according to the set of keys."
  (let (new-moss-list temp-moss-list)
    (dolist (elem moss-list)
      ;(format t "~% elem: ~S" elem)
      
      ;; navigate through the attributes in order to find key values
      (let (obj-key) ; reset obj-key
        ;; for each sublist of each element of the source list
        ;; e.g. ("project" ("start" "2007") ("montant" "2000") ("année" "2009"))
        (dolist (attrib (cdr elem))
          ;; e.g. attrib = ("start" "2007")
          ;(format t "~% attrib ~S (car attrib) ~S" attrib (car attrib))
          ;; check if some property of the list is one of the keys
          (when (some #'(lambda (x) (if (equal x (car attrib)) x)) keys)                    
            ;(print (cadr attrib))
            ;; if so, build a composite key with values, e.g. "20072009"
            (setf obj-key (concatenate 'string obj-key (cadr attrib)))
            ;(print obj-key)
            ))
        ;; build a list starting with the composite key
        (if obj-key
            (push (list obj-key elem) temp-moss-list))))
    
    ;; now the sorting process
    (setq temp-moss-list (sort temp-moss-list #'string-greaterp :key #'car))
    
    ;; now remove the temporary key
    ;; could use and return: (mapcar #'cadr (reverse temp-moss-list))
    (dolist (elem temp-moss-list)
      (push (cadr elem) new-moss-list))
    
    ;; return sorted list
    new-moss-list))

#|
(sort-moss-list 
 '(("project" ("start" "2007") ("montant" "2000") ("année" "2009"))
   ("project" ("start" "2007") ("montant" "1000") ("année" "2008"))
   ("project" ("start" "2011") ("montant" "12000") ("année" "2014"))
   ("project" ("start" "2011") ("montant" "1000") ("année" "2013"))
   ("project" ("start" "2011") ("montant" "6000") ("année" "2012"))
   ("project" ("start" "2011") ("montant" "2000") ("année" "2012")))
 '("start" "année"))
(("project" ("start" "2007") ("montant" "1000") ("année" "2008"))
 ("project" ("start" "2007") ("montant" "2000") ("année" "2009"))
 ("project" ("start" "2011") ("montant" "6000") ("année" "2012"))
 ("project" ("start" "2011") ("montant" "2000") ("année" "2012"))
 ("project" ("start" "2011") ("montant" "1000") ("année" "2013"))
 ("project" ("start" "2011") ("montant" "12000") ("année" "2014")))
|#
;;;--------------------------------------------------- UNMARSHALL-MOSS-OBJECT

(defun unmarshall-moss-object (elem)
  (let (concept-name original-individual-uid new-individual-uid prop-name 
                     conversion-table condition-list corresp-ref)
    ;; iterate over the elements ignoring the relationships for the moment
    (dolist (e elem)
      (setq concept-name (car e))
      (setq original-individual-uid (cadadr e))
      (setq new-individual-uid nil)
      ;; if id is a string then look for an object related to this "possible" entry point
      (when (stringp original-individual-uid)
        (setq new-individual-uid (car (find-objects-from-text original-individual-uid :target concept-name))))
      ;; create a new individual if needed
      (when (not new-individual-uid)
        (setq new-individual-uid (eval `(defindividual ,concept-name))))
      ;; update converstion table adding old id and new id
      (push (list original-individual-uid new-individual-uid) conversion-table)      
      ;; work with attributes using add addtribute values and remove-attribute-values
      (let (work-list property)
        (dolist (prop (cddr e))
          (setq prop-name (car prop))
          (setq property (%%get-id prop-name :attribute :class-ref concept-name))
          ;; only attribute from now!
          (when property
            (push (cons prop-name (cdr prop)) work-list)))
        (setq work-list (reverse work-list))
        ;; updating attributes
        (dolist (elem work-list)
          (send new-individual-uid '=replace (car elem) (cdr elem)))))
    ;; iterate over the elements - rebuilding relationships
    (dolist (e elem)
      ;; recover concept name, original and new uid
      (setq concept-name (car e))
      (setq original-individual-uid (cadadr e))
      (setq new-individual-uid (cadr (assoc original-individual-uid conversion-table :test #'equal)))
      ;; now it's time to rebuild relationships
      (let (work-list property work-prop)
        ;; iterate over properties to find relationships
        (dolist (prop (cddr e))
          (setq prop-name (car prop))
          (setq property (%%get-id prop-name :relation :class-ref concept-name))
          (when property
            (push prop-name work-prop)
            ;; iterate over each property value
            (dolist (v (cdr prop))
              (setq corresp-ref (assoc v conversion-table :test #'equal))
              (if corresp-ref
                (push (cadr corresp-ref) work-prop)
                (error "unmarshalling error - relationship error")))
            (push (reverse work-prop) work-list)))
        (setq work-list (reverse work-list))
        ;; updating relations
        (dolist (elem work-list)
          (send new-individual-uid '=replace (car elem) (cdr elem)))))    
    ;; retrieve object conditions
    (setq condition-list 
      (mapcar #'(lambda(x) 
                  (list (cadr (assoc "marshaller::uid" (cdr x) :test #'equal)) 
                        (cadr (assoc "marshaller::condition" (cdr x) :test #'equal)))) elem
      )
    )
    (mapcar #'(lambda(x)
                (let (condition)
                  (setq condition (cadr (assoc (car x) condition-list)))
                  (list (cadr x) condition)
                )
              ) conversion-table
    )
  )
)

#|
(unmarshall-moss-object 
'(
 ("credit-card7" ("$id" $E-CREDIT-CARD7.100) ("number" "456") ("name" "Ana Maria") ("type" $E-TYPE7.100))
 ("type7" ("$id" $E-TYPE7.100) ("acronym" "VISA") ("name" "VISA CORP") ("cards" $E-CREDIT-CARD7.100))
 ))
 
 (unmarshall-moss-object 
'(
  ("credit-card8" ("uid" "999") ("number" "999") ("name" "Ana Maria") ("type" "AMEIXA"))
  ("type8" ("uid" "AMEIXA") ("acronym" "AMEIXA") ("name" "American Express") ("cards" "999"))
 ))
 |#
 

;;;(format t "~%;*** MOSS v~A - Web support loaded ***" *moss-version-number*)

:EOF