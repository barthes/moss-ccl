;;;------------------------------------------------------ MAKE-UNIQUE-INSTANCE-REF

(defun make-unique-instance-ref (obj-id)
   "take an object id and build an MLN unique reference using the object class.
Argument:
  obj-id: e.g. ($E-PERSON . 2)
Return:
  e.g. ((:EN \"PERSON-2\")(:FR \"PERSONNE-2\"))"
  (unless (moss::%%is-id?  obj-id)
    (error "Object-id should be pair ~S" obj-id))

   (let ((class-id (car (moss::%get-value obj-id 'moss::$TYPE)))
         class-mln ref nn)
     (setq nn (cdr obj-id))
     ;; get the mln description of the class
     (setq class-mln (car (moss::%get-value class-id 'moss::$enam)))
     (unless (mln::mln? class-mln)
       (error "Bad class-name format for ~S" class-id))

     ;; compute a ref for each language
     (dolist (tag (mln::get-languages class-mln))
       (push
        `(,tag ,(moss::string+ (car (mln::extract class-mln :language tag)) "-" nn))
        ref))
     (reverse ref)))

#|
(cl-user::make-unique-instance-ref _jpb)
((:EN "PERSON-1") (:FR "PERSONNE-1"))
|#