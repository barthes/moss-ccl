;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=================================================================================
;;;20/10/29
;;;		
;;;		       M O S S - D E F I N I T I O N S - (File def.lisp)
;;;	
;;; First created July 1992 
;;;=================================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
;;;	The file contains functions to create classes, properties, and
;;;	methods, by a casual user
;;; 	They can be used only when MOSS has been bootstraped

;;; SYNTAX
;;; ======
;;;   defconcept         calls %make-concept
;;;   defattribute       calls %make-attribute
;;;   defrelation        calls %make-relation
;;;   definstmethod      calls %make-method
;;;   defownmethod       calls %make-ownmethod
;;;   defunivmethod      calls %make-universalmethod
;;;   defobject          calls %make-individual
;;;   defontology        calss %make-ontology
;;;   defvirtualconcept  calls %make-virtual-concept
;;;   defvirtualattribute  ignored
;;;   defvirtualrelation   ignored
;;;   defrule              ignored
;;;
;;; Multilingual names (MLN)
;;; ------------------------
;;; Multilingual names were represented by a list with the following format:
;;;   (:fr "titre principal; titre de chapitre" :en "chapter title")
;;; now revised to
;;;   ((:fr "titre principal" "titre de chapitre") (:en "chapter title"))
;;; Each language has a tag and the associated string may contain synonyms.
;;; One of the synonyms is chosen to build the internal ID and the other synonyms 
;;; are used to define entry points.
;;; Multilingual names can be used to define attributes, relations, classes,...
;;;
;;; When a property is specified by a simple string, this string can be one of
;;; the many synonyms of the generic property. In that case, the name used for
;;; creating the local property should be obtained from the generic property.
;;; Actually, there is no requirement to do that since the role of the internal
;;; ID is to facilitate debugging and has no semantic content.
;;; Another point is when the new multilingual name contains new synonyms, i.e. 
;;; Synonyms that are not present at the generic property level. One has then 
;;; two problems: the first is to get to the generic property (especially when
;;; the first English synonym is new); the second one is to add the new synonyms
;;; to those of the generic property (merge the corresponding MLN).
;;; Example:
;;; assume the generic property name is:
;;;   (:en "person; people" :fr "personne; individu")
;;; and we have a new mln:
;;;   (:en "guy" :fr "individu; zouave") or "guy; people"
;;; we should merge them to obtain;
;;;   (:en "person; people; guy" :fr "personne; individu; zouave")
;;; and add the HAS-GUY, HAS-ZOUAVE entry-points to the generic property.
;;; The name associated with the local property should be $X-CLASS-PERSON rather
;;; than $X-CLASS-GUY, but this is not crucial. 
;;; Locally the entry points will be HAS-GUY, HAS-INDIVIDU, HAS-ZOUAVE but the
;;; inverse property is that of the generic one, namely $X-PERSON.OF (?)
;;;
;;; In this approach, the role of a generic property is to host all the possible 
;;; synonyms for that property. Another approach would be to give the generic 
;;; property only the canonical name. However, it would then be more difficult to
;;; access it through one of the synonyms. Also, more importantly, if the generic
;;; property is defined without sub-properties, it could not have synonyms. 
;;; *** Maybe, the inverse property should contain all possible inverse synonyms,
;;; namely: IS-PERSON-OF, IS-GUY-OF, ..., IS-ZOUAVE-OF, which is not the case 
;;; currently.

;;; Versions
;;; ========
;;; The rationale with versions is the following:
;;; If a property is created in a previous reachable context, then it is inherited 
;;; in the current context and a new property with the same name cannot be built
;;; Only changes can be made to that property (which is not advised since instances
;;; might not comply with the new changes).
;;; If a generic property exists, then it cannot be redefined, but creating a
;;; concept with that property will use the generic property defined previously.
;;; A concept cannot be redefined in a child version or in a parent version.

#|
History
2019 
 0824 imported from its ACL counterpart
 1104 correcting bug in %make-ontology JPB1911
|#

(in-package "MOSS")

;;;===============================  TEST data  ================================
#|
(defpackage :address (:use :moss :cl :ccl))
(in-package :address)

(with-package :address
  (set (intern  "*MOSS-SYSTEM*") 'address::$E-SYSTEM.1)
  (setq address::$E-SYSTEM.1 
        '((MOSS::$TYPE (0 MOSS::$SYS))
          (MOSS::$ID (0 MOSS::$SYS.1))
          (MOSS::$SNAM (0 (:EN "MOSS" :FR "MOSS"))) (MOSS::$PRFX (0 "moss"))
          (MOSS::$EPLS
           (0 MOSS::MOSS))
          (MOSS::$CRET (0 "J-P.A.BARTHES")) (MOSS::$DTCT (0 "January 10"))
          (MOSS::$VERT (0 6.0)) (MOSS::$XNB (0 0)) (MOSS::$FNAM (0 "MOSS8-KERNEL"))
          (MOSS::$CLCS (0 MOSS::$CLS.CTR)))))

(with-package :address (step (moss::%make-generic-tp "Title" '(:unique))))
|#

;;;======================= Macros and functions ===============================


;;;========================== service functions ================================

;;;--------------------------------------------- %CHECK-PACKAGE-VERSION-LANGUAGE

(defUn %check-package-version-language (package context language ref)
  "check that package is an existing package, version is an allowed context ~
   and language is a legal language. If not, throws to :error.
Arguments:
   package: a package
   version: a number
   language: a language tag
   ref: reference of the object being defined (used by error messages)
Return:
   :OK if OK, throws to :error otherwise."
  ;;=== check validity of context, package and language
  ;; check context (uses mthrow to :error)
  ;; prints message to *moss-output* and throws to :error
  (%%allowed-context? context)
  ;; check package (should use mthrow when MOSS window is active)
  (unless (find-package package)
    ;; terror prints message only if *verbose* is t
    (terror " unexisting package ~S when defining ~S" package ref))
  ;; check language, include :all
  (unless (member language (cons :all *LANGUAGE-TAGS*))
    (terror " unknown language ~S when defining ~S ~%; legal ones: ~S"
            language ref *LANGUAGE-TAGS*))
  :OK
  )

#|
(%check-package-version-language *package* 0 :en "albert")
:OK
|#
;;;?--------------------------------------------------------------------- M-LOAD
;;;
;;; The idea is that we can have forward references. However, we do not allow 
;;; forward references for defattribute or defrelation for which class arguments
;;; must precede the call.
;;; When we read the ontology file with a deferred flag, we create:
;;;   - all classes and attributes
;;;   - all links among classes (*deferred-links*)
;;;   - all methods referring to classes (*deferred-methods*)
;;;   - all individuals and attributes (*deferred-instances*)
;;;   - all links among individuals (*deferred-instance-links*)
;;;   - all methods applying to individuals (*deferred-instance-methods*)
;;;   - anything that must be executed last (*deferred-defaults*)
;;; When reading the file and creating the classes, we are in  the file package, 
;;; using the specified context and language. When we are executing the deferred
;;; list we are in an unknown environment. Therefore, we save the environment 
;;; and execute the deferred commands within a with-environment macro.
;;; The reading mode (*deferred-mode*) tells whether we are reading classes
;;; value :class or instances value :instance.
;;; Within a file concepts (classes) and related methods must appear before 
;;; individuals and related methods.

;********** Virtual classes and properties must be taken care of...

;(dformat-set :mld 0)

(defUn m-load (file-name &key external-format)
  "loads MOSS classes allowing forward references in classes and instances.
Arguments:
   file-name: name or pathname of the file containing the file definitions.
   external-format (key): ignored, files should be :utf8 encoded
Return:
   t"
  (declare (special *allow-forward-references* *allow-forward-instance-references*
                    *deferred-links* *deferred-methods* *deferred-instances*
                    *deferred-instances* *deferred-instance-links*
                    *deferred-instance-methods* *deferred-defaults* *deferred-mode*
                    *language*)
           (ignore external-format))
  (drformat :mld 0 "~2%---------- Entering mload")
  
  (let* ((*allow-forward-references* t)
         ;; when executing defindividual, must take care of forward links to other
         ;; instances (*allow-forward-instance-references* t)
         (*allow-forward-instance-references* t)
         (*deferred-links* nil)
         (*deferred-methods* nil)
         (*deferred-instances* nil)
         (*deferred-instance-links* nil)
         (*deferred-instance-methods* nil)
         (*deferred-defaults* nil)
         ;; every time we load a new file, we assume that the file contains an
         ;; ontology or starts with classes. We set the mode to :class, which
         ;; will allow own-method referring to classes or properties to be loaded
         ;; before the own-methods referring to instances
         (*deferred-mode* :class)
         (*language* (or *language* :en))  ; language defaults to English
         message)
    (declare (special *language*))
    ;(setq *debug* t)     ; JPB 201029
    (dformat :mld 0 "file: ~S," file-name)
    (dformat :mld 0 "*allow-forward-references*: ~S," *allow-forward-references*)
    (dformat :mld 0 "*allow-forward-instance-references*: ~S," *allow-forward-instance-references*)

    ;; first load file as usual
    (load file-name :verbose t)
    
    (dformat :mld 0 "file ~S loaded." file-name)
    ;; once loaded, reset non existing reference error mechanism
    (setq *allow-forward-references* nil
        *deferred-links* (reverse *deferred-links*)) ; JPB060228
    
    ;;=== any deferred link definition is first executed
    (dformat :mld 0 "*deferred-instance-links*: ~%  ~S" *deferred-instance-links*)
    (setq message
          (catch :error
                 (dolist (def-expr *deferred-links*)
                   (dformat :mload 0 "m-load: deferred link:~&===> ~S" def-expr)
                   (eval def-expr)
                   )))
    (when (stringp message)
      (error "~A" message))
    ;; reset delayed definition list
    (setq *deferred-links* nil)
    
    ;;=== now we create methods
    (dformat :mld 0 "*deferred-methods*")
    (setq message
          (catch :error
                 (dolist (def-expr (reverse *deferred-methods*))
                   (dformat :mload 0 "m-load: deferred method:~&===> ~S" def-expr)
                   (eval def-expr)
                   )))
    (when (stringp message)
      (error "~A" message))
    ;; reset delayed definition list
    (setq *deferred-methods* nil)
    
    ;(break "m-load /ready to create instances.")
    ;;=== now we create instances (without links), building *deferred-instance-links*
    (dformat :mld 0 "*deferred-instances*")
    (setq message
          (catch :error
                 (dolist (def-expr (reverse *deferred-instances*))
                   (dformat :mload 0 "m-load: deferred instance:~&===> ~S" def-expr)
                   (eval def-expr)
                   )))
    (when (stringp message)
      (error "~A" message))
    ;; reset delayed definition list
    (setq *deferred-instances* nil)
    
    ;; now we execute the deferred instance links
    (setq *allow-forward-instance-references* nil)
    
    ;;=== now link instances
    (dformat :mld 0 "*deferred instance links*")
    (setq message
          (catch :error
                 (dolist (def-expr (reverse *deferred-instance-links*))
                   (dformat :mload 0 "m-load ; deferred instances link:~&===> ~S" def-expr)
                   (eval def-expr)
                   )))
    (when (stringp message)
      (error "~A" message))
    
    (setq *deferred-instance-links* nil)
    
    ;;***** Here we must create own-methods related to properties, so that entry points
    ;; are created. However, we cannot create own-methods related to instances, 
    ;; before creating the instance. Therefore, we must distinguish own methods
    ;; that apply to models and own-methods that apply to instances
    ;;=== now create own methods
    (dformat :mld 0 "*deferred-instance-methods*")
    (setq message
          (catch :error
                 (dolist (def-expr (reverse *deferred-instance-methods*))
                   (dformat :mload 0 "m-load ; deferred instances method:~&===> ~S" def-expr)
                   (eval def-expr)
                   )))
    (when (stringp message)
      (error "~A" message))
    
    (setq *deferred-instance-methods* nil)
    
    ;(mformat "~2%;m-load /*** objects loaded ***")
    ;(sleep 1) ; let the guy print the stuff
    ;; reset language defaulting to English
    ;(setq *language* language)
    
    ;;=== now execute the deferred default actions, anything that must be executed
    ;; last
    (dformat :mld 0 "*deferred-defaults*")
    (dformat :mld 0 "~S" *deferred-defaults*)
    (setq message
          (catch :error
                 (dolist (def-expr (reverse *deferred-defaults*))
                   (dformat :mload 0 "mload ; deferred default creation:~&===> ~S" def-expr)
                   ;(print (eval def-expr))
                   (eval def-expr)
                   )))
    (when (stringp message)
      (error "~A" message))
    (setq *deferred-defaults* nil)
    ;(break "m-load")
    )
  (drformat :mld 0 "~%---------- End mload")
  :done)

#|
(m-load "forward-ref-test")

(dformat-set :mload 0)
(m-load "C:\\Program Files\\Allegro CL 9.0\\MOSS 10.0\\applications\\FAMILY.lisp"
        :external-format :utf8)
(moss::m-load (ask-user-for-directory :browse-include-files t) :external-format :utf8)
|#
;;;=============================================================================
;;;
;;;                       ATTRIBUTE (Terminal Property) 
;;;
;;;=============================================================================

;;; GENERIC PROPERTY
;;; ================
;;; A generic property is
;;;   - a property created when one defines a property linked to a class
;;;     e.g., when defining the attribute "name" for the class "Person" the 
;;;     generic property $T-NAME is created if it does not exist and a sub-property
;;;     $T-PERSON-NAME is created and attached to the class "Person"
;;;   - a property defined when no class is specified
;;; Id
;;; --
;;; A propoerty Id is always a symbol
;;;
;;; Defaults
;;; --------
;;; Generic properties may have defaults when defined independently of a class 
;;; (class-id is NIL)
;;; when a class is specified the :default option is attached to the ideal
;;;
;;; Options
;;; -------
;;; When the generic property is created from a class property, it inherits the
;;; :entry option. E.g. if we write (defconcept "test" (:att "color" (:entry)))
;;; then generic property $T-COLOR will have a =make-entry own method.

;;;------------------------------------------------------------ %MAKE-GENERIC-TP

;;; If the function is called while loading a file, then the corresponding 
;;; package is stable during the execution. If the function is called while
;;; agents are working, then a context switch may break the package reference
;;; (not sure about that...)

;;; Since %make-generic-tp is called from %make-tp, arguments need not be checked
#|
(dformat-set :mgt 0)
(dformat-reset :mgt)
|#

(defUn %make-generic-tp (tp-ref &rest option-list)
  "creates a generic terminal property, that can belong to any object.
   This function is only called while processing a local property.
   Options are ignored. If options are wanted, use %make-global-tp.
Arguments:
   tp-ref: name of property, e.g. AGE, \"age\" or ((:en \"age\"))
   option-list (opt): list of options (ignored)
Returns:
   the id of the generic property, e.g. $S-AGE (applications)"
  (declare (ignore option-list))
  (drformat :mgt 0 "~2%;---------- Entering %make-generic-tp")
  
  ;; thereafter tp-name is the name of the property, e.g. HAS-TITLE
  ;; id is the id of the property, e.g. $T-TITLE
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (tp-name (%%make-name tp-ref :attribute))
         (tp-mln (mln::make-mln tp-ref))
         id)
    
    ;; check first if we have already a terminal property with this name
    (setq id (%%get-id tp-name :tp))
    ;; if property exists, return it (e.g., to use in a class)
    (when id 
      (warn "reusing previously defined generic attribute ~A, in package ~S ~
               and context ~S ignoring eventual options." 
        tp-name *package* context)
      ;; property is not modified
      (return-from %make-generic-tp id))
    
    ;; here, the property does not exist, we must create it 
    ;; first cook up the tp key.
    (setq id (%%make-id :tp :name tp-ref))
    (dformat :mgt 0 "id: ~S" id)
    ;; then check if id already exists
    ;; if so, there is a problem since we know that the property does not exist
    ;; the id symbol must be used for something else...
    (when (%pdm? id)
      (terror "when defining terminal property ~A in package ~S context ~S, ~
               ~%internal identifier ~A already exists with value:~% ~S" 
              tp-name (package-name *package*) context id (<< id)))
    
    ;; otherwise create property object
    (>> id `(($TYPE (,context $EPT))
              ($ID (,context ,id))
              ($PNAM (,context ,tp-mln))))
    
    ;; when editing declare it as new to avoid saving it as old in %link
    (save-new-id id)
    
    ;; add new property-id to local system-list of attributes
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ETLS id)
    
    ;; build entry-point if it does not exist already or add entry
    ;; we must build an entry point for each synonym in each language
    (%make-ep-from-mln tp-mln '$PNAM id :type '$EPT)
    
    ;; Now we consider inverse property: id provides the package info, tp-mln the 
    ;; language info
    (%make-inverse-property id tp-mln)
    
    ;; build external-name for temporary use only (current session), e.g. _HAS-NAME
    ;; not really useful in case of multilingual definitions with synonyms
    (set (%%make-name tp-name :var) id)
    
    ;; when editing
    (save-new-id (%%make-name tp-name :var))
    
    ;; create access function to allow to use property name in methods
    ;; If already defined (and exported) in MOSS, do not redefine it
    (unless (fboundp tp-name)
      (%make-prop-get-accessor id tp-name)
      (%make-prop-set-accessor id tp-name)
      )
    
    (drformat :mgt 0 "~%;---------- End %make-generic-tp")
    ;; return tp-id
    id))

#|
cf z-moss-tests-def.lisp
|#
;;;------------------------------------------------------------- %MAKE-GLOBAL-TP
;;; this function is called when defining a property without class reference
;;; e.g. for use by orphans. In that case options are taken into account

(defUn %make-global-tp (tp-ref &rest option-list)
  "creates a generic terminal property. Options are taken into account only ~
   when defining a property independently from a class.
Arguments:
   tp-ref: a symbol, string or multilingual name of the attribute, 
               e.g. TITLE, \"title\", (:fr \"Titre\" :en \" title ; header\")
           Will produce HAS-TITLE and a settable HAS-TITLE accessor.
               (:fr \"Titre\") will produce HAS-TITRE !
   Option list:
     (:class-ref <class-ref>)  name of the class to which the property is attached
     (:concept <class-ref>) id.
     (:context <number>)      must be a legal context
     (:default <value>)       default value (attached to the property)
     (:entry {<function-descriptor>})	specify entry-point
			                if no arg uses make-entry, otherwise uses
			                specified function
                     where 
                        <function-descriptor>::=<arg-list><doc><body>
	               e.g.  (:entry (value-list) \"Entry for Company name\"
	       			     (intern (make-name value-list)))
     (:min <number>)	       minimal cardinality
     (:max <number>)	       maximal cardinality
     (:unique)	               minimal and maximal cardinality are each 1
     (:doc <documentation>)    documentation ref
     (:type <type>)            values should be of type :number, :string, or :mln
     (:not-type <type>)        values should not be of that type
     (:value <value>)          value should be that value, a way of imposing a value
                               defined at the property level
     (:one-of <instances>+)    list of specific values
     (:forall)                 all values should be taken from the one-of list or of
                               the specified type
     (:exists)                 there should be at least one value taken from the one-of
                               list or of the specified type        
     (:not-in)                 no value should be in the one-of list
     (:same)                   all values should be identical
     (:different)              all values should be different
  options type, not-type, value, one-of/forall are strict, meaning that if the values
  do not comply with the restriction, they are not recorded.
Return:
   the id of the generic property in the specific package in the specific context"
  
  ;; thereafter tp-name is the name of the property, e.g. HAS-TITLE
  ;; id is the id of the property, e.g. $T-TITLE
  (let* ((class-ref (or (car (getv :class-ref option-list))
                        (car (getv :concept option-list))))
         (context (or (car (getv :context option-list))
                      (symbol-value (intern "*CONTEXT*"))))
         tp-name tp-mln id class-id pair ;tp-synonym-list
         )
    
    ;; check if allowed. Throw to :error if illegal
    (%%allowed-context? context)
    
    (with-context context
      (setq tp-name (%%make-name tp-ref :attribute))
      
      ;; check then if we have already a terminal property with this name
      (setq id (%%get-id tp-name :tp))
      ;; if property exists, return it (e.g., to use in a class)
      (when id 
        (warn "reusing previously defined generic attribute ~A, in package ~S ~
               and context ~S ignoring eventual options (class: ~S)." 
          tp-name *package* context class-ref)
        ;; property is not modified
        (return-from %make-global-tp id))
      
      ;; build mln from input arg, preserving multilingual data
      (setq tp-mln (mln::make-mln tp-ref)) ; jpb1406
      
      ;; check for existing class (must exist if mentioned).
      (when class-ref 
        ;; recover the class-id if there
        (setq class-id (%%get-id class-ref :class))
        (unless (%is-class? class-id)
          (terror "Cannot find the specified class ~S with id ~S when defining prop ~S"
                  class-ref class-id tp-name)))
      
      ;; here, the property does not exist, we must create it 
      ;; first cook up the tp key.
      (setq id (%%make-id :tp :name tp-ref))
      ;; then check if id already exists
      ;; if so, there is a problem since we know that the property does not exist
      ;; the id symbol must be used for something else...
      (when (%pdm? id)
        (terror "when defining terminal property ~A in package ~S, ~
               internal identifier ~A already exists" 
                tp-name *package* id))
      
      ;; otherwise create property object
      (set id `(($TYPE (,context $EPT))
                ($ID (,context ,id))
                ($PNAM (,context ,tp-mln))))
      
      ;; when editing declare it as new to avoid saving it as old in %link
      (save-new-id id)
      ;; add new property-id to local system-list
      (%link (symbol-value (intern "*MOSS-SYSTEM*"))
             '$ETLS id) ; add to system list of tp
      
      ;; build entry-point if it does not exist already or add entry
      ;; we must build an entry point for each synonym in each language
      (%make-ep-from-mln tp-mln '$PNAM id :type '$EPT)
      
      ;; Now we consider inverse property: id provides the package info, tp-mln the 
      ;; language info
      (%make-inverse-property id tp-mln)
      
      ;;=== process now OPTION LIST unless it is a user's property with a specified
      ;; class, in which case the options will be attached to the specific class
      ;; property
      
      ;; an exception id the default option
      ;; default is normally attached to the ideal of the corresponding class
      ;; if the property is defined without reference to a class, then the
      ;; default is attached to the property itself. However, if the property is used
      ;; later to support a local property with a different default, the default
      ;; attached to ideal will shadow the default attached to the property. 
      ;; we only do that for system properties, or for generic properties that
      ;; do not belong to a class. Otherwise, the default option is processed in the
      ;; %make-tp function
      (cond
       ((and (assoc :default option-list)(null class-id))
        ;; sytem property, or simply generic one
        (%%set-value-list id (getv :default option-list) '$DEFT context)))
      
      ;; when defining a new generic property, check the :entry option and, if there,
      ;; build the entry function (outside the case loop)
      (when (setq pair (or (assoc :entry option-list) (assoc :index option-list)))
        (if (cdr pair)
            ;; specific function defined in the option
            (apply #'%make-ownmethod '=make-entry id (cdr pair))
          ;; no function specified, attach default function
          (%make-ownmethod :default-make-entry id nil nil)))
      
      ;; when we have a property without reference to a class
      ;; i.e. when we are defining a generic property directly, we build all options
      (unless class-id
        (while option-list
               (case (caar option-list)
                 (:min
                  (%%set-value id (cadar option-list) '$MINT context)
                  )
                 (:max
                  (%%set-value id (cadar option-list) '$MAXT context)
                  )
                 (:unique	;; has both min and max to 1
                  (%%set-value id 1 '$MINT context)
                  (%%set-value id 1 '$MAXT context)
                  )
                 ;; Record documentation if specified
                 (:doc	
                  (%process-doc-option id (cdar option-list))
                  )
                 (:type   ;; set type of value
                  (%%set-value id (cadar option-list) '$TPRT context)
                  )
                 (:not-type  ;; must not be of that type
                  (%%set-value id (cadar option-list) '$NTPR context)
                  )
                 (:value  ;; a way to impose a value defined at property level
                  (%%set-value id (cadar option-list) '$VALR context)
                  )
                 ;; if values are specified, record them
                 (:one-of
                  ;; we should make sure that the values are of the right type and/or
                  ;; execute the =xi method
                  (%%set-value-list id (cdar option-list) '$ONEOF context)
                  ;; following test sets :one-of as a default selection
                  (unless (or (%%has-value id '$SEL context)
                              (assoc :exists option-list)
                              (assoc :forall option-list))
                    (%%set-value id :exists '$SEL context)))
                 ;; :exists and :forall make sense only when :one-of is specified
                 (:exists
                  (if (or (%%has-value id '$ONEOF context)
                          (assoc :one-of option-list))
                      (%%set-value id :exists '$SEL context)
                    (terror ":exists option requires the use of the :one-of option ~
                             for generic attribute ~S" tp-name)))
                 (:forall 
                  (if (or (%%has-value id '$ONEOF context)
                          (assoc :one-of option-list))
                      (%%set-value id :forall  '$SEL context)
                    (terror ":forall option requires the use of the :one-of option ~
                             for generic attribute ~S" tp-name)))
                 (:not-in 
                  (if (or (%%has-value id '$ONEOF context)
                          (assoc :one-of option-list))
                      (%%set-value id :not '$SEL context)
                    (terror ":not-in  option requires the use of the :one-of option
                             for generic attribute ~S" tp-name))
                  )
                 ((:between :outside)
                  (%%set-value-list id (car option-list) '$OPR context)
                  )
                 ((:same :different)
                  (%%set-value-list id (car option-list) '$OPRALL context)
                  )
                 
                 ) ; end case
               ;; all other options are ignored
               (pop option-list)
               )
        )
      ;(format t "~%== id: ~S - flag: ~S - tp-name: ~S" id *moss-engine-loaded* tp-name)
      ;; build external-name for temporary use only (current session), e.g. _HAS-NAME
      ;; not really useful in case of multilingual definitions with synonyms
      (set (%%make-name tp-name :var) id)
      ;; create access function to allow to use property name in methods
      ;; If already defined (and exported) in MOSS, do not redefine it
      (unless (fboundp tp-name)
        (%make-prop-get-accessor id tp-name)
        (%make-prop-set-accessor id tp-name)
        )
      
      ;; when editing - done at the beginning
      ;(save-new-id id)
      
      ;; return tp-id
      id)))

#|
 cf z-moss-tests-def.lisp
|#
;;;-------------------------------------------------------------- MAKE-ATTRIBUTE

(defun make-attribute (att-ref &rest option-list)
  (apply #'%make-tp att-ref option-list))

;;;-------------------------------------------------------------------- %MAKE-TP

;;; make-tp creates a terminal property. Several cases occur:
;;;  - if the property does not exist, then
;;;    - first a generic property is created
;;;    - then if a class is specified, we create a specific local class property,
;;;      sub-property of the generic property with the same property name.

;;; The first argument to %make-tp is either a simple string or a multilingual-
;;; name.
;;;    The latter case is reserved for creating a new property.
;;;    The first case is for reusing an already existing property. Thus the
;;; simple string must reference the generic property.
;;; A problem may occur when defining a new property with a list of synonyms that
;;; are not already taken into account by the generic property. The new synonyms
;;; should be merged at the generic property level. Not implemented yet.

;;; when used at run time in competing environments, context and package must
;;; be specified as local variables, since they are shared by concurrent processes

#|
(dformat-set :mkt 0)
(dformat-reset :mkt)
|#

(defUn %make-tp (tp-ref &rest option-list)
  "(%make-tp tp-ref &rest option-list) - tp-ref is the external attribute name ~
   (should be a reference).
Arguments:
   tp-ref: string, symbol or mln (:fr \"Titre\" :en \" title ; header\") ~
   Will produce HAS-TITLE and a settable HAS-TITLE accessor.
           (:fr \"Titre\") will produce HAS-TITRE !
   Option list:
     (:class-ref <class-ref>) to attach to a class
     or (:concept <class-ref>)
     (:context <number>       must be a legal context (currently ignored)
     (:default <value>)	      default value
     (:doc <doc-string>)      documentation
     (:entry {<function-descriptor>})	specify entry-point
           if no arg uses make-entry, otherwise uses specified function
               where 
               <function-descriptor>::=<arg-list><doc><body>
	       e.g.  (:entry (value-list) \"Entry for Company name\"
	       		(intern (make-name value-list)))
     (:min <number>)	       minimal cardinality
     (:max <number>)	       maximal cardinality
     (:type <type>)            values should be of type :number, :string, or :mln
     (:not-type <type>)         values should not be of that type
     (:value <value>)          value should be that value, a way of imposing a value
                               defined at the property level
     (:unique)		       minimal and maximal cardinality are each 1
     (:one-of <instances>+)    list of specific values
     (:forall)                 all values should be taken from the one-of list or of
                               the specified type
     (:exists)                 there should be at least one value taken from the one-of
                               list or of the specified type        
     (:not-in)                 no value should be in the one-of list
     (:same)                   all values should be identical
     (:different)              all values should be different
     (:between <value> <value>) all values should be numerical and in this range
     (:outside <value> <values>) all values should be numerical and outside that range
  options type, not-type, value, one-of/forall are strict, meaning that if the values
  do not comply with the restriction, they are not recorded.
  Done as follows
	- check if local property already exists - if so, then error
  If class-ref is not present, then calls %make-globl-tp
Return:
   the prop id."
  (drformat :mkt 0 "~2%;---------- Entering %make-tp")
  
  ;; check for format error in the list of options
  (unless (every #'listp option-list)
    (error "every option should be a list in: ~%  ~S" option-list))
  
  (when (set-difference (mapcar #'car option-list) *attribute-options*)
    (error 
      "%make-tp: Illegal attribute options: ~S when defining ~S"
        (set-difference (mapcar #'car option-list) *attribute-options*)
        tp-ref))          
  
  (let* ((context (symbol-value (intern "*CONTEXT*"))) ; context is current
         (class-ref (car (or (getv :class-ref option-list)
                             (getv :concept option-list))))
         ;(id (car (getv :id option-list))) ; JPB1607 (only for system props)
         tp-mln tp-name id class-id)
    
    ;; if tp-ref is not a string, symbol or mln complain
    (unless (or (stringp tp-ref)(symbolp tp-ref) (mln::mln? tp-ref))
      (error "%make-tp: attribute ~S should be string, symbol or mln" tp-ref))
    
    ;; check that package is an existing package, context is an allowed context 
    ;; and language is a legal language. If not, throw to :error.
    (%check-package-version-language *package* context *language* tp-ref)
    
    ;; get something like ((:fr "sous titre")) - result is garanteed
    ;; this allows using simple strings with synonyms
    (setq tp-mln (mln::make-mln tp-ref)) ; jpb1406
    
    ;; pick up one of the names for creating attribute name, e.g. HAS-TITLE
    ;; works with string, mln and symbols like TITLE or HAS-TITLE
    (setq tp-name (%%make-name tp-mln :tp))
    
    ;; then, if we have no class option, we return after creating global prop
    (unless class-ref
      (return-from %make-tp (apply #'%make-global-tp tp-mln option-list)))
   
    ;;=== Possible errors when a class is specified
    ;;== 1. inexisting class
    (setq class-id (%%get-id class-ref :class))
    ;; if id is not that of a class, then error
    (unless class-id 
      (terror "when defining attribute ~S in package ~S and context ~S, ~
              unknown class/concept reference: ~S" 
              tp-ref (package-name *package*) context class-ref))
    
    ;;== 2. internal local tp-id associated with class already exists
    ;; Now produce the local ID, e.g. $T-BOOK-TITLE
    (setq id (or id (%%make-id :tp :name tp-ref :prefix class-ref))) ; JPB1607
    ;; then check if id already exists. 
    (when (%pdm? id)
      (terror "when defining attribute ~S in package ~S and context ~S, ~
               for class/concept ~A, ~
               ~&internal id ~S already exists with value:~%  ~S" 
              tp-ref (package-name *package*) context class-ref id (<< id)))
    ;;=== End major errors
    
    ;; we create a specific terminal property for the specified class
    ;; and link it as a sub-property to the generic property 
    ;; Property name is the same as the generic one, id is different. Options
    ;; may be different and normally SHOULD shadow those of the generic options.
    ;; However, we do not check 
    ;; Inverse terminal property is that of the local property, however name is 
    ;; the same as generic property, e.g. is-first-name-of, and not 
    ;; is-person-first-name-of
    
    ;; call the required function 
    (apply #'%%make-tp-from-ids tp-mln tp-name id class-id option-list)
    ))

#|
 cf z-moss-tests-def.lisp for defattribute
|#
;;;---------------------------------------------------------- %%MAKE-TP-FROM-IDS
 
(defUn %%make-tp-from-ids 
    (tp-mln tp-name id class-id &rest option-list)
  "low level function to create property and handle options ~
   while creating an attribute of a specific class. Called by %make-tp.
   Should not be called directly.
Arguments:
   tp-mln: MLN of the attribute being created, e.g. ((:en \"title\"))
   tp-name: symbol specifying the attribute being created, e.g. HAS-TITLE
   id: local TP id, e.g. $T-BOOK-TITLE
   class-id: identifier of the class of the attribute, e.g. $E-BOOK
   option-list (rest): list of original options
Return:
   the attribute id or NIL in case of error"
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        ;; now get class-name associated with $ENAM (multilingual-string) -> BOOK
        (class-name (%%make-name (car (%get-value class-id '$ENAM)) :class))
        gen-id prop-list inv-id)
    
    ;; check just in case
    (unless class-id
      (warn "missing class-id when trying to link attribute ~S in package ~S context ~S"
        tp-name (package-name *package*) context)
      (return-from %%make-tp-from-ids nil))
    
    ;; Now check if we have properties with same prop name, e.g. HAS-TITLE
    ;; works even when HAS-TITLE is unbound (not yet created)
    (setq prop-list (%extract-from-id tp-name '$PNAM `$EPT))
    ;; entry-points like HAS-TITLE are valid across packages, thus we get a list
    ;; of properties in various packages, that we must filter 
    (setq prop-list (%filter-against-package prop-list *package*))
    ;; we must also eliminate any property that is not an attribute
    ;; we could obtain ($T-TITLE $T-MAGAZINE-TITLE $T-PERSON-TITLE) or NIL
    (setq prop-list (remove-if #'(lambda (xx) (not (%is-attribute? xx))) prop-list))
    
    ;; ... then get generic one, otherwise create it
    (setq gen-id
          (or (%get-generic-property prop-list) ; nil, if prop-list is nil
              (apply #'%make-generic-tp tp-mln option-list)))
    
    ;; create property object
    (set id  `(($TYPE (,context $EPT))
               ($ID (,context ,id))
               ($PNAM (,context ,tp-mln))))
    
    ;; save new object id when editing
    (save-new-id id)   
    
    ;; add new property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ETLS id)
    ;; declare it as a sub-property of generic property
    (%link id '$IS-A gen-id)
    ;; build all entry-points for each synonyms if they do not exist already
    (%make-ep-from-mln tp-mln '$PNAM id :type '$EPT)
    
    ;; ...
    ;; Now we consider inverse property. Simply build it with entry points
    (setq inv-id (%make-inverse-property id tp-mln))
    ;; when editing
    (save-new-id inv-id)
    
    ;; process now option list
    (while option-list
           (case (caar option-list)
             ;; link to class if it was specified in the options
             ((:class-ref :concept) 
              ;; we already processed those options
              (%link class-id '$PT id)
              )
             (:default
                 ;; default is normally attached to the ideal of the corresponding class
                 ;; Defaults attached to ideals shadow defaults attached to properties. 
                 (%%set-value-list (%%make-id :ideal :class-id class-id)
                                   (cdar option-list) id context)
                 )
             ;; Record documentation if specified
             (:doc	
              (%process-doc-option id (cdar option-list))
              )
             ;; entry option allows us to associate entry-points with attributes
             ;; can be different from the generic one
             
             ((:entry :index)
              (if (cdar option-list)
                  ;; specific function defined in the option
                  (apply #'%make-ownmethod '=make-entry id (cdar option-list))
                ;; no function specified, attach default function
                (%make-ownmethod :default-make-entry id nil nil))
              )
             (:min
              (%%set-value id (cadar option-list) '$MINT context)
              )
             (:max
              (%%set-value id (cadar option-list) '$MAXT context)
              )
             (:unique	;; has both min and max to 1
              (%%set-value id 1 '$MINT context)
              (%%set-value id 1 '$MAXT context)
              )
             (:type   ;; set type of value
              (%%set-value id (cadar option-list) '$TPRT context)
              )
             (:not-type  ;; must not be of that type
              (%%set-value id (cadar option-list) '$NTPR context)
              )
             (:value  ;; a way to impose a value defined at property level
              (%%set-value id (cadar option-list) '$VALR context)
              )
             ;; if values are specified, record them
             (:one-of
              ;; we should make sure that the values are of the right type and/or
              ;; execute the =xi method
              (%%set-value-list id (cdar option-list) '$ONEOF context)
              ;; following test sets :one-of as a default selection
              (unless (or (%%has-value id '$SEL context)
                          (assoc :exists option-list)
                          (assoc :forall option-list)
                          (assoc :not-in option-list))
                (%%set-value id :exists '$SEL context)))
             ;; :exists and :forall make sense only when :one-of is specified
             (:exists
              (if (or (%%has-value id '$ONEOF context)
                      (assoc :one-of option-list))
                  (%%set-value id :exists '$SEL context)
                (terror ":exists option requires the use of the :one-of option in ~S ~
                    for ~S" tp-name class-name))
              )
             (:forall 
              (if (or (%%has-value id '$ONEOF context)
                      (assoc :one-of option-list))
                  (%%set-value id :forall  '$SEL context)
                (terror ":forall  option requires the use of the :one-of option in ~S ~
                    for ~S" tp-name class-name))
              )
             (:not-in 
              (if (or (%%has-value id '$ONEOF context)
                      (assoc :one-of option-list))
                  (%%set-value id :not '$SEL context)
                (terror ":not-in  option requires the use of the :one-of option in ~S ~
                    for ~S" tp-name class-name))
              )
             ((:between :outside) ; apply to a single value
                  (%%set-value-list id (car option-list) '$OPR context)
              )
             ((:same :different) ; apply to a set of values
                  (%%set-value-list id (car option-list) '$OPRALL context)
              )
             ) ; end case
           (pop option-list)
           )
    ;; build external-name for temporary use only (current session)
    ;; build names for each synonym associated with the local property
    ;; we save temporary variables, in case of abort
    (mapc #'(lambda (xx)
              (let ((var-id 
                     (%%make-name
                      (%%make-name-string xx :prop :prefix class-name)
                      ;(%make-name-string-for-concept-property xx class-name)
                      :var)))
                (set var-id id)
                ;; when editing
                (save-new-id var-id)
                ))
      (mln::extract-all-synonyms tp-mln))
    
    ;; return tp-id
    id))

#|
 cf z-moss-tests-def.lisp for defattribute
|#
;;;=============================================================================
;;;
;;;                RELATIONS (Structural Properties / sp)
;;;
;;;=============================================================================

;;; (%defsp name key class suc &rest option-list) - creates a terminal property
;;; which is a link between class and suc. If classes do not exist it is an
;;; error
;;;     class can be the name of the class or the list (:class-id class-id). This
;;;     is used by defclass when creating new relationships
;;; Syntax of options is
;;;	(:is-a <class-id>*)	for defining inheritance
;;;	(:min <number>)		minimal cardinality
;;;	(:max <number>)		maximal cardinality
;;;	(:unique)		minimal and maximal cardinality are each 1
;;;     (:var <var-name>)       variable name to record prop id
;;;     (:doc <documentation>)  documentation (can be multilingual)
;;;  and (?)
;;;     (:one-of ...)
;;;     (:default ...)
;;;
;;; The :one-of option implies that the instances of objects are available at the 
;;; time the class is created, which is difficult since the instances are of 
;;; the same class as the one being defined.
;;; Example: creating the class professor with a default as $E-PROFESSOR.1
;;; The problem may be solved if one first creates the class and then the instances
;;; using a :new option, and then links the instances to the class...
;;; The ;default option has the same problem, however, one must link the instances
;;; to the ideal of the class.

;;;------------------------------------------------------------ %MAKE-GENERIC-SP
;;; the difference between %make-generic-sp and %make-global-sp is that the first
;;; one is called when creating a local property and does not take any options
;;; and the second one is used to create a generic property with options. It 
;;; could be used for creating relations for orphans
#|
(dformat-set :mgs 0)
(dformat-reset :mgs)
|#
(defUn %make-generic-sp (sp-ref &rest option-list)
  "creates a generic structural property, able to link any object to any object.
   This function is only called while processing a local property.
   Options are ignored.
Arguments:
   sp-ref: name of property, e.g. FATHER, \"father\" or (:en \"father\")
   option-list (opt): list of options (ignored)
Returns:
   the id of the generic property, e.g. $S-SON (applications)"
  ;(declare (ignore option-list))
  (drformat :mgs 0 "~2%;---------- Entering %make-generic-sp")

  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (sp-mln (mln::make-mln sp-ref))
         (sp-name (%%make-name sp-ref :relation))
         id)
    ;; check first if we have a structural property with this name
    (setq id (%%get-id sp-name :sp))
    (dformat :mgs 0 "sp-name: ~S" sp-name)

    ;; for the time being we are happy if property exists
    (when id
      (warn "reusing previously defined generic relation ~A, in package ~S ~
             and context ~S ignoring eventual options (class-ref: ~S)." 
            sp-name (package-name *package*) context (cadr (assoc :from option-list)))
      (return-from %make-generic-sp id))

    ;; here, the property does not exist, we must create it 
    
    ;; first cook up the sp key. 
    (setq id (%%make-id :sp :name sp-mln))
    (dformat :mgs 0 "id: ~S" id)
    ;; check if id already exists defining a different property
    ;; it is allowed to exist but only as a structural property
    (when (%pdm? id)
      (terror "~&;***Error when defining relation ~& ~S in package ~S, ~
               ~& internal name ~S already exists but is not a relation: ~S"
              sp-ref (package-name *package*) id (eval id)))
    
    ;; otherwise create new object
    (>> id  `(($TYPE (,context $EPS))
              ($ID (,context ,id))
              ($PNAM (,context ,sp-mln))
              (,(%inverse-property-id '$PS) (,context ,(intern "*ANY*")))
              ($SUC (,context ,(intern "*ANY*")))
              ))			; inverse prop is missing yet
    
    ;; when editing
    (save-new-id id)

    ;; add new property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ESLS id)

    ;; build entry-points one for each synonym
    (%make-ep-from-mln sp-mln '$PNAM id :type '$EPS)
    
    ;; Now we consider inverse property
    (%make-inverse-property id sp-mln)
    
    ;; build external-name for temporary use only (current session)
    (set (%%make-name sp-name :var) id)

    ;; when editing
    (save-new-id (%%make-name sp-name :var))
    
    ;; create access function unless engine is not loaded yet 
    ;; add now a macro to allow to use property name as a function in methods
    (unless (fboundp sp-name)
      (%make-prop-get-accessor id sp-name)
      (%make-prop-set-accessor id sp-name)
      )
    
    (drformat :mgs 0 "~%;---------- End %make-generic-sp")
    ;; return sp-id
    id))

#|
 cf z-moss-tests-def.lisp for defattribute
|#
;;;------------------------------------------------------------- %MAKE-GLOBAL-SP
;;; this function is called if
;;;   - we have :any as a domain specification and range specification
;;; in both cases, we process the options
;;; The function is similar to %make-generic-sp, but processes the option list.
;;; When the function is called, class-id and suc-id should be defined; thus, the
;;; default option can be processed
;;; Used by %make-sp

(defUn %make-global-sp (multilingual-name &rest option-list)
  "creates a global structural property taking options into account.
Arguments:
   multilingual-name: name of property, e.g. PRESIDENT
   option-list (opt): list of options
 Syntax of options is
        (:id <id>)              prop id (synthesized if not present)
                                reserved for system
	(:min <number>)		minimal cardinality
	(:max <number>)		maximal cardinality
	(:unique)		minimal and maximal cardinality are each 1
        (:var <var-name>)       variable name to record prop id
Returns:
   the id of the generic property, e.g. $ELS (system) or $S-SON (applications)"
  (drformat :msp 0 "~2%;---------- Entering %make-global-sp")
  
  (catch-system-error "while defining global relation"
    (setq multilingual-name (mln::make-mln multilingual-name)))
  (dformat :msp 0 "multilingual-name: ~S" multilingual-name)
  
  ;; error if not mln format
  (unless multilingual-name ; jpb 1406
    (terror "(%make-global-sp) relation: ~S is not a multilingual name." 
            multilingual-name))
  
  ;; if forward references are allowed record action onto the *deferred-links* list
  (when *allow-forward-references*
    (push `(apply #'%make-global-sp ',multilingual-name ',option-list)
          *deferred-links*)
    (return-from %make-global-sp nil))
  
  ;;===== otherwise create property
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (sp-mln (mln::extract-mln-from-ref multilingual-name *language*))
         (sp-name (%%make-name sp-mln :prop))
         prop-list id suc-list)
    
    ;; check first if we have a structural property with this name
    (setq prop-list (%extract-from-id sp-name '$PNAM '$EPS))
    (dformat :msp 0 "prop-list: ~S" prop-list)
    ;; return generic property, if property exists
    (when prop-list
      (warn "when attempting to define relation ~S, a relation already ~
             exists with the same name. We are reusing it." multilingual-name)
      (return-from %make-global-sp (%get-generic-property prop-list)))
    
    ;;=== create property
    ;; first cook up the sp key. We can leave the :id option on the option-list
    ;; it will be ignored during the processing of options
    (setq id (or (car (getv :id option-list)) 
                 (%%make-id :sp :name sp-mln)))
    (dformat :msp 0 "id: ~S" id)
    
    ;; check if id already exists defining a different property
    ;; it is allowed to exist but only as a structural property
    (if (boundp id)
      (terror "~&;***Error when defining general rlation~& ~S in package ~S, ~
               ~& internal symbol ~S already exists and bound : ~&~S" 
              multilingual-name *package* id (eval id)))
    
    ;; create new object
    (>> id  `(($TYPE (,context $EPS))
              ($ID (,context ,id))
              ($PNAM (,context ,sp-mln))
              (,(%inverse-property-id '$PS) (,context ,(intern "*ANY*")))
              ($SUC (,context ,(intern "*ANY*")))
              ))			; inverse prop is missing yet
    
    ;; when editing
    (save-new-id id)

    ;; add new property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ESLS id)
    
    ;; build entry-points one for each synonym
    (%make-ep-from-mln sp-mln '$PNAM id :type '$EPS)
    
    ;; ...
    ;; Now we consider inverse property
    (%make-inverse-property id sp-mln)
    
    ;;=== process now OPTION LIST; not when defining a generic property
    (while option-list
      (case (caar option-list)
        ;; ignore some options
        ((:language :package :context :export))
        (:is-a	;; superproperty must exist (this is done to catch
         ;; spelling mistakes)
         (warn ":is-a option disallowed as yet for relations ***")
         )
        (:min
         (%%set-value id (cadar option-list) '$MINT context)
         )
        (:max
         (%%set-value id (cadar option-list) '$MAXT context)
         )
        (:unique	;; has both min and max to 1
         (%%set-value id 1 '$MINT context)
         (%%set-value id 1 '$MAXT context)
         )
        ;; we can specify an external variable name
        (:var  
         (if (symbolp (cadar option-list))
           (set (cadar option-list) id))
         )
        ;; Record documentation if specified
        (:doc	 
         (%process-doc-option id (cdar option-list))
         )
        ;; process default option processing the various formats
        (:default
            (apply #'%%make-sp-default-id id (intern "*ANY*") (intern "*ANY*")
                   sp-name option-list)
            ;; because generic property is linked with *any* no check on defaults
            ;; when there is an owner class, default is attached to ideal
            )
        (:one-of ; JPB121029
         ;; following function will try to locate instances of objects of class
         ;; suc-id. If none is found, then no link is done and we send a warning
         (setq suc-list 
               (%resolve-successors-for-one-of (cdar option-list) 
                                               (intern "*ANY*")))
         ;; we add result only if result is non nil
         (cond
          (suc-list
           (%%set-value-list id suc-list '$ONEOF context)
           ;; following test sets :exists as a default selection
           (unless (or (%%has-value id '$SEL context)
                       (assoc :exists option-list)
                       (assoc :forall option-list)
                       (assoc :not-in option-list))
             (%%set-value id :exists '$SEL context))
           )
          (t (warn "illegal values ~S in the one-of option of global relation ~S"
               (cdar option-list) sp-name)))
         )
        (:exists
         ;; can use one-of or simply the successor type
         (if (or (%%has-value id '$ONEOF context)
                 (assoc :one-of option-list))
             (%%set-value id :exists '$SEL context)
           (terror ":exists option requires the use of the :one-of option in ~S"
                     sp-name))
         )
        (:forall 
         (if (or (%%has-value id '$ONEOF context)
                 (assoc :one-of option-list))
             (%%set-value id :forall '$SEL context)
           (terror ":forall  option requires the use of the :one-of option in ~S"
                     sp-name))
         )
        (:not-in 
         (if (or (%%has-value id '$ONEOF context)
                 (assoc :one-of option-list))
             (%%set-value id :not-in '$SEL context)
           (terror ":not-in  option requires the use of the :one-of option in ~S"
                     sp-name ))
         )
        ) ; end case
      (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    (set (%%make-name sp-name :var) id)
    
    ;; when editing
    (save-new-id (%%make-name sp-name :var)) ; JPB1507
    
    ;; create access function unless engine is not loaded yet 
    ;(when *moss-engine-loaded*
    ;; this is for defining application properties, engine is loaded
    ;; add now a function to allow to use property name in methods
    ;; If already defined (e.g. by MOSS) do not redefine it
    (unless (fboundp sp-name)
      (%make-prop-get-accessor id sp-name)
      (%make-prop-set-accessor id sp-name)
      )
    (drformat :msp 0 "~%:---------- Exiting %make-global-sp")
    ;; return sp-id
    id))

#|
 cf z-moss-tests-def.lisp for defattribute
|#
;;;--------------------------------------------------------------- MAKE-RELATION

(defun make-relation (rel-ref &rest option-list)
  (apply #'make-sp rel-ref option-list))

;;;-------------------------------------------------------------------- %MAKE-SP
#|
The syntax for defrelation is the following one:
 (defrelation NEAR (:from town) (:to town)...)
or
 (defrelation NEAR (:from town) (:to town) (:one-of _bordeaux _paris)...)
or
 (defrelation NEAR (:from :any) (:to :any)...)
or
 (defrelation NEAR ...) ; equivalent to the last one
If the relation does not contain domain nor range, then it is an error.

Another point is: do we impose restrictions only on the range of the property or
also on the domain?
Can we write 
 (defrelation DISLIKE (:from CLYDE) (:to MOUSE)...)
where CLYDE is an instance of ELEPHANT or should we write
 (defrelation DISLIKE (:from ELEPHANT) (:to MOUSE)...)
and link CLYDE to the typical MOUSE?

A first answer is that the relation is an abstraction of the link between two objects
Hence, it cannot specify an instance as a domain unless the interpretation is 
:one-of. A better approach would be to use an option like :domain-one-of

If we write
 (defrelation DISLIKE (:from ELEPHANT) (:to :any)...)
then this allows us to state that CLYDE does not like MICKEY or the class MOUSE, 
which is not the same as linking CLYDE to the MOUSE ideal representing a typical 
mouse.

Finally we end up with several possible formats:
 - if specified within a class: domain is a class, range is class or :any meaning 
any class or any object
 - if defined directly using defrelation, then domain can be class or :any and 
range can be class or any. For generic properties domain and range are :any.

When the domain is different from :any the id of the relation is $S-<class>-<prop>
When the domain is :any, then the relation id is $S-<prop>, meaning that the relation
is not attached to anything.
|#

#|
(dformat-set :msp 0)
(dformat-reset :msp)
|#

(defUn %make-sp (sp-ref &rest option-list)
  "(%make-sp name key class suc &rest option-list) - creates a structural property, ~
   which is a link between class and suc. If class or successor does not exist, it ~
   is an error. class is contained in the :from option, suc in the :to option.
arguments:
   sp-ref: reference, e.g. symbol, string or mln
   option list:
        (:from <class-ref>)   ref of domain class
        (:to <suc-ref>)       ref to identify successor class
	(:min <number>)	      minimal cardinality
	(:max <number>)	      maximal cardinality
	(:unique)             minimal and maximal cardinality are each 1
        (:one-of <list>)      successor must be one of the objects
        (:exists)             indicates that one of the values should be in the list
        (:forall)             indicates that all successors should be in the list
        (:default <list>)     gives defaults successors
Return:
   the id of the property (generic or local)."
  (declare (special *any* *allow-forward-references* *deferred-mode* *package*
                    *language*))
  (drformat :msp 0 "~2%;---------- Entering %make-sp")
  (dformat :msp 0 "option-list: ~S" option-list)
  
  ;; check for format error in the list of options
  (unless (every #'listp option-list)
    (error "%make-sp: every option should be a list in: ~%  ~S" option-list))
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (class-ref (cadr (assoc :from option-list)))
        (suc-ref (cadr (assoc :to option-list)))
        id class-id suc-id sp-mln sp-name)
    (dformat :msp 0 "class-ref: ~S" class-ref)
    (dformat :msp 0 "suc-ref: ~S" suc-ref)
    
    ;; if we use defsp or defrelation while allowing forward references, then we 
    ;; must defer the links. The reason is that the classes may not be created yet
    ;; The consequence is that when the function will actually be executed, the
    ;; classes will exist and default values can be created using the :new syntax
    (when *allow-forward-references*
      (setq *deferred-mode* :class)
      (push `(with-environment ',*package* ',context ',*language*
               (apply #'%make-sp ',sp-ref ',option-list))
            *deferred-links*)
      (return-from %make-sp nil))
    
    ;; make sure sp-ref has the proper format (taken care of further down)
;;;    (unless (or (stringp sp-ref)(symbolp sp-ref))
;;;      (setq sp-ref (mln::make-mln sp-ref)))
    
    ;; throw to :error when something wrong (sp-ref is for error messages)
    (%check-package-version-language *package* context *language* sp-ref)
    
    ;; remove :from and :to from option-list
    (setq option-list
          (remove :from 
                  (remove :to option-list :key #'car)
                  :key #'car))
    
    ;; first take care of sp reference, e.g. "sous titre" 'SOUS-TITRE or mln
    (setq sp-mln (mln::make-mln sp-ref)) ; jpb1406
    (dformat :msp 0 "sp-mln: ~S" sp-mln)
    
    ;; make internal relation name, e.g. HAS-BROTHER
    (setq sp-name (%%make-name sp-ref :relation))
    (dformat :msp 0 "sp-name: ~S" sp-name)
    
    ;; get domain class and range class (:any or *any* returns *any*)
    (setq class-id (%%get-id class-ref :class :include-moss t)
        suc-id (%%get-id suc-ref :class :include-moss t))
    (dformat :msp 0 "class-id: ~S" class-id)
    (dformat :msp 0 "suc-id: ~S" suc-id)
    
    ;; declare an error if suc-id or class-id is null. The rationale is:
    ;; - either the function is called from a defrelation and the class
    ;;   and successor should have already been defined or be :any
    ;; - or the function is called from defconcept, and if suc-id does not
    ;;   exist, then it will be deferred by %make-concept, and called at
    ;;   deferred time.
    ;;   If suc-id does not exist at deferred time, then this is an error
    (cond
     ((null suc-id) ; should be *any* or something
      (terror "while defining relation ~A, unknown successor class ~A."
              sp-name suc-ref))
     ((null class-id) ; should be *any* or something
      (terror "while defining relation ~A, unknown owner class ~A."
              sp-name class-ref)))
    
    ;;=== check special case
    ;; if the domain is :any or UNIVERSAL-CLASS, and range is the same
    ;; then we want to define a global relation, i.e. a relation valid for any 
    ;; object. A generic relation is a global relation with no options
    ;; if the relation is a system relation we create a global relation
    (dformat :msp 0 "class-id: ~S suc-id: ~S" class-id suc-id)
    (if (and (eql class-id (intern "*ANY*"))(eql suc-id (intern "*ANY*")))
        (return-from %make-sp
          (apply #'%make-global-sp sp-mln option-list)))
    
    ;;**************************************************************************
    ;; we don't allow the use of *any* on one side only (currently)
    ;; one problem would be the position of the property wrt the generic one
    ;; if the generic prop was created with a specific class
    ;;;    (if (or (eql class-id (intern "*ANY*"))(eql suc-id (intern "*ANY*")))
    ;;;        (terror "while defining relation ~A, illegal class (~A) or suc (~A) ~
    ;;;                 argument. Can't use :any on one side only."
    ;;;              sp-name class-ref suc-ref))
    ;;; NO: generic relations when created automatically with *any* as domain 
    ;;; and range. If a specific global relation was created
    ;;**************************************************************************
    
    ;; otherwise, if domain is specified, we want to define a local relation
    
    ;;==  Check for an already existing local property (not system)
    ;; first cook up the sp key (we need class-ref), no prefix if *any*
    (setq id
          (if (eql class-id (intern "*ANY*"))
              (%%make-id :sp :name sp-mln)
            (%%make-id :sp :name sp-mln :prefix class-ref)))
    (dformat :msp 0 "id: ~S" id)
    ;; check if id already exists defining a different property
    ;; it is allowed to exist but only as a structural property
    (when (boundp id)
      (terror "when defining relation ~S in package ~A,~& ~
               identifier ~A already exists." 
              sp-ref (package-name *package*) id)) 
    
    ;; when editing
    (save-new-id id)
    
    (apply #'%%make-sp-from-ids sp-mln sp-name id class-id suc-id option-list)
    ))

#|
 cf z-moss-tests-def.lisp for defattribute
|#
;;;-------------------------------------------------------- %%MAKE-SP-DEFAULT-ID
;;; a default value is an instance of some object. Specifying a default value
;;; uses several format:
;;; - a symbol id or id pair
;;; - a string (entry point)
;;; - a variable with value an id or id pair
;;; - a list starting with :new to create a new object
;;; - a MOSS query
#|
(dformat-set :msp 0)
(dformat-reset :msp)
|#

(defUn %%make-sp-default-id (sp-id class-id suc-id sp-name &rest option-list)
  "called on process default option. If class-id is nil or *any* we ~
   do not check the default objects class and attach them to the ~
   property. Otherwise, default objects must be instances of the ~
   class and are attached to the ideal of class-id.
   Error if some or the defaults are not PDM objects or not instances ~
   of the specified class.
Arguments:
   sp-id: relation, e.g. $S-PERSON-EMPLOYER
   class-id: class with default having the relation sp-id, e.g. $E-PERSON
   suc-id: class of the defaults, e.g. $E-ORGANIZATION
   sp-name: name of the relation, e.g. HAS-PERSON-EMPLOYER
   option-list: rest of the options (contains the defaults)
Return:
   sp list with the defaults or ideal list with the defaults or nil."
  ;; each default spec must evaluate to a PDM object, e.g. _jpb. Internal ids
  ;; (e.g. $E-PERSON.32) cannot be used directly or should be quoted
  ;;*** there is a risk of system error while evaluating defaults
  (drformat :msp 0 "~2%;----- Entering %%make-sp-default-id")
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (any-symbol (intern "*ANY*"))
        (value-list (getv :default option-list))
        defaults)
    
    (setq defaults
          (%resolve-successors-for-default value-list class-id sp-id context))
    ;; all defaults should be PDM objects
    (dformat :msp 0 "defaults: ~S" defaults)
    
    (unless defaults 
      (warn "could not determine specified defaults ~% ~
             ~S ~% Might be unbound in package ~S and context ~S ~
             ~%while defining relation ~S. We skip adding defaults..."
            (getv :default option-list) (package-name *package*) context sp-name)
      (return-from %%make-sp-default-id nil))
    
    (cond
     
     ;;== domain and range are not specified, then attach defaults to relation
     ((and (or (null class-id)(equal class-id any-symbol)(eql class-id :any))
           (or (eql suc-id any-symbol)(equal suc-id :any)))
      (dformat :msp 0 "1. domain and range are both *any*")
      (%%set-value-list sp-id defaults '$DEFS context)
      (symbol-value sp-id))
     
     ;;== domain is not specified, range is specified, check sUC class
     ;;   attach to relation
     ((and (or (null class-id)(eql class-id any-symbol)(eql class-id :any)))
      (dformat :msp 0 "2. domain is *any* and range is ~S" suc-id)
      ;; if not all the values are instance of the SUC class, issue a warning
      (unless (every #'(lambda(xx) (%is-instance-of? xx suc-id)) defaults)
        (warn "some of the specified defaults ~% ~
             ~A ~% are not instances of the class ~A~% ~
             while defining relation ~A. We add them anyway..."
            (getv :default option-list) suc-id sp-name))
      (%%set-value-list sp-id defaults '$DEFS context)
      (symbol-value sp-id))
          
     ;;== class is specified, but range is not, defaults are attached to
     ;; ideal of the class
     ((or (eql suc-id any-symbol)(eql suc-id :any))
      (dformat :msp 0 "3. domain is ~S and range is *any*" class-id)
      (let ((ideal-id (%%make-id :ideal :class-id class-id)))
        ;; when editing
        (save-new-id ideal-id)
        (%%set-value-list ideal-id defaults sp-id context)
        (<< ideal-id)))
     
     ;;== domain and range are specified, defaults are attached to ideal
     (t
      (dformat :msp 0 "4. domain is ~S and range is ~S. Attach to ideal." 
               class-id suc-id)
      ;; if not all values are instances of the SUC class, issue a warning
      (unless (every #'(lambda(xx) (%is-instance-of? xx suc-id)) defaults)
        (warn "some of the specified defaults ~% ~
               ~A ~% are not instances of the class ~A in package ~S context ~S ~
               ~%while defining relation ~A. We add them anyway..."
          (getv :default option-list) suc-id (package-name *package*) context sp-name))
      
      ;; defaults attached to ideals shadow defaults attached to properties. 
      (let ((ideal-id (%%make-id :ideal :class-id class-id)))
        (dformat :msp 0 "ideal: ~S" ideal-id)
        ;; when editing
        (save-new-id ideal-id)
        ;; yes, attach default to ideal
        (%add-values-rel ideal-id sp-id defaults :context context)
        ;(%%set-value-list ideal-id defaults sp-id context)
        (<< ideal-id)))
     ;;********** Here we should check the # of defaults against :min/:max options
     )))

#|
 cf z-moss-tests-def.lisp
|#
;;;--------------------------------------------------- %MAKE-SP-EXTRACT-CLASS-ID
;;; use %%get-id instead

;;;----------------------------------------------------------- %MAKE-SP-FROM-IDS

(defUn %%make-sp-from-ids (sp-mln sp-name id class-id suc-id &rest option-list)
  "called by %make-sp to do the job. No check on the arguments. 
   Should not be called directly"
  
  ;; check just in case
  (unless class-id
    (warn "missing class-id when trying to link relation ~S in package ~S context ~S"
      sp-name *package* (symbol-value (intern "*CONTEXT*")))
    (return-from %%make-sp-from-ids nil))
  
  ;; if class-id is :any, replace it with *any*  JPB1010
  (if (eql class-id :any) (setq class-id (intern "*ANY*")))
  ;; same for suc
  (if (eql suc-id :any) (setq suc-id (intern "*ANY*")))
  
  (let ((class-name (%%make-name (car (%get-value class-id '$ENAM)) :class))
        (context (symbol-value (intern "*CONTEXT*")))
        gen-id prop-list suc-list)
    
    ;; Check if we have structural properties with same prop name
    ;; works even when sp-name is unbound (not yet created)
    (setq prop-list (%extract-from-id sp-name '$PNAM '$EPS))
    ;; entry-points like HAS-POSITION are valid across packages, thus we get a list
    ;; of properties in various packages, that we must filter 
    (setq prop-list (%filter-against-package prop-list *package*))
    ;; we must also eliminate any property that is not a relation
    (setq prop-list (remove-if #'(lambda (xx) (not (%is-relation? xx))) prop-list))
    
    ;; ... then get generic one, otherwise create it
    (setq gen-id
          (or (%get-generic-property prop-list)
              (apply #'%make-generic-sp sp-mln option-list)))
    
    ;; create object (same name as generic property)
    (>> id `(($TYPE (,context $EPS))
              ($ID (,context ,id))
              ($PNAM (,context ,sp-mln))
              ))  ; inverse prop is missing yet
    
    ;; when editing
    (save-new-id id)
    
    ;; add new property-id to system-list
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$ESLS id)
    ;; declare it as a sub-property 
    (%link id '$IS-A gen-id)
    ;; build entry-points one for each synonym
    (%make-ep-from-mln sp-mln '$PNAM id :type '$EPS)
    
    ;; ...
    ;; Now we consider inverse property
    (%make-inverse-property id sp-mln)
    
    ;; link to class and to successor (in generic property if system)
    (%link class-id '$PS id)
    (%link id '$SUC suc-id)
    
    ;; process option list
    (while 
     option-list
     (case (caar option-list)
       ;; skip some options
       ((:from :to))
       (:is-a	;; superclasses must exist (this is done to catch
        ;; spelling mistakes)
        (warn ":is-a option disallowed as yet ***")
        )
       (:min
        (%%set-value id (cadar option-list)'$MINT context)
        )
       (:max
        (%%set-value id (cadar option-list)'$MAXT context)
        )
       (:unique	;; has both min and max to 1
        (%%set-value id 1 '$MINT context)
        (%%set-value id 1 '$MAXT context)
        )
       ;; Record documentation if specified
       (:doc	
        (%process-doc-option id (cdar option-list))
        ;(%%set-value-list id (cdar option-list) '$DOCT context)
        )
       (:one-of ; JPB121029
        ;; following function will try to locate instances of objects of class
        ;; suc-id. If none is found, then no link is done and we send a warning
        (setq suc-list (%resolve-successors-for-one-of (cdar option-list) suc-id))
        ;; we add result only if result is non nil
        (cond
         (suc-list
          (%%set-value-list id suc-list '$ONEOF context)
          ;; following test sets :exists as a default selection
          (unless (or (%%has-value id '$SEL context)
                      (assoc :exists option-list)
                      (assoc :forall option-list)
                      (assoc :not-in option-list))
            (%%set-value id :exists '$SEL context))
          )
         (t (warn "illegal values in the one-of option of ~S for class ~S"
              sp-name class-name)))
        )
       (:exists
        ;; can use one-of or simply the successor type
        (if (or (%%has-value id '$ONEOF context)
                (assoc :one-of option-list))
            (%%set-value id :exists '$SEL context)
          (terror ":exists option requires the use of the :one-of option in ~S ~
                    for ~S" sp-name class-name))
        )
       (:forall 
        (if (or (%%has-value id '$ONEOF context)
                (assoc :one-of option-list))
            (%%set-value id :forall '$SEL context)
          (terror ":forall  option requires the use of the :one-of option in ~S ~
                    for ~S" sp-name class-name))
        )
       (:not-in 
        (if (or (%%has-value id '$ONEOF context)
                (assoc :one-of option-list))
            (%%set-value id :not-in '$SEL context)
          (terror ":not-in  option requires the use of the :one-of option in ~S ~
                    for ~S" sp-name class-name))
        )
       (:default
           (apply #'%%make-sp-default-id id class-id suc-id sp-name option-list)
           )        
       ) ; end case
     (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    (mapc #'(lambda (xx)
              (let ((name (%%make-name
                           (%%make-name-string xx :prop :prefix class-name)
                           ;(%make-name-string-for-concept-property xx class-name)
                           :var)))
                
                (set name id)
                ;; when editing
                (save-new-id name)
                ))
      (mln::extract-all-synonyms sp-mln))
    ;; return sp-id
    id))

#|
 cf z-moss-tests-def.lisp  defrelation/%make-sp
|#

;;;=============================================================================
;;;
;;;                             CONCEPT (Class) 
;;;
;;;=============================================================================

;;; Next thing to create in the bootstrap are classes

;;;---------------------------------------------------------------- MAKE-CONCEPT

(defun make-concept (class-ref &rest options)
  (apply #'%make-concept class-ref options))

;;;--------------------------------------------------------------- %MAKE-CONCEPT
;;; resets *context* to 0!!!
;;; OK for options :id and :rd
#|
(dformat-set :mkc 1)
(deformat-reset :mkc)
|#

(defUn %make-concept (class-ref &rest option-list)
  "creates a class.
 (defconcept name &rest option-list) - creates a new class
Arguments:
   class-ref: symbol, string or multilingual name
   option-list (opt): a list of option pairs:
       (:is-a <class-id>)	   for defining inheritance
       (:att <att-description>)    specifies attribute (formerly terminal prop)
       (:rel <rel-description>)    specifies relationship (formerly structural prop)
       (:doc <documentation>)      text expressed as a multilingual list
       (:do-not-save)              if there does not save onto *moss-system*
       (:if-exists :return)        if exists, skip definition (so that we can reload)
       (:id <symbol>)              make symbol the id of the class (e.g. *none*)
       (:rdx <symbol>)             radix applying to instances"
  (dformat :mkc 1 "~2%;---------- Entering %make-concept")
  (dformat :mkc 1 "~%;--- class-ref: ~S" class-ref)
  (dformat :mkc 1 "~%;--- *package*: ~S" *package*)
  (dformat :mkc 1 "~%;--- option-list: ~S" option-list)
 
  (let ((do-not-save (assoc :do-not-save option-list))
        (id (cadr (assoc :id option-list)))
        (radix (cadr (assoc :rdx option-list)))
        (context (symbol-value (intern "*CONTEXT*")))
        class-name ctr-id class-mln control)
    
    (dformat :mkc 1 "~%;--- context: ~S" context)
    
    ;; throw to :error when something wrong class-ref used by error messages
    (%check-package-version-language *package* context *language* class-ref)
    
    ;; cook up a multilingual name.
    (setq class-mln (mln::make-mln class-ref)) ; jpb1406
    (dformat :mkc 1 "~%;--- class-mln: ~S" class-mln)
    
    ;; create class name: if class-ref is a symbol keep it in its own package 
    (setq class-name (%%make-name class-mln :class))
    (dformat :mkc 0 "~%;=== creating class with name: ~S in package ~S"
             class-name (package-name *package*))
	
    (unless class-name 
      (error "class-name should not be nil"))
	  
    ;; then cook up the class id. We can leave the :id option on the option-list
    ;; it will be ignored during the processing of options
    (setq id (or id (%%make-id :concept :name class-mln)))
    ;; e.g. id is $E-TEST-MOSS-SYSTEM
    ;; if $E-TEST-MOSS-SYSTEM is already bound we have a problem
    (when (boundp id)
      ;; if id already there, check whether we allow it 
      (if (eql :return (car (getv :if-exists option-list)))
        ;; if allowed send warning, just in case
        (progn
          (vformat "~&;*** Warning while trying to define class ~S~
                    ~%; in package ~S, ~
                    ~%; internal id ~A exists with value: ~
                    ~%; ~S ~
                    ~%; Using old value..." 
                   class-mln *package* id (symbol-value id))
          (return-from %make-concept id))
        ;; otherwise, throw to :error
        (terror  "while trying to define class:~%; ~S~%; in package ~S, ~
                  ~%; internal id (~A) exists with value:~%  ~S"
                 class-mln *package* id (symbol-value id))))
    
    ;; otherwise, proceed... declare special e.g. TEST-MOSS-SYSTEM (entry point)
    (eval-when (:compile-toplevel :load-toplevel :execute) 
      (proclaim `(special ,class-name))
      )
    ;(when (equal+ class-ref "keyword") (break "%make-concept keyword")) 
    ;; OK we create class
    (set id `(($TYPE (,context $ENT))
              ($ID (,context ,id))
              ($ENAM (,context ,class-mln))
              ($RDX (,context ,(or radix id)))))
    ;(dformat :mkc 0 "~%;--- *package*: ~S" *package*)
    (dformat :mkc 1 "~%;--- id: ~S" id)
    (dformat :mkc 1 "~%;---  (eval id): ~S" (eval id))    
    ;; when editing (otherwise does nothing)
    (save-new-id id)
    ;; add it to MOSS
    (unless do-not-save
      (%link (<< (intern "*MOSS-SYSTEM*")) '$ENLS id))
    ;; don't forget entry point
    (setq control
          (%make-ep-from-mln class-mln '$ENAM id :type '$ENT))
    (dformat :mkc 1 "~%;--- entry points: ~S" control)
    ;; create now associated counter unless option (:no-counter) is there
    
    ;; make new symbol
    (setq ctr-id (%%make-id :counter :class-id id))
    ;; build counter. Counters are created separate from classes because the content
    ;; of a class is rather static and the value of the counter changes. This was 
    ;; done to optimize storage in the early versions of MOSS (i.e. not to be obliged
    ;; to update the whole class whenever a new instance was created). We create 
    ;; counters in context 0, so that we can upgrade them easily from any context
    (set ctr-id `(($TYPE (0 $CTR))
                  ($VALT (0 ,1)))) ; requires comma before 1, otherwise replaces 1
                                   ; by some previous number
    (dformat :mkc 0 "~%;--- ctr-id: ~S: ~S" ctr-id (symbol-value ctr-id))
    
    ;; when editing (otherwise does nothing)
    (save-new-id ctr-id)
    ;; link to class
    (%link id '$CTRS ctr-id context)
    ;; we record instance at the system level
    ;(%link *moss-system* '$SVL ctr-id context)
    
    ;; we create the ideal associated with the class
    ;; e.g. ($E-TEST-MOSS-SYSTEM . 0) ((MOSS::$TYPE (0 $E-TEST-MOSS-SYSTEM)))
    (>> (%%make-id :ideal :class-id id) 
         `(($TYPE (,context ,id))))
    
    ;; when editing (otherwise does nothing)
    (save-new-id (%%make-id :ideal :class-id id))
    
    ;; process options
    (while option-list
           ;(format t "~%%make-concept: option: ~S" (caar option-list))
      (case (caar option-list)
        ;; skip some options
        ((:if-exists :id :rdx)) ; JPB1006 adding :id, JPB110828
        
        (:is-a	;; defines super classes (specified by strings, each string
         ;; being any legal synonym referring to the class
         (%make-concept-isa-option class-mln id (cdar option-list)))
        
        ((:tp :att)  ; call the slave adding context and package info
         ; will eventually be shadowed
         (apply #'%make-tp (cadar option-list) 
                (cons `(:class-ref ,class-name) (cddar option-list))))
         ;(%make-concept-tp-option class-name (cdar option-list)))
        
        ((:sp :rel)  ; call the slave (:rel <sp-ref> <options>)
         (apply #'%make-sp (cadar option-list) 
                (cons `(:from ,class-name) (cddar option-list))))
         ;(%make-concept-sp-option class-name id (cdar option-list)))
                
        ;; Record documentation if specified, :doc takes a single value JPB060228
        (:doc	
         (%process-doc-option id (cdar option-list))
         ;(%%set-value-list id (cdar option-list) '$DOCT context)
         )
        
        (:one-of
         ;; we cannot execute the following lines until the MOSS engine is loaded
         ;; thus the :one-of option cannot be used in the boot file
         (%make-concept-oneof-option id (cdar option-list)))
        
        (otherwise
         (verbose-warn "unknown option ~S while defining class ~S.~&We ignore it." 
                       (car option-list) class-name))	
        )  ; end case
      (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    (mapc #'(lambda (xx) (set (%%make-name xx :var) id)
              ;; when editing
              (save-new-id (%%make-name xx :var))
              )
      (mln::extract-all-synonyms class-mln))
    
    (drformat :mkc  1 "~%;---------- exiting make-concept")
    ;; return entity-id
    id))

#|
 cf z-moss-tests-def.lisp
|#
;;;---------------------------------------------------- %MAKE-CONCEPT-ISA-OPTION

(defUn %make-concept-isa-option (class-mln class-id isa-list)
  "handles the :is-a option in a concept definition. 
Arguments:
   class-mln: symbol designating the class
   class-id: class id
   isa-list: list of super-classes
Return:
   nothing of interest."
  (declare (special *allow-forward-references* *deferred-mode* *deferred-links*))
  (let ((context (symbol-value (intern "*CONTEXT*")))
        super-class-id)  
    ;;if deferred record it
    (when *allow-forward-references*
      (setq *deferred-mode* :class)
      (push `(with-environment ',*package* ',context ',*language*
               (apply #'%make-concept-isa-option ',class-mln ',class-id
                      ',isa-list nil))
            *deferred-links*)
      (return-from %make-concept-isa-option nil))
    
    ;; otherwise do the job
    (while isa-list
      (setq super-class-id (%%get-id (car isa-list) :class))
      
      (if super-class-id
        ;; if class exists link it
        (%link class-id '$IS-A super-class-id)
        ;; otherwise send a warning
        (warn "when defining class ~S in package ~S: ~&class ~
               ~A in the :is-a option is not (yet?) defined. Discarded."
              class-mln *package* (car isa-list)))
      (pop isa-list)
      ) ; end while
    class-id))

;;;-------------------------------------------------- %MAKE-CONCEPT-ONEOF-OPTION

;;;********** revoir

(defUn %make-concept-oneof-option (class-id obj-list)
  "handles the :one-of option in a concept definition. 
Arguments:
   class-id: class id
   obj-list: list of the possible class instances
Return:
   nothing of interest."
  (declare (special *allow-forward-references* *deferred-mode* *deferred-links*))

  (let ((context (symbol-value (intern "*CONTEXT*"))))
  ;;if deferred record it
  (when *allow-forward-references* 
    (setq *deferred-mode* :class)
    (push `(with-environment ',*package* ',context ',*language*
             (apply #'%make-concept-oneof-option ',class-id ',obj-list nil))
          *deferred-links*)
    (return-from %make-concept-oneof-option nil))
  
  (let (result)
    ;; if a list of simple lists, simplified format; if a list of alists, full 
    ;; format. Transform into full format using default OBJNAM property
    (unless (every #'alistp obj-list)
      ;; add default $OBJNAM property to class
      (%%add-value class-id '$PT '$OBJNAM context)
      (setq obj-list (mapcar #'(lambda (xx) (list (list "MOSS object name" xx))) 
                             obj-list)))
    ;; create each instance and keep them in a list
    (dolist (item obj-list)
      (catch :error 
        (push (apply #'%%make-instance-from-class-id class-id item) result)))
    ;; create the property
    (%%set-value-list class-id (reverse result) '$ONEOF context)
    result)))

#|
 cf z-moss-tests-def.lisp   defconcept
|#
;;;=============================================================================
;;;
;;;                            VIRTUAL CONCEPT 
;;;
;;;=============================================================================

;;;------------------------------------------------------- %MAKE-VIRTUAL-CONCEPT

;;; virtual concepts are not instantiable, therefore, in deferred mode, we can
;;; instantiate them last.

;;;(defUn %make-virtual-concept (class-ref &rest option-list)
;;;  "Virtual classes are created without counters since they cannot have instances. ~
;;;   Virtual classes are a view on some instance of a class. They contain a ~
;;;   set of constraints expressed as triples to filter some of the instances of ~
;;;   the class they refer to.
#|
(dformat-set :mvc 0)
(dfrmat-reset :mvc)
|#

(defun %make-virtual-concept (class-ref &rest option-list) 
  "creates a new MOSS virtual class, saving the definition on the virtual storage ~
   attribute of the system description.
Arguments:
   class-ref: symbol, string or multilingual name
   option-list (opt): a list of option pairs:
	(:is-a <class-id>)	   for defining class reference
	(:def <att-description>)   specifies definition
	(:rel <rel-description>)   specifies relationship (formerly structural prop)
	(:doc <documentation>)	   text expressed as a multilingual list
        (:if-exists <T/NIL>)       if T allows to reload the definition
Return:
   id of the virtual class."
  (drformat :mvc 0 "~2%;---------- Entering %defvirtualclass")
  (let ((context (symbol-value (intern "*CONTEXT*")))
        name id multilingual-name ref-class-id)
    
    ;;=== first check global parameters
    ;; throw to :error when something wrong
    (%check-package-version-language *package* context *language* class-ref)
    
    ;; the is-a option should be there referencing a valid class, otherwise error
    (unless (assoc :is-a option-list)
      (terror "~&;***Error while trying to define virtual class~& ~S, ~
          in package ~S and context ~S, missing class reference (:is-a option)"
              class-ref (package-name *package*) context))
    
    ;; get reference class id
    (unless
        (setq ref-class-id 
              (%%get-id (cadr (assoc :is-a option-list)) :class))
      (terror "while trying to define virtual class ~S in package ~S context ~S, ~
               bad class reference: ~S"
              class-ref (package-name *package*) context 
              (cadr (assoc :is-a option-list))))
    
    ;; the virtual class should also have a definition
    (unless (assoc :def option-list)
      (terror "while trying to define virtual class~& ~S, in package ~S context ~S, ~
               missing definition (:def option)"
              class-ref (package-name *package*) context))
    
    ;; cook up a multilingual name, remove :name from multilingual name
    (setq multilingual-name (mln::make-mln class-ref)) ; jpb1406

    ;; save the definition on the system list of virtual objects
    (add-values (<<symbol-value (intern "*MOSS-SYSTEM*")) '$VST 
                `((:vconcept ,(format nil "~S" (cons multilingual-name option-list)))))
    
    ;; create virtual class name (same as concept name)
    ;(trformat "=== *package* : ~S" *package*)
    (setq name (%%make-name multilingual-name :class))
    (vformat "=== creating virtual class with name: ~S" name)
    
    ;; then cook up the class id. We can leave the :id option on the option-list
    ;; it will be ignored during the processing of options
    (setq id (%%make-id :vclass :name multilingual-name))
    
    (when (boundp id)
      ;; if id already there, check whether we allow it 
      (if (eql :return (car (getv :if-exists option-list)))
          ;; if allowed send warning, just in case
          (progn
            (vformat "~&;***Warning while trying to define virtual class~& ~S, ~
                    internal id ~A exists with value~& ~A.~%Using old value..." 
                     multilingual-name id (symbol-value id))
            (return-from %make-virtual-concept id))
        ;; otherwise, throw to :error
        (terror "while trying to define virtual class~& ~S, ~
                 internal id ~A exists with value~% ~A."
                multilingual-name id (symbol-value id))))
    
    ;; OK we create class
    (set id  `(($TYPE (,context $VENT))
               ($ID (,context ,id))
               ($RFC (,context ,ref-class-id))
               ($ENAM (,context ,multilingual-name))
               ))
    ;; when editing
    (save-new-id id)
    
    ;; add it to MOSS
    (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$VELS id)
    
    ;; don't forget entry point ($ENT or $VENT ?)
    (%make-ep-from-mln multilingual-name '$ENAM id :type '$ENT)

    ;; process options
    (dolist (option option-list)
      (case (car option)
        
        (:is-a)	;; skip it, already processed
        
        (:def
            ;; currently simply store the definition
            (%%set-value id (cdr option) '$DEF context))
        
        ;; Record documentation if specified, :doc takes a single value JPB060228
        (:doc	
         (%process-doc-option id (cdr option))
         )
        
        (otherwise
         (verbose-warn "unknown option ~S while defining virtual class ~S.~
                        ~&We ignore it." 
                       (car option-list) name))	
        )  ; end case
      (pop option-list))
    
    ;; build external-name for temporary use only (current session)
    ;; should build a variable for every synonym???
    (set (%%make-name name :var) id)
    ;; when editing
    (save-new-id (%%make-name name :var))
    
    ;; return entity-id
    id))

#|
 cf z-moss-tests-def.lisp
|#
;;;=============================================================================
;;;
;;;                            INSTANCE METHOD 
;;;
;;;=============================================================================

;;; defmethod is used first in bootstrap for building new methods, in particular
;;; that of $ENT, otherwise we could not build any new object
;;; It needs the existence of $CTR.0 (method counter) to do so, but not 
;;; much else
;;; This version attaches methods only to existing classes and not to any
;;; other object - Anyway at this level we do not know how to create instances.
;;; Modified on 21/11 to use the concept of selector allowing to attach own
;;; methods to any object
;;; Also the way we record them onto the plist of classes has been changed

;;;------------------------------------------------------------ MAKE-INST-METHOD

(defun make-inst-method (method-ref class-ref arg-liet body &rest option-list)
  (apply #'%make-method method-ref class-ref arg-liet body option-list))

;;;---------------------------------------------------------------- %MAKE-METHOD
;;; 1. Definstmethod: method is attached to the class and applies to instances
;;;   class-ref is a symbol and can be 
;;;     1.1 the name (ep) of the class (PERSON, ENTITY)
;;;     1.2 the name of a variable containing the class id (_person),
;;;     1.3 the class id ($E-PERSON, $ENT)
#|
(dformat-set :mim 0)
(dformat-reset :mim)
|#

(defUn %make-method (name-ref class-ref arg-list body &rest option-list)
  "Used to define methods attached to an existing class. it is an error if ~
   the class is not already defined. When the method does not already exist, ~
   then a method object is first created; then the method is compiled and ~
   attached to an internal name built from the class-id, the current version, ~
   and the method-name. If the method already exists, then the code and arguments ~
   are updated, and the method is recompiled.
Arguments:
   name: name of the method to create or modify
   class ref: symbol, string or mln
   arglist: argument of the method
   body: code for the method
   option-list (rest): contains info about language (?)
Return:
   id of the method."
  (declare (special *package*))
  (drformat :mim 0 "~%2%;---------- Entering %make-method")
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    
    ;; if forward references are allowed record action onto the *deferred-methods* list
    ;; watch %make-method is a system function
    (when *allow-forward-references*
      (push `(with-environment ',*package* ',context ',*language*
               (apply #'moss::%make-method ',name-ref ',class-ref ',arg-list ',body 
                      ',option-list))
            *deferred-methods*)
      (return-from %make-method nil))
    
    (let (name fn-id fn-id-list class-id method-function-name)
      
      ;; build name from name-ref (simple normed value)
      (setq name (%%make-name name-ref :plain)) ; JPB1507
      ;; get class-id from selector
      (setq class-id (%%get-id class-ref :class))
      ;; if we have no value, complain
      (unless class-id
        (terror "while defining method ~A, selector is not a valid class reference~%:~
               ~A ~%; in context ~S and package ~S."
                name class-ref context *package*))
      
      (dformat :mim 0 "method: ~S, *package*: ~S" name *package*)
      
      ;; otherwise check if method already exists for the class
      (cond
       ((and (%pdm? name)       ; method-name is enfry point?
             ;; consider name as an ep and get all methods with this name
             (setq fn-id-list   ; usurp value for a short while
                   (%get-value name (%%make-id :inv :prop-id '$MNAM)))
             ;; keep ony methods of the current package. The rationale is that
             ;; we want to define a method in that package and do not want to
             ;; redefine a method in MOSS
             (setq fn-id-list (%filter-against-package fn-id-list *package*))
             ;; look at each method checking if it is an instance method of our
             ;; class
             (dolist (item fn-id-list)
               (if (member class-id 
                           (%get-value item (%%make-id :inv :prop-id '$IMS)))
                   ;; if so, return its id or id pair
                   (return (setq fn-id item))))
             )
        
        ;; here method already exists we simply patch it
        ;;********** we should remove it from the p-list of all classes and orphans
        (verbose-warn 
         "redefining method ~A for class ~A in package ~S and context ~S."
         name class-ref (package-name *package*) context)
        ;; when editing
        (save-old-value fn-id)
        
        ;; upgrade documentation
        (if (stringp (car body))
            (%%set-value fn-id (car body) '$DOCT context))
        )
       
       (t
        ;; otherwise method does not exist we must create it
        (setq fn-id 
              (if (eql *package* (find-package :moss))
                  ;; make MOSS methods
                  (%%make-id :instance-method 
                             :value (%get-and-increment-counter '$FN))
                ;; otherwise make application methods
                (%%make-id :instance-method
                           :value (%get-and-increment-counter (intern "$E-FN")))))
        ;; build new entity (code and args are available in function object)
        (dformat :mim 0 "fn-id: ~S" fn-id)
        (>> fn-id 
             `(($TYPE ,(if (eql *package* (find-package :moss))
                           (list context '$FN)
                         (list context (intern "$E-FN")))) ; JPB 0912
               ($ID (,context ,fn-id))
               ($MNAM (,context ,name))
               ,@(if (stringp (car body))
                     `(($DOCT (,context ,(car body)))))
               ))
        
        ;; when editing
        (save-new-id fn-id)
        ;; build entry-point if it does not exist already and export it
        ;;********** we do not want necessarily to export name do we?
        (%make-ep name '$MNAM fn-id :export t)
        ;; add new method-name to system-list
        (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$FNLS fn-id)
        ;; link to class
        (%link class-id '$IMS fn-id)))
      
      ;; next is common code to cases where method is created or simply patched
      ;; define or redefine a global symbol for holding method code
      (setq method-function-name 
            (%%make-name name :inst :class-ref class-id))
      (dformat :mim 0 "method-function-name: ~S" method-function-name)
      
      ;; when editing
      (save-new-id method-function-name)
      ;; record internal method name
      (%%set-value fn-id method-function-name '$FNAM context)
      ;; ...build and attach code to method-name and compile it
      (%build-method method-function-name arg-list body)
      
      ;; put reference onto p-list of class (***** only if methods cached???)
      (%putm class-id method-function-name name context :instance)
      
      (drformat :mim 0 "~%;---------- End %make-method")
      ;; return method-name
      fn-id
      )))

#|
 cf z-moss-tests-def.lisp
|#
;;;------------------------------------------ %MAKE-METHOD-RESOLVE-SELECTOR-LIST
;;; We have 2 cases
;;; 1. Definstmethod: method is attached to the class and applies to instances
;;;   if selector is a symbol, then it can be 
;;;     1.1 the name (ep) of the class, e.g. PERSON, ENTITY
;;;     1.2 the name of a variable containing the class id, e.g. _person
;;;     1.3 the class id, e.g. $E-PERSON, $ENT
;;; 2. Defownmethod: method is attached to object
;;;   if selector is a symbol, then it can be
;;;     2.1 the name of a variable containing the object id
;;;     2.2 the id of the object, e.g. PERSON, $E-PERSON.12, ($E-PERSON . 5)
;;; The problem is the ambiguity between case 1.1 and case 2.2 when selector is
;;; an entry. In case 1.1 we must return the class id, in case 2.2 we return the 
;;; symbol.
;;; To distinguish between the cases, we introduce the method-type argument
#|
(dformat-set :mrsl 0)
(dformat-reset :mrsl)
|#

(defUn %make-method-resolve-selector-list (selector method-name)                                                      
  "takes a selector list and tries to return a MOSS object id. Input may be:
        (<entry ref> <attribute ref> <concept ref> {:class-ref <class ref>}
                     {:select <fcn>)}
where ref indicates mln, string, symbol.
   When there is an ambiguity, tries first a :select function. If none, filters ~
   against the current package, if it does not work, filter against the MOSS ~
   package.
Arguments:
   selector: as described
   method-name: method we are defining
Return:
   a MOSS object-id or a list possibly empty."
  (drformat :mrsl 0 "~2%;---------- Entering %make-method-resolve-selector-list")
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (entry-ref (car selector))
        (att-ref (cadr selector))
        (class-ref (caddr selector))
        (options (cdddr selector)) 
        entry att-id class-id obj-id-list fn
        ref-class-ref ref-class-id result method-list object-list method-id)
    
    ;; first get the class id from the ref (returns a list)
    ;;********** how about virtual classes ??? Can we attach methods to them ??
    (setq class-id (%%get-id class-ref :class :include-moss t))
    (dformat :mrsl 0 "class-id: ~S" class-id)
    ;; if nil, bad class-ref
    (unless class-id
      (terror "while trying to create method ~A, the selector~%;   ~S~%; does not ~
               reference a valid class in context ~S and package ~S." 
              method-name selector context (package-name *package*)))
    
    ;; get attribute corresponding to the specific class
    (setq att-id 
          (%%get-id att-ref :tp :class-ref class-ref :include-moss t))
    (dformat :mrsl 0 "att-id: ~S" att-id)
    ;; if nil, bad attribute ref (error from %get-tp... before getting here)
    (unless att-id 
      (terror "while trying to create method ~A, the selector~%;   ~S~%; does not ~
               reference a valid attribute." method-name selector))
    
    ;; now we must find the =make-entry  method associated with the attribute
    ;; get all the own-methods asociated with the attribute
    (setq method-list (%get-value att-id '$OMS))
    (dformat :mrsl 0 "method-list:~%  ~S" method-list)
    ;; get the list of all the =make-entry methods since entry-ref is an entry
    (setq object-list 
          (if (%pdm? '=make-entry)
              (%get-value '=make-entry (%%make-id :inv :prop-id '$MNAM))))
    (dformat :mrsl 0 "object-list: ~S" object-list)
    ;; is there a method common to both lists?
    (setq method-id (car (intersection method-list object-list)))
    (dformat :mrsl 0 "method-id: ~S" method-id)
    
    ;; if OK get it, if does not exist, then complain
    (if method-id
        (setq fn (car (%get-value method-id '$FNAM)))
      (terror "while trying to create method ~A, the attribute in the selector ~%~
               ;   ~S ~%; has no =make-entry method in package ~S context ~S." 
              method-name selector (package-name *package*) context))
    
    ;; OK, we got a =make-entry method, apply fn to entry-ref
    (setq entry (car (funcall fn entry-ref)))
    (dformat :mrsl 0 "entry: ~S" entry)
    
    ;; then, get object id(s)
    (setq obj-id-list (%extract-from-id entry att-id class-id))
    (dformat :mrsl 0 "obj-id-list: ~S" obj-id-list)
    
    ;;=== if more than one object, then
    ;; - if objects are properties try the :class-ref option
    ;; - otherwise try the :select option
    (cond
     ((null obj-id-list)
      (terror "while trying to create method ~A, with selector~%;   ~S~%~
               ; no object can be found in package ~S context ~S." 
              method-name selector (package-name *package*) context ))
     
     ;;== objects are properties (test first one)
     ((and (cdr obj-id-list)
           (or (%is-attribute? (car obj-id-list))
               (%is-relation? (car obj-id-list))))
      ;;********** inverse properties ?
      ;; get class-ref
      (setq ref-class-ref (cadr (member :class-ref options)))
      
      (if ref-class-ref
          (setq ref-class-id (%%get-id ref-class-ref :class :include-moss t)))
      (dformat :mrsl 0 "ref-class-ref: ~S" ref-class-ref)
      (unless ref-class-id
        (terror "while trying to create method ~A, the :class-ref option in the ~
                 selector ~%;   ~S~%; is missing for disambiguation of~%;   ~S~&~
                 ; or argument is improper, in package ~S context ~S." 
                method-name selector obj-id-list (package-name *package*) context))
      
      ;; otherwise try to get the right property for the class
      (setq result (%determine-property-id-for-class obj-id-list ref-class-id))
      )
     
     ;;== objects are not properties, we got more than one
     ((cdr obj-id-list)
      ;; try to get discriminating function
      (setq fn (cadr (member :select options)))
      (dformat :mrsl 0 "fn: ~S" fn)
      (cond
       (fn
        (setq result (funcall fn obj-id-list)))
       ;; no discriminating function
       (t
        (prog (new-list) 
          ;; try to restrict objects to current package JPB 1001
          (setq new-list 
                (remove nil 
                        (mapcar #'(lambda(xx) 
                                    (if (and (symbolp xx)
                                             (eql (symbol-package xx) *package*))
                                        xx))
                          obj-id-list)))
          (dformat :mrsl 0 "new-list: ~S" new-list)
          ;; when result is exactly one arg, we win
          (if (and new-list (null (cdr new-list)))
              (return (setq result new-list)))
          
          ;; did not work try MOSS package JPB 1001
          (setq new-list (copy-tree obj-id-list)) ; careful mapcan is destructive
          (setq new-list
                (remove nil
                        (mapcar #'(lambda(xx) 
                                    (if (and (symbolp xx)
                                             (eql (symbol-package xx) 
                                                  (find-package :moss)))
                                        xx))
                          new-list)))
          ;; when result is exactly one arg, we win
          (if (and new-list (null (cdr new-list)))
              (return (setq result new-list)))
          
          ;; otherwise we definitely loose
          (terror "while trying to create method ~A, the :select option in the ~
                   selector~%;    ~S~%; is missing for disambiguation of~%;    ~S" 
                  method-name selector obj-id-list)
          (setq result (funcall fn obj-id-list))
          ))))
     
     ;;== here we have a single object
     ((setq result (car obj-id-list))
      ))
    
    (dformat :mrsl 0 "result 2: ~S" result)
    ;; if still more than one object, then error
    (cond
     ((and (listp result)(cdr result))
      (terror "while trying to create method ~A, with selector~%;   ~S, ~
               ~%; more than one object selected:~%;   ~S." 
              method-name selector obj-id-list))
     ((listp result) (car result))
     ((symbolp result) result)
     ((terror "while trying to create method ~A, the selector~%;   ~S~%; does not ~
               reference a valid object identifier:~%;   ~S"
              method-name selector result)))
    ))

#|
 cf z-moss-tests-def.lisp
|#
;;;=============================================================================
;;;
;;;                            OWN METHOD 
;;;
;;;=============================================================================

;;;------------------------------------------------------------- MAKE-OWN-METHOD

(defun make-own-method (method-ref selector arg-list body &rest option-list)
  (apply #'%make-ownmethod method-ref selector arg-list body option-list))

;;;------------------------------------------------------------- %MAKE-OWNMETHOD
;;; if method exists and is modified we should store the old value of the 
;;; function, otherwise the function is a newly created object

(defUn %make-ownmethod (ref selector arg-list body &rest option-list)
  "Defines an own-method, i.e. a method specific to an object and directly ~
   attached to the object. It is an error if the object is not already defined. ~
   When selector is a symbol then it is considered an object-id.
    Otherwise syntax is:
 	(entry tp-name class {:select function}{:class-ref <class-ref>} )
    where function is used when more than one object is reached via the ~
   access path entry-tp-name-class, and class-ref to select properties.
    When the method does not already exist, ~
   then a method object is first created; then the method is compiled and ~
   attached to an internal name build from the class-id, the current version, ~
   and the method-name. If the method already exists, then the code and arguments ~
   are updated, and the method is recompiled.
Arguments:
   ref: method-name (symbol, string, :default-make-entry)
   selector: if symbol: object-id or variable, or list
   arg-list: typical lisp arg-list
   body: list: body of the function
   option-list (rest): possible :export option
Return:
   id of the method."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        value)
    
    ;; if forward references are allowed record action onto the *deferred-methods* list
    (when *allow-forward-references*
      (if (eql *deferred-mode* :class) 
          (push `(with-environment ',*package* ',context ',*language*
                   (apply #'%make-ownmethod ',ref ',selector ',arg-list ',body 
                          ',option-list)) 
                *deferred-methods*)
        (push `(with-environment ',*package* ',context ',*language*
                 (apply #'%make-ownmethod ',ref ',selector ',arg-list ',body 
                        ',option-list)) 
              *deferred-instance-methods*))
      (return-from %make-ownmethod nil))
    
    (let ((name (if (eql ref :default-make-entry) 
                    '=make-entry 
                  (%%make-name ref :plain))) ; JPB 1701
          (export (car (getv :export option-list)))
          fn-id obj-id method-function-name)
      
      ;(print `(selector ,selector))
      
      ;;===== resolve selector to point to an object id
      ;; when selector is a symbol, it must be an object identifier or the
      ;; name of a variable whose value is an object identifier
      ;; if it is a symbol, then it can refer to a class or a method, if it is a 
      ;; dotted pair, e.g. ($e-person . 22), then it should refer to an instance 
      (cond
       ((symbolp selector)
        (cond
         ;; if selector is an object id, attach method to the object
         ;; %pdm? load object if needed
         ((%pdm? selector)
          (setq obj-id selector))
         ;; if the name of a symbol (a variable?) containing the id of an object,
         ;; use the value of the symbol
         ((and (boundp selector)
               (%pdm? (symbol-value selector)))
          ;; value could be an id pair
          (setq obj-id (symbol-value selector)))
         ;; otherwise, we don't know what selector is
         ((terror "non existing object ~S, while attempting ~
                 to create method ~S" selector name)))
        ;+++ issue warning for own methods attached to objects directly
        (if *warning-for-own-methods*
            (verbose-warn "attaching method ~S, to object ~S" name selector))
        )
      
      ;;=== is selector an id pair?
      ((%%is-id? selector :pair)
        (cond
         ((%pdm? selector)
          (>> obj-id selector))
         ;; otherwise, we can't find the object
         ((terror "non existing object ~S, while attempting ~
                 to create method ~S" selector name)))
        ;+++ issue warning for own methods attached to objects directly
        (if *warning-for-own-methods*
            (verbose-warn "attaching method ~S, to object ~S" name selector))
        )
      
      ;;=== otherwise selector has a more complex syntax
      ;; e.g. (ALBERT HAS-NAME PERSON :select $$agemax)
      ;; or   (HAS-NAME HAS-PROPERTY-NAME TERMINAL-PROPERTY :class-ref PERSON)
      ;; we use class-id for designating the object to cope with property trees
      ((listp selector)
       (setq obj-id (%make-method-resolve-selector-list selector name)))
      
      ((terror "Selector ~S should be an id, a variable or a selector list, ~
                while attempting to create method ~S"
               selector name))
      )
      
      ;;===== here obj-id has been determined
      ;;=== If method exists for obj-id we will redefine it!
      (cond
       ((and (%pdm? name)	; entry-point?
             (setq fn-id	; usurp value for a short while
                   (car
                    (intersection
                     ;; list of all methods with same name
                     (%get-value name (%%make-id :inv :prop-id '$MNAM))
                     ;; list of all methods belonging to obj-id
                     (%get-value obj-id '$OMS)))))
        ;; here method is redefined for the object
        (verbose-warn "redefining method ~A for object ~A" name 
                      (if (listp selector) (car selector) selector))
        ;; when editing
        (save-old-value fn-id)
        
        (if (stringp (car body))
            (%%set-value fn-id (car body) '$DOCT context))
        )
       
       ;;--- otherwise create ney method
       (t
        ;; export name
        (eval-when (compile load eval) 
          (when export
            (proclaim `(special ,name))
            (export (list name))))
        ;(trformat "%make-own-method /exported name: ~S" name)
        ;; make new symbol JPB 0912
        (setq value (if (eql *package* (find-package :moss))
                        (%get-and-increment-counter '$FN)
                      (%get-and-increment-counter (intern "$E-FN"))))
        (setq fn-id (%%make-id :own-method :value value))
        
        ;; build new entity (code and args are available in function object)
        ;(print `(+++ ,*package* ,fn-id))
        ;; build new entity
        (>> fn-id 
            `(($TYPE ,(if (eql *package* (find-package :moss))
                          (list context '$FN)
                        (list context (intern "$E-FN" *package*)))) ; JPB 0912
              ($ID (,context ,fn-id))
              ($MNAM (,context ,name))
              ,@(if (stringp (car body))
                    `(($DOCT (,context ,(car body)))))
              ))
        
        ;; when editing
        (save-new-id fn-id)
        
        ;; build entry-point if it does not exist already
        (%make-ep name '$MNAM fn-id :export export)
        ;; add new method-name to system-list
        (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$FNLS fn-id)
        ;; link to class
        (%link obj-id '$OMS fn-id)
        ))
      
      ;;===== define a global symbol for holding method code
      (cond
       ((eql ref :default-make-entry)    
            (setq method-function-name 'default-make-entry)
            ;; save as an old value to prevent saving as a new value that will be erased
            ;; in case of abort
            (save-old-value 'default-make-entry)
            (%build-method method-function-name '(value-list) 
                           '("default make-entry fonction"
                             (%make-entry-symbols value-list))))
        (t
          (setq method-function-name 
                (%%make-name name :own :class-ref obj-id))
          ;; ...build and attach code to method-name and compile it
          (%build-method method-function-name arg-list body)
          ))
      
      ;; record internal method name
      (%%set-value fn-id method-function-name '$FNAM context)
      
      ;; put reference onto p-list of object
      (%putm obj-id method-function-name name context :own)
      
      ;; return method-name
      fn-id)))

#|
 cf z-moss-tests-def.lisp
|#
;;;=============================================================================
;;;
;;;                            UNIVERSAL METHOD 
;;;
;;;=============================================================================

;;; Defining first defuniversal function
;;; It needs the existence of $UNI.CTR (method counter) to do so, but not much 
;;; else (defined below as defsysvar, and so is recorded???)
;;; BUG. does not accept ref as names, e.g. string or MLN

;;;------------------------------------------------------ MAKE-UNIVERSAL-METHOD

(defun make-universal-method (method-ref arg-list body &rest option-list)
  (apply #'%make-universal method-ref arg-list body option-list))

;;;------------------------------------------------------------- %MAKE-UNIVERSAL
#|
(dformat-set :mum 0)
(dformat-reset :mum)
|#

(defUn %make-universal (ref arg-list body &rest option-list) 
  (drformat :mum 0 "~2%;---------- Entering %make-universal")
  
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    
    ;; if forward references are allowed record action onto the *deferred-methods* list
    ;; should not we keep the instantiation for the end?
    (when *allow-forward-references*
      (push `(with-environment ',*package* ',context ',*language*
               (apply #'%make-universal ',ref ',arg-list ',body ',option-list))
            *deferred-methods*)
      (return-from %make-universal nil))
    
    (let ((export (car (getv :export option-list)))
          (uni-id (moss::%%get-id "moss universal method" :concept))
          (name (%%make-name ref :plain)) ; JPB 1701
          fn-id method-id doc)
      
      ;; try to get the universal method if it exists
      (setq fn-id (car (%extract-from-id  name '$UNAM uni-id)))
      (dformat :mum 0 "fn-id: ~S" fn-id)
      ;; check if universal method already exists
      (cond
       (fn-id
        ;;== when universal method already exists...
        (verbose-warn "~&redefining universal method ~A: " name)
        
        ;; we must save method and associated function
        ;; when editing
        (save-old-value fn-id)
        ;; must update doc, otherwise the rest remains unchanged
        (if (stringp (car body))
            (%%set-value fn-id (car body) '$DOCT context))
        ;; get the method-id, actually the internal method function name
        (setq method-id (car (%%get-value fn-id '$FNAM context)))
        (dformat :mum 0 "method-id: ~S" method-id)
        ;; when editing
        (save-old-value method-id)
        )
       
       ;;== otherwise create the method
       (t
        (eval-when (compile load eval) 
          (when export
            (proclaim `(special ,name))
            (export (list name)))
          )
        (dformat :mum 0 "exported name: ~S" name)
        
        ;; make new symbol
        (setq fn-id 
              (%%make-id :universal-method 
                         :value (%get-and-increment-counter uni-id)))
        (dformat :mum 0 "fn-id: ~S" fn-id)
        ;; look for documentation string
        (setq doc (if (stringp (car body)) (car body)))
        ;; build new entity (method object)
        (>> fn-id (copy-list
                    `(($TYPE (,context ,uni-id))
                      ($ID (,context ,fn-id))
                      ($UNAM (,context ,name))
                      ,@(if doc `(($DOCT (,context ,doc))))
                      )))
        (dformat :mum 0 "~S: ~S" fn-id (<< fn-id))
        ;; when editing
        (save-new-id fn-id)
        ;; add new method-id to system-list
        (%link (symbol-value (intern "*MOSS-SYSTEM*")) '$FNLS fn-id)
        (%make-ep name '$UNAM fn-id :export export)
        
        ;; cook up method name
        (setq method-id (%%make-name name :uni))
        (dformat :mum 0 "method-id: ~S" method-id)
        ;; when editing
        ;(save-new-id method-id)
        ;; record internal method name
        (%%set-value fn-id method-id '$FNAM context)
        (dformat :mum 0 "~S: ~S" fn-id (<< fn-id))
        )
       )
      
      ;;=== now build or rebuild the function
      ;;********** watch %build-method saves method-id (useful when redefining?)
      (%build-method method-id arg-list body)
      
      ;; put reference onto p-list of name!
      (%putm name method-id name context :universal)
      (drformat :mum 0 "~%;---------- End %make-universal")
      ;; return method-name
      fn-id)))

#|
cf z-moss-test-def.lisp for tests
|#
;;;=============================================================================
;;;
;;;                            INDIVIDUAL (Instance) 
;;;
;;;=============================================================================

;;; This is a simple default method to create instances of kernel objects
;;; We have two problems when making instances
;;;   - the corresponding class and properties must have been defined
;;;   - other instances that we want to link must have been created
;;; To resolve the issue, we must
;;;   - defer creating instances after all concepts and properties have been
;;;     created
;;;   - assign unique variables to the instances so that we can find them later
;;;     through the :name or :var option. We must defer linking the instances.
;;; Note. SOL uses thhe :name option as an MLN

;;;-------------------------------------------------------------- %MAKE-INSTANCE
;;;
;;; Ex. (%make-instance "person" ("name" "john")("age" 32))
;;; Multiple class belonging
;;;     (%make-instance '("student" "teacher") ...)
#|
(dformat-set :mki 0)
(dformat-reset :mki)
|#
(defUn %make-individual (&rest ll) (apply #'%make-instance ll))
(defUn make-individual (&rest ll) (apply #'%make-instance ll))

(defUn %make-instance (class-ref &rest option-list)
  "checks whether name is the name of an actual class and if so calls ~
   %%make-instance-from-class-id. Otherwise, sends a warning and throws ~
   an :error label.
   Note that it is used to create orphans, instances of the MOSS-NULL-CLASS.
Arguments:
   class-ref: symbol, string or multilingual name of the class 
              of the object being created, or a list of them
   option-list (opt): a list of properties for the instance
     - (:no-warning t/nil)
     - <property>
     - (:doc <doc MLN>)
     - (:var <var-name>)
     - (:is-a <obj-id>)
     - (:name <unique MLN>)  used by SOL to identify objects
Return:
   the id of the created instance."
  (drformat :mki 0 "~2%;---------- Entering %make-instance")
  (let ((context (symbol-value (intern "*CONTEXT*"))))
    ;; we do not check context since it corresponds to the current context, and thus
    ;; should be legal. However, some if the invoked functions do it.
    
    ;; When we are loading a file allowing forward references, we must postpone all
    ;; instance creations until the deferred list has been executed. Thus, we simply
    ;; add the instance creation to the list of deferred instance creation
    (when *allow-forward-references*
      ;; indicate that we are in the instance part of the file
      (setq *deferred-mode* :instance)
      ;; if there is a :var option we should declare the variable to be global to
      ;; avoid compiler messages (undeclared free variable...)
      (if (assoc :var option-list)
          (proclaim `(special ,(cadr (assoc :var option-list))))) ; JPB0702
      ;; then, create entry in the *deferred-instances* list
      (push `(with-environment ',*package* ',context ',*language*
               (apply #'%make-instance ',class-ref ',option-list))
            *deferred-instances*)
      (return-from %make-instance :made-deferred-instance))
    
    (let (class-id id)
      
      ;;=== if we have a list, then we have a multiple class belonging
      (when (and (listp class-ref)(not (mln::mln? class-ref)))
        ;; call a function to deal with an instance of multiple class
        (setq id (%make-multiple-instance class-ref option-list))
        (return-from %make-instance id))
      
      ;; OK here we have a single class-ref
      ;; check if the class exists (does not include MOSS classes)
      ;(format t "~%; %make-instance /*package*: ~S" *package*)
      (setq class-id (%%get-id class-ref :class))
      (dformat :mki 0 "class-id: ~S" class-id)
      
      (unless class-id
        (mthrow " while trying to define instance of class ~A, ~
                 class does not exist in package ~S and context ~S." 
                class-ref (package-name *package*) context))
      
      ;; if the class contains the property $ONEOF, then we are not allowed to
      ;; create new instances, send a warning but do it nevertheless
      ;; (it is interesting to note that we cannot specify the values of the ONE-OF
      ;; option while defining the class, because we cannot create instances
      ;; before the class is actually created... However, we can add them later.)
      ;;********** $ONEOF is not a class option!
;;;      (if (%get-value class-id '$ONEOF)
;;;          (warn "trying to create a new instance for the class ~A violating its ~
;;;                 ONE-OF restriction, in package ~S and context ~S. We do it anyway."
;;;            class-ref (package-name *package*) context))
      
      ;; now call the function that does the job
      (apply #'%%make-instance-from-class-id class-id option-list))))

#|
 cf z-moss-tests-def.lisp
|#
;;;------------------------------------------------ %%MAKE-INSTANCE-ADD-PROPERTY
;;; used by %make-multiple-instance
#|
(dformat-set :miap 0)
(dformat-reset :miap)
|#

(defun %%make-instance-add-property (key class-ref prop-ref value-list no-warning)
  "adds a property to an object with associated values.
Arguments:
   key: id of the object to be modified
   class-ref: ref of the corresponding class
   prop-ref: reference of property
   value-list: list of values associated with property
   no-warning: a flag to prevent printing warnings
Return:
   the content of the object"
  (declare (special *language*))
  (drformat :miap 0 "~2%;---------- Entering %%make-instance-add-property")
  
  (let* ((class-id (%%get-id class-ref :class))
         (context (symbol-value (intern "*CONTEXT*")))
         (name (car (%%get-value class-id '$ENAM context)))
         (prop-list (%get-properties key))
         tp-id sp-id) 
    (dformat :miap 0 "class-id: ~S" class-id)
    (dformat :miap 0 "*package*: ~S" *package*)
    (dformat :miap 0 "context: ~S" context)
    (dformat :miap 0 "name: ~S" name)
    (dformat :miap 0 "prop-list: ~S" prop-list)
    
    (case prop-ref
      ;; ignore control options
      (:no-warning)
      
      (:name  ; JPB060228
       ;; if name is a simple string add current language tag
       (%make-instance-name-option key name 
                                   (if (cdr value-list)
                                       value-list
                                     (cons *language* value-list))))
      
      ;; check for external variable name option (should be unique)
      (:var
       (if (%is-variable-name? (car value-list))
           (set (car value-list) key)
         (warn "~S is not a valid variable name. We ignore it." (car value-list))))
      ;; :name option is used by SOL to give an "object name" to the instance
      ;; may not be unique... MOSS should use :var option
      ;; e.g. (:name "aquitaine") or (:name :en "London" :fr "Londres")
      
      ;; doc? JPB060228
      (:doc	
       (%process-doc-option key value-list)
       )
      
      ;; must be a property
      (otherwise
       (cond
        ;; is it an attribute attached to the class? Use current package
        ((setq tp-id (%%get-id prop-ref :tp :class-ref class-id :include-moss t))
         (%make-instance-tp-option name key prop-ref tp-id value-list prop-list))
        
        ;; is it a generic attribute?
        ((%is-attribute?
          (setq tp-id (%%get-id prop-ref :tp :include-moss t)))
         (unless no-warning
           (warn "property ~S is not an attribute of class ~S. We add it anyway."
             prop-ref (mln::get-canonical-name name)))
         ;; we trick the following function by making the class-list contain the
         ;; generic property
         (%make-instance-tp-option name key prop-ref tp-id value-list 
                                   (list tp-id)))
        
        ;; structural property ?
        ((setq sp-id (%%get-id prop-ref :sp :class-ref name :include-moss t))
         
         ;(format t "~&; %%make-instance-from-class-id/ class: ~S sp: ~S deferred: ~S"
         ;        name prop-name *allow-forward-instance-references*)
         ;; Here, if we are allowing forward references, we defer creation
         (if *allow-forward-instance-references*
             (push
              `(with-environment ',*package* ',context ',*language*
                 (funcall #'%make-instance-sp-option ',key ',class-id ',sp-id
                          ',value-list))
              *deferred-instance-links*)
           ;; otherwise make link
           (%make-instance-sp-option key class-id sp-id value-list )))
        
        ;; here we did not recognize a property
        (t
         (warn 
             "while trying to define instance of class ~S in package ~S context ~S, ~
               property ~S does not exist. We ignore it." 
           (mln::get-canonical-name name) *package* context prop-ref))
        )))
    
    (drformat :miap 0 "~%;---------- End %%make-instance-add-property")    
    ;; return the content of the modified object (debugging purposes)
    (<< key)))

;;;----------------------------------------------- %%MAKE-INSTANCE-FROM-CLASS-ID
;;; make-instance should check for global object constraints
;;; Currently constraints at the class level is
;;;   :one-of 
;;; meaning that the object must be one among the list
;;; The goal is not to restrict the user to create new instances of the class
;;; but to warn that this could violate a restriction.
;;; Therefore the :one-of constraint should be considered as a usual or typical 
;;; constraint not as a strong constraint, like cardinality constraints.

;;; Missing the :is-a property, should do something like for concepts
#|
(dformat-set :mki 0)
(dformat-reset :mki)
|#

(defUn %%make-instance-from-class-id (class-id &rest option-list)
  "assume that class-id has been checked and coresponds to an actual class.
   Does not create instances of system objects.
Arguments:
   class-id: identifier of class for which we want an instance
   option-list: list of property/values for the class
Return:
   id of instance"
  (dformat :mki 0 "~2%;---------- ~A: Entering %%make-instance-from-class-id" 
           (package-name *package*))
  
  (let* ((context (symbol-value (intern "*CONTEXT*")))
         (name (car (%%get-value class-id '$ENAM context)))
         (no-warning (assoc :no-warning option-list))
         key prop-list option-tag tp-id sp-id value-list)
    
    (dformat :mki 0 "class-id: ~S" class-id)
    (dformat :mki 0 "option-list ~S" option-list)
    (dformat :mki 0 "*package*: ~S" *package*)
    (dformat :mki 0 "*context*: ~S" *context*)
    (dformat :mki 0 "context: ~S" context)
    (dformat :mki 0 "name: ~S" name)
    
    ;; when the name of the class is an MLN should recover the right string
    ;; we use get-canonical-name to avoid the *language* = :ALL situation
    (if (mln::mln? name)
        (setq name (mln::get-canonical-name name))) ; jpb 1411
    ;; question: could language be part of the option-list and if so what is the
    ;; associated semantics?
    (dformat :mki 0 "*language*: ~S" *language*)
    (dformat :mki 0 "name: ~S" name)
    
    ;; Then we ask for the creation of an object (adding :id JPB 1001)
    ;; key may be a symbol or a dotted pair (if not in MOSS package)
    (setq key (%create-basic-new-object class-id context))
    (dformat :mki 0 "key: ~S" key)
    
    ;; get list of all possible properties from classes and superclasses
    (setq prop-list (%get-properties key))
    (dformat :mki 0 "prop-list: ~S" prop-list)
    
    ;;======== then we check options (tp and sp)
    (dformat :mki 0 "loop on the option list: ~S" option-list)
    (dolist (option option-list)
      (setq option-tag (car option) 
          value-list (cdr option)
          sp-id nil 
          tp-id nil
          )
      (dformat :mki 0 "option-tag: ~S in package: ~S" option-tag *package*)
      (catch 
       :start-of-loop
       (case option-tag
         ;;== ignore control options
         (:no-warning)
         
         ;;== doc? JPB060228
         (:doc	
          (%process-doc-option key value-list)
          )
         
         ;;== :is-a is somehow similar to a relation
         (:is-a
          ;; Here, if we are allowing forward references, we defer creation
          (if *allow-forward-instance-references*
              (push
               `(with-environment ',*package* ',context ',*language*
                  (funcall #'%make-instance-isa-option ',key ',value-list))
               *deferred-instance-links*)
            ;; otherwise make link
            (%make-instance-isa-option key value-list))
          )
         
         ;;== :var check for external variable name option (should be unique)
         (:var
          (if (%is-variable-name? (cadr option))
              (set (cadr option) key)
            (warn "~S is not a valid variable name. We ignore it." (cadr option))))
         
         ;;== :name option is used by SOL to give an "object name" to the instance
         ;; may not be unique... MOSS should use :var option
         ;; e.g. (:name "aquitaine") or (:name :en "London" :fr "Londres")
         (:name  ; JPB060228
          ;; if name is a simple string add current language tag
          (%make-instance-name-option key name 
                                      (if (cdr value-list)
                                          value-list
                                        (cons *language* value-list))))
         ;;== must be a property
         (otherwise
          (dformat :mki 0 "class-ref: ~S" name)
          (dformat :mki 0 "tp-id: ~S" 
                   (%%get-id option-tag :tp :class-ref name :include-moss t))
          (cond
           ;;== attribute attached to the class? Use current package
           ((setq tp-id (%%get-id option-tag :tp :class-ref name :include-moss t))
            (%make-instance-tp-option name key option-tag tp-id value-list prop-list)
            (dformat :mki 0 " *context*: ~S" (symbol-value (intern "*CONTEXT*")))
            )
           ((prog1 nil 
              (dformat :mki 0 "tp-id (2): ~S"
                       (%%get-id option-tag :tp :include-moss t)))
            )
           ;;== foreign or generic attribute ?
           ((%is-attribute?
             (setq tp-id (%%get-id option-tag :tp :include-moss t)))
            (unless no-warning
              (warn "property ~S is not an attribute of class ~S. We add it anyway."
                option-tag (mln::extract-to-string name :canonical t)))
            ;; we trick the following function by making the class-list contain the
            ;; generic attribute
            (%make-instance-tp-option name key option-tag tp-id value-list 
                                      (list tp-id))
            ;(dformat :mki 0 "2 *context*: ~S" (symbol-value (intern "*CONTEXT*")))
            )
           
           ;;== structural property ?
           ((prog1 nil
              (dformat :mki 0 "sp-id: ~S"
                       (%%get-id option-tag :sp :class-ref name :include-moss t)))
            )
           ((setq sp-id (%%get-id option-tag :sp :class-ref name :include-moss t))
            (dformat :mki 0 "~&; %%make-instance-from-class-id/ class: ~S sp: ~S deferred: ~S"
                     name option-tag *allow-forward-instance-references*)
            ;; Here, if we are allowing forward references, we defer creation
            ;;********** we should defer linking only for suc-ids we can't resolve!
            (if *allow-forward-instance-references*
                (push
                 `(with-environment ',*package* ',context ',*language*
                    (funcall #'%make-instance-sp-option ',key ',class-id ',sp-id
                             ',value-list))
                 *deferred-instance-links*)
              ;; otherwise make link
              (%make-instance-sp-option key class-id sp-id value-list )))
           
           ;; here we look for a generic property in case of orphans
           ((setq sp-id (%%get-id option-tag :sp :include-moss t))
            
            ;(format t "~&; %%make-instance-from-class-id/ class: ~S sp: ~S deferred: ~S"
            ;        name option-tag *allow-forward-instance-references*)
            ;; Here, if we are allowing forward references, we defer creation
            ;;********** we should defer linking only for suc-ids we can't resolve!
            (if *allow-forward-instance-references*
                (push
                 `(with-environment ',*package* ',context ',*language*
                    (funcall #'%make-instance-sp-option ',key ',class-id ',sp-id
                             ',value-list))
                 *deferred-instance-links*)
              ;; otherwise make link
              (%make-instance-sp-option key class-id sp-id value-list )))
           
           ;; here we did not recognize the option viewed as a property
           (t
            (warn 
                "while trying to define instance of class ~S in package ~S ~
                 context ~S, property ~S does not exist. We ignore it." 
              (if (mln::mln? name :allow-old-format t)
                  (mln::get-canonical-name name)
                name)
              (package-name *package*) context option-tag)) ; jpb1406
           )))))
    
    ;; overall check on the entity
    ;; check the :one-of class constraint
    (let ((oneof-list (%%get-value class-id '$ONEOF context)))
      (if (and oneof-list (not (member key oneof-list)))
          (warn "instances of ~S should be one of: ~%~S~%We create the new instance ~
               ~S anyway..." class-id oneof-list key)))
    
    ;; if class is tagged with a :dont-save-instances mark, we tag instance with a
    ;; :no-save mark to avoid saving it when the agent is persistent (used by OMAS)
    ;; this will unlink the entry points JPB 1001
    (if (get class-id :dont-save-instances)
        (>>setprop key t :no-save))
        ;(setf (get key :no-save) t))
    ;; we record instance at the system level
    ;(%%set-value *moss-system* key '$SVL context) ; JPB0406 strange line...
    ;; when editing
    (save-new-id key)

    (dformat :mki 0 "~%;---------- Exiting %%make-instance-from-class-id")
    key  ;; return key
    ))

#|
 cf z-moss-tests-def.lisp  defindividual
|#
;;;-------------------------------------------------- %MAKE-INSTANCE-ISA-OPTION
;;; any instance may be linked to any onter object that will be used as a 
;;; prototype. The function is similar to sp-option without the special checking
;;; possible formats for values:
;;;   - (:new <class-ref> <option>*) => create object
;;;   - variable e.g. _jpb value must be symbol id or pair id
;;;   - string e.g. "barthès" => either deferred or use access
;;;   - query e.g. ("Person" ("name" :is "Barthès")("first name" :is "Camille"))
;;;        use access

(defun %make-instance-isa-option (key value-list)
  "builds an is-a for an instance object.
Arguments:
   key: id of the instance
   value-list: list of values designating objects to link
                    or list starting with :new to create a new object
Return:
   nil if error, :done otherwise"
  (let ((context (symbol-value (intern "*CONTEXT*")))
        suc-list)
    
    ;; call function with a nil class argument
    (setq suc-list
          (%resolve-successor-for-instances value-list key '$IS-A nil context))
    
    ;; there are no constraints on the $IS-A property, link objects
    (dolist (suc-id (reverse suc-list))
      ;; link only if object is alive for the current context
      (if (%alive? suc-id context)
          (%link key '$IS-A suc-id)
        (warn "Object ~S is not alive in this context, cannot be is-a of ~S"
          suc-id key)))
    )
  :done)

#|
 cf z-moss-tests-def.lisp for %resolve-successor-for-instances
|#
;;;--------------------------------------------------------- %MAKE-INSTANCE-LINK

(defUn %make-instance-link (obj1-id sp-id obj2-ref class-id)
  "links two objects namely an instance to another instance.
Arguments:
   obj1-ref: id of the instance to link
   sp-id: id of the relation
   obj2-ref: id of the instance, or ref (:var option in MOSS, :name in SOL)
   context (opt): context
Return:
   internal format of first object"
  (let (entry suc-list)
    (cond
     ;;== first case, we have a string
     ((stringp obj2-ref) ; possible with SOL
      ;; we should try to recover objects correponding to the string
      (setq entry (send '$OBJNAM '=make-entry obj2-ref))
      (setq suc-list (%extract-from-id (car entry) '$OBJNAM (intern "*ANY*")))
      )
     ;;== second case we have an object identifier
     ((and (%is-variable-name? obj2-ref)
           (not (<<boundp obj2-ref)))
      ;; then we skip the link and send warning
      (warn "unbound reference ~S for property ~S while creating an ~
             instance of ~S. We skip the link." 
        obj2-ref sp-id class-id)
      )
     ;;== third case we have a variable bound to a PDM object
     ((and (%is-variable-name? obj2-ref)
           (boundp obj2-ref)
           (%%is-id? (symbol-value obj2-ref))
           (<<boundp (symbol-value obj2-ref))
           (%pdm? (symbol-value obj2-ref)))
      ;(print `(+++ ref ,(symbol-value value)))
      ;; collect successor
      (push (symbol-value obj2-ref) suc-list)
      )
     ;;== fourth case, we have the identifier of a PDM object
     ((%pdm? obj2-ref) ;JPB0506 (symbol-value value) -> value
      ;; then, collect successor
      (push obj2-ref suc-list)
      )
     ;;== fifth case is we have a query
     ((parse-user-query obj2-ref)
      (setq suc-list (append (access obj2-ref) suc-list)))
     )
    
    ;;== if result is non nil link object1 to list of successors
    (if suc-list
      ;; link objects
      (dolist (suc-id suc-list)
        (%link obj1-id sp-id suc-id))
      ;; otherwise issue a warning
      (warn "something wrong with ~S for property ~S while creating an ~
             instance of ~S. Cannot find the referenced object(s)." 
            obj2-ref sp-id class-id))
    ;; return obj1-l
    (<< obj1-id))
  )

#| Example from SOL
aquitaine
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 AQUITAINE))
 (MOSS::$OBJNAM.OF (0 $E-REGION.1)) (MOSS::$EPLS.OF (0 MOSS::$SYS.1)))

gironde
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 GIRONDE))
 (MOSS::$OBJNAM.OF (0 $E-FRENCH-DEPARTMENT.2)) (MOSS::$EPLS.OF (0 MOSS::$SYS.1)))

(%make-instance-link '$E-REGION.1 '$S-REGION-FRENCH-DEPARTMENT "gironde"
     '$S-REGION)
((MOSS::$TYPE (0 $E-REGION)) (MOSS::$ID (0 $E-REGION.1))
 (MOSS::$OBJNAM (0 (:FR "aquitaine")))
 ($S-REGION-FRENCH-DEPARTMENT (0 $E-FRENCH-DEPARTMENT.2)))
|#
;;;-------------------------------------------------- %MAKE-INSTANCE-NAME-OPTION
;;; used only by SOL

(defUn %make-instance-name-option (key name value &key export)
  "builds a name for the object attached to property $OBJNAM. 
   Name is an entry (used by SOL). Not to be confused with :var.
Arguments:
   key: id of the instance
   name: name of the class
   value: value associated with :name option may be MLN
   export (key): if t we esport the name
Return:
   nil if error, :done otherwise"
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (tp-id '$OBJNAM)
        entry-point fn xi-fn)
    (setq xi-fn (get-method tp-id '=xi context))
    ;;ok, tp is valid. Norm value
    (if xi-fn (setq value (apply xi-fn value nil)))
    ;(setq value (-> tp-id '=xi value))
    ;; nil is considered an error 
    (unless value
      (warn
       "while trying to define instance of class ~A, ~
        the :name option ~A has not the proper format."
       name (list :name value))
      ;; on error skip property
      (return-from %make-instance-name-option nil))
    
    ;; no restrictions are attached to $OBJNAM
    ;; Build entry point if necessary
    (when (setq fn (get-method tp-id '=make-entry context))
      (setq entry-point (funcall fn value))
      ;; using -> leads to execute get-method twice!
      ;(setq entry-point (-> tp-id '=make-entry value))
      (%make-ep entry-point tp-id key :export export)
      )
    ;; add value to key
    (%add-value key tp-id value context)
    :done))

;;;---------------------------------------------------- %MAKE-INSTANCE-SP-OPTION
#|
(dformat-set :misp 0)
(dformat-reset :misp)
|#
(defUn %make-instance-sp-option (key class-id sp-id value-list)
  "builds a relation for an instance object.
Arguments:
   key: id of the instance
   class-id: id of the class
   sp-id: id of the relation
   value-list: list of values designating objects to link
               or list starting with :new to create a new object
Return:
   nil if error, :done otherwise"
  (drformat :misp 0 "~2%;---------- Entering %make-instance-sp-option")
  
  (let ((context (symbol-value (intern "*CONTEXT*")))
        suc-list)
    ;; get the list of successor objects that exist and could be linked now!
    (setq suc-list 
          (%resolve-successor-for-instances value-list key sp-id class-id context))
    (dformat :misp 0 "final suc-list: ~S" suc-list)
    
    ;; check constraints on the values (objects) that were put into the suc-list
    ;; deferred values will be processed later
    (multiple-value-bind (result error-list) 
        (%validate-sp sp-id suc-list :obj-id key) ; JPB060215
      (declare (ignore error-list))
      ;; here result contains the list of filtered successors
      ;; warning messages, if any, were printed by %validate-sp
      
      ;; link whatever is left of the successor list
      (dolist (suc-id result) ; JPB1611 to keep neighbors in order (?)
        (%link key sp-id suc-id)))
    (drformat :misp 0 "~%;---------- Exiting %make-instance-sp-option")
    )
  :done)

#|
cf z-moss-tests-def.lisp for ore tests
|#
;;;---------------------------------------------------- %MAKE-INSTANCE-TP-OPTION
;;; test for replacing this function by add-values

(defUn %make-instance-tp-option (name key prop-name tp-id value-list prop-list
                                      &key export)
  "builds an attribute for an instance object. Issues a warning and ignore ~
   attribute if it is not one of the class.
Arguments:
   name: name (ref) of the instance class 
   key: id of the instance
   prop-name: name (ref) of the attribute
   tp-id: id of the attribute
   value-list: list of values for the attribute
   prop-list: list of attributes attached to the class
   export (key): if t we export the name
Return:
   1. internal modified list of the instance (eval key)
   2. list of error messages"
  
  ;; here property exists and we have to check if it belongs to the class
  ;; this should be changed to accept "O2-like exceptional properties"
  ;; However it is not sensible to put it here since we are using the
  ;; class as a generator. Exceptional properties will be added through
  ;; =add-tp and =add-sp method.
  (unless (member tp-id prop-list)
    (mformat 
        "while trying to define instance of class ~A, property ~A does not belong ~
         to class in package ~S with context ~S. We ignore it. Use =add to ~
         overcome this test." 
      name prop-name (package-name *package*) (symbol-value (intern "*CONTEXT*")))
    (return-from %make-instance-tp-option nil))
  
  ;; we call add-values, recovering error messages
  (multiple-value-bind (obj-l message-list)
      (add-values key tp-id value-list :export export :no-warning t)
    (when message-list
      ;; add header to the list of messages
      (push
       (format nil "Warning: while adding values to the instance ~S in package ~S ~
        and context ~S" 
         key (package-name *package*) (symbol-value (intern "*CONTEXT*")))
       message-list)
      (mformat "~{~%~A~}" message-list))
    ;; return modified instance
    obj-l)
  )

#|
cf z-moss-tests-def.lisp
|#
;;;------------------------------------------------------ %MAKE-MULTIPLE-INSTANCE

(defun %make-multiple-instance (class-ref-list option-list &optional no-warning)
  "the instance is an instance of several classes. We must create an id for each ~
   class and record each property/values according to the class where the property ~
   is defined.
Arguments:
   class-list: list of class ids
   option-list: list of options
Return:
   the id corresponding to the first class."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (class-ref (car class-ref-list))
         id rest-of-option-list)
    ;; filter properties belonging to the first class
    (multiple-value-setq (option-list rest-of-option-list)
      (%make-multiple-instance-filter-prop class-ref option-list))
    ;; create first instance
    (setq id (apply #'%make-instance class-ref option-list))
    ;; for each remaining class
    (dolist (next-class-ref (cdr class-ref-list))
      ;; create a new id
      (multiple-value-bind (next-id class-id) (%make-ref-object next-class-ref id)
          ;; add reference to object
          (>> id (%%add-value id '$ID next-id context))
          (>> id (%%add-value id '$TYPE class-id context))
      ;; filter out properties belonging to that class
      (multiple-value-setq (option-list rest-of-option-list)
        (%make-multiple-instance-filter-prop next-class-ref rest-of-option-list))
      ;; add each property to the instance beiong built
      (dolist (pair option-list)
        ;; last arg is a no-warning argument to cancel warning messages
        (%%make-instance-add-property id class-id (car pair) (cdr pair) no-warning))
      ))
    ;; if anything left in option list must add id as an uncharted prop
    (dolist (pair rest-of-option-list)
      (%%make-instance-add-property id class-ref (car pair) (cdr pair) no-warning))
    ;; return the instance id
  id))

#|
 cf z-moss-tests-def.lisp
|#
;;;----------------------------------------- %MAKE-MULTIPLE-INSTANCE-FILTER-PROP

(defun %make-multiple-instance-filter-prop (class-ref option-list)
  "extracts from option-list the props corresponding to class-ref and returns ~
   what is left of option-list as a second value.
Arguments:
   class-ref: reference of a class
   option-list: list of pairs (prop-ref values)
Return:
   1st value: list of pairs associated with the class
   2nd value: rest of option-list"
  (let ((class-id (%%get-id class-ref :class))
        result)
    ;; get the list of properties attached to the class
    ;(setq prop-list (%%get-all-class-properties class-id))
    (setq result 
          (remove-if-not  
           #'(lambda (xx) (%%get-id xx :property :class-ref class-id))
           option-list :key #'car))
    (values
     result
     (set-difference option-list result 
                     :test #'(lambda (xx yy)
                               (if (and (stringp xx)(stringp yy))
                                   (string-equal xx yy)
                                 (equal xx yy)))
                     :key #'car))
    ))

#|
 cf z-moss-tests-def.lisp
|#
;;;=============================================================================
;;;
;;;                         ORPHANS (Classless Objects) 
;;;
;;;=============================================================================

;;;----------------------------------------------------------------- MAKE-ORPHAN

(defun make-orphan (&rest option-list)
  (apply #'%make-object option-list))

;;;------------------------------------------------------------------ MAKE-OBJECT

(defun make-object (&rest option-list)
  (apply #'%make-object option-list))

;;;----------------------------------------------------------------- %MAKE-OBJECT
;;; when allowing forward references we create objects with the instances and
;;; link them afterwards

(defUn %make-object (&rest option-list)
  "function that creates orphans (classless objects, instances of the NULL-CLASS).
   syntax of option-list is
   	(<tp-name> value[list])
   	(<sp-name> <internal-reference>)
   	(:name <var-name>)
    Properties are checked but can be any property in system 
    Values for terminal properties are checked (=xi) and also for entry-point
    generation
    Internal references to MOSS entities must correspond to existing objects
    and of the proper type. If not, then an error message is sent.
    Example
   	(defobject 
   		(HAS-NAME \"Barthes\")
   		(HAS-BROTHER _p1))
    will work if _p1 is the internal reference to an already defined person
    or specialization of person.
    Returns the internal reference to the new object
    Once created objects must be modified directly, using access methods
    Integrity constraints are checked to see whether the properties are
    correctly filled."
  ;; orphans are instances or the NULL-CLASS (id = *none*)
  (apply #'%make-instance "MOSS-NULL-CLASS" (cons '(:no-warning) option-list)))

#|
cf z-moss-tests-def.lisp
|#
;;;=============================================================================
;;;
;;;                 VIRTUAL OBJECTS (Attributes, Relations, Rules) 
;;;
;;;=============================================================================
;;; Virtual objects are not part of MOSS but can be added when a MOSS ontology
;;; is built to be translated into OWL and JENA/SPARQL rules. Thus, definitions
;;; of virtual objects are stores in the instance of current system to be used
;;; during the reconstruction of a MOSS file and the subsequent translation to
;;; OWL.

;;;------------------------------------------------------ %MAKE-VITUAL-ATTRIBUTE

(defun %make-virtual-attribute (&rest options)
  "saves definition into system"
   (let ((system (intern "*MOSS-SYSTEM*"))
        (ctxt (intern "*CONTEXT*")))
   (%add-values-att (<<symbol-value system) '$VST
               `((:vatt ,(format nil "~S" options)))
               (symbol-value ctxt))))

;;;------------------------------------------------------- %MAKE-VITUAL-RELATION

(defun %make-virtual-relation (&rest options)
  "saves definition into system"
  (let ((system (intern "*MOSS-SYSTEM*"))
        (ctxt (intern "*CONTEXT*")))
   (%add-values-att (<<symbol-value system) '$VST
               `((:vrel ,(format nil "~S" options)))
               (symbol-value ctxt))))

;;;----------------------------------------------------------- %MAKE-VITUAL-RULE

(defun %make-virtual-rule (&rest options)
  "saves definition into system"
  (let ((system (intern "*MOSS-SYSTEM*"))
        (ctxt (intern "*CONTEXT*")))
   (%add-values-att (<<symbol-value system) '$VST 
                    `((:vrule ,(format nil "~S" options)))
                    (symbol-value ctxt))))

;;;=============================================================================
;;;
;;;                               ONTOLOGY 
;;;
;;;=============================================================================
;;; An ontology is defined in the package specified in the defontology macro, say
;;; (:package :family)
;;; If defontology has no package option, then the default one is the one
;;; that was current when calling defontology
;;; The ontology package is set to moss::*application-package* unless defontology 
;;; has the option :keep-package
;;; The language option is set to moss::*language*
;;; A system stub is created and assigned to the special variable, e.g.
;;;   FAMILY::*MOSS-SYSTEM*

;;;--------------------------------------------------------------- MAKE-ONTOLOGY

(defun make-ontology (&rest option-list)
  (apply #'make-ontology option-list))

;;;-------------------------------------------------------------- %MAKE-ONTOLOGY

(defUn %make-ontology (&rest option-list)
  "creates a new ontology, package and stub if needed. If it already exists, does ~
   nothing.
Arguments:
   option-list:
    :language one of the legal languages (:fr, :en,...) or :all meaning that we 
                have multiple languages (default is English)
    :package specifies a package (default: current *package*)
    :title a string title of the ontology (default: \"Anonymous Ontology\")
    :active-languages list of used languages (used by SOL)
    :version a string, e.g. \"2.0\" (default: \"0.0\")
    :keep-package means that we do not set *package* to ontology package
Return:
   the id of the moss-system instance in the ontology package."
  (declare (special *application-package* *language*))
  
  (let* ((language-option (assoc :language option-list))
         (package (or (cadr (assoc :package option-list)) *package*))
         (title (or (cadr (assoc :title option-list)) "Noname Ontology"))
         (version (or (cadr (assoc :version option-list)) "0.0"))
         system)

    ;; why should e have a MOSS global *language* variable?
    ;(when language-option
    ;  (setq *language* (or (cadr language-option) :en)))

    
    ;; find or create the application package (a MOSS global variable)
    (setq *application-package* 
          (or (find-package package)
              ;; omas is there although for MOSS is does not matter
              (make-package package :use (list :moss #+omas :omas :cl :ccl))))

    (setq system (intern "*MOSS-SYSTEM*" *application-package*))
            
    ;; create stub for the ontology, unless already there
    ;; the stub is created in the ontology package
    (unless (boundp system)
      (format t "~%; creating an ontology stub in package ~S"
        (package-name *application-package*))
      (%create-new-package-environment *application-package* title version))
    
    ;; set global package to application package unless not wanted
    (unless (assoc :keep-package option-list)
      (setq *package* *application-package*))

    ;; save the definition of ontology options into the system description
    ;; eventually 
    (%add-value (<<symbol-value system) '$OST
               `(:onto ,(write-to-string option-list :pretty t))
               (symbol-value (intern "*CONTEXT*" *application-package*)))

    ;; return the id of the ontology stub
    (symbol-value system)))

#|
? *package*
#<Package "MOSS">
(moss::%make-ontology 
    '(:title "Family Test Ontology")
    '(:language :all)
    '(:package :family-test)
    )
($SYS . 1)

? *package*
#<Package "FAMILY-TEST">

;; in package :family-test
(<< '(family-test::$SYS . 1))
((MOSS::$TYPE (0 FAMILY-TEST::$SYS))
 (MOSS::$ID (0 (FAMILY-TEST::$SYS . 1)))
 (MOSS::$SNAM (0 ((:EN "FAMILY-TEST"))))
 (MOSS::$PRFX (0 FAMILY-TEST::FAMILY-TEST))
 (MOSS::$EPLS
  (0 FAMILY-TEST::FAMILY-TEST FAMILY-TEST::FAMILY-TEST-MOSS-SYSTEM METHOD
   FAMILY-TEST::UNIVERSAL-METHOD FAMILY-TEST::COUNTER
   FAMILY-TEST::NULL-CLASS FAMILY-TEST::UNIVERSAL-CLASS))
 (MOSS::$CRET (0 "MOSS")) (MOSS::$DTCT (0 "17/12/2019"))
 (MOSS::$VERT (0 "0.0")) (MOSS::$XNB (0 0))
 (MOSS::$SVL
  (0 FAMILY-TEST::*MOSS-SYSTEM* FAMILY-TEST::*ONTOLOGY*
   FAMILY-TEST::*CONTEXT* MOSS:*LANGUAGE* FAMILY-TEST::*VERSION-GRAPH*))
 (MOSS::$ENLS
  (0 FAMILY-TEST::$SYS FAMILY-TEST::$E-FN FAMILY-TEST::$E-UNI
   FAMILY-TEST::$E-CTR FAMILY-TEST::*NONE* FAMILY-TEST::*ANY*))
 (MOSS::$OST
  (0
   (:ONTO
    "((:TITLE \"Family Test Ontology\") (:LANGUAGE :ALL) (:PACKAGE :FAMILY-TEST))"))))
(:PDM T)
T

? family-test::*MOSS-SYSTEM*
($SYS . 1)
|#

;;;=============================================================================
;;;
;;;                         SERVICE FUNCTIONS 
;;;
;;;=============================================================================

;;;--------------------------------------------- %RESOLVE-SUCCESSOR-FOR-INSTANCES
;;; this function can be called when trying to link an instance to another object.
;;; The other object may not yet be created.
#|
(dformat-set :rsuc 0)
(dformat-reset :rsuc)
|#

(defun %resolve-successor-for-instances
    (value-list obj-id sp-id class-id &optional version)
  "tries to determine which objects are meant by value-list. Each term can have ~
   the following format:
   - a list like (:new <class-ref><option>*)
   - a variable like _jpb
   - a symbol id or an id pair
   - a string (entry point)
   - a MOSS query
   There is no check that vlaues are objects instances of the SUC class(es).
Arguments:
   value-list: a list of the above formats
   sp-id: relation linking the object
   obj-id: id of the object to link
   class-id: id of the class of obj-id (nil if sp-id is $IS-A)
   version (opt): specified context
Return:
   a list of objects that can be linked immediately.
Side-effect:
   inserts entries on the *deferred-instance-creations* list for later execution"
  (declare (special *allow-forward-instance-references*
                    *deferred-instance-creations*))
  (drformat :rsuc 0 "~2%;---------- Entering %resolve-successor-for-instances")
   
  (let* ((context (or version (symbol-value (intern "*CONTEXT*"))))
         suc-list access-list fn)
    
    (dolist (value value-list)
      (dformat :rsuc 0 "value ~S in package ~S and context ~S" 
               value (package-name *package*) context)
          
      (cond
       ;;=== value is a list starting with :new, e.g. (:new "Address" ("street" "..."))
       ((and (listp value)(eql (car value) :new))
        (dformat :rsuc 0 "list starting vith :new: ~S" value)
        ;; we must create the corresponding instance (make-instance "address" ...)
        ;; if it does not work then we get a string
        ;; if it works then we get an object identifier that we can use as a value
        ;; Presumably when loading an ontology all classes have been created before
        ;; instances are considered for creation
        (setq value (apply #'%make-instance (cdr value)))
        ;; collect suc-id
        (setq suc-list (append suc-list (list value)))  ; JPB1611 to keep order (?)
        )
       
       ;;=== first case we have a variable name, the variable is unbound and
       ;; we allow forward references (for batch files, i.e. loading ontologies)
       ;; when %make-instance is executed we never have this case since when
       ;; *allow-forward-instance-references* is true we do not call this function
       
       ((and (%is-variable-name? value)
             (not (boundp value))
             (eql sp-id '$IS-A)
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-isa-option ',obj-id ',value)))
              *deferred-instance-creations*))

       ((and (%is-variable-name? value)
             (not (boundp value))
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the (ontology) file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-link ',obj-id ',sp-id ,value ',class-id)))
              *deferred-instance-creations*))
       
       ;;=== second case, no forward references allowed, skip the value
       ;; send a warning
       ;; should not happen since we are linking instances after creating them
       ((and (%is-variable-name? value)
             (not (boundp value)))
        ;; then we skip the link and send warning
        (warn "unbound reference ~S for property ~S. We skip the link" value sp-id))
       
       ;;=== third case we have a variable, e.g. _jpb bound to ($E-PERSON . 1)
       ((and (%is-variable-name? value)
             ;; variable must be bound, if not quit
             (boundp value)
             ;; its value must be a PDM object (eliminated by %validate-sp ??)
             (%pdm? (symbol-value value)))
        ;; collect successor (id or id pair)
        (push (symbol-value value) suc-list)
        )
       
       ;;=== fourth case, we have the identifier of a PDM object (id or id pair)
       ((%pdm? value) ; contains test to %%is-id?
        ;; then, collect successor
        (push value suc-list)
        )
       
       ;;=== fifth case we have a string (SOL) linked to an object name
       ;; should not happen (done at the %make-instance level)
       ((and (eql sp-id '$IS-A)
             (stringp value) 
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-isa-option ',obj-id ,value)))
              *deferred-instance-creations*)
        )
       
       ((and (stringp value) 
             *allow-forward-instance-references*)
        ;; then we defer the binding until the end of the file
        (push `(with-package ,*package*
                 (with-context ,context
                   (%make-instance-link ',obj-id ',sp-id ,value ',class-id)))
              *deferred-instance-creations*)
        )
       
       ;;=== sixth case we have a string, no deferring linking, but $IS-A prop
       ((and (stringp value)(eql sp-id '$IS-A))
        ;; try to locate objects using access
        (setq suc-list (append suc-list (access value)))
        )
       
       ;;=== seventh case we have a string or a MOSS query but not deferred linking
       ((and (parse-user-query value)
             (setq access-list (access value)))
        (dformat :rsuc 0 "access-list: ~S" access-list)
        ;; we need not remove bad successors, %validate-sp will do it      
        (setq suc-list (append suc-list (reverse access-list)))
        (dformat :misp 0 "suc-list: ~S" suc-list)
        )
              
       ;;=== any other case is invalid
       (t (warn "something wrong with ~S when trying to locate corresponding ~
                 objects in package ~S and context ~S." 
            value (package-name *package*) context)))
      
      ) ; end of dolist
    
    (dformat :rsuc 0 "suc-list: ~S" suc-list)
    
    ;; should apply =filter method here
    (setq fn (get-method sp-id '=filter context))
    (if fn
        (setq suc-list 
              (remove nil
                      (mapcar #'(lambda(xx) (apply fn xx obj-id nil)) suc-list))))
    
    (dformat :rsuc 0 "filtered suc-list: ~S" suc-list)
    (drformat :rsuc 0 "~%;---------- Exiting %resolve-successor-for instances")
    ;;return whatever we found
    suc-list))

#|
 cf z-moss-tests-def.lisp
|#
;;;--------------------------------------------- %RESOLVE-SUCCESSORS-FOR-DEFAULT
;;; this function can be called when specifying the :default or :one-of option
;;; of a relation. Because relations are deferred when loading an ontology file
;;; the function executes when all classes have been defined. Thus, an instance
;;; can be computed right away (at least with attributes since other relations 
;;; might not have been resolved yet)
;;; Another question: should the default values be necessarily instances of the
;;; SUC class(es)?
#|
(dformat-set :rss 0)
(dformat-reset :rss)
|#

(defun %resolve-successors-for-default (value-list obj-id sp-id &optional version)
  "tries to determine which objects are meant by value-list when specifying ~
   defaults or one-of values for a relation. Each term can have the following ~
   format:
   - a list like (:new <class-ref><option>*)
   - a variable like _jpb
   - a symbol id or an id pair
   does not use access to locate values, thus strings are not allowed.
Arguments:
   value-list: a list of the above formats
   sp-id: relation linking the object e.g. $DEFT
   obj-id: id of the object to link, e.g. $S-PERSON-PRESIDENT
   version (opt): specified context
Return:
   a list of values to be used in the :default or :one-of option."
  (drformat :rss 0 "~2%;---------- Entering %resolve-successors-for-default")
   
  (let* ((context (or version (symbol-value (intern "*CONTEXT*"))))
         suc-list fn)
    
    (dolist (value value-list)
      (dformat :rss 0 "value ~S in package ~S and context ~S" 
               value (package-name *package*) context)
      
      (cond
       ;;=== first case, value is a list starting with :new, e.g. 
       ;;      (:new "Address" ("street" "..."))
       ((and (listp value)(eql (car value) :new))
        (dformat :rss 0 "list starting vith :new: ~S" value)
        ;; we must create the corresponding instance (make-instance "address" ...)
        ;; if it does not work then we get a string
        ;; if it works then we get an object identifier that we can use as a value
        ;; Presumably when loading an ontology all classes have been created before
        ;; instances are considered for creation
        (setq value (apply #'%make-instance (cdr value)))
        ;; collect suc-id taking care of nil value
        (setq suc-list (append suc-list (list value)))  ; JPB1611 
        )
              
       ;;=== second case, no forward references allowed, skip the value
       ;; send a warning
       ;; should not happen since we are linking instances after creating them
       ((and (%is-variable-name? value)
             (not (boundp value)))
        ;; then we skip the link and send warning
        (warn "unbound variable reference ~S for property ~S in package ~S and ~
               context ~S. ~%We skip the default. You could use the :new option."
          value sp-id (package-name *package*) context))
       
       ;;=== third case we have a variable, e.g. _jpb bound to ($E-PERSON . 1)
       ((and (%is-variable-name? value)
             ;; variable must be bound, if not quit
             (boundp value)
             ;; its value must be a PDM object (eliminated by %validate-sp ??)
             (%pdm? (symbol-value value)))
        (dformat :rss 0 "variable: ~S, bound to: ~S" value (symbol-value value))
        ;; collect successor (id or id pair)
        (push (symbol-value value) suc-list)
        )
       
       ;;=== fourth case, we have the identifier of a PDM object (id or id pair)
       ((and (%%is-id? value)
             (%pdm? value)
             ;; object must be instance of a class allowed by $SUC
             ;(some #'(lambda(xx)(%type? value xx)) class-id-list)
             )
        (dformat :rss 0 "id or id pair: ~S" value)
        ;; then, collect successor
        (push value suc-list)
        )
       
       ;;=== fifth case we have a string (SOL) linked to an object name
       ;; should not happen (done at the %make-instance level)
       ;; we should not refer to still not created instances when specifying
       ;; defaults for a relation, but use the (:new...) syntax
       
;;;       ((and (stringp value) 
;;;             *allow-forward-instance-references*)
;;;        ;; then we defer the binding until the end of the file
;;;        (push `(with-package ,*package*
;;;                 (with-context ,context
;;;                   (%make-instance-link ',obj-id ',sp-id ,value ',class-id)))
;;;              *deferred-instance-creations*)
;;;        )
       
;;;       ;;=== sixth case we have a string, no deferring linking, but $IS-A prop
;;;       ((and (stringp value)(eql sp-id '$IS-A))
;;;        ;; try to locate objects using access
;;;        (setq suc-list (append suc-list (access value)))
;;;        )
;;;       
;;;       ;;=== seventh case we have a string or a MOSS query but not deferred linking
;;;       ((and (parse-user-query value)
;;;             (setq access-list (access value)))
;;;        (dformat :rsuc 0 "access-list: ~S" access-list)
;;;        ;; we need not remove bad successors, %validate-sp will do it      
;;;        (setq suc-list (append suc-list (reverse access-list)))
;;;        (dformat :misp 0 "suc-list: ~S" suc-list)
;;;        )
              
       ;;=== any other case is invalid
       (t (warn "invalid format for default or one-of option: ~S when adding ~S ~
                 ~%in package ~S and context ~S"
            value sp-id (package-name *package*) context))
       ) ; end of cond

      ) ; end of dolist
    
    (dformat :rss 0 "suc-list:~%  ~S" suc-list)
    
    ;; should apply =filter method here to clean the resulting list
    (setq fn (get-method sp-id '=filter context))
    (if fn 
        (setq suc-list
              (remove nil
                      (mapcar #'(lambda(xx)(apply fn xx obj-id nil)) suc-list))))
     
    (dformat :rss 0 "filtered suc-list:~%  ~S" suc-list)
    (drformat :rss 0 "~%;---------- End %resolve-successors-for-default")
    ;;return whatever we found
    suc-list))

#|
 cf z-moss-tests-def.lisp
|#
;;;---------------------------------------------- %RESOLVE-SUCCESSORS-FOR-ONE-OF
;;; Here we assume that the defaut values already exist, which prevents from
;;; specifying defaults when creating a class, e.g.
;;;  (defconcept "Professor"
;;;    (:rel "employer" (:default (:new "Organization" ("name" "UTC"))))
;;; which assumes that the class "Organization" already exists, which may not be
;;; the case when loading an ontology file
;;; Anyway this syntax is not allowed

;;; However, processing sp is deferred until all classes have been created, which
;;; would allow to use the :new syntax with defaults containing only attibutes
;;; since other sps may not be loaded yet, as done for the :default option

(defun %resolve-successors-for-one-of (value-list suc-id)
  "checks that all values are instances of the suc-id class, no check is done ~
   on input arguments. Each term can have the following format:
   - NIL ignored
   - %PDM? saved as it
   - bound variable: save value of the variable
   - string: makes access
   - query: makes access
Arguments:
   value-list: a list of entries with the above formats
   suc-id: the successor class of the relation.
Return:
   a list of object ids of the right type (or sub-type) or nil"
  (let (result query)
    (dolist (item value-list)
      ;(format t "~%; %resolve-successors-for-one-of /item: ~S" item)
      ;; try to get la list of objects
      (cond
       ((null item))
       ;; if item is an objet id leave it as it is
       ((%pdm? item) (push item result)
        )
       ;; if item is a variable, check if bound
       ((and (%is-variable-name? item)(boundp item))
        (push (symbol-value item) result)
        )
       ;; if item is a string try for en entry point
       ((stringp item)
        (setq result (append (access item) result))
        )
       ;; list could be a query
       ((and (listp item)(setq query (parse-user-query item)) )
        (setq result (append (access query :already-parsed t) result)))
       )
      )
    ;; remove null references and reverse list
    (setq result (reverse (remove nil result)))
    ;(format t "~%; %resolve-successors-for-one-of /result: ~S" result)
    ;; check if the objects are instances of suc-id
    (setq result 
          (remove nil
                  (mapcar #'(lambda (xx) (if (%type? xx suc-id) xx)) result)))
    
    ;; return result
    result))
       
#|
 cf z-moss-tests-def.lisp
|#

:EOF