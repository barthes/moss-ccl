;;;=================================================================================
;;;19/08/06
;;;		
;;;		C O N S O L E - T E S T S - (File z-console-tests.lisp)
;;;	
;;;=================================================================================

;;; tests to apply after MOSS has been loaded
;;; done in the listener space

(use-package :moss)

(moss::%create-new-package-environment :cl-user)

(defconcept "Human")

(defconcept "Person" 
      (:att "name" (:min 1)(:max 3)(:entry))
      (:att "first name"))

(defattribute "sex")

(defattribute "sex" (:min 1) (:max 1) (:concept "Person"))
;; BUG: does not attach attribute to PERSON

(defrelation "brother" (:from "Person")(:to "Person"))

(defrelation "neighbor" (:from :any)(:to :any))

(defconcept "Department"  (:att "name"))

(defconcept "Course"
       (:att "code" (:min 1)(:max 3)(:entry))
       (:rel "department" (:to "Department")))

(defconcept "Student" 
        (:is-a "Person")
        (:rel "course" (:to "course")))

(defconcept "Teacher" 
         (:is-a "person")
         (:rel "lecture" (:to "course"))
         (:rel "department" (:to "department")))

(defindividual "person")

(setq _p1 (defindividual PERSON))

(defindividual PERSON (:var _p1))

(defindividual PERSON
          (:var _p1)
          (has-name "Dupond" "Durand")
          (has-first-name "Jean")
          (has-sex "M"))

(defindividual PERSON
            (:var _p2)
            ("name" "Dubois" "Dupond")
            ("sex" "F")
            ("brother" _p1))

(send '$E-person '=print-self)

(send _person '=print-self)

(send PERSON '=print-self)

(send _p1 '=print-self)

(send _has-brother '=print-self)

(send _has-person-brother '=print-self)

(definstmethod =print-self PERSON ()
                "Prints a list of first names and names"
              (format t "~&~{~A~^-~} ~{~A~^-~}" (HAS-FIRST-NAME) (HAS-NAME)))

(send _p1 '=print-self)

(defownmethod =print-self _p2 ()
                "Prints the name with Ms. in front of it."
              (format t "~&Ms ~{~A~^-~}" (HAS-NAME)))

(send _p2 '=print-self)

(defobject ("name" "Joe")(:var _ob1))

(defobject (:var _p4)("NAME" "George")(:is-a _p2))

(send _p4 '=print-self)

(send _p2 '=get-properties)

(send _person '=print-methods)

(send _person '=what?)

(send _p4 '=what?)

(trace-message)

(send _p1 '=print-self)

(send _ob1 '=print-self)

(untrace-message)

(trace-object _ob1)

(send _ob1 '=print-self)

(untrace-all)

(trace-method '=get-id)

(send _ob1 '=print-self)

(catch :error (defconcept "Humanoid" (:is-a "Mammal")))





