;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;19/10/19
;;;		
;;;		P A T H S - (File PATHS.LISP)
;;;	 
;;;==========================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
;;; This file contains functions allowing to access a MOSS KB using text. Some
;;; functions process the string against the database, others do a direct 
;;; access. Some functions are exported to allow users to build agent skills.

;;; The module should contain
;;; - PROCESS-PATTERNS        to preprocess word-list
;;; - ACCESS                  for executing queries
;;; - ACCESS-FROM-WORDS       takes a list of words, builds a set of formal 
;;;                           queries and applies them to the KB. Removes 
;;;                           duplicates.
;;; - %access-entities-from-word-list (in moss-service.lisp)
;;;                           takes a list of words, removes the empty
;;;                           words and tries to find entities of the KB using
;;;                           entry points from the list of words and filtering
;;;                           the results to keep only best matches.
;;; - LOCATE-OBJECTS          locate objects using entry points and target concept
;;; - PATH-BUILD-QUERY-LIST   build formal queries
;;; - FIND-BEST-ENTRIES       extracts entry points from a list of words
;;; - FILL-PATTERN (?)        fills a pattern in a generic way
;;; - FIND-OBJECTS-FROM-TEXT  extract objects using locate-objects then
;;;                           path-build-query-list and access if it does not
;;;                           work
;;; OMAS is using the following functions:
;;; - FIND-ANSWER             obeys OMAS Content Language (is in MOSS-SERVICE)
;;;                           generic function that uses the input data to a 
;;;                           skill to access info in the agent KB, and format 
;;;                           it, using either pattern, if in the data, or 
;;;                           =format-method, uses access-from-words
;;; - GET-ANSWER              same as find-answer but uses locate-objects rather
;;;                           than access-from-words and path-make-query-list


;;; the file contains functions that will produce a spanning tree on the KB
;;; encompassing the specified relations and concepts contained in the request
;;;
;;; The exported access function is:
;;;       (LOCATE-OBJECTS <word-list> <target-concept> {<max path length>})
;;; it returns a list of individuals of target concepts after traversing
;;; all relations or concepts of the word list

;;; TO DO
;;; - take into account attributes and associated values in the list of words
;;;   (should be done as a pre-processing of the sentence)
;;; - take into account operators when dealing with attribute value info
;;; - take into account conjunctions like AND or OR
;;; 

;;; The file also contains a set of functions to build a formal query from 
;;;  a list of words.
;;; The function to do that is:
;;;       (PATH-BUILD-QUERY-LIST <word-list>)
;;;
;;; The goal of this function is to build a set of formal paths using the semantic 
;;; net of the ontology, starting with a list of words.
;;; First the words are transformed into class-ids, relation-ids, or entry-points.
;;; Thus, we obtain an ordered list of places (nodes, relations) that the path
;;; must traverse to obtain the formal query.
;;; Entry points have a different role: they contain a terminal property an 
;;; operator, and the class to which it may apply. If the class is missing from 
;;; the original marked items, then it can be recovered from the entry point frame.
;;; 
;;; Second, we solve special cases directly. E.g. when there is a single item, or
;;; a single entry point frame...
;;;
;;; The main algorithm is to build paths starting from a specific class, the 
;;; target, that will go through all the items of the ordered list.
;;; At each step, we check if we came to the next item of the ordered list in
;;; one of the paths. If so, we throw away all the other possibilities, 
;;; betting on the shortest path heuristic. We then proceed from there.
;;; If we did not come to the next item, then a number of path are generated 
;;; using the relations and inverse relations attached to the class.
;;;
;;; To avoid fanning, we limit the possible length of a path, quitting when
;;; it is reached.
;;;
;;; Path format
;;;   <path>::=(<class frame>+ {<link> <sub-path>}) 
;;;  where
;;;
;;;   <class-frame>::=(<class-id> <filter>*)
;;;   <link>::=<relation-id>|<inverse-relation-id>
;;;   <sub-path>::=(<class-id>)|<path>
;;; example
;;;   (($E-UNIVERSITY ($T-ORGANIZATION-NAME :IS "UTC")) 
;;;             $S-PRESIDENT-OF ($E-PERSON))


#|
HISTORY
2019
 1019 copied from ACL file
|#

(in-package "MOSS")

;;;======================= Macros and functions ===============================
;;; We define user names for various macros

(defUn %class-and-subclasses (class)
  (%sp-gamma class (%inverse-property-id '$IS-A)))

;;;----------------------------------------------------------- FIND-BEST-ENTRIES
;;; 091007 find-best-entries, correction to keep entry point string when it 
;;;      contains more than a single word, 
;;;      id. for find-best-entries-from-property-name-and-class-ref
;;; 100815 find-best-entries, too inefficient. 
;;;      Noticing that most ep functions are moss::default-make-entry it is enough
;;;      to collect the different functions called by =make-entry, remove duplicates
;;;      compute all possible entry points from that

(defUn find-best-entries (word-list &key (max-count 5) keep-ref all used-patterns)
  "tries to extract from the sentence entry points by combining the maximum ~
   number of words. When a phrase is extracted, the sentence is broken into ~
   fragments, and each fragment is processed in turn.
Arguments:
   word-list: list of words corresponding to the initial sentence
   max-count (key): maximal number of words to be put together to form a phrase
   keep-ref (key): keeps the initial reference
   all (key): extract all possible entries not only the first one
   used-patterns (key): forbidden patterns (already used)
Return:
   2 values: a list of entry points or nil if none
             the list of words that did not contribute to entry-points."
  (when word-list
    (let* ((index  (generate-access-patterns word-list :max max-count))
           (patterns (mapcar #'car index))
           ;; make a list of =make-entry methods that define entry points
           (make-entry-method-list (send '=make-entry '=get-id 'moss::$MNAM.OF))
           ep entry-point-list fragment fragment-list start end ep-function-list
           unused-words entry-point-list-1 entry-point-list-2 unused-words-1
           unused-words-2)
      ;; index contains a list of triples, e.g. ("telephone dominique fontaine" 0 3)
      ;; allowing to know what words were used in the original list
      ;(print index)
      ;; first remove used patterns from the list of patterns to avoid testing again
      ;; we do this with a dolist to keep the order of the original list
      ;(print `("find-best-entries patterns" ,patterns))
      ;(print `("find-best-entries used-patterns" ,used-patterns))
      (dolist (item patterns)
        (if (member item used-patterns :test #'string-equal)
          (setq patterns (remove item patterns :test #'string-equal))))
      ;(print `("find-best-entries cleaned-patterns ",patterns))
      ;; get the list of actual functions computing the entry points
      (setq ep-function-list
            (remove-duplicates
             (mapcar #'car 
               (broadcast make-entry-method-list '=get-id 'moss::$FNAM))))
      ;(print `("find-best-entries / ep-function-list" ,ep-function-list))
      
      ;; compute all possible entry points for all combinations
      
      ;; cook up a list of properties corresponding to the list of the =make-entry 
      ;; keep only symbols from the current package and from moss 
      ;(setq prop-list 
      ;      (remove 
      ;       nil 
      ;       (mapcar #'(lambda (xx) (if (or (eql (symbol-package (car xx)) *package*)
      ;                                      (eql (symbol-package (car xx)) 
      ;                                           (find-package :moss)))
      ;                                (car xx)))
      ;               (broadcast make-entry-method-list '=get-id 'moss::$OMS.OF))))
      
      (catch :done
        ;; test each pattern starting with the longest one
        ;; we are in the application package (MOSS entry points are exported)
        (dolist (pattern patterns)
          ;; for each make-entry function in turn
          (dolist (fn ep-function-list)
            ;; build the symbol using the make-entry function
            (setq ep (car (funcall fn pattern)))
            ;(moss::vformat "...trying ~S as an entry-point for ~S, ep?: ~S" 
            ;               ep fn (moss::%is-entry? ep))
            (when (moss::%is-entry? ep) 
              ;(print `("ep" ,ep "pattern" ,pattern))
              (pushnew (if keep-ref (list ep (list pattern)) ep) ; JPB0910
                       entry-point-list :test #'equal)
              ;; record start and end of pattern in sentence fragment
              (setq fragment (assoc pattern index :test #'string-equal)
                    start (cadr fragment)
                    end (caddr fragment))
              (unless all (throw :done nil))))
          ;; record that we tested the pattern
          (push pattern used-patterns)))
      ;; when start and end are non nil, then we found an entry point that
      ;; matched a combination of words of word list. We
      ;; remove pattern from current word list, breaking list into fragments
      ;(print `("=====>>>> "word-list" ,word-list "start" ,start "end" ,end))
      (when (and start end)
        (setq fragment-list (list (subseq word-list 0 start)
                                  (subseq word-list end (length word-list))))
        ;(print `("sentence" ,fragment-list "used-patterns" ,used-patterns))
        ;; recurse on each fragment, preserving the order
        (multiple-value-setq (entry-point-list-1 unused-words-1)
          (find-best-entries (car fragment-list) :keep-ref keep-ref
                             :used-patterns used-patterns))
        (multiple-value-setq (entry-point-list-2 unused-words-2)
          (find-best-entries (cadr fragment-list) :keep-ref keep-ref
                             :used-patterns used-patterns))
        (setq entry-point-list 
              (append entry-point-list-1 entry-point-list entry-point-list-2)
              unused-words (append unused-words-1 unused-words-2))
        )
      ;; when start and end are both nil, then no entry point could be found
      ;; through combinations of words of word-list, we can return the unmatched 
      ;; word-list as a second value
      (unless (and start end)
        (setq unused-words word-list))
      (values entry-point-list unused-words))))

#| 
===== MCL
? (moss::find-best-entries '("quel" "est" "le" "numéro" "de" "téléphone" "du" 
                             "président" "de" "l" "utc"))
(HAS-NUMERO TELEPHONE HAS-PRESIDENT UTC)
("quel" "est" "le" "de" "du" "de" "l")

? (moss::find-best-entries '("quel" "est" "l" "adresse" "privée" "du" "président" 
                             "de" "l" "utc"))
(ADRESSE-PRIVEE HAS-PRESIDENT UTC)
("quel" "est" "l" "du" "de" "l")

? (moss::find-best-entries '("quel" "est" "le" "numéro" "de"  "portable" "du" 
                             "président" "de" "l" "utc"))
(HAS-NUMERO PORTABLE HAS-PRESIDENT UTC)
("quel" "est" "le" "de" "du" "de" "l")

? (moss::find-best-entries '("téléphone" "dominique" "barthes" "biesel"))
(TELEPHONE DOMINIQUE BARTHES-BIESEL)
NIL

? (moss::find-best-entries '("téléphone" "zorglub"))
(TELEPHONE)
("zorglub")

? (moss::path-process-word-list '("téléphone" 
    "dominique" "barthes" "biesel"))
($E-PHONE ($E-PERSON ($T-PERSON-FIRST-NAME :IS "Dominique"))
 ($E-PERSON ($T-PERSON-NAME :IS "Barthes" "Biesel")))

? (moss::find-best-entries '("quel" "est" "le" "numéro" "de"  "portable" "du" 
                             "président" "de" "l" "utc"):keep-ref t)
((HAS-NUMERO ("Numéro")) (PORTABLE ("Portable")) (HAS-PRESIDENT ("Président"))
 (UTC ("Utc")))
("quel" "est" "le" "de" "du" "de" "l")

===== ACL
SA-ADDRESS(9): (moss::find-best-entries 
                '("quel" "est" "le" "numéro" "de" "téléphone" "du" 
                             "président" "de" "l" "utc"))
(HAS-NUMÉRO HAS-TÉLÉPHONE HAS-PRÉSIDENT UTC)
("quel" "est" "le" "de" "du" "de" "l")

SA-ADDRESS(10): (moss::find-best-entries 
                 '("quel" "est" "l" "adresse" "privée" "du" "président" 
                             "de" "l" "utc"))
(HAS-ADRESSE HAS-PRÉSIDENT UTC)
("quel" "est" "l" "privée" "du" "de" "l")

SA-ADDRESS(11): (moss::find-best-entries 
                 '("quel" "est" "le" "numéro" "de"  "portable" "du" 
                             "président" "de" "l" "utc"))
(HAS-NUMÉRO HAS-PORTABLE HAS-PRÉSIDENT UTC)
("quel" "est" "le" "de" "du" "de" "l")

SA-ADDRESS(12): (moss::find-best-entries 
                 '("téléphone" "dominique" "barthès" "biesel"))
(HAS-TÉLÉPHONE DOMINIQUE BARTHÈS-BIESEL)
NIL
|#
;;;-------------------------- FIND-BEST-ENTRIES-FROM-PROPERTY-NAME-AND-CLASS-REF

(defUn find-best-entries-from-property-name-and-class-ref
       (word-list prop-name class-ref &key (max-count 5) keep-ref all used-patterns
                  keep-results prop-id)
  "tries to extract from the sentence entry points by combining the maximum ~
   number of words. When a phrase is extracted, the sentence is broken into ~
   fragments, and each fragment is processed in turn.
Arguments:
   word-list: list of words corresponding to the initial sentence
   prop-name: name of property e.g. HAS-KEYWORD
   class-ref: reference of class, e.g. \"DOMAIN\"
   max-count (key): maximal number of words to be put together to form a phrase
   keep-ref (key): keep the original list of words in the result
   keep-results (key): keeps the objects pointed to by entry point
   all (key): if true ?
   prop-id (key): id of intended property
   used-patterns (key): <used for recursion>
Return:
   2 values: a list of entry points of nil if none
             the list of words that did not contribute to entry-points."
  (when word-list
    (let (index patterns ep entry-point-list fragment fragment-list inv-id
                start end  unused-words entry-point-list-1 entry-point-list-2
                unused-words-1 unused-words-2 results own-methods)
      
      ;; first get prop-id corresponding to input
      (setq prop-id 
            (or
             prop-id
             ;(car (%get-prop-id-from-name-and-class-ref prop-name class-ref)))
             (%%get-id prop-name :prop :class-ref class-ref)) ; JPB1006 removing car
            inv-id (%inverse-property-id prop-id))
      ;; if none, five up
      (unless prop-id
        (return-from find-best-entries-from-property-name-and-class-ref nil))
      ;; if prop has no =make-entry own method, give up
      (setq own-methods (%get-value prop-id '$OMS))
      (unless (and own-methods 
                   (some #'(lambda(xx) (eql '=make-entry 
                                            (car (%get-value xx '$mnam))))
                         own-methods))
        ;(format t "~&; find-best-entries-from-property-name-and-class-ref/ ~
        ;    no make-entry method for ~S" prop-id)
        (return-from find-best-entries-from-property-name-and-class-ref nil))
      
      (setq index  (generate-access-patterns word-list :max max-count))
      ;; index contains a list of triples, e.g. ("tlphone dominique barthes" 0 3)
      ;; allowing to know what words were used in the original list
      ;(print index)
      (setq patterns (mapcar #'car index))
      ;; first remove used patterns from the list of patterns to avoid testing again
      ;; we do this with a dolist to keep the order of the original list
      ;(print `(patterns ,patterns))
      ;(print `(used-patterns , used-patterns))
      (dolist (item patterns)
        (if (member item used-patterns :test #'string-equal)
          (setq patterns (remove item patterns :test #'string-equal))))
      ;(print `(cleaned-patterns ,patterns))
      
      (catch :done
        ;; test each pattern starting with the longest one for the specific property
        (dolist (pattern patterns)
          (setq ep (car (moss::-> prop-id '=make-entry pattern)))
          ;(moss::vformat "...trying ~S as an entry-point for ~S...ep? ~S" 
          ;               ep prop-id (moss::%is-entry? ep))
          ;; keep ep that correspond to the specified property
          (when (and 
                 (moss::%is-entry? ep) 
                 ;;********** kludge should be done better
                 (setq results (%get-value ep inv-id)))
            ;(print (list "ep" ep "pattern" pattern))
            (pushnew (cond
                      ;(keep-ref (cons ep (%make-word-list pattern)))
                      (keep-ref (cons ep (list pattern))) ; JPB0910
                      (keep-results (cons ep results))
                      (t ep))
                     entry-point-list :test #'equal)
            ;; record start and end of pattern in sentence fragment
            (setq fragment (assoc pattern index :test #'string-equal)
                  start (cadr fragment)
                  end (caddr fragment))
            (unless all (throw :done nil)))
          ;; record that we tested the pattern
          (push pattern used-patterns)))
      
      ;; when start and end are non nil, then we found an entry point that
      ;; matched a combination of words of word list. We
      ;; remove pattern from current word list, breaking list into fragments
      ;(print (list '=====>>>> "word-list" word-list "start end" start end))
      (when (and start end)
        (setq fragment-list (list (subseq word-list 0 start)
                                  (subseq word-list end (length word-list))))
        ;(print (list "sentence" fragment-list "used-patterns" used-patterns))
        ;; recurse on each fragment, preserving the order
        (multiple-value-setq (entry-point-list-1 unused-words-1)
          (find-best-entries-from-property-name-and-class-ref 
           (car fragment-list) prop-name class-ref :keep-ref keep-ref
           :keep-results keep-results
           :used-patterns used-patterns :prop-id prop-id))
        (multiple-value-setq (entry-point-list-2 unused-words-2)
          (find-best-entries-from-property-name-and-class-ref 
           (cadr fragment-list) prop-name class-ref :keep-ref keep-ref
           :keep-results keep-results
           :used-patterns used-patterns :prop-id prop-id))
        (setq entry-point-list 
              (append entry-point-list-1 entry-point-list entry-point-list-2)
              unused-words (append unused-words-1 unused-words-2))
        )
      
      ;; now filter entry-point-list to remove entries not pointing on objects
      ;; of the specified class
      
      ;; when start and end are both nil, then no entry point could be found
      ;; through combinations of words of word-list, we can return the unmatched 
      ;; word-list as a second value
      (unless (and start end)
        (setq unused-words word-list))
      (values entry-point-list unused-words))))

#|
;(in-package :biblio)
(moss::find-best-entries-from-property-name-and-class-ref
 '("quel" "sont" "les" "articles" "qui" "parlent" "de" "systèmes" "multi-agents")
 'has-keyword
 "domain")
(SYSTéMES-MULTI-AGENTS)
("quel" "sont" "les" "articles" "qui" "parlent" "de")
|#
;;;------------------------------------------------------ FIND-OBJECTS-FROM-TEXT

(defun find-objects-from-text (text-or-word-list &key target)
  "takes a text and tries to access object from this text. Uses *delimiters* to ~
   break text into a word list.
Arguments:
   text-or-word-list: a string or a list of words
   target (key): concept-ref
Return:
   a set of objects or nil"
  (let (word-list query-list result)
    ;; if text is a string break it into a word list
    (setq word-list (if (stringp text-or-word-list)
                        (%get-words-from-text text-or-word-list *delimiters*)
                      text-or-word-list)
        )
    ;; remove empty phrases ??
    ;;...
    (unless word-list (return-from find-objects-from-text nil))
    
    ;; the main idea is to use locate-objects, but we need a target-concept and some
    ;; application entry point in the word list
    (if  target 
        (setq result (locate-objects word-list target)))
    ;; if result we win
    (if result (return-from find-objects-from-text result))
    
    ;;=== we need stronger stuff not garanteeing results
    (setq query-list (path-build-query-list word-list))
    ;; access everything
    (setq result 
          (loop for query in query-list 
              append (access query :already-parsed t)))
    ;; remove duplicates
    (setq result (remove-duplicates result))
    ;; return whatever is left
    result))

#|
;;;? (in-package :contact)
? (moss::find-objects-from-text "contacts avec la chine?" :target "contact")
($E-CONTACT.14 $E-CONTACT.15 $E-CONTACT.16)
? (moss::find-objects-from-text "contact avec la chine?")
($E-CONTACT.14 $E-CONTACT.15 $E-CONTACT.16)
;; next does not understand contacts, the name of concept is contact without s
? (moss::find-objects-from-text "contacts avec la chine?")
($E-COUNTRY.39)
;; next will return a list of persons having to do something with CIT
? (moss::find-objects-from-text "CIT" :target "personne")
($E-PERSON.39 $E-PERSON.18 $E-PERSON.3 $E-PERSON.27 $E-PERSON.57 $E-PERSON.26)
|#
;;;------------------------------------------------------------------ GET-ANSWER

(defun get-answer (args target &key empty-words special-words no-format
                        (format-method '=make-print-string))
  "generic function that uses the input data to a skill to access info in the agent ~
   KB, and format it, using either pattern, if in the data, or =format-method.
Arguments:
   args: an alist with properties :data/:query :pattern :language
   target: target concept
   empty-words (key): empty-word list depending on the language
   special-words (key): special words depending on the skill
   no-format (key): if t return the list of object ids
   format-method (key): method to be applied to retrieved object if no pattern
Return:
   a list of formated results to be sent back to caller."
  (let ((*language* (or (cdr (assoc :language args)) *language*))
        (pattern (cdr (assoc :pattern args)))
        (data (cdr (assoc :data args)))
        (query (cdr (assoc :query args)))
        word-list results)
    ;; should do something about :raw-data (or :text ?)
    (cond
     (data
      ;; clean the list of words from specified empty words
      (if (or empty-words special-words)
          (setq word-list
                (%clean-word-list word-list empty-words special-words)))

      ;; first locate individuals using the word list and cleaning empty words
      (setq results (find-objects-from-text data :target target)))
     (query
      ;; in that case we locate individuals by executing the query
      (setq results (access query))))
    
    (unless results 
      (return-from get-answer nil))
    
    ;; here we have results, return them formatted, unless no-format is true
    (cond
     (no-format (delete-duplicates results))
     (pattern 
      (fill-pattern (delete-duplicates results) pattern))
     ;; otherwise, put get standard PROJECT
     (t (broadcast (delete-duplicates results) format-method)))
    ;(format *debug-io* "~&+++ results: ~S" results)
    ))

;;;----------------------------------------------------------- %IS-PATH-CONCEPT?

(defUn %is-path-concept? (expr)
  "predicate to locate a concept in a path. A concept or class-frame is a list
   starting with the concept-id or a concept-id."
  (or
   (and (listp expr)
        (%is-concept? (car expr)))
   (%is-concept? expr)))

;;;=============================================================================
;;;
;;;                            Functions
;;;
;;;=============================================================================

;;; to build the formal query we start from the target class, and follow the 
;;; links using the structural properties until we hit all the classes correspond-
;;; ing to the entry-points.
;;; Starting with the target class we proceed to the next element of the access 
;;; list. It may be a milestone (class), a bridge (relation), an attribute or an 
;;; entry point.
;;;  If it is an attribute the next value should be an entry point (currently it
;;; cannot be a simple value), thus attribute are discarded.
;;;  if it is a class then we try to link the temporary paths to the class.
;;;  if it is a relation, we look to see if it is not a relation of one of the
;;; leaf classes

;;;-------------------------------------------------------- PATH-BUILD-ONE-QUERY

(defUn path-build-one-query (input-list)
  "takes a list of objects (classes, class frames, relations) and tries to build a ~
   series of queries inclusing the objects.
Argument:
   work-list: e.g. ($S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS \"utc\")))
Return:
   a list of possible formal queries."
  ;; if work-list is empty or contains a single element, return it 
  ;; (can't look for paths...)
  (unless (and input-list (cdr input-list))
    (return-from path-build-one-query input-list))
  
  (let (work-list (max-distance 5) distance path-list saved-path-list match?)
    
    ;; fuse neighbors class frames sharing the same class
    (setq work-list (path-fuse-filters input-list))
    ;(vformat "path-build-query-list /work-list:~&  ~S" work-list)
    
    ;; then we navigate from the first concept to the next item and stop
    ;; when we exhausted the work-list
    
    ;; careful! first element may be a class frame, in which case we add class
    ;; in front of the work-list
    (if (listp (car work-list))
      (setq work-list (cons (caar work-list)  work-list)))
    ;; build the starting path-list with the target class
    (setq path-list (list (list (list (pop work-list)))))
    (vformat "path-build-query-list /path-list:~&  ~S" path-list)
    ;;=== span through the neighbors
    ;; the spanning tree is recorded as a list of inverse paths from the root to
    ;; simplify processing/ It will be reconstructed later
    ;; e.g. ((($E-ADDRESS) $S-ADDRESS ($E-PERSON))
    ;;       (($E-PERSON) $S-BROTHER ($E-PERSON)) ...)
    (dolist (item work-list)
      (catch 
        :next
        ;(vformat "pbql /=== loop on item: ~S." item)
        ;; put a max distance for interpolation
        (setq distance 0
              saved-path-list path-list)
        ;; we check the mean length of the paths to avoid looping
        (if (> (/ (reduce #'+ (mapcar #'length path-list)) (length path-list))
               15)
          (return-from path-build-one-query nil))
        ;; we move forward until we meet item
        (loop
          ;;=== test
          (cond
           ;; if we did not encounter the item then skip it
           ((> distance max-distance)
            ;; reset path list
            (setq path-list saved-path-list)
            (throw :next :too-far)
            )
           ;; if item is a class, then we compare the class with the leaf classes of
           ;; the spanning tree and keep the branches that agree
           ((%is-concept? item)
            ;(vformat "pbql /item is concept.")
            ;; check for same concept
            (multiple-value-setq (match? path-list)
              (path-check-class item path-list))
            ;(vformat "pbql /item: ~S, match?: ~S, path-list:~&  ~S" 
            ;         item match? path-list)
            ;; if check, then get out of the loop
            (if match? (throw :next :OK))
            )
           
           ;; if item is a relation, then we try to move forward, checking if the
           ;; relation is part of the relations of the leaf classes
           ((%is-relation? item)
            ;(vformat "pbql /item is relation.")
            (multiple-value-setq (match? path-list)
              (path-check-relation item path-list))
            ;(vformat "pbql /item: ~S, match?: ~S, path-list:~&  ~S" 
            ;         item match? path-list)
            ;; if check, then get out of the loop
            (if match? (throw :next :OK)))
           
           ;; if item is an entry frame, then we compare the class as for 1
           ;; otherwise we move forward 1 step
           ((listp item)
            ;(vformat "pbql /item is an entry-frame.")
            (multiple-value-setq (match? path-list)
              (path-check-entry item path-list))
            ;; if check, then get out of the loop
            ;(vformat "pbql /item: ~S, match?: ~S, path-list:~&  ~S" 
            ;         item match? path-list)
            (if match? (throw :next :OK)))
           
           (t (error "unknown item ~S in access list ~&  ~S" item work-list)))
          
          ;; if no test worked, then move one step forward
          ;; but we must eventually comsume the item
          ;; the rationale here is that if we find some classes that have the item
          ;; as a relation, then we forget about all the paths that do not contain
          ;; this relation. Otherwise, we fan out paths to all possible neighbors
          (setq path-list 
                (path-step path-list (if (%is-relation? item) (list item))))
          (incf distance)
          ;(vformat "pbql /no match, moved one step forward; item: ~S path-list:~&  ~S"
          ;         item path-list)
          )))
    
    ;; here we reached the end of the work-list with possibly several distinct paths
    ;; we need to build the correponding formal queries
    (when path-list
      (path-process-results path-list))))

#|
(moss::path-build-one-query 
 '($E-PERSON $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "utc"))))
(($E-PERSON
  ($S-TEACHING-ORGANIZATION-PRESIDENT.OF (> 0)
   ($E-TEACHING-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "utc")))))
|#
;;;------------------------------------------------------- PATH-BUILD-QUERY-LIST
;;;
;;; should also return  milestones, i.e. classes that we must go through outside
;;; the first target one
;;; One question: is the direction of the crossing of a relation important ?

;;; problem: if the work-list starts with a relation, then we are missing the
;;; target class and build the wrong query. e.g. ("président" "utc") will not
;;; infer the missing class "personne"
;;; may be we should use a small expert system to deal with such issues, since
;;; it is hard to infer the missing class from the generic property (in fact
;;; not so hard: we take all sub-properties's predecessors)

(defUn path-build-query-list (word-list)
  "tries to build a formal query from a list of words, by first getting the ~
   entry points corresponding to combinations of the words, then using them ~
   to navigate in the ontology for building a query. We assume the list of ~
   words has been pre-processed to resolve references, eventually synonyms.
Arguments:
   word-list: list of words
Return:
   a list of possible formal parsed queries or nil if failure."
  (let (work-list path-list)
    ;; first process the input to build a working list of concepts, relations and
    ;; entry frames
    ;; e.g. ($E-CELL-PHONE $S-PRESIDENT 
    ;;                     ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
    (setq work-list (path-process-word-list word-list))
    ;;=== special cases
    ;; e.g. single value (no path) can be entry point
    ;; contains no class but at least a relation, ...
    (if (setq path-list (path-build-query-special-cases work-list))
      (return-from path-build-query-list path-list))
    
    ;; pop the work list to the first class, careful class maybe in a class-frame
    (setq work-list (member-if #'%is-path-concept? work-list))
    ;(print `("path-build-query-list work-list" ,work-list))
    ;; if empty, then quit
    (unless work-list (return-from path-build-query-list nil))
    ;; if it contains a single element, return it (can't look for paths...
    (unless (cdr work-list)(return-from path-build-query-list work-list))
    
    ;; now go process work-list
    (path-build-one-query work-list)))

#|
;;;=== MCL
? (moss::path-build-query-list '( "utc"))
(($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))

? (moss::path-build-query-list '( "personne"))
(($E-PERSON))

? (moss::path-build-query-list '("téléphone" "utc"))
(($E-PHONE
  ($S-ORGANIZATION-PHONE.OF (> 0)
       ($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))

? (moss::path-build-query-list '("personne" "président" "utc"))
(($E-PERSON
  ($S-TEACHING-ORGANIZATION-PRESIDENT.OF (> 0)
   ($E-TEACHING-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))

? (moss::path-build-query-list '("téléphone" "président" "utc"))
(($E-PHONE
  ($S-PERSON-PHONE.OF (> 0)
   ($E-PERSON
    ($S-TEACHING-ORGANIZATION-PRESIDENT.OF 
       (> 0)
         ($E-TEACHING-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))))
;;;=== ACL
SA-ADDRESS(27): (moss::path-build-query-list '("personne" "président" "utc"))
(($E-PERSON
  ($S-TEACHING-ORGANIZATION-PRESIDENT.OF (> 0)
   ($E-TEACHING-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))
|#
;;;---------------------------------------------- PATH-BUILD-QUERY-SPECIAL-CASES

(defUn path-build-query-special-cases (work-list)
  "processes all special cases when building a query. E.g. single class, single ~
   entry point, no class but relation,...
Arguments:
   work-list: a list of moss objects
Return:
   the query or nil if not a special case."
  (let (entity-list result)
    (cond
     ;; single object that is a class
     ((and (null (cdr work-list))
           (%is-concept? (car work-list)))
      (list work-list))
     ;; single-object entry point (appears as a list)
     ;; e.g. (($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "utc")))
     ((and (null (cdr work-list))
           (listp (car work-list)))
      (list (car work-list)))
     ;; single object relation (to be done)
     ((%is-relation? (car work-list))
      ;; collect all relations with that name from the generic property
      (setq entity-list (%get-value (car work-list) '$IS-A.OF))
      ;; get all predecessor classes using $SUC (mapcan builds a circular list...
      (dolist (item entity-list)
        (setq result (append result (%get-value item '$SUC))))
      ;; remove *any* 
      (setq result (delete-duplicates (remove '*any* result)))
      
      ;; we have a list of candidate classes to put in front of property
      (reduce ;  jpb 140820 removing mapcan
       #'append
       (mapcar #'(lambda(xx) (path-build-one-query (cons xx  work-list))) result))
      )
     ;; otherwise return nil
     )))

#|
(step (moss::path-build-query-special-cases 
 '($S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "utc")))))

(moss::path-build-query-special-cases 
 '(($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "utc"))))
(($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "utc")))
|#
;;;------------------------------------------------------------ PATH-CHECK-CLASS

(defUn path-check-class (class-id path-list)
  "checks whether the class-id is one of the leaf classes of the path-list. ~
   If so, keeps the corresponding branch and tell the caller.
Arguments:
   class-id: identifier of class to be checked
   path-list: a list of paths in reverse order to the root
Return:
   2 values: t if there was a match and the eventually modified path-list."
  (let (results match?)
    (dolist (path path-list)
      (when (and
             (%is-path-concept? (car path))  ; play it safe (class or class-frame)
             ;; if we specify "person" we want a path that goes through any 
             ;; possible subclass of "person"
             (or (member (caar path) (%ancestors class-id))
                 (member class-id (%ancestors (caar path)))))
        (setq match? t)
        (push path results)))
    (values match? (if match? (reverse results) path-list))))

#|
?  (moss::path-check-class 
   '$E-PERSON
   '((($E-Student)) (($E-person))))
T
((($E-STUDENT)) (($E-PERSON)))

? (moss::path-check-class 
   '$E-STUDENT
   '((($E-Student)) (($E-person))))
T
((($E-STUDENT)))

? (moss::path-check-class 
   '$E-STUDENT
   '((($E-Student)) (($E-UNIVERSITY))))
T
((($E-STUDENT)))

? (moss::path-check-class 
   '$E-STUDENT
   '((($E-organization)) (($E-UNIVERSITY))))
NIL
((($E-ORGANIZATION)) (($E-UNIVERSITY)))
|#
;;;------------------------------------------------------------ PATH-CHECK-ENTRY
;;; this guy needs to be improved
;;; It is assumed that the leaves or the path-list represent classes, which is
;;; not true all the time.
;;; In particular when a filter is introduced in a path the corresponding leaf
;;; looks like this:
;;;   (($T-ORGANIZATION-ACRONYM :IS "Utc") $E-UNIVERSITY)
;;; in that case the class is the first symbol upstream, after skiping all the
;;; recorded filters.
;;; There should be a better and simpler way to do that since filters are not 
;;; essential in determining the path but are associated with classes.
;;; One possible way would be to record classes as a list (class filter*)
;;; Hence a paht would look like this
;;;   (($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")) 
;;;                                 $S-PRESIDENT.OF ($E-PERSON))

(defUn path-check-entry (ep-frame path-list)
  "checks whether one of the leaf classes of the path-list contains class of the ~
   entry frame. If so, keeps the corresponding branch and merge the entry-frame.
Argments:
   ep-frame: something like ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS \"Utc\"))
   path-list: a list of paths in reverse order to the root
Return:
   2 values: t if there was a match and the eventually modified path-list."
  (let (match? results)
    (dolist (path path-list)
      (when (and
             (%is-path-concept? (car path))  ; play it safe
             (or
              (eql (car ep-frame) (caar path))
              (member (car ep-frame) (%get-subclasses (caar path)))))
        (setq match? t)
        ;; insert the new filter into the path
        (push (cons 
               (cons (caar path)             ; class
                     (append (cdr ep-frame)  ; new filters
                             (cdar path)))     ; old filters or nil
               (cdr path)) 
              results)
        ))
    (values match? (if match? (reverse results) path-list))))

#|
;; ("organization" "UTC") => ($ORGANIZATION ($T-ORGANIZATION-ACRONYM :is "UTC"))
? (moss::path-check-entry 
   '($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))
   '((($E-Student)) (($E-ORGANIZATION))))
T
((($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc"))))

? (moss::path-check-entry 
   '($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))
   '((($E-Student)) (($E-university))))
T
((($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))))

? (moss::path-check-entry 
   '($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc"))
   '((($E-Student)) (($E-university ($T-UNIVERSITY-FAME :is "high")))))
T
((($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")
   ($T-UNIVERSITY-FAME :IS "high"))))
|#
;;;--------------------------------------------------------- PATH-CHECK-RELATION

(defUn path-check-relation (rel-id path-list)
  "checks whether one of the leaf classes of the path-list contains the relation. ~
   If so, keeps the corresponding branch expand the paths including the new ~
   relation and all successor classes, creating a new path for each of them.
Arguments:
   rel-id: identifier of relation to be checked
   path-list: a list of paths in reverse order to the root
Return:
   2 values: t if there was a match and the eventually modified path-list."
  (let ((possible-relations (%get-subclasses rel-id))
        results relation-list rel-list match?)
    (dolist (path path-list)
      ;; get all relations
      (setq relation-list  (%%get-all-class-relations (caar path)))
      (when (setq rel-list (intersection possible-relations relation-list))
        (setq match? t)
        (setq results (append results (path-make-all path rel-list)))
        )
      ;; now try inverse relations
      (setq relation-list (%%get-all-class-inverse-relations (caar path)))
      (when (setq rel-list (intersection 
                            (mapcar #'%inverse-property-id possible-relations)
                            relation-list))
        (setq match? t)
        (setq results (append results 
                              (path-make-all path rel-list)))
        )
      )
    (values match? (if match? (reverse results) path-list))))

#|
? (moss::path-check-relation 
   '$S-WIFE
   '((($E-Student)) (($E-teacher))))
T
((($E-PERSON) $S-PERSON-WIFE.OF ($E-STUDENT))
 (($E-PERSON) $S-PERSON-WIFE ($E-STUDENT)))

? (moss::path-check-relation 
   '$S-president
   '((($E-Student)) (($E-address))))
T
((($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-STUDENT))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-STUDENT)))

? (moss::path-check-relation 
   '$S-president
   '((($E-Student)) (($E-person))))
T
((($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-PERSON))
 (($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-STUDENT))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-STUDENT)))

? (moss::path-check-relation 
   '$S-president
   '((($E-phone)) (($E-address))))
NIL
((($E-PHONE)) (($E-ADDRESS)))
|#
;;;----------------------------------------------------------- PATH-FUSE-FILTERS

(defUn path-fuse-filters (work-list)
  "takes a list containing class-frames and fuses two adjacent class frames sharing ~
   the same class.
Argument:
   work-list: list to process
Return:
   fused work list."
  (cond 
   ((null work-list) nil)
   ((and (listp (car work-list))
         (listp (cadr work-list))
         (eql (caar work-list)(caadr work-list)))
    (path-fuse-filters
     (cons
      (cons (caar work-list)
            (append (cdar work-list)(cdadr work-list)))
      (cddr work-list))))
   (t 
    (cons (car work-list) (path-fuse-filters (cdr work-list))))
   ))

#|
? (moss::path-fuse-filters '((a 1) (b 2) (b 3) (c 4)))
((A 1) (B 2 3) (C 4))
? (moss::path-fuse-filters '((a 1) (b 2) (b 3) (b 5) (c 4) (b 6)))
((A 1) (B 2 3 5) (C 4) (B 6))
|#
;;;------------------------------------------- PATH-GET-CLASS-LINK-AND-NEIGHBORS

(defUn path-get-class-links-and-neighbors (class-id)
  "gets a list of pairs (relation neighbor) or (inverse-link neighbor) for all the ~
   relations and inverse links of the class and its children.
Argument:
   class-id: if
Return:
   a list of pairs  (property class-id)."
  (let (properties results neighbors)
    ;; get all the relations for a given class including the inherited ones and
    ;; the ones from the subclasses
    (setq properties (delete-duplicates 
                      (append
                       (%%get-all-class-relations class-id)
                       (%%get-all-subclass-relations class-id))))
    ;; each relation has a $suc property, get the successor classes and for
    ;; each class build a pair (relation class)
    (dolist (prop-id properties)
      (setq neighbors (%get-value prop-id '$SUC))
      (dolist (class-id neighbors)
        ;; add the pair to the results
        (push (list prop-id class-id) results)
        ))
    ;; do the same with inverse relations, but use direct relation and $PT.OF
    (setq properties (delete-duplicates 
                      (append
                       (%%get-all-class-inverse-relations class-id)
                       (%%get-all-subclass-inverse-relations class-id))))
    ;; property to access neighbor classes
    (dolist (prop-id properties)
      (setq neighbors (%get-value (%inverse-property-id prop-id)
                                  (%inverse-property-id '$PS)))
      (dolist (class-id neighbors)
        ;; add the pair to the results
        (push (list prop-id class-id) results)
        ))
    ;; delete duplicated pairs from the results and return the list
    (delete-duplicates results :test #'equal)
    ))

#|
SA-ADDRESS(42): (moss::path-get-class-links-and-neighbors '$e-person)
(($S-MEETING-ORGANIZER.OF $E-MEETING) ($S-MEETING-PRESIDENT-OF-THE-PC.OF $E-MEETING)
 ($S-NON-PROFIT-ORGANIZATION-EXECUTIVE-COMMITTEE.OF $E-NON-PROFIT-ORGANIZATION)
 ($S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF $E-NON-PROFIT-ORGANIZATION)
 ($S-TEACHING-ORGANIZATION-PRESIDENT.OF $E-TEACHING-ORGANIZATION)
 ($S-PERSON-MOTHER.OF $E-PERSON) ($S-PERSON-WIFE.OF $E-PERSON)
 ($S-PERSON-HUSPAND.OF $E-PERSON) ($S-STUDENT-SCHOOL $E-UNIVERSITY)
 ($S-PERSON-MOTHER $E-PERSON) ($S-PERSON-WIFE $E-PERSON) ($S-PERSON-HUSPAND $E-PERSON)
 ($S-PERSON-WEB-PAGE $E-WEB-ADDRESS) ($S-PERSON-EMAIL $E-EMAIL)
 ($S-PERSON-PHONE $E-PHONE) ($S-PERSON-ADDRESS $E-POSTAL-ADDRESS)) 

BIBLIO(46): (moss::path-get-class-links-and-neighbors '$e-publication)
(($S-ARTICLE-JOURNAL $E-JOURNAL) ($S-MANUAL-ORGANIZATION $E-ORGANIZATION)
 ($S-MANUAL-PUBLISHER $E-PUBLISHER) ($S-PROCEEDINGS-PUBLISHER $E-ORGANIZATION)
 ($S-PROCEEDINGS-EDITOR $E-PERSON) ($S-PROCEEDINGS-CONFERENCE $E-CONFERENCE)
 ($S-INPROCEEDINGS-EDITOR $E-PERSON) ($S-INPROCEEDINGS-CONFERENCE $E-CONFERENCE)
 ($S-BOOK-ADDRESS $E-ADDRESS) ($S-BOOK-PUBLISHER $E-ORGANIZATION)
 ($S-PUBLICATION-DOMAIN $E-THEME) ($S-PUBLICATION-AUTHOR $E-PERSON))
|#
;;;------------------------------------------------------- PATH-GET-ENTRY-POINTS
;;; **********
;;; we could have entry points that are the same as class names or property names
;;; OK fixed

(defUn path-get-entry-points (ep-list)
  "looks into a list of entry points for entry points that are not for classes ~
   nor for properties.
Arguments:
   ep-list: entry-point list
Return:
   a list of what is left."
  (let ((pair-list 
         (reduce  ;  jpb 140820 removing mapcan
          #'append
          (mapcar #'(lambda(xx)
                      (moss::%%get-objects-from-entry-point xx :keep-entry t))
            ep-list))))
    ;; remove class names and property names
    (reduce  ;  JPB 140820 removing mapcan
     #'append
     (mapcar #'(lambda (xx) 
                 (unless (or (eql (cadr xx) (%inverse-property-id '$ENAM))
                             (eql (cadr xx) (%inverse-property-id '$PNAM)))
                             (list xx)))
      pair-list))))

#|
? (moss::path-get-entry-points '(TELEPHONE HAS-TELEPHONE UTC))
((UTC $T-ORGANIZATION-ACRONYM.OF $E-UNIVERSITY.1))
|#
;;;---------------------------------------------------------- PATH-GET-RELATIONS

(defUn path-get-relations (ep-list)
  "looks into a list of entry points for relations.
Arguments:
   ep-list: entry-point list
Return:
   a pair (<ep> . <class-id>) or nil on failure."
  (let (property-list result)
    (dolist (ep ep-list)
      ;; look for $PNAM.OF
      (setq property-list (%get-value ep (%inverse-property-id '$PNAM)))
      ;; must keep only relations, removing attributes
      (setq property-list 
            (remove nil 
                    (mapcar #'(lambda (xx) (if (%is-relation? xx) xx))
                            property-list)))
      (setq result (append result property-list))
      )
    result))

#|
? (moss::path-get-relations '(TELEPHONE HAS-TELEPHONE UTC))
($S-PHONE $S-PERSON-PHONE $S-ORGANIZATION-PHONE)
|#
;;;------------------------------------------------------- PATH-GET-TARGET-CLASS

(defUn path-get-target-class (ep-list &aux class-id)
  "looks into a list of entry points for the first one that is the name of a class.
Arguments:
   ep-list: entry-point list
Return:
   a pair (<ep> . <class-id>) or nil on failure."
  (dolist (ep ep-list)
    ;; look for $ENAM.OF
    (setq class-id (%get-value ep (%inverse-property-id '$ENAM)))
    (if class-id (return-from path-get-target-class (cons ep (car class-id))))))

#|
? (moss::path-get-target-class '(TELEPHONE HAS-TELEPHONE UTC))
(TELEPHONE . $E-PHONE)
|#
;;;------------------------------------------------------------------- PATH-MAKE

(defUn path-make (path rel)
  "takes a path and adds a relation with successors. If several successors, then ~
   splits the path into as many paths as successor classes.
Arguments:
   path: a path, e.g. (($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
   rel: relation, e.g. $S-COUNTRY
Result:
   a list of paths e.g. 
       ((($E-COUNTRY) $S-COUNTRY ($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
        (($E-REPUBLIC) $S-COUNTRY ($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON)))."
  (let ((successors (if (%is-relation? rel) 
                      (%get-value rel '$SUC)
                      (%get-value (%inverse-property-id rel) 
                                  (%inverse-property-id '$PS))))
        result)
    ;; for each successor build a new path
    (dolist (suc successors)
      (push (list* (list suc) rel path) result))
    ;; return result
    (reverse result)))

#|
;? (in-package :sa-address)
;#<Package "SA-ADDRESS">
? (moss::path-make '(($E-PERSON)) '$S-person-wife)
((($E-PERSON) $S-PERSON-WIFE ($E-PERSON)))
? (moss::path-make '(($E-PERSON)) '$S-TEACHING-ORGANIZATION-PRESIDENT.of)
((($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-PERSON)))
|#
;;;--------------------------------------------------------------- PATH-MAKE-ALL

(defUn path-make-all (path rel-list)
  "combines path-make for several relations.
Arguments:
   path: a path, e.g. (($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
   rel-list: a list of relations and/or inverse-relations, e.g. ($S-COUNTRY.OF)
Result:
   a list of paths e.g. 
       ((($E-COUNTRY) $S-COUNTRY ($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
        (($E-REPUBLIC) $S-COUNTRY ($E-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON)))."
  (reduce  ;jpb 140820 removing mapcan
   #'append
   (mapcar #'(lambda (xx) (path-make path xx)) rel-list)))

#|
;? (in-package :sa-address)
? (moss::path-make-all '(($E-PERSON)) '($S-person-wife $S-person-mother.of))
((($E-PERSON) $S-PERSON-WIFE ($E-PERSON))
 (($E-PERSON) $S-PERSON-MOTHER.OF ($E-PERSON)))

? (moss::path-make-all '(($E-PERSON)) 
                       '($S-person-wife $S-NON-PROFIT-ORGANIZATION-PRESIDENT.of))
((($E-PERSON) $S-PERSON-WIFE ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-PERSON)))
|#
;;;-------------------------------------------------------- PATH-PROCESS-RESULTS

(defUn path-process-results (path-list)
  "turns the path lists into a series of formal queries"
  (mapcar #'(lambda (xx) (path-process-single-result (reverse xx)))
          path-list))

#|
? (moss::path-process-results
    '((($E-PERSON) $S-PERSON-WIFE ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-PERSON)))
  )
(($E-PERSON ($S-PERSON-WIFE (> 0) ($E-PERSON)))
 ($E-PERSON
  ($S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF (> 0) ($E-NON-PROFIT-ORGANIZATION)))) 
|#
;;;-------------------------------------------------- PATH-PROCESS-SINGLE-RESULT

(defUn path-process-single-result (path)
  "takes the internal representation of a path and builds a formal query.
   the path alternates class frames and relations.
Arguments:
   path: the internal format for the path
   query, the partially built query
return:
   a full blown query."
  (cond
   ;; finished?
   ((null path) nil)
   ((null (cdr path)) (car path))
   ;; normally we should have a relation here
   (t (append (car path) 
              (list (list (cadr path) '(> 0)
                          (path-process-single-result (cddr path))))))))

#|
? (path-process-single-result
     '(($e-person)))
($E-PERSON)

? (moss::path-process-single-result
   '(($e-person ($t-PERSON-NAME :is "Barthes"))))
($E-PERSON ($T-PERSON-NAME :IS "Barthes"))

? (moss::path-process-single-result
   '(($e-person ($t-PERSON-NAME :is "Barthes")) 
     $s-is-president-of ($e-organization)))
($E-PERSON ($T-PERSON-NAME :IS "Barthes")
 ($S-IS-PRESIDENT-OF (> 0) ($E-ORGANIZATION)))
|#
;;;---------------------------------------------------- PATH-PROCESS-SUBSUMPTION
;;; when building a list containing classes, relations, and triples, we can have
;;; the case where a class is followed by a triple in which the first element
;;; is a class that subsumes the preceding one, e.g.
;;;   ($E-THESIS ($E-PROJECT ($T-PROJECT-START :IS "2010")))
;;; Here $E-THESIS is a project and thus is subsumed by $E-PROJECT. We must 
;;; then collapse the expression into
;;;   (($E-THESIS ($T-PROJECT-START :IS "2010")))
;;; since $E-THESIS is more restricted than $E-PROJECT
;;; This can be more complex as:
;;;   ($E-THESIS ($E-PROJECT ($T-PROJECT-START :IS "2010"))
;;;              ($E-PROJECT ($T-PROJECT-END :IS "2012")))
;;; which should lead to a single clause:
;;;   (($E-THESIS ($T-PROJECT-START :IS "2010")
;;;               ($E-PROJECT ($T-PROJECT-END :IS "2012")))

(defun path-process-subsumption (work-list)
  "examines a list containing class ids, generic relation ids and entry-point ~
   patterns and restrict an entry point pattern if it is preceded by a more ~
   restrictive class.
Argument:
   work-list: e.g. ($E-THESIS ($E-PROJECT ($T-PROJECT-START :IS \"2010\")))
Return:
   condensed list, e.g. (($E-THESIS ($T-PROJECT-START :IS \"2010\")))"
  (cond
   ((null work-list) nil)
   ;; if first item is a class, test if next item is an entry-point pattern
   ;; and if class subsumes the first item
   ((and (%is-concept? (car work-list)) ; item is a class
         (cadr work-list)               ; next item is not nil
         (listp (cadr work-list))       ; next item is a list
         (%is-a? (car work-list)(caadr work-list)))  ; class is subsumed
    (path-process-subsumption 
     (cons 
      (cons (car work-list)(cdadr work-list)) 
      (cddr work-list)))
    )
   ;; if item is an ep-pattern, followed by another ep-pattern, and if class of 
   ;; the first ep-pattern is subsumed by the class of the second one, we
   ;; merge the second ep-pattern condition with the first one
   ((and (listp (car work-list))
         (cadr work-list)
         (listp (cadr work-list))
         (%is-a? (caar work-list) (caadr work-list)))
    (path-process-subsumption
     ;; merge the conditions
     `((,(caar work-list)
         ,@(append (cdar work-list) (cdadr work-list)))
         ,@(cddr work-list))))   
   ;; otherwise skip it 
   (t (cons (car work-list)(path-process-subsumption (cdr work-list))))))

#|
(moss::path-process-subsumption 
 '($E-THESIS ($E-PROJECT ($T-PROJECT-START :IS "2010"))))
(($E-THESIS ($T-PROJECT-START :IS "2010")))
(moss::path-process-subsumption 
 '($E-THESIS ($E-PROJECT ($T-PROJECT-START :IS "2010"))
             ($E-PROJECT ($T-PROJECT-END :IS "20102"))))
(($E-THESIS ($T-PROJECT-START :IS "2010") ($T-PROJECT-END :IS "20102")))
(moss::path-process-subsumption 
 '($E-THESIS ($E-PROJECT ($T-PROJECT-START :IS "2010"))
             ($E-PROJECT ($T-PROJECT-END :IS "2012"))
             ($E-PROJECT ($T-PROJECT-STATUS :is "terminé"))))
(($E-THESIS ($T-PROJECT-START :IS "2010") ($T-PROJECT-END :IS "20102")
            ($T-PROJECT-STATUS :IS "terminé")))
|#
;;;------------------------------------------------------ PATH-PROCESS-WORD-LIST
;;; one of the problems with that function is the extraction of triples from
;;; an entry point. If an entry is common to several properties, the function
;;; collects the triples <entry inverse-property class> but retains only the first
;;; one to select the inverse property, which may be wrong. One way to improve
;;; the selection is to use the attribute corresponding to the triple

(defUn path-process-word-list (word-list)
  "transforms the list into a sequence containing class ids, generic relation ids ~
   entry-point patterns (class-id <filter>), to be processed by the access ~
   function.
Arguments:
   word-list: list of words representing a sentence
Return:
   2 values: a list of ids (entry-points, class-ids, prop-ids)
   e.g. ($E-HOME-ADDRESS $S-PRESIDENT 
                        ($UNIVERSITY ($T-UNIVERSITY-ACRONYM :IS \"UTC\")))
             a list of unused words."
  (let (index-list results class-id prop-id triple-list unused-words attribute-list
                   att triple)
    ;; first get the list of entry points with references
    (multiple-value-setq (index-list unused-words)
      (find-best-entries word-list :keep-ref t))
    ;(print `(+++++ index-list ,index-list unused-words ,unused-words))
    ;; if empty, then quit
    (unless index-list (return-from path-process-word-list nil))
    ;; now process each entry in turn to replace by class-id, generic property-id
    ;; or entry-point frame
    (dolist (entry index-list)
      (cond
       ;; priority is given to classes 
       ;;********** we do not handle multiple classes
       ((setq class-id (car (%get-value (car entry) (%inverse-property-id '$ENAM))))
        (push class-id results))
       
       ;; then relations
       ;((and (setq prop-id (car (%get-value (car entry) (%inverse-property-id '$PNAM))))
       ((and (setq prop-id (%%get-id (car entry) :prop))
             (%is-relation? prop-id))
        (push prop-id results))
       ;; we do not discard attributes since they may be used to
       ;; filter the entry point in case an entry point is used for several 
       ;; properties (like end-date and start-date)
       (prop-id
        (push prop-id attribute-list))
       
       ;; must be entry point for some instance
       (t  
        ;; get triples for entry points
        ;;********** we assume the inverse property and class is the same
        ;; e.g. we do not handle the case when DUPUIS is both a person and a firm
        ;; nor instances belonging to several classes
        ;; (UTC $T-ORGANIZATION-ACRONYM.OF $E-UNIVERSITY.1)
        ;; the following function gets a list of triples <entry inverse-prop instance>
        ;; we check if among the inverse properties there is one that we already
        ;; know
        (setq triple-list
              (%%get-objects-from-entry-point (car entry) :keep-entry t))
        ;; if we cannot find any triple, e.g. when an entry point corresponds to
        ;; an object that was deleted, then quit
        (unless triple-list
          (return-from path-process-word-list (values nil unused-words)))
        
        ;; try to find an attribute corresponding to one saved in the attribute list
        (catch :done
               (dolist (triple triple-list)
                 (setq att (%inverse-property-id (cadr triple)))
                 ;; attribute-list contains generic properties, att is a local prop
                 (when (member att attribute-list :test #'%is-a?)
                   ;; OK we use this attribute
                   (push (list (car (%type-of (caddr triple)))
                               (list* att :IS (cadr entry)))
                         results)
                   ;; quit do list
                   (throw :done t)))
               ;; otherwise take first triple
               (setq triple (car triple-list))
               (push 
                (list (car (%type-of (caddr triple)))
                      ;; filter
                      (list* 
                       (%inverse-property-id (cadr triple))
                       ':IS
                       (cadr entry)))
                results)))
       ))
    ;; collapse subsumed classes and entry-point patterns
    (setq results (path-process-subsumption (reverse results)))
    ;; return list
    (values results unused-words)))

#|
===== MCL
? (moss::path-process-word-list 
   '("quelle" "est" "l""adresse" "privée" "du" "président" "de" "l" "utc"))
($E-HOME-ADDRESS $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
("quelle" "est" "l" "du" "de" "l")

? (moss::path-process-word-list 
   '("quel" "est" "le" "numéro" "de" "téléphone" "du" "président" "de" "l" "utc"))
($E-PHONE $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
("quel" "est" "le" "de" "du" "de" "l")

? (moss::path-process-word-list 
   '("quel" "est" "le" "numéro" "de" "portable" "du" "président" "de" "l" "utc"))
($E-CELL-PHONE $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
("quel" "est" "le" "de" "du" "de" "l")

===== ACL
SA-ADDRESS(6): (moss::path-process-word-list 
   '("quel" "est" "le" "numéro" "de" "téléphone" "du" "président" "de" "l" "utc"))
($E-PHONE $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
 
SA-ADDRESS(7): (moss::path-process-word-list 
   '("quel" "est" "le" "numéro" "de" "portable" "du" "président" "de" "l" "utc"))
($E-CELL-PHONE $S-PRESIDENT ($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))

? (moss::path-process-word-list 
   '("quel" "est" "le" "numéro" "de" "portable" "de" "Zorglub"))
((MOSS::$EPT (MOSS::$PNAM :IS "Numéro")) $E-CELL-PHONE)
("quel" "est" "le" "de" "de" "Zorglub")

|#
;;;------------------------------------------------------------------- PATH-STEP
;;;
;;; moving one step away means that we use current class and all inherited 
;;; relations and inverse relations, all subclasses and proper relations and 
;;; inverse relations

(defUn path-step (path-list bridges)
  "takes a set of paths, a list of bridges and moves ~
   one step away at the leaf level.
Arguments:
   path-list: a list of paths modeling the spanning tree
   bridges; a list of relations
Return:
   2 values: the updated path-list, and the updated results."
  (let (link-and-class-list special-list results)
    ;; for each path
    (dolist (path path-list)
      ;; compute the list of relations and inverse-relations of the leaf class
      ;; returns a list of pairs (relation successor-class)
      (setq link-and-class-list (path-get-class-links-and-neighbors (caar path)))
      ;; check if some of the relations belong to the bridges
      (if (intersection bridges (mapcar #'car link-and-class-list))
        ;; then we link only those and save the path into a special list
        (dolist (link-and-class link-and-class-list)
          (if (member (car link-and-class) bridges)
            (push (cons (cdr link-and-class)
                        (cons (car link-and-class)
                              path))
                  special-list)))        
        ;; otherwise we build all posssible paths (they will be discarded if
        ;; the special list is non empty)
        (dolist (link-and-class link-and-class-list)
          (push (cons (cdr link-and-class)
                      (cons (car link-and-class)
                            path))  
                results)))
      )
    ;; if the special-list is non empty return that one, otherwise results
    (or special-list results)))


#|
? (moss::path-step '((($e-person))) 
                   '(($S-PERSON-WEB-PAGE) $S-TEACHING-ORGANIZATION-PRESIDENT.OF))
((($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-PERSON)))

? (moss::path-step '((($e-person))) nil)
((($E-POSTAL-ADDRESS) $S-PERSON-ADDRESS ($E-PERSON))
 (($E-PHONE) $S-PERSON-PHONE ($E-PERSON)) (($E-EMAIL) $S-PERSON-EMAIL ($E-PERSON))
 (($E-WEB-ADDRESS) $S-PERSON-WEB-PAGE ($E-PERSON))
 (($E-PERSON) $S-PERSON-HUSPAND ($E-PERSON)) (($E-PERSON) $S-PERSON-WIFE ($E-PERSON))
 (($E-PERSON) $S-PERSON-MOTHER ($E-PERSON))
 (($E-UNIVERSITY) $S-STUDENT-SCHOOL ($E-PERSON))
 (($E-PERSON) $S-PERSON-HUSPAND.OF ($E-PERSON))
 (($E-PERSON) $S-PERSON-WIFE.OF ($E-PERSON))
 (($E-PERSON) $S-PERSON-MOTHER.OF ($E-PERSON))
 (($E-TEACHING-ORGANIZATION) $S-TEACHING-ORGANIZATION-PRESIDENT.OF ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-PRESIDENT.OF ($E-PERSON))
 (($E-NON-PROFIT-ORGANIZATION) $S-NON-PROFIT-ORGANIZATION-EXECUTIVE-COMMITTEE.OF
  ($E-PERSON))
 (($E-MEETING) $S-MEETING-PRESIDENT-OF-THE-PC.OF ($E-PERSON))
 (($E-MEETING) $S-MEETING-ORGANIZER.OF ($E-PERSON))) 

;; example with inverse properties
? (moss::path-step '((($e-cell-phone))) ())
((($E-PERSON) $S-PERSON-PHONE ($E-CELL-PHONE))
 (($E-ORGANIZATION) $S-ORGANIZATION-PHONE ($E-CELL-PHONE)))
|#

;;;=============================================================================
;;;
;;;     ACCESS FROM WORD LIST TAKING RELATIONS AND CONCEPTS INTO ACCOUNT
;;;
;;;=============================================================================

;;; This addresses the issue of requests like:
;;;   Quel est le numéro de téléphone du président de l'université de Compiègne ?
;;; which yields a sequence of entry points (from find-best-entries)
;;;   HAS-NUMÉRO HAS-TÉLÉPHONE HAS-PRÉSIDENT UNIVERSITÉ COMPIÈGNE
;;; where HAS-NUMÉRO and HAS-TÉLÉPHONE are attributes and HAS-PRÉSIDENT a relation
;;; and UNIVERSITÉ a concept.
;;; The idea is to find a path from UTC to person including the HAS-PRÉSIDENT
;;; relation

;;; The idea is somewhat different from the previous ones. If we visualize the
;;; algorithm as developing a spanning tree going through marked nodes, then the
;;; particular ordering of the nodes and arcs is not really useful.
;;; Therefore, we associate to each path a set containing the concepts and nodes
;;; to visit, e.g.
;;;    (<path> <set of relations and concepts>)
;;; We call this set the checkpoint set.
;;; Given a path starting with a particular individual (that we call the current
;;; place), we do the following:
;;; - if the individual is an instance of a concept or a subconcept contained in 
;;;   the checkpoint set, we simply remove the concept from the checkpoint set, 
;;;   assuming that we are just visiting this concept
;;; - when extending a path
;;;   - if the potential relation or inverse-links of the current place correspond
;;;     to one of the relations of the checkpoint set, then
;;;     - if the relation or inverse link is present at the current place, we 
;;;       extend the path and remove the relation from the checkpoint set,
;;;     - otherwise, we kill the path to be extended
;;;   - otherwise we extend the path creating a new path for all values associated
;;;     with the relation or inverse link (neigbors).
;;;         
;;; the difference with the previous algorithm is the checking of concepts.

(defParameter *path-max-length* 5)

;;;--------------------------------------------------------------- CLEAN-CONCEPT

(defun clean-concept (path)
  "takes a path (path rel-set concept-set), checks whether the head of the path ~
   is an individual of one of the concepts of the concept set. If so, removes ~
   the concept from the concept set.
Arguments:
   path: a triple
Return:
   the path with eventually a modified concept set."
  (let ((obj-id (caar path))
        (concept-id-set (caddr path)))
    (dolist (concept-id concept-id-set)
      (if (%type? obj-id concept-id)
          (setq concept-id-set (remove concept-id concept-id-set))))
    (list (car path) (cadr path) concept-id-set)))

#|
? (moss::clean-concept 
    '(($E-PERSON.7 $S-TEACHING-ORGANIZATION-PRESIDENT $E-UNIVERSITY.4) 
      NIL ($E-PERSON)))
(($E-PERSON.7 $S-TEACHING-ORGANIZATION-PRESIDENT $E-UNIVERSITY.4) NIL NIL)
|#
;;;----------------------------------------------------------------- EXTEND-PATH

(defUn extend-path (path rel-id-set concept-id-set)
  "takes a path starting with an object-id and extends the path incrementally to ~
   the neighbor avoiding to loop back onto the same path.
Arguments:
   path: a reversed list of objects and properties, starting with an instance
   rel-id-set: list of relation ids that the path must go through
   concept-id-set: list of concept ids that the path must go through
Return:
   a set of new paths or NIL if it cannot be extended."
  (let (obj-id  prop-list inv-id incoming-list value-list result new-paths)
    ;(format t "~%; extend-path /input path: ~%  ~S~%; ..checkpoint relations: ~S~
    ;           ~%; ..checkpoint concepts: ~S"
    ;  path rel-id-set concept-id-set)
    ;; check that obj-id is PDM object loading it if needed
    (unless (%pdm? (setq obj-id (car path)))
      (error "~S is not a PDM object" obj-id))    
    
    ;; get the set of properties (local and inherited)
    (setq prop-list (send obj-id '=get-properties))
    ;; filter relations
    (setq prop-list 
          (remove nil
                  (mapcar #'(lambda(xx) (if (%is-relation? xx) xx)) prop-list)))
    
    ;; for each relation we extend the path and update rel-id-set when the relation
    ;; is part of it
    (dolist (sp-id prop-list)
      ;; check if there are values
      (setq value-list (send obj-id '=has-value-id sp-id))
      
      ;; if some we extend the path
      (when value-list
        (setq new-paths (mapcar #'(lambda (xx) (cons xx (cons sp-id path)))
                          value-list))
        ;; build set of paths to return with one element less in checkpoint-set
        ;; one cannot use a simple remove because the property may be a local
        ;; property, and the one in the rel-id-set the generic one
        ;; one must then check if the property in rel-id-set is the same or is
        ;; the generic one
        (setq new-paths
              (mapcar #'(lambda (xx) (list xx (remove-relation sp-id rel-id-set)
                                           concept-id-set))
                new-paths)))
      (setq result (append result new-paths)
          new-paths nil)
      ) ; end loop on relations
    
    ;;=== do the same for inverse relations
    (setq incoming-list (get-incoming-properties obj-id))
    
    (dolist (sp-id incoming-list)
      ;; first invert it
      (setq inv-id (%inverse-property-id sp-id))
      ;; get values
      (setq value-list (send obj-id '=has-value-id inv-id))
      
      (when value-list
        ;; otherwise extend the path with values
        (setq new-paths 
              (mapcar #'(lambda (xx) (cons xx (cons inv-id path)))
                value-list))
        ;; build the set of paths to return
        (setq new-paths 
              (mapcar #'(lambda (xx) (list xx (remove-relation sp-id rel-id-set)
                                           concept-id-set)) 
                new-paths))
        (setq result (append result new-paths)
            new-paths nil)
        )
      )
    
    ;; return the result
    (mapcar #'clean-concept result)))

#|
? (moss::extend-path '($E-UNIVERSITY.4) '($S-PRESIDENT) nil)
((($E-PERSON.20 $S-TEACHING-ORGANIZATION-PRESIDENT $E-UNIVERSITY.4) NIL NIL))

? (moss::extend-path '($E-UNIVERSITY.4) '($S-EMPLOYER) nil)
((($E-PERSON.2 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.3 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.4 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.5 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.6 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.7 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.8 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.10 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.11 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.12 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL)
 (($E-PERSON.20 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL))

? (moss::extend-path '($E-UNIVERSITY.4) '($S-PRESIDENT) '($E-UNIVERSITY))
((($E-PERSON.20 $S-TEACHING-ORGANIZATION-PRESIDENT $E-UNIVERSITY.4) NIL NIL))

? (moss::extend-path '($E-UNIVERSITY.4) '($S-PRESIDENT) '($E-ORGANIZATION))
((($E-PERSON.20 $S-TEACHING-ORGANIZATION-PRESIDENT $E-UNIVERSITY.4) NIL NIL))

? (moss::extend-path '($E-UNIVERSITY.4) '($S-PRESIDENT) '($E-POSTAL-ADDRESS))
((($E-PERSON.20 $S-TEACHING-ORGANIZATION-PRESIDENT $E-UNIVERSITY.4) NIL
  ($E-POSTAL-ADDRESS)))

? (moss::extend-path '($E-UNIVERSITY.4) nil '($E-POSTAL-ADDRESS))
((($E-POSTAL-ADDRESS.6 $S-ORGANIZATION-ADDRESS $E-UNIVERSITY.4) NIL
  ($E-POSTAL-ADDRESS))
 (($E-PERSON.20 $S-TEACHING-ORGANIZATION-PRESIDENT $E-UNIVERSITY.4) NIL
  ($E-POSTAL-ADDRESS))
 (($E-PERSON.2 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.3 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.4 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.5 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.6 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.7 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.8 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.10 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.11 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.12 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-PERSON.20 $S-PERSON-EMPLOYER.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS))
 (($E-STUDENT.1 $S-STUDENT-UNIVERSITY.OF $E-UNIVERSITY.4) NIL ($E-POSTAL-ADDRESS)))
|#
;;;--------------------------------------------------------- EXTRACT-SHORT-PATHS

(defun extract-short-paths (path-list)
  "takes a list of paths and extracts the shortest ones.
Argument:
   path-list: a list of lists
Return:
   the list of the car of the shortest paths."
  (when path-list ; path must not be empty
    (let ((nmin))
      ;; get the length of the shortest path
      (setq nmin (apply #'min (mapcar #'length path-list)))
      ;; extract the car of the shortest ones
      (remove nil (mapcar #'(lambda (xx) (if (eql (length xx) nmin) (car xx)))
                    path-list)))))

#|
(extract-short-paths nil)
NIL
(extract-short-paths '((a)))
(A)
(extract-short-paths '((A) (B B) (c) (DD D D) (e)))
(A C E)
|#    
;;;--------------------------------------------------------- FIND-PATH-TO-TARGET

;;; the function starts from an entry point, and tries to find a path to an 
;;; instance of the target class. Does that breadth first.

(defUn find-path-to-target (entry target checkpoint-set)
  "the function starts from an entry point, and tries to find a path to an ~
   instance of the target class. Does that breadth first.
Arguments:
   entry: an entry-point pattern (<class-id> (<attribute-id> :is <entry>))
   target: concept (class)
   checkpoint-set: imposed relations and concepts on the path
Return: 
   a path to the concept or nil."
  (declare (special *path-max-length*))
  (let ((distance 0) 
        obj-id-list target-paths path-set result rel-set concept-set)
    ;(format t "~%; find-path-to-target /*path-max-length*: ~S" *path-max-length*)
    ;; check the class
    (unless (%is-concept? target)
      (error "~S is not a concept in the package ~S" target *package*))
    ;; check the entry point   
    (unless (setq obj-id-list (funcall #'access entry :already-parsed t))
      ;; entry may be an entry point but point to no object any longer, which is
      ;; not an error JPB121212
      (return-from find-path-to-target nil))
    ;(format t "~%; find-path-to-target /obj-id-list: ~%  ~S" obj-id-list)
    
    ;; take the checkpoint-set and extract relation-ids and concept-ids
    (setq rel-set 
          (remove nil
                  (mapcar #'(lambda (xx) (%%get-id xx :relation)) checkpoint-set)))
    (setq concept-set
          (remove nil 
                  (mapcar #'(lambda (xx) (%%get-id xx :concept)) checkpoint-set)))
    
    ;; build a path for each object corresponding to the entry point
    (dolist (obj-id obj-id-list)
      ;; build initial set of paths (only one at a time)
      (setq path-set `(((,obj-id) ,rel-set ,concept-set)))
      ;; check if first element is an individual of the concept set, in which case 
      ;; we remove the corresponding concept from the concept set
      (setq path-set (mapcar #'clean-concept path-set))
      ;(format t "~%; find-path-to-target /path-set: ~%  ~S" path-set)
      
      ;=== loop, moving ahead one link each time, and checking the result
      (loop
        ;(format t "~%; find-path-to-target /===== distance: ~S" distance)
        ;(format t "~%; find-path-to-target /path-set: ~%  ~S" path-set)
        ;; filter out paths that do not arrive at an instance of target
        ;; and have exhausted the compulsory check point relations
        (setq target-paths 
              (remove nil 
                      (mapcar #'(lambda (xx) (if (and
                                                  (%type? (caar xx) target)
                                                  ;; all rel exhausted ?
                                                  (null (cadr xx))
                                                  (null (caddr xx)))
                                               xx))
                              path-set)))
        ;(format t "~%; find-path-to-target /target-paths: ~%  ~S" target-paths)
        ;; if anything left, stop we got it
        (if target-paths (return))
        ;; if the path length is equal to the max-length, return nil
        (if (>= (incf distance) *path-max-length*) (return))
        
        ;; otherwise extend all paths to the neighbors
        (setq path-set 
              (reduce #'append
                      (mapcar #'(lambda(xx) (apply #'extend-path xx)) path-set)))
        ;; if we have no possibility of extension (the end of the path has no
        ;; link or leads back to an already visited node, we quit
        (unless path-set (return))
        ) ; end of loop
      
      ;; collect paths arriving at target
      (setq result (append result target-paths)))
    ;; return paths 
    (mapcar #'car result)))

#|
;;; (in-package :contact)
? (moss::find-path-to-target 'barthès '$E-contact nil)
(($E-CONTACT.1 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.5 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.7 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.8 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.9 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.12 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.14 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.15 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.16 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.20 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.21 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.23 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.24 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.26 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.28 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.32 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2)
 ($E-CONTACT.33 $S-CONTACT-UTC-CORRESPONDENT.OF $E-PERSON.2))

;;; (in-package :address)
? (moss::find-path-to-target 'UTC '$e-person '(has-president))
(($E-PERSON.20 $S-TEACHING-ORGANIZATION-PRESIDENT $E-UNIVERSITY.4))
|#
;;;----------------------------------------------------- GET-INCOMING-PROPERTIES

(defUn get-incoming-properties (object-id)
  "get the list of all incoming properties from the models, i.e. properties that ~
   can be present as inverse-links in the object.
Argument:
   object-id
Return:
   list of inconming properties."
  (unless (%pdm? object-id)
    (error "~S is not a PDM object identifier" object-id))
  (reduce #'append 
          (mapcar #'(lambda(xx)
                      (moss::%get-value xx 'moss::$suc.of))
                  (moss::%sp-gamma-l 
                   (moss::%get-value object-id 'moss::$TYPE) 
                   'moss::$IS-A))))

#|
? (moss::get-incoming-properties '$E-UNIVERSITY.4)
($S-STUDENT-UNIVERSITY $S-PERSON-EMPLOYER $S-ORGANIZATION-SUBSIDIARY
 $S-ORGANIZATION-DIVISION $S-ORGANIZATION-DEPARTMENT)
|#
;;;-------------------------------------------------------- INTERSECT-PATH-LISTS

(defUn intersect-path-lists (object-lists)
  "takes a set of lists and returns the intersection"
  (cond ((null (cdr object-lists)) (mapcar #'car (car object-lists)))
        (t (intersect-path-lists
            (cons (intersection (car object-lists) (cadr object-lists) :key #'car)
                  (cddr object-lists))))))

#|
(intersect-path-lists '(((a 1) (b 2)(c 3) (d 4)) ((c 3 3) (d 4 4) (e 5 5)) 
                        ((a 1 1 1) (c 3 3 3) (d 4 4 4))))
(C D)
(moss::intersect-path-lists '(((a 1) (b 2)(c 3) (d 4)) ))
(A B C D)
|#
;;;---------------------------------------------------- LOCATE-COMPUTE-ROAD-MAPS
;;; takes a list of words, and apply first find-best-entries to extract entries
;;; from the text. Then start a multiple road-map list. For each entry check
;;; possible triples (ep, prop, obj-id). If more than one, duplicate the road maps.
;;; Return the list of road-maps

(defun locate-compute-road-maps (word-list)
  "takes a list of words and computes a set of possible road maps from the words."
  (let (index-list results class-id-list prop-id triple-list unused-words)
    ;; first get the list of entry points with references e.g.
    ;; ((LI ("li")) (PROJETS ("projets")) (BARTHÈS ("Barthès")))
    (multiple-value-setq (index-list unused-words)
      (find-best-entries word-list :keep-ref t))
    ;(print `(+++++ index-list ,index-list unused-words ,unused-words))
    ;; if empty, then quit
    (unless index-list (return-from locate-compute-road-maps nil))
    
    ;; creates an empty set of 1 road map
    (setq results '(nil))
    
    ;; now process each entry in turn to replace by class-id, generic property-id
    ;; or entry-point frame
    (dolist (entry index-list)
      (cond
       ;;== priority is given to classes 
       ;; in case of multiple classes, each class yields a distinct road-map
       ((setq class-id-list (%get-value (car entry) (%inverse-property-id '$ENAM)))
        (setq results (locate-push class-id-list results)))
       
       ;;== then relations
       ;((and (setq prop-id (car (%get-value (car entry) (%inverse-property-id '$PNAM))))
       ((and (setq prop-id (%%get-id (car entry) :prop))
             (%is-relation? prop-id))
        (setq  results (locate-push (list prop-id) results)))
       
       ;;== we discard attributes although they may be used to
       ;; filter the entry point in case it is used for several 
       ;; properties (like end-date and start-date)
       ;; however, we cannot build filters using such properties without also taking
       ;; operators into account, e.g. "before", "after", which will be done in the
       ;; subdialog
       (prop-id
        ; (setq results (locate-push (list prop-id) results))
        )
       
       ;;== must be entry point for some instance
       (t  
        ;; get triples for entry points
        ;; the following function gets a list of triples <entry inverse-prop instance>
        (setq triple-list
              (%%get-objects-from-entry-point (car entry) :keep-entry t))
        ;; if we cannot find any triple, e.g. when an entry point corresponds to
        ;; an object that was deleted, then quit
        (unless triple-list
          (return-from locate-compute-road-maps (values nil unused-words)))
        
        ;; add class-id to triples
        (setq triple-list 
              (mapcar #'(lambda(xx) 
                          (list (car (%type-of (caddr xx))) 
                                ;; filter
                                (list* 
                                 (%inverse-property-id (cadr xx))
                                 ':IS
                                 (cadr entry))))  ; e.g. "Li"
                triple-list))
        
        ;; extend paths
        (setq results (locate-push triple-list results)))
       )
      )
    ;; collapse subsumed classes and entry-point patterns
    (setq results 
          (mapcar #'(lambda (xx) (path-process-subsumption (reverse xx))) results))
    
    ;; return list
    (values results unused-words)))

#|
(moss::locate-compute-road-maps '("li"))
((($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "li"))) 
 (($E-PERSON ($T-PERSON-NAME :IS "li"))))
NIL
(moss::locate-compute-road-maps '("li" "permanent members" "UTC"))
((($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "UTC")) $S-PERMANENT-MEMBERS
  ($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "li")))
 (($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "UTC")) $S-PERMANENT-MEMBERS
   ($E-PERSON ($T-PERSON-NAME :IS "li"))))
NIL
(moss::locate-compute-road-maps '("thèses" "dirigées" "par" "Barthès"))
((($E-PERSON ($T-PERSON-NAME :IS "Barthès")) $E-THESIS))
("dirigées" "par")
|#
;;;------------------------------------------------------ LOCATE-FOLLOW-ROAD-MAP

(defun locate-follow-road-map (road-map target)
  "take a single road-map and tries to retrieve information by following it
Arguments
   road-map: a list containing road-map milestones
   target: class-id of the target object
Return:
   a list of objects that were reached."
  (let (application-entry-list checkpoint-set path-lists result)
    ;; first check if the road-map contains any application entry point
    ;; anything that is a list is an ep-pattern
    (setq application-entry-list 
          (remove nil (mapcar #'(lambda(xx) (if (listp xx) xx)) road-map)))
    ;(format t "~%; locate-follow-road-map /application-entry-list:~%  ~S" 
    ;  application-entry-list)
    
    ;; if nothing left cannot use locate-objects because we start our path from
    ;; an entry point, requiring at least one entry point
    (unless application-entry-list
      (return-from locate-follow-road-map nil))
    
    ;; keep entry points corresponding to relations and classes
    (setq checkpoint-set 
          (remove nil 
                  (mapcar #'(lambda (xx) (if (or (%%get-id xx :relation)
                                                 (%%get-id xx :concept))
                                             xx))
                    road-map)))
    ;(format t "~%; locate-follow-road-map /checkpoint-set:~%  ~S" checkpoint-set)
    
    ;; get the sets of paths back to target
    (setq path-lists 
          (remove nil
                  (mapcar #'(lambda (xx) 
                              (find-path-to-target xx target checkpoint-set))
                    application-entry-list)))
    (format t "~%; locate-follow-road-map /path-lists:~%  ~S" path-lists)
    
    (setq result
          (if (cdr path-lists)
              (delete-duplicates (intersect-path-lists path-lists))
            ;; if there is only one list, then we use the heuristic of shortest path
            (extract-short-paths (car path-lists))))
    ;; if nil return reason
    result
    ))

;;;-------------------------------------------------------------- LOCATE-OBJECTS
;;; the function is normally used to locate objects starting from en entry point.
;;; However, there might be a special case when we cannot find an entry point in
;;; the list of words. In that case, we can start exploring the graph starting 
;;; from the elements of the set and using the entry-list as a constraint;
;;; for example ("tous" "les" "projets" "ayant" "un" "financement" "annuel" "?")
;;; -> ($E-PROJECT $E-ANNUAL-FUNDING)
;;; dans ce cas, partant de chaque instance de projet, on ne gardera que ceux 
;;; qui dans une branche ont la classe "financement annuel"
;;; Il faudrait élargir le traitement préalable pour traiter la négation, e.g.
;;; "projets sans financement" ou des inormations particulières comme "projets
;;; internationaux" ou même "type de financement" qui n'est pas une propriété 
;;; de l'ontologie (toutefois dans ce dernier cas, l'expression sert à trouver
;;; la tâche correspondant, mais pas à filtrer les projets)

(defUn locate-objects (word-list class-ref 
                                 &optional (path-max-length *path-max-length*)
                                 always)
  "takes a word-list and tries to locate objects from contained entry points.
Arguments:
   word-list: a list of words
   class-ref: target concept
   path-max-length (opt): optional max-length for computed paths
   always (opt): if t will try to return something, even if the word list does
                 not contain entry points (at worst all instances of the calss)
Return:
   2 values
    - a list of object ids
    - a flag: nil if list non empty, :no-path if list list is empty due to lack ~
      of input constraint, :no-solution if list is empty because no oject fits."
  (declare (special *path-max-length*))
  (let ((*path-max-length* path-max-length)
        entry-list application-entry-list path-lists target checkpoint-set result)
    (setq target (%%get-id class-ref :class))
    (unless target
      (error "~S is not a concept reference" class-ref))
    
    ;; get a list of class ids, relation ids and entry-point patterns
    (setq entry-list (path-process-word-list word-list))
    (format t "~%; locate-objects /entry-list(1):~%  ~S" entry-list)
    
    ;;=== special case: where we return all instances JPB1502
    ;; when entry  list is empty or contains only the target class id return
    ;; all instances, only if always is true.
    ;; WRONG: list may contain a single entry expr, e.g. 
    ;;  (($E-PERSON ($T-PERSON-NAME :IS "Correa-victorino")))
    (cond
     ((null always))
     ((null entry-list)
      (return-from locate-objects 
        (access (list target) :already-parsed t)))
     ((and (null (cdr entry-list))(eql (car entry-list) target))
      (return-from locate-objects 
        (access (list target) :already-parsed t)))
     ;; here single entry point
     ((and (null (cdr entry-list))(listp (car entry-list)))
      (return-from locate-objects
        (access (car entry-list) :already-parsed t)))
     )
    
    ;; extract anything that is a list is an ep-pattern
    (setq application-entry-list 
          (remove nil (mapcar #'(lambda(xx) (if (listp xx) xx)) entry-list)))
    ;(format t "~%; locate-objects /application-entry-list:~%  ~S" 
    ;  application-entry-list)
    
    ;;=== special case
    ;; however, if there is no entry point one can use the resulting entry list
    ;; as a filter on the list of elements of the target set, i.e. the access is
    ;; done via the set itself (e.g. ("tous" "les" "projets"))
    (cond
     ;; if nothing left cannot use locate-objects because we start our path from
     ;; an entry point, requiring at least one entry point
     ((and (null application-entry-list)(null always))
      (warn "no application entry point nor alive pointed object using ~S" word-list)
      (return-from locate-objects (values nil :no-path)))
     ;; otherwise, use entry-list as a filter when starting with each instance
     ((null application-entry-list)
      (return-from locate-objects
        (locate-objects-special-case word-list class-ref)))
     )    
    
    ;(format t "~%; locate-objects /entry-list:~%  ~S" entry-list)
    ;;=== normal case: keep entry points corresponding to relations and classes
;;;    (setq checkpoint-set 
;;;          (remove nil 
;;;                  (mapcar #'(lambda (xx) (if (or (%%get-id xx :relation)
;;;                                                 (%%get-id xx :concept))
;;;                                             xx))
;;;                    entry-list)))
    (setq checkpoint-set 
          (remove nil 
                  (mapcar #'(lambda (xx) (if (or (%is-relation? xx)
                                                 (%is-concept? xx))
                                             xx))
                    entry-list)))
    ;(format t "~%; locate-objects /checkpoint-set:~%  ~S" checkpoint-set)
    ;; get the sets of paths back to target, starting from one of the entry points
    ;; of the application-entry-list
    (setq path-lists 
          (remove nil
                  (mapcar #'(lambda (xx) 
                              (find-path-to-target xx target checkpoint-set))
                    application-entry-list)))
    ;(format t "~%; locate-objects /path-lists:~%  ~S" path-lists)
    ;; intersect the set of paths returned by find-path-to-target
    ;; => ((<path1> <path2>)(<path3> <path4>)(<path5>))
    ;(intersect-path-lists path-lists) JPB 1110
    
    (setq result
          (if (cdr path-lists)
              (delete-duplicates (intersect-path-lists path-lists))
            ;; if there is only one list, then we use the heuristic of shortest path
            (extract-short-paths (car path-lists))))
    ;; if nil return reason
    (if result (values result nil) (values result :no-solution))    
    ))
  
#|
;;; here we use a new heuristics. We start with pths of length 2 try to find 
;;; something, and if we don't, we increase the length of the path until we find
;;; a result or until we exceed the max length allowed
;;; This uses a heuristic of shortest path

(defun locate-objects (word-list class-ref 
                                 &optional (path-max-length moss::*path-max-length*))
  "new version of locat-objects
   takes a word-list and tries to locate objects from contained entry points.
Arguments:
   word-list: a list of words
   class-ref: target concept
   path-max-length (opt): optional max-length for compued paths
Return:
   2 values
    - a list of object ids
    - a flag: nil if list non empty, :no-path if list list is empty due to lack ~
      of input constraint, :no-solution if list is empty because no oject fits."
  (declare (special *path-max-length*))
  (let ((max-length 1)
        result)
    (loop
      (incf max-length)
      (format t "~%; locate-objects /trying max-length: ~S" max-length)
      (if (> max-length path-max-length) (return-from locate-objects nil))
      ;; try access with limitd length
      (setq result (moss::locate-objects-2 word-list class-ref max-length))
      ;; if we found something return it and quit
      (when result (return-from locate-objects result))
      )))
|#
#|
;;; one problem with this function is if we have an entry containing a single
;;; entry point, then the answer will be all the instances that can be reached
;;; by traveling from this entry point, which is usually wrong. To solve this
;;; problem we introduce the heuristic of shortest path
;;; another problem is that of using the name of the attribute associated with
;;; an entry point to avoid ambiguities in particular when entry points are dates

;;; NEW APPROACH
;;; first apply path-process-word-list that takes into account attributes and
;;; returns a list of concepts, relations and entry-point patterns
;;; - if the returned value consists of a single ep-pattern and the class of
;;;   the ep-pattern subsumes the target, apply access
;;; - otherwise do as follows
;;;   - start the paths from the ep-patterns using the attribute to get a first
;;;     set of instances
;;;   - navigate from these instances as usual

(defUn locate-objects (word-list class-ref 
                                 &optional (path-max-length *path-max-length*))
  "takes a word-list and tries to locate objects from contained entry points.
Arguments:
   word-list: a list of words
   class-ref: target concept
   path-max-length (opt): optional max-length for compued paths
Return:
   2 values
    - a list of object ids
    - a flag: nil if list non empty, :no-path if list list is empty due to lack ~
      of input constraint, :no-solution if list is empty because no oject fits."
  (declare (special *path-max-length*))
  (let ((*path-max-length* path-max-length)
        entry-list application-entry-list path-lists target checkpoint-set result)
    (setq target (%%get-id class-ref :class))
    (unless target
      (error "~S is not a concept reference" class-ref))
    
    ;; get a list of class ids, relation ids and entry-point patterns
    (setq entry-list (path-process-word-list word-list))
    (format t "~%; locate-objects /entry-list:~%  ~S" entry-list)
    
    ;; anything that is a list is an ep-pattern
    (setq application-entry-list 
          (remove nil (mapcar #'(lambda(xx) (if (listp xx) xx)) entry-list)))
    ;(format t "~%; locate-objects /application-entry-list:~%  ~S" 
    ;  application-entry-list)
    
    ;; if nothing left cannot use locate-objects because we start our path from
    ;; an entry point, requiring at least one entry point
    (unless application-entry-list
      (warn "no application entry point nor alive pointed object using ~S" word-list)
      (return-from locate-objects (values nil :no-path)))
    
    ;; keep entry points corresponding to relations and classes
    (setq checkpoint-set 
          (remove nil 
                  (mapcar #'(lambda (xx) (if (or (%%get-id xx :relation)
                                                 (%%get-id xx :concept))
                                             xx))
                    entry-list)))
    ;(format t "~%; locate-objects /checkpoint-set:~%  ~S" checkpoint-set)
    ;; get the sets of paths back to target
    (setq path-lists 
          (remove nil
                  (mapcar #'(lambda (xx) 
                              (find-path-to-target xx target checkpoint-set))
                    application-entry-list)))
    (format t "~%; locate-objects /path-lists:~%  ~S" path-lists)
    ;; intersect the set of paths returned by find-path-to-target
    ;; => ((<path1> <path2>)(<path3> <path4>)(<path5>))
    ;(intersect-path-lists path-lists) JPB 1110
    
    (setq result
          (if (cdr path-lists)
              (delete-duplicates (intersect-path-lists path-lists))
            ;; if there is only one list, then we use the heuristic of shortest path
            (extract-short-paths (car path-lists))))
    ;; if nil return reason
    (if result (values result nil) (values result :no-solution))    
    ))
|#
#|
? (moss::locate-objects '("téléphone" "du" "président" "de" "l" "UTC" "?") "personne")
($E-PERSON.20)
? (moss::locate-objects 
   '("quels" "sont" "les" "contacts" "de" "Moulin" "au" "Japon" "?") "contact")
($E-CONTACT.21)
? (moss::locate-objects 
   '("quels" "sont" "les" "contacts" "de" "Barthès" "au" "Japon" "?") "contact")
($E-CONTACT.21)
? (moss::locate-objects 
   '("quels" "sont" "nos" "contacts" "au" "Japon" "?") "contact")
($E-CONTACT.21 $E-CONTACT.22)
|#
;;;------------------------------------------------- locate-objects-special-case

(defun locate-objects-special-case (word-list class-ref)
  "tries a different strategy by building a set of queries from the word list ~
   and using them to access the database.
Arguments:
   word-list: list of words
   class-ref: target class
Return:
   a list of instances or NIL."
  (format t "~%;---------- Entering locate-objects-special-case")
  (let (query-list result)
    ;; move target as first element of list
    (setq word-list (cons class-ref (remove class-ref word-list :test #'equal+)))
    ;; synthesize query
    (setq query-list (path-build-query-list (cons class-ref word-list)))
    (format t "~%;--- list of synthesized queries:~%  ~{~S~%~}" query-list)
    ;; apply each query
    (dolist (query query-list)
      (setq result
            (append result (access query :already-parsed t))))
    (delete-duplicates result)))
  
;--------------------------------------------------------------- LOCATE-OBJECT-2

(defun locate-objects-2 (word-list class-ref 
                                   &optional (path-max-length *path-max-length*))
  "new version of locat-objects
   takes a word-list and tries to locate objects from contained entry points.
Arguments:
   word-list: a list of words
   class-ref: target concept
   path-max-length (opt): optional max-length for compued paths
Return:
   2 values
    - a list of object ids
    - a flag: nil if list non empty, :no-path if list list is empty due to lack ~
      of input constraint, :no-solution if list is empty because no oject fits."
  (declare (special *path-max-length*))
  (let ((*path-max-length* path-max-length)
        entry-list target  result)
    (setq target (%%get-id class-ref :class))
    (unless target
      (error "~S is not a concept reference" class-ref))
    
    ;; get a list of road maps
    (setq entry-list (locate-compute-road-maps word-list))
    (format t "~%; locate-objects 2 /entry-list:~%  ~S" entry-list)
    
    ;; some of the path could be the same
    (setq entry-list (delete-duplicates entry-list :test #'equal))
    
    ;; for each element get data
    (dolist (path entry-list)
      (format t "~%; locate-objects 2/path:~%  ~S" path)
      (setq result (append (locate-follow-road-map path target) result))
      (format t "~%; locate-objects 2/result:~%  ~S" result)
      )
        
    (delete-duplicates result)))
    
;;;----------------------------------------------------------------- LOCATE-PUSH

(defun locate-push (id-list road-maps)
  "takes a list of road-maps and adds an item in front, duplicating the paths if ~
   id-list contains more than one object.
Arguments:
   id-list: list of object ids to add to paths
   road-maps: list of different road-maps
Returns:
   list of extended road maps."
  (let (result)
    (unless id-list
      (return-from locate-push road-maps))
    ;; consider each road map in turn 
    (dolist (item road-maps)
      ;; for each item (road-map) put an element of id-list in fromt, duplicating
      ;; if needed
      (dolist (id id-list)
        (push (cons id item) result)))
    (reverse result)))

#|
(locate-push nil '((L1)))
((L1))
(locate-push '(a b c) '(nil))
((A) (B) (C))
(locate-push '(a) '((l1)))
((A L1))
(locate-push '(a b c) '((l1)))
((A L1) (B L1) (C L1))
(locate-push '(a b c) '((L1) (L2) (L3)))
((A L1) (B L1) (C L1) (A L2) (B L2) (C L2) (A L3) (B L3) (C L3))
|#
;;;------------------------------------------------------------- RELATION-THERE?

(defUn relation-there? (rel-id obj-prop-id)
  "takes a property id in input (relation or inverse-relation and returns the ~
   object relation or inverse relation if it is the same.
Arguments:
   rel-id: generic property
   obj-prop-id: a relation id or inverse relation id, e.g. $S-PERSON-EMPLOYER.OF
Return:
   the object property-id like $S-TEACHING-ORGANIZATION-PRESIDENT or nil"
  (let (gen-prop-id)
    ;; if obj-prop-id is a relation get generic property from it
    (cond
     ((%is-relation? obj-prop-id)
      (setq gen-prop-id (%get-generic-property (list obj-prop-id))))
     ;; if an inverse
     ((%is-inverse-property? obj-prop-id)
      (setq gen-prop-id (%get-generic-property 
                         (list (%inverse-property-id obj-prop-id)))))
     (t (error "~S is not a property nor an inverse property" obj-prop-id)))
    ;; compare, if equal return obj property
    (if (eql rel-id gen-prop-id) obj-prop-id)))

#|
? (moss::relation-there? '$S-PRESIDENT '$S-TEACHING-ORGANIZATION-PRESIDENT)
$S-TEACHING-ORGANIZATION-PRESIDENT
? (moss::relation-there? '$S-EMPLOYER '$S-PERSON-EMPLOYER.OF)
$S-PERSON-EMPLOYER.OF
? (moss::relation-there? '$S-PRESIDENT '$S-PERSON-EMPLOYER.OF)
NIL
|#
;;;------------------------------------------------------------- REMOVE-RELATION

(defUn remove-relation (rel-id gen-rel-list)
  "takes a property id in input (relation or inverse-relation and returns the ~
   object relation or inverse relation if it is the same.
Arguments:
   rel-id: local property (maybe generic)
   gen-rel-list: list of generic properties
Return:
   the list of generic properties less eventually the one corresponding to rel-id."
  (let (gen-prop-id)
    ;; if obj-prop-id is a relation get generic property from it
    (cond
     ((%is-relation? rel-id)
      (setq gen-prop-id (%get-generic-property (list rel-id))))
     ;; if an inverse
     ((%is-inverse-property? rel-id)
      (setq gen-prop-id (%get-generic-property 
                         (list (%inverse-property-id rel-id)))))
     (t (error "~S is not a property nor an inverse property" rel-id)))
    ;; compare, if equal return obj property
    (remove gen-prop-id gen-rel-list)))

#|
(moss::remove-relation '$S-organization-address '($s-employer $s-address))
($S-EMPLOYER)
(moss::remove-relation '$S-person-employer.of '($s-employer $s-address))
($S-ADDRESS)
|#
;;;-----------------------------------------------------------------------------
;;;=============================================================================
;;;       Comparing LOCATE-OBJECTS and BUILD-QUERY-LIST
;;;=============================================================================

#|
;(in-package :address)
1. 
? (moss::path-build-query-list '( "utc"))
(($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "Utc")))
? (access '($E-UNIVERSITY ($T-ORGANIZATION-ACRONYM :IS "utc")))
($E-UNIVERSITY.4)

? (moss::locate-objects '( "utc") "organization")
($E-UNIVERSITY.4)

2.
? (moss::path-build-query-list '( "personne"))
(($E-PERSON))
Can't do that with locate-objects

3.
? (moss::path-build-query-list '("téléphone" "utc"))
(($E-PHONE
  ($S-ORGANIZATION-PHONE.OF (> 0)
                            ($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))
? (moss::locate-objects '("téléphone" "utc") "organization")
($E-UNIVERSITY.4)

4.
? (moss::path-build-query-list '("personne" "président" "utc"))
(($E-PERSON
  ($S-TEACHING-ORGANIZATION-PRESIDENT.OF (> 0)
   ($E-TEACHING-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "utc")))))
? (access '($E-PERSON
            ($S-TEACHING-ORGANIZATION-PRESIDENT.OF (> 0)
                                         ($E-TEACHING-ORGANIZATION 
                                          ($T-ORGANIZATION-ACRONYM :IS "utc"))))
                     :already-parsed t)
($E-PERSON.7)

? (moss::locate-objects '("personne" "président" "utc") "personne")
($E-PERSON.7)

5.
? (moss::path-build-query-list '( "téléphone" "président" "utc"))
(($E-PHONE
  ($S-PERSON-PHONE.OF (> 0)
   ($E-PERSON
    ($S-TEACHING-ORGANIZATION-PRESIDENT.OF 
     (> 0)
     ($E-TEACHING-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "Utc")))))))
|#






;(format t "~%;*** MOSS v~A - Paths loaded ***" moss::*moss-version-number*) 

:EOF