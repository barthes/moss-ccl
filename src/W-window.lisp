;;;-*- Mode: Lisp; Package: "NODGUI-USER" -*-
;;;==============================================================================
;;;19/08/26
;;;              M O S S - W I N D O W (file W-window.lisp)
;;;
;;;==============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| This file contains all the functions to build the MOSS interface window without
using the project mechanism.

History
2019
 0825 File created from the ACL version

TODO
 BUG: display only user-defined concepts when querying (MOSS-CONCEPT)
|#

(in-package :nodgui-user)

;;;================================= globals ====================================
;;; global parameters used by the application and the window

;(fmakunbound 'display-text)

(eval-when (:compile-toplevel :load-toplevel :execute)
 (import '(moss::*moss-output*)))

(defparameter *mw* nil)
;(defParameter *moss-window* nil "MOSS interface window")

;;;(defmacro catch-error (&rest body)
;;;           `(progn
;;;              (let ((err (catch :error ,@body nil)))
;;;                  ;; if non nil, then string to be printed
;;;                (cond 
;;;                 ((null err))
;;;                 ((stringp err) (moss::mformat err))
;;;                 (t (moss::mformat "~&... unknown error: ~S" err))))))

(defmacro catch-system-error (msg &rest body)
  `(let (test errno)
     (multiple-value-setq (test errno) 
       (ignore-errors ,@body))
     ;(print (list test errno))
     ;; if test is nil and errno is not: error
     (if (and (null test) errno)
         (moss::mformat "~%... system error ~A: ~S" ,msg errno))
     test))
#|
(catch-system-error "" (+ 2 3 4))
9
(catch-system-error "while doing things" (+ 2 3 4)(/ 4 0))
... system error while doing things: #<DIVISION-BY-ZERO #x302003BEF02D>
NIL
(catch-system-error "" NIL)
NIL
|#

;;;-------------------------------------------------------------------- QTFORMAT

(defUn qtformat (mw mw-out string &rest args)
  "to trace querying process"
  (if (trace-query mw)
      (append-text mw-out (apply #'format nil (string+ "~%" string) args))))

;;;------------------------------------------------------------------- USER-LOAD
;;; When exec does not include compiler, we cannot use load to load MOSS def 
;;; files. Hence a function that loads each expression one by one and evals them.
;;;********** to be checked...

#|
(defUn user-load (file-pathname &key dialog &aux expr)
  "function allowing to load a lisp file from an application. If compiler ~
   is there, calls the standard load function."
  (if (member :compiler *features*)
      (load file-pathname)
    (with-open-file (ss file-pathname :direction :input)
      (loop
        (setq expr (read ss nil :eof))
        (if (eql expr :eof) (return-from user-load :done))
        (mw-display-more-info dialog (format nil "~%~S" expr))
        (sleep .2)
        (eval expr))))
  )
|#

;;;===============================================================================
;;;                          ADDITIONS TO NODGUI
;;;===============================================================================

(defmethod select-all ((pane text))
  (format-wish "~A tag add sel 1.0 end" (widget-path pane)))  

(defmethod get-selection ((pane text))
  (when (equal (selection? pane) "")
    (return-from get-selection))
  (format-wish "senddatastring [~A get sel.first sel.last]" (widget-path pane))
  (read-data))

(defmethod selection? ((pane text))
  ;; check if there is a range in which the sel tag appears
  (format-wish "senddatastring [~A tag nextrange sel 1.0 end]" (widget-path pane))
  ;; the following command does not work
  ;(format-wish "senddatastring [~A tag names 1.0]" (widget-path pane))
  (read-data))

(defmethod set-focus ((input-pane text))
  (format-wish "focus ~A" (widget-path input-pane)))


;;;===============================================================================
;;;                                 MOSS-WINDOW
;;;===============================================================================

;;;=================================== Classes ==================================

;;;------------------------------------------------------------------ MOSS-WINDOW

(defClass MOSS-WINDOW (toplevel)
  ((application-package :accessor application-package :initform nil) 
   (calling-window :accessor calling-window :initform nil)
   (clean-pane-before-printing :accessor clean-pane-before-printing :initform t)
   (current-entity :accessor current-entity)
   (conversation :accessor conversation :initform nil) ; pointer to conversation
   (conversation-process :accessor conversation-process :initform nil)
   (database :accessor database :initform nil) ; unused
   (gate :accessor gate :initform nil) ; gate to keep process waiting on nil
   (interaction-mode :accessor interaction-mode :initform :query) ; :lisp :conversation
   (message-area :accessor message-area :initform nil)
   (next-editor :accessor next-editor :initform nil) ; points to first editor   
   (outline-window :accessor outline-window :initform nil)
   (owner :accessor owner :initform nil) ; owner is unused in MOSS
   (process :accessor process :initform nil) ; to kill process when closing window
   (query-last-results :accessor query-last-results :initform nil)
   (query-abort-condition :accessor query-abort-condition :initform nil)
   (trace-query :accessor trace-query :initform nil)
   (user-input :accessor user-input :initform nil) ; dialog engine waits on user-input
   )
  (:documentation 
   "Window for interacting with the MOSS environment"))

;;;------------------------------------------------- (MOSS-WINDOW) ACTIVATE-INPUT

#|
(defMethod activate-input ((win moss-window) &key erase)
  "prints a text into the output pane of the window.
Arguments:
   erase (key): if true erase the content, otherwise select it
Return: unimportant."
  ;; activate the user panel
  ;; the user will enter its data that will be transferred into the to-do
  ;; slot, waking up the resume process
  ;(window-show win)
  ;; erase pane
  ;(if erase (setf (cg:value (cg:find-component :input-pane win)) ""))
  ;(set-current-key-handler win (view-named :input-pane win))
  )
|#

;;;--------------------------------------------------- (MOSS-WINDOW) DISPLAY-TEXT

(defMethod moss::display-text ((win moss-window) text &key erase final-new-line new-line
                               &allow-other-keys)
  "prints a text into the output pane of the window. Must be a string.
Arguments:
  text: text to display
  erase (key): if t, erase previous text, otherwise append to it
  final-new-line (key): if t add a new line at the end of the text
Return: 
  nil."
  (unless (stringp text) (error "~S should be a string." text))
  (let ((out (message-area win)))
    (if new-line
        (append-newline out))
    (if erase
        (setf (text out) text)
        (setf (text out) (concatenate 'string (text out) text)))  
    (if final-new-line
        (append-newline out)
        )  
    nil))

;;;------------------------------------------------ (MOSS-WINDOW) INPUT-RECEIVED?

#|
(defMethod input-received? ((win moss-window))
  "checks if something in the user-input slot of the window. If yes, returns it ~
   resetting the slot.
Arguments:
   none
Return: 
   text or nil."
 #|
 (let ((text (user-input win)))
    (when text
      (cg::beep)
      (setf (user-input win) nil) 
      text))
|#)
|#

;;;------------------------------------------------ (MOSS-WINDOW) SET-CONVERSATION
;;; not used by ACL ?

#|
(defMethod set-conversation ((win moss-window) conversation-id)
  "initializes the conversation slot of the MOSS window with the id of the current ~
   conversation object.
Arguments:
   conversation-id: conversation id (e.g. cl-user::$CVSE.1)
Return:
   the conversation id."
  (setf (conversation win) conversation-id))
|#

;;;==================================== Window ===================================

;;;------------------------------------------------------------- MAKE-MOSS-WINDOW
;; The maker-function, which always creates a new window.
;; Call this function if you need more than one copy,
;; or the single copy should have a parent or owner window.
;; (Pass :owner to this function; :parent is for compatibility.)

#|
(progn
  (setq *context* 0)
(setq moss::*moss-window*
  (with-nodgui (:debug 3) 
     (make-moss-window :master *tk* :application-name "Family" 
                       :application-package (find-package "family")
                       :title "Family Ontology" ))
   (inspect 'moss::*moss-window*)
   ))

(format t "~S" `(inside nodgui ,*moss-window*))(finish-output))
|#
(defUn make-moss-window (&key master application-name application-package title)
  "we assume that wish is set up to communicate with Ltk"
  (declare (special moss::*legal-import-formats* *moss-output* *moss-window*
                    moss::*current-import-format*
                    moss::*legal-export-formats* moss::*current-export-format* *package*))

  ;; brute force package assignment
  (setq *package* (find-package application-package))
  ;(print `("make-moss-window *package*" ,*package*))
  (let*
      ((package (or application-package application-name))
       (mw (make-instance 'moss-window :master master))
       ; handle to access window
       (mw-top (make-instance 'frame :master mw))
       (mw-ctrl (make-instance 'frame :master mw))
       (mw-text-in (make-instance 'frame :master mw))
       (mw-meter (make-instance 'frame :master mw))
       (mw-text-out (make-instance 'frame :master mw))

       ;;=== text input area
       (mw-in (make-instance 'text :master mw-text-in :height 10))
       ;;=== message area here so that mw-out can be used in other slots
       (mw-out (make-instance 'text :master mw-text-out :height 20))

       ;;=== top info
       (mw-context (make-instance 'label :master mw-top :text "Context"))
       (mw-context-set (make-instance 'entry :master mw-top :text "0" :width 4))
       (mw-language  (make-instance 'label :master mw-top :text "Language"))
       (mw-language-set (make-instance 'combobox :master mw-top :width 15
                     :values moss::*legal-tag-names* :justify "center"
                     :text (car (rassoc *language* moss::*language-names-tags*))))
       (mw-package  (make-instance 'label :master mw-top :text "Package"))
       (mw-package-set (make-instance 'entry :master mw-top :text (package-name *package*)
                         :width 15 :justify "center"))
       (mw-exit (make-instance 'button :master mw-top :text "EXIT"
                      :command (lambda () (destroy mw))))

       ;;== left left control buttons OVERVIEW, DB LOAD ALL
       (mw-llcb (make-instance 'frame :master mw-ctrl))
       (mw-overview (make-instance 'button :text "OVERVIEW" :master mw-llcb 
                      :command #'(lambda () (mw-overview-on-click mw))))
       (mw-db (make-instance 'button :text "DB LOAD ALL" :master mw-llcb
                :command #'(lambda () (mw-db-load-all-on-click))))
       ;;== left control buttons DB STRUCTURE, DB CONTENT
       (mw-lcb (make-instance 'frame :master mw-ctrl))
       (mw-db-struc (make-instance 'button :text "DB STRUCTURE" :master mw-lcb 
                      :command #'(lambda () (mw-db-structure-on-click))))
       (mw-db-content (make-instance 'button :text "DB CONTENT" :master mw-lcb 
                        :command #'(lambda () (mw-db-content-on-click))))
 
      ;;== left check boxes CLEAN PANE, TRACE QUERIES
       (mw-lcbx (make-instance 'frame :master mw-ctrl))
       (mw-clean (make-instance 'check-button :master mw-lcbx :text "clean pane"
                   ;:variable "*text*" :onvalue :active :offvalue :dead
                   :command #'(lambda (button-state) 
                                (mw-clean-box-on-change mw mw-out button-state))
                   ))
       (mw-check (make-instance 'check-button :master mw-lcbx :text "trace queries"
                   :command (lambda (button-state) 
                           (mw-trace-query-on-change mw mw-out button-state))))
       (mw-un (make-instance 'check-button :master mw-lcbx :text "(unused)"
                :command (lambda (button-state) 
                           (mw-internal-format-on-change mw mw-out button-state))))

       ;;== center radio buttons QUERY, HELP, DATA
       (mw-crb (make-instance 'labelframe :master mw-ctrl :text "Mode"))
       (mw-query (make-instance 'radio-button :master mw-crb :text "query"
                   :value "query" :variable "mode"
                   :command (lambda (val) 
                              (mw-fix-mode-on-click mw val "query" mw-in mw-out))))
       (mw-help (make-instance 'radio-button :master mw-crb :text "MOSS help"
                  :value "help" :variable "mode"
                  :command (lambda (val) 
                             (mw-fix-mode-on-click mw val "help" mw-in mw-out))))
       (mw-data (make-instance 'radio-button :master mw-crb :text "data"
                  :value "data" :variable "mode"
                  :command (lambda (val) 
                             (mw-fix-mode-on-click mw val "data" mw-in mw-out))))

       ;;== right control buttons EXECUTE, ABORT QRY
       (mw-rcb (make-instance 'frame :master mw-ctrl))
       (mw-execute (make-instance 'button :master mw-rcb :text "EXECUTE" 
                     :command (lambda () (mw-execute-on-click mw mw-in mw-out))))
       (mw-abort (make-instance 'button :master mw-rcb :text "ABRT QRY" 
                   :command (lambda () (mw-abort-query-on-click mw))))
       ;;== rightmost control buttons BROWSE, CLEAR ANSWER
       (mw-rrcb (make-instance 'frame :master mw-ctrl))
       (mw-browse (make-instance 'button :master mw-rrcb :text "BROWSE" 
                    :command (lambda () (mw-browse-on-click mw mw-out))))
       (mw-clear (make-instance 'button :master mw-rrcb :text "CLR ANSWER" 
                   :command (lambda () (mw-clear-answer-on-click mw-out))))
       ;;=== progressbar
       (mw-progress (make-instance 'progressbar :master mw-meter))
       )

    ;; save calling window to get back when closing this one, and switch channel
    (setf (calling-window mw) moss::*moss-output*
          *moss-output* mw *moss-window* mw
          (message-area mw) mw-out)
    
    (set-geometry-xy mw 50 100) ; was 650 770
    (wm-title mw (or title 
                     (string+ "MOSS " moss::*moss-version-number* " - " application-name)))

    ;;=== set top area
    (grid mw-top 0 0 :sticky "ew")
    (grid mw-context  0 0 :sticky "w" :pady "10 0" :padx "10 0")
    (grid mw-context-set 0 1 :pady "10 0" :padx "0 20")
    (grid mw-language 0 2 :pady "10 0")
    (grid mw-language-set 0 3 :pady "10 0" :padx "0 20")
    (grid mw-package 0 4 :pady "10 0")
    (grid mw-package-set 0 5 :pady "10 0")
    (grid mw-exit 0 6 :pady "10 0" :padx "20 10")

    ;;=== set control area
    (grid mw-ctrl 1 0 :sticky "ew")
    ;;
    (grid mw-llcb 0 0)
    (grid mw-overview 0 0)
    (grid mw-db 1 0 :padx "10 0")
    ;;
    (grid mw-lcb 0 1)
    (grid mw-db-struc 0 0)
    (grid mw-db-content 1 0)
    ;;=== check buttons
    (grid mw-lcbx 0 2)
    (grid mw-clean 0 0 :sticky "w" :pady "20 0")
    (grid mw-check 1 0 :sticky "w")
    (grid mw-un 2 0 :sticky "w")
    ;;=== radio buttons
    (grid mw-crb 0 3)
    (grid mw-query 0 0 :sticky "w")
    (grid mw-help 1 0 :sticky "w")
    (grid mw-data 2 0 :sticky "w")
    ;;
    (grid mw-rcb 0 4)
    (grid mw-execute 0 0)
    (grid mw-abort 1 0)
    ;;
    (grid mw-rrcb 0 5)
    (grid mw-browse 0 0)
    (grid mw-clear 1 0 :padx "0 10")
    ;;=== input frame
    (grid  mw-text-in 2 0 :sticky "ew")
    (pack mw-in :expand t :fill :both :padx 10 :pady "5 0")
    ;;=== progress-bar
    (grid mw-meter 3 0 :sticky "ew")
    (pack mw-progress :expand t :fill :both :padx 10)
    ;;=== output frame
    (grid mw-text-out 4 0 :sticky "ew")
    ;(grid mw-text-out 0 0 :sticky "ew":ipadx 5)
    (pack mw-out :expand t :fill :both :padx 10 :pady "0 10")

    (setq *package* (find-package package))
    (setf (application-package mw) *package*)
    
    ;; procedure to ask Tk to filter non digit characters
    (format-wish "proc ValidInt {val}  {
                    return [ expr  {[ string is integer $val ]
                          || [ string match {[-+]} $val ]} ]
                             }")
    ;;=== context (should be an integer)
    (format-wish "~A configure -validatecommand {ValidInt %P}" (widget-path mw-context-set))
    ;; call right function on return
    (bind mw-context-set "<Return>" 
          (lambda (event)
            (declare (ignore event))
            (mw-context-on-change  mw mw-context-set mw-out)))
    ;;=== change language
    (bind mw-language-set "<<ComboboxSelected>>"
          (lambda (event)
            (declare (ignore event))
            (mw-set-language-on-change  (text mw-language-set) mw-out)))
    ;;=== package: call back for package value
    (bind mw-package-set "<Return>"
          (lambda (event)
            (declare (ignore event))
            (mw-package-on-change mw mw-package-set (text mw-package-set) mw-out)))
    ;;==check buttons
    ;; initial value of the clean check button
    (setf (value mw-clean) 1)
    ;; initial value of the trace queries button
    (setf (value mw-check) (trace-query mw))
    ;;=== radio buttons
    ;; initial value for the radio buttons
    (setf (value mw-data) "query")

    ;;=== input pane
    (bind mw-in "<KeyPress>"
          (lambda (event) 
            (mw-input-on-change event mw mw-in mw-out mw-progress)))
    (bind mw-in "<Enter>" 
          (lambda (event) (declare (ignore event)) (select-all mw-in)))
    ;; initial text in the input pane
    (append-text mw-in (format nil "*** Now in query mode in package ~A ***
   End input with Return." (package-name *package*)))

    ;;=== output pane
    ;; initial text in the output pane
    (append-text mw-out "<output pane>")

    (set-focus mw-in)
    (select-all mw-in)

    ;; when closing this window, reestablish previous one
    (on-close mw (lambda () 
                   ;; reestablish previous output channel
                   ;; and reset *moss-window* to indicate it is closed
                   (setq *moss-output* (calling-window mw)
                         *moss-window* nil)
                   ;; close window
                   (destroy mw)))
    ))

#|
  (with-nodgui (:debug 3) 
     (make-moss-window :master *tk* :application-name "Family" 
                       :application-package "FAMILY"
                       :title "Family Ontology" ))
|#

;;;===============================================================================
;;;                      CALLBACKS & SERVICE FUNCTIONS
;;;===============================================================================

;;;-----------------------------------------------------  MW-ABORT-QUERY-ON-CLICK

(defUn mw-abort-query-on-click (mw)
  "set up the query-abort-condition flag, which should abort the query"
  (setf (query-abort-condition mw) t)
  t)

;;;--------------------------------------------------------  MW-BALANCED-PARENTS?

#|
(defUn mw-balanced-parents? (text)
  "rough check..."
  (unless (stringp text) 
    (error "arg: ~S should be a string." text))
  (when (equal text "")
    (return-from mw-balanced-parents? nil))
  (let ((right 0)(left 0))
    (dotimes (jj (length text))
      (cond
       ((equal #\( (char text jj)) (incf left))
       ((equal #\) (char text jj)) (incf right))))
    (>= right left)))
|#

#|
CG-USER(151): (moss::mw-balanced-parents? "ljljkh")
T
CG-USER(152): (moss::mw-balanced-parents? "(ljljkh")
NIL
CG-USER(153): (moss::mw-balanced-parents? "(ljljkh)")
T
CG-USER(154): (moss::mw-balanced-parents? "(ljl (jkh))))")
T
CG-USER(155): (moss::mw-balanced-parents? 12)
Error: Funcall of "arg: ~S should be a string." which is a non-function.
[condition type: SIMPLE-ERROR]
CG-USER(156): (moss::mw-balanced-parents? "")                                         
NIL
|#
;;;----------------------------------------------------------  MW-BROWSE-ON-CLICK

(defUn mw-browse-on-click (mw mw-out)
  "launches browser"
  ;; reads the selected number of the answer pane, error if not a number
  (let ((input-string (get-selection mw-out))
        test errno input candidate)
    (print `("MW-BROWSE-ON-CLICK ===> (selection? mw-out)" ,(selection? mw-out)))
    (print `("MW-BROWSE-ON-CLICK ===> input-string" ,input-string))
    (print `("MW-BROWSE-ON-CLICK ===> *package*" ,*package*))
   ;; protect execution
    (multiple-value-setq (test errno)
      (ignore-errors
       (cond
        ((or (null input-string)
             (equal input-string ""))
         (hemlock-interface:beep)
         (ask-okcancel "Empty selection?"))

        ((numberp (setq input (read-from-string input-string)))
         ;(print `("MW-BROWSE-ON-CLICK ===> input" ,input))
         ;(print `("MW-BROWSE-ON-CLICK ===> query-last-results" ,(query-last-results mw)))
         ;(print `("MW-BROWSE-ON-CLICK ===> current-entity" ,
         ;                           (cdr (assoc input (query-last-results mw)))))
         ;(print `("MW-BROWSE-ON-CLICK ===> *package*" ,*package*))
         (setq candidate (cdr (assoc input (query-last-results mw))))
         (browse candidate)
         )

        (t
         (hemlock-interface:beep)
         (ask-okcancel (format nil "Can't do anything with input: ~S" input)))
        )))
    ;; test for errors
    (if (and (null test) errno)
        (hemlock-interface:beep))
    )
  t)

;;;------------------------------------------------------  MW-CLEAN-BOX-ON-CHANGE

(defUn mw-clean-box-on-change (mw mw-out new-value)
  "when flag set, then clean the answer box before printing a new result"
  (setf (clean-pane-before-printing mw) new-value)
  (cond
   ((eql new-value 1) ; 1 is same as true for Tk
    (setf (text mw-out) "")
    (setf (clean-pane-before-printing mw) t))
   (t 
    (setf (clean-pane-before-printing mw) nil))
   )
  t)

;;;----------------------------------------------------------- MW-CLEAR-ANSWER-IF

#|
(defun mw-clear-answer-if (dialog)
  "clean pane if flag is set"
  (let ((pane (cg:find-component :answer-pane dialog))
        (flag (cg:value (cg:find-component :clean-box dialog)))
        )
    (if flag (setf (cg:value pane) ""))))
|#

;;;----------------------------------------------------  MW-CLEAR-ANSWER-ON-CLICK

(defUn mw-clear-answer-on-click (mw-out)
  "clear the answer pane"
  (clear-text mw-out)
  ;; reset gauge
  ;(setf (cg:value (find-component :entity-gauge dialog)) 0)
  t)

;;;-------------------------------------------------------- MW-CONTEXT-ON-CHANGE

(defUn mw-context-on-change (win mw-context-set mw-out)
  "sets up displaying context
Arguments:
  win: moss window
  new-value: a string specifying the context
  mw-out: output area"
  (declare (ignore win)(special *context*))
  (let* ((new-value (text mw-context-set))
         (context-value (read-from-string new-value)))
    (if (catch :error (moss::%%allowed-context? context-value))  
        (progn 
          (setq *context* context-value)
          (setf (text mw-out)
            (format nil "*** context is now: ~S ***" context-value))
          )
      (progn
        (format-wish "bell")
        (ask-okcancel 
          (format nil "*** ~S is an illegal context ***" new-value))
        ;; should reset the value to *context*
        (setf (text mw-context-set) *context*)
        (return-from mw-context-on-change nil)
        ))
    t))

;;;------------------------------------------------------- MW-DB-CONTENT-ON-CLICK

(defUn mw-db-content-on-click ()
  "creates a specific window for displaying the classes, instances and objects"
  (ask-okcancel "Currently not available"))

#|
(defun mw-db-content-on-click (dialog widget)
  "prints the structure of the database"
  (declare (ignore widget)(special *moss-window*))
  (mw-clear-answer-if dialog)
  (if *database-pathname*
      (let ((*moss-output* (cg:find-component :answer-pane dialog)))
        (db-vomit (intern (package-name *package*) :keyword)))
    ;; otherwise complain
    (let ((answer-pane (cg:find-component :answer-pane dialog)))
      (cg:beep)
      (setf (cg:value answer-pane) "*** No database opened ***")
      )
    ))
|#

;;;------------------------------------------------------ MW-DB-LOAD-ALL-ON-CLICK
(defUn mw-db-load-all-on-click ()
  "creates a specific window for displaying the classes, instances and objects"
  (ask-okcancel "Currently not available"))

#|
(defun mw-db-load-all-on-click (dialog widget)
   "unused"
   (declare (ignore widget))
   (let ((answer-pane (cg:find-component :answer-pane dialog)))
      (cg:beep)
      (setf (cg:value answer-pane) "*** Currently unused ***")
      ))
|#
	  
;;;----------------------------------------------------- MW-DB-STRUCTURE-ON-CLICK
(defUn mw-db-structure-on-click ()
  "creates a specific window for displaying the classes, instances and objects"
  (ask-okcancel "Currently not available"))

#|
(defun mw-db-structure-on-click (dialog widget)
  "print the content of database"
  (declare (ignore widget)(special *moss-window*))
  (mw-clear-answer-if dialog)
  (if *database-pathname*
      (let ((*moss-output* (cg:find-component :answer-pane dialog)))
        (db-show-structure))
    ;; otherwise complain
    (let ((answer-pane (cg:find-component :answer-pane dialog)))
      (cg:beep)
      (setf (cg:value answer-pane) "*** No database opened ***")
      )
    ))
|#

;;;--------------------------------------------------------  MW-DISPLAY-MORE-INFO

(defUn mw-display-more-info (mw-out text)
  "adds some text to the end of the dialog :answer-pane.
Arguments:
   mw-out: output pane
   text: string to add
Return:
   t"
  (append-text mw-out text)
  t)

;;;------------------------------------------------------------- MW-EXIT-ON-CLICK
;;; this function replaces the user-close function of the window, because user-close
;;; has some problems when running a project, and most of the time is not activated

#|
(defun mw-exit-on-click (dialog widget)
  "replace the close button of the window."
  (declare (ignore widget))
  ;; get the gate info and open the gate
  (let ((gate (gate dialog)))
    (if gate (mp:open-gate gate))
    ;; this should kill the process
    (close dialog))
  t)
|#

;;;---------------------------------------------------------  MW-EXECUTE-ON-CLICK

(defUn mw-execute-on-click (mw mw-in mw-out)
  "if  nothing is selected in the output pane, reads the content of the input frame, ~
  evals it and prints it into the answer pane. If something is selected in the ~
  answer pane, uses this value." 
  (let* ((input-string (text mw-in))
         ;start end
         )
    ;(print `(mw-execute-on-click interaction-mode ,(interaction-mode mw)))
    (case (interaction-mode mw)
      ;; when the interaction mode is lisp, then the argument should be a lisp exper
      (:lisp
       ;(print `(input-string ,input-string))
       ;(print (last input-string))
       ;; look for anything selected in the answer pane
       ;(multiple-value-setq (start end)(cg::get-selection answer-pane))
       ;; if end is different from start, then we have something selected
       ;(if (> end start)
       ;; then recover it
       ;(setq input-string (selected-object answer-pane)))
       ;; if nothing is selected use the value from input pane
       
       (mw-process-lisp-expr mw mw-out input-string)
       ;; write input-string back into the input pane
       ;(setf (text mw-in) input-string)
       ;; select input from input pane and set focus
       (set-focus mw-in)
       (select-all mw-in)
       )
      (otherwise
       (ask-okcancel "EXECUTE can only be called in DATA mode"))
      )
    t))

#|
(progn
  (setq *context* 0 *legal-tag-names* '("français" "English") 
        *package* (find-package :moss) *current-language* "English")
  (with-nodgui () (make-moss-window *tk* "family")))
|#
;;;--------------------------------------------------------- MW-FIX-MODE-ON-CLICK

(defUn mw-fix-mode-on-click (mw val mode mw-in mw-out)
  (declare (ignore val mw-out)(special *package*)) 
  (cond
   ((equal mode "data")
    (setf (interaction-mode mw) :lisp)
    (setq *package* (find-package (application-package mw)))
    ;; tell the user that we are now in query mode
    (setf (text mw-in) 
          (format nil "*** Now in LISP mode in package ~A ***"
                  (package-name *package*))))
   
   ((equal mode "help")
    (setf (interaction-mode mw) :conversation)
    (with-package (application-package mw)
      ;; if no conversation exist, then create one
      (unless (conversation mw)
        (setf (conversation mw)
              (moss::start-conversation nil :input mw-in :output mw)))
      (setf (text mw-in)     
            "*** Now in conversation mode (end input with period or question mark) ***")
      ))
   
   ((equal mode "query")
    (setf (interaction-mode mw) :query)    
    (setf (text mw-in) 
          (format nil "*** Now in query mode in package ~A ***
   End your query with Return."
                  (package-name *package*)))
    )
   )
  ;; select input from input pane and set focus
  (set-focus mw-in)
  (select-all mw-in)
  t)


;;;----------------------------------------------------------- MW-INPUT-ON-CHANGE

(defUn mw-input-on-change (event mw mw-in mw-out mw-progress)
  "Called whenever the content of the input pane (a string) changes.
  If we are in query mode or lisp mode (data) and if we have balanced parents ~
  (more parents that close than that open), we execute the expr.
  If we are in conversation mode (help), we check whether the new char is a ~
  period or a question mark, if so we terminate input and process it.
  Arguments:
  event: a vector representing the event
  mw: the moss window
  mw-in: input pane
  mw-out: output pane
  mw-progress: progress bar"
  (let ((last-char (slot-value event 'char))
        (text (text mw-in))
        )
    (format t "last char: ~S, text: ~S" last-char text)(finish-output)
    (format t "interaction mode: ~S" (interaction-mode mw))(finish-output)
    (case (interaction-mode mw) 
      (:query
       (cond 
        ;; dbg  (make sure clause is nil)
        ((and
          (string-equal last-char "Return")
          (print `("mw-input-on-change query option, *package*:" ,*package*))
          ;; remove the trailing return and possibly leading spaces
          (setq text  (string-trim '(#\space #\return #\newline) (text mw-in)))
          (mw-is-valid-lisp-expr? text))
         ;; go process query
         (moss::catch-system-error "while processing query" 
                                   (mw-process-query mw mw-in mw-progress mw-out))
         ;; select the text from the input pane
         (select-all mw-in)
         (focus mw-in)
         )))
      
      (:lisp
       ;(print `("mw-input-on-change lisp option, *package*:" ,*package*))
       (cond
        ;; a return terminates the input
        ((and  (string-equal last-char "Return")
               (mw-is-valid-lisp-expr? text))       
         ;; OK, remove the last char; posting it is not advised (addition of \")
         (setq text (subseq text 0 (1- (length text))))
         ;; process lisp expr, ship text string along
         (mw-process-lisp-expr mw mw-out text)
         ;; select the text from the input pane
         (select-all mw-in)
         (focus mw-in)
         )))
      
      
      (:conversation
       (with-package :moss
         (cond 
          ;; a period or a question mark terminate the input
          ((or (string-equal last-char "period")(string-equal last-char "question"))
           (print `("mw-input-on-change conversation, input text:" ,text))
           (print `("mw-input-on-change conversation, *package*:" ,*package*))
           ;; remove the last char
           (setq text (subseq text 0 (1- (length text))))
           ;; and post it
           (setf (text mw-out) text)
           (setf (user-input mw) text)
           ;; process (segment) text and update conversation object
           (moss::process-input (conversation mw) text)
           ;; call crawler
           (moss::crawler (conversation mw))
           ))))
      
      ;; otherwise process the key stroke by doing nothing
      )
    ;; return t to accept changes ?
    t))


#|
(with-package (application-package moss::*moss-window*)
  (with-nodgui (:debug 3) 
    (make-moss-window :master *tk* :application-name "Family" 
                      :application-package "FAMILY"
                      :title "Family Ontology" )))
|#
;;;------------------------------------------------- MW-INTERNAL-FORMAT-ON-CHANGE

(defUn mw-internal-format-on-change (mw mw-out new-value)
  "sets up a flag for printing internal format of objects"
  (declare (ignore mw mw-out)(special *print-internal-format*))
  (setq *print-internal-format* (if (eql new-value 1) t))
  t)

;;;----------------------------------------------------- MW-IS-LAST-CHAR-NEWLINE?

#|
(defUn mw-is-last-char-newline? (text)
  "checks if last char is newline or return"
  (and (> (length text) 0)
       (member (char text (1- (length text))) '(#\return #\newline) 
               :test #'equal)))
|#

;;;------------------------------------------------------- MW-IS-VALID-LISP-EXPR?

(defUn mw-is-valid-lisp-expr? (text)
  "checks if the input string can be read as a lisp expression. Brute force."
  (let (test errno)
    (multiple-value-setq (test errno)
      (ignore-errors
       (read-from-string text)))
    (if (or test (not errno)) t)))

#|
(mw-is-valid-lisp-expr? "t")
T
(mw-is-valid-lisp-expr? "")
NIL
(mw-is-valid-lisp-expr? "(+ 2 3)")
T
(mw-is-valid-lisp-expr? "(+ 2 ")
NIL
|#
;;;--------------------------------------------------------- MW-OVERVIEW-ON-CLICK

(defUn mw-overview-on-click (mw)
  "creates a specific window for displaying the classes, instances and objects"
  ;(ask-okcancel "Currently not available"))
  ;; build the tree of class names
  (with-package (application-package mw)
  (let ((forest (moss::%make-entity-tree-names 
                 (moss::%make-entity-tree))))
    (make-overview-window forest mw (application-package mw)))))


#|
(defUn mw-overview-on-click (dialog widget)
  "creates a specific window for displaying the classes, instances and objects"
  (declare (ignore widget))
  ;; build the tree of class names
  (let ((forest (moss::%make-entity-tree-names 
                 (moss::%make-entity-tree))))
    ;; display with concepts in alphabetic order
    (ow-make-outline-window  
     (mw-gg (onto-sort (mw-ff forest))) dialog
     :title (format nil "MOSS ~A - ~A -ONTOLOGY and KNOWLEDGE BASE"
              *moss-version-number* (package-name *package*))
     :database (database dialog)
     :moss-window dialog
     :owner (owner dialog) ; the first arg is unused by ow-make-outline-window
     )))
|#

#|
(defUn mw-ff (ll &aux lres)
  (loop 
    (unless ll (return-from mw-ff (reverse lres)) )
    (cond
     ((and (cadr ll)(listp (cadr ll)))
      (push (cons (car ll) (mw-ff (cadr ll))) lres)
      (pop ll) (pop ll))
     (t (push (pop ll) lres)))))
|#

#|
(defUn mw-gg (ll &aux lres)
  (loop
    (unless ll (return-from mw-gg (reverse lres)) )
    (cond
     ((listp (car ll))
      (push (caar ll) lres)
      (push  (mw-gg (cdar ll)) lres)
      (pop ll))
     (t (push (pop ll) lres)))))
|#

#|
(defUn onto-sort (ll)
  (cond ((null ll) nil)
        ((null (member-if #'listp ll)) (sort ll #'string<=))
        (t (mapcar #'cdr 
             (sort 
              (mapcar #'(lambda (xx)
                          (if (stringp xx) 
                              (cons xx xx)
                            (let ((carxx (car xx))
                                  (yy (onto-sort (cdr xx))))
                              (cons carxx (cons carxx yy))
                              )))
                ll)
              #'string<= :key #'car)))
        ))
|#

;;;--------------------------------------------------------- MW-PACKAGE-ON-CHANGE

(defun mw-package-on-change (mw widget new-value mw-out)
  "on change reset package to the one specified."
  (declare (special *package*))
  (let* ((package (find-package (string-upcase new-value))))
    (if package
        (progn
          (append-text mw-out
           (format nil "...Current package changed to: ~S." 
             (string-upcase new-value)))
          (setq *package* package)
          (setf (application-package mw) package)
          )
      (progn
        (hemlock-interface:beep)
        (ask-okcancel
         (format nil "...Can't find the specified package: ~S." new-value))
        ;; restore old value
        (setf (text widget)  (package-name (application-package mw)))))))

;;;--------------------------------------------------------- MW-PROCESS-LISP-EXPR

(defUn mw-process-lisp-expr (mw mw-out text)
  "takes expr, tries to evaluate it. Traps errors. Prints result into output pane.
  Arguments:
  mw-out: output pane
  text: string representing a lisp expression to evaluate
  Return:
  t"
  (let* (expr test errno)
    ;; if cleaning pane, old text will be erased
    (if (clean-pane-before-printing mw)(setf (text mw-out) ""))
    ;(print `(mw-process-lisp-expr expr ,expr))
    ;; evaluate expr catching possible errors
    (multiple-value-setq (test errno)
      (ignore-errors
       ;; to avoid crash on unmatched parents
       (setq expr (read-from-string text)) ; JPB1605
       (print `(mw-process-lisp-expr expr ,expr evaled ,(eval expr)
                                     moss::*moss-window* ,moss::*moss-window*))
       (eval expr)))
    ;(print `(mw-process-lisp-expr test ,test errno ,errno))
    ;; if test is nil and errno is not, then print error
    (cond
     ((and (null test) errno)
      (append-text mw-out
            (format nil "~%*** Error trying to evaluate: ~A  ~A" 
                     text (type-of errno)))
      )
     ;; otherwise print result
     ((string-equal (text mw-out) "Return")
      ;; no new line if pane is empty
       (append-text mw-out (format nil "$ ~A~S" text test))
      )
     (t
       (append-text mw-out (format nil "~%$ ~A~S" text test))
      ))
    t))

;;;------------------------------------------------------------  MW-PROCESS-QUERY
;;; when querying user classes must get rid of system classes
#|
(progn
  ;(defparameter *text*)
  (setq *context* 0 *package* (find-package :moss))
  (with-nodgui (:debug 3) 
     (make-moss-window :master *tk* :application-name "Family" 
                       :application-package "FAMILY"
                       :title "Family Ontology" ))
  )
|#

(defUn mw-process-query (mw mw-in mw-progress mw-out)
  "reads a query from the input pane. Checks its syntax. If OK, prints the result ~
  into the answer pane. Uses a number of functions from the query package.
  Search can be interrupted by clicking the ABORT QUERY button.
  This callback executes within the package in which the window has been created.
  Arguments:
  mw: moss-window structure
  mw-in: input pane
  mw-progress: progress indicator
  mw-out: output pane"
  (let ((gauge mw-progress)
        (ctr 0)
        expr parsed-expr candidates result gauge-max checking-info text)
    (moss::with-package (application-package mw)
      (print `("mw-process-query *package*" ,*package*))
      (setq text (string-trim '(#\space #\return #\newline) (text mw-in)))
      (print `("mw-process-query text" ,text))
      (setq expr (read-from-string text nil nil))
      (print `("mw-process-query input expr" ,expr))
      (print `("mw-process-query input mw-out" ,mw-out))
      (append-text mw-out  (format nil "~%$-1 ~S~%~S" expr parsed-expr))
      (sleep 1)
      
      ;; if querying user classes call special MOSS method
      (when (and
             (listp expr)
             (or (eql (car expr) (intern "MOSS-CONCEPT")) ; symbol
                 (moss::equal+ (car expr) "moss concept")
                 (moss::equal+ (car expr) "moss-concept")))
        (setf (query-last-results mw) nil) 
        (clear-text mw-out)
        (setq candidates (remove (intern "$SYS") (moss::%get-application-classes)))
        (dolist (candidate candidates)
          (incf ctr)
          ;; save result onto the result list
          (push (cons ctr candidate) (query-last-results mw))
          ;; print info for each object
          (append-text mw-out 
                       (format nil "~%~3D - ~{~A~}" ctr (moss::send candidate 'moss::=summary))))
        (return-from mw-process-query t))
      
      ;; parse the expression
      ;(trace moss::parse-user-query)
      (setq parsed-expr (moss::parse-user-query expr))
      
      (append-text mw-out  (format nil "~%$-2 ~S~%~S" expr parsed-expr))
      ;; if error, then quits
      (unless parsed-expr 
        (append-text mw-out "...Can't understand the query.")
        (return-from mw-process-query nil))
      
      ;; reset gauge
      (setf (value gauge) 0)
      
      ;; if desired clean up the answer area
      (if (clean-pane-before-printing mw) 
          (setf (text mw-out) ""))
      ;; reset result list
      (setf (query-last-results mw) nil)
      ;(moss::mw-format "query: ~S" expr)
      ;; build an initial set of objects to examine: candidates
      (setq candidates (moss::collect-candidates parsed-expr))
      (qtformat mw mw-out  (format nil "~%$-3 ~S~%~S" expr candidates))
      
      ;(moss::mw-format "parsed query; ~S ~%candidates: ~S" "candidates" candidates)
      ;; if the resulting candidate-list is *none*, then we have no solutions
      (when (or (eq candidates 'moss::*none*)  ; early failure detected by find-subset
                (null candidates)) ; no instances in this class ??
        (append-text mw-out "...Could not find anything.")
        (return-from mw-process-query nil))
      
      ;; here we have a list of candidates. If the query was an entry point then
      ;; we do not need to filter anything (validate will return immediately)
      ;; otherwise we must prepare filters that will be used for each candidate
      (unless (or (symbolp parsed-expr)(stringp parsed-expr))
        (setq checking-info 
              (moss::split-query-and-compute-filters parsed-expr '(:all)))
        ;; reduce the list by applying local filter to each candidate
        (setq candidates (remove nil (mapcar (car checking-info) candidates)))
        ;(qtformat mw "candidates filtered locally:~% ~S" candidates)
        ;; if the resulting candidate-list is *none*, then we have no solutions
        (when (or (eq candidates 'moss::*none*)  ; early failure detected by find-subset
                  (null candidates)) ; no instances in this class ??
          (append-text mw-out "...Could not find anything.")
          (return-from mw-process-query nil)))
      
      ;; set up a progress gauge
      (setq gauge-max (length candidates))
      ;; when tracing tell what happens
      (qtformat mw mw-out "Examining ~S candidate(s)" gauge-max)
      ;; loop for filtering objects, and printing them
      (dolist (candidate candidates)
        ;(qtformat mw mw-out"...examining:~% ~S" candidate)      
        ;; check for abort condition
        (when (query-abort-condition mw)
          (setf (query-abort-condition mw) nil) ;reset flag
          (append-text mw-out "... Query aborted at user request...")
          (return-from mw-process-query nil))
        (incf ctr)
        ;; trace
        (qtformat mw mw-out "... candidate#~S: ~S (~{~S ~})" 
                  ctr CANDIDATE (moss::send candidate 'moss::=summary))
        (setq result (moss::validate2 candidate parsed-expr () checking-info))
        ;; update gauge
        (incf (value gauge)
              (ceiling (/ (* 100 ctr) gauge-max)))
        (when result
          ;; save result onto the result list
          (push (cons ctr candidate) (query-last-results mw))
          ;; print info for each object
          (append-text mw-out 
                       (format nil "~%~3D - ~{~A~}" ctr (moss::send candidate 'moss::=summary)))))
      
      ;; at the end push gauge to 100%
      (setf (value gauge) 100)
      t)))


;(trace mw-process-query)

#|
(defUn mw-process-query (&rest ll)
  "creates a specific window for displaying the classes, instances and objects"
  (ask-okcancel "Currently not available"))
|#

;;;----------------------------------------------------------------- MW-RESET-FONT

#|
(defUn mw-reset-font ()
  "resets the font back to the default in the input and output pane of the MOSS ~
   window."
  ;;;  (let ((ww *moss-window*)
  ;;;        input output)
  ;;;    (when (and ww (wptr ww))
  ;;;      (setq output (view-named :answer-pane ww)
  ;;;            input (view-named :input-pane ww))
  ;;;      ;; must clear window to affect default fonts
  ;;;      (set-dialog-item-text output "")
  ;;;      (set-dialog-item-text input "")
  ;;;      ;; now reset font
  ;;;      (ed-set-view-font output *mw-default-font*)
  ;;;      (set-view-font input *mw-default-font*)
  ;;;      :done)))
  nil)
|#

;;;--------------------------------------------------------------- MW-SELECT-TEXT

#| may not be usefull
(defUn mw-select-text (pane &key focus)
  "select text from input pane and eventually focus on it.
Arguments:
   pane: input pane (must be valid)
   focus (key): if true set focus on pane
Return:
   t"
  (let ((newline-count (count '#\newline (cg:value pane))))
    ;; we must add 1 for each newline in the text...
    (cg::set-selection pane 0 (+ newline-count (length (cg:value pane))))
    ;(sleep 0.4)
    (when focus 
      ;; we cannot set focus directly otherwise button is not updated
      ;; we create a process (birrrkkkhhh!) to do so
      (mp:process-run-function  
       (list :name "focus process" :initial-bindings cg:*default-cg-bindings*)
       #'mw-select-text-set-focus pane)
      )
    t))
|#

;;;----------------------------------------------------- MW-SELECT-TEXT-SET-FOCUS

#|
(defUn mw-select-text-set-focus (pane)
  (sleep .2) ; wait 2/10s
  (cg:set-focus-component pane)
  (mp:process-kill sys:*current-process*))
|#

;;;---------------------------------------------------- MW-SET-LANGUAGE-ON-CHANGE

(defUn mw-set-language-on-change (new-value mw-out)
  "set the language to the new value"
  (declare (ignore mw-out)
           (special *language*))
  (setq *language* 
        (cdr (assoc new-value moss::*language-names-tags* :test #'moss::equal+)))
  t)

;;;-------------------------------------------------------- MW-START-CONVERSATION

#|
(defun mw-start-conversation (dialog)
  "kludge: function called by mw-start-conversation-process to make sure we run ~
   the conversation process in the :moss package. calls start-conversation."
  (declare (special _main-conversation))
  (let ((*package* (find-package :moss)))
    (start-conversation _main-conversation :input dialog :output dialog)))
|#

;;;------------------------------------------------------- MW-TRACE-BOX-ON-CHANGE

(defUn mw-trace-box-on-change (mw mw-out button-state)
  "toggles the trace switch moss::*mw-trace-query*"
  (declare (ignore mw mw-out)(special moss::*mw-trace-query*))
  (setq moss::*mw-trace-query* (if (eql button-state 1) t))
  t)

;;;----------------------------------------------------- MW-TRACE-QUERY-ON-CHANGE

(defUn mw-trace-query-on-change (mw mw-out button-state)
  "toggles the trace switch moss::*mw-trace-query*"
  (declare (ignore mw-out))
  (setf (trace-query mw) (if (eql button-state 1) t))
  t)

;;;------------------------------------------------------------ MW-USE-BIGGER-FONT

#|
(defUn mw-use-bigger-font (&optional (size 18))
  "sets the font to a larger size in the input and output panes of the MOSS ~
   window."
  ;;;  (let ((ww *moss-window*)
  ;;;        mw-font input output)
  ;;;    (when (and ww (wptr ww))
  ;;;      (setq output (view-named :answer-pane ww)
  ;;;            input (view-named :input-pane ww))
  ;;;      ;; make new font 
  ;;;      (setq mw-font (subst size 11 *mw-default-font*))
  ;;;      ;; must clear window to affect default fonts
  ;;;      (set-dialog-item-text output "")
  ;;;      (set-dialog-item-text input "")
  ;;;      ;; now reset font
  ;;;      (ed-set-view-font output mw-font)
  ;;;      (set-view-font input mw-font)
  ;;;      :done)))
  (declare (ignore size))
  nil)
|#

:EOF