;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;21/05/25
;;;              M O S S - S A - P E R S I S T E N C Y (file persistency.lisp)
;;;
;;;===============================================================================

;;; (c) Jean-Paul Barthèss@HEUDIASYC, CNRS, UTC. See LICENSE file for permissions.


;;; This file in the previous MOSS versions used the WOOD storage mechanism for
;;; MCL, and the ALLEGRO CACHE for ACL. The CCL version uses the Redis database
;;; Most of the functions had to be rewritten since the Redis mechanism is
;;; quite different from the previous ones.

;;; In a first attempt we consider Redis as a single database, that is only 
;;; containing 1 ontology. Of course one could save several ontologies in the
;;; same database by prefixing the keys with the package prefix. A better
;;; approach would be to use Redis instances or clusters, although I am not
;;; sure of the interest of clusters for our purposes.

;;; Objects are saved using a key, being the object-id, and the 
;;; associated value being a string representing the value. An object is 
;;; recovered by loading the corresponding string and doing a read-from-string.
;;; Redis works as a server, installed on some machine and accessed via a
;;; socket. Since MOSS never erases objects completely, the only necessary 
;;; Redis commands are get and set

;;; Global variables that contain valuable data:
;;;   *APPLICATION-PACKAGE* package of the application and partition name
;;;   *DATABASE-STUB* list of objects acting as proxies to MOSS
;;;   *REDIS-CONNECTED* t if Redis sever is running and is available
;;;   *REDIS-INSTANCE* integer specifying the current database (Redis instance)

;;; Some functions assume that a database is opened and do not require a name
;;; argument. A name argument is a filename for MCL or a directory name for ACL

;;; Functions without name argument:
;;;  DB-ABORT-TRANSACTION () ; aborts a transaction, flushing the queue of commands
;;;  DB-CLOSE () ; closes the currently opened DB, sets *redis-instance* to nil
;;;  DB-COMMIT ; saves world to disk ending a transaction
;;;  DB-CREATE area &key (if-exists :error) ; activates a new Redis instance
;;;  DB-DELETE area ; kills the specified database
;;;  DB-DUMP (area &opt file-name) dumps the content of a partition
;;;  DB-EMPTY? () ; returns T if current database is empty
;;;  DB-ERASE key ; erase a key in the currently opened database
;;;  DB-EXISTS? area ; check for existence (from the Redis structure)
;;;  DB-EXPORT (?) not implemented yet
;;;  DB-EXTEND new-area &key (if-exists :error) ; creates a new Redis instance
;;;  DB-GET-INDEX area ; get the integer specifying the Redis instance (database)
;;;  DB-GET-KEYS area pattern ; get the list of keys corresponding to pattern
;;;  DB-GET-NAME () ; recover the database name of the currently opened database
;;;  DB-HANDLE () ; return index of currently opened database
;;;  DB-INIT-APP area ; bulk loads initial data
;;;  DB-LOAD key area ; gets the values associated with the key
;;;  DB-OPEN name ; actually sets *redis-instance* to the index of the instance
;;;  DB-OPENED? area ; checks if DB is opened
;;;  DB-RESET () ; clear all Redis instances (debugging function)
;;;  DB-RESTORE dump-file-pathname,db-pathname,area
;;;  DB-SAVE-ALL () ; saves an ontology, creating its database (MOSS standalone)
;;;  DB-SHOW-STRUCTURE () ; prints the names of the existing instances
;;;  DB-START-TRANSACTION () ; sets the beginning of a transaction until COMMIT
;;;  DB-STORE key,value ; stores the value associated with the key
;;;  DB-UPDATE-STRUCTURE op data ; updates the Resis instance structure
;;;  DB-VOMIT area &key output-file, print-values ; prints partition keys

;;; WARNING: to print the result into the Listener, one must set *moss-output* to T, 
;;;    otherwise the output is lost somewhere...

#|
2021
 0914 coning from MOSS-CCL
|#

(in-package :moss)

#|
; debug print into listener
(setq *moss-output* t *moss-window* nil)  
|#
;;;================================== Globals ====================================
;;; connect Redis server when launching MOSS

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; connect redis server when loading the file
  (unless (redis::connected-p)
    (redis:connect :host "127.0.0.1" :port 6379))
  (setq *redis* t)
  (setq *redis-connected* t)
  )

;;;============================= macros ==========================================

;;;----------------------------------------------------------- DB-WITH-TRANSACTION

(defMacro db-with-transaction (&rest body)
  `(progn ,@body (db-commit))
  )

;;;===============================================================================
;;;                              DB Functions
;;;===============================================================================
;;; All DB functions assume that we have a current instance whose number is saved
;;; in the *redis-instance* special variable. The variable is set when we execute
;;; the DB-OPEN function. Thus, when *redis-instance* is nil, this means that no
;;; database is opened.
;;; Redis DB:0 contains the description of the server structure associated to the
;;; keys :redis-instance-list :redis-counter :redis-free-numbers
;;; each instance when created saves its number with the key :redis-instance-number
;;; When a database is deleted, its number is added to the :redis-free-numbers and
;;; the current database becomes Redis DB:0 if it was the deleted database
;;; When DB functions operate on a specific instance, then the instance for 
;;; safety reasons must be a key (not the instance index)
;;; When a database is created or opened, it becomes the current database

;;;=============================== Creating functions ============================

;;;---------------------------------------------------------- DB-ABORT-TRANSACTION

(defUn db-abort-transaction ()
  "Flushes all previously queued commands in a transaction"
  (red:discard))

;;;---------------------------------------------------------------------- DB-CLOSE
;;; we cannot close the database, but we can tell the system not to use the disk
;;; backup

(defUn db-close ()
  "We do not disconnect the server, but set the current logical instance to nil.
Arguments:
   none
Return:
   NIL"
  (declare (special *redis-instance*))
  ;; we should save everything on disk before exiting
  (when (integerp *redis-instance*)
    ;; save everything on disk
    (red:save))
  ;; tell MOSS that no database is opened
  (setq *redis-instance* nil))

;;;--------------------------------------------------------------------- DB-COMMIT

(defUn db-commit ()
  "to avoid naming conflicts"
  (red:exec))

;;;--------------------------------------------------------- DB-COMMIT-TRANSACTION

;;;--------------------------------------------------------------------- DB-CREATE

(defUn db-create (area &key (if-exists :error))
  "creates an instance in the Redis server.  
Arguments:
  area (key): name of object store, e.g. \"test\" or :test
  if-exists (key): what to do if file exists (default :error) unused
Return:
  the index of the instance (integer)"
  (declare (ignore if-exists) 
           (special *redis-connected* *redis-instance*))
  (let (redis-list counter free-numbers name)
    ;; if server is not connected, complain
    (unless *redis-connected*
      (error "Redis server is not connected"))
    
    ;; make area a keyword
    (setq name (keywordize area))
    
    ;; see whether DB:0 has already some instance
    (red:select 0)
    (setq *redis-instance* 0) ; used by db-empty?
    (setq redis-list (red:get :redis-instance-list))
    
    ;;== if redis-list does not exist, then Redis was not initialized
    (unless redis-list
      ;; if DB:0 is not empty, then it is used for something else
      (unless (db-empty?) 
        (error "DB:0 is not empty and not structured for OMAS/MOSS"))
      ;; otherwise, initialize structural data in Redis db:0
      (red:set :redis-instance-list (setq redis-list "((:redis . 0))"))
      (red:set :redis-instance-number 0)
      (red:set :redis-counter 1)
      (red:set :redis-database-name :REDIS) ; to keep the keyword
      (red:set :redis-free-numbers nil) ; list of instance numbers to recycle
      (when (eql area :redis)
        (setq *redis-instance* 0)  ; setting current database
        (return-from db-create 0)))

    ;; otherwise get the list of instances
    (setq redis-list (read-from-string redis-list nil nil))
    (unless (alistp redis-list)
      (error "Null redis-instance-list in db:0 ~S" redis-list))

    (setq free-numbers (red:get :redis-free-numbers))
    (if (stringp free-numbers)
        (setq free-numbers (read-from-string (red:get :redis-free-numbers))))

    (cond
     ;;== if database instance already exists, error (should improve that)
     ((assoc name redis-list)
      (error  "database already exists as Redis db:~A." 
              (cdr (assoc name redis-list))))
     
     ;;== if server contains other database instances, then create this one
     (t
      ;; get counter for the created instance
      (cond
       (free-numbers
        ;; reuse one of the freed numbers
        (setq counter (pop free-numbers))
        (red:set :redis-free-numbers free-numbers))
       (t
        (setq counter (read-from-string (red:get :redis-counter) nil nil))
        (unless (numberp counter) (error "Bad counter value: ~S" counter))
        ;; update disk value
        (red:set :redis-counter  (1+ counter))))

      ;; set current database
      (setq *redis-instance* counter)
      (push `(,name . ,counter) redis-list)
      ;; save list
      (red:set :redis-instance-list redis-list)

      ;; open new instance
      (red:select *redis-instance*)
      ;; save its id and OMAS application or MOSS ontology name
      (red:set :redis-instance-number counter)
      (red:set :redis-database-name (format nil "~S" name))
      ;; return id
      (return-from db-create counter))
     )))

#|
? (red:flushall)  ; clear all instances
"OK"
? (db-create :redis)
0
? (db-create :family)
1
? (red:select 0)
"OK"
? (red:get :redis-instance-list)
"((:FAMILY . 1) (:TEST . 0))"
? (db-delete :family)
:DONE
? (db-vomit :redis :print-values t)
 === "REDIS-COUNTER" : 2
 === "REDIS-DATABASE-NAME" : :REDIS
 === "REDIS-FREE-NUMBERS" : (1)
 === "REDIS-INSTANCE-LIST" : ((:REDIS . 0))
 === "REDIS-INSTANCE-NUMBER" : 0
:DONE
? (db-create :test)
1
? (db-vomit :redis :print-values t)
 === "REDIS-COUNTER" : 2
 === "REDIS-DATABASE-NAME" : :REDIS
 === "REDIS-FREE-NUMBERS" : NIL
 === "REDIS-INSTANCE-LIST" : ((:TEST . 1) (:REDIS . 0))
 === "REDIS-INSTANCE-NUMBER" : 0
:DONE
? (db-vomit :test :print-values t)
 === "REDIS-DATABASE-NAME" : :TEST
 === "REDIS-INSTANCE-NUMBER" : 1
:DONE
? (db-load :REDIS-DATABASE-NAME)
:TEST
T
NIL
? (db-create :test)
> Error: database already exists as Redis db:1.
> While executing: DB-CREATE, in process Listener(4).
|#
;;;--------------------------------------------------------------------- DB-DELETE

(defUn db-delete (area &aux index)
  "deletes a Redis instance and updates structure.
Argument:
   area: a keyword specifying the partition to be cleared
Return:
   base-handle"
  (declare (special *redis-connected* *redis-instance*))

  (unless *redis-connected*
    (error "Redis is not connected."))

  (unless (setq index (db-get-index area))
    (error "Cannot get index (Redis instance) for the ~S database." area))

  (when (eql index 0)
    (warn "Erasing Redis db:0 is not advisable, since it contains the server ~
           instance structure")
    (return-from db-delete))

  ;; set database to erase as current database
  (red:select index)
  ;; erase database, which removes the Redis instance
  (red:flushdb)
  ;; update :redis-instance-list
  (db-update-structure :rem area)

  (cond
   ((eql *redis-instance* index)
      (setq *redis-instance* nil)) ; indicates that no database is opened
   ((integerp *redis-instance*)
      (red:select *redis-instance*))  ; otherwise, go back to current database
      )
  :done)

#|
(db-delete-area :redis)
; Warning: Erasing Redis db:0 is not advisable, since it contains the server instance structure
; While executing: DB-DELETE-AREA, in process Listener(4).
|#
;;;----------------------------------------------------------------------- DB-DUMP
;;; should be modified to include the date in the file name

(defUn db-dump (area &optional file-name) 
  "dumps pairs (key . value) into the specified file"
  (declare (special *redis-connected* *redis-instance*))
  
  (setq area (keywordize area))
  
  (unless *redis-connected*
    (warn ";*** Server not connected...")
    (return-from db-dump))
  
  (let ((index (db-get-index area))
        key-list out-file)
    ;; check if db exists
    (unless index
      (warn ";*** database ~S does not exist..." area)
      (return-from db-dump))
    
    ;; if database exists, select it
    (red:select index)
    ;; get the keys
    (setq key-list (red:keys "*"))
    ;; order them
    (setq key-list (sort key-list #'string<))

    (setq out-file  
          (string+ *application-directory* "applications/" area "-base-dump"
                   (get-current-date :compact t) ".lisp"))
    
    (with-open-file (stream (or file-name out-file) :if-does-not-exist :create
                            :direction :output :external-format :utf-8)
      (dolist (item key-list)
        (format stream "~%(~S . ~S)"  item (red:get item))))
    )
  :done)
  
#|
(db-dump :test)
> Error: File exists : "~/MOSS projects/TEST-base-dump.lisp"
> While executing: CCL::MAKE-FILE-STREAM, in process Listener(4).
|#
;;;--------------------------------------------------------------------- DB-EMPTY?

(defun db-empty? ()
  "returns T if the count of object is 0 (db should be opened)."
  (declare (special *redis-instance*))
  (unless *redis-instance*
    (error "No database opened."))
  (eql (red:dbsize) 0))

;;;---------------------------------------------------------------------- DB-ERASE
;;; we do not allow removing a key from a database that is not the current one

(defUn db-erase (key)
  "removes a key from the specified instance of the object store.
Arguments:
   key: key to erase
   area: Redis instance name
Return:
   key or nil when something wrong"
  (declare (special *redis-connected* *redis-instance*))

  (unless *redis-connected*
    (error "Redis is not connected."))

  (unless *redis-instance* 
    (error "No database opened"))
  
  (red:select *redis-instance*) ; to make sure
  
  (red:del key)
  key
  )

#|
? (db-open :redis)
0
? (db-store 'K001 "K001")
K001
? (moss::db-vomit :redis)
 === "K001"
 === "REDIS-COUNTER"
 === "REDIS-FREE-NUMBERS"
 === "REDIS-INSTANCE-LIST"
 === "REDIS-INSTANCE-NUMBER"
:DONE
? (moss::db-erase 'K001)
K001
? (moss::db-vomit :redis)
 === "REDIS-COUNTER"
 === "REDIS-FREE-NUMBERS"
 === "REDIS-INSTANCE-LIST"
 === "REDIS-INSTANCE-NUMBER"
:DONE
|#
;;;-------------------------------------------------------------------- DB-EXISTS?
;;; does not act on the current database

(defUn db-exists? (area)
  "checks if Redis instance exist.
Argument:
   area: Redis instance name
Return:
   index if exists, nil otherwise."
  (declare (special *redis-connected*))
  (unless *redis-connected*
    (error "Redis is not connected."))
  (db-get-index area))

#|
(db-exists? :redis)
0
(db-exists? :zzz)
NIL
|#
;;;--------------------------------------------------------------------- DB-EXPORT
;;; This function produces a flat file structured as follows:
;;; Name of the Redis server instance, e.g. :ADDRESS or :FAMILY
;;; Sequence of pairs (<key> . "<value>") content of the partition
;;; :EOF
;;; The name of the file should be <instance-name>-ONTOLOGY.lisp

(defUn db-export (area &key file-name (pattern "*") &aux index)
  "exporting the content of a specific database partition into a flat file.
  Arguments:
  area: partition to dump
  file-name: name of the resulting text file
  Return:
  :done"
  (declare (special *redis-connected*))
  ;; check whether the database exists
  (unless *redis-connected*
    (mformat "~%; Redis server is not connected.")
    (return-from db-export nil))
  
  (setq index (db-get-index area))
  (unless index
    (mformat "~%; No database with name ~S." area)
    (return-from db-export nil))
  
  ;; select the right instance
  (red:select index)
  ;; cook up a file name if needed
  (setq file-name 
        (or file-name (string+ *application-directory* "Applications/" area 
                               "-ONTOLOGY.lisp")))
  (when (probe-file file-name)
    (mformat "~%; File ~S already exists." file-name)
    (return-from db-export nil))

  ;; we dump the result into the specified file
  (with-open-file (ss file-name :direction :output
                   :if-does-not-exist :create :external-format :utf-8)
    (format ss ";;; Saved text output of the ~S ontology database." area)
    
    (dolist (item (red:keys pattern))
      (print (cons item (red:get item)) ss))
    
    (format ss "~%:EOF")
    )
  :done)
	
#|
;; resulting file is barthes/MOSS projects/applications/TEST-ONTOLOGY.lisp
(db-export :redis)
:done
(db-export :redis)
; File "~/MOSS projects/Applications/REDIS-ONTOLOGY.lisp" already exists.
NIL
|#
;;;--------------------------------------------------------------------- DB-EXTEND

(defUn db-extend (new-area)
  "identical to db-create when using Redis.
Arguments:
   new-area: name to qualify the new partition, usually that of a package
Return:
   database index if OK, NIL otherwise."
  (db-create new-area)) 

#|
(moss::db-extend :address)
2
|#
;;;------------------------------------------------------------------ DB-FUSE-EP
;;; Saved entry points are stripped of all references to objects outside the
;;; application. Thus, when reloading the application from disk, one needs to
;;; add references to objects that are not in the application package. E.g.
;;; =make-entry is defined in the MOSS package, but also in the application
;;; package; same for =summary; ...

(defUn db-fuse-ep (ep area)
  "function called by db-init-app when opening an object base and reconnecting ~
   stub. Fuses application and MOSS entries.
Argument:
   ep: entry-point
   area:: a keyword specifying the application database in the Redis server
Return:
   ep value."
  (let (app-ep-l ep-l res)
    ;; first load the local interpretation of entry point
    (setq app-ep-l (db-load ep))

    ;; ep-l is the value of the entry-point defined in the MOSS package
    (setq ep-l (symbol-value ep))
    
    ;; brute-force merge on inverse links
    (dolist (item ep-l)
      (if (%is-inverse-property? (car item))
        (push (cons (car item) 
                    (db-fuse-ep-values (cdr item)(cdr (assoc (car item) app-ep-l))))
              res)
        (push item res)
        ))
    (reverse res)))

#|
;In package FAMILY:
? method
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 METHOD))
 (MOSS::$ENAM.OF (0 MOSS::$FN COMMON-LISP-USER::$E-FN))
 (MOSS::$EPLS.OF (0 MOSS::$SYS.1 COMMON-LISP-USER::$E-SYS.1)))
? (db-load 'M_method)
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 METHOD)) (MOSS::$ENAM.OF (0 $E-FN))
 (MOSS::$EPLS.OF (0 $E-SYS.1)))

? (moss::db-fuse-ep 'method)
((MOSS::$TYPE (0 MOSS::$EP)) (MOSS::$ID (0 METHOD))
 (MOSS::$ENAM.OF (0 MOSS::$FN COMMON-LISP-USER::$E-FN $E-FN))
 (MOSS::$EPLS.OF (0 MOSS::$SYS.1 COMMON-LISP-USER::$E-SYS.1 $E-SYS.1)))
|#
;;;----------------------------------------------------------- DB-FUSE-EP-VALUES


(defUn db-fuse-ep-values (val app-val)
  "merges values, deleting duplivates across multiple versions. E.g.
   ((0 A B)(1 C)(3 D)) and ((0 A F G)(2 H J)) -> ((0 A B F G)(1 C)(2 H J)(3 D))
   No check is done on the validity of contexts. Order of values not preserved.
Arguments:
   val: version a-list
   app-val: application version a-list
Result:
   merged a-list."
  (let (res app-pair)
    (dolist (pair val)
      ;; get first pair of the value of the object
      (if (setq app-pair (assoc (car pair) app-val))
          ;; when it is also in the second argument
          (progn
            ;; buid a simgle value starting with the property
            (push (cons (car pair)
                        (delete-duplicates (append (cdr pair)(cdr app-pair))
                                           :test #'equal))
                res)
          (setq app-val (remove-if #'(lambda (xx) (eql xx (car pair))) app-val 
                                   :key #'car))
          )
        ;; otherwise keep value
        (push pair res))
      )
    (setq res (append (reverse res) app-val))
    ;; return result
    res))

#|
? (db-fuse-ep-values '((0 A B)(1 C)(3 D)) '((0 A F G)(2 H J)))
((0 B A F G) (1 C) (3 D) (2 H J))
|#
;;;------------------------------------------------------------------ DB-GET-INDEX

(defun db-get-index (name &aux redis-list)
  "get the Redis instance index corresponding to the partition name.
Arguments
  name: a string or keyword
Return:
  the index (integer)"
  (declare (special *redis-connected* *redis-instance*))
  (unless *redis-connected*
    (error "Redis server is not connected."))
  ;; open db:0, closing whatever was opened
  (red:select 0)
  ;; get redis-instance-list
  (setq redis-list (red:get :redis-instance-list))
  ;; if nil, Redis has not been initialized
  (unless redis-list 
    (when (integerp *redis-instance*)
      (red:select *redis-instance*))
    (return-from db-get-index nil))

  ;; otherwise, get a-list from string
  (setq redis-list (read-from-string redis-list nil nil))
  ;; return the index and reset to original instance
  (prog1
    (cdr (assoc (keywordize name) redis-list))
    ;; reopen what was opened, closing DB:0
    (when (integerp *redis-instance*)
      (red:select *redis-instance*)))
  )

#|
? (red:flushall)
"OK"
? (setq *redis-instance* nil)
NIL
? (db-create :redis)
0
? (db-open :redis)
0
? (db-create :family) ; redis is open and stays open
1
? (db-get-index :zzz)
NIL
? (db-get-index "Family") ; redis is open and stays open
1
|#
;;;------------------------------------------------------------------- DB-GET-KEYS

(defun db-get-keys (area pattern)
  "get the list of keys corresponding to pattern"
  ;; error if one cannot connect to the area
  (db-open (keywordize area))
  (red:keys pattern)
  )
  
#|
Examples of patterns
h?llo matches hello, hallo and hxllo
h*llo matches hllo and heeeello
h[ae]llo matches hello and hallo, but not hillo
h[^e]llo matches hallo, hbllo, ... but not hello
h[a-b]llo matches hallo and hbllo
|#
;;;------------------------------------------------------------------- DB-GET-NAME

(defun db-get-name ()
  "gets the name of the currently opened database."
  (declare (special *redis-instance*))
  (let (index name)
    ;; get index of current database
    (setq index (db-handle))
    (red:select 0) ; open :redis DNB:0
    (unwind-protect
        ;; extract the name from the :redis-instance-list
        (setq name
              (car (rassoc index 
                           (read-from-string (red:get :redis-instance-list)))))
      (red:select *redis-instance*)
      name)
    ))

#|
? (db-get-name)
:REDIS
? (db-open :family)
1
? (db-get-name)
:FAMILY
|#
;;;--------------------------------------------------------------------- DB-HANDLE

(defUn db-handle ()
  "return handle to the database, actually the index of its instance in the server.
   Checks that the current database index is the same as *redis-instance*.
Argument:
   none
Return:
   the index (integer)"
  (declare (special *redis-connected* *redis-instance*))
  ;; if not opened error
  (unless *redis-connected* (error "Redis server not connected."))

  (unless *redis-instance*
    (error "No database opened."))

  (let ((index (red:get :redis-instance-number)))
    (unless (stringp index)
      (error "Can't find :redis-instance-number in current database."))
    (setq index (read-from-string index))
    ;; compare to *redis-instance*
    (unless (eql index *redis-instance*)
      (error "index of current database ~S does not comply with *redis-instance* ~S"
             index *redis-instance*))
    index)
  )

#|
? (moss::db-handle)
1
? (setq *redis-instance* 0) ; break consistency
0
? (moss::db-handle)
> Error: index of current database 1 does not comply with *redis-instance* 0
> While executing: DB-HANDLE, in process Listener(4).
|#
;;;------------------------------------------------------------------- DB-INIT-APP
;;; Because we could have saved noisy data, in particular for entry points. We must 
;;; then fuse the existing entry points with the ones depending solely on the 
;;; application package

;(dformat-set :db-init 0)

(defUn db-init-app (area &optional agent-key)
  "opens and initialize the application environment for further processing. We ~
   load all concepts, attributes, properties, methods, with the exception of ~
   entry points. Current package must be application package.
Arguments:
   area: keyword, both the name of the partition and of the application package
   agent-key (opt): agent key that will be used as a prefix for retrieving objects
                    and for the database package
Return:
   :done"
  (setq area (keywordize area))
  (dformat :db-init 0 "== Entering db-init-app / agent-key: ~S" agent-key) 
  
  (let (prefix package sys-id moss-ep-list ep-list id val package-key)
    ;(setq agent-key :calendar)
    (setq prefix (if agent-key (string+ agent-key ":") ""))
    ;; first open the base (normally already opend when function is called)
    (db-open area)
    
    ;;=== OK here database is opened and partition exists
    
    ;; check package; CAREFUL: in OMAS package is agent package not area name!
    (setq package-key (or agent-key area))
    (setq package  
          (or (find-package package-key)
              ;; if does not exist, create it
              (make-package package-key :use '(:moss :cl :ccl))))
    ;; switch to application package
    (setq *package* package)
    ;; record package for MOSS (safety measure)
    (setq *application-package* *package*)
    (dformat :db-init 0 "~%; db-init-app /*package*: ~S" *package*)

    ;; create new package environment (in particular will create *ST*)
    (%create-new-package-environment package-key (symbol-name area))

    ;(break "db-init-app /returning from %create-new-package-environment")

    ;; need here to load minimal set of objects
    ;; first load the object symbol representing the application, e.g. ($SYS . 1)
    (setq sys-id (db-load (string+ prefix "*MOSS-SYSTEM*")))
    (unless sys-id
      (mformat "~%Database problem: can't load *MOSS-SYSTEM*")
      (return-from db-init-app))
         
    (setq val (db-load (string+ prefix "*VERSION-GRAPH*")))
    (unless val
       (mformat "~%Database problem: can't load *version-graph*"))
    ;; careful: NIL is a-list
    (if (and val (alistp val)) (set (intern "*VERSION-GRAPH*") val))

    (setq val (db-load (string+ prefix "*CONTEXT*")))
     (unless val
       (mformat "~%Database problem: can't load *context*"))
    (if (integerp val) (set (intern "*CONTEXT*") val))
    
    ;; load stub, i.e. load ($SYS . 1)
    (>> sys-id (db-load (string+ prefix sys-id)))
    ;(dformat :db-init 0 "~%; db-init-app 0 / sys-id: ~% ~S" (pprint (<< sys-id)))
   
    ;; get the list of MOSS entry points
    (setq moss-ep-list (%get-value *moss-system* '$EPLS))

    ;; load variables (includes *moss-system* and *ontology*)
    (dolist (var (%get-value sys-id '$SVL))
      (%ldif var))
    
    ;; load function definitions
    (dolist (fn (%get-value sys-id '$DFXL))
      (dformat :db-init 0 "~%; db-init-app /%ldif on ~S" fn)
      (setq id (%ldif fn))
      ;; eval function definition
      (eval (symbol-value id)))
    
    ;; load functions
    (dolist (fn (%get-value sys-id '$SFL))
      (%ldif fn :function))
    ;; fuse system entry points, load only entry points common to MOSS and app
    ;; first get the list of application entry points
    (setq ep-list (%get-value sys-id '$EPLS))
    (dformat :db-init 0 "~% db-init-app 1 / ep-list: ~% ~S" ep-list)
    (dformat :db-init 0 "~% db-init-app 1 / common objects: ~% ~S" 
            (intersection ep-list moss-ep-list))

    (dolist (ep (intersection ep-list moss-ep-list))
      (set ep (db-fuse-ep ep area)))
    (dformat :db-init 0 "~% db-init-app 2 / methods: ~% ~S" =make-entry)
    ;; load concepts
    (dolist (id (%get-value sys-id '$ENLS))
      (%ldif id))
    ;; load attributes
    (dolist (id (%get-value sys-id '$ETLS))
      (%ldif id))
    ;; load relations
    (dolist (id (%get-value sys-id '$ESLS))
      (%ldif id))
    ;; load inverse properties
    (dolist (id (%get-value sys-id '$EILS))
      (%ldif id))
    ;; load methods
    (dolist (var (%get-value sys-id '$FNLS))
      (%ldif var :method))
    
    (dformat :db-init 0 "== Exit db-init-app")
    ;(mformat "~%;*** Application ~A connected to MOSS ***" area)
    :done))

;;;----------------------------------------------------------------------- DB-LOAD
;;; one problem occurs when the value stored in the database is NIL, in which case
;;; we cannot differentiate whether the value was not in the database or whether it
;;; was there and it is null.

(defUn db-load (key &aux index)
  "load an object from the current database (*redis-instance*).
Argument:
   key: a symbol or pair
Return:
   2 or 3 values
     - the value of key as stored in the database or nil
     - T if the value was found in the database, NIL otherwise
     - a message in case partition was not found."
  (declare (special *redis-connected* *redis-instance*))
  
  (setq index (db-handle)) ; checks for opened database

  (unless (red:exists (string+ key))
    ;; if key does not exist, complain
    (return-from db-load (values nil nil "Key not found in the database")))
 
  (values (read-from-string (red:get (string+ key))) T nil)
  )
  
#|
? (db-load 'K001)
"test K001"
T
NIL
? (db-load 'zzz)
NIL
NIL
"Key not found in the database"
|#
;;;----------------------------------------------------------------------- DB-OPEN

(defUn db-open (name &aux index)
  "opens the database instance of server, not supposed to fail. 
Arguments:
   name (key): a string or keyword
Return:
   database (Redis instance) index (an integer)."
  (declare (special *redis-connected* *redis-insstance*))

  (unless *redis-connected*
    (error "Redis server not connected."))
  
  ;; select the right instance of database
  (setq index (db-get-index name))
  (unless index
     (error "database ~A does not exist." name))

  ;; set access to the database
  (red:select index)
  (setq *redis-instance* index)
  index
  )

#|
? (db-open :test)
> Error: database TEST does not exist.
> While executing: DB-OPEN, in process Listener(4).
? (db-open "FAMILY")
1
|#
;;;------------------------------------------------------------------- DB-OPENED?

(defUn db-opened? (name)
  "checks if database opened, meaning Redis is connected and instance is selected.
Argument:
   name: a keyword or string
Return:
   db-pathname if opened, NIL otherwise."
  (declare (special *redis-connected*))
  (and *redis-connected*
       (eql (db-handle) (db-get-index name))))

#|
(moss::db-opened? :test)
NIL
(moss::db-opened? "Family")
T
|#
;;;-------------------------------------------------------------------- DB-RESET

(defun db-reset ()
  "erase everything (for debugging purposes)"
  (declare (special *redis-instance*))
  (red:flushall)
  (setq *redis-instance* nil)
  :done)

;;;------------------------------------------------------------------ DB-RESTORE

(defUn db-restore (dump-file-name area &optional no-check)
  "restores a database partition by reloading it. Redis partition should exist ~
   and be empty.
Arguments:
   dump-file-name: file from which we want to restore
   area: name of the partition to initialize in the database, should be a keyword
Return:
   handle"  
  (let (pair)
    ;; open t
    (db-open area)
    (with-open-file (ss dump-file-name :direction :input 
                        :external-format :utf-8)
      (loop
        (setq pair (read ss nil nil))
        (when (or (null pair)(eql pair :eof)) (return))
        (if (listp pair)
            (db-store (car pair)(cdr pair) :no-commit t)
          (warn "+++ something wrong, pair is not a list: ~S" pair))
        ))
    ;; commit
    (db-commit)
    )
  ;; return
  (db-handle))

#|
(db-restore "financing-121126.dmp" 
            :name "SERVER" 
            :directory (omas-application-directory *omas)
            :area :FINANCING)
|#
;;;*---------------------------------------------------------------- DB-SAVE-ALL
;;; executed in the ontology package

(defUn db-save-all (area)
  "save the ontology and knowledge base into a Redis server. Normally, used ~
   after loading the ontology from a text file. The Redis instance should not ~
   exist.
   Used by MOSS in its standalone version.
Argument:
   area: name of the redis instance for saving the data
Return:
   :done"
  (declare (special *redis-connected* *redis-instance*))
  (let (index message)
    (unless *redis-connected*
      (mformat "Redis server is not connected, nothing saved.")
      (return-from db-save-all))
    
    ;; if database alrready exists, complain
    (when (db-exists? area)
      (mformat "Database already exists, nothing saved.")
      (return-from db-save-all))

    ;; otherwise create a new Redis instance
    (setq index (db-create area))
    (red:select index)

    (with-package *application-package*
      ;; save application, if error then we catch a string error message
      (setq message 
            (catch :error 
              (send (symbol-value (intern "*MOSS-SYSTEM*" *application-package*))
                    '=save)
              nil))
      ;; report what happened
      (if (stringp message)
        (mformat message)
        (mformat ";*** application saved, database still opened ***"))
      ))
  :done)

#|
? (db-delete :family)

? (db-save-all :family)
Database already exists, nothing saved.
NIL
? (db-vomit :family)

? (db-vomit :family :print-values t)

|#
;;;------------------------------------------------ DB-SAVE-ALL-INTO-NEW-PARTITION

#|
(defUn db-save-all-into-new-partition (database-pathname)
  "we want to save environment into an existing database, in the partition ~
   corresponding to the current package. If it already exists, we ask user if it ~
   must be reinitialized.
Arguments:
   database-pathname: pathname of the current database
Return:
   nil in case of failure, :done otherwise."
  (unless (db-open :db-pathname database-pathname)
    (return-from db-save-all-into-new-partition))
  
  (let ((area (intern (package-name *application-package*) :keyword))
        answer message)
    ;; check if partition exists
    (when (db-area-exists? area)
      (setq answer
            (#+MCL y-or-n-dialog #+MICROSOFT-32 y-or-n-p
             (format nil "Partition ~S already exists. Do you want to erase it?"
                     area)))
      ;; if no, get out of here
      (unless answer
        (return-from db-save-all-into-new-partition))
      ;; otherwise erase partition
      (db-clear-all area)
      )
    
    ;; here database is open, but area does not exist or was erased. (Re)create area
    (db-extend area)
    ;; save world
    (with-package *application-package*
      ;; save application, if error then we catch a string error message
      (setq message 
            (catch :error 
              (send (symbol-value (intern "*MOSS-SYSTEM*" *application-package*))
                    '=save)
              nil))
      ;; report what happened
      (if (stringp message)
        (mformat message)
        (mformat ";*** application saved, database still opened ***"))
      )
    ;; and exit
    :done))
|#

;;;------------------------------------------------------------- DB-SHOW-STRUCTURE

(defUn db-show-structure (&aux redis-list)
  "displays the structure of the currently opened server (partitions).
Argument:
   none
Return:
   :done"
  (declare (special *redis-connected* *redis-instance*))
  
  (when *redis-connected*
    (red:select 0)
    (setq redis-list (red:get :redis-instance-list))
    (when (stringp redis-list)
      (setq redis-list (read-from-string redis-list nil nil))
      (unless (alistp redis-list)
        (return-from db-show-structure nil))
      (mformat "~%===== Structure of the Redis server:")
      (dolist (item (reverse redis-list))
               (mformat "~%  ~S (~S)" (car item) (cdr item)))
      (mformat "~%=====")
    )
    (when (integerp *redis-instance*)
      (red:select *redis-instance*))
  :done))

#|
(setq *moss-output* t *moss-window* nil)
? (db-show-structure)
===== Structure of the Redis server:
  :TEST (0)
  :FAMILY (1)
=====
:DONE
|#
;;;---------------------------------------------------------- DB-START-TRANSACTION

(defUn db-start-transaction ()
  "starts a transaction when we want to modify a set of objects and keep the database ~
   consistent."
  (declare (special *redis-connected*))
  (unless *redis-connected*
    (error "Redis is not connected."))
  (red:multi)
  )

;;;---------------------------------------------------------------------- DB-STORE
;;; Redis stores everything as strings. Thus, there is a problem when storing a
;;; value that is a string unless we transform it into a double string

(defUn db-store (key value &key no-commit key-prefix)
  "store a key and associated value in the current database (normally the symbol ~
   package of the key). Commits the result unless no-commit is true. We do not ~
   allow a NIL key.
Aruments:
   key: a symbol
   value: any expr
   no-commit (key): if t don't commit
   key-prefix (key): string to prefix keys (used by omas)
Return:
   the key"
  (declare (special *redis-instance*))

  (unless *redis-instance*
    (error "No database opened"))

  (unless (%%is-id? key)
    (error "Key ~S must be a non nil symbol or a pair." key))

  (if (or (stringp value)(keywordp value))
      (setq value (format nil "~S" value)))
  
  (red:set (string+ key-prefix key) value)
  (unless no-commit (red:save))
  key)
  
#|
? (moss::db-create :test)
2
? (moss::db-open :test)
2
? (moss::db-store 'k001 "test K001")
? (moss::db-store 'k002 "test K002")
? (moss::db-store 'k003 "test K003")
? (moss::db-store '($e-person . 1) (moss::<< '($e-person . 1)))

? (moss::db-vomit :test)
 === "($E-PERSON . 1)"
 === "K001"
 === "K002"
 === "K003"
 === "REDIS-COUNTER"
 === "REDIS-INSTANCE-LIST"
 === "REDIS-INSTANCE-NUMBER"
(moss::db-vomit :test :print-values t)
 === "K001" : "\"test K001\""
 === "K002" : "\"test K002\""
 === "K003" : "\"test K003\""
 === "REDIS-INSTANCE-NUMBER" : "2"
:DONE

? (moss::db-store 'k004 "test K004" :key-prefix "FAC:")
MOSS::K004
in the space:
=== "FAC:K004" : "test K004"
|#
;;;----------------------------------------------------------- DB-UPDATE-STRUCTURE

(defun db-update-structure (op data)
  "updates the :redis-instance-list in instance 0 adding a new instance (database) ~
   or removing one.
Arguments:
  op: operation: :add or :rem
  data: the corresponding data
Return:
  the modified list"
  (declare (special *redis-connected* *redis-instance*))
  ;; make sure data is a keyword
  (setq data (keywordize data))
  (let (db-list index free-numbers)
    ;; set :redis area as the current database
    (red:select 0)
    (setq db-list (red:get :redis-instance-list))
    ;; get the a-list
    (unless (stringp db-list)
      (error "Value for :redis-instance-list ~S should be a string" db-list))

    (setq db-list (read-from-string db-list nil nil))
    (case op
      (:add
       (push data db-list))
      (:rem
       ;; get database index
       (setq index (cdr (assoc data db-list)))
       ;; remove entry, e.g. from the list
       (setq db-list (remove data db-list :key #'car))
       ;; add number to free numbers list
       (setq free-numbers (read-from-string (red:get :redis-free-numbers)))
       (pushnew index free-numbers)
       (red:set :redis-free-numbers free-numbers))
      (otherwise
       (error "bad operator ~S, should be :ADD or REM" op)))

    ;; update value in instance 0
    (red:set :redis-instance-list db-list)
    ;; restore previous environment (database)
    (if (integerp *redis-instance*)
        (red:select *redis-instance*))
    ))

#|
? (db-reset)
:DONE
? (db-create :redis)
0
? (db-create :family)
1
? (db-create :test)
2
? (db-vomit :redis :print-values t)
 === "REDIS-COUNTER" : 3
 === "REDIS-FREE-NUMBERS" : NIL
 === "REDIS-INSTANCE-LIST" : ((:TEST . 2) (:FAMILY . 1) (:REDIS . 0))
 === "REDIS-INSTANCE-NUMBER" : 0
:DONE
? (db-delete :test)
:DONE
? (db-vomit :redis :print-values t)
 === "REDIS-FREE-NUMBERS" : (2)
 === "REDIS-INSTANCE-LIST" : ((:FAMILY . 1) (:REDIS . 0))
 === "REDIS-INSTANCE-NUMBER" : 0
:DONE
? (db-create :test)
2
? (db-vomit :redis :print-values t)
 === "REDIS-COUNTER" : 3
 === "REDIS-FREE-NUMBERS" : NIL
 === "REDIS-INSTANCE-LIST" : ((:TEST . 2) (:FAMILY . 1) (:REDIS . 0))
 === "REDIS-INSTANCE-NUMBER" : 0
:DONE
|#
;;;---------------------------------------------------------------------- DB-VOMIT

(defUn db-vomit (area &key print-values dump (pattern "*"))
  "prints the entire  content of a database partition (for checkpointing). Uses ~
  *moss-output* as the output stream.
Arguments:
  area: a keyword specifying the partition to be dumped, e.g. :PUBLISHER
  print-values (key): if t prints the value along with the key
  dump (key): if non nil, dumps as pointed pairs
  pattern (key): pattern for filtering keys (default \"*\" = none)
Return:
  :done"
  (declare (special *redis-connected* *redis-instance*))
  (setq area (keywordize area))
  
  (unless *redis-connected*
    (warn ";*** Server not connected...")
    (return-from db-vomit))
  
  (let ((index (db-get-index area))
        key-list)
    ;; check if db exists
    (unless index
      (warn ";*** database ~S does not exist..." area)
      (return-from db-vomit))

    ;; if database exists, select it
    (red:select index)
    ;; get the keys
    (setq key-list (red:keys pattern))
    ;; order them
    (setq key-list (sort key-list #'string<))
    (dolist (item key-list)
      (cond
       ;; dumping amount to print pairs 
       (dump
        (mformat "~%(~S . ~S)"  item (read-from-string (red:get item))))
       ;; print key and value for checking purposes
       (print-values
        (mformat "~% === ~S : ~S" item (read-from-string (red:get item))))
       ;; print only the list of keys
       (t
        (mformat "~% === ~S" item))
       )
      )
    )
  (when (integerp *redis-instance*)
    (red:select *redis-instance*))
  :done)

#|
? (db-vomit :address)
; Warning: ;*** database :ADDRESS does not exist...
; While executing: DB-VOMIT, in process Listener(4).
NIL
:DONE

? (db-vomit :test)
 === "@K001"
 === "@K002"
 === "@K003"
 === "REDIS-COUNTER"
 === "REDIS-INSTANCE-LIST"
 === "REDIS-INSTANCE-NUMBER"
:DONE

? (db-vomit :test :pattern "@K*")
 === "@K001"
 === "@K002"
 === "@K003"
:DONE

? (db-vomit :test :print-values t)
 === "@K001" : "test K001"
 === "@K002" : "test K002"
 === "@K003" : "test K003"
 === "REDIS-COUNTER" : "2"
 === "REDIS-INSTANCE-LIST" : "((:FAMILY . 1) (:TEST . 0))"
 === "REDIS-INSTANCE-NUMBER" : "0"
:DONE
(db-vomit :test :dump t)
("@K001" . "test K001")
("@K002" . "test K002")
("@K003" . "test K003")
("REDIS-COUNTER" . "2")
("REDIS-INSTANCE-LIST" . "((:FAMILY . 1) (:TEST . 0))")
("REDIS-INSTANCE-NUMBER" . "0")
|#
;;;============================== tests ==========================================
;;; A special test file is available moss-persistency-test.lisp

;(format t "~&;*** MOSS v~A - persistency loaded ***" *moss-version-number*)

:EOF
