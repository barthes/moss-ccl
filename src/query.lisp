;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=================================================================================
;;;19/08/24
;;;		Q U E R Y (File: moss-query.lisp)
;;;
;;; First created December 1994
;;; Revised 2017 for MOSS v10
;;;=================================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
;;; The following subsystem is used to query a MOSS environment It queries by
;;; navigating from a particular object.
;;; Syntax for querying the system is the following:
;;;    (send *qh* '=access '<query> {t})
;;;    Arg1: query (parsed or unparsed)
;;;    Arg2: optional already-parsed, if true indicates that the query is parsed.
;;;    Returns a list of objects filtered by the constraints specified in the query."
;;;
;;; The syntax for the query is the following:
;;;                      (ENTITY (prop ...) 
;;;       "prop" can be a terminal or a structural property
;;; e.g.
;;; 	(PERSON (IS-AUTHOR-OF (> 0) (BOOK (HAS-TITLE is "La relativite"...]
;;;
;;; However the actual query uses internal names, which looks like
;;;	($E-PERSON ($S-AUTHOR.OF (> 0) ($E-BOOK ($T-TITLE IS "La ..."...)
;;;
;;; More formally:
;;;   <general-query> ::= <entry-point> | <query>
;;;   <query>         ::= (<class-ref> {<clause>}*) 
;;;                         [| <class> <class-variable> <clause>+)] ; not available
;;;   <clause>        ::= (<simple-clause>)
;;;                         | (<or-clause>) | (<or-constrained-sub-query>)
;;;   <simple-clause> ::= (<tp-clause>) | (<sub-query>)
;;;   <tp-clause>     ::= (<terminal-property> <tp-operator> <value>) 
;;;                            | (<terminal-property> <tp-operator> <variable>)
;;;   <sub-query>     ::= (<relationship> {<cardinality-condition>} <query>)
;;;   <relationship>  ::= <structural-property> | <inverse-property>
;;;   <or-clause>     ::= (OR <simple-clause>+)
;;;   <or-constrained-sub-query> ::= (OR <cardinality-condition> {<sub-query>}+)
;;;   <cardinality-condition> ::= (<numeric-operator> <arg>+)
;;;   <numeric operator> ::= < | <= | = | >= | > | <> | :BETWEEN | :OUTSIDE
;;;   <tp-operator>       ::=  < | <= | = | >= | > | <> | :BETWEEN | :OUTSIDE  
;;;                            | :IS | :IS-NOT | IN | ALL-IN
;;;                           | CARD= | CARD< | CARD<= | CARD> | CARD>=
;;;   <variable>       ::= any Lisp symbol starting with ? (e.g. ?albert)
;;;   <class-variable> ::= <variable>
;;;   <entry-point>    ::= symbol | string
;;;
;;;  where * means 0 or more, and + one or more.
;;;
;;; Negation is taken into account at 2 levels:
;;;   - within a tp-clause through a negative operator
;;;   - at the relationship level with a cardinality constraint (= 0)

;;; Examples:
;;;   access all persons
;;;   (person) or ("person")
;;;   access all males
;;;   (person (has-sex :is "m")) or ("person" ("sex" :is "m"))
;;;   access all person named "Barths" and not known to be males
;;;   (person (has-name is "Barths") (has-sex is-not "m"))
;;;   access all persons with brothers
;;;   (person (has-brother (person)))
;;;   access all persons with no (gettable or recorded) brothers (close world)
;;;   (person (has-brother (= 0) (person)))
;;;   access all persons who have only one brother called "Sebastien"
;;;   (person (has-brother (= 1) (person (has-first-name is "Sebastien"))))
;;;   access all persons, brother of a person called sebastien
;;;   (person (is-brother-of (= 1) (person (has-first-name is "Sebastien"))))
;;;   or, usint the inverse notation for strings:
;;;   ("person" (">brother" (=1) ("person" ("first-name" :is "Sébastien"))))
;;;   access all persons with at least 2 children of any sex
;;;   (person (OR (>= 2) (has-son (>= 0) (person))
;;;                      (has-daughter (>= 0) (person)))
;;;   access all persons with at least 3 children one of them being a son
;;;   (person (OR (>= 3) (has-son (>= 1) (person))
;;;                     (has-daughter (>= 0) (person)))
;;;   persons who are cousins of a person who has a father with name "Labrousse"
;;;   (person (is-cousin-of  
;;;                  (person (has-father 
;;;                                (person (has-name is "Labrousse"))))))
;;;   persons who have a brother who is cousin of a person who has a father...
;;;   (person (has-brother
;;;            (person (is-cousin-of
;;;                        (person (has-father 
;;;                                 (person (has-name is "Labrousse"))))))))
;;;   persons who have a father with more than 2 boys
;;;    (person (has-father (person (has-son (>= 2) (person)))))
;;;   persons who has  a father with  more than 3 children with at least one girl
;;;           and whose father has a brother
;;;   (person (has-father
;;;               (person   ; condition on the cardinality of the subset!
;;;                      (OR (> 3) (has-son (> -1) (person))
;;;                                (has-daughter (> -1) (person)))
;;;                      (has-brother (person)))))
;;;   person who have more than 1 child, of which one has first-name "camille" and
;;;     at least a daughter or
;;;     else, more than one daughter who is a student (answer jpb, dbb)
;;;   (person (or (> 1) (has-son (person (has-first-name is "camille")))
;;;                     (has-daughter (student))))
;;;   person who has a son with the same name as the son of his brother
;;;   (person (has-son  (person (has-first-name is ?X)))
;;;          (has-brother 
;;;                 (person (has-son  (person (has-first-name is ?X))))))

;;; Possible improvements: 
;;;   introduce a class variable to record instanciated variables for a given
;;;   class. E.g., ($PERS ?-23 (> 3) (OR ($SONS (> 0)($PERS ?-24 (> 0)))
;;;                                      ($DAUS (> 1)($PERS ?-25 (> 0)))))
;;;   meaning: find all persons who have more than 3 children and at least one
;;;   daughter. Finding such variables in the bindings would extract some values
;;;   i.e., the minimal number of values needed to satisfy the query conditions.
;;;   Another advantage would be to have a systematic syntax for queries.


;;; The query process is done in two steps:
;;; Step 1
;;; ======
;;;   Since the query starts with a list of target objects belonging to a given 
;;;   target class, it is necessary to determine a minimal list of such objects
;;;   to minimize the amount of work to do to obtain the answer. In particular, 
;;;   not all objects of the target class have to be examined.
;;;   During step 1, a temporary structure is built to determine a subset of the
;;;   original target class, using the relevant entry-points existing in the
;;;   query.
;;;
;;; Step 2
;;; ======
;;;   During Step 2, one proceeds, starting from every candidate of the obtained
;;;   subset, and checking the validity of the rest of the query by navigating 
;;;   following the specified relationships and applying the various conditions.

;;;1995   Introducing a new version 3.3.0
;;;  1/2  Changing the structure of a query by introducing class variables
;;;       Also the negation of a sub-clause is now part of the cardinality-
;;;       condition (e.g. (= 0) or (<= 0) or (< 1))
;;;       Having class variables obliges to pass a context along. The context
;;;       is implemented as an a-list
;;;       Example of a query new style
;;;         (STUDENT (HAS-COURSE (> 0) (COURSE ?X))
;;;                  (HAS-COUSIN (> 0) (PERSON (HAS-COURSE (> 0) (COURSE ?X)))))
;;;       meaning a student having a cousin that takes the same course
;;;  1/5  replacing mapcan with reduce #'append mapcar to avoid destructive
;;;       operations
;;;  2/15 Introducing treatment of entry points -> v 3.3.1
;;;  2/18 Adding class variables 
;;;       Renaming version to be consistent with MOSS 3.2 -> v 3.2.2
;;;       Replacing make-value-string with normalize-value in filters
;;;  2/19 Fixing some problems with the numerical arguments
;;;  2/20 Fixing split-query so that query clauses are kept in order
;;;  2/20 Fixing parsing of queries with OR clauses
;;;       Computation of the starting candidate list must be fixed.
;;;  2/22 Fixing find-subset in the treatment of OR branches
;;;  2/25 Changing make-local-filter to take entry-points into account
;;;       Changing =access-by-query to have a better dialog, and incremental 
;;;       display. Adding a few related methods
;;;       Defining *query-include-kernel-objects* to control inclusion of
;;;       kernel objects in the answer to a query, e.g. about classes
;;;  2/26 making make-local-filter return a list of values rather than
;;;       multiple values; and using local symbols for housing test functions
;;;  3/25 Collect1 should return the new binding when there is a sub-query
;;;       with a variable. The binding is returned through collect2 within collect1
;;;       However sub-queries like 
;;;                   (person (has-son (>= 2) (person (has-age is ?x))))
;;;       have little meaning, since one does not know to which son the variable
;;;       ?x will refer to.
;;;       Another question is that of the backtrack in case of failure, since
;;;       a (> 0) test will be satisfied with the first value.
;;;       Take the following question:
;;;                   (person (has-son (> 0) (person (has-age is ?x)))
;;;                           (has-daughter (person (has-age > ?x))))
;;;       If a person has 2 sons with age 15 10 and a daughter with age 13 the
;;;       query will fail.
;;;       Independently the meaning of the query is ambiguous. Does it mean that the
;;;       daughter must be older than one son or than the two of them. We should
;;;       ban the use of variables from the sub-request following a restiction
;;;       condition. The request could be rewritten as:
;;;                   (person (has-son (> 0) (person)
;;;                           (has-son (person (has-age is ?x)))
;;;                           (has-daughter (person (has-age > ?x))))
;;;       This states unambiguously that the daughter should be older than one of
;;;       the sons. If we want to state that the daughter must be older than every
;;;       son, then we have a problem.
;;;       In fact a subquery containing a variable to be set can succeed, but failed
;;;       later because another sub-query that used the assigned value. We must
;;;       then backtrack to the sub-query, remove the object to which belongs the
;;;       assigned value and take the next one. Could be done with a catch.
;;;       A sub-query returns a set, then we should be able to do operations on
;;;       this set: i.e.  (?x MIN has-age {set}). However LISP functions can be used
;;;       for that.
;;; 6/4   Trying a new algorithm -> V 3.3.0
;;;1997
;;; 02/18 Adding in-package for compatibility with Allegro CL on SUNs
;;; 02/25 Back to version 3.2.1. No variables are allowed.
;;; 05/17 Adding some explainations in the introductory comments
;;;2001
;;; 09/30 Adding some color 


#|
2004
 0716 reintroducing queries in the MOSS v 4.2 version
 0719 introducing cardinality operators on terminal values, e.g. person who has at 
      least 2 names:
         (person (has-name CARD>= 2))
      operators are CARD> CARD>= CARD= CARD>= CARD>
 0914 operators should be considered as markers within the queries. We modif $$Translate
      to replace symbols with keywords.
2005
 0306 using mformat in the last line
      modifying QFORMAT
      replacing mw-format by mformat
      updating keywordize
 0308 changing nv-check-simple-clause? to repair queries with OR clauses
 0328 modifying **translate-1 to take into account property trees
 0910 adding stream option to parse-moss-query (deprecated?)
2006
 0213 Version 6
      =========
      Values have been change e.g. to multilingual names. Thus, code has to be
      modified.
      Looks like the query system functions (without coreference variables)
 0221 allowing symbols arguments in parse-moss-query
 0811 replacing the ALL test with :all
 0813 rebuilt access functions entirely and reeorganizing the file
 1106 correcting $$translate-1
 1107 adding package arg to (ENTITY) =GET-INSTANCE-LIST
 1112 changing $$translate-1 to accommodate string names and packages
 1207 changing $$translate-1 to be able to use inverse notation ">title"
2007
 0220 rewriting the translation process for the queries (old $$translate functions)
 0221 removing dead objects from list returned by access
 0224 modified validate2 must return candidate or nil
 0226 when trying to access ("concept" ("concept-name" :is "person")) the function
      filter-candidates uses a filter:
 (LAMBDA (MOSS::XX)
     (AND (INTERSECTION '(MOSS::$ENT) (MOSS::%TYPE-OF MOSS::XX))
          (INTERSECTION '("PERSON")
                        (MAPCAR #'MOSS::NORMALIZE-VALUE
                                (MOSS::=> MOSS::XX '=GET-ID 'MOSS::$ENAM))
                        :TEST
                        #'EQUAL)
          MOSS::XX))
      However, (MOSS::=> MOSS::XX '=GET-ID 'MOSS::$ENAM) returns a multilingual 
      name (:en "person") that cannot be matched with "person". Furthermore,
      MLN may contain synonyms that must be exploded for comparison.
 0319 adding MLN to make-local-filter-clause
 0923 introducing some order in the different operators. All operators are in
      English and should be in the *legal-attribute-operators* list.
      cardinality operators should be in the *legal-cardinality-operators* list
 1015 changing the order of argument in predicate between...
2008
 0604 === v5.11.2
 0611 modify keywordize
 0621 === v7.0.0
 0705 cleaning the code somewhat
2009 
 0210 adding filter-objects for easier outside use than filteer-candidates
 1117 modifying %TRANSLATE-CLAUSE to find the correct inverse property
 0121 adding cardinality-operator? to service functions
2010
 0616 changing %defclass to defsysclass
 0917 introducing the MOSS prefix into the method selectors
 1010 added collect-check-subquery-resolve-ghost-links to take care of =if-needed
      demons, modified find-subset
2014
 03/07 -> UTF8 encoding
 06/29 correcting bug in find-subset for dealing with MLNs
 11/19 changing make-local-filter to process MLN with old format
2016
 0622 added check-not-clause that was missing
2017
 0610 upgrading to MOSS v10
|#

(in-package :moss)

;(format *standard-output* "~%;*** Loading MOSS queries V 4.2.1 ***")

#|
(eval-when (compile load eval) 
  (export '(*qh* *query-handler* access <> filter-objects))
  ;; trace everything
  ;(dformat-set :query 3)
  )
|#

;;;=================================== Macros ======================================
;;; unused

;;;(defMacro %g> (prop-id) `(%get-value *self* ,prop-id))
;;;(defMacro %g=> (obj-id prop-id) `(%get-value ,obj-id ,prop-id))
;;;(defMacro %cg> (prop-id) `(car (%get-value *self* ,prop-id)))
;;;(defMacro %cg=> (obj-id prop-id) `(car (%get-value ,obj-id ,prop-id)))

;;; used for removing references on co-reference variables

(defMacro zap-old (xx) `(setf (get ,xx :old) nil)) ; use in accesses

;;;============================== global variables =================================

#|
(defParameter *abort-qery* nil "It true access is aborted")

(defVar *query-trace-flag* nil "Flag used to trace details of the query process~
                                for debugging purposes")

(defVar *explain-flag* t "flag used to trace the behavior of the query system ~
                          - used for explaining purposes.")
#|
(defVar *grundy-level* 0 "Distance from the goal class within a query branch. ~
                                                    Used to indent explanations.")
|#
(defParameter *input* t "default moss channel")
(defParameter *output* t "default moss channel")
(defVar *query-allow-sub-classes* t "Flag allowing to include sub-classes")

(if  *query-allow-sub-classes*
  (format *standard-output* 
          "~%;*** Instances of sub-classes are also included, this can be changed
~&;     by setting the global variable *query-allow-sub-classes* to nil ***")
  (format *standard-output* 
          "~%;*** Instances of sub-classes are not included, this can be changed
~&;     by setting the global variable *query-allow-sub-classes* to t ***"))

(defParameter *filters* () "alist to cache the filters avoiding recomputation")

(defParameter *operators*
  '(:BETWEEN :OUTSIDE :IS :IS-NOT :EQUAL :NOT-EQUAL 
             :IN :ALL-IN :EST :N-EST-PAS :PP :PG :PPE :PGE
             :CARD< :CARD<= :CARD= :CARD>= :CARD>))

(defparameter *cardinality-operators*
  '(:CARD< :CARD<= :CARD= :CARD>= :CARD>))

(defParameter *numeric-operators*
  '(;; operators requiring numerical values
    :LESS-THAN :LT :LESS-THAN-OR-EQUAL :LTE
    :GREATER-THAN :GT :GREATER-THAN-OR-EQUAL :GTE
    :EQUAL :NOT-EQUAL < <= > >= = <>
    ;; range operators, unary: take a list representing a range
    :BETWEEN :OUTSIDE 
    ;; structural operators related to cardinality
    :CARD< :CARD<= :CARD= :CARD<> :CARD>= :CARD>))

(defParameter *legal-attribute-operators*
  ;; operators are in English and are keywords
  (append *numeric-operators*
          '(:IS :IS-NOT :IN :ALL-IN :NOT-IN)))

;;;(defParameter *legal-attribute-operators*
;;;  ;; operators are in English and are keywords
;;;  '(:LESS-THAN :LT :LESS-THAN-OR-EQUAL :LTE
;;;    :GREATER-THAN :GT :GREATER-THAN-OR-EQUAL :GTE
;;;    :EQUAL :NOT-EQUAL :IS :IS-NOT
;;;    ;; range operators
;;;    :BETWEEN :OUTSIDE 
;;;    :IN :NOT-IN :ALL-IN :NOT-ALL-IN
;;;    ;; structural operators related to cardinality
;;;    :CARD< :CARD<= :CARD= :CARD<> :CARD>= :CARD>))

(defParameter *legal-cardinality-operators*
  '(< <= = <> > >= :between :outside))

(defParameter *assignment-operators* '(:is = :equal))

(defVar *query-handler* () "Current instance of the query handler")
(defVar *qh* () "Abbreviated name of the current instance of the query handler")

(defVar *query-include-kernel-objects* nil 
  "Flag allowing to include kernel objects as an answer to the query, e.g. ~
   meta-classes.")
|#

;;;=============================== debugging help ==================================

;;;------------------------------------------------------------------------- QFORMAT
;;; obsolete, should be replaced by dwformat

(defUn qformat (text &rest args)
  "Tracing steps of the querying mechanism. Used for debugging purposes.
   Prints into the consol with ACL or listener with MCL."
  (when *QUERY-TRACE-FLAG*
    (format *debug-io* "~&~%;~?"  text args)
    ;; wait so that user can read
    ;(read-char)
    ))

;;;----------------------------------------------------------------------------- ASK

(defUn ask (&rest args)
  "sends a string to the *output* stream and reads a line from the *input* stream."
  (apply #'format *output* args)
  ;; fake a message send
  (setq *answer* (read-line *input*)))

;;;---------------------------------------------------------------------------- TELL

(defUn tell (&rest args)
  "sends a string to the *output* stream."
  (apply #'format *output* args))

;;;================================================================================
;;;
;;;                              Service functions
;;;
;;;================================================================================

;;;----------------------------------------------------------------------------- <>

(defUn <> (n1 n2)
  "not-equal operator."
  (not (= n1 n2)))

#|
(<> 2 3)
T

(<> 2 2)
NIL
|#
;;;--------------------------------------------------------- APPLY-CARDINALITY-TEST

(defun apply-cardinality-test (test arg)
  "check whether the test applies to the argument.
Arguments:
   test: e.g. (> 2)
   arg: a number
Return:
   The result from the test"
  (case (car test)
    ((< <= = > >=)
     (funcall (car test) arg (cadr test)))
    (<>
     (not (= (cadr test) arg)))
    (:between
     (between arg (cdr test)))
    (:outside
     (not (between arg (cdr test))))))

#|
(apply-cardinality-test '(= 0) 0)
T

(apply-cardinality-test '(> 0) 0)
NIL

(apply-cardinality-test '(> 0) 1)
T

(apply-cardinality-test '(<> 2) 0)
T

(apply-cardinality-test '(<> 2) 2)
NIL

(apply-cardinality-test '(:between 2 3) 2)
T

(apply-cardinality-test '(:between 2 3) 4)
NIL

(apply-cardinality-test '(:outside 2 3) 1)
T

(apply-cardinality-test '(:outside 2 3) 3)
NIL
|#
;;;------------------------------------------------------------------------ BETWEEN

(defUn between (value range)
  "checks if a number is within a range (non strict).
Arguments:
   range: e.g. (2 5)
   value: e.g. 4
Return:
   nil or domething non nil."
  (and (>= value (car range))(<= value (cadr range))))

#|
(between 3 '(2 5))
T

(between 3 '(3 5))
T

(between 5 '(2 5))
T

(between 1 '(2 5))
NIL
|#
;;;----------------------------------------------------------- CARDINALITY-OPERATOR?

(defUn cardinality-operator? (opr)
  "test is opr is one of the legal cardinalit operators."
  (member opr *legal-cardinality-operators*))

#|
*legal-cardinality-operators*
(< <= = <> > >= :BETWEEN :OUTSIDE)

(cardinality-operator? '>)
(> >= :BETWEEN :OUTSIDE)

(cardinality-operator? :is-not)
NIL
|#
;;;---------------------------------------------------------- CONVERT-TO-NUMBER-DATA

(defUn convert-data-to-number (val &key no-error &aux res)
  "tries to convert data to number. Data can be:
   - a number (already converted)
   - a string represeting a number, e.g. \"2006\"
   - a list of numbers
   - a  list of strings representing numbers
Arguments:
   val: the data structure
   no-error (key): if non nil returns nil on error
Return:
   a number, a list of number NIL or error."
  (cond
   ((numberp val) val)
   ((and (stringp val) (setq res (read-from-string val)) (numberp res)) res)
   ((and (stringp val)(null no-error)) (error "trying to convert string ~S to number" val))
   ((and (listp val)(every #'numberp val)) val)
   ((and (listp val)(every #'stringp val)(setq res (mapcar #'read-from-string val))
         (every #'numberp res))
    res)
   (no-error nil)
   (t (error "data: ~S~& cannot be converted into a number or a list of numbers." val))
   ))

(defmacro cv2nb (val &key no-error)
  `(convert-data-to-number ,val :no-error ,no-error))

#|
(convert-data-to-number 234)
234
(cv2nb 234)


(convert-data-to-number "2345")
2345

(convert-data-to-number '(234 "2345"))
Error: data: (234 "2345")
cannot be converted into a number or a list of numbers.
(cv2nb '(234 "2345"))
id.

(convert-data-to-number "albert")
> Error: trying to convert string "albert" to number
> While executing: convert-data-to-number

(convert-data-to-number "albert" :no-error t)
NIL
(cv2nb "albert" :no-error t)
NIL

(convert-data-to-number '(1 2 3 4))
(1 2 3 4)

(convert-data-to-number '("2004" "2005"))
(2004 2005)

(convert-data-to-number '(:en "number" :fr "nombre"))
> Error: data: (:EN "number" :FR "nombre")
>         cannot be converted into a number or a list of numbers.
> While executing: convert-data-to-number

(convert-data-to-number '(1 2 a))
Error: data: (1 2 A)
cannot be converted into a number or a list of numbers.

(convert-data-to-number '(1 2 a) :no-error t)
NIL
|# 

;;;---------------------------------------------------------------------- KEYWORDIZE
;;; CCL intern requires strings!

#|
(defUn keywordize (xx)
  "interns an atom into the keyword package making it a keyword."
  (if (symbolp xx) 
      (intern xx :keyword) ; JPB0806
    xx))
|#

(defUn keywordize (xx)
  "interns an atom into the keyword package making it a keyword."
  (cond
   ((keywordp xx) xx)
   ((symbolp xx) (intern (symbol-name xx) :keyword))
   ((stringp xx)(intern (string-upcase xx) :keyword))
   (t xx)))
  
#|
(keywordize 'albert)
:ALBERT
:EXTERNAL

(keywordize :is)
:IS

(keywordize "test")
:TEST
:EXTERNAL
|#
;;;----------------------------------------------------------------------------- NEG

(defUn neg (clause)
  "negate a simple clause, whether sp or tp"
  (cond 
   ((moss::%is-attribute? (car clause))
    `(,(car clause) ,(negate-tp (cadr clause)) ,@(cddr clause)))
   ((moss::%is-relation? (car clause))
    `(,(car clause) ,(negate-sp (cadr clause)) ,@(neg-norm (cddr clause))))
   (t (error "clause ~S should start with a property" clause))))

#|
(moss::neg '($T-PERSON-NAME :IS "Labrousse"))
($T-PERSON-NAME :IS-NOT "Labrousse")

(moss::neg '($T-PERSON-AGE >= "22"))
($T-PERSON-AGE :LT "22")

(moss::neg '($T-PERSON-AGE :between "18" "22"))
($T-PERSON-AGE :OUTSIDE "18" "22")

(moss::neg '($T-PERSON :between "18" "22"))
Error: clause ($T-PERSON :BETWEEN "18" ...) should start with a property 
|#
;;;------------------------------------------------------------------------ NEG-NORM

(defUn neg-norm (query)
  "simplifies constrined OR clauses with negation (= 0) by transforming it into ~
   a conjunction of negated clauses. Works also on constrained mixed clauses ~
   although this is an illegal syntax."
  (cond
   ((null query) nil)
   ((not (listp query)) query)
   ((not (listp (car query))) (cons (car query)(neg-norm (cdr query))))
   ((and (eq (caar query) 'OR) (equal (cadar query) '(= 0)))
    (append (mapcar #'neg (cddar query)) (neg-norm (cdr query)))) 
   ((cons (neg-norm (car query)) (neg-norm (cdr query))))))

#|
(moss::neg-norm (moss::%translate-query '(PERSON (HAS-NAME IS "Labrousse")
              (OR (= 0) (HAS-BROTHER (= 2) (PERSON))
                  (HAS-HUSBAND (> 0) (PERSON))))
 ))
($E-PERSON ($T-PERSON-NAME :IS "Labrousse")
           ($S-PERSON-BROTHER (<> 2) ($E-PERSON)) 
           ($S-PERSON-HUSBAND (= 0) ($E-PERSON)))
|#
;;;----------------------------------------------------------------------- NEGATE-SP

(defUn negate-sp (constraint)
  "entering with sp constraints like (> 0) or (<= 5) that operate on the cardinal ~
   of an access result. We negate such constraints, taking into account that the ~
   argument cannot be negative, hence (> 0) -> (= 0) ; (= 0) -> (> 0)"
  (let ((opr (car constraint))
        (nn (cadr constraint)))
    (case opr
      (> (if (zerop nn) '(= 0) `(< ,(1+ nn))))
      (>= `(< ,nn))
      (= `(<> ,nn))
      ;(<> `(= nn)) ; why is it not in?
      (< `(> ,(1- nn)))
      (<= `(> ,nn)))))

#|
(moss::negate-sp '(>= 12))
(< 12)
|#
;;;----------------------------------------------------------------------- NEGATE-TP

(defUn negate-tp (opr)
  "takes the negative of an operator"
  (case opr
    ((:LESS-THAN :LT <) :GTE)
    ((:LESS-THAN-OR-EQUAL :LTE <=) :GT)
    ((:GREATER-THAN :GT >) :LTE)
    ((:GREATER-THAN-OR-EQUAL :GTE >=) :LT)
    ((:equal =) :NOT-EQUAL)
    ((:NOT-EQUAL <>) :EQUAL)
    (:IS :IS-NOT)
    (:IS-NOT :IS)
    (:BETWEEN :OUTSIDE)
    (:OUTSIDE :BETWEEN)
    (:IN :NOT-IN)
    (:NOT-IN :IN)
    (:ALL-IN :NOT-ALL-IN)
    (:NOT-ALL-IN :ALL-IN)
    (:CARD< :CARD>=)
    (:CARD<= :CARD>)
    (:CARD= :CARD<>)
    (:CARD<> :CARD=)
    (:CARD>= :CARD<)
    (:CARD> :CARD<=)
    (t (error "illegal operator: ~S, cannot negate it" opr))
    ))

;;;----------------------------------------------------------------- NORMALIZE-VALUE

(defUn normalize-value (value &optional (interchar #\.))
  "normalize value for comparisons during queries.
   If value is a list, returns the list of normalized values.
   If value is an MLN, then each of the language-strings will be normalized
   Otherwise applies make-value-string.
Arguments:
   value: a number, a symbol, a string, a list of strings, or an MLN
   interchar (opt): a character that will be inserted between two words
Return:
   the initial object with normalized strings"
  (cond 
   ((mln::mln? value) (mln::normalize-values (mln::make-mln value))) ; jpb 1406, JPB1606
   ;((mln::%mln? value) (mln::normalize-values (mln::make-mln value))) ; JPB1606
   ((listp value) (mapcar #'(lambda (xx)(make-value-string xx interchar)) value))
   (t (make-value-string value interchar))))

#|
(normalize-value 345)
"345"

(normalize-value 'albert)
"ALBERT"

(normalize-value :none)
":NONE"

(normalize-value '(Albert 223 "la belle   vie  ..."))
("ALBERT" "223" "LA.BELLE.VIE....")

(normalize-value '(Albert 223 "la belle   vie  ...") #\space)
("ALBERT" "223" "LA BELLE VIE ...")

(normalize-value '(Albert 223 "la belle   vie  ...") #\-)
("ALBERT" "223" "LA-BELLE-VIE-...")

(normalize-value "la belle   vie  s'écoule lentement ...")
"LA.BELLE.VIE.S'ÉCOULE.LENTEMENT...."

(normalize-value "le jour  le plus long   ")
"LE.JOUR.LE.PLUS.LONG"

(normalize-value "le jour  le plus long   " #\space)
"LE JOUR LE PLUS LONG"

(normalize-value '(:en "person" :fr "personne"))
((:EN "PERSON") (:FR "PERSONNE"))

(normalize-value '(:en " Joe is   happy!; Oh yes " :fr " Albert est  content "))
((:EN "JOE.IS.HAPPY!" "OH.YES") (:FR "ALBERT.EST.CONTENT"))
|#
;;;----------------------------------------------------------------------- NOT-EQUAL

(defUn not-equal (xx yy) (not (equal xx yy)))

;;;--------------------------------------------------------------- NUMBER-STRUCTURE?

(defUn number-structure? (expr)
  "Check if we have a Lisp structure containing only numbers"
  (cond
   ((null expr) t)
   ((numberp expr) t)
   ((not (listp expr)) nil)
   ((and (number-structure? (car expr))(number-structure? (cdr expr))))))

;;;#|--------------------------------------------------------------- NUMERIC-OPERATOR?
;;;;;; we must export <> otherwise it is considered as the atom moss::<>
;;;(defun numeric-operator? (opr)
;;;  "Checks the input argument against the following definition:
;;;      <numeric operator> ::= < | <= | = | >= | > | <> | BETWEEN | OUTSIDE"
;;;  (member opr '( < <= = >= > <> :not :BETWEEN :OUTSIDE)))
;;;|#


#|
;;;--------------------------------------------------------------- OR-SIMPLE-CLAUSE?
(defun or-simple-clause? (expr)
  "clause like (OR <simple-clause>*)"
  (and
   (listp expr)
   (eq 'OR (car expr))
   (simple-clause-list? (cdr expr))))

|#
;;;---------------------------------------------- OR-SIMPLE-CLAUSE-WITH-NO-VARIABLE?

(defUn or-simple-clause-with-no-variable? (expr)
  "checks if we have an OR-clause that contains only attribute clauses with no ~
   co-reference variable."
  (and
   (listp expr)
   (eq 'OR (car expr))
   (if (cdr expr)
     (dolist (clause (cdr expr) t)
       (if (or (not (%is-terminal-property? (car clause)))
               (variable? (caddr clause)))
         (return-from or-simple-clause-with-no-variable? nil))))
   ))

#|
(or-simple-clause-with-no-variable? '(OR ($T-person-age = 30)
                                         ($T-person-name :is "Albert")))
T
|#
;;;------------------------------------------------------------------ OUTSIDE-STRICT

(defUn outside-strict (range value)
  "checks if a value is outside a given range (strictly).
Arguments:
   range: i.e. (2 5)
   value: i.e. 4
Return:
   nil or something not nil."
  (or (< value (car range))(> value (cadr range))))

#|
(outside-strict '(2 5) 5)
NIL

(outside-strict '(2 5) 6)
T
|#
;;;------------------------------------------------------------- POSITIVE-CONDITION?

(defUn positive-condition? (expr)
  "A positive condition is a condition with a positive~
   operator, e.g., (= 2) or (> 0) or (BETWEEN 1 3) or "
  (and (listp expr) (positive-operator? (car expr))
       (not (equal expr '(= 0)))   ; this implements negation!!!
       (not (equal expr '(>= 0)))  ; since this contains the case (= 0)
       ))

#|
;; positive operators
(= >= > :BETWEEN :IS :EQUAL)

(positive-condition? '(= 2))
T

(positive-condition? '(< 2))
NIL
|#
;;;-------------------------------------------------------------- POSITIVE-OPERATOR?
(defUn positive-operator? (opr)
  "Checks if an operator is of positive type, i.e., >, >=, =, IS, BETWEEN, EQUAL."
  (member opr '(= >= > :BETWEEN :IS :EQUAL)))

;;;------------------------------------------------------------------- RELATIONSHIP?

(defUn relationship? (obj-id)
  "check for the following:
    <relationship>  ::= <structural-property> | <inverse-property>"
  (or (%is-structural-property? obj-id)(%is-inverse-property? obj-id)))

#|
;;;------------------------------------------------------------- SIMPLE-CLAUSE-LIST?

(defun simple-clause-list? (expr)
  "checks if the argument is a list of attribute clauses."
  (if expr
    (and (%is-terminal-property? (caar expr))
         (simple-clause-list? (cdr expr)))
    t))
|#

;;;------------------------------------------------- SIMPLE-CLAUSE-WITH-NO-VARIABLE?

(defUn simple-clause-with-no-variable? (expr)
  "checks if the argument is not an attribute clause containing a co-reference variable."
  (and
   (listp expr)
   (%is-terminal-property? (car expr))
   (not (variable? (caddr expr)))))

#|
(simple-clause-with-no-variable? '($T-PERSON-NAME :is ?X))
NIL

(simple-clause-with-no-variable? '($T-PERSON-NAME > 22))
T
|#
;;;---------------------------------------------------- SIMPLE-CLAUSE-WITH-VARIABLE?

(defUn simple-clause-with-variable? (expr)
  "checks if the argument is an attribute clause containing a co-reference variable."
  (and
   (listp expr)
   (%is-terminal-property? (car expr))
   (variable? (caddr expr))))

#|
(moss::simple-clause-with-variable? '($T-PERSON-NAME :is ?X))
T

(moss::simple-clause-with-variable? '($T-PERSON-NAME :is 12))
NIL
|#
;;;------------------------------------------------------------- TP-CLAUSE-OPERATOR?

(defUn tp-clause-operator? (opr)
  "Checks the input argument against the following definition:
      <tp-clause-operator> ::= < | <= | = | >= | > | <> | BETWEEN | OUTSIDE | IS ~
   | IS-NOT"
  (member opr '( < <= = >= > <> :BETWEEN :OUTSIDE :IS :IS-NOT)))

;;;---------------------------------------------------- TP-CLAUSE-CARDINAL-OPERATOR?

(defUn tp-clause-cardinal-operator? (opr)
  "Checks the input argument against the following definition:
      <tp-clause-cardinal-operator> ::= CARD< | CARD<= | CARD= 
                 | CARD>= | CARD>"
  (member opr *numeric-operators*))

#|
*numeric-operators*
(:LESS-THAN :LT :LESS-THAN-OR-EQUAL :LTE :GREATER-THAN :GT
 :GREATER-THAN-OR-EQUAL :GTE :EQUAL :NOT-EQUAL < <= > >= = <> :BETWEEN :OUTSIDE
 :CARD< :CARD<= :CARD= :CARD<> :CARD>= :CARD>)
|#
;;;----------------------------------------------------------------------- VARIABLE?

(defUn variable? (var)
  "A variable is a symbol starting with ?"
  (and var (symbolp var) (equal #\? (char (symbol-name var) 0))))

#|
(variable? 'xx)
NIL

(variable? '?xx)
T
|#


;;;================================================================================
;;;
;;;                    Step 1 : translate and normalize user query
;;;
;;;================================================================================

;;; first we must replace all virtual entities (classes or properties) by their
;;; expansion in terms of actual classes or properties

;;;?----------------------------------------------------------------- %EXPAND-QUERY

(defUn %expand-query (expr &key (package *package*))
  "takes a query and replaces all virtual classes and properties by their expansion.
Arguments:
   expr: formal query
   package (key): package of the query (default current)
Result: an expanded query"
  (cond
   ;; if query is symbol or string, check for virtual class and return
   ((or (symbolp expr)(stringp expr))
    (%expand-class expr :package package))
   ;; otherwise, take car
   ;; expand car (virtual class)
   ;; cdr is a set of clauses, expand each clause in turn
   (t (append (%expand-class (car expr):package package)
              (mapcar #'%expand-clause (cdr expr))))
   ))

#|
;;; currently does not work (missing VIRTUAL-ATTRIBUTE class
(%expand-query '("adult" ("residence" :is "Paris")) :package :test)
($E-PERSON ("age" >= 18) ("residence" :IS "Paris"))
|#
;;;?----------------------------------------------------------------- %EXPAND-CLASS

(defUn %expand-class (ref &key (package *package*))
  "takes a reference (symbol, string, or mln) and if a virtual class, replaces it~
   by its expansion.
Arguments:
   ref: class reference
   package: package (default current)
Return:
   expanded class or initial reference."
  ;; get class id, if not a class returns ref (no error check)
  (with-package package
    (let ((context (symbol-value (intern "*CONTEXT*")))
          (virtual-class-id (%%get-id ref :virtual-class))
          base-class-id)
      (unless virtual-class-id (return-from %expand-class ref))
      ;; if virtual class, then return  expansion
      (setq base-class-id (car (%%get-value virtual-class-id '$RFC context)))
      ;; base class is a class-id
      (car (%expand-virtual-definition 
            base-class-id
            (car (%%get-value virtual-class-id '$DEF context))
            )))))

#|
(%expand-class "person")
"person"

(%expand-class "adult" :package :cl-user)
($E-PERSON ("age" >= 18))

(catch :syntax-error 
       (%translate-query '($E-PERSON ("age" >= 18)) :package :test))
"Syntax error (\"TEST\"), wrong property?:
  (\"age\" >= 18)"
;; BUG in %translate-query ?
|#
;;;----------------------------------------------------------------- %EXPAND-CLAUSE

(defUn %expand-clause (clause &key (package *package*))
  "takes a clause and expands each clause in turn.
Arguments:
   clause: clause to expand (may be an OR or NOT clause)
   package: package (default current)
Return:
   expanded clause or initial clause."
  (cond
   ((null clause) nil)
   ;; if OR or NOT clause, recurse on its cdr
   ((or (member (car clause) '(or NOT OU PAS)))
    (cons (car clause) 
          (mapcar #'(lambda (xx) (%expand-clause xx :package package))
                  (cdr clause)))))
  (let ((context (symbol-value (intern "*CONTEXT*")))
        (prop-ref (car clause))
        prop-id definition)
    (cond
     ;; check car for reference to virtual attribute
     ((setq prop-id 
            (%extract prop-ref 'HAS-PROPERTY-NAME 'VIRTUAL-ATTRIBUTE :package package))
      ;; kludge 
      (if (listp prop-id)(setq prop-id (car prop-id)))
      ;; get definition of virtual relation
      (setq definition (car (%%get-value prop-id '$DEF context)))
      ;; replace attribute by its expansion
      ;; e.g. ("residence" :is "Paris") 
      ;;           -> ("living place" ("place" ("name" :is "Paris"))
      
      clause)
     ;; check for reference to virtual relation
     ((setq prop-id
            (%extract prop-ref 'HAS-PROPERTY-NAME 'VIRTUAL-RELATION :package package))
      ;; kludge 
      (if (listp prop-id)(setq prop-id (car prop-id)))
      ;; get definition of virtual relation
      (setq definition (car (%%get-value prop-id '$DEF context)))
      ;; check for cardinality constraint e.g. ("uncle" (> 1) ("teacher"))
      ;; replace
      ;;          -> (OR (> 1)
      ;;                 ("mother" ("person" ("brother" ("teacher"))))
      ;;                 ("father" ("person" ("brother" ("teacher")))))
      (if (member (caadr clause) '(< <= = >= >))
        (subst (cadr clause) :card 
               (subst (%expand-query (caddr clause) :package package) 
                      :rest definition))
        ;; if no card constraint, then insert (> 0)
        ;;          -> (OR ("mother" ("person" ("brother" ("teacher"))))
        ;;                 ("father" ("person" ("brother" ("teacher")))))
        (subst (copy-list '(> 0)) :card 
               (subst (%expand-query (cadr clause) :package package)
                      :rest definition)))
      )
     ;; ... must process regular relations
     
     ;; probably regular attribute
     (t clause)))
  )

#|
Definition must be something like
    ("living place" ("place" ("name" . :rest))
or
(or :card
    ("mother" ("person" ("brother" :rest)))
    ("father" ("person" ("brother" :rest))))
|#
;;;----------------------------------------------------- %EXPAND-VIRTUAL-DEFINITION

(defUn %expand-virtual-definition (class-id expr &key (package *package*))
  "takes the definition from a virtual class and transforms it into a piece of ~
   query. E.g. (?* \"age\" >= 18)  -> (\"person\" (\"age\" >= 18))
Argument: 
   class-ref: ref of base class
   expr: definition for the virtual class
Return:
   a piece of query."
  ;; replaces with id of class that will be left unchanged in query translation
  (subst class-id (intern "?*" package) expr))




;;; We cannot translate without at the same time checking the syntax since the same
;;; string can point to a concept or a property. Thus, a quick syntax check is done
;;; at translation time

;;;--------------------------------------------------------------- %TRANSLATE-QUERY

(defUn %translate-query (expr &key (package *package*) &aux $$$)
  "translates a query into internal format (replacing refs with ids).
Arguments:
   expr: can be 
         - symbol or string denoting an entry point
         - a list starting with a symbol or string denoting a class
   package (key): package (default is current)
Returns:
   a query expressed in internal format."
  (cond
   ((null expr) nil)
   ;; for entry points?
   ((or (symbolp expr)(stringp expr)) expr)
   ;; check if car names a class, if so replace the name by the id and parse the
   ;; rest of the query 
   ((or (symbolp (car expr))(stringp (car expr))(mln::mln? (car expr)))
    (setq $$$ (with-package package (%%get-id (car expr) :class)))
    ;; if the result is not a class complain
    (unless $$$
      (throw :syntax-error 
             (format nil "Bad query structure?: expr should be a query ~
                          in ~S:~&(car expr) should refer to the name of a class ~
                          in:~&  ~S." package expr)))
      
    (cons $$$ (mapcar #'(lambda (xx) (%translate-clause xx (car expr) package))
                      (cdr expr))))
   ;; otherwise bad syntax
   (t (throw :syntax-error 
             (format nil "Expr is not an entry point nor a query ~
                          in ~S:~& (car expr) should refer to the name of a class ~
                          ~&  ~S." package expr)))
   ))

#|
(with-package :family
  (catch :syntax-error 
         (moss::%translate-query 
          '("Person" ("first name" is "Albert")))))
(F::$E-PERSON (F::$T-PERSON-FIRST-NAME :IS "Albert"))

(with-package :family
 (catch :syntax-error 
    (moss::%translate-query 
     '(Person (has-first-name :is "Albert")))))
(F::$E-PERSON (F::$T-PERSON-FIRST-NAME :IS "Albert"))

(catch :syntax-error 
    (moss::%translate-query
     '("person"("husband" (> 3) ("Person" ("name" is "Barthès"))))
     :package *package*))
($E-PERSON ($S-PERSON-HUSBAND (> 3) ($E-PERSON ($T-PERSON-NAME :IS "Barthès"))))

(with-package :family
  (catch :syntax-error 
         (moss::%translate-query
          '("person"("husband" ((> 3) ("Person" ("name" is "Barthès")))))
          :package *package*)))
"Expr is not an entry point nor a query in #<The FAMILY package>:
 (car expr) should refer to the name of a class 
  ((> 3) (\"Person\" (\"name\" MOSS::IS \"Barthès\")))."
  |#
;;;-------------------------------------------------------------- %TRANSLATE-CLAUSE

(defUn %translate-clause (expr class-ref package &aux $$$)
  "translates a clause: list starting with
   - OR, OU, NOT, PAS
   - an attribute, relation or inverse relation
Arguments:
   expr: expression to translate
   class-ref: reference of class to which property belongs
   package: package
Return:
   list of translated clause or throws to :syntax-error"
  #|
(declare (inline %translate-clause-get-attribute
                   %translate-clause-get-relation
                   ))
|#
  (cond
   ((not (listp expr))
    (throw :syntax-error 
      (format nil "Syntax error (~S), expr should be a query or an entry point: ~
                   ~&  ~S" package expr)))
   ;; or clause, disjunction of simple clauses (what about (> 3) ?)
   ((member (car expr) '(or OU))
    (if (not (every #'listp (cdr expr)))
      (throw :syntax-error (format nil "Bad clause in ~S:~&  ~S" package expr)))
    (cons 'OR 
          (mapcar #'(lambda (xx)
                      (%translate-clause xx class-ref package))
                  (cdr expr))))
   
   ;; check for a negative clause
   ((member (car expr) '(not  NON  PAS))
    (if (not (every #'listp (cdr expr)))
      (throw :syntax-error (format nil "Bad clause in ~S:~&  ~S" package expr)))
    (cons 'NOT 
          (mapcar #'(lambda (xx)
                      (%translate-clause xx class-ref package))
                  (cdr expr))))
   
   ;; attribute?
   ((setq $$$ (%translate-clause-get-attribute (car expr) class-ref package expr))
    (list* $$$ (keywordize (cadr expr)) (cddr expr)))
   
   ;; relation?
   ((setq $$$ (%translate-clause-get-RELATION (car expr) class-ref package expr))
    (cons $$$ (%translate-rel-clause (cdr expr) class-ref package)))
   
   ;; inverse-link?
   ;; Watch it! In case of inverse property the class ref is on the following expr
   ;; e.g. (">address" ("organization" ...))
   ;; therefore, we must replace class-ref by (caadr expr)
   ;; test if the second list element is a cardinality constraint
   ((and (listp (cadr expr))(cardinality-operator? (caadr expr))
         ;(setq $$$ (%translate-clause-get-inv-prop (car expr) (caaddr expr) package))
         (setq $$$ (%%get-id (car expr) :inv-from :class-ref class-ref
                             :from (caaddr expr)))
         )
    (list* $$$ (cadr expr) (%translate-rel-clause (cddr expr) (caaddr expr) package)))
   
   ;; otherwise clause without cardinality condition
   ;; e.g. (">thèse" ("Doctorant")) -> ($S-IS-PHD-STUDENT-THESIS.OF ($E-PHD-STUDENT))
   ((and (listp (cadr expr))
         (not (cardinality-operator? (caadr expr)))
         ;(setq $$$ (%translate-clause-get-inv-prop (car expr) (caadr expr) package))
         (setq $$$ (%%get-id (car expr) :inv-from :class-ref class-ref 
                             :from (caadr expr)))
         )
    (cons $$$ (%translate-rel-clause (cdr expr) (caadr expr) package)))
   
   ;; cardinality constraint
   ((member (car expr) '(> >= = > >=))
    ;; leave it as it is
    expr)
   
   ;; otherwise error
   (t (throw :syntax-error (format nil "Syntax error (~S), wrong property?:~&  ~S" 
                                   (package-name package) expr)))))

#| Exemples à revoir
(moss::%translate-query '("university" ("president" (> 0) ("person"))))
($E-UNIVERSITY ($S-TEACHING-ORGANIZATION-PRESIDENT (> 0) ($E-PERSON)))

(moss::%translate-query '("person" (">president" (> 0) ("university"))))
($E-PERSON ($S-TEACHING-ORGANIZATION-PRESIDENT.OF (> 0) ($E-UNIVERSITY)))

(moss::%translate-clause '("first name" is "Albert") "Person" *package*)
($T-PERSON-FIRST-NAME :IS "Albert")


(catch :syntax-error
       (moss::%translate-clause '("first name" isS "Albert") "Person" *package*))
"Bad operator ISS in #<The MOSS package>:
  (\"first name\" ISS \"Albert\")"

(catch :syntax-error 
    (moss::%translate-clause '("firstt name" is "Albert") "Person" *package*))
"Syntax error (\"MOSS\"), wrong property?:
  (\"firstt name\" IS \"Albert\")"

(catch :syntax-error 
       (moss::%translate-clause '(or ("first name" is "Albert")
                                     ("first name" is-not "Joe")) 
                             "Person" *package*))
(OR ($T-PERSON-FIRST-NAME :IS "Albert") ($T-PERSON-FIRST-NAME :IS-NOT "Joe"))

(catch :syntax-error 
    (moss::%translate-clause 
     '(or (> 2) ("first name" is "Albert")
          ("first name" is-not "Joe"))
     "Person" *package*))
(OR (> 2) ($T-PERSON-FIRST-NAME :IS "Albert")
    ($T-PERSON-FIRST-NAME :IS-NOT "Joe"))

(catch :syntax-error 
    (moss::%translate-clause 
     '(or (> 2) ("first name" is "Albert")
          ("first name" is-not "Joe") :zz)
     "Person" *package*))
"Bad clause in #<The MOSS package>:
  (OR (> 2) (\"first name\" IS \"Albert\") (\"first name\" IS-NOT \"Joe\") :ZZ)"


(moss::parse-user-query '("person" 
                          (">president" ("organization" ("acronym" :is "PUC")))))
($E-PERSON
 ($S-ORGANIZATION-PRESIDENT.OF (> 0)
  ($E-ORGANIZATION ($T-ORGANIZATION-ACRONYM :IS "PUC"))))
|#
;;;------------------------------------------------ %TRANSLATE-CLAUSE-GET-ATTRIBUTE

(defUn %translate-clause-get-attribute (ref class-ref package expr)
  "used to get the precise attribute of a class or signal failure.
Arguments:
   ref: attribute ref, e.g. \">president\" or IS-PRESIDENT-OF
   class-ref: class bearing the direct property, e.g. UNIVERSITY
   package: references package
   expr: hicher level expression used for returning meaningful error messages
Return:
   a single symbol, e.g. $S-UNIVERSITY-PRESIDENT.OF or nil"
  (with-package package
    (let (att-id)
      (setq att-id 
            (catch :error (%%get-id ref :attribute :class-ref class-ref)))
      (when att-id
        (if (not (member (keywordize (cadr expr)) *legal-attribute-operators*))
          (throw :syntax-error (format nil "Bad operator ~S in ~S:~&  ~S"
                                       (cadr expr) package expr)))
        att-id))))

;;;------------------------------------------------- %TRANSLATE-CLAUSE-GET-INV-PROP
;;; unused, was replaced by %%get-id

;;;(defUn %translate-clause-get-inv-prop (ref class-ref package)
;;;  "used to get the precise inverse property-id pointing back to class-ref.
;;;Arguments:
;;;   ref: inverse prop ref, e.g. \">president\" or IS-PRESIDENT-OF
;;;   class-ref: class bearing the direct property, e.g. UNIVERSITY
;;;Return:
;;;   a single symbol, e.g. $S-UNIVERSITY-PRESIDENT.OF or throws to :error"
;;;  (with-package package
;;;    (let ((context (symbol-value (intern "*CONTEXT*")))
;;;          name prop-list class-id id)
;;;      ;; cook up inverse property name 
;;;      ;; BUG: ref should be a direct prop name
;;;      (setq name (%%make-name ref :inv))
;;;      ;(print name)
;;;      (unless (%pdm? name)
;;;        (throw :syntax-error
;;;               (format nil "~S does not refer to an inverse property in package ~S ~
;;;                            context (~S)."
;;;                       ref *package* context)))
;;;      
;;;      ;; now class-ref
;;;      (setq class-id (%%get-id class-ref :class :include-moss t))
;;;      (unless class-id
;;;        (throw :syntax-error
;;;               (format nil "~S does not refer to a valid class in package ~S context ~
;;;                            (~S) while trying to determine inverse property ~S"
;;;                       class-ref *package* context ref)))
;;;      
;;;      ;; get all possible inverse properties with that name
;;;      (setq prop-list (%get-value name (%inverse-property-id '$INAM)))
;;;      (unless prop-list (return-from %translate-clause-get-inv-prop nil))
;;;      ;; get direct properties
;;;      (setq prop-list (mapcar #'%inverse-property-id prop-list))
;;;      ;; return 
;;;      (setq id (car (%determine-property-id-for-class prop-list class-id)))
;;;      (when id (%inverse-property-id id)))))

;;;#| in package moss
;;;(%translate-clause-get-inv-prop ">president" "organization" *package*)
;;;$S-TEACHING-ORGANIZATION-PRESIDENT.OF
;;;? (moss::%translate-clause-get-inv-prop ">president" "university" :address)
;;;$S-TEACHING-ORGANIZATION-PRESIDENT.OF
;;;? (%translate-clause-get-inv-prop ">brother" "person" :family)
;;;|#
;;;------------------------------------------------- %TRANSLATE-CLAUSE-GET-RELATION

(defUn %translate-clause-get-relation (ref class-ref package expr)
  "used to get the precise relation of a class or signal failure.
Arguments:
   ref: attribute ref, e.g. \">president\" or IS-PRESIDENT-OF
   class-ref: class bearing the direct property, e.g. UNIVERSITY
   package: references package
   expr: hicher level expression used for returning meaningful error messages
Return:
   a single symbol, e.g. $S-UNIVERSITY-PRESIDENT.OF or nil"
  (declare (ignore expr))
  (with-package package
    (catch :error (%%get-id ref :relation :class-ref class-ref))))

;;;---------------------------------------------------------- %TRANSLATE-REL-CLAUSE

(defUn %translate-rel-clause (expr class package)
  "translates a clause like ({(<opr> nn)} <query>))
Arguments:
   expr: list starting eventually by a cardinality constraint
   package: package
Return:
   the translate list or throws to :syntax-error"
  (cond
   ((or (null expr) 
        (not (listp (car expr))))
    (throw :syntax-error (format nil "Bad relation at ~S level" class)))
   ;; cardinality constraint
   ((member (caar expr) '(< <= = > >=))
    (list (car expr) (%translate-query (cadr expr) :package package)))
   ;; error if more than one clause
   ((cdr expr)
    (throw :syntax-error 
           (format nil "At ~S level in ~S, bad format following a relation. ~
                        ~&  ~S~&Probably bad operator in cardinality clause."
                   class package expr)))
   (t (list (%translate-query (car expr) :package package)))
   ))


#|
;; tests done in the :family package

;; direct query
(catch :syntax-error 
    (moss::%translate-rel-clause 
     '(("Person" ("first name" is "Albert"))) "Person" *package*))
(($E-PERSON ($T-PERSON-FIRST-NAME :IS "Albert")))

;; subquery with cardinality constraint
(catch :syntax-error 
    (moss::%translate-rel-clause 
     '((< 3)("Person" ("first name" is "Albert"))) "Person" *package*))
((< 3) ($E-PERSON ($T-PERSON-FIRST-NAME :IS "Albert")))

;; bad format in cardinality constraint
(catch :syntax-error 
    (moss::%translate-rel-clause 
     '((:is 3)("Person" ("first name" is "Albert"))) "Person" *package*))
"At \"Person\" level in #<The MOSS
                       package>, bad format following a relation. 
  ((:IS 3) (\"Person\" (\"first name\" IS \"Albert\")))
Probably bad operator in cardinality clause."

;; bad structure (too many parents)
(catch :syntax-error 
    (moss::%translate-rel-clause 
     '(((< 3)("Person" ("first name" is "Albert")))) "Person" *package*))
"Expr is not an entry point nor a query in #<The MOSS package>:
 (car expr) should refer to the name of a class 
  ((< 3) (\"Person\" (\"first name\" IS \"Albert\")))."

(catch :syntax-error
       (moss::%translate-rel-clause 
        '((has-first-name is "antoine")) person *package*))
"Bad query structure?: expr should be a query in #<The MOSS package>:
(car expr) should refer to the name of a class in:
  (HAS-FIRST-NAME IS \"antoine\")."
|#
;;;================================================================================
;;;
;;;                            Step 2 : parse query
;;;   
;;;================================================================================
#|
(trace parse-query check-query check-clause-list check-clause check-tp-clause
       check-sub-query check-or-clause check-simple-clause check-simple-clause-list
       check-or-constrained-sub-query check-sub-query-list)
(untrace parse-query check-query check-clause-list check-clause check-tp-clause
         check-sub-query check-or-clause check-simple-clause check-simple-clause-list
         check-or-constrained-sub-query check-sub-query-list)
|#

;;;----------------------------------------------------------------- PARSE-USER-QUERY

(defUn parse-user-query (query &key (package *package*))
  "Takes a query in user format; translates it into internal format and checks its ~
   syntax.
   Query syntax is the following:
      <general-query>            ::= <entry-point> | <query>
      <query>                    ::= (<class> {<clause>}*) 
                                           | (<class> <class-variable> <clause>+)
      <clause>                   ::= (<simple-clause>)
                                           | (<or-clause>) | (<or-constrained-sub-query>)
      <simple-clause>            ::= (<tp-clause>) | (<sub-query>)
      <tp-clause>                ::= (<attribute> <tp-operator> <value>) 
                                           | (<attribute>> <tp-operator> <variable>)
      <sub-query>                ::= (<relation> {<cardinality-condition>} <query>)
      <relationship>             ::= <relation> | <inverse-property>
      <or-clause>                ::= (OR <simple-clause>+)
      <or-constrained-sub-query> ::= (OR <cardinality-condition> {<sub-query>}+)
      <cardinality-condition>    ::= (<numeric-operator> <arg>*)
      <numeric operator>         ::= < | <= | = | >= | > | <> | BETWEEN | OUTSIDE
      <tp-operator>              ::= < | <= | = | >= | > | <> | BETWEEN | OUTSIDE  
                                          | IS | IS-NOT | IN | ALL-IN
                                          | CARD= | CARD< | CARD<= | CARD> | CARD>=
      <variable>                 ::= any Lisp symbol starting with ? (e.g. ?albert)
      <class-variable>           ::= <variable>
Arguments:
   query: with the above syntax
   package (opt): default current
Returns:
   nil if an error, query in internal format otherwise."
  (let ((variable-list (collect-variables query))
        expr)
    ;; if query is a string then output it directly (entry point)
    (if (stringp query)
      (return-from parse-user-query query))
    ;; first translate query into internal format, reshuffling negations
    ;(setq expr (neg-norm ($$translate-1 query nil package)))
    ;; when $$translate-1 cannot translate something into internal format, it leaves
    ;; is as it is, e.g. (person (youppee)) -> ($E-PERSON (YOUPPEE))
    ;; check now syntax
    (setq expr (catch :syntax-error
                 (neg-norm (%translate-query query :package package))))
    ;; if error (string) then return nil
    (when (stringp expr)
      (warn expr)
      (return-from parse-user-query nil))
    ;; otherwise check some more
    (prog1
      (cond
       ;; check for entry-point (must be a symbol or a string), includes nil
       ((or (symbolp expr) (stringp expr)) expr)
       ;; otherwise check for query
       ((catch :parse (check-query expr))))
      ;; remove marks set on co-reference variables
      ;(print (mapcar #'(lambda (xx) (cons xx (symbol-plist xx))) variable-list))
      (mapcar #'(lambda (xx) (zap-old xx)) variable-list)
      )))

#|
;;===== MOSS package

(moss::parse-user-query "Barthès Biesel")
"Barthès Biesel"

(moss::parse-user-query 'Barthès)
BARTHÈS

(moss::parse-user-query '(person))
($E-PERSON)

(moss::parse-user-query '(person (has-name is "barthès")))
($E-PERSON ($T-PERSON-NAME :IS "barthès"))

(moss::parse-user-query '(person (name is "barthès")))
($E-PERSON ($T-PERSON-NAME :IS "barthès"))

(moss::parse-user-query '(person ("name" is "barthès")))
($E-PERSON ($T-PERSON-NAME :IS "barthès"))

(moss::parse-user-query 
   '(person (has-name is "barthès")
     (has-brother (person (has-first-name is "antoine")))))
($E-PERSON ($T-PERSON-NAME :IS "barthès")
           ($S-PERSON-BROTHER (> 0) ($E-PERSON ($T-PERSON-FIRST-NAME :IS "antoine"))))

(moss::parse-user-query 
   '(person (has-name is "barthès")
     (has-brother (has-first-name is "antoine"))))
Warning: Bad query structure?: expr should be a query in #<The MOSS package>:
(car expr) should refer to the name of a class in:
  (HAS-FIRST-NAME IS "antoine").
NIL

(moss::parse-user-query 
   '(person (has-first-name is ?x)
     (has-husband (person (has-first-name is ?x)))))
($E-PERSON ($T-PERSON-FIRST-NAME :IS ?X)
 ($S-PERSON-HUSBAND (> 0) ($E-PERSON ($T-PERSON-FIRST-NAME :IS ?X))))

(moss::parse-user-query 
   '(person (is-husband-of (person (has-first-name is "antoine")))))
($E-PERSON
 ($S-PERSON-HUSBAND.OF (> 0) ($E-PERSON ($T-PERSON-FIRST-NAME :IS "antoine"))))

(moss::parse-user-query 
   '(person (">husband" (person (has-first-name is "antoine")))))
($E-PERSON
 ($S-PERSON-HUSBAND.OF (> 0) ($E-PERSON ($T-PERSON-FIRST-NAME :IS "antoine"))))

(moss::parse-user-query '(person) :package :test)
(TEST::$E-PERSON)

;;===== TEST package

(with-package :test
  (moss::parse-user-query "Barthès Biesel"))
"Barthès Biesel"

(with-package :test
  (moss::parse-user-query 'test::Barthès))
TEST::BARTHÈS

(with-package :test
  (moss::parse-user-query '(test::person)))
(TEST::$E-PERSON)

(with-package :test
  (moss::parse-user-query '(test::person (test::has-name test::is "barthès"))))
(TEST::$E-PERSON (TEST::$T-PERSON-NAME :IS "barthès"))

(with-package :test
  (moss::parse-user-query '(test::person (test::name test::is "barthès"))))
(TEST::$E-PERSON (TEST::$T-PERSON-NAME :IS "barthès"))

(with-package :test
  (moss::parse-user-query '(test::person ("name" test::is "barthès"))))
(TEST::$E-PERSON (TEST::$T-PERSON-NAME :IS "barthès"))

(with-package :test
  (moss::parse-user-query 
   '(test::person (test::has-name test::is "barthès")
                  (test::has-brother 
                   (test::person (test::has-first-name test::is "antoine"))))))
(TEST::$E-PERSON (TEST::$T-PERSON-NAME :IS "barthès")
 (TEST::$S-PERSON-BROTHER (> 0)
  (TEST::$E-PERSON (TEST::$T-PERSON-FIRST-NAME :IS "antoine"))))

(with-package :test
  (moss::parse-user-query 
   '(test::person (test::has-name test::is "barthès")
            (test::has-brother (test::has-first-name test::is "antoine")))))
Warning: Bad query structure?: expr should be a query in #<The MOSS package>:
(car expr) should refer to the name of a class in:
  (HAS-FIRST-NAME IS "antoine").
NIL

(with-package :test
  (moss::parse-user-query 
   '(test::person (test::has-first-name test::is ?x)
                  (test::has-husband 
                   (test::person (test::has-first-name test::is ?x))))))
(TEST::$E-PERSON (TEST::$T-PERSON-FIRST-NAME :IS ?X)
 (TEST::$S-PERSON-HUSBAND (> 0)
  (TEST::$E-PERSON (TEST::$T-PERSON-FIRST-NAME :IS ?X))))

(with-package :test
  (moss::parse-user-query 
   '(test::person (test::is-husband-of 
                   (test::person (test::has-first-name test::is "antoine"))))))
(TEST::$E-PERSON
 (TEST::$S-PERSON-HUSBAND.OF (> 0)
  (TEST::$E-PERSON (TEST::$T-PERSON-FIRST-NAME :IS "antoine"))))

(with-package :test
 (moss::parse-user-query 
  '(test::person (">husband" 
                  (test::person (test::has-first-name test::is "antoine"))))))
(TEST::$E-PERSON
 (TEST::$S-PERSON-HUSBAND.OF (> 0)
  (TEST::$E-PERSON (TEST::$T-PERSON-FIRST-NAME :IS "antoine"))))

(with-package :test
  (moss::parse-user-query '(test::person) :package :test))
(TEST::$E-PERSON)
|#
;;;---------------------------------------------------------- CARDINALITY-CONDITION?

(defUn cardinality-condition? (expr)
  "Checks the following:
    <cardinality-condition> ::= (<numeric-operator> <arg>*)"
  (and expr
       (listp expr)
       (member (car expr) *legal-cardinality-operators*)
       ;; if more than 1 arg should be numbers
       (every #'numberp (cdr expr))))

#|
(moss::cardinality-condition? '(> 0))
T

(moss::cardinality-condition? '(<> 0))
T

(moss::cardinality-condition? '(:between 0)) ; problem no check on # args
T
|#
;;;--------------------------------------------------------------------- CHECK-QUERY

(defUn check-query (expr)
  "Checks for a formal query:
    <query> ::= (<class> {<clause>}*) | (<class> <class-variable> <clause>+)"
  (cond 
   ((or (null expr)(not (listp expr))) nil)
   ;; first element must be a class
   ((not (%is-model? (car expr))) 
    (format t "~&Error in query format, first symbol is not a class: ~S" expr)
    (throw :parse nil))
   ;; the class can be alone
   ((null (cdr expr)) expr)
   ;; second element might be a class variable
   ((and (variable? (cadr expr))
         (cddr expr))
    (cons (car expr) (check-clause-list+ (cddr expr))))
   ;; otherwise: case where there is no class variable
   ((cdr expr) (cons (car expr) (check-clause-list+ (cdr expr))))
   ;; otherwise error
   (t (format t "~&Error query or subquery format: ~S" expr)
      (throw :parse nil))))

;;;-------------------------------------------------------------- CHECK-CLAUSE-LIST+

(defUn check-clause-list+ (expr)
  "Checking list of clauses. When called should have a clause"
  (unless expr
    (format t "~&Error in query format, missing clause.")
    (throw :parse nil))
  (unless (listp expr)
    (format t "~&Error in query format, clause should be a list: ~&~S" expr)
    (throw :parse nil))
  (cons (check-clause (car expr))
        (check-clause-list (cdr expr))))

;;;--------------------------------------------------------------- CHECK-CLAUSE-LIST

(defUn check-clause-list (expr)
  "Checking list of clauses."
  (if expr (cons (check-clause (car expr))
                 (check-clause-list (cdr expr)))))

;;;-------------------------------------------------------------------- CHECK-CLAUSE

(defUn check-clause (expr)
  "Checking a single clause:
    <clause> ::= (<simple-clause>)  | (<or-clause>) | (<or-constrained-sub-query>)"
  (or (check-simple-clause expr)
      (check-or-clause expr)
      (check-not-clause expr)
      (check-or-constrained-sub-query expr)
      (progn
        (format t "Error in clause format (bad syntax): ~S" expr)
        (throw :parse nil))))

;;;----------------------------------------------------------------- CHECK-OR-CLAUSE

(defUn check-or-clause (expr)
  "Checking an OR clause, i.e. a clause starting with the atom OR."
  (cond
   ((not (listp expr))
    (format t "Error in clause format, should be a list: ~S" expr)
    (throw :parse nil))
   ((not (eq 'OR (car expr))) nil)
   ;; if second element is a cardinality condition, this is not our business
   ;; checked by check-or-constrained sub-query 
   ((cardinality-condition? (cadr expr)) nil)
   ;; otherwise, rest must be simple clauses (tp or sp)
   ((cons 'OR (check-simple-clause-list+ (cdr expr))))))

;;;---------------------------------------------------------------- CHECK-NOT-CLAUSE

(defUn check-not-clause (expr)
  "Checking a NOT clause, i.e. a clause starting with the atom NOT."
  (cond
   ((not (listp expr))
    (format t "Error in clause format, should be a list: ~S" expr)
    (throw :parse nil))
   ((not (eq 'NOT (car expr))) nil)
   ;; if second element is a cardinality condition, this is not our business
   ;; checked by check-or-constrained sub-query 
   ((cardinality-condition? (cadr expr)) nil)
   ;; otherwise, rest must be simple clauses (tp or sp)
   ((cons 'NOT (check-simple-clause-list+ (cdr expr))))))

;;;------------------------------------------------------- CHECK-SIMPLE-CLAUSE-LIST+

(defUn check-simple-clause-list+ (expr)
  "checking for a list of simple clauses. When called should have a simple clause."
  (unless expr
    (format t "~&Error in query format, missing simple clause.")
    (throw :parse nil))
  (unless (listp expr)
    (format t "~&Error in query format, we should have a list of simple~
               clauses: ~&~S" expr)
    (throw :parse nil))
  (cons (check-simple-clause (car expr))
        (check-simple-clause-list (cdr expr))))

;;;------------------------------------------------------------- CHECK-SIMPLE-CLAUSE

(defUn check-simple-clause (expr)
  "check for a simple tp or sp clause, i.e. not containing OR
Arguments:
   expr: query fragment to test
Return:
   expr when the clause is a tp-clause or a subquery
   nil if not a tp-clause or a subquery
   a throw to :parse when expr is nul or not a list or has a bad clause format."
  (cond 
   ((or (null expr)(not (listp expr)))
    (format t "~&Error in simple clause format, simple clause should be a ~
               non empty list: ~S" expr)
    (throw :parse nil))
   ((check-tp-clause expr))
   ((check-sub-query expr))))

;;;-------------------------------------------------------- CHECK-SIMPLE-CLAUSE-LIST

(defUn check-simple-clause-list (expr)
  "checking for a list of simple clauses"
  (if expr (cons (check-simple-clause (car expr))
                 (check-simple-clause-list (cdr expr)))))

;;;----------------------------------------------------------------- CHECK-TP-CLAUSE

(defUn check-tp-clause (expr &aux $$$)
  "Checking simple attribute clause:
    <tp-clause> ::= (<terminal-property> <operator> <value>) 
                               | (<terminal-property> <operator> <variable>)
Arguments:
   expr: fragment of query to check
Returns:
    expr if the clause is a tp-clause 
    nil when the clause is not a tp-clause
   throw to :parse when expr is nil or not a list or has a bad clause format"
  (cond
   ;; expr must be a non empty list
   ((or (null expr)(not (listp expr)))
    (format t "~&Error in tp-clause format, should be a non empty list: ~S" expr)
    (throw :parse nil))
   ;; first element must be an attribute
   ((not (%is-terminal-property? (car expr))) nil)
   
   ;; Here it must be a tp-clause with or without variable
   ;; second element must be a numeric operator
   ;; *** could be user-defined operator (2-arg predicate operating on the
   ;; pattern value and on the list of values attached to the actual TP)
   ((and (tp-clause-cardinal-operator? (cadr expr))
         (not (setq $$$ (convert-data-to-number (cddr expr)))))
    (format t "~&Error in tp-clause format, cardinal operator: ~S requires number ~
               argument in ~S" (cadr expr) expr)
    (throw :parse nil))
   ;; otherwise accept it
   ((tp-clause-cardinal-operator? (cadr expr))
    (list* (car expr) (cadr expr) $$$))
   
   ((not (member (cadr expr) *legal-attribute-operators*))
    (format t "~&Error in tp-clause format, illegal operator: ~S in ~S" 
            (cadr expr) expr)
    (throw :parse nil))
   
   ;; if the third element is a variable, encountered for the first time, then
   ;; the operator must be =
   ((and (variable? (caddr expr)) (get (caddr expr) :old)) expr)
   ;; encountering variable for the first time?
   ((and (variable? (caddr expr)) (not (member (cadr expr)'(= :IS))))
    (format t "~&Error in tp-clause format: ~S~&first occurence of a variable ~
               must be with an equality operator (= or IS): " expr)
    (throw :parse nil))
   ((variable? (caddr expr))
    ;; mark variable as old
    (setf (get (caddr expr) :old) t)
    expr)
   
   ;; otherwise we must have at least a third term (ref argument to the predicate)
   ;; with some operators like BETWEEN we have more terms
   ;; ***** maybe we should check for the number of args and their type...
   ((cddr expr) expr)
   ;; otherwise error
   ((progn
      (format t "~&Error in tp-clause format: ~S" expr)
      (throw :parse nil)))))

#|
;;; Tests on the family data
? (moss::check-query '($E-PERSON ($T-PERSON-NAME :IS "barthes")))
($E-PERSON ($T-PERSON-NAME :IS "barthes"))
|#
;;;------------------------------------------------------------------ CHECK-SUBQUERY

(defUn check-sub-query (expr)
  "Checking sub-query:
    <sub-query>::= (<relationship> {<cardinality-condition>} <query>)
   A nil return indicates that this is not a sub-query. Error throws to :parse."
  (cond
   ((not (listp expr)) nil)
   ;; fist element must be relationship (structural property or inverse-property)
   ((not (relationship? (car expr)))  nil)
   ;; second element can be constraint
   ((and (cddr expr)
         (cardinality-condition? (cadr expr)))
    (list (car expr) (cadr expr)(check-query (caddr expr))))
   ;; if only two elements, then we include defaul cardinality condition
   ((and (cdr expr)(null (cddr expr)))
    (list (car expr) (copy-list '(> 0)) (check-query (cadr expr))))
   ;; otherwise error
   ((progn
      (warn "Error in sub-query format: ~S" expr)
      (throw :parse nil)))))

;;;-------------------------------------------------- CHECK-OR-CONSTRAINED-SUB-QUERY

(defUn check-or-constrained-sub-query (expr)
  "Check for an OR-clause with an associated constraint:
    <or-constrained-sub-query> ::= (OR <cardinality-condition> {<sub-query>}+)"
  (cond
   ;; test fails if expr does not start with OR JPB060813
   ((not (eq 'OR (car expr))) nil)
   ((or (not (eq 'OR (car expr)))
        (not (cardinality-condition? (cadr expr)))
        (not (cddr expr)))
    (format t "~&Error in or-constrained-sub-query format: ~S" expr)
    (throw :parse nil))
   ;; build or clause
   ((list* (car expr) (cadr expr) (check-sub-query (caddr expr))
           (check-sub-query-list (cdddr expr))))))

;;;----------------------------------------------------------- CHECK-SUB-QUERY-LIST+

(defUn check-sub-query-list+ (expr &aux temp)
  "expr must contain only sub-queries. Should at least contain one."
  (unless expr
    (format t "~&Error in or-constrained-sub-query: missing subquery")
    (throw :parse nil))
  (unless (listp expr)
    (format t "~&Error in or-constrained-sub-query: we shold have a list of~
               subqueries: ~&~S" expr)
    (throw :parse nil))
  (cond 
   ((setq temp (check-sub-query (car expr)))
    (cons temp (check-sub-query-list (cdr expr))))
   (t (format t "Error in sub-query-list format: ~S" expr)
      (throw :parse nil))))

;;;------------------------------------------------------------ CHECK-SUB-QUERY-LIST

(defUn check-sub-query-list (expr &aux temp)
  "expr must contain only sub-queries, otherwise we have an error, and throw ~
   to :parse"
  (cond 
   ((null expr)  nil)
   ((setq temp (check-sub-query (car expr)))
    (cons temp (check-sub-query-list (cdr expr))))
   (t (format t "Error in sub-query-list format: ~S" expr)
      (throw :parse nil))))

;;;--------------------------------------------------------------- COLLECT-VARIABLES

(defUn collect-variables (expr &optional result)
  "makes a list of the variables in the query"
  (cond ((null expr) (remove-duplicates result))
        ((symbolp expr) (if (variable? expr) (list expr)))
        ((listp expr)
         (collect-variables (cdr expr)
                            (append (collect-variables (car expr)) result)))))

#|
(moss::collect-variables '($E-PERSON ($S-PERSON-BROTHER (> 0) ($E-PERSON))
                                     ($T-PERSON-FIRST-NAME :IS ?X)))
(?X)
|#
;;;================================================================================
;;;
;;;                        Step 3 : generate candidates
;;;
;;;================================================================================
;;; Step 1 takes into account entry-points if any, in order to reduce the 
;;; starting set of objects of the target class.
;;; When the query does not contain entry points, then this step is pointless.

;;;-------------------------------------------------------------- COLLECT-CANDIDATES
;;; used by the MOSS window interface

(defUn collect-candidates (parsed-query)
  "takes a parsed query as input and determines the set of objects to filter against
   If the argument is a symbol it should be an entry point, if it is a string, then ~
   all possible entry points for the application or the system are computed and ~
   corresponding objects are retrieved.
Arguments:
   parsed-query: parsed query, i.e., expressed in internal format
Returns:
   a list of objects to be tested."
  (let (entry-point-list)
    (cond
     ((stringp parsed-query)
      ;; if we have a string then we compute every possible entry point 
      ;; collecting all the make-entry functions from MOSS
      (setq entry-point-list (make-all-possible-entry-points parsed-query))
      ;; now for each entry point get the list of referenced objects
      (delete-duplicates
       (reduce  ;  JPB 140820 removing mapcan
        #'append
        (mapcar #'(lambda (xx)
                    (mapcar #'cdr (%%get-objects-from-entry-point xx)))
          entry-point-list))))
     ((symbolp parsed-query)
      ;; if symbol is not an entry point,then returns nil
      (mapcar #'cdr (%%get-objects-from-entry-point parsed-query)))
     ;; otherwise trying to determine a subset of target entities
     ((find-subset parsed-query))
     ;; otherwise generate list of class instances
     ((send (car parsed-query) '=get-instance-list)))))

#|
;;===== MOSS package

(moss::collect-candidates 'barthès)
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)

(moss::collect-candidates "barthès")
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)

(moss::collect-candidates '($E-person))
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)

(with-context 3
  (moss::collect-candidates '($E-person)))
($E-PERSON.1 $E-PERSON.2 $E-PERSON.3 $E-STUDENT.1)

(moss::collect-candidates '($E-person ($T-PERSON-NAME :is "Barthès")))
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)

(with-context 3
  (moss::collect-candidates '($E-person ($T-PERSON-NAME :is "Barthès"))))
($E-PERSON.1 $E-PERSON.2 $E-PERSON.3)
;; $E-STUDENT.1 does not exist in context 3 because it was created in context 0
;; after $E-PERSON.3 was created in context 3. Thus context 3 was modified to 
;; include $E-PERSON.3, but not upgraded when $E-STUDENT.1 was created...

(with-context 3
  (send '$e-person '=get-instance-list))
($E-PERSON.1 $E-PERSON.2 $E-PERSON.3 $E-STUDENT.1)

(with-context 3
  (find-subset '($E-person ($T-PERSON-NAME :is "Barthès"))))
($E-PERSON.1 $E-PERSON.2 $E-PERSON.3)

;;===== TEST package

(with-package :test
  (moss::collect-candidates 'test::barthès))
((TEST::$E-PERSON . 1) (TEST::$E-PERSON . 2) (TEST::$E-STUDENT . 1))

(with-package :test
  (moss::collect-candidates "barthès"))
((TEST::$E-PERSON . 1) (TEST::$E-PERSON . 2) (TEST::$E-STUDENT . 1))

(with-package :test
  (moss::collect-candidates '(test::$E-person)))
((TEST::$E-PERSON . 1) (TEST::$E-PERSON . 2) (TEST::$E-STUDENT . 1))

(with-package :test
  (moss::collect-candidates
   '(test::$E-person (test::$T-PERSON-NAME :is "Barthès"))))
((TEST::$E-PERSON . 1) (TEST::$E-PERSON . 2) (TEST::$E-STUDENT . 1))

(with-package :test
  (with-context 3
    (moss::collect-candidates
     '(test::$E-person (test::$T-PERSON-NAME :is "Barthès")))))
((TEST::$E-PERSON . 1) (TEST::$E-PERSON . 2) (TEST::$E-PERSON . 3))
|#
;;;--------------------------------------------------------------------- FIND-SUBSET

(defUn find-subset (expr &key (package *package*))
  "Function that returns a list of objects computed from the entry-points ~
   eventually found in the sub-query corresponding to the expr argument.
   When the function returns nil, then this means that there is no limit ~
   coming from the current branch, when the function returns the symbol ~
   *none*, then this means that the current branch has no possible solution.
Arguments:
   expr: parsed query
   package (key): used by =make-entry functions that produce symbols"
  ;(qformat "~&;=== find-subset; expr: ~S" expr) ; use trace instead
  (cond 
   ;   ((prog1 nil (print expr)))
   ;; expr must be non nil. If nil or symbol, we discard
   ((or (not (listp expr)) (symbolp expr)) nil)
   
   ;; when expr is a terminal property, then we check further
   ((%is-terminal-property? (car expr))
    ;; we must check for entry-point (careful: method may be an entry point)
    (let ((%method (get-method (car expr) '=make-entry)))
      ;; check for presence of variable (we could have (<tp> <opr> <var>)
      ;; in which case we cannot use the entry-point
      ;; the operator must also be positive, it cannot be a cardinality operator
      (unless (or (variable? (caddr expr))
                  (not (positive-operator? (cadr expr)))
                  (tp-clause-cardinal-operator? (cadr expr))
                  (get-method (car expr) '=if-needed) ; JPB 1010
                  (null %method))
        ;; then we can access the data from the entry point
        (=> (car expr) '=make-entry (caddr expr))
        ;; if there is an answer we access the corresponding objects
        (when *answer* ; jpb 1406
          ;; =make-entry returns a list. In case the value is an MLN there may
          ;; be several entry points some of them not present in the object we 
          ;; are looking for. Thus we must check each entry in turn
          (or
           (delete-duplicates
            (loop for item in *answer* if (%is-entry? item)
                append (send item '=get-id (%inverse-property-id (car expr)))))
           '*none*)
          ;;;          (dolist (item *answer*)
          ;;;            (when (%is-entry? item)
          ;;;              (send item '=get-id (%inverse-property-id (car expr)))
          ;;;              ;; if we have an answer return it
          ;;;              (if *answer* (return *answer*)))
          ;;;            )
          ;;;;; here we did not find any object with this entry point and attribute
          ;;; (or *answer* '*none*)
          )
        )))
   
   ;; if we have a list starting with OR, we simply merge the results
   ;; we first deal with constrained or-clauses, e.g. (OR (> 2)(...)(...))
   ((and (eq 'OR (car expr)) (positive-condition? (cadr expr)))
    ;; in that case we just ignore the condition
    (let ((result-list (mapcar #'(lambda (xx) (find-subset xx))
                         (cddr expr))))
      ;; If some are nil, this means that there is no limit to the candidate-list 
      (if (member nil result-list)
          nil
        ;; otherwise,we must remove the *none* symbols
        (reduce #'append (remove '*none* result-list :test #'equal)))))
   
   ((eq 'OR (car expr))
    ;; get result of applying find-subset to every branch
    (let ((result-list (mapcar #'(lambda (xx) (find-subset xx))
                         (cdr expr))))
      ;; If some are nil, this means that there is no limit to the candidate-list 
      (if (member nil result-list)
          nil
        ;; otherwise,we must remove the *none* symbols
        (reduce #'append (remove '*none* result-list :test #'equal)))))
   
   ;; if a relation or inverse relation
   ((%is-structural-property? (car expr))
    ;; if the cardinality condition is strictly positive, then we propagate the
    ;; subset information (opr is = > >= IS EQUAL BETWEEN
    ;; the rationale is that all the objects that will be loaded into the neighbor
    ;; node have to participate into the comparison. In other words, there should
    ;; be objects in the neighboring  node for the constraint to have a chance to
    ;; be satisfied. Thus, all objects that do not have successors for this property
    ;; can be safely ruled out
    ;; if relation has an =if-needed demon, the demon when executed may add neighbors
    ;; (ghost links). Therefore we must do a class access in that case
    (cond
     ((get-method (car expr) '=if-needed)
      nil)
     ((positive-condition? (cadr expr))
      ;; get restriction on the neighboring class
      (let ((entity-list (find-subset (cddr expr))))
        (cond ((eq entity-list '*none*) '*none*)
              ((null entity-list) nil)
              ;; when we obtain objects, we access all inverses
              ((remove-duplicates 
                (loop for item in entity-list append
                      (send item '=get-id (%inverse-property-id (car expr))))))
              ;; when there are no objects left, then we lose meaning that we 
              ;; cannot reach such objects from where we are
              ('*none*))))))
   
   ((%is-inverse-property? (car expr))
    ;; if the cardinality condition is positive, then we propagate the
    ;; subset information (opr is = > >= IS EQUAL BETWEEN
    ;; the rationale is that all the objects that will be loaded into the neighbor
    ;; node have to participate into the comparison. In other words, there should
    ;; be objects in the neighboring  node for the constraint to have a chance to
    ;; be satisfied. Thus, all objects that do not have successor for this property
    ;; can be safely ruled out, which through gamma-1 provides the largest list of
    ;; objects that can be candidates.
    (if (positive-condition? (cadr expr))
        (let ((entity-list (find-subset (cddr expr))))
          (cond ((eq entity-list '*none*) '*none*)
                ((null entity-list) nil)
                ;; when we obtain objects, we access all inverses
                ((remove-duplicates
                  (loop for item in entity-list append
                        (send item '=get-id (%inverse-property-id (car expr))))))
                ('*none*)))))
;;;                (t (remove-duplicates
;;;                    (reduce 
;;;                     #'append
;;;                     (mapcar
;;;                         #'(lambda (xx) 
;;;                             (send xx '=get-id (%inverse-property-id (car expr))))
;;;                       entity-list))))))))
   
   ;; when we have a class, we skip it
   ;; of course, we could compute an upper bound by looking at the counter
   ;; associated with the class, but this does not give us a list of objects
   ((%is-model? (car expr))
    ;; if we have a cardinality check we must discard it (ignore it temporarily)
    ;; this is illegal syntax... (person (> 3) ...)
    (find-subset (cdr expr) :package package))
   
   ;; any other kind of list is an implicit AND condition
   ((let ((entity-list (remove nil (mapcar #'find-subset expr))))
      ;; we have removed nils indicating no bound on a particular branch
      (cond 
       ;; if we have nil left, then we have no bounds
       ((null entity-list) nil)
       ;; if one of the branch has a *none* tag, then we have no solution
       ((member '*none* entity-list) '*none*)
       ;; otherwise,
       ;; we return the intersection of the various object lists of the different
       ;; branches, and, if the intersection is empty, we return *none*
       ;; Before doing that we must remove the nil tags indicating no upper limit
       ((remove-duplicates (intersect-sets entity-list)))
       ;; if the intersection does not leave anything, then we have no solution
       ('*none*)))
    )))

#|
(defUn find-subset (expr &key (package *package*))
  "Function that returns a list of objects computed from the entry-points ~
   eventually found in the sub-query corresponding to the expr argument.
   When the function returns nil, then this means that there is no limit ~
   coming from the current branch, when the function returns the symbol ~
   *none*, then this means that the current branch has no possible solution.
Arguments:
   expr: parsed query
   package (key): used by =make-entry functions that produce symbols"
  ;(qformat "~&;=== find-subset; expr: ~S" expr) ; use trace instead
  (cond 
   ;   ((prog1 nil (print expr)))
   ;; expr must be non nil. If nil or symbol, we discard
   ((or (not (listp expr)) (symbolp expr)) nil)
   
   ;; when expr is a terminal property, then we check further
   ((%is-terminal-property? (car expr))
    ;; we must check for entry-point
    (let ((method (get-method (car expr) '=make-entry)))
      ;; check for presence of variable (we could have (<tp> <opr> <var>)
      ;; in which case we cannot use the entry-point
      ;; the operator must also be positive, it cannot be a cardinality operator
      (unless (or (variable? (caddr expr))
                  (not (positive-operator? (cadr expr)))
                  (tp-clause-cardinal-operator? (cadr expr))
                  (null method))
        ;; then we can access the data from the entry point
        (=> (car expr) '=make-entry (caddr expr))
        ;; if there is an answer we access the corresponding objects
        (when *answer*
          ;; =make-entry returns a list, thus we send a message to the first
          ;; element of the list. The others if present are synonyms
          ;; the result may not be a MOSS object
          (if (%is-entry? (car *answer*))
            (progn
              (send (car *answer*) '=get-id (%inverse-property-id (car expr)))
              ;; when there are no answer we return the symbol *none*
              (or *answer* '*none*))
            '*none*)
          ))))
   
   ;; if we have a list starting with OR, we simply merge the results
   ;; we first deal with constrained or-clauses
   ((and (eq 'OR (car expr)) (positive-condition? (cadr expr)))
    ;; in that case we just ignore the condition
    (let ((result-list (mapcar #'(lambda (xx) (find-subset xx))
                               (cddr expr))))
      ;; If some are nil, this means that there is no limit to the candidate-list 
      (if (member nil result-list)
        nil
        ;; otherwise,we must remove the *none* symbols
        (reduce #'append (remove '*none* result-list :test #'equal)))))
   
   ((eq 'OR (car expr))
    ;; get result of applying find-subset to every branch
    (let ((result-list (mapcar #'(lambda (xx) (find-subset xx))
                               (cdr expr))))
      ;; If some are nil, this means that there is no limit to the candidate-list 
      (if (member nil result-list)
        nil
        ;; otherwise,we must remove the *none* symbols
        (reduce #'append (remove '*none* result-list :test #'equal)))))
   
   ;; if a relation or inverse relation
   ((or (%is-structural-property? (car expr))
        (%is-inverse-property? (car expr)))
    ;; if the cardinality condition is positive, then we propagate the
    ;; subset information (opr is = > >= IS EQUAL BETWEEN
    ;; the rationale is that all the objects that will be loaded into the neighbor
    ;; node have to participate into the comparison. In other words, there should
    ;; be objects in the neighboring  node for the constraint to have a chance to
    ;; be satisfied. Thus, all objects that do not have successor for this property
    ;; can be safely ruled out, which through gamma-1 provides the largest list of
    ;; objects that can be candidates.
    (if (positive-condition? (cadr expr))
      (let ((entity-list (find-subset (cddr expr))))
        (cond ((eq entity-list '*none*) '*none*)
              ((null entity-list) nil)
              ;; when we obtain objects, we access all inverses
              (t (remove-duplicates
                  (reduce 
                   #'append
                   (mapcar
                    #'(lambda (xx) 
                        (send xx '=get-id (%inverse-property-id (car expr))))
                    entity-list))))))))
   
   ;; when we have a class, then we do nothing
   ;; of course, we could compute an upper bound by looking at the counter
   ;; associated with the class, but this does not give us a list of objects
   ((%is-model? (car expr))
    (find-subset (cdr expr) :package package))
   
   ;; any other kind of list is an implicit AND condition
   ((let ((entity-list (remove nil (mapcar #'find-subset expr))))
      ;; we have removed nils indicating no bound on a particular branch
      (cond 
       ;; if we have nil left, then we have no bounds
       ((null entity-list) nil)
       ;; if one of the branch has a *none* tag, then we have no solution
       ((member '*none* entity-list) '*none*)
       ;; otherwise,
       ;; we return the intersection of the various object lists of the different
       ;; branches, and, if the intersection is empty, we return *none*
       ;; Before doing that we must remove the nil tags indicating no upper limit
       ((remove-duplicates (intersect-sets entity-list)))
       ;; if the intersection does not leave anything, then we have no solution
       ('*none*)))
    )))
|#

#|
;;; test on the family file
(moss::find-subset '($E-person))    ; class access, no limit
NIL

(moss::find-subset '($T-PERSON-NAME :is "Barthès"))  
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)

(moss::find-subset '($E-student ($T-PERSON-NAME :is "Barthès")))
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)
; no difference (same ep) 3 objects

(moss::find-subset '($E-student ($T-PERSON-NAME :is "Zoe")))
*NONE*
; no objects

(moss::find-subset '(($T-PERSON-NAME :is "Barthès")($T-person-sex :is "f")))
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)
; 3 objects / sex is not an entry point

(moss::find-subset '(($T-PERSON-FIRST-NAME :is "Bernard")($T-person-sex :is "f")))
NIL
; AND clause, no limit

(moss::find-subset '($S-PERSON-HUSBAND 
                     (> 0) ($E-person ($T-PERSON-name :is "barthès"))))
($E-PERSON.2)
; >0 opr 1 objects

(moss::find-subset '($S-PERSON-HUSBAND 
                     (> 0) ($E-person ($T-person-name :is-not "barthès"))))
NIL ; no limit

(moss::find-subset '($E-person
                     ($T-PERSON-NAME :is "barthès")
                     (or ($S-PERSON-BROTHER 
                          (> 0) ($E-person ($T-person-name :is "barthes")))
                         ($S-PERSON-SISTER 
                          (> 0) ($E-person ($T-person-name :is "labrousse"))))))
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)
; 3 objects

(moss::find-subset '($E-person   ; not the same as above
                     (or ($T-PERSON-NAME :is "barthès")
                         ($T-PERSON-NAME :is "labrousse"))
                     (or ($S-PERSON-BROTHER 
                          (> 0) ($E-person ($T-person-name :is "barthès")))
                         ($S-PERSON-SISTER 
                          (> 0) ($E-person ($T-person-name :is "labrousse"))))))
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)
; 10 objects

(moss::find-subset '($S-PERSON-BROTHER 
                     (>= 2) ($E-person ($T-person-name :is "barthès")))) 
; 12 objects

(moss::find-subset '(($T-PERSON-NAME :is "Barthès")($T-PERSON-NAME :is "Labrousse"))) 
; intersection 1 object

(moss::find-subset '(($T-PERSON-NAME :is "Barthès")($T-PERSON-NAME :is "Zorglub"))) 
; no solution (no error message)

(moss::find-subset '(($T-PERSON-NAME :is "Barthès")  ; some intersection
                     ($S-PERSON-SISTER 
                      (> 0) ($E-person ($T-PERSON-name :is "labrousse")))))
; 2 objects

(moss::find-subset '($E-person       ; positive condition
                     (or (> 2) ($S-PERSON-BROTHER 
                                (> 0) ($E-person ($T-PERSON-name :is "barthès")))
                         ($S-PERSON-SISTER 
                          (> 0) ($E-person ($T-PERSON-name :is "labrousse"))))))
; 10 objects

(moss::find-subset '($E-person   ; negative condition
                     (or (< 2) ($S-PERSON-BROTHER 
                                (> 0) ($E-person ($T-PERSON-name :is "barthès")))
                         ($S-PERSON-SISTER 
                          (> 0) ($E-person ($T-PERSON-name :is "labrousse"))))))
NIL ; no limit 
|#
;;;---------------------------------------------------------- GENERATE-INSTANCE-LIST
;;; unused
;;;(defun generate-instance-list (model-id)
;;;  "Builds a list of instances for a given class. If the global variable ~
;;;   *query-allow-sub-classes* is true adds instances of the spanned sub-classes."
;;;  (flet ((ff (xx)
;;;           (=> xx '=get-instances)))
;;;    (if *query-allow-sub-classes*
;;;      (delete-duplicates ; JPB060810
;;;       (reduce #'append 
;;;               (mapcar #'ff 
;;;                       (%sp-gamma model-id (%inverse-property-id '$IS-A)))))
;;;      (ff model-id))))
;;;#|
;;;moss::*query-allow-sub-classes*
;;;T
;;;? (moss::generate-instance-list '$E-person)
;;;($E-PERSON.1 $E-PERSON.2 $E-PERSON.3 $E-PERSON.4 $E-PERSON.5 $E-PERSON.6 $E-PERSON.7
;;; $E-PERSON.8 $E-PERSON.9 $E-PERSON.10 $E-PERSON.11 $E-PERSON.12 $E-PERSON.13
;;; $E-PERSON.14 $E-PERSON.15 $E-PERSON.16 $E-PERSON.17 $E-PERSON.18 $E-PERSON.19
;;; $E-PERSON.20 $E-PERSON.21 $E-STUDENT.1 $E-STUDENT.2 $E-STUDENT.3 $E-STUDENT.4
;;; $E-STUDENT.5 $E-STUDENT.6)
;;;|#
;;;================================================================================
;;;
;;;                     Step 4 : access and filter candidates
;;;
;;;================================================================================
;;; In Step 4 we start from a set of objects and a (sub-)query, and for each object 
;;; in turn we check that the (sub-)query is verified.
#|
(trace access access-from-entry-point parse-user-query find-subset filter-candidates
       split-query-and-compute-filters validate2 collect-fail-test? 
       collect-success-test?)
|#
;;;------------------------------------------------------------------------- ACCESS

(defUn access (query &key already-parsed (verbose nil))
  "takes a parsed query and tries to access the database. Specifying the ~
   package is useful for the =make-entry functions that produce symbols.
Arguments:
   query: parsed or unparsed
   already-parsed (opt): if true indicates that the query is parsed.
   package (key): package in which the query symbols are defined (default current)
Returns
   a list of objects filtered by the constraints specified in the query."
  (let ((context (symbol-value (intern "*CONTEXT*")))
        parsed-query candidate-list result)
    ;; this clause catches access errors returning a null list
    (catch 
     :access-error
     
     ;; check first if we have a single entry point. If so, special access
     (if (or (symbolp query) (stringp query))
         (return-from access (access-from-entry-point query :verbose verbose)))
     
     ;; parse query unless already done
     (setq parsed-query (if already-parsed query (parse-user-query query)))
     ;; if parsed query is nil then quit (bad syntax)
     (unless parsed-query (return-from access nil))
     
     ;; --- Now ready to process parsed query
     (catch 
      :access-error
      ;; first compute a list of possible candidates
      (setq candidate-list 
            (cond
             ;; otherwise trying to determine a subset of target entities
             ((find-subset parsed-query))
             ;; otherwise generate the list of class instances
             ((send (car parsed-query) '=get-instance-list))))
      
      ;; if the resulting candidate-list is *none*, then we have no solutions
      (when (or (eq candidate-list '*none*)  ; early failure detected by find-subset
                (null candidate-list)) ; no instances in this class ??
        (if verbose 
            ;; report 
            (format t "~&...access : early test indicates that no object satisfies ~
                       the query."))
        (return-from access nil))
      
      ;; otherwise we are ready to filter our candidates
      ;; first report
      (if verbose
          (format t "~&...access ; starting object set contains ~A candidates:~
                     ~&~A ~A~&" 
            (length candidate-list) 
            (>>f (if (<= (length candidate-list) 6)
                candidate-list
              (subseq candidate-list 0 6)))
            (if (<= (length candidate-list) 6) "" "...")))
      
      ;; then call filtering function
      (setq result 
            (filter-candidates 
             candidate-list parsed-query '(:all) () :early-success-allowed nil
             :verbose verbose))
      ;; remove dead object and return
      (remove-if-not #'(lambda(xx) (%alive? xx context)) result)
      ))))

#|
;;;===== test on family data
;;; macro to print the results
(defMacro ppp ()
  `(progn (mapcar #'(lambda (xx) (print (send xx '=summary))) *)
          nil))

;;; all persons with no brother, i.e. who ARE KNOWN to have no brother
(access '(person (has-sister (= 0) (person))))
;;; answer should be psb

;;; access all males
(access '(person (has-sex is "m")))

;;; ===== For the next ones use MOSS window...
;;;   access all person named "Barthès" and not known to be males
(moss::access '(person (has-name is "Barthès")(has-sex is-not "m")))

;;;   access all persons with brothers
(moss::access '(person (has-brother (person))))

;;;   access all persons with no (gettable or recorded) brothers (close world)
(moss::access '(person (has-brother (= 0) (person))))
BUG: id.

;;;   access all Barthes's who have only one brother called "Sebastien"
(moss::access 
 '(person (has-name is "barthès")
          (has-brother (= 1) (person (has-first-name is "Sébastien")))))
;;; answer should be ab, eb

;;;   access all persons brother of a person called sebastien
(moss::access 
 '(person (is-brother-of (= 1) (person (has-first-name is "Sébastien")))))
;;; answer should be ab

;;;   access all persons with at least 2 children of any sex
(moss::access 
 '(person (or (>= 2) (has-son (>= 0) (person))
              (has-daughter (>= 0) (person)))))
;;; answer should be: jpb dbb pxb chb pb mb mbl ml

;;;=== the following question removes jpb and dbb from the list. Indeed, one must
;;; have at least 2 sons if any, or 2 girls.
(moss::access 
 '(person (or (>= 2) (has-son (>= 2) (person))
              (has-daughter (>= 0) (person)))))
;;; answer should be: pxb, chb, pb, mb, mbl, ml

;;; The following question removes mlb and ml, since we want at most 2 girls.
(moss::access 
 '(person (or (>= 2) (has-son (>= 2) (person))
              (has-daughter (<= 2) (person)))))
;;; answer should be: pxb, chb, pb, mb

;;; next query has a bad syntax (cannot include TP-clause in a constrained sub-query)
(moss::access 
 '(person (or (>= 2) (has-son (>= 2) (person))
              (has-sex = "f")
              (has-daughter (<= 2) (person)))))
;;; correct syntax should be. However it is not accepted (recursive OR)
(moss::access 
 '(person (or (has-sex is "f")
              (or (>= 2) (has-son (>= 2) (person))
                  (has-daughter (<= 2) (person))))))
;;; OK got all the girls

;;; access all persons whith at least 2 children and most 2 girls and a boy
;;; answer is jpb, dbb
(moss::access 
 '(person (or (>= 2) (has-son (<= 1) (person))
              (has-daughter (<= 2) (person)))))

;;;   access all persons with at least 3 children one of them being a son
;;; note that we cannot do that with a single OR clause.
;;; answer is pxb, chb, pb, mb
(moss::access 
 '(person (or (>= 3) (has-son (>= 0) (person))
              (has-daughter (>= 0) (person)))
          (has-son (>= 1) (person))))

;;;   persons who are cousins of a person who has a father with name "Labrousse"
;;; answer is: psb cxb ab sb eb
(moss::access 
 '(person (is-cousin-of  
           (person (has-father 
                    (person (has-name is "Labrousse")))))))

;;;   persons who have a brother who is cousin of a person who has a father...
;;; answer is: psb sb eb ab
(moss::access 
 '(person (has-brother
           (person (is-cousin-of
                    (person (has-father 
                             (person (has-name is "Labrousse")))))))))

;;;   persons who have a father with more than 1 boy
;;; answer is: jpb pxb ab sb eb mglb
(moss::access 
 '(person (has-father (person (has-son (>= 2) (person))))))

;;;   persons who have  a father with  at least 3 children with at least one girl
;;;           and whose father has a brother
;;; answer: ab, sb, eb
(moss::access 
 '(person (has-father
           (person   ; condition on the cardinality of the subset!
            (or (>= 3) (has-son (> -1) (person))
                (has-daughter (> -1) (person)))
            (has-daughter (>= 1) (person))
            (has-brother (person))))))

;;; persons who have 2 names: dbb, mgl
(moss::access 
 '(person (has-name card= 2)))

(moss::access 
 '(person (has-name is "labrousse")(has-name card= 2)))

;;; persons with more than 1 first name: jpb, cxb, chb (!), psb
(moss::access 
 '(person (has-first-name card>= 2)))

;;; ------------------- Queries with coreference varaibles
;;;   person who has a cousin of the same sex
;;; answer should be: cxb ab sb eb sl cl al psb
(moss::access 
 '(person (has-sex is ?x) (has-cousin (person (has-sex is ?x)))))

;;;   person who has a son with the same name as the son of his brother
;;; illegal query since one cannot set coreference variables in a sub-query
(moss::access 
 '(person (has-son  (person (has-first-name is ?X)))
   (has-brother 
    (person (has-son  (person (has-first-name is-not ?X)))))))
*** The first occurence of a coreference variable should be in an assignment, which is not the case:
($T-PERSON-FIRST-NAME :IS-NOT ?X)

;;; ------------------- Query with an if-needed demon
in the ZE package (Milton's PA)
(access '("address" (">address" ("person" ("name" :is "Paraiso")))))
($E-OFFICE-ADDRESS.3)
|#
;;;--------------------------------------------------------- ACCESS-FROM-ENTRY-POINT

(defUn access-from-entry-point (entry-ref &key (verbose t))
  "uses entry-ref to access all possible objects referenced by any entry point  ~
   that can be built from entry-ref when it is a string.
Arguments:
   entry-ref: a symbol or a string
   verbose (key): prints something when t
Return:
   a list of objects."
  (declare (ignore verbose))
  (let (entry-point-list)
    (setq entry-point-list
          (cond
           ;; if we have a string then we compute every possible entry point collecting
           ;; all the =make-entry functions from MOSS
           ((stringp entry-ref)
            (make-all-possible-entry-points entry-ref))
           ;; if symbol we make a list
           ((symbolp entry-ref)
            (list entry-ref))
           ;; otherwise error
           (t (error "entry-ref should be a symbol or a string, not: ~S" entry-ref))))
    ;; now for each entry point get the list of referenced objects
    (delete-duplicates
     (reduce  ; JPB 140820 removing mapcan
      #'append
      (mapcar #'(lambda (xx)
                  (mapcar #'cdr (%%get-objects-from-entry-point xx)))
        entry-point-list)))))

#|
(MOSS::ACCESS-FROM-ENTRY-POINT "barthès")
($E-PERSON.1 $E-PERSON.2 $E-PERSON.3 $E-STUDENT.1 $E-PERSON.4 $E-PERSON.5
             $E-PERSON.6 $E-PERSON.7 $E-PERSON.8 $E-PERSON.9 $E-PERSON.10 $E-PERSON.11)

(MOSS::ACCESS-FROM-ENTRY-POINT "Barths Biesel")
NIL

(MOSS::ACCESS-FROM-ENTRY-POINT 23)
Error: entry-ref should be a symbol or a string, not: 23

(MOSS::ACCESS-FROM-ENTRY-POINT "moss method name")
($MNAM)

(MOSS::ACCESS-FROM-ENTRY-POINT "name")
($T-NAME $T-TARGET-NAME $T-JUNK-NAME $T-ORGANIZATION-NAME $T-PERSON-NAME
 $T-COUNTRY-NAME)

(MOSS::ACCESS-FROM-ENTRY-POINT "name")
($T-NAME $T-TARGET-NAME $T-JUNK-NAME $T-ORGANIZATION-NAME $T-PERSON-NAME
 $T-COUNTRY-NAME)

(MOSS::ACCESS-FROM-ENTRY-POINT 'has-name)
($T-NAME $T-TARGET-NAME $T-JUNK-NAME $T-ORGANIZATION-NAME $T-PERSON-NAME
 $T-COUNTRY-NAME)
|#
;;;--------------------------------------------- COLLECT-CHECK-CLAUSE-WITH-VARIABLE

(defUn collect-check-clause-with-variable (candidate clause bindings)
  "checks a clause containing a variable. If the variable is not in bindings ~
   and if the operator is an assignment operator, then we simply add it to ~
   the bindings with the values of the candidates's attribute. If the operator ~
   is not an assignment, then we have an access error (bad query syntax).
   When the variable is in the bindings, then we use the operator to check the ~
   recorded value.
Arguments:
   candidate: the object to be tested
   clause: the clause containing the attribute variable
   bindings: a list of bindings
Returns:
   a new binding or nil in case of operator failure."
  (drformat :query 2 "~2%;---------- Entering collect-check-clause-with-variable")
  (let ((var (caddr clause)))
    (dformat :query 2 "var: ~S" var)
    ;; check bindings
    (cond
     ((assoc var bindings)
      ;; must check the conditon
      (let ((opr `(lambda (xx) 
                    ,(make-local-filter-for-multiple-values 
                      (cdr (assoc var bindings)) (car clause) (cadr clause)))))
        (dformat :query 2 "filter: ~S" opr)
        ;; apply filter to the candidate, return bindings if success
        (when (eval `(,opr ',candidate))
          (drformat :query 2 "~%;---------- End collect-check-clause-with-variable")
          bindings))
      )
     ;; we encounter the variable for the first time must have an assignment
     ((member (cadr clause) *assignment-operators*)
      (dformat :query 2 "Encountering var for first time, make binding: ~S" 
               (cons var (%get-value candidate (car clause))))
      (push (cons var (%get-value candidate (car clause))) bindings)
      (drformat :query 2 "~%;---------- End collect-check-clause-with-variable")
      bindings
      )
     (t (tell "~&*** The first occurence of a coreference variable should be ~
               in an assignment, which is not the case:~%~S" clause)
        (throw :access-error nil)))))

#|
(MOSS::collect-check-clause-with-variable 
   '$E-PERSON.8
   '($T-PERSON-NAME :is ?x)
   '())
((?X "Barthès"))

(MOSS::collect-check-clause-with-variable 
   '$E-PERSON.2
   '($T-PERSON-NAME :is ?x)
   '((?X "Barthès")))
((?X "Barthès"))

(MOSS::collect-check-clause-with-variable 
   '$E-PERSON.2
   '($T-PERSON-NAME :is-not ?x)
   '((?X "Barthès")))
NIL

(MOSS::collect-check-clause-with-variable 
   '$E-PERSON.2
   '($T-PERSON-NAME :is ?Y)
   '((?X "Labrousse")))
((?Y "Barthès" "Biesel") (?X "Labrousse"))
|#
;;;---------------------------------------- COLLECT-CHECK-CLAUSE-LIST-WITH-VARIABLES

(defUn collect-check-clause-list-with-variables (candidate clauses bindings)
  "Checks if the specified candidate corresponds to the specified clauses. ~
   The algorithm is as follows:
    - check each clause in turn. 
       - If the variable is new (not in the bindings) add it to the bindings ~
   with the candidate attribute values
       - if the variable is already in the context, then the values in the ~
   candidate should match the recorded values using the operator. ~
   If not, then return nil.
Arguments:
   candidate: the object to check
   clauses the list of clauses with variables
   bindings: an initial binding
Return:
   nil or bindings"
  (drformat :query 1 "~2%;---------- Entering collect-check-clause-list-with-variables")
  (dformat :query 1 "candidate: ~S" candidate)
  (dformat :query 1 "clauses: ~S" clauses)
  (dformat :query 1 "initial bindings: ~S" bindings)
;;;  (qformat "collect-check-clause-list-with-variables; Checking variable clauses:~
;;;            ~%candidate: ~S ~
;;;            ~%clauses: ~S ~%initial bindings: ~S" 
;;;           candidate clauses bindings)
  (dolist (clause clauses)
    ;; checks the clause, eventually updating the bindings
    (setq bindings (collect-check-clause-with-variable candidate clause bindings))
    ;; on failure, the previous function returns nil
    (unless bindings 
      (drformat :query 1 "~%;---------- Exit collect-check-clause-list-with-variables")
      (return-from collect-check-clause-list-with-variables nil)))
  (dformat :query 1 "resulting bindings: ~S" bindings)
  (drformat :query 1 "~%;---------- Exit collect-check-clause-list-with-variables")
  bindings)

#|
(MOSS::collect-check-clause-list-with-variables 
 '$E-PERSON.2
 '(($T-PERSON-NAME :is ?x)
   ($T-PERSON-FIRST-NAME :is ?y))
 '((?X "Barthès")(?y "Dominique")))
((?X "Barthès") (?Y "Dominique"))

(MOSS::collect-check-clause-list-with-variables 
 '$E-PERSON.2
 '(($T-PERSON-NAME :is ?x)
   ($T-PERSON-FIRST-NAME :is ?y))
 '((?X "Barthès")(?y "Bernard")))
NIL

(MOSS::collect-check-clause-list-with-variables 
 '$E-PERSON.2
 '(($T-PERSON-NAME :is ?x)
   ($T-PERSON-FIRST-NAME :is ?y))
 '((?X "Barthès")))
((?Y "Dominique") (?X "Barthès"))
|#
;;;--------------------------------------------------- COLLECT-CHECK-EARLY-SUCCESS?

(defun collect-check-early-success? (test local-list old-list)
  "we check local-list (immediate return from subquery) and old list (recorded ~
   results from past result to see if we have an immediate success.
Arguments:
   test: e.g. (> 3)
   local-list: the list that was just returned by a subquery
   old-list: a list of numbers corresponding to combinations of the length of ~
      previous returns
Return:
   the updated old-list taking into accoount the length of the local list, or t if ~
   the test succeedes."
  (let ((nn (length local-list))
        (opr (car test))
        )
    ;; update old-list with new value
    (setq old-list
          (delete-duplicates 
           (append old-list (list nn) 
                   (mapcar #'(lambda(xx) (+ xx nn)) old-list))))
    ;(print old-list)
    (dolist (item old-list)
      
      (case opr
        ((< <= = >= >) 
         (if (funcall opr item (cadr test))
             (return-from collect-check-early-success? t)))
        (<>
         (unless (= item (cadr test))
           (return-from collect-check-early-success? t)))
        (:between
         (if (between item (cdr test)) 
             (return-from collect-check-early-success? t)))
        (:outside
         (if (not (between item (cdr test))) 
                  (return-from collect-check-early-success? t)))
        ))
      ;; if we are still there we failed and return updated old-list
      old-list))
      
#|
(moss::collect-check-early-success? '(< 2) '(a) nil)
T

(moss::collect-check-early-success? '(< 2) '(a b) nil)
(2)

(moss::collect-check-early-success? '(< 2) '(a) '(2))
T
;; previous check was 2 new one is 1, OK

(moss::collect-check-early-success? '(:BETWEEN 2 3) '(a) nil)
(1)

(moss::collect-check-early-success? '(:BETWEEN 3 4) '(a) '(1))
(1 2)

(moss::collect-check-early-success? '(:BETWEEN 3 4) '(a b c d e) '(1 2))
(1 2 5 6 7)

(moss::collect-check-early-success? '(>= 2) '(a) '(1))
T

(moss::collect-check-early-success? '(<> 2) '(a b) ())
(2)

(moss::collect-check-early-success? '(<> 2) '(a b) '(2))
T
|#         
;;;--------------------------------------------------------- COLLECT-CHECK-OR-CLAUSE
;;;
;;; OR clauses must not SET coreference variables. However, they can contain such
;;; variables to be checked against the context, e.g.
;;;   (person (has-name is ?x)
;;;           (has-brother (person (OR  (has-name is-not ?x)
;;;                                     (has-hair-color is "red")))))
;;; Therefore OR clauses cannot return a meaningful binding
;;;
;;; Examples
;;;   (OR (has-name is-not ?x)
;;;       (has-hair-color is "red")))))
;;;   cannot be transformed into a local filter, has to be checked separately
;;;
;;;   (OR (has-hair-color is "red")
;;;       (has-brother (person (has-name is "Barthès")
;;;                            (has-first-name is ?x))))

(defUn collect-check-or-clause (candidate or-clause bindings)
  "Function to deal with OR-clauses. If the OR-clause does not contain a  ~
   cardinality test, then we simply test each clause in turn until one is ~
   satisfied. Otherwise, we must proceed differently.
   If terminal clauses are present, then the cardinality constraint does not ~
   apply to those. We simply use them as a filter test on each candidate in turn ~
   If any of the terminal clause succeeds, then the OR-clause succeeds. ~
   Otherwise, we must test the sub-queries, with the cardinality constraints. ~
   To do so, we test each sub-query in turn, disallowing early successes (but ~
   not early failures). The sub-query test must return the list of objects ~
   that have passed whatever screening there was to pass. We test for an ~
   early  success. Otherwise, we continue until all the sub-query clauses have ~
   been checked.
Arguments:
   candidate: object to be tested
   or-clause: clause
Return:
   nil: "
  (let (old-list tp-clauses sp-clauses)
    ;; first separate simple clauses from sub-queries
    ;; cardinality condition is (OR (> 2) ...)
    ;; However, such a condition is meaningless
    (dolist (clause (if (cardinality-condition? (cadr or-clause))
                      (cddr or-clause)
                      (cdr or-clause)))
      (cond ((%is-terminal-property? (car clause))
             (push clause tp-clauses))
            ((or (%is-structural-property? (car clause))
                 (%is-inverse-property? (car clause)))
             (push clause sp-clauses))
            (t (warn "Unknown clause ~A in the query ~A" 
                     clause or-clause))))
    
    ;; then check for cardinality condition
    (cond
     ((cardinality-condition? (cadr or-clause))
      ;; then test each tp-clause in turn. If any is OK, then we succeed
      (dolist (clause tp-clauses)
        ;; if the clause fails, we examine the next one with the initial bindings
        ;; indeed, the returned bindings was nil...
        ;; a simple clause SETTING a coreference variable is simply ignored
        ;; but coreference variables are not ignored when checking
        (cond
         ((simple-clause-with-variable? clause))
         ;; build a local filter and apply it to the candidate
         ((let ((opr `(lambda (xx) ,(make-local-filter-clause 
                                     (caddr clause) (car clause) (cadr clause)))))
            ;; apply filter to the candidate
            (eval `(,opr ',candidate)))
          ;; if it checks, OR clause succeeds
          (return-from collect-check-or-clause t))))
      
      ;; then test each subquery in turn, this is more difficult
      ;; e.g.  ($S-PERSON-BROTHER.OF (> 2) ($E-PERSON))
      (when sp-clauses ; build test function only if necessary
        ;; for each relation or inverse relation, get the set of successors
        
        ;; Then, we try to filter that set using the sub-query
        ;; It is like accessing the subquery with a candidate list
        (dolist (clause sp-clauses)
          (multiple-value-bind 
            (object-list success)
            (filter-candidates   
             (send candidate '=get-id (car clause)) ; meighbors
             (caddr clause)  ; sub-query
             (cadr clause)   ; test coming from the property
             bindings
             :early-success-allowed nil ; collect all good objects
             )
            (when success ; from the filter-candidates function
              ;; check for an early success, if so will return T, otherwise returns
              ;; an updated version of old-list
              ;; the test is on a combination of the number of objects returned by
              ;; each subquery
              (setq old-list 
                    (collect-check-early-success? 
                     (cadr or-clause) object-list old-list))
              (unless (listp old-list)
                (qformat "collect-check-or-clause; early success; opr: ~S; old-list: ~S"
                         (cadr or-clause) old-list)
                (return-from collect-check-or-clause t))
              )
            ;; on failure of the clause we simple look at the next one
            ))
        ;; here none of the tests worked, therefore we fail
        (return-from collect-check-or-clause nil)
        ))
     
     (t
      ;; when the OR-clause does not contain any cardinality condition, 
      ;; then we check each sub-clause in turn until one succeeds
      (dolist (clause tp-clauses)
        ;; same as above
        (cond
         ((simple-clause-with-variable? clause))
         ;; build a local filter and apply it to the candidate
         ((let ((opr `(lambda (xx) ,(make-local-filter-clause 
                                     (caddr clause) (car clause) (cadr clause)))))
            ;; apply filter to the candidate
            (eval `(,opr ',candidate)))
          ;; if it checks, OR clause succeeds
          (return-from collect-check-or-clause t))))
      ;; check now sub-queries
      (dolist (clause sp-clauses)
        (multiple-value-bind 
          (object-list success)
          (filter-candidates   
           (send candidate '=get-id (car clause)) ; meighbors
           (caddr clause)  ; sub-query
           (cadr clause)   ; test coming from the property
           bindings
           )
          (declare (ignore object-list))
          (when success (return-from collect-check-or-clause t)))
        ;; on failure of the clause we simple look at the next one
        )
      ;; no more clauses, then failure
      (return-from collect-check-or-clause nil)
      ))))


;;;--------------------------------------------------- COLLECT-CHECK-OR-CLAUSE-LIST

(defUn collect-check-or-clause-list (candidate clauses bindings)
  "Checks if the specified candidate corresponds to the specified clauses. ~
   OR clauses use coreference variables but do not set them.
   If one of the OR-clause fails the whole set fails
Arguments:
   candidate: the object to check
   clauses the list of clauses with variables
   bindings: an initial binding
Return:
   nil or bindings"
  (let (result)
    (qformat "collect-check-or-clause-list; Checking variable clauses:~
              ~%candidate: ~S ~
              ~%clauses: ~S ~%initial bindings: ~S" 
             candidate clauses bindings)
    (dolist (clause clauses)
      ;; checks the clause, eventually updating the bindings
      (setq result (collect-check-or-clause candidate clause bindings))
      ;; on failure, the previous function returns nil
      (unless result (return-from collect-check-or-clause-list nil)))
    (qformat "collect-check-or-clause-list; Candidate ~S OK wrt sub-queries" candidate)
    t))

;;;---------------------------------------------------- COLLECT-CHECK-SUBQUERY-LIST
;;;
;;; Subqueries can contain co-reference variables to be checked against the context, 
;;; e.g.
;;;   (person (has-brother (person (has-first-name is ?x)))
;;;           (has-cousin (person (has-first-name is ?x))))
;;; the first subquery sets ?x, the second checks it.

(defUn collect-check-subquery-list (candidate subqueries bindings)
  "Function to deal with subqueries. 
Arguments:
   candidate: object to be tested
   subqueries: a list of subqueries, e.g. (($S-PERSON-BROTHER (> 1) ($E-PERSON)))
   bindings: already assigned coreference variables
Return:
   nil on failure, t otherwise."
  (drformat :query 2 "~2%;---------- Entering collect-check-subquery-list")
  (dformat :query 2 "candidate: ~S ~S" (>>f candidate)(send candidate '=summary))
  (dformat :query 2 "subqueries: ~S" subqueries)
  (dformat :query 2 "bindings: ~S" bindings)
  (let (candidate-list tag)
    (dolist (clause subqueries)
      ;; checks the clause, without updating the bindings
      ;; get the list of candidates (accessed from the property)
      ;(setq candidate-list (send candidate '=get-id (car clause)))
      (multiple-value-setq (candidate-list tag)
        (send candidate '=get-id (car clause)))
      (dformat :query 2 "neighbors (new candidate-list: ~S ~{~S~^, ~}" 
               (>>f candidate-list)(broadcast candidate-list '=summary))
      ;; if there is no result but tag is non nil, then we know there is no
      ;; neighbor. If the test is (= 0), then, this is a success
      (cond
       ;; if the result is a certified 0 value and the test is (= 0), success
       ;; for this subquery. We do nothing
       ((and (null candidate-list) tag)
        (unless (apply-cardinality-test (cadr clause) 0)
          (drformat :query 2 "~%;---------- Exit collect-check-subquery-list")
          (return-from collect-check-subquery-list nil)))
       ;; if the result is that we get null result but do not know the value
       ;; then we cannot apply the test
       ((and (null candidate-list) (null tag)) 
        (drformat :query 2 "~%;---------- Exit collect-check-subquery-list")
        (return-from collect-check-subquery-list nil))
       ;; otherwise we must test the values that we got
       (t
        ;;***** if (car clause) is an inverse link and relation has an =if-needed 
        ;; method, then some inverse-links may not exist... 
        ;; We must do the following:
        ;;  1. go to the target class (caaddr clause)
        ;;  2. do a find-subset on that class
        ;;  3. apply the =if-needed demon to whatever is returned
        ;;  4. collect the objects on the other side connected to candidate
        ;;  5. go check the rest of the query (because find-subset is not strict)
        ;;*****
        (when (and (%is-inverse-property? (car clause))
                 (get-method (%inverse-property-id (car clause)) '=if-needed))
            (setq candidate-list
                  (delete-duplicates
                   (append
                    (collect-check-subquery-resolve-ghost-links candidate clause)
                    candidate-list))))
        
        ;; call the function that filters candidates
        (multiple-value-bind 
              (succeeding-candidates success) 
            (filter-candidates
             candidate-list   ; objects to examine
             (caddr clause)   ; subquery
             (cadr clause)    ; cardinality test attached to the relation
             bindings         ; current bindings
             )
          (declare (ignore succeeding-candidates))
          (unless success 
            (drformat :query 2 "~%;---------- Exit collect-check-subquery-list")
            (return-from collect-check-subquery-list nil))))))
    ;(vformat "collect-check-subquery-list; Candidate ~S OK wrt sub-queries" candidate)
    ;; returning t means that the object passed the subquery constraint
    t))

#|
(moss::collect-check-subquery-list 
   '$E-PERSON.1
   '(($S-PERSON-DAUGHTER (> 0) ($E-STUDENT)))
   ()
   )
T

(moss::collect-check-subquery-list 
   '$E-PERSON.1
   '(($S-PERSON-DAUGHTER (= 0) ($E-STUDENT)))
   ()
 )
NIL

;;; here we know that $E-STUDENT.1 has no sister
(moss::collect-check-subquery-list 
   '$E-STUDENT.1
   '(($S-PERSON-SISTER (= 0) ($E-PERSON)))
   ()
 )
T

(moss::collect-check-subquery-list 
   '$E-STUDENT.1
   '(($S-PERSON-SISTER (> 0) ($E-PERSON)))
   ()
 )
NIL

;;; here we do not know if $E-PERSON.11 has a sister or not, so we cannot apply the
;;; cardinality test, thus, we rule out the candidate
(moss::collect-check-subquery-list 
   '$E-PERSON.11
   '(($S-PERSON-SISTER (> 0) ($E-PERSON)))
   ()
 )
NIL

(moss::collect-check-subquery-list 
   '$E-PERSON.1
   '(($S-PERSON-DAUGHTER (> 0) ($E-STUDENT))
     ($S-PERSON-BROTHER (> 0) ($E-PERSON ($T-PERSON-FIRST-NAME :is "Pierre-Xavier"))))
   ()
   )
T

(moss::collect-check-subquery-list  
   '$E-PERSON.1
   '(($S-PERSON-DAUGHTER (> 0) ($E-STUDENT))
     ($S-PERSON-BROTHER (> 0) ($E-PERSON ($T-PERSON-FIRST-NAME :is ?x))))
   '((?x "Pierre-Xavier")(?y "George"))
   )
T

(moss::collect-check-subquery-list  
   '$E-PERSON.1
   '(($S-PERSON-DAUGHTER (> 0) ($E-STUDENT))
     ($S-PERSON-BROTHER (> 0) ($E-PERSON ($T-PERSON-FIRST-NAME :is ?x)
                                         ($T-PERSON-AGE > ?z))))
   '((?x "Pierre-Xavier")(?y "George"))
 )
T
*** The first occurence of a coreference variable should be an assignment, which is 
not the case:

This is to test the use of =if-needed demons the address being that of the
employer of Paraiso
? (moss::collect-check-subquery-list 
   '$E-OFFICE-ADDRESS.3 
   '(($S-PERSON-ADDRESS.OF (> 0) ($E-PERSON ($T-PERSON-NAME :IS "paraiso"))))
   nil)
T
|#
;;;-------------------------------------- COLLECT-CHECK-SUBQUERY-RESOLVE-GHOST-LINKS

(defun collect-check-subquery-resolve-ghost-links (candidate il-clause)
  "tries to resolve ghost links, i.e. links induced by =if-needed methods. Tries ~
   to find if a candidate object is linked to an instance of the class-id through ~
   executing an =if-needed method. If so returns all instances of the class-id ~
   that have such a virtual link. Instances of class id must be the restricted ~
   set obtained from the rest of the query.
Arguments:
   candidate: an oject id, e.g. $E-OFFICE-ADDRESS.3
   il-clause: e.g. ($S-PERSON-ADDRESS.OF (> 0) 
                     ($E-PERSON ($T-PERSON-NAME :IS \"paraiso\")))
Return:
   the list of instances of the neighboring class that have a link with candidate."
  (drformat :query 3 "~2%;---------- Entering collect-check-subquery-resolve-ghost-links")
  (let ((rel-id (%inverse-property-id (car il-clause)))
        (class-id (caaddr il-clause))
        (subquery (caddr il-clause))
        subset suc-list results)
    
    ;;  1. do a find-subset on the class
    (setq subset (find-subset subquery))
    (cond
     ;; if *NONE* return nil
     ((eql subset '*NONE*) 
      (return-from collect-check-subquery-resolve-ghost-links nil))
     ;; if nil must do a class access
     ((null subset)
      (setq subset (send class-id '=get-instances)))
     ;; otherwise do nothing
     )
      
    ;;  2. apply the =if-needed demon to whatever is returned
    (dolist (obj-id subset)
      ;; =get-id applies =if-needed
      (setq suc-list (send obj-id '=get-id rel-id))
      (if (member candidate suc-list)
          (push obj-id results)))
    
    (dformat :query 3 "results: ~S" (>>f results))
    ;; 3. return results
    (drformat :query 3 "~%;---------- Exit collect-check-subquery-resolve-ghost-links")
    results))

#|
Test in the context of ZE (milton's PA)
(moss::collect-check-subquery-resolve-ghost-links '$E-OFFICE-ADDRESS.3
    '($S-PERSON-ADDRESS.OF (> 0) ($E-PERSON ($T-PERSON-NAME :IS "paraiso"))))
($E-PERSON.17)
The address of Paraiso is actually the address of its employer.
|#
;;;-------------------------------------------------------------- COLLECT-FAIL-TEST?

(defUn collect-fail-test? (test good-list)
  "Checks if the good-list fails the test definitely.
    Complement of collect-continue-test?
   I.e., if test is = or < or <=, then we stop as soon as we have too many ~
   objects; if it is a BETWEEN test, we stop as soon as we have a violation."
  (case (car test)
    ;; all never fails
    (:all nil) 
    ;; failure if > 
    ((<= =)
     (> (length good-list) (cadr test)))
    ;; same for BETWEEN, check against upper bound
    (:BETWEEN (> (length good-list) (caddr test)))
    ;; we continue as long as >, failure when >=
    (< (>= (length good-list) (cadr test)))
    ;; otherwise we return a nil value (to continue search)
    (otherwise nil)))

#|
? (moss::collect-fail-test? '(< 2) '(A B))
T
? (moss::collect-fail-test? '(> 2) '(A B))
NIL
? (moss::collect-fail-test? '(:between 2 3) '(A B))
NIL
? (moss::collect-fail-test? '(:between 2 3) '(A B C D))
T
? (moss::collect-fail-test? '(:all) '(A B C D))
NIL
|#
;;;----------------------------------------------------------- COLLECT-SUCCESS-TEST?

(defUn collect-success-test? (test good-list)
  "Checks if the good-list satisfies the test definitely.
   I.e., if test is positive (> or >=) we access entities until we have the ~
   right number (it does not matter if we have more); if it is an OUTSIDE test, ~
   we stop as soon as we are over the upper limit; if it is a NOT EQUAL~
   test we have to access all objects; if it is an ALL test, we return all objects ~
   is something else (= < <= BETWEEN OUTSIDE), we must collect all objects."
  (case (car test)
    ;; if we want all values then it is not a success
    (:all nil)
    ;; test showing success:
    ;; on positive tests we stop as soon as condition is satisfied
    (> (> (length good-list)(cadr test)))
    (>= (>= (length good-list)(cadr test)))
    ;; when OUTSIDE, we stop as soon as we are over the upper limit
    (OUTSIDE (> (length good-list) (caddr test)))
    ;; when we have a NOT-EQUAL, then we stop as soon as we have more than 
    ;; specified 
    (<> (> (length good-list) (cadr test)))
    ;; otherwise return nil
    (otherwise nil)))

;;;--------------------------------------------------------------- FILTER-CANDIDATES
;;;
;;; Here we have a function that takes a list of objects, a cardinality test to be
;;; applied to the objects once they are filtered and a path from the node 
;;; representing the class of the objects, and bindings of coreferenced variables.
;;; The role of the function is to filter the objects, one at a time, checking
;;; that they obey the constraints represented by the path: e.g.
;;;    ($E-PERSON ($S-BROTHER (> 1) ($E-PERSON)))
;;; which means that we discard objects that have not at least 2 brothers.
;;;
;;; As objects are checked they are added to a good-list. Each time we add an object
;;; to the good-list we can check the cardinality conditions for failure (e.g. we
;;; want less than 2 cousind and have already found a second one, in which case
;;; it is not necessary to accexx more cousins.
;;; However, sometimes we want to access all objects that conform to the path.
;;; In that case we simply disallow the early success test and keep filtering all
;;; the objects.
;;;
;;; Now the question is: what do we return?
;;; Sometimes we want the list of objects (e.g. at toplevel), at other times we
;;; simply want to know if the candidates pass the cardinality test, in which case
;;; a simple T/nil answer is acceptable.
;;; Note that we cannot use the list of objects as an answer to this purpose, since
;;; the list may be empty, but the test (= 0) could be satisfied.
;;; The idea is to return 2 values: a T/NIL value and the list of objects.
;;;
;;; Bindings
;;; ========
;;; When we call the function, we may have already a set of instantiated coreference
;;; variables. They will be used to check the values. E.g.
;;;    ($E-PERSON ($S-COUSIN (> 1) ($E-PERSON ($T-HAS-NAME :is ?x))))
;;;   with bindings
;;;    ((?x "Labrousse")(?y "Barths"))
;;; If the path contains simple clauses with variables, then the value of bindings
;;; will be changed temporarily for checking the rest of the path.
;;; However, on return, the value of bindings is not modified and thus need not be
;;; returned. Indeed, we'll check the next object with the same initial bindings as
;;; as the previous one.

(defUn filter-candidates 
    (candidate-list query test bindings &key (early-success-allowed t) verbose)
  "Takes a list of candidates, a sub-query and checks if the candidates satisfy ~
   the query. The difference between this function and access is that we have ~
   an external test (usually (> 0), and possibly a context (bindings).
   Since we are considering candidates one by one, we can try to check for ~
   early success or early failure using the external test.
   In case of failure
Arguments:
   candidate-list: the list of objects to check
   query: the query
   test: cardinality test to be done on filtered candidates
   bindings: an initial binding
   early-success-allowed (key): allows the function to return as soon as a  
                 success is  detected (for subqueries with cardinal constraints)
   verbose (key): to print a star showing progress
Return:
   2 values:
    - the list of candidates (may be empty)
    - T or nil indicating if the test was successful (useful for subqueries)."
  (let (good-list filtering-items local-filter class-test result) 
    (drformat :query 0 "~2%;---------- Entering filter-candidates")
    (dformat :query 0 "Candidate-list: ~S" (>>f candidate-list))
    (dformat :query 0 "names: ~{~S~^, ~}" (broadcast candidate-list '=summary))
    (dformat :query 0 "query ~S" query)
    (dformat :query 0 "test ~S" (>>f test))
    (dformat :query 0 "bindings ~S" bindings)
    (dformat :query 0 "early-success-allowed: ~S" early-success-allowed)
    ;; get data from splitting query and preparing filters
    (setq filtering-items (split-query-and-compute-filters query test))
    ;; the function returns a list of several items, pick the filters
    (setq local-filter (nth 0 filtering-items)
        class-test (nth 1 filtering-items))
    
    ;; first reduce the list by applying local filter to each candidate
    (setq candidate-list (remove nil (mapcar local-filter candidate-list)))
    (dformat :query 0 "filtered candidate-list wrt class:~%   ~S; ~%   test: ~S"
             (>>f candidate-list) test)
    (dformat :query 0 "names: ~{~S~^, ~}" (broadcast candidate-list '=summary))
    
    (unless candidate-list
      ;; if we have no candidate left we return an empty list and the result of
      ;; applying the cardinality test to the list: e.g. t when (= 0) 
      (return-from filter-candidates 
        (values nil (and (funcall class-test good-list) t))))
    
    ;; here we have a good-list; check each candidate in turn
    ;; any object that passes the test is added to the good-list
    ;(dformat :query 0 "candidate-list: ~S" (>>f candidate-list))
    (dolist (candidate candidate-list) 
      (drformat :query 0 "~2%;-- testing next candidate: ~S ~S" (>>f candidate)
               (send candidate '=summary))
      ;; show user that we work
      (if verbose (format t "*"))
      ;; check whether the candidate is acceptable
      (setq result (validate2 candidate query bindings filtering-items))
      (dformat :query 0 "candidate was valid?: ~S" result)
      (when result
        (if verbose (format t "+"))
        ;; here we get one more candidate into the good-list
        (push candidate good-list)
        ;; time to do some checking with the cardinality test if allowed
        (cond
         ;; return if we are past a threshold
         ;; includes test for all values, i.e. (:all) never fails
         ((collect-fail-test? test good-list)
          (dformat :query 0 "good-list ~S failed the test ~S" good-list test)
          (dformat :query 0 "filter-candidates ; good-list:~& ~S" (reverse good-list))
          (return-from filter-candidates (values (reverse good-list) nil)))
         ;; test if we have a definite success. Don't stop if we want all solutions.
         ((and early-success-allowed
               ;; includes test for all values, i.e. (:all) always fails
               (collect-success-test? test good-list))
          (dformat :query 0 "early success: good-list ~S for test ~S"
                   good-list test)
          (dformat :query 0 "filter-candidates ; good-list:~& ~S" (reverse good-list))
          (return-from filter-candidates (values (reverse good-list) t)))
         )))
    
    ;; return the current list applying the cardinality test it
    (dformat :query 0 "filter-candidates ; good-list:~& ~S" (reverse good-list))
    (return-from filter-candidates 
      (values (reverse good-list) (and (funcall class-test good-list) t)))
    ))

#|
(with-package :f
  (moss::filter-candidates 
   (list f::_jpb f::_dbb f::_cxb f::_psb)
   '(f::$E-person)
   '(:ALL)
   ()
   ))
(($E-PERSON . 1) ($E-PERSON . 2) ($E-PERSON . 3) ($E-STUDENT . 1))
T

;;; here we test if we have less than 2 objects in the list
(with-package :f
 (moss::filter-candidates 
   (list f::_jpb f::_dbb f::_cxb f::_psb)
   '(f::$E-person)
   '(< 2)
   ()
   ))
((F::$E-PERSON . 1) (F::$E-PERSON . 2))
NIL ; indicates that test failed

;; same test but we count only students
;; we apply first the test on object class, then the test on cardinality
(with-package :f
  (moss::filter-candidates 
   (list f::_jpb f::_dbb f::_cxb f::_psb)
   '(f::$E-student)
   '(< 2)
   ()
   ))
($E-STUDENT.1)
T

;; here we have a limit of 2 objects and look for persons who have a brother
;; the first name clause is useless (not a constraint)
(with-package :f
 (moss::filter-candidates 
   (list f::_jpb f::_dbb f::_cxb f::_psb)
   '(f::$E-PERSON (f::$S-PERSON-BROTHER (> 0)(f::$E-PERSON))
     (f::$T-person-first-name :is ?x))
   '(< 3)
   ()
   ))
((F::$E-PERSON . 1) (F::$E-STUDENT . 1))
T
;; dbb and cxb have no brother

;; same test but no limit on the number of results
(with-package :f
 (moss::filter-candidates 
   (list f::_jpb f::_dbb f::_cxb f::_psb)
   '(f::$E-PERSON (f::$S-PERSON-BROTHER (> 0)(f::$E-PERSON))
     (f::$T-person-first-name :is ?x))
   '(:all)
   ()
   ))
((F::$E-PERSON . 1) (F::$E-STUDENT . 1))
T

;; same test, allowing early success; i.e. since we want at least 1 object, the
;; function returns as soon as we have one object
;; early success is useful for testing predicate, not for collecting objects
(with-package :f
  (moss::filter-candidates 
   (list f::_jpb f::_dbb f::_cxb f::_psb)
   '(f::$E-PERSON (f::$S-PERSON-BROTHER (> 0)(f::$E-PERSON))
     (f::$T-person-first-name :is ?x))
   '(> 0)
   ()
   :early-success-allowed t))
((F::$E-PERSON . 1))
T

;; when we do not allow early success, we collect all answers
(with-package :f
  (moss::filter-candidates 
   (list f::_jpb f::_dbb f::_cxb f::_psb)
   '(f::$E-PERSON (f::$S-PERSON-BROTHER (> 0)(f::$E-PERSON))
     (f::$T-person-first-name :is ?x))
   '(> 0)
   ()
   :early-success-allowed nil))
((F::$E-PERSON . 1) (F::$E-STUDENT . 1))
T
|#
;;;------------------------------------------------------------------ FILTER-OBJECTS

(defUn filter-objects (candidate-list query &key verbose)
  "Top level filter of a list of candidates against a specific query. Calls ~
   filter-candidates.
Arguments:
   candidate-list: a list of objects
   query: a formal unparsed query
   verbose (key): a flag for printing info
Return:
   a possibly empty list of objects satisfying the query."
  (let (parsed-query)
    ;; first parse the query
    (setq parsed-query (parse-user-query query))
    ;; if parsed query is nil then quit (bad syntax)
    (unless parsed-query 
      (warn "Bad syntax for query: ~S" query)
      (return-from filter-objects nil))
    ;; otherwise filter candidates
    (filter-candidates candidate-list parsed-query '(:all) nil 
                       :early-success-allowed nil :verbose verbose)))

;;;-------------------------------------------------- MAKE-ALL-POSSIBLE-ENTRY-POINTS

(defUn make-all-possible-entry-points (string-arg)
  "collects all =make-entry functions of the system and applies them to the string ~
   argument to produce a list of possible entry-point symbols"
  (let (method-list fn-list lres)
    ;; collect all =make-entry methods
    (setq method-list 
          (%get-value '=make-entry (%inverse-property-id '$MNAM)))
    ;; remove entries not from the current package or the MOSS package
    (setq method-list 
          (delete-duplicates
           (append
            (%filter-against-package method-list *package*)
            (%filter-against-package method-list (find-package :moss)))))
    ;; get the list of =make-entry functions
    (setq fn-list 
          (delete-duplicates
           (mapcar #'(lambda (xx) (car (%get-value xx '$FNAM)))
             method-list)))
    ;(format t "~% make-all-possible-entry-points/ method-list:~%  ~S" method-list)
    ;; apply each function to the argument
    (dolist (fn fn-list)
      (push (car (funcall fn string-arg)) lres))
    ;; remove entries that are identical
    (setq lres (delete-duplicates lres))
    ;;return cleaned result
    lres))

#|
(with-package :family (moss::make-all-possible-entry-points "barthès"))
(HAS-BARTHÈS BARTHÈS)

(MOSS::MAKE-ALL-POSSIBLE-ENTRY-POINTS " barthès biesel")
(HAS-BARTHÈS-BIESEL >-BARTHÈS-BIESEL BARTHÈS-BIESEL)

(MOSS::MAKE-ALL-POSSIBLE-ENTRY-POINTS " person name")
(HAS-PERSON-NAME >-PERSON-NAME PERSON-NAME)
|#
;;;-------------------------------------------------------------- MAKE-CLAUSE-FILTER

(defUn make-clause-filter (prop-id opr ref-val &optional (mode 'every))
  "builds a filter for a particular attribute clause with numeric values.
  The filter checks that :
   - we obtain a value for the property
   - all the values we obtain satisfy the operator when checked agaisnt ~
   the reference value.
Arguments:
   prop-id: attribute id
   opr: one of the legal numeric operators
   ref-val: a single numeric reference
   mode: either 'some or 'every (default is 'every)
Returns:
   a lambda expr to be use to build the final filter."
  (declare (special *numeric-operators*))  
  ;; mode is some or any and tests aver a list of values
  `(,mode #'(lambda (yy) (,opr (cv2nb yy) ',ref-val))
          (=> xx '=get-id ',prop-Id)))

;;;(defUn make-clause-filter (prop-id opr ref-val &optional (mode 'every))
;;;  "builds a filter for a particular attribute clause. The filter checks that :
;;;   - we obtain a value for the property
;;;   - all the values we obtain satisfy the operator when checked agaisnt ~
;;;   the reference value.
;;;Arguments:
;;;   prop-id: attribute id
;;;   opr: one of the legal numeric operators
;;;   ref-val: a single numeric reference
;;;Returns:
;;;   a lambda expr to be use to build the final filter."
;;;  (declare (special *numeric-operators*))  
;;;  `((lambda (zz)
;;;      (and zz
;;;           (,mode #'(lambda (yy)
;;;                      (,opr (convert-data-to-number yy) ',ref-val))
;;;                  zz)))
;;;    (=> xx '=get-id (quote ,prop-Id))))

#|
(make-clause-filter '$T-YEAR '<= 2002)
(EVERY #'(LAMBDA (YY) (<= (CV2NB YY) '2002)) (=> XX '=GET-ID '$T-YEAR))

(make-clause-filter '$T-YEAR :LTE 2002)
(EVERY #'(LAMBDA (YY) (:LTE (CV2NB YY) '2002)) (=> XX '=GET-ID '$T-YEAR))

(make-clause-filter '$T-YEAR :EQUAL 2002 'some)
(SOME #'(LAMBDA (YY) (:EQUAL (CV2NB YY) '2002)) (=> XX '=GET-ID '$T-YEAR))
;; here only some value needs to be equal

(make-clause-filter '$T-YEAR :between '(2002 2004))
(EVERY #'(LAMBDA (YY) (BETWEEN (CV2NB YY) '(2002 2004))) (=> XX '=GET-ID '$T-YEAR))
;; here all values need to be in the specified range
|#
;;;-------------------------------------------------------- MAKE-CLAUSE-FILTER-CARD=

(defun make-clause-filter-card (prop-id opr ref-val)
  "builds a special filter to deal with the case when we have a cardinality ~
   test. If the property in the target object cannot be found, then we do not ~
   know the number of values associated with this property, and the test fails.
Arguments:
   prop-id: attribute id
   opr: cardinality oprator
   ref-val: a number
Return:
   a lambda expression to be used to build the final filter."
  `(multiple-value-bind (val tag) (=> xx '=get-id ',prop-id)
     ;; OK if property values have been found (tag non nil), then we can compare
     (if tag (,opr (length val) ,ref-val))))
           
#|
(moss::make-clause-filter-card '$T-FIRST-NAME '<= 0)
(MULTIPLE-VALUE-BIND (VAL TAG) (=> XX '=GET-ID '$T-FIRST-NAME) 
  (IF TAG (<= (LENGTH VAL) 0)))

(make-clause-filter-card '$T-FIRST-NAME '= 3)
(MULTIPLE-VALUE-BIND (VAL TAG) (=> XX '=GET-ID '$T-FIRST-NAME) 
  (IF TAG (= (LENGTH VAL) 3)))
|#
;;;-------------------------------------------------------------- MAKE-LOCAL-FILTER

(defUn make-local-filter (query)
  "Takes a (sub-)query and does several things:
   - splits the query into 4 parts
        - one that can be used to filter objects locally
        - a list of attribute clauses containing variables
        - a list containing the rest of the query
        - the class variable if any
    - builds an immediate filter for a particular step of a given query - 
      We first include a test on the type of object we want to keep, ~
   unless we have *any* as a model. 
      Then, we include all clauses that do not contain a variable or a sub-query. ~
   Several cases: 
        - 1 filter clause / several filter clauses 
        - single value / multiple value for the property of the object to test. 
      We build our filter accordingly
Arguments:
   query: a (sub-)query
Return:
   a list containing:
    - the filter as a lambda expression
    - the list of the clauses with variables
    - the rest of the query
    - the name of the class variable if any."
  (let (filter filter-info class-sub-filter)
    (when query
      ;; the special query "(*any*)" returns a null filter.
      (if (and (null (cdr query)) (eql '*any* (car query)))
        (return-from make-local-filter 
          (list `(lambda (xx) xx) nil nil)))
      
      ;; check whether we have not cached this filter before
      (setq filter-info 
            (cdr (assoc (cons *query-allow-sub-classes* query) 
                        *filters* :test #'equal)))
      ;; qfilter prints info and waits until the user types a char, to continue
      (when filter-info
        ;(dwformat :query 1 
        (dformat :query 1 
                  "make-local-filter; recovering cached filter for query ~&  ~S"
                  query)
        (return-from make-local-filter filter-info))
      
      ;; otherwise, we must build the filter
      ;; we build it piece by piece to be assembled as a lambda expr later
      ;; prepare a piece of filter to be used later (for checking class)
      ;; e.g. ((INTERSECTION '($E-PERSON $E-STUDENT) (MOSS::%TYPE-OF XX)))
      (setq class-sub-filter 
            (if (eql '*any* (car query))
              nil
              (if *query-allow-sub-classes*
                `((intersection ',(%sp-gamma (car query)
                                             (%inverse-property-id '$IS-A))
                                (%type-of xx)))
                `((member ',(car query) (%type-of xx))))))
      
      ;; gather first all simple clauses and OR-simple-clauses that do not have
      ;; a variable
      (multiple-value-bind 
        (simple-clauses simple-clauses-with-variables or-clauses sub-queries variable)
        (split-query query)
        ;(dwformat :query 1 
        (dformat :query 1 
                  "~%;=== make-local-filter /simple-clause-list: ~S" simple-clauses)
        (setq filter `(lambda (xx)
                        (and
                         ,@class-sub-filter
                         ,@(make-multiple-filter simple-clauses)
                         xx)))
        ;; show filter, save it on p-list and return it
        ;;;        (pushnew (cons (cons *query-allow-sub-classes* query)
        ;;;                       (list filter simple-clauses-with-variables
        ;;;                             or-clauses sub-queries variable)) 
        ;;;                 *filters* :test #'equal)
        ;;;        (qformat "make-local-filter; saving on the filter list:~& ~S" filter)
        (list filter simple-clauses-with-variables
              or-clauses sub-queries variable)))))

#|
(MOSS::make-local-filter  
   '($E-PERSON
     ($T-PERSON-NAME :IS ?X)
     ($T-PERSON-FIRST-NAME :is "dominique")
     ($T-PERSON-AGE = 22)
     (OR
      ($S-PERSON-BROTHER (> 0) ($E-PERSON))
      ($S-PERSON-SISTER (< 1) ($E-PERSON)))
     ($S-PERSON-COUSIN (> 1) ($E-PERSON))))
((LAMBDA (MOSS::XX)
   (AND (INTERSECTION '($E-PERSON $E-STUDENT) (MOSS::%TYPE-OF MOSS::XX))
        (INTERSECTION '("DOMINIQUE")
                      (MOSS::NORMALIZE-VALUE 
                       (MOSS::=> MOSS::XX '=GET-ID '$T-PERSON-FIRST-NAME))
                       :TEST #'EQUAL+)
        (SOME #'(LAMBDA (MOSS::YY) (= (MOSS::CV2NB MOSS::YY) '22))
              (MOSS::=> MOSS::XX '=GET-ID '$T-PERSON-AGE))
        MOSS::XX))
 (($T-PERSON-NAME :IS ?X))
 ((OR ($S-PERSON-BROTHER (> 0) ($E-PERSON)) ($S-PERSON-SISTER (< 1) ($E-PERSON))))
 (($S-PERSON-COUSIN (> 1) ($E-PERSON)))
 NIL)
|#
;;;-------------------------------------------------------- MAKE-LOCAL-FILTER-CLAUSE

;;;(defParameter *legal-attribute-operators*
;;;  ;; operators are in English and are keywords
;;;  '(:LESS-THAN :LT :LESS-THAN-OR-EQUAL :LTE
;;;    :GREATER-THAN :GT :GREATER-THAN-OR-EQUAL :GTE
;;;    :EQUAL :NOT-EQUAL :IS :IS-NOT
;;;    ;; range operators
;;;    :BETWEEN :OUTSIDE 
;;;    :IN :ALL-IN :NOT-IN
;;;    ;; structural operators related to cardinality
;;;    :CARD< :CARD<= :CARD= :CARD<> :CARD>= :CARD>))

;;; the local attribute tests have the following semantics:
;;; the object values under test are retrieved as a list of 0, 1 or more values
;;; if the reference value, ref-val, is unique and is a number, then
;;;  - the = test is true if one if the values is equal to ref-val
;;;  - the <> test is true if non of the values is equaal to ref-val
;;;  - the >, <, >=, <=, :BETWEEN tests are true if all values conform to the test
;;;  - the cardinality tests can be done only if one knows the number of values
;;;    in the target object. If the property is not present then the number of
;;;    values is unknown and the test fails.

(defUn make-local-filter-clause (val prop-id opr)
  "Build a piece of code to insert in the filtering function - We assume ~
   that all properties can be multiple values, even if they are declared ~
   single-valued, since the user can add a value in all cases.
    We check values against a single reference value, that can ~
   be a list of 2 values (e.g. for the between and outside operators).
    If the operator is a numeric operator values are processed to ~
   numeric values. Otherwise, values are transformed into ~
   normalized strings.
Arguments:
   val: reference value(s)
   prop-id: attribute id
   opr: one of the legal attribute operators
Return:
   a lambda expr to be used as a filter."
  (let (ref-val filter)
    ;; val is normalized unless it is a structure containing only numbers
    ;;===== first process numeric operators, i.e. operators requiring numeric values
    (when (member opr *numeric-operators*)
      ;; convert first reference value to number if needed
      (setq ref-val (convert-data-to-number val))
      (setq filter
            (case opr
              ;; equality, true if some of the value is equal to the reference
              ((:equal =)
               (make-clause-filter prop-id '= ref-val 'some))
              ;; must be different numerical values (no common values)
              ((:NOT-EQUAL <>)  
               (make-clause-filter prop-id '<> ref-val))
              ;; all values must not be < than the values of the reference
              ((:LT :LESS-THAN <)  
               (make-clause-filter prop-id '< ref-val))
              ;; all values must not be <= than the reference
              ((:LTE :LESS-THAN-OR-EQUAL <=)
               (make-clause-filter prop-id '<= ref-val))              
              ;; all values must not be > than the reference
              ((:GT :GREATER-THAN >)  
               (make-clause-filter prop-id '> ref-val))
              ;; all values must not be > than the reference
              ((:GTE :GREATER-THAN-OR-EQUAL >=)  
               (make-clause-filter prop-id '>= ref-val))
              ;; all values must not be outside the values of the ref-list
              (:BETWEEN  
               ;; we assume that ref-val is a range value
               (make-clause-filter prop-id 'between ref-val))
              ;; all values must not BETWEEN the values of the ref-list
              (:OUTSIDE  
               (make-clause-filter prop-id 'outside-strict ref-val))
              
              ;;=== test on the number of values
              ;; when the length of the object value list is 0, we do not know if
              ;; the obje
              (:CARD< 
               `(< (length (=> xx '=get-id ',prop-id)) ',ref-val))
              (:CARD<= 
               `(<= (length (=> xx '=get-id ',prop-id)) ',ref-val))
              ;; :CARD= requires a special treatment because of null values
              (:CARD= 
               (make-clause-filter-card prop-id '= ref-val))
              ;`(eql (length (=> xx '=get-id ',prop-id)) ',ref-val))
              (:CARD>= 
               `(>= (length (=> xx '=get-id ',prop-id)) ',ref-val))
              (:CARD> 
               `(> (length (=> xx '=get-id ',prop-id)) ',ref-val))
              )))
    
    ;;===== case where we have non numeric comparisons
    (unless (member opr *numeric-operators*)
      ;; special case of MLN values
      ;; if val is MLN with old format upgrade it to newer format
      (if (mln::%mln? val) (setq val (mln::make-mln val)))
      ;; normalize-value also works for MLNs
      (setq ref-val (normalize-value val))
      ;; we need ref-val to be a list because of the intersection
      (when (or (not (listp ref-val)) (mln::mln? ref-val))
        (setq ref-val (list ref-val)))
      ;(error "make-local-filter-clause val: ~S, should be a list" ref-val))
      (setq filter
            (case opr
              ;; some values must be in the ref list
              (:IN
               (unless (listp ref-val) 
                 (error "reference-value: ~S is not a list in: ~&~S"
                   ref-val (list prop-id opr val)))
               `(intersection 
                 ',ref-val 
                 ;; we must normalize each item of the list of values
                 (mapcar #'normalize-value (=> xx '=get-id (quote ,prop-Id)))
                 :test #'equal+))
              
              ;; no value must be in the ref list
              (:NOT-IN
               `(not (intersection 
                      ',ref-val 
                      (mapcar #'normalize-value (=> xx '=get-id (quote ,prop-Id)))
                      :test #'equal+)))
              
              ;; all values of the ref list must be in the data
              (:ALL-IN  
               (unless (listp ref-val) 
                 (error "reference-value: ~S is not a list in: ~&~S"
                   ref-val (list prop-id opr val)))
               `(every #'(lambda (yy) (member yy ',ref-val :test #'equal+)) 
                       (mapcar #'normalize-value (=> xx '=get-id (quote ,prop-Id))))
               )
              
              ;; here, result is true if some value of ref-val is in the list of 
              ;; values, which accounts for multivalues properties
              ;; if ref-val is a list of an MLN, the semantics is that of a OR, i.e.
              ;; the test is true if the object has one of the attribute values
              (:IS
               `(intersection
                 ',ref-val
                 (mapcar #'normalize-value (=> xx '=get-id (quote ,prop-Id)))
                 :test #'equal+))
              
              ;; all values must be different
              (:IS-NOT
               `(not (intersection 
                      ',ref-val
                      (mapcar #'normalize-value (=> xx '=get-id (quote ,prop-Id)))
                      :test #'equal+)))
              
              (t (warn "illegal operator: ~S~& when trying to build attribute ~
                        filter for property: ~S" opr prop-id)
                 nil))))
    filter))

#|
(make-local-filter-clause "2005" '$T-YEAR :equal)
(SOME #'(LAMBDA (YY) (= (CV2NB YY) '2005)) (=> XX '=GET-ID '$T-YEAR))

(make-local-filter-clause "2005" '$T-YEAR :lte)
(EVERY #'(LAMBDA (YY) (<= (CV2NB YY) '2005)) (=> XX '=GET-ID '$T-YEAR))

(make-local-filter-clause '("2002" "2004") '$T-YEAR :between)
(EVERY #'(LAMBDA (YY) (BETWEEN (CV2NB YY) '(2002 2004))) (=> XX '=GET-ID '$T-YEAR))

(make-local-filter-clause 2 '$T-YEAR :CARD>=)
(>= (LENGTH (=> XX '=GET-ID '$T-YEAR)) '2)

(make-local-filter-clause '("2002" "2005") '$T-YEAR :in)
(INTERSECTION '("2002" "2005") 
              (MAPCAR #'NORMALIZE-VALUE (=> XX '=GET-ID '$T-YEAR)) 
              :TEST #'EQUAL+)

(make-local-filter-clause "Barthès" '$T-PERSON-NAME :is)
(INTERSECTION '("BARTHÈS") 
              (MAPCAR #'NORMALIZE-VALUE (=> XX '=GET-ID '$T-PERSON-NAME))
              :TEST #'EQUAL+)

;; non numerical tests
(make-local-filter-clause '(:FR "Albert" :EN "John;George") '$t-person-NAME :is)
(INTERSECTION '(((:FR "ALBERT") (:EN "JOHN" "GEORGE")))
              (MAPCAR #'NORMALIZE-VALUE (=> XX '=GET-ID '$T-PERSON-NAME))
              :TEST #'EQUAL+)

(make-local-filter-clause '(:FR "Albert" :EN "John;George") '$t-person-NAME :not-in)
(NOT (INTERSECTION '(((:FR "ALBERT") (:EN "JOHN" "GEORGE")))
                   (MAPCAR #'NORMALIZE-VALUE (=> XX '=GET-ID '$T-PERSON-NAME))
                   :TEST #'EQUAL+))
|#
;;;------------------------------------------ MAKE-LOCAL-FILTER-FOR-MULTIPLE-VALUES
;;; this function is only called when there are some variable bindings of
;;; co-reference variables. Should be checked in that case
;;;**********
;;; Because val-list is the value extracted from the bindings, it comes from a 
;;; previous assignment that could have multiple values, even sone strange things
;;; like ($T-PERSON-lucky-number (0 7 13)). Now this will lead to a binding like
;;; (?X 7 13). Then, if we encounter a query clause like ($T-PERSON-LUCKY-NUMBER
;;; > 10), then the semantics would be to find a value from the list that is indeed 
;;; greater than 10. Same for < = :between, but not for <> or :outside for which 
;;; all values should pass the test.
;;; Cardinality constraints cannot be assigned to a coreference variable (excluded
;;; by the syntax) although it could make sense: "find persons who have the same
;;; number of names". First encountered  clause should be ($T-ATT1 :CARD= ?X)
;;; test on the second value could be ($T-ATT1 :CARD= ?X), which could be somewhat
;;; tricky. Furthermore it cannot be applied easily to relations. Hence, we discard
;;; cardinality constraint from being included in coreference variables as a
;;; responsibility of the programmer.
;;; Nevertheless, it could be implemented, with the semantics to be made clear

(defUn make-local-filter-for-multiple-values (val-list prop-id opr)
  "similar to make-local-filter-clause, except that the reference value is ~
   a list of values rather than a single value, coming from a bindings.
   Cardinality operators are excluded to appear in a variable clause.
   We use othewise the make-local-filter function.
Arguments:
   val-list: list of values, e.g. (\"Barthès\" \"Biesel\") or (23) is always a list
   prop-id: attribute id, e.g. $T-PERSON-NAME
   opr: one of the legal attribute operators in the variable clause
Return:
   a piece of code to be included into a lambda expr to implement the test."
  (declare (special *cardinality-operators*))
  (cond
   ;; if reference not a list quit (safety feature, since value associated with a
   ;; binding should always be a non empty list)
   ((not (listp val-list))
    (error "value-list should be a list in: ~&~S"
      (list prop-id opr val-list)))
   ;; it is illegal to have a cardinality operator with a coreference variable
   ((member opr *cardinality-operators*)
    (error "cardinality operator is illegal with coreference varaibles in: ~&~S"
      (list prop-id opr val-list)))
   ;; if reference is a list of one element, use existing function
   ((null (cdr val-list)) 
    (return-from make-local-filter-for-multiple-values
      (make-local-filter-clause (car val-list) prop-id opr)))
   ;; if operator in numeric
   (t 
    (return-from make-local-filter-for-multiple-values
      (make-local-filter-clause val-list prop-id opr)))
   ))

;;;(defUn make-local-filter-for-multiple-values (val-list prop-id opr)
;;;  "similar to make-local-filter-clause, except that the reference value is ~
;;;   a list of values rather than a single value.
;;;    We use the intersection function for checking the result of applying the ~
;;;   operator to combinations of elements of the 2 lists. Intersection returns ~
;;;   a NIL answer only if the test fails everywhere. Thus :
;;;          We use the intersection function for checking the result of applying the ~
;;;   operator to combinations of elements of the 2 lists. Intersection returns ~
;;;   a NIL answer only if the test fails everywhere. Thus :
;;;      (intersection '(1 2 3) '(4 5 2) #'=) returns (2)
;;; returns (2)
;;;   All operators involving numbers assume a single number structure.
;;;Arguments:
;;;   val-list: list of values, e.g. (\"Barthès\" \"Biesel\") or (23) is always a list
;;;   prop-id: attribute identifier, e.g. $T-PERSON-NAME
;;;   opr: one of the legal attribute operators in the clause
;;;Return:
;;;   a lambda expr, implementing the test."
;;;  (cond
;;;   ;; if reference not a list quit (safety feature, since value associated with a
;;;   ;; binding should always be a non empty list)
;;;   ((not (listp val-list)) 
;;;    (return-from make-local-filter-for-multiple-values nil))
;;;   ;; if reference is a list of one element, use existing function
;;;   ((null (cdr val-list)) 
;;;    (return-from make-local-filter-for-multiple-values
;;;      (make-local-filter-clause (car val-list) prop-id opr))))
;;;  
;;;  (let (ref-val filter)
;;;    (when (member opr *numeric-operators*)
;;;      ;; convert first reference value to number if needed
;;;      (setq ref-val (convert-data-to-number val-list))
;;;      ;; semantics is undefined for numeric operators other than = and <>
;;;      ;; e.g. ($T-AGE <= 22 34) does not mean anything
;;;      ;; same remark for cardinality ($T-AGE :CARD> 2 4)
;;;      (setq filter
;;;            (case opr
;;;              ;; semantics is undefined for numeric operators other than = and <>
;;;              ;; e.g. ($T-AGE <= 22 34) does not mean anything
;;;              ;; same remark for cardinality ($T-AGE :CARD> 2 4)
;;;              ((:LT :LESS-THAN :LESS-THAN-OR-EQUAL :LTE
;;;                    :GREATER-THAN :GT :GREATER-THAN-OR-EQUAL :GTE
;;;                    :BETWEEN :OUTSIDE 
;;;                    :IN :ALL-IN :NOT-IN
;;;                    ;; structural operators related to cardinality
;;;                    :CARD< :CARD<= :CARD= :CARD<> :CARD>= :CARD>)
;;;               ;;==========
;;;               ;; since we are using the function with coreference variables, and
;;;               ;; those can only be set in assignments, occurences of this operator
;;;               ;; should not happen if assignments are done on single-valued 
;;;               ;; attributes. It could happen though:
;;;               ;;    ("person" ("age" :is ?x)  ("wife" ("person" :GT ?x)))
;;;               ;; and a person has more than one age recorded....
;;;               ;; The solution is to issue a warning and use the first value (?)
;;;               ;;==========
;;;               (error "attribute operator has too many reference values in: ~&~S"
;;;                      (list prop-id opr val-list)))
;;;              
;;;              ;; here, result is true if some value is in the ref-list
;;;              ((:equal =) 
;;;               (make-clause-filter prop-id 'member ref-val 'some))
;;;              
;;;              ;; here true if no value is in the ref-list 
;;;              ((:NOT-EQUAL <>)
;;;               `((lambda(zz) 
;;;                   (and zz
;;;                        (not (intersection 
;;;                              ',(list ref-val)  
;;;                              (mapcar #'convert-data-to-number 
;;;                                      (=> xx '=get-id (quote ,prop-Id)))
;;;                              :test #'equal)))))))))
;;;    
;;;    ;; case where we have non numeric comparisons
;;;    (unless (member opr *numeric-operators*)
;;;      ;; special case of MLN values. Numbers are turned into strings
;;;      (setq ref-val (normalize-value val-list))
;;;      (setq filter
;;;            (case opr
;;;              ;;OK if some values are in the reference list
;;;              (:IS 
;;;               `((lambda(zz) 
;;;                   (and zz
;;;                        (intersection 
;;;                         ',ref-val (mapcar #'normalize-value zz)
;;;                         :test #'equal)))
;;;                 (=> xx '=get-id (quote ,prop-Id))))
;;;              
;;;              ;; all values must be different
;;;              (:IS-NOT  
;;;               `((lambda(zz) 
;;;                   (and zz
;;;                        (not (intersection ',ref-val  
;;;                                           (mapcar #'normalize-value zz)
;;;                                           :test #'equal))))
;;;                 (=> xx '=get-id (quote ,prop-Id))))
;;;              )))
;;;    filter))

#|
(moss::make-local-filter-for-multiple-values
   '("barthès" "biesel")
   '$T-PERSON-NAME
   :is)
(INTERSECTION 
 '("BARTHÈS" "BIESEL")
 (MOSS::NORMALIZE-VALUE (MOSS::=> MOSS::XX '=GET-ID '$T-PERSON-NAME))
 :TEST #'EQUAL+)

(moss::make-local-filter-for-multiple-values
 '("barthès" "biesel")
 '$T-PERSON-NAME
 :is-not)
(NOT (INTERSECTION '("BARTHÈS" "BIESEL") (NORMALIZE-VALUE (=> XX '=GET-ID '$T-PERSON-NAME))
                   :TEST #'EQUAL+))

(moss::make-local-filter-for-multiple-values
 '(22)
 '$T-PERSON-AGE
 :equal)
(SOME #'(LAMBDA (YY) (= (CV2NB YY) '22)) (=> XX '=GET-ID '$T-PERSON-AGE))

(moss::make-local-filter-for-multiple-values
 '22
 '$T-PERSON-AGE
 :equal)
Error: value-list should be a list in: 
($T-PERSON-AGE :EQUAL 22)

(moss::make-local-filter-for-multiple-values
 '(22 25)
 '$T-PERSON-AGE
 :between)
(EVERY #'(LAMBDA (YY) (BETWEEN (CV2NB YY) '(22 25))) 
       (=> XX '=GET-ID '$T-PERSON-AGE))
;;********** should be SOME (?)

(moss::make-local-filter-for-multiple-values
   '(("barthès" "biesel"))
   '$T-PERSON-NAME
   :all-in)
(EVERY #'(LAMBDA (YY) (MEMBER YY '("BARTHÈS" "BIESEL") :TEST #'EQUAL+))
       (NORMALIZE-VALUE (=> XX '=GET-ID '$T-PERSON-NAME)))

(moss::make-local-filter-for-multiple-values
   '(2)
   '$T-PERSON-NAME
   :card<)
Error: cardinality operator is illegal with coreference varaibles in: 
($T-PERSON-NAME :CARD< (2))
|#
;;;------------------------------------------------------------ MAKE-MULTIPLE-FILTER

(defUn make-multiple-filter (ll)
  "Build a piece of code for a filter function on properties"
  (if ll   ; must contain something
    (cond
     ;; we may have a sub clause starting with OR
     ((eq (caar ll) 'OR)
      (cons (cons 'or (make-multiple-filter (cdar ll)))
            (make-multiple-filter (cdr ll))))
     ;; for a terminal property, just build the clauses
     ((%is-terminal-property? (caar ll))
      (cons (make-local-filter-clause  (caddar ll) ; value
                                       (caar ll)   ; property id
                                       (cadar ll)) ; operator
            (make-multiple-filter (cdr ll))
            ))
     )))

#|
(moss::make-multiple-filter 
     '(($T-PERSON-NAME :is-not "Labrousse")
       ($T-AGE <= 23)))
((NOT (INTERSECTION
       '("LABROUSSE")
       (MOSS::NORMALIZE-VALUE (MOSS::=> MOSS::XX '=GET-ID '$T-PERSON-NAME))
        :TEST #'EQUAL+))
 (EVERY #'(LAMBDA (MOSS::YY) (<= (MOSS::CV2NB MOSS::YY) '23))
        (MOSS::=> MOSS::XX '=GET-ID '$T-AGE)))

(moss::make-multiple-filter 
     '((OR ($T-PERSON-NAME :is-not "Labrousse")
       ($T-AGE <= 23))))
((OR (NOT (INTERSECTION 
           '("LABROUSSE") 
           (MOSS::NORMALIZE-VALUE #) 
           :TEST #'EQUAL+))
     (EVERY #'(LAMBDA (MOSS::YY) (<= # '23)) 
            (MOSS::=> MOSS::XX '=GET-ID '$T-AGE))))
|#
;;;----------------------------------------------------------------------- MAKE-TEST

(defUn make-test (expr)
  "Builds a test for a single object with the query cardinality operators
   e.g.  (>= 2)  yields (lambda (xx) (>= xx 2))
   Tests are:  < | <= | = | >= | > | <> | BETWEEN | OUTSIDE | IN"
  (case (car expr)
    ((< <= > >=)
     `(lambda (xx) (if (,(car expr) (length xx) ,(cadr expr)) xx)))
    (=
     ;; if arg is 0 and list is empty cannot return the list and return True
     ;; for any other value we return the original list
     `(lambda (xx) (if (= (length xx) ,(cadr expr)) (if xx xx t))))
    (:ALL
     `(lambda (xx) xx))
    (<>
     `(lambda (xx) (if (not (eql (length xx) ,(cadr expr))) 
                     (if xx xx t))))
    (:BETWEEN
     `(lambda (xx) (if (and (>= (length xx) ,(cadr expr))
                            (<= (length xx) ,(caddr expr)))
                     xx)))
    (:OUTSIDE
     `(lambda (xx) (if (or (< (length xx) ,(cadr expr))
                           (> (length xx) ,(caddr expr)))
                     xx)))))

#|
(moss::make-test '(> 0))
(lambda (moss::xx) (if (> (length MOSS::XX) 0) MOSS::XX))

(moss::make-test '(:between 2 3))
(lambda (moss::xx)
  (if (and (>= (length MOSS::XX) 3) (<= (length MOSS::XX) 4)) MOSS::XX))

(moss::make-test '(:all))
(lambda (moss::xx) MOSS::XX)

(moss::make-test '(= 0))
(LAMBDA (XX) (IF (EQL (LENGTH XX) 0) (IF XX XX T)))

(moss::make-test '(= 2))
(LAMBDA (XX) (IF (EQL (LENGTH XX) 2) (IF XX XX T)))
|#
;;;--------------------------------------------------------------------- SPLIT-QUERY

(defUn split-query (query)
  "Takes a sub-query. Isolates simple clauses which do not contain variables ~
   from the rest of the sub-query.
Arguments:
   query: a parsed query
Returns:
   4 values:
      - a list of simple clauses including OR clauses with simple clauses, 
      - a list of simple variable clauses
      - a list of mixed OR-clauses, including those with variables
      - a list of sub-queris, 
      - and the class variable if any."
  (let ((var (cadr query)) simple-clauses simple-clauses-with-variables
        complex-or-clauses sub-queries)
    ;; if we have a class variable (person ?Y (...)), then record it
    (if (variable? var)(setq query (cdr query)) (setq var nil))
    ;; if we have more than a clause, examine each clause in turn
    (if (cdr query)
      (dolist (clause (cdr query))
        (cond
         ((or (simple-clause-with-no-variable? clause)
              (or-simple-clause-with-no-variable? clause))
          (push clause simple-clauses))
         ((simple-clause-with-variable? clause)
          (push clause simple-clauses-with-variables))
         ((eql 'OR (car clause))
          (push clause complex-or-clauses))
         (t
          (push clause sub-queries)))))
    (values (reverse simple-clauses) (reverse simple-clauses-with-variables) 
            (reverse complex-or-clauses) (reverse sub-queries) var)))

#|
? (MOSS::SPLIT-QUERY  
   '($E-PERSON
     ($T-PERSON-NAME :IS ?X)
     ($T-PERSON-FIRST-NAME :is "dominique")
     ($T-PERSON-AGE = 22)
     (OR
      ($S-PERSON-BROTHER (> 0) ($E-PERSON))
      ($S-PERSON-SISTER (< 1) ($E-PERSON)))
     ($S-PERSON-COUSIN (> 1) ($E-PERSON))))
(($T-PERSON-FIRST-NAME :IS "dominique") ($T-PERSON-AGE = 22))
(($T-PERSON-NAME :IS ?X))
((OR ($S-PERSON-BROTHER (> 0) ($E-PERSON)) ($S-PERSON-SISTER (< 1) ($E-PERSON))))
(($S-PERSON-COUSIN (> 1) ($E-PERSON)))
NIL
? (MOSS::SPLIT-QUERY  
   '($E-PERSON
     ($T-PERSON-NAME :IS ?X)
     ($T-PERSON-FIRST-NAME :is "dominique")
     (or ($T-PERSON-AGE = 22) ($T-PERSON-AGE = 24))
     (OR
      ($S-PERSON-BROTHER (> 0) ($E-PERSON))
      ($S-PERSON-SISTER (< 1) ($E-PERSON)))
     ($S-PERSON-COUSIN (> 1) ($E-PERSON))))
(($T-PERSON-FIRST-NAME :IS "dominique")
 (OR ($T-PERSON-AGE = 22) ($T-PERSON-AGE = 24)))
(($T-PERSON-NAME :IS ?X))
((OR ($S-PERSON-BROTHER (> 0) ($E-PERSON)) ($S-PERSON-SISTER (< 1) ($E-PERSON))))
(($S-PERSON-COUSIN (> 1) ($E-PERSON)))
NIL
? (MOSS::SPLIT-QUERY  
   '($E-PERSON ?+E
     ($T-PERSON-NAME :IS ?X)
     ($T-PERSON-FIRST-NAME :is "dominique")
     (or ($T-PERSON-AGE = 22) ($T-PERSON-AGE = 24))
     (OR
      ($S-PERSON-BROTHER (> 0) ($E-PERSON))
      ($S-PERSON-SISTER (< 1) ($E-PERSON)))
     ($S-PERSON-COUSIN (> 1) ($E-PERSON))))
(($T-PERSON-FIRST-NAME :IS "dominique")
 (OR ($T-PERSON-AGE = 22) ($T-PERSON-AGE = 24)))
(($T-PERSON-NAME :IS ?X))
((OR ($S-PERSON-BROTHER (> 0) ($E-PERSON)) ($S-PERSON-SISTER (< 1) ($E-PERSON))))
(($S-PERSON-COUSIN (> 1) ($E-PERSON)))
?+E
|#
;;;------------------------------------------------ SPLIT-QUERY-AND-COMPUTE-FILTERS

(defUn split-query-and-compute-filters 
    (query &optional (cardinality-test '(> 0)))
  "function called by filter-candidates, to prepare the ground for testing. It ~
   splits the query into different parts, and compute a filter from attribute ~
   clauses and simple OR attribute clauses to apply to future candidates. If ~
   the compiler is available, then compiles the filter.
   When a cardinality test is provided, builds another filter to test that will ~
   apply to the number of retrieved objects.
   Those operations should be done only once and not everytime a new candidate ~
   is examined, which is the justification for this function.
Arguments:
   query: sub-query, e.g. ($E-PERSON ($T-PERSON-NAME :IS \"Barthes\")
                     ($S-PERSON-COUSIN (> 0) 
                       ($E-PERSON ($T-PERSON-FIRST-NAME :IS \"antoine\"))))
   cardinality-filter: something like (> 2) or (:BETWEEN 4 5) or nil
Return:
   6 values
     name of local filter
     name of cardinality filter (can be nil)
     clauses-with-variables 
     or-clauses
     subqueries 
     class variable"
  (let ((test-lambda (make-test cardinality-test)) ; lambda expr (can be nil)
        (local-filter (gensym "LOCAL-FILTER-"))
        (cardinality-test (gensym "TEST-"))
        filter-clauses-items filter-form clauses-with-variables
        subqueries or-clauses)
    
    ;; we first split the query into a few parts
    (setq filter-clauses-items  (make-local-filter query))
    ;; the function returned a list of several vallues
    (setq filter-form (car filter-clauses-items)
        clauses-with-variables (cadr filter-clauses-items)
        or-clauses (caddr filter-clauses-items)
        subqueries (cadddr filter-clauses-items)
        ; we ignore last item (class variable)
        )
    
    ;; debug info
    (dformat :query 0 "split-query-and-compute-filters ; ~
              ~&  class filter: ~S; filter-form: ~&   ~S
   Cardinality test: ~S; test-lambda: ~%    ~S
   Clauses with variables:~%     ~S
   OR-Clauses:~%     ~S
   Subqueries:~%     ~S~%" 
             local-filter filter-form  cardinality-test test-lambda
             clauses-with-variables or-clauses subqueries)
    
    ;; we cannot use the compiler on a distribution version (not licensed...)
    (if (member :compiler *features*)
        (progn
          (compile cardinality-test test-lambda)
          (compile local-filter filter-form))
      ;; otherwise, we keep the lambda forms...
      (setq cardinality-test test-lambda local-filter filter-form))
    
    ;; return a list of the pieces
    (list local-filter cardinality-test clauses-with-variables subqueries or-clauses) 
    ))

#|
(moss::split-query-and-compute-filters
   '($E-PERSON ($S-PERSON-COUSIN (> 0) 
                                 ($E-PERSON ($T-PERSON-NAME :IS "Labrousse"))))
   )
split-query-and-compute-filters ; 
  class filter: #:LOCAL-FILTER-3304; filter-form: 
   (LAMBDA (MOSS::XX)
     (AND (INTERSECTION '($E-PERSON $E-STUDENT) (MOSS::%TYPE-OF MOSS::XX)) MOSS::XX))
   Cardinality test: #:TEST-3305; test-lambda: 
    (LAMBDA (MOSS::XX) (IF (> (LENGTH MOSS::XX) 0) MOSS::XX))
   Clauses with variables:
     NIL
   OR-Clauses:
     NIL
   Subqueries:
     (($S-PERSON-COUSIN (> 0) ($E-PERSON ($T-PERSON-NAME :IS "Labrousse"))))
(#:LOCAL-FILTER-3304 
 #:TEST-3305 NIL
 (($S-PERSON-COUSIN (> 0) ($E-PERSON ($T-PERSON-NAME :IS "Labrousse"))))
 NIL)
;; here the class filter checks that the object belongs to either class, and the
;; cardinality filter ensures that we must have at least one object at the end
;; the subqueries start with the relation

(moss::split-query-and-compute-filters
   '($e-person ($T-PERSON-NAME :IS "Labrousse")))
split-query-and-compute-filters ; 
  class filter: #:LOCAL-FILTER-3309; filter-form: 
   (LAMBDA (MOSS::XX)
     (AND (INTERSECTION '($E-PERSON $E-STUDENT) (MOSS::%TYPE-OF MOSS::XX))
          (LET ((MOSS::DATA (MOSS::=> MOSS::XX '=GET-ID '$T-PERSON-NAME)))
            (IF (OR (MLN::MLN? '"Labrousse") (SOME #'MLN::MLN? MOSS::DATA)
                    (MLN::%MLN? '"Labrousse") (SOME #'MLN::%MLN? MOSS::DATA))
                (INTERSECTION '("Labrousse") MOSS::DATA :TEST #'MLN::MLN-EQUAL)
              (INTERSECTION '("LABROUSSE")
                            (MAPCAR #'MOSS::NORMALIZE-VALUE
                                    (MOSS::=> MOSS::XX '=GET-ID '$T-PERSON-NAME))
                :TEST #'EQUAL)))
          MOSS::XX))
   Cardinality test: #:TEST-3310; test-lambda: 
    (LAMBDA (MOSS::XX) (IF (> (LENGTH MOSS::XX) 0) MOSS::XX))
   Clauses with variables:
     NIL
   OR-Clauses:
     NIL
   Subqueries:
     NIL
(#:LOCAL-FILTER-3309 #:TEST-3310 NIL NIL NIL)
;; here the class filter is more complex since we have an attribute clause
;; in addition to testing class belonging. Actually it should be
  (LAMBDA (MOSS::XX)
     (AND (INTERSECTION '($E-PERSON $E-STUDENT) (MOSS::%TYPE-OF MOSS::XX))
          (LET ((MOSS::DATA (MOSS::=> MOSS::XX '=GET-ID '$T-PERSON-NAME)))
            (IF (OR (MLN::MLN? '"Labrousse") (SOME #'MLN::MLN? MOSS::DATA)
                    (MLN::%MLN? '"Labrousse") (SOME #'MLN::%MLN? MOSS::DATA))
                (INTERSECTION '("Labrousse") MOSS::DATA :TEST #'MLN::MLN-EQUAL)
              (INTERSECTION '("LABROUSSE")
                            (MAPCAR #'MOSS::NORMALIZE-VALUE
                                    (MOSS::=> MOSS::XX '=GET-ID '$T-PERSON-NAME))
                :TEST #'EQUAL)))
          MOSS::XX))
|#
;;;----------------------------------------------------------------------- VALIDATE

(defUn validate2 (candidate query bindings checking-material)
  "checks a single candidate against the rest of the query. If the query is an ~
   entry-point then the function returns immediately. Otherwise the rest of the ~
   candidates is checked against the rest of the query using the filters and ~
   query pieces from the last argument.
Arguments:
   candidate: an object to accept or reject
   query: the query for which the object is checked
   checking-material: a list of filters and query parts coming from the function
                      split-query-and-compute-filters
Returns:
   candidate or nil"
  (drformat :query 0 "~2%;---------- Entering validate2")
  ;; if query was an entry point, then candidate is valid
  (when (or (stringp query)(symbolp query))
     (drformat :query 0 "~%;---------- Leaving validate2") 
    (return-from validate2 candidate))
  ;; otherwise it is more complex
  (let ((clauses-with-variables (nth 2 checking-material))
        (subqueries (nth 3 checking-material))
        (or-clauses (nth 4 checking-material)))
    (dformat :query 0 "clauses-with-variables:~% ~S~%subqueries:~%  ~S~
            ~%or-clauses:~%  ~S" clauses-with-variables subqueries or-clauses)
    (and (if clauses-with-variables
           ;; check if a clause eliminates the candidate
           ;; eventually set the context up failure if nil
           ;; if clause succeeds update bindings, if not failure
           (setq bindings 
                 (collect-check-clause-list-with-variables 
                  candidate clauses-with-variables bindings))
           t)
         (if subqueries
           ;; check if a sub-query eliminates the candidate
           ;; setting a coreference variable in a sub-query does not affect
           ;; the global bindings, but may affect downstream processing
           ;; inside the sub-query
           (collect-check-subquery-list candidate subqueries bindings)
           t)
         ;; then we must check complex OR clauses 
         ;; bindings are not modified by an OR clause
         (if or-clauses 
           (collect-check-or-clause-list candidate or-clauses bindings)
           t)
         candidate)))


;;;=================================================================================
;;;
;;;                       Classes and Methods for Access
;;;  
;;;=================================================================================

;;; Defining methods with definstmethod and defownmethod make the methods internal 
;;; to the MOSS package. Thus, the =get-instance-list methods are not available 
;;; outside MOSS, unless prefixed by moss::

;;;------------------------------------------------------------------- QUERY-HANDLER

(defsysclass "QUERY-HANDLER"
           (:doc :en "Handler for driving the querying process of MOSS.")
           (:id $QHDR)
           )

(setq *query-handler* (%definstance QUERY-HANDLER)
      *qh* *query-handler*)

;;;--------------------------------------------------------------------------- QUERY

(defsysclass "QUERY" 
           (:doc :en "Each request is stored as a temporary object.")
           (:id $QRY)
           (:tp QUERY-NAME (:id $QNAM)(:unique)(:entry))
           (:tp EXPRESSION (:id $QXPT))
           (:tp USER-EXPRESSION (:id $QUXT))
           (:tp ANSWER (:id $QANT))
           )

;;;===================================== methods ===================================

;;;--------------------------------------------------------- (QUERY-HANDLER) =ACCESS

(definstmethod =access QUERY-HANDLER (query &key already-parsed verbose)
  "Accesses the query handler. Uses the access function.
Arguments:
   query: parsed or unparsed
   already-parsed (key): if true indicates that the query is parsed
   verbose (key): trace action if true
Returns
   a list of objects filtered by the constraints specified in the query."
  (access query  :already-parsed already-parsed :verbose verbose))

;;;=================================================================================
;;;                         GET INSTANCE LIST METHODS
;;;=================================================================================
;;; The methods are used to obtain instances of a class, or list of concepts, 
;;; attributes, relations, inverse links, entry points, etc.
;;; When executed in the MOSS environment, get the entities of the moss environment.
;;; When executed in the application environment, should get the instances and lists
;;; of entities defined in the application environment.
;;; During a dialog, the requests and accesses are done in the application environment
;;; Tasks however are defined in the MOSS package, and, depending on the question,
;;; sometimes we want MOSS entities (e.g. documentation objects), sometimes we want
;;; application objects (e.g. "What is a PERSON?").
;;; The explanation dialog is called when we are in the application package. However,
;;; since we create a conversation process, we can execute it in the MOSS package,
;;; with a special hook when asking info about specific application objects. Detailed
;;; information about the KB can be obtained in the QUERY mode. In addition, concept
;;; documentation in the application can be defined in the MOSS environment.
;;;=================================================================================

;;;---------------------------------------------- (ATTRIBUTE:own) =GET-INSTANCE-LIST

;;; MOSS-ATTRIBUTE being an entry point defined in the :moss package, we cannot get
;;; the application attributes

(defownmethod =get-instance-list (MOSS-ATTRIBUTE HAS-MOSS-CONCEPT-NAME MOSS-ENTITY) ()
  "Returns the list of relations in a given system."
  ;; get first the system name
  (%get-value
   (symbol-value (intern "*MOSS-SYSTEM*"))
   '$ETLS))

#|
FAMILY(230): (access '("moss attribute"))
($T-NAME $T-PERSON-NAME $T-FIRST-NAME $T-PERSON-FIRST-NAME $T-NICK-NAME $T-PERSON-NICK-NAME $T-AGE $T-PERSON-AGE
         $T-BIRTH-YEAR $T-PERSON-BIRTH-YEAR ...)

FAMILY(231): (with-package :moss (access '("moss attribute")))
(MOSS::$ID MOSS::$TYPE MOSS::$DEFT MOSS::$VALT MOSS::$CNAM MOSS::$XNB MOSS::$TMBT MOSS::$DOCT
 MOSS::$ARG ...)
|#
;;;----------------------------------------------------- (ENTITY) =GET-INSTANCE-LIST
;;; looks like it is the same method as =get-instances
;;; we create instances in the same package as the concept, which seems sensible
;;; Thus for methods if the concept is $FN, we'll ge MOSS methods; if the concept
;;; is $E-FN, we'll get application methods
#|
(dformat-set :gil 0)
(dformat-reset :gil)
|#

(defsysmethod =get-instance-list MOSS-ENTITY ()
  "Get the explicit list of instances corresponding to a given user class.~
   If the global variable *query-allow-sub-classes* is true adds instances ~
   of the spanned sub-classes."
  (let ((class-list (if *query-allow-sub-classes*
                        (%sp-gamma *self* (%inverse-property-id '$IS-A))
                      (list *self*)))
        (context (symbol-value (intern "*CONTEXT*")))
        max-count lres id)
    (drformat :gil 0 ";========== Entering =get-instance-list method")
    (dformat :gil 0 "class-list: ~S" class-list)
    
    (dolist (class-id class-list)
      (send class-id '=get 'HAS-MOSS-COUNTER)
      ;; 0407JPB decreasing max value by one
      (setq max-count (1- (car (send (car *answer*) '=get 'HAS-MOSS-VALUE))))
      (dotimes (nn max-count)
        (setq id (%%make-id :instance :class-id class-id :value (1+ nn)))
        (when (%alive? id context)
          (push id lres))
        ))
    
    (drformat :gil 0 ";========== End =get-instance-list")
    (reverse lres)))

#|
(send '$E-PERSON 'moss::=get-instance-list)
($E-PERSON.1 $E-PERSON.2 $E-STUDENT.1)

(send '$E-STUDENT '=get-instance-list)
($E-STUDENT.1)

(with-package :test
  (moss::send 'test::$E-PERSON 'moss::=get-instance-list))
((TEST::$E-PERSON . 1) (TEST::$E-PERSON . 2) (TEST::$E-STUDENT . 1))

(with-package :test
  (with-context 3
    (moss::send 'test::$E-PERSON 'moss::=get-instance-list)))
((TEST::$E-PERSON . 1) (TEST::$E-PERSON . 2) (TEST::$E-PERSON . 3)
 (TEST::$E-STUDENT . 1))
|#
;;;------------------------------------------------- (ENTITY:own) =GET-INSTANCE-LIST

;;; does not work properly

(defownmethod  =get-instance-list $ENT ()
  "Get the explicit list of classes of a given application.~
   If the global variable *query-allow-sub-classes* is true adds instances ~
   of the spanned sub-classes.
   Kernel classes like $ENT, $EPS, $EPT are not included unless the flag ~
   *query-include-kernel-objects* is set to T."
  ;; First read all the classes from the system object
  (let ((class-list 
         (%get-value 
          (symbol-value (intern "*MOSS-SYSTEM*"))
          '$ENLS)))
    (if *query-include-kernel-objects*
      ;; when we want all classes, we simply return the class list
      class-list
      ;; otherwise, we must remove all kernel entities, i.e. those that are
      ;; instances of $ENT
      (remove-if-not #'(lambda (xx) (string= "$E-" (subseq (symbol-name xx) 0 3)))
                     class-list))))

#|
(send 'moss::$ENT 'moss::=get-instance-list)
($E-TARGET $E-JUNK $E-SUB-TARGET $E-TEST $E-ORGANIZATION $E-PERSON $E-STUDENT
           $E-COUNTRY)

(access '("moss class"))
($E-TARGET $E-JUNK $E-SUB-TARGET $E-TEST $E-ORGANIZATION $E-PERSON $E-STUDENT
           $E-COUNTRY)

(with-package :test
  (access '("moss class")))
(TEST::$E-FN TEST::$E-UNI TEST::$E-CTR TEST::$E-TARGET TEST::$E-JUNK
 TEST::$E-SUB-TARGET TEST::$E-TEST TEST::$E-ORGANIZATION TEST::$E-PERSON
             TEST::$E-STUDENT ...)

(setq *query-include-kernel-objects* t)
(access '($ent))
($CTR $FN $UNI $SYS $ENT $VENT $EPR $EPS $EPT $EP $EIL *NONE* *ANY* $DOCE $ENDCE
 $FRDCE $CNFG $USR $QHDR $QRY $E-TARGET $E-JUNK $E-SUB-TARGET $E-TEST
 $E-ORGANIZATION $E-PERSON $E-STUDENT $E-COUNTRY)
|#
;;;----------------------------------------------------- (*NONE*) =GET-INSTANCE-LIST

;;;(defownmethod =get-instance-list *none* ()
;;;  "Get the explicit list of orphans."
;;;  (let (max-count lres)
;;;    (send (symbol-value (intern "*MOSS-SYSTEM*" (symbol-package *self*)))
;;;          '=get 'HAS-CLASSLESS-COUNTER)
;;;    (setq max-count (car (send (car *answer*) '=get 'HAS-VALUE)))
;;;    (dotimes (nn max-count)
;;;      (setq lres (cons (%%make-id :orphan :value (1+ nn))
;;;                       lres)))
;;;    (reverse lres)))

;;;------------------------------------------------ (ENTRY-POINT) =GET-INSTANCE-LIST

(defownmethod =get-instance-list $EP ()
  "Get the explicit list of all entry-points."
  (sort (copy-list 
         (%get-value 
          (symbol-value (intern "*MOSS-SYSTEM*")) 
          '$EPLS)) 
        #'string-lessp))

#|
FAMILY(275): (access '("moss entry point"))
(=IF-NEEDED =MAKE-ENTRY =SUMMARY BARTHÈS BIESEL CANAC CHEN COUNTER COURS COURSE ...)

FAMILY(277): (with-package :moss (access '("moss entry point")))
(=ACCESS =ACTIVATE =ADD =ADD-ATTRIBUTE-DEFAULT =ADD-ATTRIBUTE-VALUES =ADD-ATTRIBUTE-VALUES-USING-ID
 =ADD-RELATED-OBJECTS =ADD-SP =ADD-SP-ID ...)
|#
;;;------------------------------------------- (INVERSE-LINK:own) =GET-INSTANCE-LIST

(defownmethod =get-instance-list (MOSS-INVERSE-LINK HAS-MOSS-ENTITY-NAME MOSS-ENTITY) ()
  "Returns the count of inverse-links in a given system."
  ;; get first the system name
  (%get-value 
   (symbol-value (intern "*MOSS-SYSTEM*")) 
   '$EILS))

#|
FAMILY(279): (access '("moss inverse link"))
($T-NAME.OF $T-PERSON-NAME.OF $T-FIRST-NAME.OF $T-PERSON-FIRST-NAME.OF $T-NICK-NAME.OF $T-PERSON-NICK-NAME.OF
 $T-AGE.OF $T-PERSON-AGE.OF $T-BIRTH-YEAR.OF $T-PERSON-BIRTH-YEAR.OF ...)
FAMILY(280): (with-package :moss (access '("moss inverse link")))
(MOSS::$ID.OF MOSS::$TYPE.OF MOSS::$DEFT.OF MOSS::$VALT.OF MOSS::$CNAM.OF MOSS::$XNB.OF
 MOSS::$TMBT.OF MOSS::$DOCT.OF MOSS::$ARG.OF ...)
|#
;;;------------------------------------------------- (METHOD:own) =GET-INSTANCE-LIST

(defownmethod =get-instance-list (MOSS-method HAS-MOSS-ENTITY-NAME MOSS-ENTITY) ()
  "Returns the list of methods in a given system."
  ;; get first the functions from the local moss system
  (%get-value 
   (symbol-value (intern "*MOSS-SYSTEM*" (symbol-package *self*)))
   '$FNLS))

#|
;;; if we are in an application package and want the list of methods, we call
(send '$E-FN 'moss::=get-instance-list)

FAMILY(185): (access '("method"))
($E-FN.1 $E-FN.2 $E-FN.3 $E-FN.4 $E-FN.5 $E-FN.6 $E-FN.7 $E-FN.8)

;;; if we want the list of MOSS methods we call
(send 'moss::$FN 'moss::=get-instance-list)
(MOSS::$FN.0 MOSS::$FN.1 MOSS::$FN.2 MOSS::$FN.3 MOSS::$FN.4 MOSS::$FN.5 MOSS::$FN.6 MOSS::$FN.7 MOSS::$FN.8
             MOSS::$UNI.0 ...)

FAMILY(182): (access '("moss method"))
(MOSS::$FN.0 MOSS::$FN.1 MOSS::$FN.2 MOSS::$FN.3 MOSS::$FN.4 MOSS::$FN.5 MOSS::$FN.6 MOSS::$FN.7 MOSS::$FN.8
             MOSS::$FN.9 ...)
|#
;;;----------------------------------------------- (RELATION:own) =GET-INSTANCE-LIST

(defownmethod =get-instance-list (MOSS-RELATION HAS-MOSS-ENTITY-NAME MOSS-ENTITY) ()
  "Returns the list of relations in a given system."
  ;; get first the system name
  (%get-value 
   (symbol-value (intern "*MOSS-SYSTEM*")) 
   '$ESLS))

#|
FAMILY(284): (access '("moss relation"))
($S-BROTHER $S-PERSON-BROTHER $S-SISTER $S-PERSON-SISTER $S-HUSBAND $S-PERSON-HUSBAND $S-WIFE $S-PERSON-WIFE
 $S-MOTHER $S-PERSON-MOTHER ...)
FAMILY(285): (with-package :moss (access '("moss relation")))
(MOSS::$DOCS MOSS::$PT MOSS::$PS MOSS::$IS-A MOSS::$OMS MOSS::$IMS MOSS::$CTRS MOSS::$SYSS MOSS::$DEFS
 MOSS::$REQS ...)
|#
;;;--------------------------------------- (UNIVERSAL-METHOD:own) =GET-INSTANCE-LIST

(defownmethod =get-instance-list 
    (MOSS-universal-method HAS-MOSS-ENTITY-NAME MOSS-ENTITY) ()
  "Returns the list of methods in a given system."
  ;; get first the functions from the local moss system
  (let ((result 
         (reduce  ; JPB 140820 removing mapcan
          #'append
         (mapcar #'(lambda (xx) (%is-universal-method? xx) (list xx))
           (%get-value 
            (symbol-value (intern "*MOSS-SYSTEM*" ))
            '$FNLS)))))
    result))

#|
FAMILY(286): (access '("moss universal method"))
NIL
FAMILY(289): (with-package :moss (access '("moss universal method")))
(MOSS::$UNI.0 MOSS::$UNI.1 MOSS::$UNI.2 MOSS::$UNI.3 MOSS::$UNI.4 MOSS::$UNI.5 MOSS::$UNI.6 MOSS::$UNI.7
 MOSS::$UNI.8 MOSS::$UNI.9 ...)
|#
;;;---------------------------------------------------------- (QUERY-HANDLER) =PARSE

(definstmethod =parse QUERY-HANDLER (query-form)
  "Parse a query form and translate into internal format."
  ;; translate into internal format - we use current system as context
  (parse-user-query query-form))

#|
? (send *qh* '=parse '(person (has-brother (person (has-name is "barthes"))) (has-first-name :is "emilie")))
($E-PERSON ($S-PERSON-BROTHER (> 0) ($E-PERSON ($T-PERSON-NAME :IS "barthes")))
 ($T-PERSON-FIRST-NAME :IS "emilie"))
? (send *qh* '=parse 'barthes)
BARTHES
? (send *qh* '=parse "barthes biesel")
"barthes biesel"
|#

;;;==================================== Tests ====================================

#|
(person (has-brother (= 0) (person)))
;;;   access all males
(person (has-sex is "m"))
;;;   access all person named "Barths" and not known to be males
(person (has-name is "Barths")(has-sex is-not "m"))
;;;   access all persons with brothers
(person (has-brother (person)))
;;;   access all persons with no (gettable or recorded) brothers (close world)
(person (has-brother (= 0) (person)))
;;;   access all Barthes's who have only one brother called "Sebastien"
(person (has-name is "barthes")
        (has-brother (= 1) (person (has-first-name is "Sebastien"))))
;;;   access all persons brother of a person called sebastien
(person (is-brother-of (= 1) (person (has-first-name is "Sebastien"))))
;;;   access all persons with at least 2 children of any sex
(person (OR (>= 2) (has-son (>= 0) (person))
                   (has-daughter (>= 0) (person))))
;;; answer should be: jpb dbb pxb chb pb mb mbl ml
;;; the following question removes jpb and dbb from the list. Indeed, one must
;;; have at least 2 sons if any, or 2 girls.
(person (OR (>= 2) (has-son (>= 2) (person))
                   (has-daughter (>= 0) (person))))
;;; answer should be: pxb, chb, pb, mb, mbl, ml
;;; The following question removes mlb and ml, since we want at most 2 girls.
(person (OR (>= 2) (has-son (>= 2) (person))
                   (has-daughter (<= 2) (person))))
;;; answer should be: pxb, chb, pb, mb
;;; next query has a bad syntax (cannot include TP-clause in a constrained sub-query)
(person (OR (>= 2) (has-son (>= 2) (person))
                   (has-sex = "f")
                   (has-daughter (<= 2) (person))))
;;; correct syntax should be. However it is not accepted (recursive OR)
(person (or (has-sex = "f")
            (or (>= 2) (has-son (>= 2) (person))
                (has-daughter (<= 2) (person)))))
;;; access all persons whith at least 2 children and most 2 girls and a boy
;;; answer is jpb, dbb
(person (OR (>= 2) (has-son (<= 1) (person))
                   (has-daughter (<= 2) (person))))
;;;   access all persons with at least 3 children one of them being a son
;;; note that we cannot do that with a single OR clause.
;;; answer is pxb, chb, pb, mb
(person (OR (>= 3) (has-son (>= 0) (person))
                   (has-daughter (>= 0) (person)))
        (has-son (>= 1) (person)))
;;;   persons who are cousins of a person who has a father with name "Labrousse"
;;; answer is: psb cb ab sb eb
(person (is-cousin-of  
               (person (has-father 
                             (person (has-name is "Labrousse"))))))
;;;   persons who have a brother who is cousin of a person who has a father...
;;; answer is: psb sb eb ab
(person (has-brother
         (person (is-cousin-of
                     (person (has-father 
                              (person (has-name is "Labrousse"))))))))
;;;   persons who have a father with more than 1 boy
;;; answer is: jpb pxb ab sb eb mglb
(person (has-father (person (has-son (>= 2) (person)))))
;;;   persons who have  a father with  at least 3 children with at least one girl
;;;           and whose father has a brother
;;; answer: ab, sb, eb
(person (has-father
            (person   ; condition on the cardinality of the subset!
                   (OR (>= 3) (has-son (> -1) (person))
                             (has-daughter (> -1) (person)))
                   (has-daughter (>= 1) (person))
                   (has-brother (person)))))
;;; persons who have 2 names
(person (has-name card= 2))
(person (has-name is "labrousse")(has-name card= 2))
(person (has-first-name card>= 2))

;;; ------------------- Queries with coreference variables
;;;   person who has a cousin of the same sex
 (person (has-sex is ?x) (has-cousin (person (has-sex is ?x))))
;;;   person who has a son with the same name as the son of his brother
;;; +++ Wrong answer because the first binding of ?x is not kept for later test
(person (has-son  (person (has-first-name is ?X)))
        (has-brother 
         (person (has-son  (person (has-first-name is-not ?X))))))
|#

:EOF