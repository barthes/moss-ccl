;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;==========================================================================
;;;21/08/12
;;;		
;;;		   U T I L S - (File utils.lisp)
;;;	
;;;	The file contains macros and functions used by the code. Some  
;;;	were part of UTC Lisp primitives. First created in April 1992
;;;     The file contains also the definition of global variables
;;;==========================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
#|
History
2019
 0913 correcting <<* to make sure *ST* is the one in the application package
 1002 adding nodgui patch to mformat
 1013 adding nth external functions
2021
 0812 modifying <<f to handle simple ids
 0816 adding ppo to pprint application objects from their reference string
|#
#|
Functions in this file
=== Debugging
vformat
D+
D-
pp
spl
V+
V-

=== New object id format
<<
<<*
<<boundp
<<f
<<get
<<symbol-package
<<symbol-plist
<<vomit
>>
>>f
>>makeunbound
>>remprop
>>setprop

=== a-lists
alistp
alist-add
alist-add-values
alist-rem
alist-rem-val
alist-set
alist-set-values

=== Miscellaneous
all-alike?
all-different?
firstn
get-field
dformat
drformat
dwformat
equal+
intersect-sets
lambdap
lob-get
member+
mexp
make-name
make-value-string
mformat
list-difference
nalist-replace-pv
nputv-last
ppo
|#
#|
(create-user-manual (choose-file-dialog))
(CREATE-USER-MANUAL (cg:ASK-USER-FOR-DIRECTORY :BROWSE-INCLUDE-FILES T))
|#
;;;==========================================================================

;;; all the following symbols are defined in the MOSS package

(in-package :moss)

;;;==========================================================================
;;;                     FUNCTIONS USED FOR DEBUGGING
;;;==========================================================================

;;;------------------------------------------------------------------ VFORMAT

#+OMAS
(defun moss::vformat (cstring &rest args)
  (declare (special omas::*trace-dialog-pane* moss::*verbose* omas::*omas*))
  (cond
   ((and moss::*verbose* (omas::text-window omas::*omas*))
    (apply #'format omas::*trace-dialog-pane* 
           (concatenate 'string "~%;***** " cstring)
           args))
   (moss:*verbose*
    (apply #'format *debug-io* (concatenate 'string "~%;***** " cstring)
             args))))

#-OMAS
(defun moss::vformat (cstring &rest args)
  "prints message to *debug-io* only when *verbose* is tT"
  (declare (special moss::*verbose*))
  (if moss:*verbose*
    (apply #'format *debug-io* (concatenate 'string "~%;***** " cstring)
           args)))
;;;----------------------------------------------------------------- DFORMAT-RESET

(defun dformat-reset (tag)
  (setf (get tag :trace) nil)
  )

;;;------------------------------------------------------------------- DFORMAT-SET
;;; debugging function, needs to be in front

(defun dformat-set (tag level)
  (setf (get tag :trace) t)
  (setf (get tag :trace-level) level)
  )

;;;----------------------------------------------------------------------- D+

(defun D+ ()
  (declare (special moss::*debug*))
  (setq moss::*debug* t)
  "moss::*debug* set to T (debugging dialogs?)"
  )

;;;----------------------------------------------------------------------- D-

(defun D- ()
  (declare (special moss::*debug*))
  (setq moss::*debug* nil)
  "moss::*debug* reset to NIL (end debugging)"
  )
;;;----------------------------------------------------------------------- PP
;;; debug function to fully print the result of a function in the listener

(defun pp ()
  (progn (pprint *) :done))

;;;---------------------------------------------------------------------- SPL

(defun spl (xx) (symbol-plist xx))

;;;----------------------------------------------------------------------- V+

(defun V+ ()
  (declare (special moss::*verbose*))
  (setq moss::*verbose* t)
  "moss::*verbose* set to T"
  )

;;;----------------------------------------------------------------------- V-

(defun V- ()
  (declare (special moss::*verbose*))
  (setq moss::*verbose* nil)
  "moss::*verbose* reset to NIL"
  )

;;;==========================================================================
;;;              FUNCTIONS FOR NEW MOSS OBJECT FORMAT AS PAIR IDs
;;;==========================================================================

;;; See elementary tests in z-moss-tests-id-pairs.list

;;; In version 10.0 instance ids are no longer symbols in application packages
;;; but are replaced by pairs (<class-id> . <nb>) in order to avoid symbol
;;; table overflow. Such pairs are kept in a special hash table *ST*.
;;; Because they are no longer symbols, plists must be associated with the
;;; entry in *ST*. Thus, an entry is a list (value plist).
;;; Thus, an instance id will be unbound if not in *ST*. However, because an
;;; unbound entry may have a plist, we must mark the entry with a special tag
;;; and use the value :_<unbound>

;;;----------------------------------------------------------------------- <<
;;; Because << replaces symbol-value, whenever the id is a dotted pair, it 
;;; should look for the value in the hash table of the package of the class 
;;; symbol of the id
;;;  (symbol-value 'test::$E-PERSON.123) returns the value of the symbol in
;;;      package TEST
;;;  (<< '(test::$E-PERSON . 123)) looks for a value in the hash table
;;;      of package TEST

(defun << (key)
  "retrieves a value corresponding to key from the application hash table
Argument:
   key: symbol or id pair, e.g. ($E-PERSON . 123)
Return:
   3 values
    - the value (nil if not an entry)
    - the plist (nil if not an entry)
    - tag: nil if not an entry or is unbound, t otherwise"
  ;; check first if we have a symbol
  (cond
   ((and (symbolp key)(boundp key))
    ;; id is a symbol and bound -> OK
    (values (symbol-value key)(symbol-plist key) t))
   
   ((symbolp key)
    ;; id is a symbol but unbound
    (error "Attempt to take the value of the unbound key '~S'" key))
    ;(values nil (symbol-plist key) nil))    
   
   ;; if not a symbol, then obj-id must be a dotted pair id
   ((%%is-id? key :pair)
      ;; id is a pair id
      (multiple-value-bind (item tag) (<<* key)
        ;(print (list (car item) tag (eql (car item) :_<UNBOUND>)))
        (if (or (null tag) (equal (car item) :_<unbound>))
            (error "Attempt to take the value of the unbound key '~S'" key)) 
        ;; otherwise return 3 values 
        (values (car item)(cadr item) tag)))))

#|
(<< '($e-person . 123))
Error: Attempt to take the value of the unbound key '($E-PERSON . 123)'

(<< '(n1::$e-person . 123))
((N1::$TYPE (0 N1::$E-PERSON)) (N1::$ID (0 (N1::$E-PERSON . 123)))
 (N1::$E-PERSON-NAME (0 "Arthur"))
 (N1::$E-PERSON-BROTHER (0 (N1::$E-PERSON . 155))))
NIL
T

(<< '($e-person . 155))
Error: Attempt to take the value of the unbound key '($E-PERSON . 155)'

(>> '($e-person . 155) 155 :new-plist '(:a 1))
155

(<< '($e-person . 155))
(155 T NIL) 
155
(:A 1)
T
|#
;;;--------------------------------------------------------------------- <<*

(defun <<* (key)
  "get the content of the *ST* cell corresponding to key in the current package.
   If key is a symbol, returns (NIL NIL) if unbound, or (NIL T) if bound and NIL.
   If key is an id pair, returns
Argument:
   key: must be an ID or ID pair
Return:
   2 values
   - the list (<value> <plist>) or nil if key is unbound or there is no entry 
     in *ST*
   - T if entry exists, NIL if unbound or entry does not exist" 
  (cond
   ((symbolp key)
    (if (boundp key)
        (values (list (symbol-value key)(symbol-plist key)) t)
      (values NIL NIL)))
   (t
    (let ((ST (intern "*ST*"  (symbol-package (car key)))))
      ;(print `(<<* ST ,ST *ST* ,*ST* *package* ,*package*))
      ;; *ST* must be the symbol hash table in the application package
      ;; i.e. the package of the object class
      (unless (and (boundp ST)(hash-table-p (setq ST (symbol-value ST))))
        (error "<<*: trying to get the value associated with the key ~S, but ~
            the *ST* symbol table is missing in package ~S" 
          key (package-name (symbol-package (car key)))))
      ;; id is a pair id
      (gethash key ST)))
   ))


#|
(<<* 'aaa) ; aaa is unbound
NIL
NIL

(setq aaa 111)
111

(<<* 'aaa)
(111 NIL)
T

(<<* '(A . 22))
NIL
NIL

(>> '(A . 1) '(:c 3))
(:C 3)

(<<* '(A . 1))
((:C 3) NIL)
T

(>> '(A . 1) nil)
NIL

(<<* '(A . 1))
(NIL NIL)
T
|#
;;;----------------------------------------------------------------- <<BOUNDP

(defun <<boundp (key)
  "checks if obj-id is bound or not. When key is a symbol, identical to ~
   (boundp key), otherwise nil means that the id is not in the *ST* table ~
   or is in it with an unbound mark."
  (cond
   ((symbolp key)
    (boundp key))
   ;; if not a symbol, then obj-id must be a dotted pair id
   ((%%is-id? key :pair)
    ;; id is a pair id
    (multiple-value-bind (item tag) (<<* key)
      ;; if val is :_<unbound> returns nil
      (if (eql (car item) :_<unbound>) (return-from <<boundp nil))
      ;; return last, if nil key is not in table 
      tag))
   (t (error "<<bound: arg ~S should be symbol or id pair" key))
   ))

#|
(<<boundp 'albert)
NIL
(boundp 'moss::$ENT)
T

;; in the TEST package
(with-package :test
 (moss::<<vomit))
($E-PERSON . 555) 
($PERSON . 123) 
:DONE

(moss::<<* '($FN . 0))
NIL

(moss::<<* '($E-PERSON . 123))
T

(with-package :test
  (moss::>> '($E-PERSON . 123) 123 :new-plist '(:a 1)))
123
(with-package :test
  (moss::<<boundp '($E-PERSON . 123)))
T
|#
;;;---------------------------------------------------------------------- <<F

(defun <<f (str &optional (package *package*))
  "takes a compact format, e.g. \"$E-CONCEPT..33\" and returns a pair id: ~
   ($E-CONCEPT . 33), interning the first symbol in the currrent package.
  Uses PPCRE.
  If str is a simple id, e.g. \"$FN.32\", returns the corresponding symbol
Argument:
  str: a string that must not contain any space
Return:
  a pair-id if OK, str otherwise"
  (or (and (stringp str)
           (not (position #\space str))
           (multiple-value-bind (start end) (ppcre::scan "\\w\\.\\.\\w" str)
             (if start
                 (cons (intern (subseq str 0 (1+ start)) package)
                       (read-from-string (subseq str (1- end)))))))
      (and (stringp str)
           (not (position #\space str))
           (char= #\$ (char str 0))
           (intern (string-upcase str) package))
      str))

#|
? (<<f "$E-CONCEPT..33")
($E-CONCEPT . 33)

? (<<f "$E-CONCEPT..33" :cscwd-onto)
(CSCWD-ONTO::$E-CONCEPT . 33)

? (<<f "$E-CONCEPT..33 and more")
"$E-CONCEPT..33 and more"

? (<<f "$E-PERSON.23") ; e.g. for kernel instances or class ids
$E-PERSON.23
|#
;;;-------------------------------------------------------------------- <<GET

(defun <<get (key prop)
  "get the value attached to the property of the pair key
Arguments:
   key: a pair key
  prop: the prop of the plist
Return:
   2 values
    - the value associated with the pair
    - t if the entry existed, nil otherwise"
  (cond
   ((symbolp key)
    (get key prop))
   ((%%is-id? key :pair)
    ;; id is a pair id
    (multiple-value-bind (item tag) (<<* key)
      (if tag (getf (cadr item) prop) nil)))
   (t (error "<<bound: arg ~S should be symbol or id pair" key))
   ))
  
  
#|
(<<GET '($e-person . 123) :test)
NIL
T

(<<GET '($e-person . 155) :test)
NIL
NIL

(>>SETPROP '($e-person . 123) "test text" :test)
"test text"

(<<GET '($e-person . 123) :test)
"test text"
T
|#
;;;--------------------------------------------------------- <<SYMBOL-PACKAGE

(defun <<symbol-package (key)
  "gets symbol-package of the key."
  (cond
   ((symbolp key)
    (symbol-package key))
   ((%%is-id? key :pair)
    ;; id is a pair id
    (symbol-package (car key)))
   (t (error "<<symbol-package: arg ~S should be symbol or id pair" key))))

#|
(<<symbol-package '$E-PERSON)
#<The MOSS package>

(with-package :test
  (<<symbol-package '(test::$E-STUDENT . 1)))
#<The TEST package>
|#
;;;----------------------------------------------------------- <<SYMBOL-PLIST

(defun <<symbol-plist (key)
  "gets symbol-plist attached to the key, aka symbol-plist"
  ;; returns symbol plist
  (cond
   ((symbolp key)
    (symbol-plist key))
   ((%%is-id? key :pair)
    ;; id is a pair id
    (multiple-value-bind (item tag)(<<* key)
      (if tag (cadr item) nil)))
   (t (error "<<symbol-plist: arg ~S should be symbol or id pair" key))))

#|
(<<symbol-plist '($e-person . 123))
(:HELLO "hello")
|#
;;;----------------------------------------------------------- <<SYMBOL-VALUE

(defun <<symbol-value (key)
  "gets the value of the key, synonym of <<
Return:
   for pair keys 3 values: value, plist, existence flag"
  (<< key))

#|
(pprint (<<symbol-value f::_jpb))
(($TYPE (0 FAMILY::$E-PERSON)) ($ID (0 (FAMILY::$E-PERSON . 1)))
 (FAMILY::$T-PERSON-NAME (0 "Barthès")) (FAMILY::$T-PERSON-FIRST-NAME (0 "Jean-Paul" "A"))
 (FAMILY::$T-PERSON-SEX (0 "m")) (FAMILY::$T-PERSON-BIRTH-YEAR (0 1945))
 (FAMILY::$S-PERSON-HUSBAND.OF (0 (FAMILY::$E-PERSON . 2)))
 ...)
|#
;;;------------------------------------------------------------------ <<VOMIT

(defun <<vomit ()
  "prints the content of *ST*"
    (let ((ST (intern "*ST*")))
      ;; *ST* must be the symbol hash table in current package
      (unless (and (boundp ST)(hash-table-p (setq ST (symbol-value ST))))
        (error "the *ST* symbol table is missing in package ~S" 
             (package-name *package*)))
      (maphash #'(lambda (&rest ll)(print (car ll))) ST))
  :done)

#| e.g. in package MOSS
(moss::<<vomit)
($SYS . 0) 
(*ANY* . 0) 
($FN . 0) 
($CTR . 0) 
(*NONE* . 0) 
($E-PERSON . 1) 
($E-PERSON . 0) 
($E-PERSON . 2) 
($UNI . 0) 
($E-PERSON . 23) 
:DONE

(with-package :test
  (moss::<<vomit))
($E-PERSON . 555) 
($PERSON . 123) 
:DONE
|#
;;;----------------------------------------------------------------------- >>

#|
(dformat-set :>> 0)
(dformat-reset :>>)
|#

(defun >> (key value &key new-plist add-to-plist clear-plist)
  "inserts a value corresponding to the key in the application hash table ~
   or if key is a symbol sets its value to value and plist to plist.
Arguments
   key: symbol or id pair
   value: value to make the value of symbol
   new-plist (key): p-list to replace p-list of key
   add-to-plist (key): alternate list of prop value to add to p-list
   clear-plist (key): reset plist to nil
Return:
   value of key"
  (dformat :>> 0 "~%;---------- Entering >>")
  (cond
   ((symbolp key)
    (dformat :>> 0 "~%;--- key: ~S is a symbol" key)
    (set key value)
    ;; check if we want to modify p-list
    (cond
     (new-plist
      (unless (and (listp new-plist)(evenp (length new-plist)))
        (error ">> bad format for new-plist: ~S of ~S in package ~S"
          new-plist key *package*))
      ;; replace old plist
      (setf (symbol-plist key) new-plist))
     (add-to-plist
      ;; add some values to plist
      (unless (and (listp add-to-plist)(evenp (length add-to-plist)))
        (error ">> bad format for add-to-plist ~S of ~S in package ~S" 
          add-to-plist key *package*))
      (loop
        (unless add-to-plist (return))
        (setf (get key (pop add-to-plist)) (pop add-to-plist)))
      )
     (clear-plist
      (setf (symbol-plist key) nil))
     )
    )
   
   ;; here if key is a dotted pair id, we must make sure that the value is
   ;; set in the symbol table of the package of the key, which is the package
   ;; of the symbol (car key)
   ((%%is-id? key :pair)
    (dformat :>> 0 "~%;--- key: ~S is an id pair" key)
    (let ((ST (intern "*ST*" (symbol-package (car key)))))
      ;; *ST* must be the symbol hash table in current package
      (unless (and (boundp ST)(hash-table-p (setq ST (symbol-value ST))))
        (error "<<: trying to get the value associated with the key ~S, but ~
            the *ST* symbol table is missing in package ~S" 
          key (package-name (symbol-package (car key)))))

      ;; check if we want to modify plist
      (multiple-value-bind (item tag)(gethash key ST)
        (declare (ignore tag))
        (dformat :>> 0 "~%;--- item: ~S" item)
        (let ((plist (cadr item)))
          (cond
           (new-plist
            (unless (and (listp new-plist)(evenp (length new-plist)))
              (error ">> bad format for new-plist: ~S of ~S in package ~S"
                new-plist key (package-name (symbol-package (car key)))))
            ;; replace old plist
            (setq plist new-plist))
           
           (add-to-plist
            ;; add some values to plist
            (unless (and (listp add-to-plist)(evenp (length add-to-plist)))
              (error ">> bad format for add-to-plist ~S of ~S in package ~S"
                add-to-plist key (package-name (symbol-package (car key)))))
            (loop
              (unless add-to-plist (return))
              (setq plist 
                    (append `(,(pop add-to-plist) ,(pop add-to-plist)) plist))))
           
           (clear-plist
            (setq plist nil))
           )
          ;; update value
          (setf (gethash key ST) (list value plist))
          (dformat :>> 0 "~%;--- saved value: ~S" (gethash key ST))
          (dformat :>> 0 "~%;--- readback value using << : ~S" (<< key))
          ))))
   
   (t (error ">>: the key argument ~S has not the proper id format in package ~S"
        key (package-name *package*))))
  
  ;; return value when no error
  value)

#|
See elementary tests in z-moss-tests-id-pairs.list

;; executing in package family
(moss::>> '($E-PERSON . 123) '(($TYPE (0 $E-PERSON))($ID (0 ($E-PERSON . 123)))
                         ($E-PERSON-NAME (0 "Arthur"))
                         ($E-PERSON-BROTHER (0 ($E-PERSON . 155)))))
(($TYPE (0 $E-PERSON)) ($ID (0 ($E-PERSON . 123))) ($E-PERSON-NAME (0 "Arthur"))
 ($E-PERSON-BROTHER (0 ($E-PERSON . 155))))   

;; executing in package MOSS
(>> '(n1::$E-PERSON . 125) '(($TYPE (0 n1::$E-PERSON))
                             ($ID (0 (n1::$E-PERSON . 125)))
                             ($E-PERSON-NAME (0 "Arthur"))
                             ($E-PERSON-BROTHER (0 (n1::$E-PERSON . 155)))))
(($TYPE (0 NURSE-1::$E-PERSON)) ($ID (0 (NURSE-1::$E-PERSON . 125)))
 ($E-PERSON-NAME (0 "Arthur"))
 ($E-PERSON-BROTHER (0 (NURSE-1::$E-PERSON . 155))))

(>> '(test::$E-PERSON . 125) '(($TYPE (0 n1::$E-PERSON))
                               ($ID (0 (n1::$E-PERSON . 125)))
                               ($E-PERSON-NAME (0 "Arthur"))
                               ($E-PERSON-BROTHER (0 (n1::$E-PERSON . 155)))))
Error: >>: trying to set the value associated with the key
       (TEST::$E-PERSON . 125), but the *ST* symbol table is missing in package
"TEST"

(>> '(23 . 125) '(($TYPE (0 n1::$E-PERSON))($ID (0 (n1::$E-PERSON . 125)))))
Error: >>: the key argument (23 . 125) has not the proper id format in package
       "MOSS"                            
|#
;;;---------------------------------------------------------------- >>ADDPROP
;;; adding a value to a list of values associated with a property
;;; aka (push xx (get ...))

:todo
;;;---------------------------------------------------------------------- >>F

(defun >>f (id)
  "prints ids in a compact way: ($E-PERSON . 1) => \"$E-PERSON..1\""
  (cond ((%%is-id? id :pair)
         (string+ (car id) ".." (cdr id)))
        ((%%is-id? id) (string+ id))
        ((listp id)(format nil "(~{~A~^ ~})" (mapcar #'>>f id)))
        ;((listp id) (string+ (mapcar #'>>f id)))
        (t (format nil "~S" id))))

#|
(>>f '$E-PERSON.2)
"$E-PERSON.2"

(>>f '($E-PERSON.2 $E-PERSON.3))
"($E-PERSON.2 $E-PERSON.3)"

(>>f test::_tg1)
"$E-TARGET..1"

(>>f (list test::_tg1 test::_tg2 test::_tg3))
"($E-TARGET..1 $E-TARGET..2 $E-TARGET..3)"

(>>f '("a" 1 "b" 2))
"(\"a\" 1 \"b\" 2)"
|#
;;;------------------------------------------------------------- >>MAKUNBOUND

(defun >>makunbound (xx)
  "if xx is a symbol makes it unbound, if a pair id removes it from *ST*"
  (let ()
    (cond
     ((symbolp xx) (makunbound xx))
     ;; can't do that because it will zap the plist
     ;((and (listp xx)(%%is-id? xx)) (remhash xx ST) xx)
     ((%%is-id? xx)
      (multiple-value-bind (item tag) (<<* xx)
          (cond
           ((null tag)
            ;; xx is not an entry in *ST*, thus considered unbound
            xx)
           ((equal (car item) :_<unbound>)
            ;; special mark making xx unbound
            xx)
           (t
            ;; otherwise xx is an entry but not unbound
            (>> xx :_<unbound> :new-plist (cadr item))
            xx))))
     (t (error ">>makunbound: arg: ~S should be object id" xx)))))

#|
(setq aa 3)
3

(>>makunbound 'aa)
AA

AA
Error: Attempt to take the value of the unbound variable `AA'.
[condition type: UNBOUND-VARIABLE]

(>> '($e-person . 77) '(($TYPE (0 $E-PERSON)) ($ID (0 ($E-PERSON . 77))) ($E-PERSON-NAME (0 "Arthur"))
                        ($E-PERSON-BROTHER (0 ($E-PERSON . 155))))
    :new-plist '(:a 1 :b 2))
(($TYPE (0 $E-PERSON)) ($ID (0 ($E-PERSON . 77))) ($E-PERSON-NAME (0 "Arthur"))
 ($E-PERSON-BROTHER (0 ($E-PERSON . 155))))

(<< '($e-person . 77))
(($TYPE (0 $E-PERSON)) ($ID (0 ($E-PERSON . 77))) ($E-PERSON-NAME (0 "Arthur"))
 ($E-PERSON-BROTHER (0 ($E-PERSON . 155))))
(:A 1 :B 2)
T

(>>makunbound '($E-PERSON . 77))
($E-PERSON . 77)

(<<boundp '($e-person . 77))
NIL

(<< '($e-person . 77))
Error: Attempt to take the value of the unbound key '($E-PERSON . 77)'
|#
;;;---------------------------------------------------------------- >>REMPROP

(defun >>remprop (key prop &aux res)
  "removes prop from the symbol plist, aka remprop"
  (cond
   ((symbolp key)
    (remprop key prop))
   (t
    (multiple-value-bind (item tag) (<<* key)
      ;; if tag is nil then entry does not exist enter plist anyway
      (setq res (remf (cadr item) prop))
      ;; record updated plist
      (>> key (if tag (car item) :_<unbound>) :new-plist (cadr item))
      res))))

#|
(<< '($e-person . 123))
123
(:A 1)
T

(>>setprop '($e-person . 123) 1 :a)
1

(>>setprop '($e-person . 123) 2 :b)


(<< '($e-person . 123))
123
(:B 2 :A 1)
T

(>>remprop '($e-person . 123) :a)
T

(<< '($e-person . 123))
123
(:B 1)
T

(>>remprop '($e-person . 123) :b)
T

(<< '($e-person . 123))
123
NIL
T

(<< '($e-person . 123))
(($TYPE (0 $E-PERSON)) ($ID (0 ($E-PERSON . 123))) ($E-PERSON-NAME (0 "Arthur"))
 ($E-PERSON-BROTHER (0 ($E-PERSON . 155))))
(:HELLO "hello")
T

(>>remprop '($e-person . 123) :test)
NIL ; could not erase because was not there
|#
;;;---------------------------------------------------------------- >>SETPROP

(defun >>setprop (key value prop &aux plist)
  "sets the symbol plist with prop and value, aka (setf (get ...) value)"
  (cond
   ((symbolp key)
    (setf (get key prop) value))
   (t
    (multiple-value-bind (item tag)(<<* key)
      ;; if tag is nil then entry does not exist enter plist anyway
      (setq plist (cadr item))
      (cond
       ;; entry in table and non null plist
       (plist
        ;; side effect is to modify plist, but returns value
        (setf (getf plist prop) value))
       (t
        ;; otherwise make list
        (setq plist (list prop value))))
      (>> key (if tag (car item) :_<unbound>) :new-plist plist)
      value))))

#|
(>>setprop '($e-person . 123) "test text" :test)
"test text"

(<< '($e-person . 123))
#:<UNBOUND>
(:TEST "test text")
T

(>>setprop '($e-person . 123) "hello" :hello)
"hello"

(<< '($e-person . 123))
(($TYPE (0 $E-PERSON)) ($ID (0 ($E-PERSON . 123))) ($E-PERSON-NAME (0 "Arthur"))
 ($E-PERSON-BROTHER (0 ($E-PERSON . 155))))
(:HELLO "hello" :TEST "test text")
T
|#
;;;==========================================================================
;;;                       FUNCTIONS FOR A-LISTS
;;;==========================================================================

;;;------------------------------------------------------------------- ALISTP

(defUn alistp (ll)
  "test if arg is an a-list."
  (and (listp ll)(every #'listp ll)))

#|
(alistp nil)
T
(alistp '((1 A)(B 2) ("C" 3)))
T
(alistp '((1 A)(B 2) ("C" 3) D))
NIL
(alistp 22)
NIL
|#
;;;---------------------------------------------------------------- ALIST-ADD

(defun alist-add (ll tag val)
  "basic function for adding a single value to an a-list.
Arguments:
   ll: a-list to modify
   tag: any lisp expr
   values: any lisp expr
Return:
   the modified a-list."
  (cond
   ((null ll) `((,tag ,val)))
   ((equal+ (caar ll) tag) 
    (cons (cons (caar ll) (append (cdar ll)(list val))) (cdr ll)))
   (t (cons (car ll)(alist-add (cdr ll) tag val)))))

#|
(alist-add '((1 A)(B 2) ("C" 3))  "c" "cc")
((1 A) (B 2) ("C" 3 "cc"))
(alist-add '((1 A)(B 2) ("C" 3))  'd "dd")
((1 A) (B 2) ("C" 3) (D "dd"))
(alist-add '((1 A)(B 2) ("C" 3))  1 'AA)
((1 A AA) (B 2) ("C" 3))
|#
;;;--------------------------------------------------------- ALIST-ADD-VALUES

(defun alist-add-values (ll tag values)
  "basic function for adding a list of values to an a-list.
Arguments:
   ll: a-list to modify
   tag: any lisp expr
   values: a list of any lisp expr
Return:
   the modified a-list."
  (cond
   ((null ll) (list (cons tag values)))
   ((equal+ (caar ll) tag) 
    (cons (cons (caar ll) (append (cdar ll) values)) (cdr ll)))
   (t (cons (car ll)(alist-add-values (cdr ll) tag values)))))

#|
(alist-add-values '((1 A)(B 2) ("C" 3))  "c" '("cc" ccc))
((1 A) (B 2) ("C" 3 "cc" CCC))
(alist-add-values '((1 A)(B 2) ("C" 3))  'd '("dd" dd))
((1 A) (B 2) ("C" 3) (D "dd" DD))
(alist-add-values '((1 A)(B 2) ("C" 3))  1 '())
((1 A AA) (B 2) ("C" 3))
(alist-add-values nil :a '(1 2))
((:A 1 2))
|#
;;;---------------------------------------------------------------- ALIST-REM

(defun alist-rem (ll tag)
  (remove tag ll :key #'car :test #'equal+))

#|
(alist-rem '((1 A)(B 2) ("C" 3))  "c")
((1 A)(B 2))

(alist-rem '((1 A)(B 2) ("C" 3))  1)
((B 2) ("C" 3))
|#
;;;------------------------------------------------------------ ALIST-REM-VAL

(defun alist-rem-val (ll tag val &aux temp)
  (cond
   ((null ll) nil)
   ((and (equal+ (caar ll) tag)(member val (cdar ll) :test #'equal+))
    (setq temp (remove val (cdar ll) :test #'equal+))
    (if temp (cons (cons (caar ll) temp)(cdr ll))
      (cdr ll)))
   (t (cons (car ll) (alist-rem-val (cdr ll) tag val)))))
    
#|
(alist-rem-val '((1 A)(B 2) ("C" 3))  "c" 3)
((1 A) (B 2))

(alist-rem-val '((1 A)(B 2) ("C" 3))  "c" 4)
((1 A) (B 2) ("C" 3))

(alist-rem-val '((1 A)(B "b" 2 22) ("C" 3))  'b "b")
((1 A) (B 2 22) ("C" 3))
|#
;;;---------------------------------------------------------------- ALIST-SET

(defun alist-set (ll tag val) 
  "replaces value associated to a specific property in an a-list"
  (cond ((null ll) (list (list tag val)))
        ((equal+ tag (caar ll))
         (cons (list tag val)(cdr ll)))
        (t (cons (car ll) (alist-set (cdr ll) tag val)))))

#|
(alist-set '((1 A)(B 2) ("C" 3))  "c" 333)
((1 A) (B 2) ("c" 333))

(alist-set '((1 A)(B 2) ("C" 3))  :d 4)
((1 A) (B 2) ("C" 3) (:D 4))

(alist-set '((1 A)(B 2) ("C" 3))  1 :A)
((1 :A) (B 2) ("C" 3))

(alist-set nil  :d 4)
((:D 4))
|#
;;;--------------------------------------------------------- ALIST-SET-VALUES

(defun alist-set-values (ll tag values) 
  "replaces value associated to a specific property in an a-list"
  (cond ((null ll) (list (cons tag values)))
        ((equal+ tag (caar ll))
         (cons (cons tag values)(cdr ll)))
        (t (cons (car ll) (alist-set-values (cdr ll) tag values)))))

#|
(alist-set-values '((1 A)(B 2) ("C" 3))  "c" '(333 3333))
((1 A) (B 2) ("c" 333 3333))

(alist-set-values '((1 A)(B 2) ("C" 3))  :d '(4))
((1 A) (B 2) ("C" 3) (:D 4))

(alist-set-values '((1 A)(B 2) ("C" 3))  1 '(:A :aa))
((1 :A :AA) (B 2) ("C" 3))

(alist-set-values nil  :d '(4 "dd"))
((:D 4 "dd"))

(alist-set-values'((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("cc" 3) ("dd" 4) ("ee" 5)))
                   'b '(("cc" 3) ("ee" 5)))
((A ("aa" 1) ("bb" 2) ("cc" 3)) (B ("cc" 3) ("ee" 5)))
|#
;;;==========================================================================
;;;                            MISCELLANEOUS
;;;==========================================================================

;;;--------------------------------------------------------------- ALL-ALIKE?

(defUn all-alike? (ll)
  "T if all elements of the list are the same. Uses equal+ to take care of 
   strings."
  ;; or less than2 args
  (or (or (null ll) (null (cdr ll)))
      ;; if 2 args or more should be equal
      (and (equal+ (car ll) (cadr ll)) (all-alike? (cdr ll)))))

#|
(all-alike? ())
T

(all-alike? '(1))
T

(all-alike? '(1 2))
NIL

(all-alike? '("a" "a" "A"))
T

(all-alike? '("a" "a" 1))
NIL

(all-alike? '("a" "a" "a"))
T
|#
;;;----------------------------------------------------------- ALL-DIFFERENT?

(defUn all-different? (ll)
  "T if all elements of the list are different"
  ;; should have at least 2 args
  (cond
   ((or (null ll)(null (cdr ll))))
   ;; more than 2 args
   ((member+ (car ll) (cdr ll)) nil)
   ((null (cddr ll)) t)
   ((all-different? (cdr ll)))))

#|
(all-different? '())
T

(all-different? '(1 1))
NIL

(all-different? '(1 2))
T

(all-different? '(1 2 1))
NIL

(all-different? '(1 2 3))
T
|#
#|
;;;-------------------------------------------------- DEFGENERIC DISPLAY-TEXT
;;; defined to keep the compiler quiet

(defUn display-text (&rest ll) ll)

|#
;;;------------------------------------------------------------------- FIRSTN

(defun firstn (ll nn)
         (cond ((or (null ll)(<= nn 0)) nil)
               (t (cons (car ll) (firstn (cdr ll) (1- nn))))))

#|
(firstn '(1 2 3 4 5) 3)
(1 2 3)
|#
;;;--------------------------------------------------------------------- get-field

(defun get-field (field-name alist)
   (cdr (assoc field-name alist :test #'string-equal)))

#|
(get-field "titre" 
           '(("name" . "sais pas")("titre" . "Le bon choix")("reste" . "junk")))
"Le bon choix"
|#
#|
;;;-------------------------------------------------- DEFGENERIC DISPLAY-TEXT
;;; defined to keep the compiler quiet

(defUn display-text (&rest ll) ll)
|#

;;;------------------------------------------------------------------ DFORMAT

(defun dformat (tag level fstring &rest ll)
  "allows selective tracing by inserting a keyword tag into the (debugging *omas*)~
   list of debugging tags (similar to *features*).
Arguments:
   tags: a tag or list of tags for inserting a format
   level: a level, the higher the more detailed (default is 0)
   fstring: format string in the call
   ll (rest): arguments to the format string
Return:
   nil"
  (declare (special *debug-io*))
  (if (and (get tag :trace)(<= level (get tag :trace-level)))
    (apply #'format *debug-io* (string+ "~%;-- " fstring) ll))
  nil)

#|
(dformat-set :tag 1)
1

(dformat :tag 0 "bonjour: ~A" "Princesse")
;--- bonjour: Princesse
NIL

(dformat :tag 2 "bonjour: ~A" "Princesse")
NIL
|#
;;;------------------------------------------------------------------------ DRFORMAT

(defun drformat (tag level fstring &rest ll)
  "like dformat but raw: does not insert ~%;-- infront"
  (cond
   ((and (symbolp tag)(get tag :trace)(<= level (get tag :trace-level)))
    (apply #'format *debug-io* fstring ll))
   ;; if a list check items in turn
   ((listp tag)
    (dolist (item tag)
      (if (and (get item :trace)(<= level (get item :trace-level)))
          (apply #'format *debug-io* fstring ll))))
   )
  nil)

#|
(drformat :tag 1 "~%;==== voilà: ~S" "Albert")
;==== voilà: "Albert"
NIL
|#
;;;-------------------------------------------------------------- DFORMAT-SET
;;; defined in moss-load

;;;(defun dformat-set (tag level)
;;;  (setf (get tag :trace) t)
;;;  (setf (get tag :trace-level) level)
;;;  )

;;;----------------------------------------------------------------- DWFORMAT
;;; used in moss-query.lisp

(defun dwformat (tag level &rest ll)
  "allows selective tracing by inserting a keyword tag into the (debugging *omas*)~
   list of debugging tags (similar to *features*). Waits until each message for ~
   a character to be inputted to resume execution.
Arguments:
   tags: a tag or list of tags for inserting a format
   level: a level, the higher the more detailed (default is 0)
Return:
   nil"
  (declare (special *debug-io*))
  (when (and (get tag :trace)(<= level (get tag :trace-level)))
    (apply #'format *debug-io* ll)
    (print "Type any char to continue: " *debug-io*)
    (read-char *debug-io*))
  nil)

;;;------------------------------------------------------------------- EQUAL+
;;; Extends equality to strings and MLNs (a string is an not an MLN)
;;; Two MLN are equal if
;;; - they are both strings and equal
;;; - if language L is specifies and not :all
;;;   - if one is a string and is present in the synonyms of the other in L
;;;   - if one is a string and is present in the other's unknown language
;;;   - if the two sets of synonyms for L share some
;;;   - if the synonyms for L in one share those of :unknown 
;;;   - if the two sets for unknown share some synonyms
;;; - if language is :all
;;;   - if all the synonyms of any language for both share some
;;; - if language is nil
;;;   - if *language* is unbound or equal to :unknown (NOT TRUE)
;;;     - if both sets of synonyms for a language are intersecting
;;;     - if a set of synonyms for a language is intersecting with the unknown 
;;;       set of the other
;;;   - if *language* is :all (same as when language is :all) not realistic!
;;;   - if *language* is a legal language
;;;     - if both sets of synonyms for *language* are intersecting
;;;     - if a set of synonyms for *language* is intersecting with the unknown 
;;;       set of the other

(defun equal+ (aa bb &key language same-case)
  "test if aa and bb are same, generalize to strings using string-equal and MLNs
Arguments:
   aa: first arg
   bb: second arg
   language: if aa and bb are MLNs restricts the language, unless it is :all
   same-case (key): if t, strings must be of the same case"
  (cond
   ((and (stringp aa)(stringp bb)same-case)
    (string= aa bb))
   ((and (stringp aa)(stringp bb))
    (string-equal aa bb))
   ((equal aa bb))
   ((and (or (stringp aa)(mln::mln? aa))(or (stringp bb)(mln::mln? bb)))
    (mln::mln-equal aa bb :language language)) ; jpb 1406
   ))

#|
(equal+ nil nil)
T
(equal+ 2 2)
T
(equal+ '(a b) '(a b))
T
(equal+ "albert" "ALBERT")
T
(equal+ "albert" "ALBERT" :same-case t)
NIL
(equal+ 3 "D")
NIL
(let ((*language* :fr))
 (equal+ "ALBERT" '(:en "George; Arthur" :fr "Jean; Albert")))
("Albert")

(let ((*language* :en))
  (equal+ "ALBERT" '(:en "George; Arthur" :fr "Jean; Albert")))
NIL

(let ((*language* :en))
  (equal+ "ALBERT" '(:en "George; Arthur" :fr "Jean; Albert")
          :language :all))
("ALBERT")

(let ((*language* :en))
  (equal+ "ALBERT" '(:en "George; Arthur" :fr "Jean; Albert")
          :language :fr))
("ALBERT")

(let ((*language* :en))
  (equal+ '(:en "Allan" :fr "albert") '(:en "George; Arthur" :fr "Jean; Albert")
          :language :en))
T ; language context does not matter for comparing 2 mlns
|#
;;;----------------------------------------------------------- INTERSECT-SETS
;;; same as a function taking a list

(defUn intersect-sets (list)
  (if (null (cdr list)) (car list)
      (intersection (car list)(intersect-sets (cdr list)))))

#|
(intersect-sets '((1 2 3 a B c)(2 3 a b)(1 2 b c)))
(B 2)
|#
;;;------------------------------------------------------- IS-VALID-LISP-EXPR?

(defUn is-valid-lisp-expr? (text)
  "checks if the input string can be read as a lisp expression. Brute force."
  (when (stringp text)
    (let (test errno)
      (multiple-value-setq (test errno)
        (ignore-errors
         (read-from-string text)))
      (if (or test (not errno)) test))))

#|
(mw-is-valid-lisp-expr? "t")
T
(mw-is-valid-lisp-expr? "")
NIL
(mw-is-valid-lisp-expr? "(+ 2 3)")
T
(mw-is-valid-lisp-expr? "(+ 2 ")
NIL
|#

;;;------------------------------------------------------------------ LAMBDAP
;;; Used to check for bounded method function names

(defUn lambdap (symbol)
  "check if symbol is a lambda list"
  (and (symbolp symbol)
       (boundp symbol)
       (listp (symbol-value symbol))
       (eql (car (symbol-value symbol)) 'lambda)
       t))

#|
(setq ff '(lambda (xx) (list xx)))
(LAMBDA (XX) (LIST XX))

(lambdap 'ff)
T

(lambdap 'tt)
NIL
|#
;;;---------------------------------------------------------- LIST-DIFFERENCE
;;; set-difference does not guaranty that order be preserved 
;;; used in moss-service.lisp

(defun list-difference (l1 l2 &optional strict)
  "remove from l1 all elements appearing in l2 preserving the order in l1
Arguments:
   l1: list
   l2: list
   strict (opt): if non nil, remove only 1 value"
  (cond
   ((null l2) l1)
   ((list-difference 
     (if strict 
         (remove (car l2) l1 :test #'equal+ :count 1)
       (remove (car l2) l1 :test #'equal+))
     (cdr l2)
     strict))))

#|
(list-difference nil nil)
NIL

(list-difference '(1 a "b" 2 3 4) nil)
(1 A "b" 2 3 4)

(list-difference '(1 a "b" 2 4 3 4) '(4))
(1 A "b" 2 3)

(list-difference '(1 a "b" 2 4 3 4) '(4 2 "b"))
(1 A 3)

(list-difference '(1 a "b" 2 4 3 4) '(1 a "b"))
(2 4 3 4)

(list-difference '(1 a "b" nil 3 4) '(4 5))
(1 A "b" NIL 3)

(list-difference '("a" "a" "a" 2 2) '("a" "a" 2 3) :strict)
("a" 2)
|#
;;;?----------------------------------------------------------------- LOB-GET
;;; lob-get is defined as a version of getv that checks for the nature of the
;;; object. It eventually loads objects from the secondary storage using %PDM?
;;; used in moss-service.lisp

(defUn lob-get (obj-id prop-id)
  "returns the list of all values attached to a given property."
  (when (%pdm? obj-id)
    (if (symbolp obj-id)
        (getv prop-id (symbol-value obj-id))
      ;; if prop-id is a pair id
      (getv prop-id (<< obj-id)))))

#|
(lob-get '$ENT '$PT)
((0 $ENAM $RDX $DOCT $ONEOF $XNB $TMBT))
T

(getv '$PT $ENT)

(>> '($e-test . 22) '(($TYPE (0 $E-PERSON)) ($T-PERSON-NAME (0 "Barthès"))) 
    '(:pdm t))
(lob-get '($e-test . 22) '$T-person-name)
((0 "Barthès"))
T
|#
;;;------------------------------------------------------------------ MEMBER+

(defun member+ (xx ll)
  (member xx ll :test #'equal+))

#|
(member+ "a" '(& 2 C "A" d v "e" 23))
("A" D V "e" 23)

(member+ '((:fr "jean" "rené")(:en "john")) '( a b c 1 2 ((:fr "rené"))))
(((:FR "rené")))
|#
;;;--------------------------------------------------------------------- MEXP
;;; mexp expands completely an s-expr to the last bits. It is useful for
;;; checking the code that will be produced. E.g. using incf is not a good
;;; idea, since it expands to a let* and creates a temporary variable!

(defUn mexp (form)
  (if form (if (listp form)
             (macroexpand (cons (mexp (car form))(mexp (cdr form))))form)))

;;; The following functions are used to make entry-points for the various 
;;; object types of MOSS

;;;---------------------------------------------------------------- MAKE-NAME
;;; make-name is defined for compatibility with UTC-Lisp; It creates a string
;;; uppercasing the names (NO). It takes any number of arguments.
;;; Could also use (apply #'string+ arg-list)
;;; used by many functions

(defUn make-name (&rest arg-list)
  (format nil "~{~A~}" arg-list))

#|
(make-name 1. 'albert "a" 23 " ans ? Paris")
"1ALBERTa23 ans ? Paris"

(string+ 1. 'albert "a" 23 " ans ? Paris")
"1ALBERTa23 ans ? Paris"
|#

;;; next line can be used to print something like (aa 47 bb 47 cc)) where
;;; *string-hyphen* is 47 "/" i.e. is non nil. => AA/BB/CC
;;; should not use mapcan (buggy)
;;; (butlast (mapcan #'(lambda (xx) (list xx *string-hyphen*))))

#|
(format nil "~{~S~#[~;~:;~C~]~}" '(aa  47 bb  47 cc ))
"AA/BB/CC"
(apply #'make-name '(aa  47 bb  47 cc ))
"AA47BB47CC"
|#
;;;-------------------------------------------------------- MAKE-VALUE-STRING
;;; make-value-string is currently the same as make-entry-point but returns
;;; a string rather than an interned symbol. However, the make-entry-point
;;; could use an intermark strategy for example,
;;;    " Le  vilain petit   canard    "  => VILPC
;;; this is a sort of hash code with possible collisions however.

;;;********** JPB1507 I don't touch this function but it does not do what it
;;; advertises!

(defUn make-value-string (value &optional (interchar '#\.))
  "Takes an input string, removing all leading and trailing blanks, replacing ~
   all substrings of blanks and simple quote with a single underscore, and ~
   building a new string capitalizing all letters. The function is used ~
   to normalize values for comparing a value with an external one while ~
   querying the database.  
      French accentuated letter are replaced with unaccentuated capitals."
  (let* ((input-string (if (stringp value) value
                         (format nil "~S" value)))
         (work-list (map 'list #'(lambda(x) x) 
                      ;; remove leading and trailing blanks
                      (string-trim '(#\space) input-string))))
    (string-upcase   ; we normalize entry-point to capital letters
     (map
         'string
       #'(lambda(x) x) 
       ;; this mapcon removes the blocks of useless spaces 
       (mapcon 
           #'(lambda(x) 
               (cond 
                ;; if we have 2 successive blanks we remove one
                ((and (cdr x)(eq (car x) '#\space)
                      (eq (cadr x) '#\space)) 
                 nil)
                ;; if we have a single blank we replace it with interchar
                ((eq (car x) '#\space) (copy-list (list interchar)))
                ;; otherwise we simply list the char
                (t (list (car x)))))
         work-list)))))

#|
(make-value-string " Le  vilain petit   canard    ")
"LE.VILAIN.PETIT.CANARD"

(make-value-string "un jour d'été d'aujourd'hui à Oïgour")
"UN.JOUR.D'ÉTÉ.D'AUJOURD'HUI.À.OÏGOUR"

(make-value-string "un jour d'été d'aujourd'hui à Oïgour" #\-)
"UN-JOUR-D'ÉTÉ-D'AUJOURD'HUI-À-OÏGOUR"
;;;compare with

(%string-norm "un jour d' été  d'aujourd'hui à  Oïgour")
"UN-JOUR-D'-ÉTÉ-D'AUJOURD'HUI-À-OÏGOUR"
(intern *)
|UN-JOUR-D'-ÉTÉ-D'AUJOURD'HUI-À-OÏGOUR|
|#
;;;------------------------------------------------------------- MAPPEARS-IN?

(defUn mappears-in? (symbol expr)
  "used by mref"
  (cond ((null expr) nil)
        ((equal symbol expr) t)
        ((listp expr)
         (or (mappears-in? symbol (car expr))
             (mappears-in? symbol (cdr expr))))))

;;;------------------------------------------------------------------ MFORMAT
;;; printing in MOSS can be done in different ways.
;;; when MOSS is used in stand alone, then we have 2 cases:
;;;  - there is no MOSS window, we print into the listener or *debug-io* stream
;;;  - there is a MOSS wndow and we use the display-text method of the MOSS
;;;    window to print the text
;;; the mformat function is a function whose output depends on the value of 
;;; the global variable *moss-output*
;;; one problem however occurs when we want to print into another channel, e.g.
;;; when using OMAS into the assistant window. In that case we cannot use
;;; mformat but must use the conversation object.
;;; consequently all printing should go throgh the conversation object, which
;;; means that the conversation object should be initialized at the very beginning

(defUn mformat (control-string &rest args)
  "prints to the output specified by *moss-output*. Can be a stream or a pane.
   First time around, if *moss-output but moss-window exists, the output is ~
   redirected to moss-windowanswer pane.
Arguments:
   same as format except for stream
Returns:
   NIL, same as format."
  (declare (special *moss-output* *moss-output* *mwap*))
  #+nodgui
  (unless *moss-output*
    (nodgui::ask-OKcancel (apply #'format nil control-string args))
    (return-from mformat))
  ;; when moss-window is up, it has priority over standard output
  (when (and (eq t *moss-output*) *moss-window*)
    (setq *moss-output* *moss-window*))
  ;; redirect output as specified; we assume here that the interface is NODGUI
  (if (eql *moss-output* t)
      (apply #'format t control-string args)
      ;; we assume that *moss-output* refer to a Lisp object, presumably a pane,
      ;; and that the object class has a display-text method
      (display-text *moss-output* (apply #'format nil control-string args))))

#|
(mformat "~%test ~S" 1)
test 1
NIL
(mformat "~%test ~S~%test ~S" 2 3)
test 2
test 3
NIL
|#
;;;============================ Function to get references ========================

;;; the following function traces in what function a specific symbol appears
;;; by examining the code of the function in the corresponding file.
;;; in fact one should look only through the files loaded by asdf

(defUn mref (symbol &optional show-file)
  "traces in what function a specific symbol appears by examining the code of the ~
   function in the corresponding file."
  (let (expr)
    (dolist (filename *moss-files*)
      (with-open-file 
        (ss (merge-pathnames ".lisp" filename))
        ;; if wanted, we print the path of the file we are checking
        (when show-file
          (print (merge-pathnames ".lisp" filename)))
        (loop
          (setq expr (read ss  nil :end))
          (when (eql :end expr) (return :done))
          (when (and (listp expr)(member (car expr)'(defUn macro defmethod))
                     (mappears-in? symbol expr))
            (print (cadr expr))))))
    :done))

;;;-------------------------------------------------------- NALIST-REPLACE-PV
;;; Put here for compatibility with UTC-Lisp. The function replaces 
;;; destructively a sublist within an a-list
;;; used in moss-service.lisp

(defUn nalist-replace-pv (obj-id prop-id value &aux obj-l)
  "replaces destructively a sublist within an a-list. If obj-id has a value ~
   then it must be an a-list (no check done).
Arguments:
   obj-id: a symbol or a dotted id
   prop-id: a symbol
   value: a list of values replacing the current values associated with prop-id
Return:
   obj-id"
  (cond
   ((and (setq obj-l (<< obj-id)) ; get obj-id "value" (must be alist or nil)
         (assoc prop-id obj-l)) ; check if property is there
    ;; if the value of obj-id is an a-list, then update value
    (setf (cdr (assoc prop-id obj-l)) value)
    ;; save new value (as a value of obj-id or into the hash table)
    (>> obj-id obj-l)
    )
   ((and obj-l (null (assoc prop-id obj-l)))
    ;; otherwise if "value" exist but prop prop not in, add the pair to the a-list
    (>> obj-id (append obj-l (list (cons prop-id value))))
    )
   ((null obj-l)
    ;; otherwise when value is nil, create a new a-list
    (>> obj-id (list (cons prop-id value)))
    )
   (t
    (error "nalist-replace-pv: bad arguments")))
  ;; return name of modified list
  obj-id)

#|
(setq ll nil)
(nalist-replace-pv 'll 'D '(5 6))
ll
((D 5 6))

(setq ll '((A 1)(B 2) (C 3)))
(nalist-replace-pv 'll 'B '(5 6))
ll
((A 1)(B 5 6) (C 3))

(nalist-replace-pv 'll 'D '(5 6))
ll
((A 1)(B 5 6) (C 3)(D 5 6))

;; execute in package N1
(MOSS::NALIST-REPLACE-PV '($E-PERSON . 156) '$S-PATIENT '(($E-PERSON . 155)))
($E-PERSON . 156)
(moss::<< '($E-PERSON . 156))
(($S-PATIENT ($E-PERSON . 155)))
NIL
T

;; execute in the MOSS package
(nalist-replace-pv '(n1::$e-person . 156) '$s-patient '(($e-person . 1)))
(N1::$E-PERSON . 156)

(<< '(n1::$E-PERSON . 156))
(($S-PATIENT ($E-PERSON . 155)) (MOSS::$S-PATIENT (MOSS::$E-PERSON . 1)))
NIL
T
;; property and value belonged to the MOSS package...

(nalist-replace-pv '(n1::$e-person . 156) 'n1::$s-patient '((n1::$e-person . 3)))
(N1::$E-PERSON . 156)
(<< '(n1::$E-PERSON . 156))
((N1::$S-PATIENT (N1::$E-PERSON . 3)) ($S-PATIENT ($E-PERSON . 1)))
NIL
T

|#
;;;--------------------------------------------------------------- NPUTV-LAST
;;; nputv-last is defined for compatibility with UTC-Lisp. It adds a value
;;; to the list of values associated with a property in an a-list at the
;;; last position. Destructive function.
;;; (nputv-last 'a 3 '((a 1 2)(b 5 ))) -> ((a 1 2 3)(b 5 ))
;;; used in moss-service.lisp

(defUn nputv-last (prop val ll)
  "nputv-last is defined for compatibility with UTC-Lisp. It adds a value
   to the list of values associated with a property in an a-list at the
   last position. Destructive function.
Arguments:
   prop: a symbol
   val: any lisp expr
   ll: an a-list
Return:
   the value of the modified list"
  (if (assoc prop ll)
    (setf (cdr (last (assoc prop ll))) (list val))
    (if ll 
        (setf (cdr (last ll)) (list (list prop val)))
      (error "*** Can't do an nputv-last on a null list")))
  ll)

#|
(nputv-last 'a 'aaa '((b bb)(c cc)))
((B BB) (C CC) (A AAA))

(nputv-last 'b 'bbb '((b bb)(c cc)))
((B BB BBB) (C CC))

(nputv-last 'a 'aaa 'nil)
Error: *** Can't do an nputv-last on a null list
|#
;;;-------------------------------------------------------------------- NTH-INSERT

(defun nth-insert (nn item ll)
  "insert the item at position nn, in front if nn is negative, at the end if nn 
   is greater then the length of the list"
  (cond
   ((and (null item)(null ll))
    (error "can't insert a null item into a null list"))
   ((<= nn 0) (cons item ll))
   ((null ll) (list item))
   (t (cons (car ll) (nth-insert (1- nn) item (cdr ll))))))

#|
(setq ll '(0 1 2 3 4))
(nth-insert 3 99 nil)
(99)
(nth-insert -3 99 nil)
(99)
(nth-insert -3 99 ll)
(99 0 1 2 3 4)
(nth-insert 2 99 ll)
(0 1 99 2 3 4)
(nth-insert 10 99 ll)
(0 1 2 3 4 99)
(nth-insert 3 nil nil)
> Error: can't insert a null item into a null list
|#
;;;--------------------------------------------------------------------- NTH-MOVE

(defun nth-move (nn ll delta)
  "moves the nth item delta places forward it delta is positive, backward if delta
   is negative up to the end of the list or to the beginning"
  (let (item)
    ;; get the item at position n
    (setq item (nth nn ll))
    ;; insert it at position n+delta
    (setq ll (nth-insert (if (< delta 0)(+ nn delta) (+ nn delta 1)) item ll))
    ;; remove item at position n if delta is positive, at n+1 if delta is negative
    (nth-remove (if (> delta 0) nn (1+ nn)) ll)
  ))
   
#|
(setq ll '(0 1 2 3 4))
(nth-move 1 nil 2)
> Error: can't insert a null item into a null list
(nth-move 1 nil -2)
> Error: can't insert a null item into a null list
(nth-move 1 ll 1)
(0 2 1 3 4)
(nth-move 2 ll -1)
(0 2 1 3 4)
(nth-move 1 ll 25)
(0 2 3 4 1)
(nth-move 1 ll -25)
(1 0 2 3 4)
(nth-move 3 ll -2)
(0 3 1 2 4)
(nth-move 1 ll 2)
(0 2 3 1 4)
|#
;;;-------------------------------------------------------------------- NTH-REMOVE

(defun nth-remove (nn ll)
    (cond ((null ll) nil)
          ((<= nn 0) (cdr ll))
          (t (cons (car ll) (nth-remove (1- nn)(cdr ll))))))

;;;------------------------------------------------------------------- NTH-REPLACE

(defun nth-replace (nn item ll)
    (cond ((null ll) (list item))
          ((<= nn 0) (cons item (cdr ll)))
          (t (cons (car ll) (nth-replace (1- nn) item (cdr ll))))))

#|
(setq ll '(a b c d e))

(nth-replace 0 'z nil)
(Z)
(nth-replace 2 'z ll)
(A B Z D E)
|#
;;;--------------------------------------------------------------------------- PPO

(defun ppo (obj-ref)
  (pprint (<< (<<f obj-ref))))

#|
(ppo "$EPS")
(($TYPE (0 $ENT)) ($ID (0 $EPS))
 ($ENAM (0 ((:EN "MOSS-RELATION" "MOSS-STRUCTURAL-PROPERTY")))) ($RDX (0 $EPS))
 ($ENLS.OF (0 $SYS.1)) ($CTRS (0 $EPS.CTR)) ($IS-A (0 $EPR))
 ($SUC.OF (0 $PS $ESLS)) ($PS (0 $INV $SUC $ONESUCOF $SUCV $NSUC))
 ($IMS
  (0 $FN.10 $FN.14 $FN.17 $FN.20 $FN.21 $FN.24 $FN.27 $FN.44 $FN.48 $FN.54 $FN.57
   $FN.60 $FN.61 $FN.63 $FN.79 $FN.86 $FN.96 $FN.100))
 ($OMS (0 $FN.37 $FN.110)))

(with-package :members
   (ppo "$E-MEMBER..2"))
((MOSS::$TYPE (0 $E-MEMBER)) (MOSS::$ID (0 ($E-MEMBER . 2)))
 ($T-MEMBER-LOGIN (0 "barthes")) ($T-MEMBER-PASSWORD (0 "barthes"))
 ($T-MEMBER-NAME (0 "Barthès")) ($T-MEMBER-FIRST-NAME (0 "Jean-Paul"))
 ($T-MEMBER-EMAIL (0 "barthes@utc.fr")) ($S-MEMBER-SPOUSE (0 ($E-MEMBER . 3)))
 ($S-MEMBER-SPOUSE.OF (0 ($E-MEMBER . 3))))
|#
;;;----------------------------------------------------------------------- STRING+
;;; inspired by ACL string+

(defun string+ (&rest ll)
  (if (equal ll '(nil)) "" (format nil "~{~A~}" (remove nil ll))))

#|
(string+)
""
(string+ nil)
""
(string+ 1 2 3 4 5)
"12345"
(setq a "new string")
(string+ a)
"new string"
(string+ "foo" nil 'bar) ; "fooNILBAR" in ACL...
"fooBAR"
(string+ "foo" (symbol-name nil) 'bar nil)
"fooNILBAR"
|#


:EOF