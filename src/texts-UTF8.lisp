;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;===============================================================================
;;;19/08/24
;;;               T E X T S - U T F 8  (file texts-UTF8.lisp)
;;;
;;;===============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
#| This file contains some strings that must be displayed in the MOSS 
windows.
It must be saved and loaded using UTF-8 encoding. 

2014
 0306 creation
|#

(in-package :moss)

;;;===============================================================================
;;;
;;;                             TEXT MARKERS
;;;
;;;===============================================================================
;;; used by the window callbacks to detect the end of the sentence.
;language tags (:BR :CN :EN :ES :FR :IT :JP :LU :PL :UNKNOWN)

(defParameter *question-markers* 
  '(:br (#\?) :cn (#\?) :en (#\?) :es (#\?) :fr (#\?) :it (#\?) :jp (#\?)
    :lu (#\?) :pl (#\?) :unknown (#\?)))

(defParameter *full-stop-markers*
  '(:br (#\. #\!) :cn (#\. #\!) :en (#\. #\!) :es (#\. #\!) :fr (#\. #\!) 
    :it (#\. #\!) :jp (#\. #\!) :lu (#\. #\!) :pl (#\. #\!) 
    :unknown (#\. #\!) ))

;;; because of many problems with Japanese input, we specify the delimiters in
;;; a brute force manner, providing the UNICODE code
(defParameter *japanese-delimiters*
  (mapcar #'code-char
    '(65294  ; period
      12289  ; comma
      12300  ; left parent
      12301  ; right parent
      12540  ; hyphen
      12290  ; hollow period
      )))

;;;===============================================================================
;;;
;;;                             OVERVIEW WINDOW
;;;
;;;===============================================================================

;;; Agent window is in English
;;; deprecated...

(defVar *wovr-internal-format* 
    '((:en "Internal format") 
      (:fr "Format interne")))
(defVar *wovr-show-concept* 
    '((:en "Show concept") 
      (:fr "Afficher concept")))
(defVar *wovr-make-instance* 
    '((:en "MK INST") 
      (:fr "CR INST")))
(defVar *wovr-edit*
    '((:en "EDIT") 
      (:fr "ÉDITER")))
(defVar *wovr-context* 
    '((:en "CONTEXT") 
      (:fr "CONTEXTE")))

:EOF
