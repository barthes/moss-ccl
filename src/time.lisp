;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;19/1030
;;;		
;;;		T I M E - (File time.lisp)
;;;	
;;; First created September 2007 
;;;=============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

;;;	The file contains functions to extract time expressions from a list
;;;     of words.
;;;

#|
2007
 0918 Creation
 1015 adding list for the range in between predicate
2008
 0604 === v5.11.2
 0621 === v7.0.0
2009
 0209 change in time-extract-from-words
2011
 0801 adding functions for representing dates as number of days since 1/1/0
2012
 1128 adding get-day-and-time
2014
 0307 -> UTF8 encoding
2016
 0209 modified time-extract-from-words
|#

(in-package "MOSS")


;;;======================= Macros and functions ================================
;;; We define user names for various macros

;;;===================================== Globals ===============================
;;; pattern and what to do if pattern is found

;;; first arg is pattern,  second is function to apply, third is function to
;;; apply to the ?x part, and fourth is function to apply to the ?y sublist

(defParameter *time-markers*
  '((("last" "year") (make-marker-for-last-year))
    (("this" "year") (make-marker-for-this-year))
    (("year" "ago") (make-marker-for-previous-year) butlast)
    (("years" "ago") (make-marker-for-previous-year)  butlast)
    ;; French
    (("année" "dernière") (make-marker-for-last-year))
    (("cette" "année") (make-marker-for-this-year))
    (("il" "y" "a" (?* ?z) "ans") (make-marker-for-previous-year-French))
    ))

(defParameter *time-patterns-and-functions*
  '((("in") (make-predicate-for-year) nil cdr)
    (("from") (make-predicate-for-year) nil cdr)
    (("since") (make-predicate-for-since) nil cdr)
    (("before") (make-predicate-for-before) nil cdr)
    (("between") (make-predicate-for-between) nil cdddr)
    (("after") (make-predicate-for-after) nil cdr)
    ;; French
    (("de") (make-predicate-for-year) nil cdr)
    (("en") (make-predicate-for-year) nil cdr)
    (("depuis") (make-predicate-for-year) nil cdr)
    (("avant") (make-predicate-for-before) nil cdr)
    (("entre") (make-predicate-for-between) nil cdddr)
    (("après") (make-predicate-for-after) nil cdr)
    ))

;;;=============================================================================
;;;
;;;                            Functions
;;;
;;;=============================================================================

;;; we try to extract time expressions from a list of words. Since we are dealing
;;; with publications, important expressions are related to years (and possibly
;;; months, rarely days).
;;; thus we look for expressions like:
;;; - last year
;;; - two years ago
;;; - since 2004
;;; - after 2004
;;; - between 2003 and 2006
;;; - until today
;;; - until 2006
;;; - this year
;;; - before 2005
;;; we see that we have time markers (nos, this year, 2005 and prepositions like
;;; from, until, since, before, after,...
;;; prepositions may be linked to time or other concepts like space.
;;;
;;; We split the sentence into several parts, <start> <time expr> <end>

;;;------------------------------------------------------------ GET-CURRENT-YEAR

;;;(defun get-current-year ()
;;;  "returns the number of the year."
;;;  (multiple-value-bind (ss mm hh dd mo yy dw dst tz)
;;;                       (get-decoded-time)
;;;    ;; next line noop: to keep the compiler happy
;;;    (list ss mm hh dd mo dw dst tz)
;;;    yy))

#|
? (get-current-year)
2007
|#
;;;------------------------------------------------------------ GET-DAY-AND-TIME

(defun get-day-and-time ()
  (let (seconds minutes hours day month year)
    (multiple-value-setq (seconds minutes hours day month year) (get-decoded-time))
    (format nil "~S:~S:~S ~S/~S/~S" seconds minutes hours day month year)))

#|
(get-day-and-time)
"11:28:22 28/11/2012"
|#
;;;--------------------------------------------------- MAKE-MARKER-FOR-LAST-YEAR

(defUn make-marker-for-last-year (bindings)
  "returns the string containing the number of last year.
Arguments:
   none
Return:
   string with a year number."
  (declare (ignore bindings))
  (format nil "~A" (1- (get-current-year))))

#|
? (make-marker-for-last-year nil)
"2006"
|#
;;;----------------------------------------------- MAKE-MARKER-FOR-PREVIOUS-YEAR

(defUn  make-marker-for-previous-year (bindings)
  "returns the string containing the number of last year.
Arguments:
   bindings: e.g. ((?x \"from\" \"2\")  (?y \"?\"))
Return:
   string with a year number."
  (let ((nn (read-from-string (car (last (assoc '?x bindings))) nil nil)))
    (when (numberp nn)
      (format nil "~A" (- (get-current-year) nn)))))

#|
? (make-marker-for-previous-year '((?x "from" "2") (?y "?")))
"2005"
|#
;;;---------------------------------------- MAKE-MARKER-FOR-PREVIOUS-YEAR-FRENCH

(defUn  make-marker-for-previous-year-french (bindings)
  "returns the string containing the number of last year.
Arguments:
   bindings: e.g. ((?x \"from\")  (?y \"?\") (?z \"3\"))
Return:
   string with a year number."
  (let ((nn (read-from-string (cadr (assoc '?z bindings)) nil nil)))
    (when (numberp nn)
      (format nil "~A" (- (get-current-year) nn)))))

#|
? (make-marker-for-previous-year-French 
     '((?x "articles" "publiŽs") (?z "3") (?y "ans" "?")))
"2004"
|#
;;;--------------------------------------------------- MAKE-MARKER-FOR-THIS-YEAR

(defUn make-marker-for-this-year (bindings)
  "returns the string containing the number of this year.
Arguments:
   none
Return:
   string with a year number."
  (declare (ignore bindings))
  (format nil "~A" (get-current-year)))

#|
? (make-marker-for-this-year nil)
"2007"
|#
;;;---------------------------------------------------- MAKE-PREDICATE-FOR-AFTER

(defUn make-predicate-for-after (bindings)
  "builds a predicate involving the year and MOSS predicates.
Arguments:
   bindings:  e.g. ((?x \"from\" \"2\")  (?y \"?\"))
Return:
   predicate, e.g. (:LT \"2006\")."
  (let ((year (cadr (assoc '?y bindings))))
    (when (numberp (read-from-string year nil nil))
      (list :gt year))))

#|
? (make-predicate-for-after '((?x) (?y "2005" "?")))
(:LT "2005")
? (make-predicate-for-after '((?x) (?y "Albert" "?")))
NIL
|#
;;;--------------------------------------------------- MAKE-PREDICATE-FOR-BEFORE

(defUn make-predicate-for-before (bindings)
  "builds a predicate involving the year and MOSS predicates.
Arguments:
   bindings:  e.g. ((?x \"from\" \"2\")  (?y \"?\"))
Return:
   predicate, e.g. (:LT \"2006\")."
  (let ((year (cadr (assoc '?y bindings))))
    (when (numberp (read-from-string year nil nil))
      (list :lt year))))

#|
? (make-predicate-for-before '((?x) (?y "2005" "?")))
(:LT "2005")
? (make-predicate-for-before '((?x) (?y "Albert" "?")))
NIL
|#
;;;-------------------------------------------------- MAKE-PREDICATE-FOR-BETWEEN

(defUn make-predicate-for-between (bindings)
  "builds a predicate involving the year and MOSS predicates.
Arguments:
   bindings:  e.g. ((?x \"from\" \"2\")  (?y \"?\"))
Return:
   predicate, e.g. (:LT \"2006\")."
  (let* ((sentence (cdr (assoc '?y bindings)))
         (start (car sentence))
         (end (caddr sentence))
         )
    (when (and (numberp (read-from-string start nil nil))
               (numberp (read-from-string end nil nil)))
      (list :between (list start end)))))

#|
? (make-predicate-for-between '((?x) (?y "2005" "and" "2006" "?")))
(:BETWEEN "2005" "2006")
? (make-predicate-for-between '((?x) (?y "Albert" "?")))
NIL
|#
;;;---------------------------------------------------- MAKE-PREDICATE-FOR-SINCE

(defUn make-predicate-for-since (bindings)
  "builds a predicate involving the year and MOSS predicates.
Arguments:
   bindings:  e.g. ((?x \"from\" \"2\")  (?y \"?\"))
Return:
   predicate, e.g. (:LT \"2006\")."
  (let ((year (cadr (assoc '?y bindings))))
    (when (numberp (read-from-string year nil nil))
      (list :ge year))))

#|
? (make-predicate-for-since '((?x) (?y "2005" "?")))
(:GE "2005")
? (make-predicate-for-before '((?x) (?y "Albert" "?")))
NIL
|#
;;;----------------------------------------------------- MAKE-PREDICATE-FOR-YEAR

(defUn make-predicate-for-year (bb)
  "builds a predicate involving the year and MOSS predicates.
Arguments:
   bindings:  e.g. ((?x \"from\" \"2\")  (?y \"?\"))
Return:
   predicate, e.g. (:LT \"2006\")."
  (let ((year (cadr (assoc '?y bb))))
    (when (and (stringp year)(numberp (read-from-string year nil nil)))
      (list :is year))))

#|
(make-predicate-for-year '((?x) (?y "2005" "?")))
(:GE "2005")
(make-predicate-for-before '((?x) (?y "Albert" "?")))
NIL
(make-predicate-for-year '((?Y "cours" "en" "2013")(?X "ah" "thèses")))
|#
;;;----------------------------------------------------- TIME-EXTRACT-FROM-WORDS
;;; does not work if preposition is repeated in the input. E.g.
;;;  "tèses en cours en 2013" wil take first "en" and fail

(defUn time-extract-from-words (word-list)
  "tries to extract a time expression from the list of words.
Arguments:
   word-list: the list of words
Returns:
   2 values the time-expression as a list of words or nil
            the remaining word-list or the original one"
  ;; recursive test
  (unless word-list
    (return-from time-extract-from-words nil))
  
  ;; when we have a single value check if it is a number
  (when (and word-list (stringp (car word-list)) (null (cdr word-list)))
    (if (numberp (read-from-string (car word-list)))
      ;; number, we assume it is a year
      (return-from time-extract-from-words (values (cons :is word-list) nil))
      ;; not a number, failure
      (return-from time-extract-from-words (values nil word-list))))
  
  (let (bindings pred x-fcn y-fcn result rest)
    ;; normalize time markers
    (setq word-list (time-process-time-markers word-list))
    ;; for each possible pattern
    (dolist (item *time-patterns-and-functions*)
      ;; check if pattern applies to word-list
      (setq bindings (pat-match `((?* ?x) ,@(car item) (?* ?y)) word-list))
      ;; if so build the corresponding predicate
      (when bindings
        (setq pred
              (funcall (caadr item) bindings))
        ;; if OK return pred and split sentence
        (when pred
          (setq x-fcn (third item)
                y-fcn (fourth item))
          ;; return pred and split sentence
          ;; we assume a single time condition
          (return-from time-extract-from-words
            (values
             pred
             (list (if x-fcn ;  first part of the sentence
                     (funcall x-fcn (cdr (assoc '?x bindings)))
                     (cdr (assoc '?x bindings)))
                   ;; rest of the sentence
                   (if y-fcn 
                     (funcall y-fcn (cdr (assoc '?y bindings)))
                     (cdr (assoc '?y bindings))))
             )
            ))
        ;; failed on this binding (pred=NIL), try rest of the sentence
        (multiple-value-setq (result rest)
          (time-extract-from-words (cdr (assoc '?y bindings))))
        (if result (return-from time-extract-from-words (values result rest)))
        ))
    ;; nothing found
    (values nil word-list)))

#|
(time-extract-from-words '("papers" "from" "last" "year"))
(:IS "2015")
(("papers") NIL)

(time-extract-from-words 
   '("papers" "published " "since" "2" "years" "ago" "in" "French"))
(:GE "2014")
(("papers" "published ") ("in" "French"))

(time-extract-from-words 
   '("quels" "sont" "les" "articles" "publiés " "entre" "2003" "et" "2006"))
(:BETWEEN ("2003" "2006"))
(("quels" "sont" "les" "articles" "publiés ") NIL)

(time-extract-from-words '("2003"))
(:IS "2003")
NIL

(time-extract-from-words '("en2003")) ; error missing space
NIL
("en2003")
|#
;;;--------------------------------------------------- TIME-PROCESS-TIME-MARKERS

(defUn time-process-time-markers (word-list)
  "looks into a list of words for numbers or expressions indicating a year.
Replaces it with the year number.
Arguments:
   word-list: sentence as a list of strings
Return:
   modified sentence."
  (let (bindings year x-fcn y-fcn)
    ;; for each time-marker
    (dolist (marker *time-markers*)
      ;(print marker)
      ;; check if pattern applies
      (setq bindings (pat-match `((?* ?x) ,@(car marker) (?* ?y)) word-list))
      ;; if so, compute the year number
      (when bindings
        (setq year (funcall (caadr marker) bindings))
        ;; update word-list
        (setq x-fcn (third marker)
              y-fcn (fourth marker))
        ;(print x-fcn)
        (setq word-list 
              (append (if x-fcn 
                        (funcall x-fcn (cdr (assoc '?x bindings)))
                        (cdr (assoc '?x bindings)))
                      (list year) 
                      (if y-fcn 
                        (funcall y-fcn (cdr (assoc '?y bindings)))
                        (cdr (assoc '?y bindings)))))
        ))
    ;; return modified word-list
    word-list))

#|
? (time-process-time-markers '("papers" "from" "last" "year"))
("papers" "from" "2006")
? (time-process-time-markers '("papers" "from" "2" "years" "ago"))
("papers" "from" "2005")
|#

;;;=============================================================================
;;;
;;;                    REPRESENTING DATES AS NUMBER OF DAYS
;;;
;;;=============================================================================

;;; use Gregorian calendar and does not work for negative dates
;;; does not check if the date is correct

(defparameter *month-table* '(31 28 31 30 31 30 31 31 30 31 30 31))

;;;------------------------------------------------- COMPUTE-YEAR-FROM-DAY-COUNT

(defun compute-year-from-day-count (nn)
  ;; use average number of days
  (let* ((yy (floor nn 365.2422))
         ;; year not counting 1 day, i.e. 31/12 of previous year
         (check (encode-year-in-days yy)))
    ;; yy is approximate, correct if erroneous
    (if (>= check nn)(decf yy))
    yy))

;;;---------------------------------------------------------------- CONVERT-DATE

(defun convert-date (date-string)
  "takes a string like 3/5/2011 meaning May 3, 2011 to return a list of day, month ~
   and year, or NIL if syntax is wrong. No check on the validity of the date."
  (let (pos2 day month year pos)
    ;; get day
    (setq pos (position #\/ date-string))
    (unless pos (return-from convert-date))
    ;; extract anything before / and convert to number
    (setq day (read-from-string (subseq date-string 0 pos)))
    (unless (integerp day)  (return-from convert-date))
    ;; look for next slash
    (setq pos2 (position #\/ date-string :start (1+ pos)))
    (unless pos2 (return-from convert-date))
    ;; extract month
    (setq month (read-from-string (subseq date-string (1+ pos) pos2)))
    (unless (integerp month)  (return-from convert-date))
    ;; now year
    (setq year (read-from-string (subseq date-string  (1+ pos2))))
    (unless (integerp year)  (return-from convert-date))
    ;; result
    (list day month year)))
  
#|
? (moss::convert-date "2/3/2011")
(2 3 2011)
|#
;;;----------------------------------------------------------------- DAY-IN-YEAR

(defun day-in-year (day month year)
  "computes the day rank in the year given its day/month/year format, taking into ~
   account leap years."
  (let ((count 0))
    (dotimes (mm 12 mm)
      ;; take care of leap years
      (if (and (= mm 2)(leap-year? year)) (incf count))
      ;; if we are past current month 
      (if (< mm (1- month))
          ;; add the number of days of the month
          (setq count (incf count (nth mm *month-table*)))
        ;; otherwise add the number of days and return
        (return-from day-in-year (+ day count))))))

;;;--------------------------------------------------------- ENCODE-YEAR-IN-DAYS

(defun encode-year-in-days (year)
  "computes the number of days to the first day of the year not counting it"
  ;; years must take into account leap years every 4 years 365.2422
  ;; those that devide by 100 are not leap years except those that divide by 400
  (let ((cc 0))
    (dotimes (yy year cc)
      (incf cc (if (leap-year? yy) 366 365)))))

;;;--------------------------------------------------------- ENCODE-DATE-IN-DAYS

(defun encode-date-in-days (day month year)
  ;; years must take into account leap years every 4 years 365.2422
  ;; those that devide by 100 are not leap years except those that divide by 400
  (+ (encode-year-in-days year)
     ;; plus the number of days in the year
     (day-in-year day month year)
     ))

;;;-------------------------------------------------- DECODE-DATE-FROM-DAY-COUNT

(defun decode-date-from-day-count (nn)
  "returns a 3 values year month and day"
  (let ((yy (compute-year-from-day-count nn))
        days-in-year day-and-month)
    ;; get day rank in year
    (setq days-in-year (- nn (encode-year-in-days yy)))
    (if (or (< days-in-year 1)(> days-in-year 366))
        (error "bad decoding of year"))
    
    ;; get month
    (setq day-and-month
          (dotimes (mm 12)
            ;; special case for February
            (cond
             ;; if we are in a leap year and February 29 return
             ((and (= mm 1)(= days-in-year 29)(leap-year? yy))
              (return (list 29 2)))
             ((prog1 nil ;; make sure that clause fails
                (if (and (leap-year? yy) (= mm 2))(decf days-in-year))))
             ((<=  days-in-year (nth mm *month-table*))
              (return (list days-in-year (1+ mm))))
             (t (decf days-in-year (nth mm *month-table*))))))
    ;; return result
    (append day-and-month (list yy))))

;;;------------------------------------------------------------------ LEAP-YEAR?

(defun leap-year? (nn)
  (or (= (mod nn 400) 0)
      (and (= (mod nn 4) 0)
           (not (= (mod nn 100) 0)))))


;(format t "~%;*** MOSS v~A - Time loaded ***" *moss-version-number*)

:EOF 