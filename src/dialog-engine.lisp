;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;16/07/10
;;;		D I A L O G - E N G I N E (File dialog-engine.lisp)		
;;;
;;;=============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
This file contains a model of dialog engine to be used between a user and MOSS.
It was adapted from the OMASWA interface.
The dialog occurs in the :moss space.
The file was updated to run on the omas platform

HISTORY
2019
 1016 receated using the experience from the ACL approach
|#

(in-package :moss)

;;;---------------------------------------------------------------- CHECK-DIALOG

(defun check-dialog (&key debug-level)
  "test function for running dialogs from the listener"
  (let ((conversation  (car (last (access '("moss-conversation")))))
        text)
    (if debug-level
        (dformat-set :crw debug-level)
        (dformat-reset :crw))
    ;; make sure we run the conversation in the moss package
    (with-package :moss
      ;; if no converation exists, create one
      (unless conversation
        (setq conversation (start-conversation nil)))
      ;; triggers the initial execute method without changing state
      (crawler-init conversation)
      (loop
        (print '-)
        (setq text (read-line t nil nil))
        ;; quit on an empty line
        (when (equal text "")
          (return-from check-dialog :end-of-conversation))

        (process-input conversation text)
        (crawler conversation)
        ))))
#|
(moss::check-dialog)
|#
;;;----------------------------------------------------------- COMPUTE-NEW-STATE

(defun compute-new-state (conversation state-info)
  "takes the result of the =resume method and computes the transition state.
  The result may include a state, a mark :success or :failure or :return from
  an incident subconversation. In the last case info is obtainable from the
  conversation object.
Arguments:
  conversation: id of the current conversation object
  state-info: (:transition <next-state-id>)
              (:transition :failure)
              (:transition :success)
              (:return) ?
              (:subdialog <next-state-id> :success <state> :failure <state>)
Return:
  a state or nil"
  (let ()
    ;(print `("compute-new-state state-info: " ,state-info ,(car state-info)))
    (case (car state-info)
      (:transition
       (case (cadr state-info)
         ((:failure :success)
          ;; return from a subdialog, get info from conversation
          (return-from-sub-dialog conversation (cadr state-info)))
         (otherwise
          (if (%type? (cadr state-info) '$QSTE) 
              (cadr state-info)
              (error "Bad transition from =resume: state-info: ~S" state-info)))))
      ;; sometimes =resume returns (:failure) or (:success)!!!
      ((:failure :success)
       (return-from-sub-dialog conversation (car state-info)))
      (:sub-dialog
       ;; when having a transition to a subdialog, we must update conversation
       ;; e.g. (:SUB-DIALOG _PROCESS-CONVERSATION :SUCCESS _Q-MORE? :FAILURE _Q-MORE?)
       ;; return the next state
       (make-transition-to-sub-dialog conversation state-info)
       )
      (:return
       ;; return from side conversation, go back to the interrupted case
       (error "compute-new-state state-info: return from side dialog. todo.")
       ))
    ))

;(trace compute-new-state)
;(untrace compute-new-state)
;;;--------------------------------------------------------------------- CRAWLER
;;; The crawler is a function that enters a specific state with information in
;;; the FACTS base of the conversation object. It applies the rules attached to 
;;; this state (=resume method) and makes a transition to a new state. It then 
;;; applies the rules of the new state (=execute method). If the result of this
;;; last action is to wait it returns. Otherwise, it makes a further step by
;;; starting the process all over again

;;; because we might be in a subdialog, the answer to the =resume method can be
;;; :success or :failure, in which case one must pop the frame list and 
;;; retrieve the state from the poped entry

 (dformat-set :crw 0)
; (dformat-reset :crw)

(defun crawler (cvs-id)
  "Enters a node of the conversation graph, applies the rules to process the 
  contents of the conversation/FACTS base, makes a transition, then applies
  the =execute method of the new state, triggers an eventual action, and if
  the result is to wait, returns."
  (let ((state (car (has-moss-state cvs-id)))
        new-state res)
    (dformat :crw 0 "crawler: state ~S" (car (has-moss-label state)))
    ;; check the state from the conversation object, if nil, go initialize
    ;; otherwise enter a loop, the content of which is to traverse the
    ;; conversation graph one step
    (loop
      ;; apply the =resume method to the conversation/FACTS content
      (setq new-state (compute-new-state cvs-id (send state '=resume cvs-id)))
      ;; the result should be a list allowing to compute a transition state
      (unless (send new-state '=has-type? "moss-state")
        (error "Bad result when applying =resume in state ~S" new-state))
      ;; make the transition to the new state
      (send cvs-id '=replace "moss-state" (list new-state))
      (dformat :crw 0 "crawler: transition to state ~S" (car (has-moss-label new-state)))
      ;; make the transition and apply the =execute method to the new state
      (setq res (car (send new-state '=execute cvs-id)))
      (dformat :crw 0 "crawler: return from =execute res ~S" res)
      ;; the result should be either :wait or :resume
      (case res
        (:wait
         ;; if the result is :wait, then return to let the user type something
         (return-from crawler new-state))
        (:resume
         ;; otherwise, continue crawling
         (setq state new-state))
        (otherwise
         (error "crawler: bad return from execute: ~S in state ~S" res
                (car (send new-state '=summary)))))
      ) ; end loop
    ))

#|
(setq *package* (find-package :m))
(setq *moss-output* t *moss-window* nil)
 (defparameter cvs (moss::start-conversation nil))
(defun ttt ()
  (moss::process-input cvs "What is a class?")
  ;(inspect (sv cvs))
  ;(send cvs '=print-self)
  (moss::crawler cvs)
  )

? (defun tt () 
    (let ((conversation  (car (last (access '("moss-conversation")))))
          text)
      (with-package :moss
        ;; if no converation exists, create one
        (unless conversation
          (setq conversation (start-conversation nil)))
        (loop
          (print '?)
          (setq text (read-line t nil nil))
          (when (equal text "")(return-from tt :end-of-conversation))
          (moss::process-input conversation text)
          (moss::crawler conversation)))))
|#
;;;---------------------------------------------------------------- CRAWLER-INIT

(defun crawler-init (cvs-id)
  "first step of a converations: triggers the =execute method and call the ~
   crawler to continue"
  (let* ((header (car (has-moss-dialog-header cvs-id)))
         (state (car (has-moss-entry-state header))))
    (unless state (error "this conversation has no initial state"))
    ;; trigger the =execute method of the initial state
    (send state '=execute cvs-id)
    ;; make sure we continue from initial state
    (send cvs-id '=replace "moss-state" (list state))
    :done))

;;;----------------------------------------------- MAKE-TRANSITION-TO-SUB-DIALOG

(defUn make-transition-to-sub-dialog (conversation info)
  "makes a transition to the initial input state of the sub-conversation. ~
   Builds a new entry on the stack of sub-dialogs
    (<current-state> <success> <failure><current-sub-dialog-header>)
Arguments:
  conversation: current conversation id
  info: a-list, e.g.
        (:SUB-DIALOG _PROCESS-CONVERSATION :SUCCESS _Q-MORE? :FAILURE _Q-MORE?)
Return:
  the id of the new state"
  (let ((dialog-header (symbol-value (getf info :sub-dialog)))
        (current-state (car (has-moss-state conversation)))
        ;; if :failure transition is not specified, we consider it as a failure
        (failure (or (getf info :failure) :failure))
        ;; similar decision for success
        (success (or (getf info :success) :success))
        state)
    ;; check the validity of the dialog header
    (unless (%type? dialog-header '$QHDE)
      (error "bad sub-conversation dialog header: ~S in state: ~S" 
        dialog-header current-state))
    ;; OK. push an entry frame onto the frame-list
    ;;   (<suc> <fail> <current sub-dialog-header>)
    ;; we assume that the success and failure state arguments are there
    (push (list success failure (car (HAS-MOSS-SUB-DIALOG-HEADER conversation)))
          (HAS-MOSS-FRAME-LIST conversation))
    ;; record sub-conversation header
    (send conversation '=replace 'HAS-MOSS-SUB-DIALOG-HEADER (list dialog-header))
    ;; get entry state from conversation header and save it
    (setq state (car (HAS-MOSS-ENTRY-STATE dialog-header)))
    (send conversation '=replace 'HAS-MOSS-STATE (list state))
;(inspect (sv conversation))
    ;; return new state
    state
    ))

;(trace make-transition-to-sub-dialog)

;;;------------------------------------------------------ RETURN-FROM-SUB-DIALOG
;;; a frame in the frame-list stack can mention names of a state, a keyword ot
;;; a state id, depending on the call to subdialog in the dialog
;;;    '(_Q-MORE? _Q-MORE? $QHDE.1)
;;; or '(:SUCCESS $QSTE.9 $QHDE.2)

(defUn return-from-sub-dialog (conversation fail/success)
  "returns from a sub-dialog. Get the frame list from the conversation object ~
  pops it, install the new state, previous sub-dialog header and entry-state ~
  and return. If the state is a keyword, then return again.
  When the frame list is empty resets the conversation by throwing to dialog ~
  error.
Arguments:
  conversation: current conversation
  fail/success: a keyword indicating failure or success
Return:
  a state to go to or declare an error"
  (let ((frame-list (HAS-MOSS-FRAME-LIST conversation))
        state frame sub-dialog-header)
    ;; here we have a loop in case the return state is a failure/success keyword
    ;; if so we return once more until we get a state object or there is no more
    ;; level to return to, in which case we reset the dialog
    ;(print `("Entering return-from-sub-dialog"))
    (loop
      ;; if frame-list is empty, reset dialog, meaning that we restart at the
      ;; entry state of the current sub-dialog
      (unless frame-list 
        (return-from return-from-sub-dialog (restart-conversation conversation)))
      ;; otherwise get frame (<fail> <success> <sub-dialog-hdr>)
      (setq frame (pop frame-list))
      (dformat :crw 0 "Entering return-from-sub-dialog: frame ~S" frame)
      ;; update frame-list
      (setf (HAS-MOSS-FRAME-LIST conversation) frame-list)
      ;; frame looks like (success failure state goal sub-dialog-header)
      ;; return ad hoc state or keyword to set up new transition
      (setq state
            (case fail/success
              (:success (car frame)) ; eval keyword or state name (_Q-MORE?)
              (:failure (cadr frame))
              (otherwise 
               (error "bad fail/success argument when returning from sub-dialog"))))
      ;; state can be a keywors, a state-id or a state name
      (setq state
            (cond
             ((member state '(:failure :success)) state)
             ((%type? state '$QSTE) state)
             ((and (boundp state)(%type? (symbol-value state) '$QSTE))
              (symbol-value state))
             (t (error "bad state value when returning from sub-dialog"))))

      (dformat :crw 0 "return-from-sub-dialog state ~S" state)
      ;; in case we have a state, we return to this state and quit
      (when (%type? state '$QSTE)
        ;(print `("... processing state"))
        ;; if so, get the dialog header of the previous sub-dialog
        (setq sub-dialog-header (nth 2 frame))
        ;(print `("... new-sub-dialog-header" ,sub-dialog-header))
        ;; reinstall it
        (send conversation '=replace 'has-moss-sub-dialog-header sub-dialog-header)
        ;; reinstall the entry state of the previous subdialog
        (send conversation '=replace 'has-moss-initial-state 
                  (send sub-dialog-header '=get 'has-moss-entry-state))
        ;; install the new state to go to
        (send conversation '=replace 'has-moss-state (list state))
;(send conversation '=print-self)
;(break "return-from-sub-dialog")
        ;; return the state
        (return-from return-from-sub-dialog state))
      ;; if not a state continue to return to previous sub-dialog
      (setq fail/success state)
      (dformat :crw 0 "Returning one more level")
      )))

;;;---------------------------------------------------------- START-CONVERSATION
;;; this function is called when clicking on the Help button. It creates an
;;; empty conversation object to be filled when the text is processed

(defun start-conversation (conversation  &key header (input t)(output t))
  "creates a conversation object for the conversation specified by the header; ~
  if no header is specified, then use the _main-conversaation header; calls ~
  the crawler-init function to print whatever welcome message is specified. 
Arguments:
  conversation: conversation object (if nil will be created)
  header (key): header of the conversation to start (default is _main-conversation)
  input (key): input channel or window
  output (key): output channel or window
Return:
  the conversation object id"
  (let ((main _main-conversation)
        state cvs-id)
    (cond
     (conversation
      (warn "A conversation already exists")
      conversation)
     (t
      ;; create conversation object if not already there
      ;; get pointer to the main conversation header
      (unless main (error "Can't find the main conversation header"))
      ;; get entry state from the header
      (setq state (car (has-moss-entry-state main)))
      (setq cvs-id
            (moss::make-individual 
             "MOSS-CONVERSATION"
             '("MOSS-LABEL" "Conversation Object")
             `("MOSS-DIALOG-HEADER" ,(or header main)) ; toplevel dialog
             `("MOSS-SUB-DIALOG-HEADER" ,(or header main)) ; used in the dialog stack
             `("MOSS-INITIAL-STATE" ,state) ; corresponding to the subdialog header
             `("MOSS-STATE" ,state)
             `("MOSS-INPUT-WINDOW" ,input)
             `("MOSS-OUTPUT-WINDOW" ,output)
             ))
      (crawler-init cvs-id)
      cvs-id))))

;(trace start-conversation)
;(untrace start-conversation)

#|
(start-conversation nil)
$CVSE.1
|#


#|
;;;=============================================================================
;;;                            SALIENT FEATURES
;;;=============================================================================
;;; the SALIENT FEATURE mechanism consists in inserting entries in an agent stack
;;; to resolve references or find the last time a-something was mentioned

;;;------------------------------------------------- DIALOG-STATE-CRAWLER-PUT-SF
;;; this function is used with OMAS to add a salient features stack frame mark

(defun dialog-state-crawler-put-sf (conversation state)
  "adds a stack-frame micro-context mark on OMAS agent salient features queue."
  #-OMAS conversation ; does nothing without OMAS
  #+OMAS
  (let ((agent (car (%get-value conversation '$AGT))))
    ;; recover agent from conversation object
    ;; then insert mark
    (if agent 
        (omas::sf-put agent `(:micro-context ,(get-universal-time) ,state)))
    ))

#|
(dialog-state-crawler-put-sf '$CVSE.1)
((:MICRO-CONTEXT 3534182443 $QHDE.12) 
 (:END-MICRO-CONTEXT "success")
 ("postit" ("id" "P-21"))
 (:MICRO-CONTEXT 3534171765 'JEAN-PAUL::_DELETE-POSTIT-CONVERSATION))
|#
;;;--------------------------------------------- DIALOG-STATE-CRAWLER-PUT-SF-END
;;; this function is used with OMAS for inserting an end-of-micro-context mark
;;; on the salient feature queue

(defun dialog-state-crawler-put-sf-end (conversation state)
  "adds a stack frame micro-context-end mark on OMAS agent salient features ~
    queue."
  #-OMAS (list conversation state) ; does nothing without OMAS
  #+OMAS
  (let ((agent (car (%get-value conversation '$AGT))))
    ;; insert mark (state is :success or :failure)
    (if agent
        (omas::sf-put agent
                      `(:end-micro-context ,(get-universal-time) ,state))))
  )

#|
(dialog-state-crawler-put-sf-end '$CVSE.1)
((:END-MICRO-CONTEXT 3534183086)
 (:MICRO-CONTEXT 3534182443 $QHDE.12)
 (:END-MICRO-CONTEXT "success")
 ("postit" ("id" "P-21"))
 (:MICRO-CONTEXT 3534171765 'JEAN-PAUL::_DELETE-POSTIT-CONVERSATION))
|#

|#

:EOF
