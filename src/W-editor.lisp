;;;-*- Mode: Lisp; Package: "NODGUI-USER" -*-
;;;===============================================================================
;;;19/10/06
;;;		
;;;		M O S S - E D I T O R  (File W-editor.Lisp)
;;;	
;;;===============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#| This file contains all the functions to build the MOSS editor window.
Any object can be edited. Its attributes, relations and inverse links ~
    are shown. 
  Entry points associated with ~
    terminal properties can be visualized by double-clicking on any terminal ~
    property. 
  Objects connected to the displayed object can be shown by ~
    selecting the proper row. When a property is multi-valued, then ~
    a list appears. One then can select an element to brawse it in turn.

History
2019
 0825 File created as an extension to the browser window
|#
#|
2019
 10/27 Creation by reusing the browser file file
|#

#|
TODO
 - capture errors on process-query to avoid crash
 - implement the database buttons before rewriting the database code
 - implement the MOSS HELP dialog
 - complete documentation
 - explain the role of plist tags :saved and :new on the plist of edited objects
 - tile editor windows
 - hide wish window
|#

(in-package :nodgui-user)

#|
(eval-when (:compile-toplevel :load-toplevel :execute)
  (import 
   '(moss::with-package moss::with-ap moss::*application-package* moss::send 
      moss::broadcast moss::*moss-output*
      moss:nth-insert moss:nth-move moss:nth-remove moss:nth-replace)))
|#

;;;============================ MOSS function ===================================

(defUn edit (obj-id)
  ;(with-package (moss::<<symbol-package obj-id)
  (with-ap obj-id
    (make-editor-window obj-id)))

;;;============================ Service function =================================

;;;--------------------------------------------------------------- TREEVIEW-SELECT

(defmethod treeview-select ((tv treeview))
  "Get the selected items (a list of tree-item) of this treeview"
  (format-wish "senddatastrings [~a selection]" (widget-path tv))
  (read-data))

;;;=============================== Globals =======================================
;;; defined in the NODGUI-USER package

;; browser position on the screen
(defParameter *editor-left* 200)
(defParameter *editor-top* 250)
(defParameter *editor-width* 520)
;; height is computed by the system
(defparameter *list-area-height* 5 "number of rows in each pane")
;; width of the list areas
(defparameter *list-area-width* 440)
;; ad hoc scaling because can't get the width of the list area from Tk!
(defparameter *detail-area-width* (floor (/ (* *list-area-width* 45) 440)))

;;;============================= Classes =========================================

(defClass editor (toplevel) 
  ((application-package :accessor application-package :initform  nil)
   (attribute-list :accessor attribute-list :initform nil)
   (caller :accessor caller :initform nil :initarg :caller)
   (calling-window :accessor calling-window :initform nil)
   (current-entity :accessor current-entity)
   ;(current-property-info :accessor current-property-info)
   (editing-box :accessor editing-box :initform nil)
   (inverse-list :accessor inverse-list :initform nil)
   (relation-list :accessor relation-list :initform nil)
   ;; where the result of the editing att and rel panes put the values
   (result :accessor result :initform nil)
   )
  (:documentation 
   "Structure saving information used for editing a MOSS object"))

;;;------------------------------------------------------- MAKE-EDITOR-WINDOW

(defun make-editor-window (current-entity &optional caller create)
  "build the various areas to display object info.
Arguments:
  current-entity: id ob object to be displayed
  caller (opt): the window calling the editor
  create (opt): if present, means that current-entity is a class id and we are
                asked to create a new object of that class
Return:
  irrelevant."
  ;; we execute in the package of the object
  (with-ap current-entity
    (print `(make-editor-window ===> *package* ,*package*))
    ;; tell moss to post warnings into the pop up window
    (setq moss::*moss-output* nil)
    ;; create an editing box in the application package
    (moss::start-editing)
    ;; when creating a new object replaces the class-id with the new instance id
    (when create
      (setq current-entity (moss::%make-individual current-entity)))
    (let*
        ((ew (make-instance 'editor :width *browser-width* :title "MOSS Editor"))
         ;;=== top area
         (top (make-instance 'frame :master ew :width *browser-width* :padding "5"))
         ;; abort button
         (abort (make-instance 'button :master top :text "Abort" :width 10
                  :command (lambda () (ew-abort-on-click ew))))
         ;; accept
         (accept (make-instance 'button :master top :text "Commit" :width 10
                   :command (lambda () (ew-commit-on-click ew))))
         ;; context button
         (context-name  (make-instance 'label :master top :text "Context"))
         (context (make-instance 'entry :master top :text "0" :width 2))
         ;;=== Object description, e.g. Claire Labrousse
         (title (make-instance 'frame :width *browser-width* :master ew))
         ;;== attributes
         (att (make-instance 'frame :master ew))
         (att-title (make-instance 'label :master att :text "Attributes"))
         (att-list (make-instance 'frame :master att :width *list-area-width*))
         (att-vsb (make-instance 'scrollbar :orientation "vertical" :master att-list))
         (att-tree (make-instance 'treeview :height *list-area-height* :columns "value" 
                     :master att-list))
         (att-add (make-instance 'button :master att-list :text "ADD ATTR" :width 10
                    :command (lambda () (ew-add-attribute ew att-tree current-entity))))
         (att-del (make-instance 'button :master att-list :text "DELETE VAL" :width 10
                    :command (lambda () (ew-delete-attribute ew att-tree current-entity))))
         (att-mod (make-instance 'button :master att-list :text "MODIFY" :width 10
                    :command (lambda () (ew-modify-attribute ew att-tree current-entity))))
         ;;===relations
         (rel (make-instance 'frame :master ew))
         (rel-title (make-instance 'label :master rel :text "Relations"))
         (rel-list (make-instance 'frame :master rel :width *list-area-width*))
         (rel-vsb (make-instance 'scrollbar :orientation "vertical" :master rel-list))
         (rel-tree (make-instance 'treeview :height *list-area-height* :columns "value" 
                     :master rel-list))
         (rel-add (make-instance 'button :master rel-list :text "ADD REL" :width 10
                    :command (lambda () (ew-add-relation ew rel-tree current-entity))))
         (rel-del (make-instance 'button :master rel-list :text "DELETE VAL" :width 10
                    :command (lambda () (ew-delete-relation ew rel-tree current-entity))))
         (rel-mod (make-instance 'button :master rel-list :text "MODIFY" :width 10
                    :command (lambda () (ew-modify-relation ew rel-tree current-entity))))
         ;;===inverse links
         (inv (make-instance 'frame :master ew))
         (inv-title (make-instance 'label :master inv :text "Inverse Link"))
         (inv-list (make-instance 'frame :master inv :width *list-area-width*))
         (inv-vsb (make-instance 'scrollbar :orientation "vertical" :master inv-list))
         (inv-tree (make-instance 'treeview :height *list-area-height* :columns "value" 
                     :master inv-list))
         ;(inv-del (make-instance 'button :master inv-list :text "DELETE VAL" :width 10
         ;           :command (lambda () (ew-delete-inverse ew inv-tree current-entity))))
         ;(inv-mod (make-instance 'button :master inv-list :text "MODIFY" :width 10
         ;           :command (lambda () (ew-modify-inverse ew inv-tree current-entity))))
         (inv-det (make-instance 'button :master inv-list :text "DETAILS" :width 10
                    :command (lambda () (ew-details-inverse ew inv-tree current-entity))))
         )
      ;; save application package used by all callbacks
      (setf (application-package ew) *package*)
      (setf (current-entity ew) current-entity) 
      (setf (calling-window ew) *moss-output*
            *moss-output* ew)
      
      (set-geometry-xy ew *editor-left* *editor-top*) 
      
      (grid top 0 0 :sticky "ew")
      (pack abort :side :left )
      (pack accept :side :left)
      (pack context :side :right ) ;:fill :both)
      (pack context-name :side :right :fill :both)
      
      ;;=== Posting object name
      ;; create a variant of the label style
      (format-wish 
       "ttk::style configure Center.TLabel -font {Helvetica 16} -justify center -anchor center
      ttk::style layout Center.TLabel {Label.label -sticky ew}")
      ;; title text 
      (grid title 1 0 :sticky "ew")
      
      (format-wish
       "pack [ttk::label ~A.classname -text ~S -width 50 -style Center.TLabel] -pady 5 -fill both -expand 1"
       (widget-path title) 
       (car (moss::send-no-trace current-entity 'moss::=summary)))
      
      ;;=== ATTRIBUTES
      (grid att 3 0 :sticky "ew")
      (grid att-title 0 0 :sticky "w")
      (grid-configure  att-title :padx "10")
      (configure att-vsb :command (format nil "~A yview" (widget-path att-tree)))
      (configure att-tree :yscrollcommand  (format nil "~A set"  (widget-path att-vsb)))
      (grid-configure att-list :padx "40 10")
      (grid-configure att-list :pady "5")
      (grid att-list 1 0)
      (grid att-tree 0 0 :rowspan 3)
      (grid att-vsb 0 1 :sticky "ns" :rowspan 3)
      ;; add column titles
      (format-wish "~A heading #0 -text {Attribute Name}" (widget-path att-tree))
      (format-wish "~A heading value -text {~A}"  (widget-path att-tree) "Value")
      (ew-fill-attributes ew current-entity att-tree)
      ;; add buttons
      (grid att-add 0 2)
      (grid att-del 1 2)
      (grid att-mod 2 2)
      ;; show details of the attributes (goal is to edit them)
      
      ;;=== RELATIONS
      (grid rel 5 0 :sticky "ew")
      (grid rel-title 0 0 :sticky "w")
      (grid-configure  rel-title :padx "10")
      (configure rel-vsb :command (format nil "~A yview" (widget-path rel-tree)))
      (configure rel-tree :yscrollcommand  (format nil "~A set"  (widget-path rel-vsb)))
      (grid-configure rel-list :padx "40 10")
      (grid-configure rel-list :pady "5")
      (grid rel-list 1 0)
      (grid rel-tree 0 0 :rowspan 3)
      (grid rel-vsb 0 1 :sticky "ns" :rowspan 3)
      ;; add column titles
      (format-wish "~A heading #0 -text {Relation Name}" (widget-path rel-tree))
      (format-wish "~A heading value -text {~A}"  (widget-path rel-tree) "Value")
      (ew-fill-relations ew current-entity rel-tree)
      ;; add buttons
      (grid rel-add 0 2)
      (grid rel-del 1 2)
      (grid rel-mod 2 2)
      
      ;;=== INVERSE LINKS
      (grid inv 7 0 :sticky "ew")
      (grid inv-title 0 0 :sticky "w")
      (grid-configure  inv-title :padx "10")
      (configure inv-vsb :command (format nil "~A yview" (widget-path inv-tree)))
      (configure inv-tree :yscrollcommand  (format nil "~A set"  (widget-path inv-vsb)))
      (grid-configure inv-list :padx "40 10")
      (grid-configure inv-list :pady "5 10")
      (grid inv-list 1 0)
      (grid inv-tree 0 0) ; :rowspan 2)
      (grid inv-vsb 0 1 :sticky "ns") ; :rowspan 2)
      ;; add column titles
      (format-wish "~A heading #0 -text {Inverse Link Name}" (widget-path inv-tree))
      (format-wish "~A heading value -text {~A}"  (widget-path inv-tree) "Value")
      (ew-fill-inverses ew current-entity inv-tree)
      ;(grid inv-del 0 2)
      ;(grid inv-mod 1 2)
      (grid inv-det 0 2)
      ;; initialize the editing process
      (ew-start-editing ew current-entity)
      (on-close ew (lambda () (ew-close ew)))
      ;; record caller
      (setf (caller ew) caller)
      ;; when closing this window, reestablish previous one
      (on-close ew (lambda () 
                     ;; reestablish previous output channel
                     ;; and reset *moss-window* to indicate it is closed
                     (setq *moss-output* (calling-window ew))
                     ;; close window
                     (destroy ew)))
      )))

#| Test
 (catch :error
  (with-nodgui (:debug 3) 
    (moss::with-package :family
     (make-editor-window f::_jpb))))
|#
;;;-------------------------------------------------------- EW-ABORT-ON-CLICK

(defun ew-abort-on-click (ew)
  "aborting the editing session, meaning that we restore the old values and ~
  make the new ids unbound."
  (with-ap (current-entity ew)
    ;; DBG
    (moss::veb)
    ;; proceed, clearing the editing box
    (moss::abort-editing))
  (ew-close ew :no-commit)
  )

;;;--------------------------------------------------------- EW-ADD-ATTRIBUTE

(defun ew-add-attribute (ew att-tree current-entity)
  "add a new attribute, not part of the concept but taken from the list of ~
   generic attribute, with the value \"?\"."
  (with-ap (current-entity ew)
    (make-add-attribute-pane current-entity ew att-tree)))

;;;---------------------------------------------------------- EW-ADD-RELATION

(defun ew-add-relation (ew rel-tree current-entity)
  "add a new relation, not part of the concept but taken from the list of ~
   generic relations, linking to an orphan with name \"?\"."
  (with-package (moss::<<symbol-package current-entity)
    (make-add-relation-pane current-entity ew rel-tree)))

;;;----------------------------------------------------------------- EW-CLOSE

(defun ew-close (ew &optional no-commit)
  "on leaving the editor must update the environment if we are aborting, or ~
  save data on disc if we are committing"
  ;; set the package to the application package
  (with-ap (current-entity ew)
    ;; when clicking the exit button directly, we assume we are committing the 
    ;; changes, which clears the box
    (unless no-commit (moss::commit-editing))
    ;; when the window was not called be another window, we retore the moss
    ;; window flags so that we print now into the listener and kill wish
    (cond
     ((caller ew)
      ;; restore moss output channel
      (when (moss-output (caller ew))
        (setq moss::*moss-output*  (moss-output (caller ew))
              moss::*moss-window* moss::*moss-output*))
      ;; update the caller's window
      (update (caller ew) (current-entity ew))
      ;; close editor window, returning to the caller
      (destroy ew))
     (t
      (setq moss::*moss-output* t moss::*moss-window* nil)
      (destroy ew)
      (exit-wish) ; quit wish since no more windows are active
      ))
    ))

;;;------------------------------------------------------- EW-COMMIT-ON-CLICK

(defun ew-commit-on-click (ew)
  "commiting the editing session, meaning that we must save the new values on ~
  disc."
  (with-ap (current-entity ew)
    ;; DBG, executes in the application package
    (moss::veb)
    ;; proceed, clearing the editing box
    (moss::commit-editing)
    (ew-close ew :no-commit)
    ))

;;;------------------------------------------------------ EW-DELETE-ATTRIBUTE

(defun ew-delete-attribute (ew att-tree current-entity)
  "delete the attribute corresponding to the selected line of the table from ~
  the current entity. The semantics of the operation is that all values of ~
  the property are deleted, leaving the resulting attribute with 0 values.
  The attribute is NOT removed from the list"
  (declare (ignore ew))
  (with-ap current-entity
    (let (prop-id att-string-list)
      ;; get the list of selected attributes
      (setq att-string-list  (ew-get-property att-tree))
      ;; complain if nothing selected
      (unless att-string-list
        (ask-okcancel "please select an attribute")
        (return-from ew-delete-attribute))
      
      ;; get the attribute id
      (setq prop-id (intern (string-upcase (car att-string-list))))
      ;(print `("ew-delete-attribute prop-id:" ,prop-id))
      ;; ask for confirmation
      (unless
          (ask-okcancel "Are you sure you want to delete the values of this attribute?")
        ;; if none, quit
        (return-from ew-delete-attribute))
      ;; call the MOSS delete all values function
      (moss::delete-values current-entity prop-id :all)
      ;; check min value wrt 0 ?
      ;; delete the values from the value column
      (format-wish "~A set ~A value {~A}" (widget-path att-tree) prop-id
                   (format nil "~{~A~^, ~}" nil))
      )))

;;;------------------------------------------------------- EW-DELETE-RELATION

(defun ew-delete-relation (ew rel-tree current-entity)
  "delete the relation corresponding to the selected line of the table from ~
  the current entity. The semantics of the operation is that all values of ~
  the property are deleted, leaving the resulting attribute with 0 values.
  The attribute is NOT removed from the list"
  (declare (ignore ew))
  (with-ap current-entity
    (let (rel-string-list rel-id)
      ;; get the list of selected attributes
      (setq rel-string-list  (ew-get-property rel-tree))
      ;; complain if nothing selected
      (unless rel-string-list
        (ask-okcancel "please select a relation")
        (return-from ew-delete-relation))
      
      ;; get the attribute id
      (setq rel-id (intern (string-upcase (car rel-string-list))))
      ;(print `("ew-delete-relation prop-id:" ,prop-id))
      ;; ask for confirmation
      (unless
          (ask-okcancel "Are you sure you want to delete the values of this relation")
        ;; if none, quit
        (return-from ew-delete-relation))
      ;; call the MOSS delete all values function (updates editing box)
      (moss::delete-values current-entity rel-id :all)
      ;; check min value wrt 0 ?
      ;; delete the values from the value column
      (format-wish "~A set ~A value {~A}" (widget-path rel-tree) rel-id
                   (format nil "~{~A~^, ~}" nil))
      )))

;;;------------------------------------------------------- EW-DETAILS-INVERSE

(defun ew-details-inverse (ew inv-tree current-entity)
  "call the attribute pane to display the list of predecessors."
  (with-ap current-entity
    (let (inv-string-list inv-id)
      ;; get a list of the selected inverse link, e.g. ("$S-PERSON-BROTHER.OF")
      (setq inv-string-list  (ew-get-property inv-tree))
      ;; complain if nothing selected
      (unless inv-string-list
        (ask-okcancel "please select a link")
        (return-from ew-details-inverse))
      
      ;; get the attribute id from the list in the application package
      (setq inv-id (intern (string-upcase (car inv-string-list))))
      ;; call the attribute pane
      (make-inverse-details-pane current-entity inv-id ew inv-tree)
      )))

;;;---------------------------------------------------- EW-DISPLAY-PROPERTIES
;;; cannot be used for redisplaying properties because the ids conflict with
;;; the previous ones deined when displaying for the first time

(defun ew-display-properties (vtree current-entity prop-list tag)
  "displays a list of properties and values in the ad hoc pane
  Arguments:
  vtree: property pane
  current-entity: ..
  prop-list: list of properties to display
  tag: :att, :rel, :inv"
  (with-ap current-entity
    (let (value-list)
      ;; we use the items slot because the vtree is a flat one
      (setf (items vtree) prop-list)
      ;; for each property:
      (dolist (prop prop-list)
        (setq value-list 
              (case tag
                (:att
                 (mapcar #'(lambda (xx) (moss::send prop 'moss::=format-value xx))
                         (moss::send current-entity 'moss::=get-id prop)))
                ((:rel :inv)
                 (mapcar #'(lambda (xx) 
                             (let ((res (car (moss::send xx 'moss::=summary))))
                               (if (moss::%%is-id? res)
                                   (moss::>>f res)
                                   res)))
                         (moss::send current-entity 'moss::=get-id prop)))))
        (unless value-list (setq value-list '("")))
        ;; add the prop name to the tree
        (format-wish "~A insert {} end -id ~A -text {~A}" (widget-path vtree)
                     prop (car (moss::send prop 'moss::=instance-name)))
        ;; post the result into the value column of the tree
        (format-wish "~A set ~A value {~A}" (widget-path vtree) prop
                     (format nil "~{~A~^, ~}" value-list)))
      )))
    
;;;------------------------------------------------------- EW-FILL-ATTRIBUTES
   
(defun ew-fill-attributes (ew current-entity vtree)
  "function that fills the left treeview column with the name of the attribute ~
  and the first column with the associated values using the =format-value method."
  (with-ap current-entity
    (let* (prop-list)
      ;; get the list of attributes from the class (ordered as they are in the class)
      (setq prop-list (moss::send current-entity 'moss::=get-attributes))
      ;; save the list into the window slot
      (setf (attribute-list ew) prop-list)
      ;; display the result
      (ew-display-properties vtree current-entity prop-list :att)
      )))
    
;;;--------------------------------------------------------- EW-FILL-INVERSES
    
(defun ew-fill-inverses (ew current-entity vtree)
  "function that fills the left treeview column with the name of the inverse relation ~
  and the first column with the associated values using the =format-value method."
  (with-ap current-entity
    (let* (prop-list)
      ;; get the list of inverse relations
      (setq prop-list (moss::send current-entity 'moss::=has-inverse-properties))
      ;; save the list into the window slot
      (setf (inverse-list ew) prop-list)
      ;; dipslay the result
      (ew-display-properties vtree current-entity prop-list :inv)
      )))
    
;;;-------------------------------------------------------- EW-FILL-RELATIONS
    
(defun ew-fill-relations (ew current-entity vtree)
  "function that fills the left treeview column with the name of the relation and ~
  the first column with the associated values using the =format-value method."
  (with-ap current-entity
    (let* (prop-list)
      ;; get the list of attributes from the class (ordered as they are in the class)
      (setq prop-list (moss::send current-entity 'moss::=get-relations))
      ;; save them
      (setf (relation-list ew) prop-list)
      ;; dipslay the result
      (ew-display-properties vtree current-entity prop-list :rel))))
    
;;;------------------------------------------------------- EW-GET-PROPERTY-ID
;;; this function is a modified version of the treview-get-selection, because
;;; in our case, the id accessor is not defined since it is associated with
;;; the class treeview-item that e do not use

(defmethod ew-get-property ((tv treeview))
  "Get the selected property for the selected row of this treeview"
  (format-wish "senddatastrings [~a selection]" (widget-path tv))
  (read-data))

;;;------------------------------------------------------ EW-MODIFY-ATTRIBUTE

(defun ew-modify-attribute (ew att-tree current-entity)
  "call the attribute pane to modify the selected attribute. On return ~
  updates the object."
  (with-ap current-entity
    (let (att-string-list prop-id)
      ;; get the list of selected attributes
      (setq att-string-list  (ew-get-property att-tree))
      ;; complain if nothing selected
      (unless att-string-list
        (ask-okcancel "please select an attribute")
        (return-from ew-modify-attribute))
      
      ;; get the attribute id
      (setq prop-id (intern (string-upcase (car att-string-list))))
      ;; clean the result slot
      ;(setf (result ew) nil)
      
      ;; call the attribute pane
      (make-attribute-pane current-entity prop-id ew att-tree)
      )))
    
#|
;;;-------------------------------------------------------- EW-MODIFY-INVERSE

(defun ew-modify-inverse (ew inv-tree current-entity)
  "call the attribute pane to modify the selected attribute. On return ~
  updates the object."
  (with-ap current-entity
    (let (inv-string-list inv-id)
      ;; get a list of the selected inverse link, e.g. ("$S-PERSON-BROTHER.OF")
      (setq inv-string-list  (ew-get-property inv-tree))
      ;(print `("ew-modify-inverse inv-string-list:" ,inv-string-list))
      ;; complain if nothing selected
      (unless inv-string-list
        (ask-okcancel "please select a link")
        (return-from ew-modify-inverse))
      
      ;; get the attribute id from the list in the application package
      (setq inv-id (intern (string-upcase (car inv-string-list))))
      ;; call the attribute pane
      (make-inverse-pane current-entity inv-id ew inv-tree)
      )))
|#


;;;------------------------------------------------------- EW-MODIFY-RELATION

(defun ew-modify-relation (ew rel-tree current-entity)
  "call the relation pane to modify the selected relation. On return ~
  updates the object."
  (with-ap current-entity
    (let (rel-string-list prop-id)
      ;; must be in the application package (ap)
      (with-ap current-entity
        ;; get the list of selected attributes
        (setq rel-string-list  (ew-get-property rel-tree))
        ;; complain if nothing selected
        (unless rel-string-list
          (ask-okcancel "please select a relation")
          (return-from ew-modify-relation))
        
        ;; get the relation id
        (setq prop-id (intern (string-upcase (car rel-string-list))))
        ;; clean the result slot
        ;(setf (result ew) nil)
        
        ;; call the relation pane
        (make-relation-pane current-entity prop-id ew rel-tree)
        ))))

#|
(moss::with-package :family
  (setq *browser-left* 555 *browser-top* 250 *browser-width* 520 *context* 0
        *version-graph* '((0)) *list-area-height* 5)
 (catch :error
  (with-nodgui (:debug 3) 
    (moss::with-package :family
     (make-editor-window f::_jpb)))))
|#
    
;;;============================= Actions ====================================
        
;;;--------------------------------------------- EW-SHOW-INV-DETAILS-ON-CLICK
    
(defUn ew-show-inv-details-on-click (win tree-list rel-id)
  "Called when a line of the structural property display area has been clicked upon. 
  We display the details of the successors one per line.
  Arguments:
  win: the browser window displayin the current-entity
  tree-list: pointer to the pane displaying the list of relations
  current-entity: object being displayed in tree-list window
  rel-id: id of the property we want to detail"
  (with-ap (current-entity win)
    (let* ((current-entity (current-entity win))
           (inv-id (moss::send rel-id 'moss::=inverse-id))
           (details (make-instance 'frame :master tree-list))
           
           (details-list (make-instance 'listbox :master details 
                           :width *detail-area-width* :height 5))
           (details-button (make-instance 'button :master details :text "exit"
                             :command (lambda () (destroy details))))
           inv-list-details)
      (grid details 0 0 :sticky "ew")
      (grid details-button 0 0 :sticky "w")
      (grid details-list 1 0)
      ;; when selecting a value in the list opens a browser window 
      (bind details-list "<ButtonPress-1>"  
            (lambda (xx) 
              (declare (ignore xx))
              (on-selection details-list current-entity inv-id)))
      ;; get the list of strings summarizing each successor
      (setq inv-list-details (mapcar #'car
                                     (moss::broadcast 
                                      (moss::send current-entity 'moss::=get-id inv-id)
                                      'moss::=summary)))
      (listbox-append details-list inv-list-details)
      )))
    
 
#|
;;;---------------------------------------- EW-SHOW-NEXT-PAGE-ON-DOUBLE-CLICK
;;; opens a new editor window for selecter successor. In such a case, one must
;;; review the handling of *EDITING-BOX*

(defUn ew-show-next-page-on-double-click (current-page item)
  "Displays the selected entity on another page, when clicking on item."
  (let ((property-type (car (current-property-info current-page)))
        (object-list (cdr (current-property-info current-page)))
        index obj-id)
    (ewformat "~&;=== ew-show-next-page; current-page: ~S ~
  ~&property-type: ~S; object-list: ~S" 
              current-page property-type object-list)
    ;; we open windows only for sp or il
    (case property-type
      ((:sp :il)
       ;; get the index of the selected cell
       (setq index (car (%get-selected-cells-indexes item)))
       ;; get the object from the list of objects
       (setq obj-id (nth index object-list))
       (ewformat "~&;ew-show-next-page; obj-id: ~S" obj-id)
       ;; open a new browser
       (make-browser-window obj-id)
       )
      ;; don't do anything for terminal values
      )))
|#
;;;-------------------------------------------------------- EW-START-EDITING

(defun ew-start-editing (ew current-entity)
  "initializes a new editing process by creating an editing box in the  ~
   application package, linking it to the editor window and creating a special ~
   variable referencing it in the application package."
  ;; we assume that the application package is that of current entity
  (let ((application-package (moss::<<symbol-package current-entity))
        box)
    ;; save the displayed entity
    (setf (current-entity ew) current-entity)
    ;; create an editing box in the package of the edited object
    (setq box (moss::start-editing :owner ew :package application-package))
    ;; link it to the editing window
    (setf (editing-box ew) box)
    ;; create a special variable in the application package
    (set (intern "*EDITING-BOX*" application-package) box)
    ))
    
;;;==========================================================================
;;;                             ATTRIBUTE PANE
;;;==========================================================================
;;; The attribute pane is a window in which one can edit the value(s) ot an
;;; attribute

;;;----------------------------------------------------------- listbox-insert
;;; defining this method undefined in nodgui

(defmethod listbox-insert ((l scrolled-listbox) index values)
  (listbox-insert (listbox l) index values))

;;;=============================== Pane object =============================

(defclass attribute-pane (toplevel)
  ((old-values :accessor old-values :initform nil)
   (new-values :accessor new-values :initform nil)
   (closing-state :accessor closing-state :initform :normal) ; can be :abort
   )
  )

;;;------------------------------------------------------ MAKE-ATTRIBUTE-PANE
;;; the edit window that called the pane must contain two slots for saving
;;; values of objects before they are modified and modified objects

(defun make-attribute-pane (current-entity prop-id master att-tree)
  "build the various areas to display object info.
  Arguments:
  current-entity: id of object to be displayed
  master: the edit window ew that called the pane
  prop-id: the id of the attribute
  att-tree: the pane in the edit window (master)
  Return:
  nil."
  (with-ap current-entity
    ;(print `("make-attribute-pane ===> *package*" ,*package*))
    (let*
        ((ap (make-instance 'attribute-pane :width *editor-width* :title "ATTRIBUTE Pane"))
         ;;=== top area
         (top (make-instance 'frame :master ap :width *browser-width* :padding "5"))
         ;; abort button
         (abort (make-instance 'button :master top :text "Abort" :width 10
                  :command (lambda () 
                             (ap-abort-on-click 
                              ap master att-tree current-entity prop-id))))
         ;; accept
         (accept (make-instance 'button :master top :text "Accept" :width 10
                   :command (lambda () 
                              (ap-accept-on-click 
                               ap master att-tree current-entity prop-id))))
         ;; context button
         (context-name  (make-instance 'label :master top :text "Context"))
         (context (make-instance 'entry :master top :text "0" :width 2))
         ;;=== header
         (head (make-instance 'frame :width *browser-width* :master ap))
         ;;=== value area
         (val (make-instance 'frame :master ap :width *browser-width* :padding "5"))
         (val-show (make-instance 'frame :master val))
         ;; value list
         (val-vsb (make-instance 'scrollbar :master val-show :orientation "vertical"))
         (val-list (make-instance 'listbox :master val-show :height 10))
         ;;=== work area
         (val-entry (make-instance 'text :master val :height 4))
         (val-del (make-instance 'button :master val :text "DELETE"
                    :command (lambda ()(ap-delete ap val-list val-entry current-entity
                                                  prop-id))))
         (val-up (make-instance 'button :master val :width 5 :text "UP"
                   :command (lambda () (ap-up ap val-list prop-id))))
         (val-down (make-instance 'button :master val :width 5 :text "DOWN"
                     :command (lambda () (ap-down ap val-list prop-id))))
         (val-clr (make-instance 'button :master val :text "CLEAR TEXT"
                    :command (lambda () (clear-text val-entry))))
         (val-valid-add (make-instance 'button :master val :text "VALIDATE ADD"
                          :command (lambda () 
                                     (ap-validate-add 
                                      ap val-entry val-list current-entity prop-id))))
         (val-valid-mod (make-instance 'button :master val :text "VALIDATE REPLACE"
                          :command (lambda () 
                                     (ap-validate-replace 
                                      ap val-entry val-list current-entity prop-id))))
         )
      (set-geometry-xy ap *editor-left* *editor-top*) 
      
      (grid top 0 0 :sticky "ew")
      (pack abort :side :left )
      (pack accept :side :left)
      (pack context :side :right ) ;:fill :both)
      (pack context-name :side :right :fill :both)
      (grid head 1 0 :sticky "ew")
      ;; title text          
      (format-wish
       "pack [ttk::label ~A.classname -text ~S -width 50 -style Center.TLabel] -pady 5 -fill both -expand 1"
       (widget-path head) 
       (car (moss::send-no-trace prop-id 'moss::=instance-name)))
      ;; value area
      (grid val 2 0 :sticky "ew")
      (grid val-show 0 0 :rowspan 5)
      (grid val-list 0 0 :padx 5)
      (grid val-vsb 0 1 :sticky "ns")
      
      (configure val-vsb :command (format nil "~A yview" (widget-path val-list)))
      (configure val-list :yscrollcommand  (format nil "~A set" (widget-path val-vsb)))
      ;; populate list
      (ap-fill-values ap val-list current-entity prop-id)
      (bind val-list "<<ListboxSelect>>" 
            (lambda (xx) (declare (ignore xx)) (ap-set-text ap val-list val-entry)))
      (grid val-entry 0 1 :sticky "ne" :padx 6 :columnspan 2)
      (bind val-entry "<<Selection>>"
            (lambda (xx) (print `("selection callback / event:" ,xx))))
      (grid val-del 1 1)
      (grid val-up 2 1)
      (grid val-down 3 1)
      (grid val-clr 1 2)
      (grid val-valid-add 2 2)
      (grid val-valid-mod 3 2)
      (on-close ap (lambda () (ap-close ap master att-tree current-entity prop-id)))
      )))

;;;-------------------------------------------------------- AP-ABORT-ON-CLICK

(defun ap-abort-on-click (ap master att-tree current-entity prop-id)
  (with-ap current-entity
    (setf (closing-state ap) :abort)
    (ap-close ap master att-tree current-entity prop-id)))

;;;------------------------------------------------------- AP-ACCEPT-ON-CLICK

(defun ap-accept-on-click (ap master att-tree current-entity prop-id)
  (with-ap current-entity
    (setf (closing-state ap) :normal)
    (ap-close ap master att-tree current-entity prop-id)))

;;;----------------------------------------------------------------- AP-CLOSE
;;; we must make the changes here because the call creating the pane returns
;;; immediatly and the changes are not taken into account

(defun ap-close (ap master att-tree current-entity prop-id)
  "when closing the window, we transfer the results to the master if there. 
  Arguments:
  ap: current window object
  master: previous edit window"
  (with-ap current-entity
    (cond
     (master
      (case (closing-state ap)
        (:abort) ; do nothing other than closing the window
        (t  ; :normal, must update the environment
         (let ((old-values (old-values ap))
               (new-values (new-values ap))
               rem-val-list add-val-list)
           ;; note that here all changes will be added to the active editing box
           ;; compute what values were removed
           (setq rem-val-list (set-difference old-values new-values))
           ;; remove them
           (moss::delete-values current-entity prop-id rem-val-list :no-warning t)
           ;; compute what values were added
           (setq add-val-list (set-difference new-values old-values))
           ;; add new ones
           (moss::add-values current-entity prop-id add-val-list :no-warning t)
           ;; update the value-list in the value column of editor window (master)
           (format-wish "~A set ~A value {~A}" (widget-path att-tree) prop-id
                        (format nil "~{~A~^, ~}" new-values))
           )))
      ;; closes the window, but stay with wish to return to caller window
      (destroy ap)
      )
     ;; here we exercised the window directly. We close it, restore the listener 
     ;; output and quit wish
     (t
      (setq moss::*moss-output* t moss::*moss-window* nil)
      (destroy ap)
      (exit-wish)
      ))
    ))

;;;---------------------------------------------------------------- AP-DELETE
;;; deletes the selected value from the list of values and records the result
;;; into the new-values

(defun ap-delete (ap val-list val-entry current-entity prop-id)
  "delete a value from the list of vvalues and save the result into the ~
  new-values slot of the pane object"
  (declare (ignore current-entity))
  (with-ap current-entity
    (let (index cur-vals new-vals)
      ;; get the selected value index
      (setq index (listbox-get-selection-index val-list))
      (print `(delete index ,index))
      ;; if none, complain
      (unless index
        (ask-okcancel "Please select a value first.")
        (return-from ap-delete))
      ;; get the list of values from the new values slot
      (setq cur-vals (new-values ap))
      ;; clean entry
      (clear-text val-entry)
      ;; remove the value from the list
      (setq new-vals (nth-remove (car index) cur-vals))
      ;; record the new list
      (setf (new-values ap) new-vals)
      ;; update the list of values
      (ap-update-values ap val-list prop-id new-vals)
      ))
  t)

;;;------------------------------------------------------------------ AP-DOWN

(defun ap-down (ap val-list prop-id)
  "moves the selected cell one notch down in the list of posted values"
  (let (index vals)
    ;; get the selected cell index
    (setq index (car (listbox-get-selection-index val-list)))
    ;; if none complain and quit
    (unless index
      (ask-okcancel "No selected item?")
      (return-from ap-down))
    ;; get the list of current values
    (setq vals (new-values ap))
    ;; move the value down 1 place
    (setq vals (nth-move index vals 1))
    ;; record the result
    (setf (new-values ap) vals)
    ;; update the display
    (ap-update-values ap val-list prop-id vals)
    ;; keep it selected?
    (listbox-select val-list (min (1+ index) (1- (length vals))))
    ))

;;;----------------------------------------------------------- AP-FILL-VALUES

(defun ap-fill-values (ap val-list entity-id prop-id &key init)
  "fills the list with the values associated to prop-id for entity entity-id"
  (with-ap entity-id
    (let ((raw-values (moss::send entity-id 'moss::=get-id prop-id))
          vals)
      ;; put them into the right format
      (setq vals (mapcar #'(lambda (xx) (moss::send prop-id 'moss::=format-value xx))
                         raw-values))
      ;; save values into old-values slot of the attribut pane object
      (setf (old-values ap) raw-values)
      ;; fill the new-values slot as well
      (setf (new-values ap) raw-values)
      (listbox-insert val-list 0 vals))))
    
;;;-------------------------------------------------------------- AP-SET-TEXT

(defun ap-set-text (ap val-list val-entry)
  "callback that moves the data from the display list to the text area"
  (let ((index (car (listbox-get-selection-index val-list)))
        val)
    (when index
      ;; get value from the list
      (setq val (nth index (new-values ap)))
      ;; copy it to the text area
      (setf (text val-entry) val)
      )))
    
;;;-------------------------------------------------------------------- AP-UP

(defun ap-up (ap val-list prop-id)
  "moves the selected cell one notch down in the list of posted values"
  (let (index vals)
    ;; get the selected cell index
    (setq index (car (listbox-get-selection-index val-list)))
    ;; if none complain and quit
    (unless index
      (ask-okcancel "No selected item?")
      (return-from ap-up))
    ;; get the list of current values
    (setq vals (new-values ap))
    ;; move the value down 1 place
    (setq vals (nth-move index vals -1))
    ;; record the result
    (setf (new-values ap) vals)
    ;; update the display
    (ap-update-values ap val-list prop-id vals)
    ;; keep it selected?
    (listbox-select val-list (max 0 (1- index)))
    ))

;;;--------------------------------------------------------- AP-UPDATE-VALUES

(defun ap-update-values (ap val-list prop-id value-list)
  "update the list of (raw) values while saving the new list of values"
  (setf (new-values ap) value-list)
  (listbox-delete val-list)
  (listbox-insert val-list 0 
                  (mapcar #'(lambda (xx) 
                              (moss::send prop-id 'moss::=format-value xx))
                          value-list))
  t)
              
;;;--------------------------------------------------------- AP-VALIDATE-ADD 

(defun ap-validate-add (ap val-entry val-list current-entity prop-id)
  "takes the content of the text area and runs the validate function for attributes
  If the result is nil, complain, otherwise add the new value to the end of 
  the list of values."
  (declare (ignore current-entity))
  (with-ap current-entity
    (let (text val value-list)
      ;; select the text, removing space and end of line
      (setq text (string-trim  '(#\space #\return #\newline) (text val-entry)))
      ;(print `(ap-validate-add text ,text))
      ;; if "" complain
      (when (equal text "")
        (unless (ask-okcancel "value is an empty text, OK?")
          (return-from ap-validate-add)))
      ;; run the validate function
      (setq val (moss::%validate-tp prop-id (list text)))
      ;; if error, complain and quit
      (unless val
        (ask-okcancel "value is is not valid for this property.")
        (return-from ap-validate-add))
      ;; otherwise, get the current list of values
      (setq value-list (new-values ap))
      ;; append the new value to the end
      (setq value-list (append value-list val))
      ;; save it 
      (setf (new-values ap) value-list)
      ;; update the display
      (ap-update-values ap val-list prop-id value-list)
      )))

;;;------------------------------------------------------ AP-VALIDATE-REPLACE

(defun ap-validate-replace (ap val-entry val-list current-entity prop-id)
  "takes the content of the text area and runs the validate function for attributes
  If the result is nil complain, otherwise replaces the selected value in the 
  list of deisplayed values."
  (declare (ignore current-entity))
  (with-ap current-entity
    (let (text val value-list nn)
      ;; select the text, removing space and end of line
      (setq text (string-trim  '(#\space #\return #\newline) (text val-entry)))
      (print `(ap-validate-add text ,text))
      ;; if "" complain
      (when (equal text "")
        (unless (ask-okcancel "value is an empty text, OK?")
          (return-from ap-validate-replace)))
      ;; run the validate function
      (setq val (moss::%validate-tp prop-id (list text)))
      ;; if error, complain and quit
      (unless val
        (ask-okcancel "value is is not valid for this property.")
        (return-from ap-validate-replace))
      ;; get the index of the selected value
      (setq nn (car (listbox-get-selection-index val-list)))
      ;; if nothing selected complain
      (unless nn
        (ask-okcancel "vno value selected in the list.")
        (return-from ap-validate-replace))
      ;; otherwise, get the current list of values
      (setq value-list (new-values ap))
      ;; append the new value to the end
      (setq value-list (nth-replace nn (car val) value-list))
      ;; save it 
      (setf (new-values ap) value-list)
      ;; update the display
      (ap-update-values ap val-list prop-id value-list)
      )))

;;;==========================================================================
;;;                             RELATION PANE
;;;==========================================================================
;;; The relation pane is a window in which one can edit the value(s) ot a
;;; relation, i.e. add or remove successor entities.

;;;----------------------------------------------------------- listbox-insert
;;; defined for attributes

;;;=============================== Pane object =============================

(defclass relation-pane (toplevel)
  ((old-values :accessor old-values :initform nil)
   (new-values :accessor new-values :initform nil)
   (query-last-results :accessor query-last-results :initform nil)
   (closing-state :accessor closing-state :initform :normal) ; can be :abort
   )
  )

;;;------------------------------------------------------- MAKE-RELATION-PANE
;;; the edit window that called the pane must contain a slot referring to
;;; an editing box for saving values of objects before they are modified and 
;;; modified objects

(defun make-relation-pane (current-entity prop-id master rel-tree)
  "build the various areas to display object info.
  Arguments:
  current-entity: id of object to be displayed
  master: the edit window ew that called the pane (calling editor)
  prop-id: the id of the relation
  rel-tree: relation pane of the calling editor
  Return:
  nil."
  (with-package (moss::<<symbol-package current-entity)
    (print `(make-relation-pane ===> *package* ,*package*))
    (let*
        ((rp (make-instance 'relation-pane :width *editor-width* :title "RELATION Pane"))
         ;;=== top area
         (top (make-instance 'frame :master rp :width *browser-width* :padding "5"))
         ;; abort button
         (abort (make-instance 'button :master top :text "Abort" :width 10
                  :command (lambda () 
                             (rp-abort-on-click rp master rel-tree current-entity prop-id))))
         ;; accept
         (accept (make-instance 'button :master top :text "Accept" :width 10
                   :command (lambda () 
                              (rp-accept-on-click rp master rel-tree current-entity prop-id))))
         ;; context button
         (context-name  (make-instance 'label :master top :text "Context"))
         (context (make-instance 'entry :master top :text "0" :width 2))
         ;;=== header
         (head (make-instance 'frame :width *browser-width* :master rp))
         
         ;;=== work area
         (val (make-instance 'frame :master rp :width *browser-width* :padding "5"))
         ;;= value list
         (val-show (make-instance 'frame :master val))
         (val-vsb (make-instance 'scrollbar :master val-show :orientation "vertical"))
         (val-list (make-instance 'listbox :master val-show :height 10))
         
         ;;= result area
         ;; results from the query
         (res-show (make-instance 'frame :master val))
         (res-vsb (make-instance 'scrollbar :master res-show :orientation "vertical"))
         (res-list (make-instance 'listbox :master res-show :height 10))
         
         ;;= work area query entry
         (val-entry (make-instance 'text :master val :height 4))
         ;;= work area left buttons
         (val-del (make-instance 'button :master val :text "DELETE"
                    :command (lambda ()(rp-delete rp val-list val-entry current-entity
                                                  prop-id))))
         (val-up (make-instance 'button :master val :width 5 :text "UP"
                   :command (lambda () (rp-up rp val-list prop-id))))
         (val-down (make-instance 'button :master val :width 5 :text "DOWN"
                     :command (lambda () (rp-down rp val-list prop-id))))
         ;;= work area central buttons
         (val-exe (make-instance 'button :master val :text "EXECUTE QUERY"
                    :command (lambda () ())))
         ;;= work area right buttons
         (val-clr (make-instance 'button :master val :text "CLEAR QUERY"
                    :command (lambda () (clear-text val-entry))))
         (val-valid-add (make-instance 'button :master val :text "VALIDATE ADD"
                          :command (lambda () 
                                     (rp-validate-add rp res-list val-list
                                                      current-entity prop-id))))
         (val-valid-mod (make-instance 'button :master val :text "VALIDATE REPLACE"
                          :command (lambda () 
                                     (rp-validate-replace rp res-list val-list
                                                          current-entity prop-id))))
         )
      (set-geometry-xy rp *editor-left* *editor-top*) 
      
      (grid top 0 0 :sticky "ew")
      (pack abort :side :left )
      (pack accept :side :left)
      (pack context :side :right ) ;:fill :both)
      (pack context-name :side :right)
      
      ;; create a variant of the label style
      (format-wish 
       "ttk::style configure Center.TLabel -font {Helvetica 16} -justify center -anchor center
      ttk::style layout Center.TLabel {Label.label -sticky ew}")
      ;;=== header 
      (grid head 1 0 :sticky "ew")         
      (format-wish
       "pack [ttk::label ~A.classname -text ~S -width 50 -style Center.TLabel] -pady 5 -fill both -expand 1"
       (widget-path head) 
       (car (moss::send-no-trace prop-id 'moss::=instance-name)))
      
      ;;=== value area
      (grid val 2 0 :sticky "ew")
      
      ;;= query input area
      (grid val-entry 0 3 :sticky "ne" :padx 6 :columnspan 3)
      (bind val-entry "<KeyPress>"
            (lambda (event) (rp-input-on-change event rp val-entry res-list)))
      
      ;;= value list
      (grid val-show 0 0 :rowspan 5)
      (grid val-list 0 0 :padx 5)
      (grid val-vsb 0 1 :sticky "ns")
      (configure val-vsb :command (format nil "~A yview" (widget-path val-list)))
      (configure val-list :yscrollcommand  (format nil "~A set" (widget-path val-vsb)))
      ;; populate list
      (rp-fill-values rp val-list current-entity prop-id)
      
      ;;= result list
      (grid res-show 0 1 :rowspan 5)
      (grid res-list 0 1 :padx 5)
      (grid res-vsb 0 2 :sticky "ns")
      (configure res-vsb :command (format nil "~A yview" (widget-path res-list)))
      (configure res-list :yscrollcommand  (format nil "~A set" (widget-path res-vsb)))
      (listbox-export-selection res-list nil)
      
      ;;=== work area
      (grid val-del 1 3)
      (grid val-up 2 3)
      (grid val-down 3 3)
      (grid val-clr 1 4)
      (grid val-exe 2 4)
      (grid val-valid-add 1 5)
      (grid val-valid-mod 2 5)
      (on-close rp (lambda () (rp-close rp master rel-tree current-entity prop-id)))
      )))

#|
 (catch :error
  (with-nodgui (:debug 3) 
    (wap f::_jpb
     (make-relation-pane f::_ab 'f::$s-person-cousin :dummy))))
|#
;;;-------------------------------------------------------- RP-ABORT-ON-CLICK

(defun rp-abort-on-click (rp master rel-tree current-entity prop-id)
  (setf (closing-state rp) :abort)
  (rp-close rp master rel-tree current-entity prop-id))

;;;------------------------------------------------------- RP-ACCEPT-ON-CLICK

(defun rp-accept-on-click (rp master rel-tree current-entity prop-id)
  (setf (closing-state rp) :normal)
  (rp-close rp master rel-tree current-entity prop-id))

;;;----------------------------------------------------------------- RP-CLOSE
;;; we must make the changes here because the call creating the pane returns
;;; immediatly and the changes are not taken into account

(defun rp-close (rp master rel-tree current-entity prop-id)
  "when closing the window, we transfer the results to the master that should
  have a result slot. On abort set the result slot to :abort.
  Arguments:
  rp: current window object
  master: calling edit window
  rel-tree: relation pane of the edit-window
  current-entity: ..
  prop-id: relation id"
  (cond
   (master
    (with-ap current-entity
      (case (closing-state rp)
        (:abort) ; do nothing but close the window and return to caller 
        (t  ; :normal
         ;; must do that in the application package
         
         (let ((old-values (old-values rp))
               (new-values (new-values rp))
               rem-val-list add-val-list value-string-list)
           ;; note that here all changes will be added to the active editing box
           ;; compute what values were removed
           (setq rem-val-list (set-difference old-values new-values))
           ;; remove them
           (moss::delete-values current-entity prop-id rem-val-list :no-warning t)
           ;; compute what values were added
           (setq add-val-list (set-difference new-values old-values))
           ;; add new ones
           (moss::add-values current-entity prop-id add-val-list :no-warning t)
           ;; get the strings corresponding to new values (list of ids)
           (setq value-string-list 
                 (mapcar #'(lambda (xx) (car (moss::send xx 'moss::=summary)))
                         new-values))
           ;; update the value-list in the value column of the calling editor
           (format-wish "~A set ~A value {~A}" (widget-path rel-tree) prop-id
                        (format nil "~{~A~^, ~}" value-string-list))
           )))
      (moss::veb))
    ;; closes the window
    (destroy rp)
    )
   ;; here window was called directly, we restore writing to the listener and kill
   ;; wish
   (t
    (setq moss::*moss-output* t moss::*moss-window* nil)
    (destroy rp)
    (exit-wish)
    ))
  )

;;;---------------------------------------------------------------- RP-DELETE

(defun rp-delete (rp val-list val-entry current-entity prop-id)
  "remove a successor from the list of successors, updates the list of saved ~
  values"
  (declare (ignore current-entity))
  (with-ap current-entity
    (let (index cur-vals new-vals)
      ;; get the selected value index
      (setq index (listbox-get-selection-index val-list))
      (print `(delete index ,index))
      ;; if none, complain
      (unless index
        (ask-okcancel "Please select a value first.")
        (return-from rp-delete))
      ;; get the list of values from the new values slot
      (setq cur-vals (new-values rp))
      ;; remove the value from the list
      (setq new-vals (nth-remove (car index) cur-vals))
      ;; record the new list
      (setf (new-values rp) new-vals)
      ;; update the list of values
      (rp-update-values rp val-list prop-id new-vals)
      ))
  t)

;;;------------------------------------------------------------------ RP-DOWN

(defun rp-down (rp val-list prop-id)
  "moves the selected cell one notch down in the list of posted values"
  (let (index vals)
    ;; get the selected cell index
    (setq index (car (listbox-get-selection-index val-list)))
    ;; if none complain and quit
    (unless index
      (ask-okcancel "No selected item?")
      (return-from rp-down))
    ;; get the list of current values
    (setq vals (new-values rp))
    ;; move the value down 1 place
    (setq vals (nth-move index vals 1))
    ;; record the result
    (setf (new-values rp) vals)
    ;; update the display
    (rp-update-values rp val-list prop-id vals)
    ;; keep it selected?
    (listbox-select val-list (min (1+ index) (1- (length vals))))
    ))

;;;----------------------------------------------------------- RP-FILL-VALUES

(defun rp-fill-values (rp val-list entity-id prop-id &key init)
  "fills the list with the values associated to prop-id for entity entity-id"
  (declare (ignore init))
  (with-ap entity-id
    (let ((raw-values (moss::send entity-id 'moss::=get-id prop-id))
          vals)
      ;; put them into the right format
      (setq vals (mapcar #'(lambda (xx) (car (moss::send prop-id 'moss::=format-value xx)))
                         raw-values))
      ;; save suc-ids into old-values slot of the relation pane object
      (setf (old-values rp) raw-values)
      ;; fill the new-values slot as well
      (setf (new-values rp) raw-values)
      (listbox-insert val-list 0 vals))))

;;;------------------------------------------------------- RP-INPUT-ON-CHANGE

(defUn rp-input-on-change (event rp val-entry res-list)
  "Called whenever the content of the input pane (a string) changes.
   If we have balanced parents (more parents that close than that open), we ~
   execute the expr as a query
Arguments:
   event: a vector representing the event
   rp: relation pane
   val-entry: input pane
   res-list: output pane"
  (let ((last-char (slot-value event 'char))
        (text (text val-entry))
        )
    (format t "last char: ~S, text: ~S" last-char text)(finish-output)
       (cond 
        ((and
          (string-equal last-char "Return")
          (moss::is-valid-lisp-expr? text))
         ;; go process query
         (moss::catch-system-error "while processing query" 
              (rp-process-query rp val-entry res-list))
         ;; select the text from the input pane
         (select-all val-entry)
         (focus val-entry)
         )
        ;; otherwise process the key stroke by doing nothing
        )
    ;; return t like ACL
    t))

;;;-------------------------------------------------------- RP-PROCESS-QUERRY
;;; we assume that we execute this function in the application package

;(trace moss::parse-user-query)

(defUn rp-process-query (rp val-entry res-list)
  "reads a query from the input pane. Checks its syntax. If OK, prints the result ~
  into the result list. Uses a number of functions from the query package.
  This callback executes within the package in which the window has been created.
  Arguments:
  rp: relation pane object
  val-entry: input pane
  res-list: output pane"
  (let ((expr (read-from-string (text val-entry) nil nil))
        parsed-expr result)
    ;; clean slot to save the results
    (setf (query-last-results rp) nil)
    ;(moss::with-package (application-package mw)
    (print `("rp-process-query *package*" ,*package*))
    (print `("rp-process-query expr" ,expr))
    ;; parse the expression
    (setq parsed-expr (moss::parse-user-query expr))
    (print `("rp-process-query parsed-expr" ,parsed-expr))
    
    ;; if error, then quits
    (unless parsed-expr 
      (ask-okcancel "...Can't understand the query.")
      (return-from rp-process-query))

   ;; execute the query
    (setq result (moss::access parsed-expr :already-parsed t))
    (print `("rp-process-query result" ,result))
    ;; reset saved result list
    (setf (query-last-results rp) result)
 
    (unless result 
      (ask-okcancel "Could not find anything...")
      (return-from rp-process-query))

    ;; clean res-list
    (listbox-delete res-list)
    ;; add object to the list of results
    (listbox-append res-list 
                    (mapcar #'(lambda (xx)
                                 (car (moss::send xx 'moss::=summary)))
                             result))
    t))

;;;-------------------------------------------------------------------- RP-UP
 
(defun rp-up (rp val-list prop-id)
  "moves the seected item 1 notch up in the list"
  (let (index vals)
    ;; get the selected cell index
    (setq index (car (listbox-get-selection-index val-list)))
    ;; if none complain and quit
    (unless index
      (ask-okcancel "No selected item?")
      (return-from rp-up))
    ;; get the list of current values
    (setq vals (new-values rp))
    ;; move the value down 1 place
    (setq vals (nth-move index vals -1))
    ;; record the result
    (setf (new-values rp) vals)
    ;; update the display
    (rp-update-values rp val-list prop-id vals)
    ;; keep it selected?
    (listbox-select val-list (max 0 (1- index)))
    ))

;;;--------------------------------------------------------- RP-UPDATE-VALUES
;;; the saved list of values is the list of ids of the successors

(defun rp-update-values (rp val-list prop-id value-list)
  "update the list of (raw) values while saving the new list of values"
  (setf (new-values rp) value-list)
  (listbox-delete val-list)
  (listbox-insert val-list 0 
                  (mapcar #'(lambda (xx) 
                              (car (moss::send xx 'moss::=summary)))
                          value-list))
  t)

;;;--------------------------------------------------------- RP-VALIDATE-ADD 

(defun rp-validate-add (rp res-list val-list current-entity prop-id)
  "takes the selected successor from the result list and runs the validate ~
  function for relations.
  If the result is nil, complain, otherwise add the new value to the end of 
  the list of values."
  (declare (ignore current-entity))
  (with-ap current-entity
    (let (index suc-id value-list)
      ;; get the index of the selected successor
      (setq index (car (listbox-get-selection-index res-list)))
      (print `(rp-validate-add index ,index))
      ;; if "" complain
      (unless index
        (ask-okcancel "No selected item?")
        (return-from rp-validate-add))
      ;; get the object from the resulting list
      (setq suc-id (nth index (query-last-results rp)))
      (print `("... rp-validate-add suc-id" ,suc-id))
      ;; get the list of successors
      (setq value-list (new-values rp))
      ;; add it to the list of successors
      (setq value-list (append value-list (list suc-id)))
      ;; run the validate function (to do when closing the pane)
      ; (setq val (moss::%validate-sp prop-id val))
      ;; save it 
      (setf (new-values rp) value-list)
      ;; update the display
      (rp-update-values rp val-list prop-id value-list)
      )))

;;;----------------------------------------------------- RP-VALIDATE-REPLACE

(defun rp-validate-replace (rp res-list val-list current-entity prop-id)
  "takes the selected successor from the result list and runs the validate ~
  function for relations.
  If the result is nil, complain, otherwise add the new value to the end of 
  the list of values."
  (declare (ignore current-entity))
  (with-ap current-entity
    (let (index index1 suc-id value-list)
      ;; get the index of the selected successor
      (setq index (car (listbox-get-selection-index res-list)))
      (print `(rp-validate-add index ,index))
      ;; if null complain
      (unless index
        (ask-okcancel "No selected item?")
        (return-from rp-validate-replace))
      ;; get the index of the object to replace
      (setq index1 (car (listbox-get-selection-index val-list)))
      ;; if null complain
      (unless index1
        (ask-okcancel "Item to replace should be selected?")
        (return-from rp-validate-replace))
      
      ;; get the object from the resulting list
      (setq suc-id (nth index (query-last-results rp)))
      (print `("... rp-validate-add suc-id" ,suc-id))
      ;; get the list of successors
      (setq value-list (new-values rp))
      ;; add it to the list of successors
      (setq value-list (nth-replace index1 suc-id value-list))
      ;; run the validate function (to do when closing the pane)
      ; (setq val (moss::%validate-sp prop-id val))
      ;; save it 
      (setf (new-values rp) value-list)
      ;; update the display
      (rp-update-values rp val-list prop-id value-list)
      )))

#|
(moss::with-package :family
  (setq *browser-left* 555 *browser-top* 250 *browser-width* 520 *context* 0
        *version-graph* '((0)) *list-area-height* 5)
 (catch :error
  (with-nodgui (:debug 3) 
    (moss::with-package :family
     (make-relation-pane f::_ab 'f::$s-person-cousin :dummy)))))
|#
#|
;;;==========================================================================
;;;                             INVERSE PANE
;;;==========================================================================
;;; The inverse pane is a window in which one can edit the value(s) ot an
;;; inverse link. It is only possible to delete a link.

;;;----------------------------------------------------------- listbox-insert
;;; defined for attributes

;;;=============================== Pane object =============================

(defclass inverse-pane (toplevel)
  ((old-values :accessor old-values :initform nil)
   (new-values :accessor new-values :initform nil)
   (closing-state :accessor closing-state :initform :normal) ; can be :abort
   )
  )

;;;-------------------------------------------------------- MAKE-INVERSE-PANE
;;; the edit window that called the pane must contain a slot referring to
;;; an editing box for saving values of objects before they are modified and 
;;; the list of modified objects

(defun make-inverse-pane (current-entity inv-id master inv-tree)
  "build the various areas to display object info.
Arguments:
  current-entity: id of object to be displayed
  master: the edit window ew that called the pane (calling editor)
  inv-id: the id of the inverse relation
  inv-tree: inverse relation pane of the calling editor
Return:
  nil."
  (print `(make-inverse-pane ===> *package* ,*package*))
  (let*
      ((ip (make-instance 'inverse-pane :width *editor-width* :title "INVERSE Pane"))
       ;;=== top area
       (top (make-instance 'frame :master ip :width *browser-width* :padding "5"))
       ;; abort button
       (abort (make-instance 'button :master top :text "Abort" :width 10
                :command (lambda () 
                          (ip-abort-on-click ip master inv-tree current-entity inv-id))))
       ;; accept
       (accept (make-instance 'button :master top :text "Accept" :width 10
                 :command (lambda () 
                            (ip-accept-on-click ip master inv-tree current-entity inv-id))))
       ;; context button
       (context (symbol-value (intern "*CONTEXT*")))
       (context-name  (make-instance 'label :master top :text "Context"))
       (context (make-instance 'entry :master top :text context :width 2))
       ;;=== header
       (head (make-instance 'frame :width *browser-width* :master ip))

       ;;=== work area
       (inv (make-instance 'frame :master ip :width *browser-width* :padding "5"))
       ;;= value list
       (inv-show (make-instance 'frame :master inv))
       (inv-vsb (make-instance 'scrollbar :master inv-show :orientation "vertical"))
       (inv-list (make-instance 'listbox :master inv-show :height 10 :width 35))
       ;;= delete button
       (inv-del (make-instance 'button :master inv :text "DELETE"
                  :command (lambda () (ip-delete ip inv-list inv-id))))
       )
    (set-geometry-xy ip *editor-left* *editor-top*) 
    
    (grid top 0 0 :sticky "ew")
    (pack abort :side :left )
    (pack accept :side :left)
    (pack context :side :right ) ;:fill :both)
    (pack context-name :side :right)

    ;; create a variant of the label style
    (format-wish 
     "ttk::style configure Center.TLabel -font {Helvetica 16} -justify center -anchor center
    ttk::style layout Center.TLabel {Label.label -sticky ew}")
    ;;=== header 
    (grid head 1 0 :sticky "ew")         
    (format-wish
     "pack [ttk::label ~A.classname -text ~S -width 50 -style Center.TLabel] -pady 5 -fill both -expand 1"
     (widget-path head) 
     (car (moss::send-no-trace inv-id 'moss::=instance-name)))

    ;;=== value area
    (grid inv 2 0 :sticky "ew")

    ;;= value list
    (grid inv-show 0 0 :rowspan 5)
    (grid inv-list 0 0 :padx 5)
    (grid inv-vsb 0 1 :sticky "ns")
    (configure inv-vsb :command (format nil "~A yview" (widget-path inv-list)))
    (configure inv-list :yscrollcommand  (format nil "~A set" (widget-path inv-vsb)))
    ;; populate list
    (ip-fill-values ip inv-list current-entity inv-id)

    ;;= Delete button
    (grid inv-del 0 1 :padx 5)
    (on-close ip (lambda () (ip-close ip master inv-tree current-entity inv-id)))
    ))

#|
(moss::with-package :family
  (setq *browser-left* 555 *browser-top* 250 *browser-width* 520 *context* 0
        *version-graph* '((0)) *list-area-height* 5)
 (catch :error
  (with-nodgui (:debug 3) 
    (moss::with-package :family
     (make-inverse-pane f::_jpb 'f::$s-person-brother.of :dummy :ivt)))))
|#
;;;-------------------------------------------------------- IP-ABORT-ON-CLICK

(defun ip-abort-on-click (ip master inv-tree current-entity prop-id)
  (setf (closing-state ip) :abort)
  (ip-close ip master inv-tree current-entity prop-id))

;;;------------------------------------------------------- IP-ACCEPT-ON-CLICK

(defun ip-accept-on-click (ip master inv-tree current-entity prop-id)
  (setf (closing-state ip) :normal)
  (ip-close ip master inv-tree current-entity prop-id))

;;;----------------------------------------------------------------- IP-CLOSE
;;; we must make the changes here because the call creating the pane returns
;;; immediatly and the changes are not taken into account

(defun ip-close (ip master inv-tree current-entity inv-id)
  "when closing the window, we transfer the results to the master that should
   have a result slot. On abort set the result slot to :abort.
Arguments:
   ip: current window object
   master: calling edit window
   inv-tree: inverse relation pane of the edit-window
   current-entity: ..
   inv-id: inverse relation id"
  (when master
    (case (closing-state ip)
      (:abort t) ; do nothing    
      (t  ; :normal
       (let ((old-values (old-values ip))
             (new-values (new-values ip))
             rem-val-list value-string-list prop-id)
         (print `("ip-close old-values" ,old-values))
         (print `("ip-close new-values" ,new-values))
         ;; note that here all changes will be added to the active editing box
         ;; compute what values were removed
         (setq rem-val-list (set-difference old-values new-values))
         (print `("ip-close rem-val-list" ,rem-val-list))
         (setq prop-id (moss::%inverse-property-id inv-id))
         ;; remove them
         (mapc #'(lambda (xx) (moss::delete-values xx prop-id (list current-entity)
                                                   :no-warning t))
               rem-val-list)
         (pprint `("ip-close jpb" ,(moss::<< current-entity)))
         ;; get the strings corresponding to new values (list of ids)
         (setq value-string-list 
               (mapcar #'(lambda (xx) (car (moss::send xx 'moss::=summary)))
                       new-values))
         ;; update the value-list in the value column of the calling editor
         (format-wish "~A set ~A value {~A}" (widget-path inv-tree) inv-id
                      (format nil "~{~A~^, ~}" value-string-list))
         )))
    ;; closes the window
    (destroy ip)
  ))

;;;---------------------------------------------------------------- IP-DELETE

(defun ip-delete (ip inv-list inv-id)
  "remove a predecessor from the list of predecessors, updates the list of saved ~
   values"
  (declare (ignore inv-id))
  (let (index cur-vals new-vals)
    ;; get the selected value index
    (setq index (car (listbox-get-selection-index inv-list)))
    (print `(ip-delete index ,index))
    ;; if none, complain
    (unless index
      (ask-okcancel "Please select a value first.")
      (return-from ip-delete))
    ;; get the list of values from the new values slot
    (setq cur-vals (new-values ip))
    ;; remove the value from the list
    (setq new-vals (nth-remove index cur-vals))
    ;; record the new list
    (setf (new-values ip) new-vals)
    ;; update inv-list
    (listbox-delete inv-list index (1+ index))
    )
  t)

;;;----------------------------------------------------------- IP-FILL-VALUES

(defun ip-fill-values (ip inv-list entity-id inv-id &key init)
  "fills the list with the values associated to prop-id for entity entity-id"
  (declare (ignore init))
  (let ((raw-values (moss::send entity-id 'moss::=get-id inv-id))
        vals)
    ;; put them into the right format
    (setq vals (mapcar #'(lambda (xx) (car (moss::send xx 'moss::=summary)))
                       raw-values))
    ;; save suc-ids into old-values slot of the relation pane object
    (setf (old-values ip) raw-values)
    ;; fill the new-values slot as well
    (setf (new-values ip) raw-values)
    (listbox-insert inv-list 0 vals)))
|#

;;;==========================================================================
;;;                           INVERSE DETAILS PANE
;;;==========================================================================

(defun make-inverse-details-pane (current-entity inv-id master inv-tree)
  "build the various areas to display object info.
  Arguments:
  current-entity: id of object to be displayed
  master: the edit window ew that called the pane (calling editor)
  inv-id: the id of the inverse relation
  inv-tree: inverse relation pane of the calling editor
  Return:
  nil."
  (declare (ignore inv-tree master))
  (with-ap current-entity
    (print `(make-inverse-pane ===> *package* ,*package*))
    (let*
        ((ip (make-instance 'toplevel :width *editor-width* :title "INVERSE Pane"))
         ;;=== top area
         (top (make-instance 'frame :master ip :width *browser-width* :padding "5"))
         ;; context button
         (context (symbol-value (intern "*CONTEXT*")))
         (context-name  (make-instance 'label :master top :text "Context"))
         (context (make-instance 'entry :master top :text context :width 2))
         ;;=== header
         (head (make-instance 'frame :width *browser-width* :master ip))
         
         ;;=== display area
         (inv (make-instance 'frame :master ip :width *browser-width* :padding "5"))
         ;;= value list
         (inv-show (make-instance 'frame :master inv))
         (inv-vsb (make-instance 'scrollbar :master inv-show :orientation "vertical"))
         (inv-list (make-instance 'listbox :master inv-show :height 10 :width 35))
         )
      (set-geometry-xy ip *editor-left* *editor-top*) 
      
      (grid top 0 0 :sticky "ew")
      (pack context :side :right )
      (pack context-name :side :right)
      
      ;; create a variant of the label style
      (format-wish 
       "ttk::style configure Center.TLabel -font {Helvetica 16} -justify center -anchor center
      ttk::style layout Center.TLabel {Label.label -sticky ew}")
      ;;=== header 
      (grid head 1 0 :sticky "ew")         
      (format-wish
       "pack [ttk::label ~A.classname -text ~S -width 50 -style Center.TLabel] -pady 5 -fill both -expand 1"
       (widget-path head) 
       (car (moss::send-no-trace inv-id 'moss::=instance-name)))
      
      ;;=== value area
      (grid inv 2 0 :sticky "ew")
      
      ;;= value list
      (grid inv-show 0 0)
      (grid inv-list 0 0 :padx 5)
      (grid inv-vsb 0 1 :sticky "ns")
      (configure inv-vsb :command (format nil "~A yview" (widget-path inv-list)))
      (configure inv-list :yscrollcommand  (format nil "~A set" (widget-path inv-vsb)))
      ;; populate list
      (ip-fill-details ip inv-list current-entity inv-id)
      )))
#|
 (catch :error
  (with-nodgui (:debug 3) 
    (moss::with-package :family
     (make-inverse-details-pane f::_jpb 'f::$s-person-brother.of :dummy :dummy))))
|#
;;;---------------------------------------------------------- IP-FILL-DETAILS

(defun ip-fill-details (ip inv-list entity-id inv-id &key init)
  "fills the list with the values associated to prop-id for entity entity-id"
  (declare (ignore ip init))
  (with-ap entity-id
    (let ((raw-values (moss::send entity-id 'moss::=get-id inv-id))
          vals)
      
      ;; put them into the right format
      (setq vals (mapcar #'(lambda (xx) 
                             (let ((res (car (moss::send xx 'moss::=summary))))
                               (if (moss::%%is-id? res)
                                   (moss::>>f res)
                                   res)))
                         raw-values))
      (listbox-insert inv-list 0 vals))))

;;;==========================================================================
;;;                             ADD-ATTRIBUTE-PANE
;;;==========================================================================
;;; The pane is meant to add a new attribute either that is part of the concept
;;; attributes but has no value yet, or is not part of the concept attributes 
;;; and can be added to the instance (as a generic property)

;;;=============================== Pane object =============================

(defclass add-attribute-pane (toplevel)
  ((new-values :accessor new-values :initform nil)))

;;;-------------------------------------------------- MAKE-ADD-ATTRIBUTE-PANE
;;; the edit window that called the pane must contain a slot referring to
;;; an editing box for saving values of objects before they are modified and 
;;; the list of modified objects

(defun make-add-attribute-pane (current-entity master att-tree)
  "build a list of properties that can be added.
  Arguments:
  current-entity: id of object to be displayed
  master: the edit window ew that called the pane (calling editor)
  att-tree: attribute pane of the calling editor
  Return:
  nil."
  (declare (ignore master))
  (with-ap current-entity
    (print `(make-inverse-pane ===> *package* ,*package*))
    (let*
        ((aap (make-instance 'add-attribute-pane :width *editor-width* 
                :title "ADD ATTRIBUTE Pane"))
         ;;=== top area
         (top (make-instance 'frame :master aap :width *browser-width* :padding "5"))
         ;;=== header
         (head (make-instance 'frame :width *browser-width* :master aap))
         
         ;;=== work area
         (wk (make-instance 'frame :master aap :width *browser-width* :padding "5"))
         
         ;;= list of concept attributes
         (aap-show (make-instance 'frame :master wk))
         (aap-title (make-instance 'label :master aap-show :text "Additional Attributes"))
         (aap-vsb (make-instance 'scrollbar :master aap-show :orientation "vertical"))
         (aap-list (make-instance 'listbox :master aap-show :height 10 :width 35))
         ;;= select button
         (aap-sel (make-instance 'button :master wk :text "SELECT"
                    :command (lambda () 
                               (aap-select aap aap-list current-entity att-tree))))
         )
      (set-geometry-xy aap *editor-left* *editor-top*) 
      
      (grid top 0 0 :sticky "ew")
      ;; create a variant of the label style
      (format-wish 
       "ttk::style configure Center.TLabel -font {Helvetica 16} -justify center -anchor center
      ttk::style layout Center.TLabel {Label.label -sticky ew}")
      ;;=== header 
      (grid head 1 0 :sticky "ew")         
      (format-wish
       "pack [ttk::label ~A.classname -text ~S -width 50 -style Center.TLabel] -pady 5 -fill both -expand 1"
       (widget-path head) 
       "ADDING A NEW ATTRIBUTE")
      
      ;;=== value area
      (grid wk 2 0 :sticky "ew")
      (grid aap-title 0 0)
      ;;= concept attributes
      (grid aap-show 1 0 :rowspan 5)
      (grid aap-list 1 0 :padx 5)
      (grid aap-vsb 1 1 :sticky "ns")
      (configure aap-vsb :command (format nil "~A yview" (widget-path aap-list)))
      (configure aap-list :yscrollcommand  (format nil "~A set" (widget-path aap-vsb)))
      ;; populate list
      (aap-fill-values aap aap-list current-entity)
      ;;= Select button
      (grid aap-sel 1 2 :padx 5)
      )))

#|
(moss::with-package :family
  (setq *browser-left* 555 *browser-top* 250 *browser-width* 520 *context* 0
        *version-graph* '((0)) *list-area-height* 5)
  (catch :error
    (with-nodgui (:debug 3) 
      (moss::with-package :family
        (make-add-attribute-pane f::_jpb :dummy :at)))))
|#
;;;---------------------------------------------------------- AAP-FILL-VALUES

(defun aap-fill-values (aap aap-list entity-id)
  "fills the list with the attributes not part of the concept"
  (with-ap entity-id
    (let ((concept-attributes (moss::%%get-all-class-attributes 
                               (car (moss::%type-of entity-id))))
          gen-attributes att-names vals)
      ;; get the list of attributes that are not part of the current concept
      (setq vals (set-difference (moss::%get-all-attributes) concept-attributes))
      ;; we must remove all attributes that are not generic 
      (setq vals 
            (remove-if #'(lambda (xx) (not (moss::%is-generic-property-id? xx)))
                       vals))
      ;; and remove generic attributes that correspond to the current concept 
      ;; attributes
      ;; get generic attributes of concept attributes
      (setq gen-attributes 
            (mapcar #'(lambda (xx) (car (reverse (moss::%ancestors xx))))
                    concept-attributes))
      (setq vals (set-difference vals gen-attributes))
      ;; save id-list
      (setf (new-values aap) vals)
      ;; get names
      (setq att-names 
            (mapcar #'(lambda (xx) (car (moss::send xx 'moss::=instance-name)))
                    vals))
      (listbox-insert aap-list 0 att-names))))

;;;--------------------------------------------------------------- AAP-SELECT

(defun aap-select (aap aap-list entity-id att-tree)
  "select one of the attributes in the posted list"
  (with-ap entity-id
    (let ((index (car (listbox-get-selection-index aap-list)))
          att-id)
      ;; get the selected attribute
      (setq att-id (nth index (new-values aap)))
      ;; add it to the edited object with a "?" value
      (moss::add-values entity-id att-id '("?"))
      ;; must add the new property and the new value
      ;; add the prop name to the tree
      (format-wish "~A insert {} end -id ~A -text {~A}" (widget-path att-tree)
                   att-id (car (moss::send att-id 'moss::=instance-name)))
      ;; post the result into the value column of the tree
      (format-wish "~A set ~A value {~A}" (widget-path att-tree) att-id
                   (format nil "~{~A~^, ~}" '("?")))
      ;; quit
      (destroy aap)
      )))

;;;==========================================================================
;;;                             ADD-RELATION-PANE
;;;==========================================================================
;;; The pane is meant to add a new relation either that is part of the concept
;;; relations but has no value yet, or is not part of the concept relations 
;;; and can be added to the instance (as a generic property)
;;; generic relations link any object to any object

;;;=============================== Pane object =============================

(defclass add-relation-pane (toplevel)
  ((new-values :accessor new-values :initform nil)))

;;;--------------------------------------------------- MAKE-ADD-RELATION-PANE

;;; the edit window that called the pane must contain a slot referring to
;;; an editing box for saving values of objects before they are modified and 
;;; the list of modified objects

(defun make-add-relation-pane (current-entity master rel-tree)
  "build a list of properties that can be added.
  Arguments:
  current-entity: id of object to be displayed
  master: the edit window ew that called the pane (calling editor)
  rel-tree: relation pane of the calling editor
  Return:
  nil."
  (declare (ignore master))
  (with-ap current-entity
    (print `(make-add-relation-pane ===> *package* ,*package*))
    (let*
        ((arp (make-instance 'add-relation-pane :width *editor-width* 
                :title "ADD RELATION Pane"))
         ;;=== top area
         (top (make-instance 'frame :master arp :width *browser-width* :padding "5"))
         ;;=== header
         (head (make-instance 'frame :width *browser-width* :master arp))
         
         ;;=== work area
         (wk (make-instance 'frame :master arp :width *browser-width* :padding "5"))
         
         ;;= list of concept relations
         (arp-show (make-instance 'frame :master wk))
         (arp-title (make-instance 'label :master arp-show :text "Additional Relations"))
         (arp-vsb (make-instance 'scrollbar :master arp-show :orientation "vertical"))
         (arp-list (make-instance 'listbox :master arp-show :height 10 :width 35))
         ;;= select button
         (arp-sel (make-instance 'button :master wk :text "SELECT"
                    :command (lambda () 
                               (arp-select arp arp-list current-entity rel-tree))))
         )
      (set-geometry-xy arp *editor-left* *editor-top*) 
      
      (grid top 0 0 :sticky "ew")
      ;; create a variant of the label style
      (format-wish 
       "ttk::style configure Center.TLabel -font {Helvetica 16} -justify center -anchor center
      ttk::style layout Center.TLabel {Label.label -sticky ew}")
      ;;=== header 
      (grid head 1 0 :sticky "ew")         
      (format-wish
       "pack [ttk::label ~A.classname -text ~S -width 50 -style Center.TLabel] -pady 5 -fill both -expand 1"
       (widget-path head) 
       "ADDING A NEW RELATION")
      
      ;;=== value area
      (grid wk 2 0 :sticky "ew")
      (grid arp-title 0 0)
      ;;= concept attributes
      (grid arp-show 1 0 :rowspan 5)
      (grid arp-list 1 0 :padx 5)
      (grid arp-vsb 1 1 :sticky "ns")
      (configure arp-vsb :command (format nil "~A yview" (widget-path arp-list)))
      (configure arp-list :yscrollcommand  (format nil "~A set" (widget-path arp-vsb)))
      ;; populate list
      (arp-fill-values arp arp-list current-entity)
      ;;= Select button
      (grid arp-sel 1 2 :padx 5)
      )))

#|
(moss::with-package :family
  (setq *browser-left* 555 *browser-top* 250 *browser-width* 520 *context* 0
        *version-graph* '((0)) *list-area-height* 5)
  (catch :error
    (with-nodgui (:debug 3) 
      (moss::with-package :family
        (make-add-relation-pane f::_jpb :dummy :at)))))
|#
;;;---------------------------------------------------------- ARP-FILL-VALUES

(defun arp-fill-values (arp arp-list entity-id)
  "fills the list with the relations not part of the concept"
  (with-ap entity-id
    (let ((concept-relations (moss::%%get-all-class-relations 
                              (car (moss::%type-of entity-id))))
          gen-relations rel-names vals)
      ;; get the list of relations that are not part of the current concept
      (setq vals (set-difference (moss::%get-all-relations) concept-relations))
      ;; we must remove all relations that are not generic 
      (setq vals 
            (remove-if #'(lambda (xx) (not (moss::%is-generic-property-id? xx)))
                       vals))
      ;; and remove generic relations that correspond to the current concept 
      ;; relations
      ;; get generic relations of concept relations
      (setq gen-relations 
            (mapcar #'(lambda (xx) (car (reverse (moss::%ancestors xx))))
                    concept-relations))
      (setq vals (set-difference vals gen-relations))
      ;; save id-list
      (setf (new-values arp) vals)
      ;; get names
      (setq rel-names 
            (mapcar #'(lambda (xx) (car (moss::send xx 'moss::=instance-name)))
                    vals))
      (listbox-insert arp-list 0 rel-names))))

;;;--------------------------------------------------------------- ARP-SELECT
;;; we create here an orphan to be able to link the new relation when assigning
;;; it to the current entity

(defparameter *x* nil)

(defun arp-select-make-joker ()
  "creates an orphan with name \"?\" to connect temporarily to the entity being 
  edited when we add a new relation"
  (declare (special *x*))
  (print `("arp-select-make-joker *package*" ,*package*))
  (unless (and (moss::<<boundp *x*) *x*)
    ;; create orphan (vars must start with underscore)
    (setq *x* (moss::defobject  ("name" "?")))
    ;; create method to display it
    (moss::defownmethod moss::=summary *x* ()
      (moss::send moss::*self* 'moss::=get "name"))
    )
  ;; return the existing or newly created orphan
  *x*)


;(trace arp-select-make-joker)

(defun arp-select (arp arp-list entity-id rel-tree)
  "select one of the relations in the posted list"
  (with-ap entity-id
    (let ((index (car (listbox-get-selection-index arp-list)))
          rel-id suc-id)
      ;; create joker object if it does not exist, assign it to suc-id
      (setq suc-id (arp-select-make-joker))
      ;; get the selected attribute
      (setq rel-id (nth index (new-values arp)))
      ;; add it to the edited object with a "?" value
      (moss::add-values  entity-id rel-id (list suc-id) :no-warning t)
      ;; must add the new property and the new value
      ;; add the prop name to the tree
      (format-wish "~A insert {} end -id ~A -text {~A}" (widget-path rel-tree)
                   rel-id (car (moss::send rel-id 'moss::=instance-name)))
      ;; post the result into the value column of the tree
      (format-wish "~A set ~A value {~A}" (widget-path rel-tree) rel-id
                   (format nil "~{~A~^, ~}"  
                           (moss::send suc-i 'moss::=summary)))
      ;; quit
      (destroy arp)
      )))

#|
(moss::with-package :family
  (setq *browser-left* 555 *browser-top* 250 *browser-width* 520 *context* 0
        *version-graph* '((0)) *list-area-height* 5)
 (catch :error
  (with-nodgui (:debug 3) 
    (moss::with-package :family
     (make-editor-window f::_jpb)))))
|#

:EOF