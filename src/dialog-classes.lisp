;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;20/10/29
;;;		D I A L O G - C L A S S E S (File dialog-classes.lisp)
;;;
;;;=============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#

#|
This file contains classes and attached methods for setting up a dialog.
The dialog occurs in the :moss space.
Because the Tcl/Tk windows are blocking, we must run the conversation in
the MOSS window.

Process
-------
The user types something into the text pane of the MOSS window in HELP mode
When the request finishes with a dot or a question mark, then the result is
pased to the crawler
 - if it is the beginning of conversation, then the conversation object is 
   initialized
 - then, the input is analyzed and a transition is computed; each node has 2
   methods: =EXECUTE and =RESUME
 - if an output is produced, then it is posted in the window and the crawler
   returns
The conversation graph must be prepared before being traversed, although it
could be possible to dynamically modify it. A conversation has a HEADER and
several STATES


2019
 0409 creation from the ACL file. The oldversion had a features including
      goals and actions that would trigger questions from MOSS automatically
      until all paramters for the final question would be filled. This feature
      has been temporarily removed.
|#

(in-package :moss)

#|
(eval-when (:compile-toplevel :load-toplevel :execute)
  (export '(deftask)))
|#

;;;=================================== macros ==================================
;;; debugging macros
(defMacro ps (obj-id) `(send ',obj-id '=print-self))
(defMacro pe (obj-id) `(%pep ',obj-id))

;;;================================================================================
;;;
;;;             Minimal MOSS Query Conversation Graph
;;;
;;;================================================================================
;;; Here we define the possibility of querying MOSS as a state graph. 

;;; The object state specifies a node of the expected conversation. The conversation
;;; object records the current state and the content of the Fact Base in the FACTS
;;; slot.
;;; At a given state we ask something to the user, try to make sense out of it using 
;;; rules, and do a transition. 
;;;--------------------------------------------------------------------- MOSS-STATE

(defsysclass "MOSS-STATE"
  (:id $QSTE)
  (:doc 
   :en "A MOSS-STATE models a particular state of a state graph which defines ~
         the possible sequences corresponding to a plan and to a dialog. ~
         A state graph implements a complex skill of an agent.")
  (:att MOSS-STATE-NAME (:id $STNAM) (:entry))
  (:att MOSS-LABEL (:entry)(:id $LBLT))
  (:att MOSS-EXPLANATION (:id $XPLT)) ; text to be posted to a human reader
  )

;;;------------------------------------------------------------- MOSS-DIALOG-HEADER

(defsysclass "MOSS-DIALOG-HEADER"
  (:doc :en "An object used to identify a dialog.")
  (:id $QHDE)
  (:att MOSS-LABEL (:id $LBLT)(:entry))
  (:att MOSS-EXPLANATION (:id $XPLT)) ; text to be posted to a human sender
  (:rel MOSS-ENTRY-STATE MOSS-STATE (:id $ESTS)) ;  link onto the entry-state
  (:rel MOSS-PROCESS-STATE MOSS-STATE (:id $PRSS)) ; process state entry
  )

;;;---------------------------------------------------------------- MOSS-TASK-INDEX

(defsysclass "MOSS-TASK-INDEX"
  (:id $TIDXE)
  (:doc "a TASK INDEX contains an expression and a weight.")
  (:att "MOSS INDEX" (:id $IDXT))
  (:att "MOSS WEIGHT" (:id $WGHT))
  )

;;;---------------------------------------------------------------------- MOSS-TASK

(defsysclass "MOSS-TASK"
  (:id $QTSKE)
  (:doc "A TASK is a model of task used to trigger sub-dialogs.")
  (:att "MOSS task name" (:id $TSKNAM)(:unique)(:entry))
  (:att "MOSS target" (:id $TRGT) (:one-of :object :time :cause :subject :owner))
  (:att "MOSS performative" (:id $PRFT)(:one-of :request :assert :command :answer))
  (:att "MOSS dialog" (:id $DLGT))
  (:rel "MOSS index pattern" MOSS-TASK-INDEX (:id $IDXS))
  )

;;;-------------------------------------------------------------- MOSS-CONVERSATION
;;; the conversation object is the interface between the MOSS dialog system and 
;;; the external world. It contains a number of fields to exchange information.
;;; WEB I/O
;;;  MOSS-WEB if true means that the request came from a web process. Should be set
;;;    by the web process and removed when the answer is provided
;;;  MOSS-WEB-GATE gate to allow the web process to resume when it is opened
;;;  MOSS-WEB-TEXT part of the HTML text to sent back to user in the body of the 
;;;    page.

(defsysclass "MOSS-CONVERSATION"
  (:doc :en "An object whose instances represent a conversation and points to the ~
              stage of this conversation. A new conversation starts with the ~
              creation of an instance of this class, e.g. after a reset.")
  (:id $CVSE)
  (:att MOSS-AGENT (:id $AGT)) ; used by OMAS to record the PA
  (:att MOSS-ANSWER-SEM (:id $AST)) ; semaphore used by OMAS for answers
  (:att MOSS-CONTACT (:id $CTCT)) ; used to synchronize dialog answers
  (:rel MOSS-DIALOG-HEADER MOSS-DIALOG-HEADER (:id $DHDRS)) ; main conversation header
  (:att MOSS-FACTS (:id $FCT)) ; to replace the context object
  (:att MOSS-FRAME-LIST (:id $FLT)) ; list of return addresses of sub-dialogs
  (:att MOSS-GOAL (:id $GLT))  ; unused
  (:att MOSS-INPUT-WINDOW (:id $IWS)) ; panel from which to read master's data
  (:rel MOSS-INITIAL-STATE MOSS-STATE (:id $ISTS)) ; initial state
  (:att MOSS-INTERRUPT (:id $INTT)) ; if T, allows interrupting a dialog and start a 
  ; new one. This is checked by the dialog-state-crawler to decide to interrupt or not
  ; must be reset to avoid looping
  (:att MOSS-LABEL (:id $LBLT))
  (:att MOSS-OUTPUT-WINDOW (:id $OWS)) ; panel in which to write questions
  (:rel MOSS-STATE MOSS-STATE (:id $STS)) ; current-state of the conversation
  (:rel MOSS-SUB-DIALOG-HEADER MOSS-DIALOG-HEADER (:id $SDHDRS)) ; current sub-conversation
  (:rel MOSS-TASK MOSS-TASK (:id $TSKS)) ; current task
  (:rel MOSS-TASK-LIST MOSS-TASK (:id $TSKL)) ; list of potential tasks for dialog
  (:att MOSS-TEXT-LOG (:id $TXLT)) ; record conversation texts
  (:att MOSS-TODO-SEM (:id $TDST)) ; semphore used to synchonize dialog inputs
  )

;;;================================================================================
;;;
;;;                             Actions and Patterns
;;;
;;;================================================================================
;;; Here we define classes for modeling actions and patterns
#|

;;;----------------------------------------------------------------------- ARGUMENT
;;; start with model of argument of a pattern corresponding to an action

(defsysclass MOSS-TASK-ARGUMENT
  (:id $TARGE)
  (:doc :en "MOSS-TASK-ARGUMENT represents one of the arguments that must be filled without ~
              which an action or a task cannot be launched. The requirement however ~
              is not strict since the argument may be optional.
   An argument contains a name, one or more questions to ask the user when it is ~
              missing, a validation function, room for a value, it has a name.")
  (:att MOSS-ARG-NAME (:id $ARNAM)(:entry))
  (:att MOSS-ARG-KEY (:id $ARKT))
  (:att MOSS-ARG-VALUE (:id $ARVT))
  (:att MOSS-ARG-QUESTION (:id $ARQT))
  )

;;;------------------------------------------------------------------- OBJ-ARGUMENT

(defsysclass MOSS-OBJ-ARGUMENT
  (:id $OBRGE)
  (:doc :en "MOSS-OBJ-ARGUMENT is a special type of argument whose value must be a MOSS ~
              object. It may have a CLASS specification, which defaults to *any*. ~
              It has a filtering method on the VALUE argument that tests whether ~
              the argument if of the proper class.")
  (:att MOSS-REFERENCE (:id $REFT) (:vr (:all *any*)))
  (:att MOSS-CLASS-REFERENCE (:id $CREFT)(:default *any*))
  )

;;;------------------------------------------------------------------------- ACTION

(defsysclass MOSS-ACTION
  (:id $ACTE)
  (:doc :en "MOSS-ACTION represents all actions that can be executed by MOSS. For example, ~
              explaining a term or detailing a procedure. It contains an operator ~
              reference (function name) and a list of arguments to be given as ~
              keywords when calling the function.")
  (:att MOSS-ACTION-NAME (:id $ANAM)(:entry))
  (:att MOSS-DOCUMENTATION (:id $DOCT))
  (:att MOSS-OPERATOR (:id $OPRT))
  (:rel MOSS-OPERATOR-ARGUMENTS MOSS-TASK-ARGUMENT (:id $ARGS))
  )

;;; Add ACTION to Conversation header 
;;; a conversation header points to a particular sub-class of action
(defsyssp MOSS-ACTION MOSS-DIALOG-HEADER  MOSS-ENTITY (:id $ACTS))
;;; a conversation object points to an instance of the sub-class of action
;;; created by the =create-pattern method
(defsyssp MOSS-GOAL MOSS-CONVERSATION MOSS-ACTION (:id $GLS))

;;;==================================== methods ===================================

;;;------------------------------------------ (MOSS-CONVERSATION) =CLEAN-OUPUT-PANE

(defmossinstmethod =clean-output-pane MOSS-CONVERSATION ()
  "cleans output pane no matter what"
  (send *self* '=display-text "" :clean-pane t))


|#
;;;---------------------------------------------- (MOSS-CONVERSATION) =DISPLAY-TEXT

(defmossinstmethod 
    =display-text MOSS-CONVERSATION (text-arg &key erase new-line (final-new-line t)
                                              newline clean-pane html
                                              ;(color *black-color*)
                                              (header ""))
  "displays text using the output conversation channel. When the text is a multi- ~
  lingual string, extract the proper language version.
  Arguments:
  text-arg: text or list of texts to display (possibly multilingual strings)
  clean-pane (key): it T, erase output pane no  matter what
  color (key): a standard color e.g. moss::*red-color*
  erase (key): if T, erases the area (prints null string)
  header (key): leading header if any (default is no header)
  html (key): if t indicates that the string is an HTML string
  newline (key): if t print inserting a newline
  Return:
  nil."
  (let ((output-pane (car (HAS-MOSS-OUTPUT-WINDOW)))
        output-text)
    ;; get text as a simple string
    (setq output-text 
          (cond
           ;; NB: HTML string is a string
           ((stringp text-arg) text-arg)
           ;; if MLN, get the one associated with current language
           ((mln::mln? text-arg)  ; jpb 1406
            (or (mln::filter-language text-arg *language*) ""))
           ;; list of strings
           ((and (listp text-arg) (every #'stringp text-arg))
            (nth (random (length text-arg)) text-arg))
           ;; list of MLN
           ((and (listp text-arg) (every #'mln::mln? text-arg)) ; jpb 1406
            (or (mln::filter-language
                 (nth (random (length text-arg)) text-arg)
                 *language*)
                ""))
           ;; if a list, but not of strings or mlns, coerce the list (to print smth)
           ((listp text-arg) 
            (format nil "~{~S~% ~}" text-arg))
           ;; otherwise error
           (t (warn "bad text argument: ~S" text-arg) "")))
    
    ;;=== here send regular text to a window pane
    ;; print a trace into the listener
    (if new-line
        (format t (concatenate 'string "~%" output-text))
        (format t output-text)
        )
    
    ;(format t "~%; CONVERSATION/=display-text /ag: ~S." (car (has-moss-agent)))
    ;(format t "~%; CONVERSATION/=display-text /no-window: ~S." 
    ;        (omas::no-window (car (has-moss-agent))))
    ;(format t "~%; CONVERSATION/=display-text /test: ~S." 
    ;        (and (setq ag (car (has-moss-agent))) (eql (omas::no-window ag) :web)))
    #+OMAS
    (let* ((ag (car (has-moss-agent))))           
      (setf (omas::answer ag) output-text)
      ;; resume the dialog (error if cannot get semaphore)
      (signal-semaphore (omas::get-answer-sem-from-cvs ag *self*))
      (throw :return nil))

    ;;=== back to MOSS environment
    ;;== no window to print info, quit
    (unless output-pane
      (format t "~%; CONVERSATION/=display-text /no output window and no web mark.")
      (throw :return nil))

    (unless (eql output-pane t) ; if output-pane is t, then already printed
      ;; here we assume that the output-window slot of the conversation object 
      ;; contains the window object and that the window object has a display-text 
      ;; method
      (display-text output-pane output-text :erase erase :newline newline
                    :final-new-line final-new-line :header header :html html
                    :clean-pane clean-pane )) ;:color color)) ; JPB1104 color       
      nil))
#|      		   
;;;----------------------------------------------------- (MOSS-CONVERSATION) =RESET

(defmossinstmethod =reset MOSS-CONVERSATION ()
  "reset does several things:
     - clean the FACTS area in MOSS-CONVERSATION
     - doe not modify web parameters
     - puts a new goal (action pattern) into the GOAL slot of conversation"
  (let ((action (car (HAS-MOSS-ACTION (car (HAS-MOSS-DIALOG-HEADER)))))
        )
    (setf (HAS-MOSS-FACTS *self*) nil)
    ;; set a new goal specified by the type of action in the dialog header
    (if action
      (send *self* '=replace 'HAS-MOSS-GOAL
            (list (send action '=create-pattern)))
      ;; otherwise should delete everything
      (progn
        (send *self* '=get 'HAS-MOSS-GOAL)
        (send *self* '=delete-sp 'HAS-MOSS-GOAL *answer*))
      )))

;;;--------------------------------------------------------- (MOSS-ACTION) =SUMMARY

(defmossinstmethod =summary MOSS-ACTION ()
  "returns the key, name and value of the argument."
  (append (HAS-MOSS-ACTION-NAME) '(":") (HAS-MOSS-OPERATOR) 
          (broadcast (HAS-MOSS-OPERATOR-ARGUMENTS) '=summary)))

;;;-------------------------------------------------- (MOSS-TASK-ARGUMENT) =SUMMARY

(defmossinstmethod =summary MOSS-TASK-ARGUMENT ()
  "returns the key, name and value of the argument."
  (append (HAS-MOSS-ARG-KEY) '(":") (HAS-MOSS-ARG-NAME) 
          (or (HAS-MOSS-ARG-VALUE) '("*unspecified*"))))

;;;--------------------------------------------------- (MOSS-OBJ-ARGUMENT) =SUMMARY

(defmossinstmethod =summary MOSS-OBJ-ARGUMENT ()
  "returns the key, name and value of the argument."
  (append (HAS-MOSS-ARG-KEY) '(":") (HAS-MOSS-ARG-NAME) 
          (or (HAS-MOSS-ARG-VALUE) '("*unspecified*"))
          '("/ CLASS-REFERENCE :")(or (HAS-MOSS-CLASS-REFERENCE)'("*unspecified*"))
          '("/ FILTER :") (or (HAS-MOSS-REFERENCE)'("*unspecified*"))))
|#

;;;---------------------------------------------------------- (MOSS-STATE) =SUMMARY

(defmossinstmethod =summary MOSS-STATE ()
  "Returns the state label to be printed."
  (HAS-MOSS-LABEL))


#|
;;;================================================================================
;;;
;;;                             Actions and Patterns
;;;
;;;================================================================================
;;; Here we define classes for modeling actions and patterns

;;;------------------------------------------------------------- (ARGUMENT) =FILTER
;;; start with model of argument of a pattern corresponding to an action

(defmossownmethod =FILTER $TARGE (input)
  "takes a user input and tries to recover some value corresponding to the argument. ~
   This method has to be redefined for each type of argument..."
  input)

;;;------------------------------------------------------------- (OBJ-ARGUMENT) =XI

(defmossownmethod =xi $CREFT (value)
  "when the value has the right type we return it, otherwise we return nil."
  (if (%type? value (car (HAS-MOSS-CLASS-REFERENCE))) value))

;;;------------------------------------------------------------- (ACTION) =ACTIVATE

(defmossinstmethod =activate MOSS-ACTION ()
  "takes an action instance builds a message and sends it to self. Thus, action ~
   message should be defined at the action level."
  (apply #'send *self*
         (car (HAS-MOSS-OPERATOR))  ; message name
         (reduce #'append      ; arguments (keys)
                 ;; for each argument we build the pair key value
                 (mapcar #'(lambda (xx) (cons (car (HAS-MOSS-ARG-KEY xx))
                                              (HAS-MOSS-ARG-VALUE xx)))
                         (HAS-MOSS-OPERATOR-ARGUMENTS)))))

;;;------------------------------------------------------- (ACTION) =CREATE-PATTERN

(defmossownmethod =create-pattern $ACTE ()
  "create a pattern ACTION <OPR, ARG*> to be filled as a conversation goal.
Arguments:
   none
Return:
   the id of the action."
  
  (let* ((sp (%%get-id 'HAS-MOSS-OPERATOR-ARGUMENTS :prop :class-ref *self* 
                       :include-moss t))
         (suc-list (HAS-MOSS-SUCCESSOR sp))
         nn lres key)
    ;; for each successor in the list build the required number of argument objects
    (dolist (suc suc-list)
      ;; first get max number of args
      (setq nn (car (HAS-MOSS-MAXIMAL-CARDINALITY suc)))
      ;; if nil; default is one
      (dotimes (jj (if (numberp nn) nn 1))
        ;; build an argument
        (push (%%make-instance-from-class-id suc) lres)))
    ;; build the ACTION object
    (setq key (%%make-instance-from-class-id *self*))
    ;; add list of arguments
    (send key '=add-sp 'HAS-MOSS-OPERATOR-ARGUMENTS (reverse lres))
    ;; return key
    key))

;;;------------------------------------------------ (ACTION) =DISPLAY-DOCUMENTATION

(defmossinstmethod =display-documentation MOSS-ACTION (&key obj-id)
  "prints the documentation associated with the object using the output conversation ~
   channel.
Arguments:
   obj-id (key): object of interest
Return:
   nil."
  (let ((conversation (car (send *self* '=get-id '$gls.of)))
        (text (or (car (send obj-id `=has-value-id '$DOCT))
                  "*No documentation available*")))
    ;; we assume that we can get the conversation object
    ;; doc
    (when (%pdm? conversation)
      (send conversation '=display-text 
            (concatenate 'string
                         (or (car (send obj-id '=summary)) (symbol-name obj-id))
                         "~%")
            :no-new-line t)
      ;; required because text may be multlingual
      (send conversation '=display-text  (format nil "~A" text)))))

;;;------------------------------------------------------- (ACTION) =DISPLAY-OBJECT

(defmossinstmethod =display-object MOSS-ACTION (&key obj-id)
  "prints the object using the output conversation channel.
Arguments:
   obj-id (key): object of interest
Return:
   nil."
  (let ((conversation (car (send *self* '=get-id '$gls.of))))
    ;; we assume that we can get the conversation object
    ;; doc
    (when (%pdm? conversation)
      (send obj-id '=print-self :stream (car (has-MOSS-output-window conversation))))))

;;;--------------------------------------------------------------- (ACTION) =READY?

(defmossinstmethod =ready? MOSS-ACTION ()
  "checks that all args are filled."
  (every #'(lambda(xx) xx) (mapcar #'HAS-MOSS-ARG-VALUE (HAS-MOSS-OPERATOR-ARGUMENTS))))

;;;------------------------------------------------------------------ %MAKE-PATTERN

(defUn %make-pattern ()
  "builds a set of objects representing an action pattern from the action"
  :to-do)

|#

:EOF