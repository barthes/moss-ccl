;;;-*- Mode: Lisp; Package: "MOSS" -*-
;;;=============================================================================
;;;19/10/18
;;;		O N L I N E - D O C (File online-doc.lisp)
;;;		
;;;=============================================================================
#|
Copyright: Barthès@HEUDIASYC, CNRS, Université de Technologie de Compiègne (July 2019)

barthes@utc.fr

This software is a computer program whose purpose is to allow developing
prototyped systems of cognitive agents. It includes a knowledge representation
system for creating ontologies.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
|#
;;; multilingual approach
;;; If the documentation has to be provided in several languages, then we create 
;;; several DOCUMENTATION objects: EN-DOCUMENTATION, FR-DOCUMENTATION, etc.
;;; the value of the global variable *language will determine the right object.
;;; This approach obliges to modify the =get-id method associated with the $DOCS
;;; property so that we select the right object.
;;; Another way to do that would be to define different configurations, one for 
;;; each language, of to introduce advise functions for the display...
#|
This file contains a set of documentation objects for answering users' questions
about how MOSS operates.

2005
 0504 creation
 0510 adding the possibility of linking a doc object to a MOSS object 
2006
 MOSS v6
 =======
 0213 Multiple documentation objects are replaced with a single object (concept)
      using multilingual types.
2007
 0824 Introducing a distinction between MCL and MICROSOFT for the French accents
      This file may replace the previous one without problem
      Adding a SORRY object
2008
 0621 === v7.0.0
2010
 0917 === v8.0.0
      introducing MOSS- prefix
 1002 orrecting a few definitions
2014
 0307 -> UTF8 encoding
|#

;;;********** There is a possibility that doc entries will crush MOSS variables
;;; thus se should produce a special =make-entry function producing entry-points
;;; like !XXX
;;; Another solution is to use a different package e.g. :moss-doc

(in-package :moss)

;;;=================================== macros ==================================

(defMacro doc (item text &key obj-id)
  "a macro to produce documentation objects.
Arguments:
   item: a string that will qualify the documentation item and produce an entry
         point
   test: the associated text
   obj-id (key): id of object being documented
Return:
   the identifier of the documentation item."
  (let ((title-value (mln::make-mln item)))
    `(let ((doc-object
            (defindividual MOSS-DOCUMENTATION        
                ("MOSS-TITLE" ,title-value)
              ("MOSS-DOCUMENTATION" ,(mln::make-mln text))))
           ep-list)
       ;; we must export entry points
       (setq ep-list 
             (%make-entry-symbols ',title-value :type '$DOCE))
       (export ep-list)
       ,@(if obj-id `((%link  ',obj-id '$DOCS doc-object)))
       doc-object)))

;;;(defMacro doc (item text &key obj-id)
;;;  "a macro to produce documentation objects.
;;;Arguments:
;;;   item: a string that will qualify the documentation item and produce an entry
;;;         point
;;;   test: the associated text
;;;Return:
;;;   the identifier of the documentation item."
;;;  `(let ((doc-object
;;;          (defindividual MOSS-DOCUMENTATION        
;;;            ("MOSS-TITLE" ,@(list (if (listp item) item (list item))))
;;;            ("MOSS-DOCUMENTATION" ,text)))
;;;         ep-list)
;;;     ;; we must export entry points
;;;     (setq ep-list (%make-entry-symbols (car (HAS-MOSS-TITLE doc-object)) :type '$DOCE))
;;;     (export ep-list)
;;;     ,@(if obj-id `((%link  ',obj-id '$DOCS doc-object)))
;;;     doc-object))
;;;================================== class ====================================
;;; Class DOCUMENTATION is defined in mossboot

(defmossinstmethod =summary MOSS-DOCUMENTATION ()
  "return a list containing the title of the documentation object"
  (list (mln::filter-language (car (HAS-MOSS-TITLE)) *language*))) ; jpb1406

#|
? (send 'moss::$DOCE.3 '=summary)
("concept")
? (send '$DOCE.39 '=summary)
("global help")
|#
;;;=============================================================================

(doc (:en "SORRY" :fr "DESOLE")
     (:en "*sorry no documentation available*"
          :fr "*désolé, pas de documentation sur le sujet*"))

(doc (:en "HELP" :fr "AIDE")
     (:en 
       "I can give you information about the various MOSS concepts as an answer 
       to a question: 
   WHAT IS ...?
I can tell you how to operate MOSS as an answer to a question: 
   HOW TO ...?"
      :fr
       "Dans le futur je pourrai vous donner des informations sur les différents ~
       concepts utilisés par MOSS en réponse à des questions du type : 
   QU'EST-CE ...? ou PROPRIETE TERMINALE ?
Je pourrai également vous aider à utiliser MOSS en répondant à des questions du type :
   COMMENT ... ?
Pour l'instant ma documentation est en anglais...")
      )

;;;=============================================================================
;;; Answers to WHAT IS QUESTIONS

(doc (:en "AGENT")
     (:en
      "An AGENT is a specific autonomous entity that may have goals and lives in ~
       a specific environment. An agent may be situated, having a geographical ~
       position or may be intellectual. An OMAS agent may be a Service agent or ~
       a personal Assistant."))

(doc (:en "ATTRIBUTE")
     (:en 
      "the concept of ATTRIBUTE is used to qualify an object ~
       directly. For example the name of somebody, the title of a book. Associated ~
       with an ATTRIBUTE is a set of values. Thus, terminal properties are ~
       multi-valued. Each value can be used to produce an index by using a ~
       specific =make-entry method. An index needs not be unique like in databases.
   The name of the direct property is obtained by tagging a prefix HAS-, e.g. ~
       AGE will produce HAS-AGE. A filter can be associated with an ~
       ATTRIBUTE as a =xi own-method."))

(doc (:en "ATTRIBUTE")
     (:en "the generic relation HAS-ATTRIBUTE allows attaching attribute objects ~
           to classes (usually)."))

(doc (:en "CLASS ; METACLASS")
     (:en
      "a MOSS CLASS/CONCEPT is an instance of the object ENTITY (metaclass). ~
       E.g., a PERSON.
      It turns out that ENTITY is both the metaclass and the meta-metaclass. ~
       E.g. ENTITY is a model of itself or an instance of itself, which has nice ~
       properties.
   A CLASS defines the structure of an object. However, the structure is not binding ~
       in the sense that instances or individuals, may not obey that structure ~
       entirely.
   If we view a CLASS as a CONCEPT, this means that the structure is not ~
       prescriptive, but typical, contrary to most ontological definitions.
   Methods, constraints, may be attached to a CLASS/CONCEPT"))

(doc (:en "CONCEPT")
     (:en
      "A CONCEPT is something representing an object of the real world. It can 
       be abstract (an idea) or concrete (a person). If abstract, also said to 
       be defined in intension, it is usually represented by a class (a MOSS 
       ENTITY). When concrete, i.e. representing an individual, id is usually 
       represented by an instance of some class. In MOSS, it could be represented 
       as an ORPHAN, i.e. a classless object.
   In the MOSS system CONCEPT is synonym of CLASS or ENTITY."))

(doc (:en "COUNTER")
     (:en "the concept of COUNTER is an internal structure mainly used to construct ~
           identifiers by appending a numerical value to a radix."))

(doc (:en "COUNTER")
     (:en "the generic relation HAS-COUNTER is used to attach counters to classes ~
           to generate instance identifiers."))

(doc (:en "DOCUMENT")
     (:en "the generic attribute HAS-DOCUMENT is used to link an object to its ~
           documentation object."))

(doc (:en "DOCUMENTATION")
     (:en "a DOCUMENTATION is an object containing information about another MOSS ~
           object"))

(doc (:en "ENTRY-POINT")
     (:en "an ENTRY POINT is an object representing an index to other objects. ~
           It may refer to several objects of different types that share the ~
           same attribute."))

(doc (:en "IDENTIFIER")
     (:en "the generic attribute HAS-ID is a special attribute used for storing ~
           the identifier symbol within the object."))

(doc (:en "INSTANCE METHOD")
     (:en "the generic relation HAS-INSTANCE-METHOD is used to attach methods ~
           to classes. Such instance-methods will be used by instance of the ~
           class like in traditional object-oriented languages."))

(doc (:en "INVERSE")
     (:en "the generic relation HAS-INVERSE is used to link a relation to ~
           its inverse relation. It is a relation both of the relation class ~
           and of the attribute class (use to link an attribute to its ~
           entry point)."))

(doc (:en "INVERSE PROPERTY")
     (:en
      "an INVERSE-PROPERTY is what it is intuitively. For ATTRIBUTES inverse~
       properties are used in the index, for RELATIONS inverse properties are ~
       used for the inverse links."))

(doc (:en "IS-A")
     (:en "the generic relation HAS-IS-A allows specifying that a concept (class) ~
           is a sub-concept (sub-class) of another concept (class), or that a ~
           MOSS object has for prototype another MOSS object."))

(doc (:en "METHOD")
     (:en "a METHOD in MOSS can be an OWN-METHOD, an INSTANCE-METHOD, or a ~
           UNIVERSAL-METHOD.
      - an OWN-METHOD is attached to an object directly
      - an INSTANCE-METHOD is attached to a CLASS
      - a UNIVERSAL-METHOD is attached to no object in particular but applies ~
           by default to all objects.
      A METHOD is a MOSS object."))

(doc (:en "MOSS")
     (:en "MOSS is an experimental system for modeling concepts. It can be ~
           used in particular to develop ontologies and associated knowledge ~
           base. MOSS was first ~
           implemented in 1986, using a model called PDM (Property-~
           Driven Model). PDM has been refined and improved until today. ~
           PDM and MOSS were developed at UTC by Jean-Paul Barthès and his ~
           PhD students."
          ))

(doc (:en "MOSS-SYSTEM")
     (:en "MOSS-SYSTEM is a class that models a system and in particular ~
           the MOSS system."))

(doc (:en "MOSS-SYSTEM")
     (:en "the generic relation HAS-MOSS-SYSTEM is used to relate a class to ~
           a particular instance of MOSS system (internal use)."))

(doc (:en "NULL-CLASS")
     (:en "NULL-CLASS is a universal empty class defined to type orphans."))

(doc (:en "ORPHAN")
     (:en "An ORPHAN is a classless object. Its type is *none*."))

(doc (:en "MOSS-CONFIGURATION ; VERSION")
     (:en "A MOSS-CONFIGURATION is a specific version of the database that has been~
           singled out and given a name, so that the user can refer to it more ~
           easily."))

(doc (:en "MOSS-USER")
     (:en "A USER is an object that collects the characteristics of  a typical user ~
           of the database, i.e. the name, access rights last version that was ~
           accessed, configurations that were defined"))

(doc (:en "MULTI-AGENT SYSTEM ; multiagent system ; MAS")
     (:en
      "A MULTI-AGENT SYSTEM is a system involving several agents that communicate ~
       and usually cooperate to solve problems. OMAS is a platform for developing ~
       such systems."))

(doc (:en "OBJECT ; objects")
     (:en
      "a MOSS object is something with properties. It may be an instance of a class ~
       or a classless object also called an ORPHAN. An object may have other ~
       properties than those strictly described in its class. 
      An object can be used as a prototype for another object.
      An object may have versions.
      In the current implementation objects are not persistant."))

(doc (:en "OBJECT-NAME")
     (:en "the generic attribute HAS-OBJECT-NAME is used as a default name for ~
           objects. It may be useful for producing OWL identifiers."))

(doc (:en "OWN METHOD")
     (:en 
      "the concept of OWN-METHOD represents a method directly attached to an object, ~
       whether an orphan, an instance, a class, a metaclass, a method (yes you can ~
       attach methods to methods)."))

(doc (:en "PROPERTY")
     (:en 
      "a PROPERTY in the MOSS system is attached to an object. Objects may have ~
       ATTRIBUTES (also known as TERMINAL-PROPERTIES), or RELATIONS (also ~
       known as STRUCTURAL-PROPERTIES). Associated with an attribute is a set of ~
       values or literals, associated with a RELATION is a set of identifiers of ~
       MOSS objects.")
      :obj-id $EPR)

(doc (:en "REFERENCE")
     (:en "the generic attribute HAS-REFERENCE is a special attribute used ~
           for handling multiple class instances. It is similar to a forward ~
           reference pointer in Lisp."))

(doc (:en "RELATION ; relationship")
     (:en
      "the concept of RELATION is used to link an object to ~
       a set of other MOSS objects, called successors. The value associated with a RELATION is a ~
       set of object identifiers. Thus, a RELATION is multi-valued.
   All links introduced by a RELATION are automatically inverted ~
       and maintained by MOSS. The name of the direct property is obtained by ~
       tagging a prefix HAS-, e.g. BROTHER will produce HAS-BROTHER for the direct ~
       property and IS-BROTHER-OF for the inverse property."))

(doc (:en "RELATION ; relationship")
     (:en "the generic relation HAS-RELATION allows attaching relation objects ~
           to classes (usually)."))

(doc (:en "SUCCESSOR")
     (:en "the generic relation HAS-SUCCESSOR is used to define the range of~
           a relation in a class or concept. It is a relation of the concept ~
           of relation."))

(doc (:en "UNIVERSAL-CLASS")
     (:en "UNIVERSAL-CLASS is a class that can stand for any class."))

(doc (:en "UNIVERSAL METHOD")
     (:en
      "a UNIVERSAL-METHOD is a method that applies to all objects whatever their ~
       status. It is shadowed by OWN METHODS and INSTANCE METHODS."))

(doc (:en "VIRTUAL CONCEPT; VIRTUAL CLASS")
     (:en "A VIRTUAL CONCEPT in MOSS defines a view on some elements of a specific ~
           class, filtered by a constraint. For example a virtual concept ADULT ~
           can be defined to distinguish persons over 18."))

;;;=============================================================================
;;; Answers to HOW TO QUESTIONS

(doc (:en "MAKE-CLASS")
     (:en
      "to create a CLASS or CONCEPT, you can use the defconcept macro ~
       or the %make-class function. 
      E.g.,
       (defconcept PERSON
            (:att NAME (:entry)(:max 3))
            (:att AGE (:max 1))
            (:rel BROTHER (:to PERSON)))
      or
       (%make-class 'PERSON
            '(:att NAME (:entry)(:max 3))
            '(:att AGE (:max 1))
            '(:rel BROTHER (:to PERSON)))

      The detailed syntax is the following:
        (defconcept name &rest option-list) - creates a new class
      syntax of option list is
   	(:is-a <class-id>*)	      for defining inheritance
   	(:att <att-description>)	specifies attriute
   	(:rel <rel-description>)	specifies relation
   	(:doc <documentation list>)
      Options for terminal properties are:
   	(:is-a <class-id> {(:system <system-name>)} )*
   				for defining inheritance one class at a time
   	(:min <number>)         minimal cardinality
   	(:max <number>)         maximal cardinality
   	(:name <var-name>)      external variable-name
   	(:default <value>)      default, not obeying PDM2 specifications
   	(:unique)               minimal and maximal cardinality are each 1
   	(:entry {<function-descriptor>})	specify entry-point
   				if no arg uses make-entry, otherwise uses
   				specified function
      where 
   	<function-descriptor>::=<arg-list><doc><body>
   	e.g.  (:entry (value-list)(Entry for Company name)
   				  (intern(make-name value-list)))
      Options for relations are:
   	(:is-a <class-id>*)	for defining inheritance
   	(:min <number>)		minimal cardinality
   	(:max <number>)		maximal cardinality
   	(:unique)		minimal and maximal cardinality are each 1
   	(:inv <prop-name>)	inverse-property name
        (:to <class-ref>)       range of the relation
   
      Examples
   	(:att NAME (:min 1)(:max 2))
   	(:rel AUTHOR (:to BOOK))

      All names can be replaces by multilingual names or simple strings if a ~
       language has been specified. E.g.
      (defconcept \"Person\"
            (:att \"Name\" (:entry)(:max 3))
            (:att \"Age\" (:max 1))
            (:rel \"Brother\" (:to \"Person\")))
      Doing this will preserve cases in printed values of property and class ~
       names."
      ))

(doc (:en "MAKE-OBJECT ; make-orphan")
     (:en
      "to create an OBJECT you can instantiate a class using the defindividual ~
       macro or the m-make-instance function.
      E.g.,
       (defindividual PERSON
           (NAME \"Dupond\" \"Dupond-Durand\")
           (AGE 23)
           (:var _dupond-durand))
      or
       (m-make-instance 'PERSON
           '(NAME \"Dupond\" \"Dupond-Durand\")
           '(AGE 23)
           '(:var _dupond-durand))
   It is also possible to create classless objects or ORPHANS using the defobject ~
       macro or the %make-object function.
       E.g.,
        (defobject
            (NAME \"strange stuff\")
            (COLOR \"greenish\")
            (:var _strange-stuff))
       or
        (%make-object
            '(NAME \"strange stuff\")
            '(COLOR \"greenish\"))
   The :var option allows to keep the identifier of the object in the specified ~
       variable - same as doing (setq _strange-stuff (defobject ...))."))

(doc (:en "make-method")
     (:en
      "MOSS has three kinds of methods:
      - own-methods
      - instance-methods
      - universal-methods
   Each type of methods has a corresponding creation macro and function."))

(doc (:en "MAKE-OWN-METHOD")
     (:en "an OWN-METHOD is a method directly attached to an object, whether an orphan, ~
           an instance, a class, a metaclass, a method (yes you can attach methods to ~
           methods). Creating an OWN-METHOD is done with the defownmethod macro ~
           or the m-make-ownmethod function.
        E.g.,
         (defownmethod =my-print _strange-stuff ()
             (print (HAS-NAME)))
      Some remarks:
       - =my-print is the name of the method (MOSS convention is to start a method ~
           name with an = sign)
       - _strange-stuff is the name of a variable that contains the identifier of ~
           the object we want to attach the method to
       - (HAS-NAME) is an accessor (built automatically by the system that allows ~
           to get the content of the HAS-NAME slot of the object
      In more complex cases the argument corresponding to _strange-stuff may be ~
           replaced with a SELECTOR having the following syntax: 
         (DUPOND HAS-NAME PERSON :filter a-filter-function)
      which has the following meaning:
       - we want the object that is a person with name DUPOND, and if they are many ~
           we apply our own filter function to get only one."))

(doc (:en "MAKE-INSTANCE-METHOD")
     (:en
      "an INSTANCE-METHOD is a method that applies to instance of a given class as ~
       in any object-oriented environment. Creating a METHOD is done with ~
       the definstmethod macro or the m-make-method function.
        E.g.,
         (definstmethod =summary PERSON ()
             (HAS-NAME))
      Some remarks:
       - =my-print is the name of the method (MOSS convention is to start a method ~
       name with an = sign
       - PERSON is the name of the class
       - (HAS-NAME) is an accessor (built automatically by the system that allows ~
       to get the content of the HAS-NAME slot of the object."))

(doc (:en "make-universal-method")
     (:en
      "a UNIVERSAL-METHOD is a method that applies to all objects whatever their ~
       status. Creating a UNIVERSAL-METHOD is done with the defuniversalmethod ~
       macro or the m-make-universal-method function.
        E.g.,
         (defuniversalmethod =number-of-properties ()
             (length (symbol-value *self*)))
      Some remarks:
       - =my-print is the name of the method (MOSS convention is to start a method ~
       name with an = sign
       - this method uses the internal format of the objects to count the number ~
       of properties (there are much better ways to so that)."))

(doc (:en "MAKE-ATTRIBUTE ; make-terminal-property")
     (:en
      "an ATTRIBUTE, is an object independent from classes. Thus it can be created 
       at any time using the defattribute macro or the %make-attribute function.
        E.g.,
          (defattribute NAME (:entry)(:max 3))
        or
          (%make-attribute 'NAME '(:entry) '(:max 3)) 

     The detailed syntax is the following:
          (defattribute name &rest option-list) - name is external name
     syntax of option list is
   	(:class <class-name>)   to attach to a class
        (:class-id <class-id>)  same as above, but with internal class id
   	(:default <value>)        default, not obeying PDM2 specifications
   	(:is-a <class-id>*)       for defining inheritance
   	(:min <number>)           minimal cardinality
   	(:max <number>)           maximal cardinality
        (:var <var-name>)       id.
   	(:unique)                 minimal and maximal cardinality are each 1
   	(:entry {<function-descriptor>})    specify entry-point
   				if no arg uses make-entry, otherwise uses
   				specified function
     where 
   	<function-descriptor>::=<arg-list><doc><body>
   	e.g.  (:entry (value-list) \"Entry for Company name\"
   				  (intern (make-name value-list)))"))

(doc (:en "MAKE-RELATION")
     (:en
      "a RELATION is used to link two MOSS objects. It can be created using the ~
       defrelation macro or the %make-relation function.
        E.g.,
         (defrelation NEIGHBOR *any* *any* (:max 35))
      This creates a property that can be used between any two MOSS objects ~
       with a limit of 35 on the number of neighbors. That limit however is not ~
       binding. The user is allowed to put more neighbors if needed.

    The detailed syntax is the following:
         (defrelation name class suc system &rest option-list) - creates a relation 
    which is a link between class and suc. If classes do not exist it is an error

     option list:
	(:min <number>)         minimal cardinality
	(:max <number>)         maximal cardinality
        (:from <class-ref>)     domain of the relation
        (:to <class-ref>)       range of the relation
	(:unique)               minimal and maximal cardinality are each 1
	(:inv <prop-name>)      inverse-property name
        (:system)               indicate a system property (not necessary wen :id option)
        (:value-restriction)    imposed value attached to the property
        (:VR-operator <opr>)    tells how the VR is used (all or exist)
        (:var <var-name>)       variable name to record prop id"))

(doc (:en "MAKE-QUERY")
     (:en
      "documentation not availabe yet."))

(doc (:en "MAKE-VERSION")
     (:en
      "documentation not availabe yet."))

;;;=============================================================================
;;; Answers to "HOW TO MODIFY" Questions

(doc (:en "MODIFY-OBJECT")
     (:en
      "it is possible to interactively modify the content of any object by locating ~
       it first and then using the EDITOR.
      Alternatively, a number of methods can be called on the object to add a ~
       value, a link, to remove a value, a property, etc. See the printed doc ~
       on Kernel Methods."))

;;;=============================================================================
;;; Answers to "HOW TO DELETE" Questions

(doc (:en "DELETE-OBJECT")
     (:en
      "an object can be deleted by sending it the message =DELETE.
      However, the object is not removed from the KB, it is simply marked as ~
      unavailable for that particular version."))

(doc (:en "DELETE-ATTRIBUTE")
     (:en
      "deleting an attribute is not the same as deleting a value.
      A value of an attribute or terminal property can be deleted interactively ~
       using the EDITOR or by sending the message:
        (send <obj-id> '=delete-attribute-values <attribute-name> <value-list>)
      Alternatively, one can send a message to the property object:
        (send <tp-id> '=delete <obj-id> <value>)
      Deleting an attribute, i.e., all values of an attribute is done as:
        (send <tp-id '=delete-attribute <obj-id>)"))

(doc (:en "DELETE-RELATION")
     (:en
      "Deleting a RELATION is not the same as deleting an ~
       object from a relation (i.e. severing the link).
      An object can be interactively removed from a relation of structural property ~
       by using the EDITOR or by sending the message: 
        (send <obj-id> '=delete-related-objects <relation-name> <suc-id>)
      Alternatively, one can send a message to the property object:
        (send <rel-id> '=delete <obj-id> <suc-id>)
      Deleting an entire relation, i.e., all values of an attribute is done as:
        (send <obj-id> '=delete-all-successors <relation-ref>)"))

(doc (:en "DELETE-VERSION")
     (:en
      "Currently one cannot delete an entire version of the KB."))

;;;=============================================================================
;;; Answers to "HOW ... FIND ... XXX ..." questions

(doc (:en "FIND-OBJECT")
     (:en 
      "To find an object you can write queries in the MOSS window. Alternatively, ~
       you can give them to the access function:
        (access '(\"person\" (\"name\" :is \"barthès\")))
      The syntax of a query is fairly complex."
      ))

;;;=============================================================================
;;; Answers to "HOW ... START ... MOSS ..." questions

(doc (:en "START-MOSS")
     (:en
      "MOSS is started as any other application on your machine. Alternatively ~
       you can start the Lisp environment and load the MOSS load file."))

;;;=============================================================================
;;; Answers to "SHOW ME ... EXAMPLE ... XXX ..." questions

(doc (:en "CLASS-EXAMPLE")
     (:en 
      "A class represents a concept, e.g., a PERSON, a BOOK, a PROPERTY. It gives 
       the structure of objects that will be considered as its instances. However, 
       an instance needs not have the exact structure described in the class as in 
       most of the OOL. Instance methods, attached to the class, factor part of the 
       behavior of an instance.
      Example:
        Let us define the class PERSON as follows:
           (defconcept PERSON 
                       (:att name (:entry) (:max 3))
                       (:att first-name) 
                       (:rel brother (:to person)))
        stating that a PERSON has a NAME (index) a FIRST-NAME, may have a BROTHER.
        The system returns an internal identifier for the class, here $E-PERSON. 
       Remember that the class is a MOSS object.
        Sending a message to the object is done as follows:
          ? (send _person '=print-object)
          ----- $E-PERSON
          HAS-TYPE: $ENT
          HAS-ENTITY-NAME: PERSON
          HAS-RADIX: $E-PERSON
          IS-ENTITY-LIST-OF: MOSS
          HAS-COUNTER: 1
          HAS-ATTRIBUTE: HAS-NAME / PERSON, HAS-FIRST-NAME / PERSON
          HAS-RELATION: HAS-BROTHER
          IS-SUCCESSOR-OF: HAS-BROTHER
          -----
          \"*done*\"
        which describes the class structure."))

(doc (:en "ATTRIBUTE-EXAMPLE")
     (:en "*not available yet*"))

(doc (:en "RELATION-EXAMPLE")
     (:en "*not available yet*"))

(doc (:en "METHOD-EXAMPLE")
     (:en "*not available yet*"))

(doc (:en "OWN-METHOD-EXAMPLE")
     (:en "*not available yet*"))

(doc (:en "UNIVERSAL-METHOD-EXAMPLE")
     (:en "*not available yet*"))

;;;=============================================================================
;;; Answers to "SHOW ME ... EXAMPLE ... XXX ..." questions

(doc (:en "ONTOLOGY")
     (:en 
      "An ontology is a specification of a conceptualization (Tom Gruber). It ~
       consists of a set of concepts, attributes and relationships. Concepts ~
       may be individual, meaning that if concepts are represented by classes, ~
       then individuals are instances."))


;(format t "~%;*** MOSS v~A - online doc loaded (control) ***" *moss-version-number*)

;:EOF
