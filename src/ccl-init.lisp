
;;; load asdf

#-:asdf (load #P"/Applications/ccl/asdf.lisp")

;(print `("asdf loaded: " ,*features*))

;(pushnew "/Applications/ccl/" asdf:*central-registry* :test #'equal)

(load #P"/Applications/ccl/quicklisp.lisp")
;(require :quicklisp)

(load #P"/Users/barthes/quicklisp/setup.lisp")


(ql:quickload :nodgui)
;(require :nodgui)

(ql:quickload :moss)
;(require :moss)

(nodgui-user::make-moss-init-window)


;;; loading CCL-MOSS unless alt key is down in which case it loads only LISP


#| ;; For some reason this does not work!
(cond
  ((and (print (hi::prompt-for-key #k"a")) nil))

  (t
   ;; load interface
   ;(ql:quickload :nodgui)
   ;; load MOSS
   ;(ql:quickload :moss)
   ;; launch interface
   (nodgui-user::make-moss-init-window)
   ))
|#



#|
(defparameter *OMAS-folder* "OMAS-MOSS 14.0")
;(defparameter *OMAS-folder* "OMAS-MOSS Release 13.0.3 (ACL 9.0) beta")


;;; loading OMAS by default, MOSS if CONTROL key is hold down, nothing if ALT key is
;;; hold down
;;; keys
;;;  ALT: bare Lisp
;;;  CTRL: MOSS in OMAS-MOSS file
;;;  
;;; nothing: OMAS 

(cond 
 ((cg:key-is-down-p cg:vk-alt))
 
 ((cg:key-is-down-p cg:vk-control)
  (load
   (make-pathname 
    :host (pathname-host *load-pathname*)
    :device (pathname-device *load-pathname*)
    :directory (append (pathname-directory *load-pathname*)
                       (list *omas-folder* "MOSS"))
    :name "moss-load"
    :type "fasl")
   :external-format :utf-8)
  )
 ;; while working on MOSS v10
;;; ((cg:key-is-down-p cg:vk-control)
;;;  (load
;;;   (make-pathname 
;;;    :host (pathname-host *load-pathname*)
;;;    :device (pathname-device *load-pathname*)
;;;    :directory (append (pathname-directory *load-pathname*)
;;;                       (list "MOSS 10.0"))
;;;    :name "moss-load"
;;;    :type "fasl")
;;;   :external-format :utf8)
;;;   )
  ;; while working on OMAS/MOSS v14
 ((cg:key-is-down-p cg:vk-shift)
  (load
   (make-pathname 
    :host (pathname-host *load-pathname*)
    :device (pathname-device *load-pathname*)
    :directory (append (pathname-directory *load-pathname*)
                       (list *omas-folder*))
    :name "load-omas-moss"
    :type "fasl")
   :external-format :utf-8)
  )
 (t
  (load
   (make-pathname 
    :host (pathname-host *load-pathname*)
    :device (pathname-device *load-pathname*)
    :directory (append (pathname-directory *load-pathname*)
                       (list *omas-folder*))
    :name "load-omas-moss"
    :type "fasl")
   :external-format :utf-8))
 )
|#