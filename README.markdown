# Moss-Ccl
  MOSS-CCL is a knowledge representation language using an object-oriented framework. It requires CCL, QUICKLISP for loading subsystems, NODGUI for handling
  interface windows, REDIS for persistency. It is meant to work stand alone, i.e. not as a part of OMAS, because the Nodgui interface does not mix well with multithreading.
## Usage
  MOSS-CCL is used for representing versioned multilingual ontologies. Its output may be a Lisp system or an OWL file associated with JENA or SPARQL files. 
  The resulting ontology can also be viewed with an HTML browser.
## Installation
Running MOSS-CCL requires installing other systems. 
1. Install the CCL version of Lisp

The best location is in the `/Applications` forlder

2. Install the `ccl-init.lisp` file at the root of the home directory

It is a good idea to check if the asdf version coming with CCL is not too old. 

```lisp
? (require :asdf)
:ASDF
("uiop" "UIOP" "asdf" "ASDF")
;; check features 
? (asdf::asdf-version)
"3.1.5"
``` 

Otherwise, install the last version of asdf into the `/Applications/ccl`folder
and include the following line in the ccl-init.lisp file:

```lisp
#-:asdf (load #P"/Applications/ccl/asdf.lisp")
```
3. Install a version of quicklisp to load the various libraries.
  You can install a quicklisp folder at the root of the home package and use the 
  `/quicklisp/local-projects/` folder to install the moss-ccl application.

You can add the following lines to your init-ccl.lisp file, which will launch moss-ccl when you start CCL.
```lisp
(load #P"~/quicklisp/quicklisp.lisp")

#-quicklisp
(load #P"~/quicklisp/setup.lisp")

(ql:quickload :moss-ccl)

(nodgui-user::make-moss-init-window)
```
4. Note that on recent versions of MacOSX some versions of CCL crash. You might be led to load the CCL files from the site and recompile CCL if the version 
  obtained from Applestore does not work.
