(defpackage moss-ccl/tests/main
  (:use :cl
        :moss-ccl
        :rove))
(in-package :moss-ccl/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :moss-ccl)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
