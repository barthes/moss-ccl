(defsystem "moss-ccl"
  :version "10.0"
  :author "Jean-Paul Barthès <barthes@utc.fr>"
  :license "CeCILL-B"
  :depends-on ("quicklisp" "nodgui" "cl-redis")
  :components ((:module "src"
                        :components
                        ((:file "main")
                         (:file "packages")
                         (:file "globals" :depends-on ("packages"))
                         (:file "W-init" :depends-on ("packages"))
                         (:file "W-import" :depends-on ("packages" "utils" "persistency"))
                         (:file "mln" :depends-on ("globals"))
                         (:file "texts-UTF8" :depends-on ("globals"))
                         (:file "macros" :depends-on ("globals" "mln"))
                         (:file "utils" :depends-on ("macros" "globals"))
                         (:file "service" :depends-on ("utils"))
                         (:file "boot" :depends-on ("service"))
                         (:file "engine" :depends-on ("boot"))
                         (:file "def" :depends-on ("engine"))
                         (:file "kernel" :depends-on ("def"))
                         (:file "W-window" :depends-on ("engine" "W-import"))
                         (:file "query" :depends-on ("def"))
                         (:file "W-browser" :depends-on ("W-window" "W-window"))
                         (:file "W-overview" :depends-on ("W-window" "W-window"))
                         (:file "W-editor" :depends-on ("W-window"))
                         (:file "dialog-macros" :depends-on ("globals" "utils" "service" ))
                         (:file "dialog-classes" :depends-on ("dialog-macros"))
                         (:file "dialog-engine" :depends-on ("dialog-classes" "paths"))
                         (:file "dialog" :depends-on ("dialog-engine"))
                         (:file "online-doc" :depends-on ("dialog"))
                         ;(:file "W-value-editor") ; to edit values on disk
                         (:file "paths" :depends-on ("query"))
                         (:file "time" :depends-on ("dialog-engine"))
                         (:file "persistency" :depends-on ("kernel"))
                         (:file "web" :depends-on ("persistency"))
                         (:file "export" :depends-on ("W-import"))
                         (:file "W-sol-control" :depends-on ("globals" "W-import"))
                         (:file "sol2html" :depends-on ("W-import"))
                         (:file "sol2owl" :depends-on ("W-sol-control"))
                         (:file "sol2rules" :depends-on ("sol2owl"))
                         )))
  :description "MOSS is a knowledge representation language"
  :in-order-to ((test-op (test-op "moss-ccl/tests"))))

(defsystem "moss-ccl/tests"
  :author ""
  :license ""
  :depends-on ("moss-ccl"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for moss-ccl"
  :perform (test-op (op c) (symbol-call :rove :run c)))
